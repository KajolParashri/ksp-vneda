package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class NewChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View itemView;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    private ImageView profileImageView;
    private TextView nameTextView, designationTextView;
    private CheckBox checkBox;

    public NewChatViewHolder(View itemView, OnRecyclerItemClickListener onRecyclerItemClickListener) {
        super(itemView);

        this.itemView = itemView;
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;

        this.profileImageView = itemView.findViewById(R.id.user_item_profile_pic);
        this.nameTextView = itemView.findViewById(R.id.user_item_fullname);
        this.designationTextView = itemView.findViewById(R.id.user_item_designation);
        this.checkBox = itemView.findViewById(R.id.checkbox);

        itemView.setOnClickListener(this);
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }

    public ImageView getProfileImageView() {
        return profileImageView;
    }

    public TextView getNameTextView() {
        return nameTextView;
    }

    public TextView getDesignationTextView() {
        return designationTextView;
    }

    @Override
    public void onClick(View view) {
        onRecyclerItemClickListener.onClick(view, getAdapterPosition());
    }
}
