package com.indiareads.ksp.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View itemView, imageLayout;
    private LinearLayout chatRootLayout;
    private CardView cardView;
    private ImageView imageViewLeft, imageViewRight, messageImage;
    private TextView nameTextView, messageTextView, attachmentTextView;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    private ProgressBar progressBar;

    public ChatViewHolder(View itemView, OnRecyclerItemClickListener onRecyclerItemClickListener) {
        super(itemView);

        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
        this.itemView = itemView;
        cardView = itemView.findViewById(R.id.chat_item_cardview);
        chatRootLayout = itemView.findViewById(R.id.chat_item_root_layout);
        imageViewLeft = itemView.findViewById(R.id.chat_image_left);
        imageViewRight = itemView.findViewById(R.id.chat_image_right);
        messageImage = itemView.findViewById(R.id.chat_message_image);
        imageLayout = itemView.findViewById(R.id.chat_image_layout);
        nameTextView = itemView.findViewById(R.id.chat_user_name);
        messageTextView = itemView.findViewById(R.id.chat_message);
        attachmentTextView = itemView.findViewById(R.id.chat_attachment);
        progressBar = itemView.findViewById(R.id.progressBar);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onRecyclerItemClickListener.onClick(itemView, getAdapterPosition());
    }

    public View getImageLayout() {
        return imageLayout;
    }

    public View getItemView() {
        return itemView;
    }

    public CardView getCardView() {
        return cardView;
    }

    public LinearLayout getChatRootLayout() {
        return chatRootLayout;
    }

    public ImageView getMessageImage() {
        return messageImage;
    }

    public TextView getAttachmentTextView() {
        return attachmentTextView;
    }

    public ImageView getImageViewLeft() {
        return imageViewLeft;
    }

    public ImageView getImageViewRight() {
        return imageViewRight;
    }

    public TextView getNameTextView() {
        return nameTextView;
    }

    public TextView getMessageTextView() {
        return messageTextView;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }
}
