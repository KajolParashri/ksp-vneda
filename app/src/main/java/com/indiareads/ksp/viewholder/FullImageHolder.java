package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;

public class FullImageHolder extends RecyclerView.ViewHolder {

    public ImageView cover_image;


    public FullImageHolder(View itemView, OnItemClick listener) {
        super(itemView);

        cover_image = itemView.findViewById(R.id.cover_image);

    }

    public ImageView getImageSlider() {
        return cover_image;
    }


}

