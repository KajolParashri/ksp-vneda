package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;

public class UserPointsViewHolder extends RecyclerView.ViewHolder {

    ImageView profile_image;
    TextView userName;
    TextView Points;

    public UserPointsViewHolder(View itemView) {
        super(itemView);

        profile_image = itemView.findViewById(R.id.userImage);
        userName = itemView.findViewById(R.id.userName);
        Points = itemView.findViewById(R.id.pointsText);


    }

    public ImageView getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(ImageView profile_image) {
        this.profile_image = profile_image;
    }

    public TextView getUserName() {
        return userName;
    }

    public void setUserName(TextView userName) {
        this.userName = userName;
    }

    public TextView getPoints() {
        return Points;
    }

    public void setPoints(TextView points) {
        Points = points;
    }
}

