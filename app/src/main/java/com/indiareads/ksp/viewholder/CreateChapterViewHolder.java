package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnChapterButtonsClickListener;
import com.indiareads.ksp.model.Chapter;

import java.util.List;

/**
 * Created by Kashish on 12-10-2018.
 */

public class CreateChapterViewHolder extends ViewHolder implements View.OnClickListener {
    private TextView mChapterTitle, mChapterNo, mSave, mDelete;
    private ImageView mRename;
    private OnChapterButtonsClickListener mListener;
    private List<Chapter> mChapters;

    public CreateChapterViewHolder(View itemView, OnChapterButtonsClickListener listener, List<Chapter> chapters) {
        super(itemView);
        mChapterNo = itemView.findViewById(R.id.chapter_no_text_view);
        mChapterTitle = itemView.findViewById(R.id.chapter_title_text_view);

        mSave = itemView.findViewById(R.id.save_chapter);
        mRename = itemView.findViewById(R.id.rename_chapter);
        mDelete = itemView.findViewById(R.id.delete_chapter);

        mListener = listener;
        mChapters = chapters;

        mSave.setOnClickListener(this);
        mRename.setOnClickListener(this);
        mDelete.setOnClickListener(this);

    }

    public void bindData(Chapter chapter) {
        int chapterNo = 0;

        if ((getAdapterPosition() + 1) < 9) {
            chapterNo = (getAdapterPosition() + 1) % 10;

        } else if ((getAdapterPosition() + 1) > 9 && (getAdapterPosition() + 1) < 100) {
            chapterNo = (getAdapterPosition() + 1) % 100;

        } else {
            chapterNo = (getAdapterPosition() + 1) % 1000;
        }
        mChapterNo.setText(String.valueOf(chapterNo + "."));

        if (chapter.getChapterName() == null) {
            mChapterTitle.setText(" Chapter: Untitled");
        } else mChapterTitle.setText(" Chapter: " + chapter.getChapterName());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_chapter:
                if (getAdapterPosition() != RecyclerView.NO_POSITION)
                    mListener.onEditBtnClick(mChapters.get(getAdapterPosition()), getAdapterPosition());
                break;
            case R.id.rename_chapter:
                if (getAdapterPosition() != RecyclerView.NO_POSITION)
                    mListener.onRenameBtnClick(mChapters.get(getAdapterPosition()), getAdapterPosition());
                break;
            case R.id.delete_chapter:
                if (getAdapterPosition() != RecyclerView.NO_POSITION)
                    mListener.onDeleteBtnClick(mChapters.get(getAdapterPosition()), getAdapterPosition());
                break;
        }
    }
}
