package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;

public class KeywordViewHolder extends RecyclerView.ViewHolder {
    public TextView itemName;


    public TextView getHeading() {
        return itemName;
    }


    public KeywordViewHolder(View itemView, OnItemClick listener) {
        super(itemView);

        itemName = itemView.findViewById(R.id.itemName);

    }
}

