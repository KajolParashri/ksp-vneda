package com.indiareads.ksp.viewholder;

import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class ContentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View view;
    private ImageView contentImage, img_Content;
    RatingBar ratingBar;
    private TextView textContent, textType, author_Name, source_Name;
    Button returnButton;
    TextView textStatus;
    private OnRecyclerItemClickListener mOnClickListener;

    RelativeLayout dummyLay;
    TextView titleDummy, sourceDummy;

    public RatingBar getRatingBar() {
        return ratingBar;
    }

    public RelativeLayout getDummyLay() {
        return dummyLay;
    }

    public TextView getTitleDummy() {
        return titleDummy;
    }

    public TextView getSourceDummy() {
        return sourceDummy;
    }

    public ContentViewHolder(View itemView, OnRecyclerItemClickListener onClickListener) {
        super(itemView);

        view = itemView;

        titleDummy = itemView.findViewById(R.id.titleDummy);
        sourceDummy = itemView.findViewById(R.id.sourceDummy);
        ratingBar = itemView.findViewById(R.id.ratingBar);
        dummyLay = itemView.findViewById(R.id.dummyLay);

        author_Name = itemView.findViewById(R.id.textAuthor);
        source_Name = itemView.findViewById(R.id.textSource);
        contentImage = itemView.findViewById(R.id.mainImage);
        textContent = itemView.findViewById(R.id.textContent);
        textType = itemView.findViewById(R.id.textType);
        img_Content = itemView.findViewById(R.id.img_Content);
        returnButton = itemView.findViewById(R.id.returnButton);
        textStatus = itemView.findViewById(R.id.textStatus);
        ;
        mOnClickListener = onClickListener;

        view.setOnClickListener(this);
    }

    public View getView() {
        return view;
    }

    public TextView getTextStatus() {
        return textStatus;
    }

    @Override
    public void onClick(View v) {
        mOnClickListener.onClick(v, getAdapterPosition());
    }

    public ImageView getContentImage() {
        return contentImage;
    }

    public ImageView getImg_Content() {
        return img_Content;
    }

    public Button getReturnButton() {
        return returnButton;
    }

    public TextView getAuthor_Name() {
        return author_Name;
    }

    public TextView getSource_Name() {
        return source_Name;
    }

    public TextView getTextContent() {
        return textContent;
    }

    public TextView getTextType() {
        return textType;
    }
}