package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnHomeFragmentClickListener;

public class SuggestionViewHolder extends RecyclerView.ViewHolder {

    ImageView profile_image, like_image;
    TextView home_post_item_person_name;
    TextView home_post_item_createdate, home_post_item_description;
    TextView like_count, comment_count;
    TextView home_post_item_person_designation,like_text_sugg;
    LinearLayout comment_layout, like_layout;

    private OnHomeFragmentClickListener mHomeFragmentClickListener;


    public SuggestionViewHolder(View itemView) {
        super(itemView);

        like_text_sugg=itemView.findViewById(R.id.like_text_sugg);
        like_layout = itemView.findViewById(R.id.like_layout);
        like_image = itemView.findViewById(R.id.like_image_sugg);
        like_count = itemView.findViewById(R.id.like_count_sugg);
        profile_image = itemView.findViewById(R.id.profile_image);
        home_post_item_person_name = itemView.findViewById(R.id.home_post_item_person_name);
        home_post_item_description = itemView.findViewById(R.id.home_post_item_description);
        home_post_item_createdate = itemView.findViewById(R.id.home_post_item_createdate);
        comment_count = itemView.findViewById(R.id.comment_count_sugg);
        home_post_item_person_designation = itemView.findViewById(R.id.home_post_item_person_designation);
        comment_layout = itemView.findViewById(R.id.comment_layout_sugg);

    }

    public TextView getLike_text_sugg() {
        return like_text_sugg;
    }

    public LinearLayout getLike_layout() {
        return like_layout;
    }

    public LinearLayout getComment_layout() {
        return comment_layout;
    }

    public ImageView getLike_image() {
        return like_image;
    }

    public TextView getLike_count() {
        return like_count;
    }

    public ImageView getProfile_image() {
        return profile_image;
    }

    public TextView getHome_post_item_person_name() {
        return home_post_item_person_name;
    }

    public TextView getHome_post_item_createdate() {
        return home_post_item_createdate;
    }

    public TextView getHome_post_item_description() {
        return home_post_item_description;
    }

    public TextView getComment_count() {
        return comment_count;
    }

    public TextView getHome_post_item_person_designation() {
        return home_post_item_person_designation;
    }
}

