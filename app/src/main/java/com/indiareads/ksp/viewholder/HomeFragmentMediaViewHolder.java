package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnHomeFragmentClickListener;

public class HomeFragmentMediaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private static final String TAG = HomeFragmentMediaViewHolder.class.getSimpleName();

    private ImageView homeMediaProfileImage;
    private TextView homeMediaPersonName;
    private TextView homeMediaPersonDesignation;
    private TextView homeMediaItemCreateDate;
    private ImageView homeMediaItemMediaImage;
    private TextView homeMediaContentIconImage;
    private TextView homeMediaItemMediaTitle;
    private TextView homeMediaItemMediaDescription;

    private LinearLayout homeMediaLikeBtn;
    private ImageView homeMediaLikeImage;
    private TextView homeMediaLikeCount;
    private TextView homeMediaLikeText;
    private LinearLayout homeMediaCommentBtn;
    private TextView homeMediaCommentText;
    private TextView homeMediaCommentCount;
    private ImageView homeMediaCommentImage;
    public ImageView content_item_icon;
    private OnHomeFragmentClickListener mHomeFragmentClickListener;

    public HomeFragmentMediaViewHolder(View itemView, OnHomeFragmentClickListener homeFragmentClickListener) {
        super(itemView);

        homeMediaProfileImage = itemView.findViewById(R.id.home_media_item_profile_image);
        homeMediaPersonName = itemView.findViewById(R.id.home_media_item_person_name);
        homeMediaPersonDesignation = itemView.findViewById(R.id.home_media_item_person_designation);
        homeMediaItemCreateDate = itemView.findViewById(R.id.home_media_item_createdate);
        homeMediaItemMediaImage = itemView.findViewById(R.id.home_media_item_media_image);
        homeMediaContentIconImage = itemView.findViewById(R.id.home_media_item_media_image_icon);
        homeMediaItemMediaDescription = itemView.findViewById(R.id.home_media_item_media_description);
        homeMediaItemMediaTitle = itemView.findViewById(R.id.home_media_item_media_title);
        content_item_icon = itemView.findViewById(R.id.content_item_icon);
        homeMediaLikeBtn = itemView.findViewById(R.id.like_layout);
        homeMediaLikeImage = itemView.findViewById(R.id.like_image);
        homeMediaLikeCount = itemView.findViewById(R.id.like_count);
        homeMediaLikeText = itemView.findViewById(R.id.like_text);
        homeMediaCommentBtn = itemView.findViewById(R.id.comment_layout);
        homeMediaCommentImage = itemView.findViewById(R.id.comment_image);
        homeMediaCommentCount = itemView.findViewById(R.id.comment_count);
        homeMediaCommentText = itemView.findViewById(R.id.comment_text);

        mHomeFragmentClickListener = homeFragmentClickListener;

        itemView.setOnClickListener(this);
        homeMediaCommentBtn.setOnClickListener(this);
        homeMediaLikeBtn.setOnClickListener(this);
        homeMediaLikeCount.setOnClickListener(this);
        homeMediaProfileImage.setOnClickListener(this);
        homeMediaPersonName.setOnClickListener(this);
        homeMediaPersonDesignation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.like_count) {
            mHomeFragmentClickListener.onLikeCountClicked(v, getAdapterPosition());
        } else if (v.getId() == R.id.like_layout) {

            mHomeFragmentClickListener.onLikeClicked(v, getAdapterPosition());
        } else if (v.getId() == R.id.comment_layout) {
            mHomeFragmentClickListener.onCommentClicked(v, getAdapterPosition());
        } else if (v.getId() == R.id.home_media_item_profile_image || v.getId() == R.id.home_media_item_person_name || v.getId() == R.id.home_media_item_person_designation)
            mHomeFragmentClickListener.onProfileClicked(v, getAdapterPosition());
        else
            mHomeFragmentClickListener.onViewClicked(v, getAdapterPosition());

    }

    public LinearLayout getHomeMediaCommentBtn() {
        return homeMediaCommentBtn;
    }

    public ImageView getHomeMediaCommentImage() {
        return homeMediaCommentImage;
    }

    public TextView getHomeMediaCommentCount() {
        return homeMediaCommentCount;
    }

    public TextView getHomeMediaCommentText() {
        return homeMediaCommentText;
    }

    public LinearLayout getHomeMediaLikeBtn() {
        return homeMediaLikeBtn;
    }

    public ImageView getHomeMediaLikeImage() {
        return homeMediaLikeImage;
    }

    public TextView getHomeMediaLikeCount() {
        return homeMediaLikeCount;
    }

    public TextView getHomeMediaLikeText() {
        return homeMediaLikeText;
    }

    public ImageView getHomeMediaProfileImage() {
        return homeMediaProfileImage;
    }

    public TextView getHomeMediaPersonName() {
        return homeMediaPersonName;
    }

    public TextView getHomeMediaPersonDesignation() {
        return homeMediaPersonDesignation;
    }

    public TextView getHomeMediaItemCreateDate() {
        return homeMediaItemCreateDate;
    }

    public ImageView getHomeMediaItemMediaImage() {
        return homeMediaItemMediaImage;
    }

    public TextView getHomeMediaItemMediaTitle() {
        return homeMediaItemMediaTitle;
    }

    public TextView getHomeMediaItemMediaDescription() {
        return homeMediaItemMediaDescription;
    }

    public TextView getHomeMediaContentIconImage() {
        return homeMediaContentIconImage;
    }

    public ImageView getContent_item_icon() {
        return content_item_icon;
    }
}
