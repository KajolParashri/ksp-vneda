package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnAddressClickListener;
import com.indiareads.ksp.model.Address;

public class NormalLoaderViewHolder extends RecyclerView.ViewHolder {
    ProgressBar progressBar;

    public NormalLoaderViewHolder(View itemView, OnAddressClickListener listener) {
        super(itemView);

        progressBar = itemView.findViewById(R.id.progressBar);
    }

    public NormalLoaderViewHolder(View view) {
        super(view);

    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

}
