package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;

public class DoubleImageHolder extends RecyclerView.ViewHolder {

    public ImageView item1, item2;


    public DoubleImageHolder(View itemView, OnItemClick listener) {
        super(itemView);

        item1 = itemView.findViewById(R.id.item1);
        item2 = itemView.findViewById(R.id.item2);
    }

    public ImageView getItem1() {
        return item1;
    }

    public ImageView getItem2() {
        return item2;
    }

}

