package com.indiareads.ksp.viewholder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.NotificationModel;
import com.indiareads.ksp.utils.CircleTransform;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Urls;
import com.squareup.picasso.Picasso;

public class NotifcationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView notificationText, dateTime;
    private ImageView profilePic;
    private OnItemClick listener;

    public NotifcationViewHolder(View itemView, OnItemClick listener) {
        super(itemView);
        this.listener = listener;

        notificationText = itemView.findViewById(R.id.notificationText);
        dateTime = itemView.findViewById(R.id.datetime);
        profilePic = itemView.findViewById(R.id.userImage);

        itemView.setOnClickListener(this);
    }

    public void setData(NotificationModel notificationModel, Activity activity) {
        String mainMsg = "";
        if (notificationModel.getActivityValue().equals("commented")) {
            mainMsg = notificationModel.getActivityType() + " on your" + notificationModel.getActivityValue();
        } else {
            mainMsg = notificationModel.getActivityType() + " your " + notificationModel.getActivityValue();
        }
        notificationText.setText(notificationModel.getUserName() + " " + mainMsg);

        dateTime.setText(CommonMethods.getElapsedTime(notificationModel.getCreatedDate()));

        Picasso.get().load(notificationModel.getProfileURL())
                .placeholder(R.drawable.profile).error(R.drawable.profile)
                .transform(new CircleTransform()).into(profilePic);

    }


    @Override
    public void onClick(View view) {
        if (getAdapterPosition() != RecyclerView.NO_POSITION){

        }
            //listener.onCategoryClick();
    }
}
