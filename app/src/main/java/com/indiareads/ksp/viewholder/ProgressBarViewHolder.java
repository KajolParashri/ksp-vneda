package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.indiareads.ksp.R;

public class ProgressBarViewHolder extends RecyclerView.ViewHolder {

    public ShimmerFrameLayout shimmerFrameLayout;

    public ProgressBarViewHolder(View itemView) {
        super(itemView);

        shimmerFrameLayout = itemView.findViewById(R.id.shimmer_view_container);
    }

    public ShimmerFrameLayout getShimmer() {
        return shimmerFrameLayout;
    }


}
