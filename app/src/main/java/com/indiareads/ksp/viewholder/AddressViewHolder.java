package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnAddressClickListener;
import com.indiareads.ksp.model.Address;

public class AddressViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView fullName, addressLine1, addressLine2, city, phone;
    private OnAddressClickListener listener;
    RelativeLayout delte_lay;
    ProgressBar progressBar;

    public AddressViewHolder(View itemView, OnAddressClickListener listener) {
        super(itemView);
        this.listener = listener;
        fullName = itemView.findViewById(R.id.address_item_fullname);
        addressLine1 = itemView.findViewById(R.id.address_item_line1);
        addressLine2 = itemView.findViewById(R.id.address_item_line2);
        city = itemView.findViewById(R.id.address_item_city);
        phone = itemView.findViewById(R.id.address_item_phone);
        delte_lay = itemView.findViewById(R.id.delte_lay);
        progressBar = itemView.findViewById(R.id.progressBar);
        itemView.setOnClickListener(this);
    }

    public AddressViewHolder(View view) {
        super(view);

    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public RelativeLayout getDelte_lay() {
        return delte_lay;
    }

    public void setDelte_lay(RelativeLayout delte_lay) {
        this.delte_lay = delte_lay;
    }

    public void setData(Address address) {
        fullName.setText(address.fullname);

        if (!address.addressLine1.isEmpty())
            addressLine1.setText(address.addressLine1);
        else
            addressLine1.setVisibility(View.GONE);
        if (!address.addressLine2.isEmpty())
            addressLine2.setText(address.addressLine2);
        else
            addressLine2.setVisibility(View.GONE);
        city.setText(address.city + ", " + address.state + " - " + address.pincode);
        phone.setText("Mobile: " + address.phone);
    }

    @Override
    public void onClick(View view) {
        if (getAdapterPosition() != RecyclerView.NO_POSITION)
            listener.OnAddressClick(getAdapterPosition());
    }
}
