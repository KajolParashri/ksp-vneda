package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnAddressClickListener;
import com.indiareads.ksp.model.Address;

public class PointsHolder extends RecyclerView.ViewHolder {
    private TextView mainText;


    public PointsHolder(View itemView) {
        super(itemView);

        mainText = itemView.findViewById(R.id.mainText);

    }

    public TextView getMainText() {
        return mainText;
    }
}
