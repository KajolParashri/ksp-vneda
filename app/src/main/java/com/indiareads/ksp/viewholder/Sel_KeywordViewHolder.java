package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;

public class Sel_KeywordViewHolder extends RecyclerView.ViewHolder {

    public TextView selected_text;

    public TextView getHeading() {
        return selected_text;
    }


    public Sel_KeywordViewHolder(View itemView, OnItemClick listener) {
        super(itemView);

        selected_text = itemView.findViewById(R.id.selected_text);

    }
}

