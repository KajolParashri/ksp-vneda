package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnDirectoryItemClickListener;

public class DirectoryItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View view;
    private ImageView fileIcon;
    private ImageView optionsIcon;
    private TextView itemName;

    private OnDirectoryItemClickListener mOnClickListener;

    public DirectoryItemViewHolder(View itemView, OnDirectoryItemClickListener onClickListener) {
        super(itemView);

        view = itemView;
        fileIcon = itemView.findViewById(R.id.directory_item_icon);
        optionsIcon = itemView.findViewById(R.id.directory_item_options);
        itemName = itemView.findViewById(R.id.directory_item_name);
        mOnClickListener = onClickListener;

        fileIcon.setOnClickListener(this);
        itemName.setOnClickListener(this);
        optionsIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.directory_item_options)
            mOnClickListener.onOptionsClicked(v, getAdapterPosition());
        else
            mOnClickListener.onItemClicked(v, getAdapterPosition());
    }

    public View getView() {
        return view;
    }

    public ImageView getFileIcon() {
        return fileIcon;
    }

    public ImageView getOptionsIcon() {
        return optionsIcon;
    }

    public TextView getItemName() {
        return itemName;
    }
}
