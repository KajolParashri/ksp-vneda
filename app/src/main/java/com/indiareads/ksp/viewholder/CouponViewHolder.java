package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnAddressClickListener;
import com.indiareads.ksp.model.Address;

public class CouponViewHolder extends RecyclerView.ViewHolder {
    private TextView titleCoupon, textCoupon, validity, termsAndConditon;

    public CouponViewHolder(View itemView) {
        super(itemView);
        titleCoupon = itemView.findViewById(R.id.titleCoupon);
        textCoupon = itemView.findViewById(R.id.couponMain);
        validity = itemView.findViewById(R.id.validity);
        termsAndConditon = itemView.findViewById(R.id.termsAndConditon);
    }

    public TextView getTermsAndConditon() {
        return termsAndConditon;
    }

    public TextView getTitleCoupon() {
        return titleCoupon;
    }

    public TextView getTextCoupon() {
        return textCoupon;
    }

    public TextView getValidity() {
        return validity;
    }


}
