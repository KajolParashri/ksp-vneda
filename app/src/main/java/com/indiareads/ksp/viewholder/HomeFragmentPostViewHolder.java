package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnHomeFragmentClickListener;
import com.indiareads.ksp.utils.Logger;

public class HomeFragmentPostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private static final String TAG = HomeFragmentPostViewHolder.class.getSimpleName();

    private ImageView homePostProfileImage;
    private TextView homePostPersonName;
    private TextView homePostPersonDesignation;
    private TextView homePostItemCreateDate;
    private TextView homePostItemDescription;
    private ImageView deletePost;
    private LinearLayout homePostLikeBtn;
    private ImageView homePostLikeImage;
    private TextView homePostLikeCount;
    private TextView homePostLikeText;

    private LinearLayout homePostCommentBtn;
    private TextView homePostCommentText;
    private TextView homePostCommentCount;
    private ImageView homePostCommentImage;

    private OnHomeFragmentClickListener mHomeFragmentClickListener;


    public ImageView getDeletePost() {
        return deletePost;
    }

    public void setDeletePost(ImageView deletePost) {
        this.deletePost = deletePost;
    }

    public HomeFragmentPostViewHolder(View itemView, OnHomeFragmentClickListener homeFragmentClickListener) {
        super(itemView);

        homePostCommentBtn = itemView.findViewById(R.id.comment_layout);
        homePostCommentImage = itemView.findViewById(R.id.comment_image);
        homePostCommentCount = itemView.findViewById(R.id.comment_count);
        homePostCommentText = itemView.findViewById(R.id.comment_text);
        deletePost = itemView.findViewById(R.id.deletePost);

        homePostLikeBtn = itemView.findViewById(R.id.like_layout);
        homePostLikeImage = itemView.findViewById(R.id.like_image);
        homePostLikeCount = itemView.findViewById(R.id.like_count);
        homePostLikeText = itemView.findViewById(R.id.like_text);

        homePostProfileImage = itemView.findViewById(R.id.home_post_item_profile_image);
        homePostPersonName = itemView.findViewById(R.id.home_post_item_person_name);
        homePostPersonDesignation = itemView.findViewById(R.id.home_post_item_person_designation);
        homePostItemCreateDate = itemView.findViewById(R.id.home_post_item_createdate);
        homePostItemDescription = itemView.findViewById(R.id.home_post_item_description);

        mHomeFragmentClickListener = homeFragmentClickListener;

        homePostLikeBtn.setOnClickListener(this);
        homePostPersonName.setOnClickListener(this);
        homePostCommentBtn.setOnClickListener(this);
        homePostLikeCount.setOnClickListener(this);
        deletePost.setOnClickListener(this);
    }

    public LinearLayout getHomePostCommentBtn() {
        return homePostCommentBtn;
    }

    public ImageView getHomePostCommentImage() {
        return homePostCommentImage;
    }

    public TextView getHomePostCommentCount() {
        return homePostCommentCount;
    }

    public TextView getHomePostCommentText() {
        return homePostCommentText;
    }

    public LinearLayout getHomePostLikeBtn() {
        return homePostLikeBtn;
    }

    public ImageView getHomePostLikeImage() {
        return homePostLikeImage;
    }

    public TextView getHomePostLikeCount() {
        return homePostLikeCount;
    }

    public TextView getHomePostLikeText() {
        return homePostLikeText;
    }

    public ImageView getHomePostProfileImage() {
        return homePostProfileImage;
    }

    public TextView getHomePostPersonName() {
        return homePostPersonName;
    }

    public TextView getHomePostPersonDesignation() {
        return homePostPersonDesignation;
    }

    public TextView getHomePostItemCreateDate() {
        return homePostItemCreateDate;
    }

    public TextView getHomePostItemDescription() {
        return homePostItemDescription;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.like_count) {
            mHomeFragmentClickListener.onLikeCountClicked(v, getAdapterPosition());
        } else if (v.getId() == R.id.like_layout) {
            mHomeFragmentClickListener.onLikeClicked(v, getAdapterPosition());
        } else if (v.getId() == R.id.comment_layout) {
            mHomeFragmentClickListener.onCommentClicked(v, getAdapterPosition());
        } else if (v.getId() == R.id.home_post_item_person_name) {
            Logger.error("POSTINTENT", " < UserID    Company ID >");
            mHomeFragmentClickListener.onProfileClicked(v, getAdapterPosition());
        } else if (v.getId() == R.id.deletePost) {
            mHomeFragmentClickListener.onDeleteClicked(v, getAdapterPosition());
        }
    }
}
