package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;

public class AnnouncementViewHolder extends RecyclerView.ViewHolder {

    private TextView homeAnnouncementItemCreateDate;
    private TextView homeAnnouncementItemDescription;

    public AnnouncementViewHolder(View itemView) {
        super(itemView);

        homeAnnouncementItemCreateDate = itemView.findViewById(R.id.announcement_item_createdate);
        homeAnnouncementItemDescription = itemView.findViewById(R.id.announcement_item_description);

    }

    public TextView getHomeAnnouncementItemCreateDate() {
        return homeAnnouncementItemCreateDate;
    }

    public TextView getHomeAnnouncementItemDescription() {
        return homeAnnouncementItemDescription;
    }
}

