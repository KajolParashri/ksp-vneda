package com.indiareads.ksp.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;

public class ExploreMoreHolder extends RecyclerView.ViewHolder {

    public CardView articleCard;
    public CardView videoCard;
    public CardView courseCard;
    public CardView podcastCard;
    public CardView infoCard;
    public CardView booksCard;


    public ExploreMoreHolder(View itemView, OnItemClick listener) {
        super(itemView);

        articleCard = itemView.findViewById(R.id.articleCard);
        videoCard = itemView.findViewById(R.id.videoCard);
        courseCard = itemView.findViewById(R.id.coursesCard);

        podcastCard = itemView.findViewById(R.id.podcastCard);
        infoCard = itemView.findViewById(R.id.infoCard);
        booksCard = itemView.findViewById(R.id.booksCard);

    }

    public CardView getArticleCard() {
        return articleCard;
    }

    public void setArticleCard(CardView articleCard) {
        this.articleCard = articleCard;
    }

    public CardView getVideoCard() {
        return videoCard;
    }

    public void setVideoCard(CardView videoCard) {
        this.videoCard = videoCard;
    }

    public CardView getCourseCard() {
        return courseCard;
    }

    public void setCourseCard(CardView courseCard) {
        this.courseCard = courseCard;
    }

    public CardView getPodcastCard() {
        return podcastCard;
    }

    public void setPodcastCard(CardView podcastCard) {
        this.podcastCard = podcastCard;
    }

    public CardView getInfoCard() {
        return infoCard;
    }

    public void setInfoCard(CardView infoCard) {
        this.infoCard = infoCard;
    }

    public CardView getBooksCard() {
        return booksCard;
    }

    public void setBooksCard(CardView booksCard) {
        this.booksCard = booksCard;
    }
}

