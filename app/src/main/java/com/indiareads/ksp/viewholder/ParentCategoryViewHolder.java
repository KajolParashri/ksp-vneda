package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnCategoryClickListener;
import com.indiareads.ksp.listeners.OnFilterCategoryClickListener;

public class ParentCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private View itemView;
    private TextView categoryNameTextView;
    private RecyclerView childCategoryRecyclerView;
    private OnFilterCategoryClickListener onFilterCategoryClickListener;

    public ParentCategoryViewHolder(View itemView,OnFilterCategoryClickListener onFilterCategoryClickListener) {
        super(itemView);

        this.itemView = itemView;
        categoryNameTextView = itemView.findViewById(R.id.fragment_filter_parent_category);
        childCategoryRecyclerView = itemView.findViewById(R.id.fragment_filter_child_recyclerview);
        this.onFilterCategoryClickListener = onFilterCategoryClickListener;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onFilterCategoryClickListener.onParentCategoryClicked(itemView,getAdapterPosition());
    }

    public TextView getCategoryNameTextView() {
        return categoryNameTextView;
    }

    public RecyclerView getChildCategoryRecyclerView() {
        return childCategoryRecyclerView;
    }
}
