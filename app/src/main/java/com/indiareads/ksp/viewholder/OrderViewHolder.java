package com.indiareads.ksp.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnOrderClickListener;
import com.indiareads.ksp.model.Order;
import com.indiareads.ksp.utils.CommonMethods;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;
import static com.indiareads.ksp.utils.CommonMethods.loadNormalImageWithGlide;
import static com.indiareads.ksp.utils.Constants.Order.CANCELLED;
import static com.indiareads.ksp.utils.Constants.Order.DELIVERED;
import static com.indiareads.ksp.utils.Constants.Order.ORDER_DISPATCHED;
import static com.indiareads.ksp.utils.Constants.Order.ORDER_PLACED;
import static com.indiareads.ksp.utils.Constants.Order.ORDER_PROCESSING;
import static com.indiareads.ksp.utils.Constants.Order.PICKUP_PROCESSING;
import static com.indiareads.ksp.utils.Constants.Order.PICKUP_REQUESTED;
import static com.indiareads.ksp.utils.Constants.Order.RETURNED;
import static com.indiareads.ksp.utils.Constants.Order.SENT_TO_COURIER;
import static com.indiareads.ksp.utils.Urls.baseAWSUrl;

public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public Context mContext;
    public OnOrderClickListener mListener;
    public TextView orderId, orderTitle, orderDate, orderStatus;
    public ImageView orderImage;
    public Button mCancelReturnButton;
    public Order mOrder;
    public ProgressBar progressBar;


    public RelativeLayout dummyLay;
    public TextView titleDummy, sourceDummy;

    public OrderViewHolder(View itemView, OnOrderClickListener listener, Context context) {
        super(itemView);
        mContext = context;
        mListener = listener;
        titleDummy = itemView.findViewById(R.id.titleDummy);
        sourceDummy = itemView.findViewById(R.id.sourceDummy);

        dummyLay = itemView.findViewById(R.id.dummyLay);

        orderId = itemView.findViewById(R.id.view_order_id);
        orderTitle = itemView.findViewById(R.id.view_order_title);
        orderDate = itemView.findViewById(R.id.view_order_date);
        orderImage = itemView.findViewById(R.id.order_book_image);
        orderStatus = itemView.findViewById(R.id.view_order_status);
        mCancelReturnButton = itemView.findViewById(R.id.perform_order_status);
        progressBar = itemView.findViewById(R.id.cancel_order_progress);
        mCancelReturnButton.setOnClickListener(this);
    }

    public TextView getOrderId() {
        return orderId;
    }

    public TextView getOrderTitle() {
        return orderTitle;
    }

    public TextView getOrderDate() {
        return orderDate;
    }

    public TextView getOrderStatus() {
        return orderStatus;
    }

    public ImageView getOrderImage() {
        return orderImage;
    }

    public Button getmCancelReturnButton() {
        return mCancelReturnButton;
    }

    public Order getmOrder() {
        return mOrder;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public RelativeLayout getDummyLay() {
        return dummyLay;
    }

    public TextView getTitleDummy() {
        return titleDummy;
    }

    public TextView getSourceDummy() {
        return sourceDummy;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.perform_order_status) {

            if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                view.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                mListener.OnCancelClick(getAdapterPosition());

            }
        }
    }
}
