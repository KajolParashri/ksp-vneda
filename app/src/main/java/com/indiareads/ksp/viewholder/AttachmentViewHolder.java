package com.indiareads.ksp.viewholder;

import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class AttachmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    View view;
    private TextView attachmentName;
    private OnRecyclerItemClickListener mOnClickListener;
    TextView extension;

    public AttachmentViewHolder(View itemView, OnRecyclerItemClickListener onClickListener) {
        super(itemView);

        view = itemView;
        extension = itemView.findViewById(R.id.extension);
        attachmentName = itemView.findViewById(R.id.attachment_item_name);
        mOnClickListener = onClickListener;
        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mOnClickListener.onClick(v, getAdapterPosition());
    }

    public TextView getAttachmentName() {
        return attachmentName;
    }

    public TextView getExtension() {
        return extension;
    }
}