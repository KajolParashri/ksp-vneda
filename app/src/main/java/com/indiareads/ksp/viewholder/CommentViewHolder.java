package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;

/**
 * Created by Kashish on 05-08-2018.
 */

public class CommentViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = CommentViewHolder.class.getSimpleName();

    private ImageView commentUserImage;
    private TextView commentUserName;
    private TextView commentDate;
    private TextView commentContent;


    public CommentViewHolder(View itemView) {
        super(itemView);

        commentUserImage = itemView.findViewById(R.id.comment_image);
        commentContent = itemView.findViewById(R.id.comment_data);
        commentDate = itemView.findViewById(R.id.comment_time);
        commentUserName = itemView.findViewById(R.id.comment_name);

    }

    public ImageView getCommentUserImage() {
        return commentUserImage;
    }

    public TextView getCommentUserName() {
        return commentUserName;
    }

    public void setCommentUserName(String commentUserName) {
        this.commentUserName.setText(commentUserName);
    }

    public TextView getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate.setText(commentDate);
    }

    public TextView getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent.setText(commentContent);
    }

}
