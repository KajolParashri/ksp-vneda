package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;

public class ExploreMainHolder extends RecyclerView.ViewHolder {
    public TextView textTile, typeContent;
    public ImageView imgTile;


    public ExploreMainHolder(View itemView, OnItemClick listener) {
        super(itemView);

        typeContent = itemView.findViewById(R.id.typeContent);
        imgTile = itemView.findViewById(R.id.imgTile);
        textTile = itemView.findViewById(R.id.textTile);
    }

    public TextView getTextTile() {
        return textTile;
    }


    public TextView getTypeContent() {
        return typeContent;
    }

    public ImageView getImgTile() {
        return imgTile;
    }

}