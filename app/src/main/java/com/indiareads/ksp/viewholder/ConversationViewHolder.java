package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class ConversationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View itemView;
    private View rootView;
    private ImageView imageView;
    private TextView nameTextView, messageTextView, readCountTextView;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public ConversationViewHolder(View itemView) {
        super(itemView);
    }

    public ConversationViewHolder(View itemView, OnRecyclerItemClickListener onRecyclerItemClickListener) {
        super(itemView);

        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
        this.itemView = itemView;
        rootView = itemView.findViewById(R.id.rootView);
        imageView = itemView.findViewById(R.id.conversation_image);
        nameTextView = itemView.findViewById(R.id.conversation_name);
        messageTextView = itemView.findViewById(R.id.conversation_message);
        readCountTextView = itemView.findViewById(R.id.read_count);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onRecyclerItemClickListener.onClick(itemView, getAdapterPosition());
    }

    public View getItemView() {
        return itemView;
    }

    public View getRootView() {
        return rootView;
    }

    public TextView getReadCountTextView() {
        return readCountTextView;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getNameTextView() {
        return nameTextView;
    }

    public TextView getMessageTextView() {
        return messageTextView;
    }
}
