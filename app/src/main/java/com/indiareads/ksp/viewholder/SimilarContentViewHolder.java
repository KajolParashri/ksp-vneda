package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;

public class SimilarContentViewHolder extends RecyclerView.ViewHolder {

    View view;
    ImageView contentImage, contentIconImage;
    TextView contentTitle, typeText, contentDescription;

    RelativeLayout dummyLay;
    TextView titleDummy, sourceDummy;

    public RelativeLayout getDummyLay() {
        return dummyLay;
    }

    public TextView getTitleDummy() {
        return titleDummy;
    }

    public TextView getSourceDummy() {
        return sourceDummy;
    }

    public SimilarContentViewHolder(View itemView) {
        super(itemView);

        view = itemView;

        titleDummy = itemView.findViewById(R.id.titleDummy);
        sourceDummy = itemView.findViewById(R.id.sourceDummy);

        dummyLay = itemView.findViewById(R.id.dummyLay);
        typeText = view.findViewById(R.id.textType);
        contentImage = view.findViewById(R.id.mainImage);
        contentIconImage = view.findViewById(R.id.img_Content);
        contentTitle = view.findViewById(R.id.textContent);
        contentDescription = view.findViewById(R.id.textAuthor);
    }

    public View getView() {
        return view;
    }

    public TextView getTypeText() {
        return typeText;
    }

    public ImageView getContentImage() {
        return contentImage;
    }

    public TextView getContentTitle() {
        return contentTitle;
    }

    public TextView getContentDescription() {
        return contentDescription;
    }

    public ImageView getContentIconImage() {
        return contentIconImage;
    }
}

