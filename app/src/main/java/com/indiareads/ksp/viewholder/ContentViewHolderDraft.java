package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnDraftedContentItemClickListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class ContentViewHolderDraft extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View view;
    private ImageView contentImage, img_Content;
    private TextView textContent, textType;
    ImageView edit, delete;
    RelativeLayout edtlay, delLay;
    private OnDraftedContentItemClickListener mOnClickListener;

    TextView titleDummy;
    RelativeLayout dummyLay;

    public ContentViewHolderDraft(View itemView, OnDraftedContentItemClickListener onClickListener) {
        super(itemView);

        view = itemView;

        titleDummy = itemView.findViewById(R.id.titleDummy);
        dummyLay = itemView.findViewById(R.id.dummyLay);


        contentImage = itemView.findViewById(R.id.mainImage);
        textContent = itemView.findViewById(R.id.textContent);
        textType = itemView.findViewById(R.id.textType);
        img_Content = itemView.findViewById(R.id.img_Content);

        edit = itemView.findViewById(R.id.edit);
        delete = itemView.findViewById(R.id.delete);

        edtlay = itemView.findViewById(R.id.edtlay);
        delLay = itemView.findViewById(R.id.delLay);

        mOnClickListener = onClickListener;

        view.setOnClickListener(this);
    }

    public TextView getTitleDummy() {
        return titleDummy;
    }

    public RelativeLayout getDummyLay() {
        return dummyLay;
    }

    public ImageView getEdit() {
        return edit;
    }

    public ImageView getDelete() {
        return delete;
    }

    @Override
    public void onClick(View v) {
        mOnClickListener.onItemClick(v, getAdapterPosition());
    }

    public ImageView getContentImage() {
        return contentImage;
    }

    public RelativeLayout getEdtlay() {
        return edtlay;
    }

    public RelativeLayout getDelLay() {
        return delLay;
    }

    public ImageView getImg_Content() {
        return img_Content;
    }

    public TextView getTextContent() {
        return textContent;
    }

    public TextView getTextType() {
        return textType;
    }
}