package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;

public class BookAdapterHolder extends RecyclerView.ViewHolder {
    public TextView heading;
    public RecyclerView list_item;

    public TextView getHeading() {
        return heading;
    }

    public void setHeading(TextView heading) {
        this.heading = heading;
    }

    public RecyclerView getList_item() {
        return list_item;
    }

    public void setList_item(RecyclerView list_item) {
        this.list_item = list_item;
    }

    public BookAdapterHolder(View itemView) {
        super(itemView);

        list_item = itemView.findViewById(R.id.list_item);
        heading = itemView.findViewById(R.id.heading);

    }
}
