package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnHomeFragmentClickListener;

public class HomeFragmentAnnouncementViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ImageView homeAnnouncementProfileImage;
    private TextView homeAnnouncementPersonName;
    private TextView homeAnnouncementPersonDesignation;
    private TextView homeAnnouncementItemCreateDate;
    private TextView homeAnnouncementItemTitle;
    private TextView homeAnnouncementItemDescription;

    private OnHomeFragmentClickListener mHomeFragmentClickListener;

    public HomeFragmentAnnouncementViewHolder(View itemView, OnHomeFragmentClickListener mHomeFragmentClickListener) {
        super(itemView);

        this.mHomeFragmentClickListener = mHomeFragmentClickListener;
        homeAnnouncementProfileImage = itemView.findViewById(R.id.home_announcement_item_profile_image);
        homeAnnouncementPersonName = itemView.findViewById(R.id.home_announcement_item_person_name);
        homeAnnouncementPersonDesignation = itemView.findViewById(R.id.home_announcement_item_person_designation);
        homeAnnouncementItemCreateDate = itemView.findViewById(R.id.home_announcement_item_createdate);
        homeAnnouncementItemTitle = itemView.findViewById(R.id.home_announcement_item_title);
        homeAnnouncementItemDescription = itemView.findViewById(R.id.home_announcement_item_description);

        homeAnnouncementPersonName.setOnClickListener(this);
    }

    public ImageView getHomeAnnouncementProfileImage() {
        return homeAnnouncementProfileImage;
    }

    public TextView getHomeAnnouncementPersonName() {
        return homeAnnouncementPersonName;
    }

    public TextView getHomeAnnouncementPersonDesignation() {
        return homeAnnouncementPersonDesignation;
    }

    public TextView getHomeAnnouncementItemCreateDate() {
        return homeAnnouncementItemCreateDate;
    }

    public TextView getHomeAnnouncementItemTitle() {
        return homeAnnouncementItemTitle;
    }

    public TextView getHomeAnnouncementItemDescription() {
        return homeAnnouncementItemDescription;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.home_announcement_item_person_name) {
            mHomeFragmentClickListener.onProfileClicked(v, getAdapterPosition());
        }
    }
}
