package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class DirectoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private View view;
    private TextView directoryName;

    private OnRecyclerItemClickListener mOnClickListener;

    public DirectoryViewHolder(View itemView, OnRecyclerItemClickListener onClickListener) {
        super(itemView);

        view = itemView;
        directoryName = itemView.findViewById(R.id.directory_item_name);
        mOnClickListener = onClickListener;

        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mOnClickListener.onClick(v, getAdapterPosition());
    }

    public View getView() {
        return view;
    }

    public TextView getDirectoryName() {
        return directoryName;
    }

    public OnRecyclerItemClickListener getmOnClickListener() {
        return mOnClickListener;
    }
}
