package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class BreakageHistoryHolder extends RecyclerView.ViewHolder {

    private TextView dealtitle;
    private TextView burnedPoints;

    public BreakageHistoryHolder(View itemView) {
        super(itemView);

        dealtitle = itemView.findViewById(R.id.dealtitle);
        burnedPoints = itemView.findViewById(R.id.burnedPoints);
    }

    public TextView getTitle() {
        return dealtitle;
    }

    public TextView getBurnedPoints() {
        return burnedPoints;
    }
}


