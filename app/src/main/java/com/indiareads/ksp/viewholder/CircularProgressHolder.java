package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class CircularProgressHolder extends RecyclerView.ViewHolder {

    private ProgressBar contentImage;

    public CircularProgressHolder(View itemView) {
        super(itemView);
        contentImage = itemView.findViewById(R.id.progressBar);
    }


}

