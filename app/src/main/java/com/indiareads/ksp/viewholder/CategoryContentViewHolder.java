package com.indiareads.ksp.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnDraftedContentItemClickListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class CategoryContentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View view;
    private ImageView contentImage, img_Content;
    private TextView textContent, textType, txtAuthor, txtSource;
    private RatingBar ratingBar;
    OnDraftedContentItemClickListener mOnClickListenerMain;
    RelativeLayout dummyLay;
    TextView titleDummy, sourceDummy;
    CardView card_Main_item;

    RelativeLayout ed_de_layout, edtlay, delLay;

    public RelativeLayout getEd_de_layout() {
        return ed_de_layout;
    }

    public RelativeLayout getEdtlay() {
        return edtlay;
    }

    public RelativeLayout getDelLay() {
        return delLay;
    }

    public CategoryContentViewHolder(View itemView, OnDraftedContentItemClickListener mOnClickListener) {
        super(itemView);

        view = itemView;
        ed_de_layout = itemView.findViewById(R.id.ed_de_layout);
        edtlay = itemView.findViewById(R.id.edtlay);
        delLay = itemView.findViewById(R.id.delLay);

        titleDummy = itemView.findViewById(R.id.titleDummy);
        sourceDummy = itemView.findViewById(R.id.sourceDummy);
        card_Main_item = itemView.findViewById(R.id.card_Main_item);

        dummyLay = itemView.findViewById(R.id.dummyLay);

        contentImage = itemView.findViewById(R.id.mainImage);
        textContent = itemView.findViewById(R.id.textContent);
        textType = itemView.findViewById(R.id.textType);
        img_Content = itemView.findViewById(R.id.img_Content);

        txtAuthor = itemView.findViewById(R.id.textAuthor);
        txtSource = itemView.findViewById(R.id.textSource);
        ratingBar = itemView.findViewById(R.id.ratingBar);
        mOnClickListenerMain = mOnClickListener;
        card_Main_item.setOnClickListener(this);
    }

    public RelativeLayout getDummyLay() {
        return dummyLay;
    }

    public TextView getTitleDummy() {
        return titleDummy;
    }

    public TextView getSourceDummy() {
        return sourceDummy;
    }

    public RatingBar getRatingBar() {
        return ratingBar;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card_Main_item:
                mOnClickListenerMain.onItemClick(v, getAdapterPosition());
                break;
        }
    }

    public ImageView getContentImage() {
        return contentImage;
    }

    public ImageView getImg_Content() {
        return img_Content;
    }

    public TextView getTextContent() {
        return textContent;
    }

    public TextView getTextType() {
        return textType;
    }


    public TextView getTxtAuthor() {
        return txtAuthor;
    }

    public TextView getTxtSource() {
        return txtSource;
    }
}