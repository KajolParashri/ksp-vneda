package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;

public class ChapterViewHolder extends RecyclerView.ViewHolder {

    View view;
    TextView chapterName;
    TextView chapterNumber;

    public ChapterViewHolder(View itemView) {
        super(itemView);

        view = itemView;
        chapterName = itemView.findViewById(R.id.chapter_name);
        //chapterNumber
        chapterNumber = itemView.findViewById(R.id.chapter_number);
    }

    public TextView getChapterNumber() {
        return chapterNumber;
    }

    public View getView() {
        return view;
    }

    public TextView getChapterName() {
        return chapterName;
    }
}
