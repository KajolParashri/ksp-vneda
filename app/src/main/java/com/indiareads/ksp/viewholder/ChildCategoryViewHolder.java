package com.indiareads.ksp.viewholder;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnFilterCategoryClickListener;

public class ChildCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private View itemView;
    private TextView childCategoryTextView;
    private AppCompatCheckBox childCategoryCheckBox;
    private int parentCategoryPosition;
    private OnFilterCategoryClickListener onFilterCategoryClickListener;

    public ChildCategoryViewHolder(View itemView,OnFilterCategoryClickListener onFilterCategoryClickListener,int parentCategoryPosition) {
        super(itemView);

        this.itemView = itemView;
        childCategoryTextView = itemView.findViewById(R.id.fragment_filter_child_category);
        childCategoryCheckBox = itemView.findViewById(R.id.fragment_filter_child_category_checkbox);
        this.onFilterCategoryClickListener = onFilterCategoryClickListener;
        this.parentCategoryPosition = parentCategoryPosition;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onFilterCategoryClickListener.onChildCategoryClicked(itemView,parentCategoryPosition,getAdapterPosition());
    }

    public AppCompatCheckBox getChildCategoryCheckBox() {
        return childCategoryCheckBox;
    }

    public TextView getChildCategoryTextView() {
        return childCategoryTextView;
    }
}
