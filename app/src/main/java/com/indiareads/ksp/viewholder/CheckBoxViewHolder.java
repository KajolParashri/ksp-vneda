package com.indiareads.ksp.viewholder;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnFilterCategoryClickListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class CheckBoxViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View itemView;
    private TextView textView;
    private AppCompatCheckBox checkBox;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public CheckBoxViewHolder(View itemView,OnRecyclerItemClickListener onRecyclerItemClickListener) {
        super(itemView);
        this.itemView = itemView;
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;

        textView = itemView.findViewById(R.id.filter_viewholder_textview);
        checkBox = itemView.findViewById(R.id.filter_viewholder_checkbox);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onRecyclerItemClickListener.onClick(itemView,getAdapterPosition());
    }

    public TextView getTextView() {
        return textView;
    }

    public AppCompatCheckBox getCheckBox() {
        return checkBox;
    }
}
