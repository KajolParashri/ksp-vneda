package com.indiareads.ksp.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;

public class CategoryViewHolder extends RecyclerView.ViewHolder {

    public TextView textTile;
    public ImageView imgTile;
    CardView card;


    public CategoryViewHolder(View itemView) {
        super(itemView);
        card = itemView.findViewById(R.id.card);
        imgTile = itemView.findViewById(R.id.imgTile);
        textTile = itemView.findViewById(R.id.textTile);
    }


    public TextView getTextTile() {
        return textTile;
    }

    public void setTextTile(TextView textTile) {
        this.textTile = textTile;
    }

    public ImageView getImgTile() {
        return imgTile;
    }

    public void setImgTile(ImageView imgTile) {
        this.imgTile = imgTile;
    }

    public CardView getCard() {
        return card;
    }

    public void setCard(CardView card) {
        this.card = card;
    }
}
