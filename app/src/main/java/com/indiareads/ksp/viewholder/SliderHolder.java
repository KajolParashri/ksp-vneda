package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class SliderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ImageView imageView;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public SliderHolder(View itemView, OnRecyclerItemClickListener listener) {
        super(itemView);

        this.onRecyclerItemClickListener = listener;
        imageView = itemView.findViewById(R.id.imageSlider);

        imageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        onRecyclerItemClickListener.onClick(view, getAdapterPosition());
    }

    public ImageView getImageView() {
        return imageView;
    }


}


