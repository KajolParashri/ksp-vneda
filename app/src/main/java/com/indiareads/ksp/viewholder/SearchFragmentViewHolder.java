package com.indiareads.ksp.viewholder;

import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class SearchFragmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    View view;
    private ImageView contentImage;
    private TextView contentTitle;
    private TextView contentSource;
    private ImageView contentTypeImage;
    private TextView contentTypeTitle;
    private TextView authorName;
    private OnRecyclerItemClickListener mOnClickListener;
    RatingBar ratingBar;

    RelativeLayout dummyLay;
    TextView titleDummy, sourceDummy;


    public TextView getAuthorName() {
        return authorName;
    }

    public SearchFragmentViewHolder(View itemView, OnRecyclerItemClickListener mOnClickListener) {
        super(itemView);
        this.mOnClickListener = mOnClickListener;

        view = itemView;
        titleDummy = itemView.findViewById(R.id.titleDummy);
        sourceDummy = itemView.findViewById(R.id.sourceDummy);
        ratingBar = itemView.findViewById(R.id.ratingBar);
        dummyLay = itemView.findViewById(R.id.dummyLay);

        contentImage = itemView.findViewById(R.id.mainImage);
        authorName = itemView.findViewById(R.id.textAuthor);
        contentTitle = itemView.findViewById(R.id.textContent);
        contentSource = itemView.findViewById(R.id.textSource);
        contentTypeImage = itemView.findViewById(R.id.img_Content);
        contentTypeTitle = itemView.findViewById(R.id.textType);
        this.mOnClickListener = mOnClickListener;
        view.setOnClickListener(this);
    }

    public RelativeLayout getDummyLay() {
        return dummyLay;
    }

    public TextView getTitleDummy() {
        return titleDummy;
    }

    public TextView getSourceDummy() {
        return sourceDummy;
    }

    @Override
    public void onClick(View v) {
        mOnClickListener.onClick(v, getAdapterPosition());
    }

    public View getView() {
        return view;
    }

    public ImageView getContentImage() {
        return contentImage;
    }

    public TextView getContentTitle() {
        return contentTitle;
    }

    public TextView getContentSource() {
        return contentSource;
    }

    public ImageView getContentTypeImage() {
        return contentTypeImage;
    }

    public TextView getContentTypeTitle() {
        return contentTypeTitle;
    }

    public RatingBar getRatingBar() {
        return ratingBar;
    }
}
