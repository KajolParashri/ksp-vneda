package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;

public class EmptyListViewHolder extends RecyclerView.ViewHolder{

    private ImageView imageView;
    private TextView textView;

    public EmptyListViewHolder(View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.empty_item_image);
        textView = itemView.findViewById(R.id.empty_item_text);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getTextView() {
        return textView;
    }
}
