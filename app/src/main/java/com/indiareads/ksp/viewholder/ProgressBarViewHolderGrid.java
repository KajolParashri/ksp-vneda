package com.indiareads.ksp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.indiareads.ksp.R;

public class ProgressBarViewHolderGrid extends RecyclerView.ViewHolder {

    public ShimmerFrameLayout shimmerFrameLayout;

    public ProgressBarViewHolderGrid(View itemView) {
        super(itemView);

        shimmerFrameLayout = itemView.findViewById(R.id.shimmer_view_container);
    }

    public ShimmerFrameLayout getShimmer() {
        return shimmerFrameLayout;
    }


}
