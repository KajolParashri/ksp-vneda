package com.indiareads.ksp.viewholder;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.indiareads.ksp.R;
import com.indiareads.ksp.glide.SvgDecoder;
import com.indiareads.ksp.glide.SvgDrawableTranscoder;
import com.indiareads.ksp.glide.SvgSoftwareLayerSetter;
import com.indiareads.ksp.listeners.OnCategoryImageClickListener;
import com.indiareads.ksp.model.Category;
import com.indiareads.ksp.utils.Urls;

import java.io.InputStream;

import static com.indiareads.ksp.utils.CommonMethods.loadContentImageWithGlide;

/**
 * Created by Kashish on 10-09-2018.
 */

public class ParentCategoryImagesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private static final String CATEGORY_IMAGE_URL = Urls.baseAWSUrl + "/ksp/icons/category/";

    private Context mContext;
    private ImageView mParentCategoryImage;
    private TextView mParentCategoryImageName;
    private OnCategoryImageClickListener mListener;
    private Category mCategory;
    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;

    public ParentCategoryImagesViewHolder(View itemView, Context context, OnCategoryImageClickListener listener) {
        super(itemView);
        this.mListener = listener;
        this.mContext = context;
        mParentCategoryImage = itemView.findViewById(R.id.parent_category_image);
        mParentCategoryImageName = itemView.findViewById(R.id.parent_category_image_name);
        itemView.setOnClickListener(this);
    }

    public void setCategoryImageData(Category category) {
        this.mCategory = category;
        setupSVGfiles(mContext, mParentCategoryImage, category.getCategoryImage());
        mParentCategoryImageName.setText(category.getCategoryName());
    }

    public void setupSVGfiles(Context mContext, ImageView image, String url) {

        requestBuilder = Glide.with(mContext)
                .using(Glide.buildStreamModelLoader(Uri.class, mContext), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.drawable.bookiconsmall)
                .error(R.drawable.bookiconsmall)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<Uri>());

        requestBuilder.diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .load(Uri.parse(url))
                .into(image);
    }

    @Override
    public void onClick(View view) {
        mListener.onCategoryImageClickListener(mCategory);
    }
}
