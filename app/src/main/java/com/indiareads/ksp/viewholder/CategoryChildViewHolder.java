package com.indiareads.ksp.viewholder;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnCategoryClickListener;
import com.indiareads.ksp.model.Category;

import java.util.List;

/**
 * Created by Kashish on 24-08-2018.
 */

public class CategoryChildViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private List<Category> mCategories;
    private OnCategoryClickListener mListener;
    private TextView categoryName;


    public CategoryChildViewHolder(View itemView, List<Category> mList, OnCategoryClickListener listener) {
        super(itemView);
        categoryName = itemView.findViewById(R.id.category_name);

        this.mListener = listener;
        this.mCategories = mList;

        itemView.setOnClickListener(this);
    }

    public void setCategoryName(String name) {
        categoryName.setText(name);
    }

    public String getCategoryName() {
        return categoryName.getText().toString();
    }

    @Override
    public void onClick(View view) {
        mListener.onCategoryChildClickListener(mCategories.get(getAdapterPosition()));
    }

}
