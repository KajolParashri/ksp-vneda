package com.indiareads.ksp.viewholder;

import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class MemberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private View view;
    private TextView userName,userDesignation;
    private ImageView profilePic;

    private OnRecyclerItemClickListener mOnClickListener;

    public MemberViewHolder(View itemView, OnRecyclerItemClickListener onClickListener) {
        super(itemView);

        view = itemView;
        userName = itemView.findViewById(R.id.user_item_fullname);
        userDesignation = itemView.findViewById(R.id.user_item_designation);
        profilePic = itemView.findViewById(R.id.user_item_profile_pic);
        mOnClickListener = onClickListener;

        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mOnClickListener.onClick(v,getAdapterPosition());
    }

    public void setView(View view) {
        this.view = view;
    }

    public TextView getUserName() {
        return userName;
    }

    public void setUserName(TextView userName) {
        this.userName = userName;
    }

    public TextView getUserDesignation() {
        return userDesignation;
    }

    public void setUserDesignation(TextView userDesignation) {
        this.userDesignation = userDesignation;
    }

    public ImageView getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(ImageView profilePic) {
        this.profilePic = profilePic;
    }
}