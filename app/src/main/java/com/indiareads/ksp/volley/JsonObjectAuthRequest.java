package com.indiareads.ksp.volley;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SharedPreferencesHelper;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JsonObjectAuthRequest extends JsonObjectRequest {

    private Context mContext;


    public JsonObjectAuthRequest(Context context, int method, String url, JSONObject jsonRequest,
                                 Response.Listener<JSONObject> listener,
                                 Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);

        this.mContext = context;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("origin-source", "app");
        headerMap.put("Authorization", "Bearer " + SharedPreferencesHelper.getBearerToken(mContext));
        Logger.info("HEADERS", headerMap.toString());
        return headerMap;
    }
}