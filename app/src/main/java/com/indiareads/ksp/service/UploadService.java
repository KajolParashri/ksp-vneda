package com.indiareads.ksp.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.UploadParams;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.ChatActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.indiareads.ksp.utils.Constants.AWS.BUCKET_NAME;
import static com.indiareads.ksp.utils.SharedPreferencesHelper.getCurrentUser;

public class UploadService extends Service {

    public static final String STATUS = "Status";
    public static final String STATUS_SUCCESS = "Status Success";
    public static final String STATUS_FAILED = "Status Failed";
    public static final String STATUS_IN_PROGRESS = "Status In Progress";
    public static final String UPLOAD_PARAMS = "Upload Params";
    private static final int UPLOAD_NOTIFICATION_ID = 100;
    private static final String NOTIFICATION_CHANNEL_ID = "123";
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;

    private static String TAG = UploadService.class.getSimpleName();

    public UploadService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            UploadParams uploadParams = intent.getParcelableExtra(UPLOAD_PARAMS);
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            startForeground(UPLOAD_NOTIFICATION_ID, getUploadNotification(0));
            uploadSingleFile(uploadParams);
            Logger.info(TAG, "UPLOAD PARAMS: " + uploadParams.toString());
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private Notification getUploadNotification(int progress) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }

        if (builder == null)
            builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

        builder.setContentTitle("Uploading Attachment")
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.ic_notification)
                .setPriority(NotificationCompat.PRIORITY_LOW);

        if (progress == 0) {
            builder.setContentText(String.valueOf(progress) + "%").setProgress(0, 0, true);
        } else if (progress == 100) {
            builder.setContentText("Upload Complete").setProgress(0, 0, false);
        } else {
            builder.setContentText(String.valueOf(progress) + "%").setProgress(100, progress, false);
        }
        return builder.build();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_DEFAULT);
        adminChannel.setDescription(adminChannelDescription);

        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    private void uploadSingleFile(final UploadParams uploadParams) {

        File file = new File(Constants.File.ROOT_DIR_PATH + "/" + uploadParams.getFileName());
        String fileExt = uploadParams.getFileName().substring(uploadParams.getFileName().lastIndexOf(".") + 1).toLowerCase();

        final Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");
        String formattedDate = df.format(c.getTime());
        final String uploadFileName = getCurrentUser(this).getUserId() + "_" + formattedDate + "." + fileExt;

        uploadParams.setPath(uploadParams.getPath() + uploadFileName);

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "ap-south-1:345799b2-3506-4245-a702-3a7e9d4c0071", // Identity pool ID
                Regions.AP_SOUTH_1 // Region
        );
        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider, Region.getRegion(Regions.AP_SOUTH_1));
        TransferNetworkLossHandler.getInstance(getApplicationContext());
        TransferUtility transferUtility =
                TransferUtility.builder()
                        .defaultBucket(BUCKET_NAME)
                        .context(this.getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

        final TransferObserver uploadObserver =
                transferUtility.upload(uploadParams.getKey() + uploadParams.getPath(), file, CannedAccessControlList.PublicRead);

        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    Logger.info(TAG, "Uploading completed: " + uploadParams.getKey() + uploadParams.getPath());
                    notificationManager.notify(UPLOAD_NOTIFICATION_ID, getUploadNotification(100));
                    stopForeground(false);
                    if (uploadParams.getAction().equals(Constants.Network.ACTION_UPDATE_ATTACHMENT))
                        sendMessageUpdateRequest(uploadParams);
                    else if (uploadParams.getAction().equals(Constants.Network.ACTION_UPLOADED_FILE_DATABASE_ENTRY))
                        sendRepositoryUploadRequest(uploadParams);
                } else if (TransferState.CANCELED == state || TransferState.FAILED == state) {
                    stopForeground(false);
                    sendBroadcast(uploadParams, STATUS_FAILED);
                    endService();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDone = (int) (((float) bytesCurrent / (float) bytesTotal) * 100);
                Logger.info(TAG, String.valueOf(percentDone) + "% uploading complete");
                notificationManager.notify(UPLOAD_NOTIFICATION_ID, getUploadNotification((int) percentDone));
                sendBroadcast(uploadParams, STATUS_IN_PROGRESS);
            }

            @Override
            public void onError(int id, Exception ex) {
                Logger.info(TAG, ex.getMessage() + " is the error while uploading");
                sendBroadcast(uploadParams, STATUS_FAILED);
            }
        });
    }

    private void sendRepositoryUploadRequest(final UploadParams uploadParams) {
        try {
            final JSONObject jsonObject = uploadParams.getJsonObject();
            jsonObject.put(Constants.Network.PATH, uploadParams.getPath());
            Logger.info("RepoUpload", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());
                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    sendBroadcast(uploadParams, STATUS_SUCCESS);
                                } else {
                                    Error.generateError(NullPointerException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                            "sendRepositoryUploadRequest", Error.ERROR_TYPE_SERVER_ERROR, "Message not sent");
                                    sendBroadcast(uploadParams, STATUS_FAILED);
                                }
                                endService();
                            } catch (JSONException e) {
                                Error.generateError(JSONException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                        "sendRepositoryUploadRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                endService();
                                sendBroadcast(uploadParams, STATUS_FAILED);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Error.generateError(VolleyError.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                    "sendRepositoryUploadRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                            endService();
                            sendBroadcast(uploadParams, STATUS_FAILED);
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                    "sendRepositoryUploadRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            endService();
            sendBroadcast(uploadParams, STATUS_FAILED);
        }
    }

    private void sendMessageUpdateRequest(final UploadParams uploadParams) {
        try {
            final JSONObject jsonObject = uploadParams.getJsonObject();
            jsonObject.put(Constants.Network.ATTACHMENT_ID, uploadParams.getPath());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Logger.info(TAG, response.toString());

                    try {
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                            sendBroadcast(uploadParams, STATUS_SUCCESS);
                        } else {
                            Error.generateError(NullPointerException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                    "sendMessageUpdateRequest", Error.ERROR_TYPE_SERVER_ERROR, "Message not sent");
                            sendBroadcast(uploadParams, STATUS_FAILED);
                        }
                        endService();
                    } catch (JSONException e) {
                        Error.generateError(JSONException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                "sendMessageUpdateRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                        endService();
                        sendBroadcast(uploadParams, STATUS_FAILED);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    Error.generateError(VolleyError.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                            "sendMessageUpdateRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                    endService();
                    sendBroadcast(uploadParams, STATUS_FAILED);
                }
            });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                    "sendMessageUpdateRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            endService();
            sendBroadcast(uploadParams, STATUS_FAILED);
        }
    }

    private void endService() {
        stopSelf();
    }

    private void sendBroadcast(UploadParams uploadParams, String status) {
        Intent intent = new Intent(uploadParams.getAction());
        intent.putExtra(UPLOAD_PARAMS, uploadParams);
        intent.putExtra(STATUS, status);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.info(TAG, "onDestroy");
    }
}