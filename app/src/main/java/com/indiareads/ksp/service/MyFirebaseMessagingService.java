package com.indiareads.ksp.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SharedPreferencesHelper;
import com.indiareads.ksp.view.activity.ChatActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private static final String ADMIN_CHANNEL_ID = "123";
    private NotificationManager notificationManager;

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        SharedPreferencesHelper.saveToken(getApplicationContext(), token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {
            Logger.info(TAG, "Message data payload: " + remoteMessage.getData());
        }

        if (remoteMessage.getNotification() != null) {
            Logger.info(TAG, "Message Notification Body: " + remoteMessage.getNotification().getTitle());
        }

        if (User.getCurrentUser(this) == null)
            return;

        if (!(remoteMessage.getNotification().getClickAction().equals("ACTION_VIEW_CHAT") && ChatActivity.currentConversationId.equals(remoteMessage.getData().get("convo_id"))))
            displayNotification(remoteMessage);
    }

    private void displayNotification(RemoteMessage remoteMessage) {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }

        Intent notificationIntent = new Intent(remoteMessage.getNotification().getClickAction());
        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
            notificationIntent.putExtra(entry.getKey(), entry.getValue());
        }
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(notificationIntent);
        final PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        String title = remoteMessage.getNotification().getTitle();
        String body = remoteMessage.getNotification().getBody();
        try {
            title = title.replace("'", "''");
            body = body.replace("'", "''");
        } catch (Exception e) {
            Logger.error(TAG, e.getMessage());
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setContentText(body)
                .setContentIntent(pendingIntent)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));

        if (remoteMessage.getData().get("image") != null) {
            Bitmap bitmap = getBitmapfromUrl(remoteMessage.getData().get("image"));
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                    .setSummaryText(remoteMessage.getNotification().getBody())
                    .bigPicture(bitmap));
        }

        int id;
        if (remoteMessage.getNotification().getTag() != null)
            id = Integer.parseInt(remoteMessage.getNotification().getTag());
        else
            id = (int) (System.currentTimeMillis());
        notificationManager.notify(id, notificationBuilder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);

        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            Logger.error(TAG, e.getMessage());
            return null;
        }
    }
}