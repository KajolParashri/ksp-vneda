package com.indiareads.ksp.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.Params;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.KeyWordsEntittyActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Objects;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.indiareads.ksp.utils.Constants.AWS.BUCKET_NAME;
import static com.indiareads.ksp.utils.Constants.Article.ACTION;
import static com.indiareads.ksp.utils.Constants.Article.CATEGORY;
import static com.indiareads.ksp.utils.Constants.Article.DRAFT_STATUS;
import static com.indiareads.ksp.utils.Constants.Article.EXISTING_ARRAY;
import static com.indiareads.ksp.utils.Constants.Article.FEAT_IMAGE;
import static com.indiareads.ksp.utils.Constants.Article.NEW_ARRAY;
import static com.indiareads.ksp.utils.Constants.Article.TITLE;
import static com.indiareads.ksp.utils.Constants.Article.USER_ID;
import static com.indiareads.ksp.utils.Constants.Content.EBOOK;
import static com.indiareads.ksp.utils.Constants.Content.INFOGRAPHIC;
import static com.indiareads.ksp.utils.Constants.Content.PODCAST;
import static com.indiareads.ksp.utils.Constants.Content.VIDEO;
import static com.indiareads.ksp.utils.Constants.EBOOK.EBOOK_URL;
import static com.indiareads.ksp.utils.Constants.EBOOK.ISBN;
import static com.indiareads.ksp.utils.Constants.EBOOK.UPLOAD_EBOOK;
import static com.indiareads.ksp.utils.Constants.Infographic.DESCRIPTION;
import static com.indiareads.ksp.utils.Constants.Infographic.IMAGE_URL;
import static com.indiareads.ksp.utils.Constants.Infographic.PRIVACY_STATUS;
import static com.indiareads.ksp.utils.Constants.Infographic.UPLOAD_INFOGRAPHIC;
import static com.indiareads.ksp.utils.Constants.Network.RESPONSE_CODE;
import static com.indiareads.ksp.utils.Constants.Network.RESPONSE_OK;
import static com.indiareads.ksp.utils.Constants.Podcast.AUDIO_URL;
import static com.indiareads.ksp.utils.Constants.Podcast.TRANSCRIPT;
import static com.indiareads.ksp.utils.Constants.Podcast.UPLOAD_PODCAST;
import static com.indiareads.ksp.utils.Constants.Video.UPLOAD_VIDEO;
import static com.indiareads.ksp.utils.Constants.Video.VIDEO_URL;
import static com.indiareads.ksp.utils.SharedPreferencesHelper.getCurrentUser;
import static com.indiareads.ksp.utils.Urls.baseAWSUrl;
import static com.indiareads.ksp.view.activity.CreateContentActivity.draftValue;
import static com.indiareads.ksp.view.activity.CreateContentActivity.selectedCategory;


public class PublishMediaService extends IntentService {
    private static final String TAG = "PublishMediaService";
    NotificationManagerCompat notificationManagerCompat;
    NotificationCompat.Builder notificationBuilder;
    public static int CODENOTI = 111;
    PendingIntent notifyPendingIntent;
    String content_type = "";

    public PublishMediaService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        super.onStart(intent, startId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void showNotification() {
        notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
        String contentTitle = "Media Uploading...";

        Intent notifyIntent = new Intent();
        notifyPendingIntent = PendingIntent.getActivity(getApplicationContext(), 007, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder = createNotificationBuilder("007");
        notificationBuilder.setContentIntent(notifyPendingIntent);
        notificationBuilder.setTicker("Upload Initialising...");
        notificationBuilder.setOngoing(true);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSmallIcon(android.R.drawable.stat_sys_download);
        notificationBuilder.setContentTitle(contentTitle);
        notificationBuilder.setContentText("0%");
        notificationBuilder.setProgress(100, 0, false);
        notificationManagerCompat.notify(CODENOTI, notificationBuilder.build());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        Params params = intent.getParcelableExtra("data");
        Logger.info(TAG, "PARAMS:" + params.toString());

        if (params.getMediaUri() != null && !params.getMediaUri().equals("")) {
            showNotification();
        }

        switch (params.getMediaType()) {
            case VIDEO:
                if (!params.getFeatImageName().equals(""))
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                if (params.getMediaUri() != null && !params.getMediaUri().equals("")) {
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                    if (params.getFeatUri() != null && !params.getFeatUri().equals("")) {
                        uploadFile(params, this, Uri.parse(params.getFeatUri()), params.getFeatImageName(), FEAT_IMAGE);
                    }
                    uploadFile(params, this, Uri.parse(params.getMediaUri()), params.getMediaName(), params.getMediaType());
                } else {
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                    publishVideo(params);
                }
                break;
            case PODCAST:
                if (!params.getFeatImageName().equals(""))
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                if (params.getMediaUri() != null && !params.getMediaUri().equals("")) {
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                    if (params.getFeatUri() != null && !params.getFeatUri().equals("")) {
                        uploadFile(params, this, Uri.parse(params.getFeatUri()), params.getFeatImageName(), FEAT_IMAGE);
                    }
                    uploadFile(params, this, Uri.parse(params.getMediaUri()), params.getMediaName(), params.getMediaType());
                } else {
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                    publishPodcast(params);
                }
                break;
            case INFOGRAPHIC:
                if (!params.getFeatImageName().equals(""))
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                if (params.getMediaUri() != null && !params.getMediaUri().equals("")) {
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                    if (params.getFeatUri() != null && !params.getFeatUri().equals("")) {
                        uploadFile(params, this, Uri.parse(params.getFeatUri()), params.getFeatImageName(), FEAT_IMAGE);
                    }
                    uploadFile(params, this, Uri.parse(params.getMediaUri()), params.getMediaName(), params.getMediaType());
                } else {
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                    publishInfographic(params);
                }
                break;
            case EBOOK:
                if (!params.getFeatImageName().equals(""))
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                if (params.getMediaUri() != null && !params.getMediaUri().equals("")) {
                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                    if (params.getFeatUri() != null && !params.getFeatUri().equals("")) {
                        uploadFile(params, this, Uri.parse(params.getFeatUri()), params.getFeatImageName(), FEAT_IMAGE);
                    }
                    uploadFile(params, this, Uri.parse(params.getMediaUri()), params.getMediaName(), params.getMediaType());
                } else {

                    content_type = String.valueOf(CommonMethods.getContentTypeFromName(params.getMediaType()));
                    publishBook(params);
                }
                break;
        }
    }

    public void uploadFile(final Params params, Context context, Uri uri, String name, final String fileType) {

        final File file = new File(Objects.requireNonNull(uri.getPath()));

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "ap-south-1:345799b2-3506-4245-a702-3a7e9d4c0071", // Identity pool ID
                Regions.AP_SOUTH_1 // Region
        );
        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider, Region.getRegion(Regions.AP_SOUTH_1));
        TransferNetworkLossHandler.getInstance(context);
        TransferUtility transferUtility =
                TransferUtility.builder()
                        .defaultBucket(BUCKET_NAME)
                        .context(context)
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

        TransferObserver uploadObserver =
                transferUtility.upload(Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(PublishMediaService.this).getUserId() + "/" + name, file, CannedAccessControlList.PublicRead);

        final String finalName = name;
        Logger.info(TAG, "Uploading to !!" + "--" + baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(PublishMediaService.this).getUserId() + "/" + finalName);

        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    finished("Upload", "Successful");

                    switch (fileType) {
                        case VIDEO:
                            publishVideo(params);
                            break;
                        case PODCAST:
                            publishPodcast(params);
                            break;
                        case INFOGRAPHIC:
                            publishInfographic(params);
                            break;
                        case EBOOK:
                            publishBook(params);
                            break;
                    }
                    Logger.info(TAG, "Uploading completed !!" + "--" + baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(PublishMediaService.this).getUserId() + "/" + finalName);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;
                if (notificationBuilder != null && notificationManagerCompat != null) {
                    notificationBuilder.setContentText(percentDone + "%");
                    notificationBuilder.setProgress(100, percentDone, false);
                    notificationManagerCompat.notify(CODENOTI, notificationBuilder.build());
                }
                Logger.info(TAG, String.valueOf(percentDone) + "% uploading complete");
            }

            @Override
            public void onError(int id, Exception ex) {
                Logger.error(TAG, ex.getMessage() + " is the error while uploading");
                Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                if (notificationManagerCompat != null)
                    notificationManagerCompat.cancel(CODENOTI);
            }
        });
    }

    public void finished(String title, String statusText) {
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setSmallIcon(R.drawable.noti_vneda);
        notificationBuilder.setOngoing(false);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setContentText(statusText);
        notificationBuilder.setProgress(0, 0, false);
        notificationManagerCompat.notify(CODENOTI, notificationBuilder.build());
    }

    private NotificationCompat.Builder createNotificationBuilder(String channelId) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            String channelName = getString(R.string.app_name);

            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
        return new NotificationCompat.Builder(this, channelId);
    }

    private void publishVideo(final Params params) {
        try {

            JSONArray existingArray = new JSONArray();
            existingArray.put("35762");

            JSONArray newArray = new JSONArray();
            newArray.put("dfs");
            newArray.put("gdhg");
            newArray.put("poio");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, UPLOAD_VIDEO);
            jsonObject.put(CATEGORY, selectedCategory);
            jsonObject.put(VIDEO_URL, params.getMediaURL());
            jsonObject.put(FEAT_IMAGE, params.getMediaFeatImage());
            jsonObject.put(TITLE, params.getMediaTitle());
            jsonObject.put(DRAFT_STATUS, draftValue);
            jsonObject.put(USER_ID, params.getUserId());
            jsonObject.put(EXISTING_ARRAY, existingArray);
            jsonObject.put(NEW_ARRAY, newArray);
            jsonObject.put(DESCRIPTION, params.getMediaDescription());
            jsonObject.put(PRIVACY_STATUS, params.getPrivacyStatus());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {

                            Logger.info(TAG, "Video published successfully");
                            if (draftValue == 0) {
                                String content_id = response.getString(Constants.Network.DATA);

                                Intent intent = new Intent(getApplicationContext(), KeyWordsEntittyActivity.class);
                                intent.putExtra("content_type", content_type);
                                intent.putExtra("content_id", content_id);
                                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                Toast.makeText(PublishMediaService.this, R.string.article_draft_success, Toast.LENGTH_SHORT).show();
                            }

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            //     SideBarPanel.logout(this);
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                                    "createVideo", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                                "createVideo", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                            "createVideo", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });
            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                    "createVideo", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    private void publishPodcast(final Params params) {
        try {
            JSONArray existingArray = new JSONArray();
            existingArray.put("35762");

            JSONArray newArray = new JSONArray();
            newArray.put("dfs");
            newArray.put("gdhg");
            newArray.put("poio");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, UPLOAD_PODCAST);
            jsonObject.put(CATEGORY, selectedCategory);

            jsonObject.put(AUDIO_URL, params.getMediaURL());
            jsonObject.put(TRANSCRIPT, params.getMediaTranscript());
            jsonObject.put(FEAT_IMAGE, params.getMediaFeatImage());
            jsonObject.put(TITLE, params.getMediaTitle());
            jsonObject.put(USER_ID, getCurrentUser(this).getUserId());
            jsonObject.put(EXISTING_ARRAY, existingArray);
            jsonObject.put(NEW_ARRAY, newArray);
            jsonObject.put(DRAFT_STATUS, draftValue);
            jsonObject.put(DESCRIPTION, params.getMediaDescription());
            jsonObject.put(PRIVACY_STATUS, params.getPrivacyStatus());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {

                            Logger.info(TAG, "Podcast published successfully");
                            if (draftValue == 0) {
                                String content_id = response.getString(Constants.Network.DATA);

                                Intent intent = new Intent(getApplicationContext(), KeyWordsEntittyActivity.class);
                                intent.putExtra("content_type", content_type);
                                intent.putExtra("content_id", content_id);
                                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                Logger.info(TAG, "IntentCodeExecuted");
                            } else {
                                Toast.makeText(PublishMediaService.this, R.string.article_draft_success, Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                                    "createPodcast", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                        }

                    } catch (JSONException e) {

                        Error error = Error.generateError(JSONException.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                                "createPodcast", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                            "createPodcast", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }

            });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                    "createPodcast", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    private void publishInfographic(final Params params) {

        Logger.info(TAG, "PARAMS:" + params.toString());
        try {
            JSONArray existingArray = new JSONArray();
            existingArray.put("35762");
            JSONArray newArray = new JSONArray();
            newArray.put("dfs");
            newArray.put("gdhg");
            newArray.put("poio");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, UPLOAD_INFOGRAPHIC);
            jsonObject.put(CATEGORY, selectedCategory);

            jsonObject.put(IMAGE_URL, params.getMediaURL());
            jsonObject.put(FEAT_IMAGE, params.getMediaFeatImage());
            jsonObject.put(TITLE, params.getMediaTitle());

            jsonObject.put(USER_ID, getCurrentUser(this).getUserId());
            jsonObject.put(EXISTING_ARRAY, existingArray);
            jsonObject.put(NEW_ARRAY, newArray);

            jsonObject.put(DRAFT_STATUS, draftValue);
            jsonObject.put(DESCRIPTION, params.getMediaDescription());
            jsonObject.put(PRIVACY_STATUS, params.getPrivacyStatus());

            Logger.info(TAG, jsonObject.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.info(TAG, response.toString());
                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {
                            Logger.info(TAG, "Infographic successfully uploaded");
                            if (draftValue == 0) {
                                String content_id = response.getString(Constants.Network.DATA);
                                Intent intent = new Intent(getBaseContext(), KeyWordsEntittyActivity.class);
                                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("content_type", content_type);
                                intent.putExtra("content_id", content_id);
                                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                Toast.makeText(PublishMediaService.this, R.string.article_draft_success, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                                    "createInfographic", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                        }

                    } catch (JSONException e) {

                        Error error = Error.generateError(JSONException.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                                "createInfographic", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    Error error = Error.generateError(VolleyError.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                            "createInfographic", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {

            Error error = Error.generateError(JSONException.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                    "createInfographic", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    private void publishBook(final Params params) {
        try {
            JSONArray existingArray = new JSONArray();
            existingArray.put("35762");

            JSONArray newArray = new JSONArray();
            newArray.put("dfs");
            newArray.put("gdhg");
            newArray.put("poio");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, UPLOAD_EBOOK);
            jsonObject.put(CATEGORY, selectedCategory);

            jsonObject.put(EBOOK_URL, params.getMediaURL());
            jsonObject.put(ISBN, params.getMediaISBNno());
            jsonObject.put(FEAT_IMAGE, params.getMediaFeatImage());
            jsonObject.put(TITLE, params.getMediaTitle());
            jsonObject.put(USER_ID, getCurrentUser(this).getUserId());
            jsonObject.put(EXISTING_ARRAY, existingArray);
            jsonObject.put(NEW_ARRAY, newArray);
            jsonObject.put(DRAFT_STATUS, draftValue);
            jsonObject.put(DESCRIPTION, params.getMediaDescription());
            jsonObject.put(PRIVACY_STATUS, params.getPrivacyStatus());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.info(TAG, response.toString());
                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {
                            Logger.info(TAG, "Book uploaded successfully");
                            if (draftValue == 0) {
                                String content_id = response.getString(Constants.Network.DATA);
                                Intent intent = new Intent(getApplicationContext(), KeyWordsEntittyActivity.class);
                                intent.putExtra("content_type", content_type);
                                intent.putExtra("content_id", content_id);
                                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                Toast.makeText(PublishMediaService.this, R.string.article_draft_success, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                                    "createEBook", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                                "createEBook", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                            "createEBook", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), PublishMediaService.class.getSimpleName(),
                    "createEBook", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }
}
