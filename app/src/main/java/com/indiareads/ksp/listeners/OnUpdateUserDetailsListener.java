package com.indiareads.ksp.listeners;

import com.indiareads.ksp.model.User;

public interface OnUpdateUserDetailsListener {

    public void OnUpdateUserDetails(User user);

}
