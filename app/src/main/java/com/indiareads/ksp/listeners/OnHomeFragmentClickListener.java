package com.indiareads.ksp.listeners;

import android.view.View;

public interface OnHomeFragmentClickListener {

    public void onLikeClicked(View view, int position);

    public void onLikeCountClicked(View view, int position);

    public void onCommentClicked(View view, int position);

    public void onProfileClicked(View view, int position);

    public void onViewClicked(View view, int position);

    public void onDeleteClicked(View view, int position);
}
