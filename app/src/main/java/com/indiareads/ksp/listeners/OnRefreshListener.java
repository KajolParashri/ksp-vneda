package com.indiareads.ksp.listeners;

public interface OnRefreshListener {

    public void onRefresh();

}
