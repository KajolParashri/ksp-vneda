package com.indiareads.ksp.listeners;

import com.indiareads.ksp.model.Error;

import java.io.File;

public interface OnFileCopiedListener {
    public void onFileCopied(File file);
    public void onFileCopyFailed(Error error);
}
