package com.indiareads.ksp.listeners;

import com.indiareads.ksp.model.Category;

/**
 * Created by Kashish on 10-09-2018.
 */

public interface OnCategoryImageClickListener {
    void onCategoryImageClickListener(Category category);
}
