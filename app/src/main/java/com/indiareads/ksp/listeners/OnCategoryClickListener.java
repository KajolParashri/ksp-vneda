package com.indiareads.ksp.listeners;

import com.indiareads.ksp.model.Category;

/**
 * Created by Kashish on 23-08-2018.
 */

public interface OnCategoryClickListener {
    void onCategoryClickListener(Category category);
    void onCategoryChildClickListener(Category category);
}
