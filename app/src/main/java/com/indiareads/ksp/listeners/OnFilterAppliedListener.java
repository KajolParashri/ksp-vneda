package com.indiareads.ksp.listeners;

import android.os.Bundle;

public interface OnFilterAppliedListener {

    public void OnFilterApplied(Bundle arguments);
}
