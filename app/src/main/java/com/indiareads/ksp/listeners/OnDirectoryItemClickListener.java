package com.indiareads.ksp.listeners;

import android.view.View;

public interface OnDirectoryItemClickListener {
    public void onOptionsClicked(View view, int position);
    public void onItemClicked(View view, int position);
}
