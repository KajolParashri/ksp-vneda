package com.indiareads.ksp.listeners;

import android.graphics.Bitmap;

import java.io.File;

public interface onEBookLoad {
    void resultData(Bitmap data);

    void progressData(int progress);

    void failed(Throwable t);

    void complete();

    void onDownloadComplete(File file);
}
