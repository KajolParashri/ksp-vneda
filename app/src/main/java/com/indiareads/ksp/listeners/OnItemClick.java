package com.indiareads.ksp.listeners;

public interface OnItemClick {
    public void onArticleClick(String tileName);

    public void onInfoClick(String tileName);

    public void onCategoryClick(int pos);
}
