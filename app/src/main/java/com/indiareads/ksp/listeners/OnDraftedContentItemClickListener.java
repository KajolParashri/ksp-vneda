package com.indiareads.ksp.listeners;

import android.view.View;

public interface OnDraftedContentItemClickListener {

    public void onEditClick(View v, int position);

    public void onDeleteClick(View v, int position);

    public void onItemClick(View v, int position);

    public void onLikeCOunt(View v, int position);


}
