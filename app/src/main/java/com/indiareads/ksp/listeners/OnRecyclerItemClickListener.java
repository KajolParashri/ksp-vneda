package com.indiareads.ksp.listeners;

import android.view.View;

public interface OnRecyclerItemClickListener {
    public void onClick(View v,int position);
}
