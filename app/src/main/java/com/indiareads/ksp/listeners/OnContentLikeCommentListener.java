package com.indiareads.ksp.listeners;

import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.SuggestionModel;

public interface OnContentLikeCommentListener {

    public void onContentLiked(Content content, String likeStatus, String likeCount);
    public void onContentCommented(Content content,String commentCount,int pos);
    public void onContentCommented(SuggestionModel content, String commentCount,int pos);
}
