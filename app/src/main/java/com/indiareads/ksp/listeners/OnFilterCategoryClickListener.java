package com.indiareads.ksp.listeners;

import android.view.View;

import com.indiareads.ksp.model.Category;

public interface OnFilterCategoryClickListener {

    public void onParentCategoryClicked(View view, int position);
    public void onChildCategoryClicked(View view, int parentCategoryposition,int childCategoryposition);

}
