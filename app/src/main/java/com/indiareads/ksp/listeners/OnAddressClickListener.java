package com.indiareads.ksp.listeners;

import android.view.View;

import com.indiareads.ksp.model.Address;

public interface OnAddressClickListener {
    void OnAddressClick(int position);
    void OnAddressRemoveClick(int position);
}
