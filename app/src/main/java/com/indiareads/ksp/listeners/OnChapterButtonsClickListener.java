package com.indiareads.ksp.listeners;

import com.indiareads.ksp.model.Chapter;

/**
 * Created by Kashish on 12-10-2018.
 */

public interface OnChapterButtonsClickListener {
    void onEditBtnClick(Chapter chapter, int position);
    void onRenameBtnClick(Chapter chapter, int position);
    void onDeleteBtnClick(Chapter chapter, int position);
}
