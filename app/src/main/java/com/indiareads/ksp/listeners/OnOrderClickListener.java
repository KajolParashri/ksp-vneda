package com.indiareads.ksp.listeners;

import android.widget.ProgressBar;

public interface OnOrderClickListener {
    void OnReturnClick(int position);
    void OnCancelClick(int position);
}
