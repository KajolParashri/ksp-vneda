package com.indiareads.ksp.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.indiareads.ksp.utils.SharedPreferencesHelper;

public class User {

    public static final String USER_ID = "userId";
    public static final String USER_EMAIL = "userEmail";
    public static final String LOAD_TUTORIAL = "loadTutorials";
    public static final String COMPANY_ID = "companyId";
    public static final String COMPANY_NAME = "companyName";
    public static final String PROFILE_PIC = "profilePic";
    public static final String DESIGNATION = "designation";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String IS_ADMIN = "is_admin";
    public static final String COMPANY_FEATURE_ID = "company_feature_Id";
    public static final String COMPANY_FEATURE_COMPANY_ID = "company_feature_companyId";
    public static final String COMPANY_FEATURE_FEATURE_ID = "company_feature_featureId";
    public static final String COMPANY_FEATURE_VALUE = "company_feature_value";
    public static final String COMPANY_FEATURE_STATUS = "company_feature_status";
    public static final String FULLNAME = "fullname";
    public static final String USER_TYPE_NORMAL = "1";
    public static final String CONTENT_CREATED_COUNT = "content_created_count";
    public static final String CONTENT_CONSUMED_COUNT = "content_consumed_count";
    public static final String TOTAL_POINTS = "total_points";
    public static final String RANK = "rank";
    public static final String BEARER_TOKEN = "bearer_token";

    private String loadTutorials;
    private String userId;
    private String userEmail;
    private String addressType;
    private String addressBookId;
    private String fullName;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String pincode;
    private String phone;
    private String companyId;
    private String companyName;
    private String firstName;
    private String lastName;
    private String designation;
    private String profilePic;
    private String gender;
    private String birthDate;
    private String landline;
    private String mobile;
    private String location;
    private String rank;
    private String points;
    private String createdContents;
    private String consumedContents;
    private String isAdmin;
    private String companyFeaturesId;
    private String companyFeaturesCompanyId;
    private String companyFeaturesFeatureId;
    private String companyFeaturesValue;
    private String companyFeaturesStatus;
    private String userType;
    private String bearerToken;

    public User() {
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddressBookId() {
        return addressBookId;
    }

    public void setAddressBookId(String addressBookId) {
        this.addressBookId = addressBookId;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firseName) {
        this.firstName = firseName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getCreatedContents() {
        return createdContents;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setCreatedContents(String createdContents) {
        this.createdContents = createdContents;
    }

    public String getConsumedContents() {
        return consumedContents;
    }

    public void setConsumedContents(String consumedContents) {
        this.consumedContents = consumedContents;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getCompanyFeaturesId() {
        return companyFeaturesId;
    }

    public void setCompanyFeaturesId(String companyFeaturesId) {
        this.companyFeaturesId = companyFeaturesId;
    }

    public String getCompanyFeaturesCompanyId() {
        return companyFeaturesCompanyId;
    }

    public void setCompanyFeaturesCompanyId(String companyFeaturesCompanyId) {
        this.companyFeaturesCompanyId = companyFeaturesCompanyId;
    }

    public String getCompanyFeaturesFeatureId() {
        return companyFeaturesFeatureId;
    }

    public void setCompanyFeaturesFeatureId(String companyFeaturesFeatureId) {
        this.companyFeaturesFeatureId = companyFeaturesFeatureId;
    }

    public String getCompanyFeaturesValue() {
        return companyFeaturesValue;
    }

    public void setCompanyFeaturesValue(String companyFeaturesValue) {
        this.companyFeaturesValue = companyFeaturesValue;
    }

    public String getCompanyFeaturesStatus() {
        return companyFeaturesStatus;
    }

    public void setCompanyFeaturesStatus(String companyFeaturesStatus) {
        this.companyFeaturesStatus = companyFeaturesStatus;
    }


    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBearerToken() {
        return bearerToken;
    }

    public void setBearerToken(String bearerToken) {
        this.bearerToken = bearerToken;
    }

    public String getLoadTutorials() {
        return loadTutorials;
    }

    public void setLoadTutorials(String loadTutorials) {
        this.loadTutorials = loadTutorials;
    }

    public static void setAsCurrentUser(Context context, User user) {
        SharedPreferencesHelper.saveUser(context, user);
    }

    public static void removeCurrentUser(Context context) {
        SharedPreferencesHelper.removeUser(context);
    }



    public static User getCurrentUser(Context context) {
        return SharedPreferencesHelper.getCurrentUser(context);
    }
}
