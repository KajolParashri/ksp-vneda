package com.indiareads.ksp.model;

public class Company {

    public static final String COMPANY_INFO = "Company Info";

    private String companyName;
    private String companyInfo;
    private String companyLogo;
    private String totalUsers;
    private String totalContentCreated;
    private String totalContentConsumed;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyInfo() {
        return companyInfo;
    }

    public void setCompanyInfo(String companyInfo) {
        this.companyInfo = companyInfo;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(String totalUsers) {
        this.totalUsers = totalUsers;
    }

    public String getTotalContentCreated() {
        return totalContentCreated;
    }

    public void setTotalContentCreated(String totalContentCreated) {
        this.totalContentCreated = totalContentCreated;
    }

    public String getTotalContentConsumed() {
        return totalContentConsumed;
    }

    public void setTotalContentConsumed(String totalContentConsumed) {
        this.totalContentConsumed = totalContentConsumed;
    }
}
