package com.indiareads.ksp.model;

import android.content.Context;

import com.indiareads.ksp.utils.SharedPreferencesHelper;

public class CompanyFeature {

    private String featureId;
    private String value;

    public CompanyFeature(String featureId, String value) {
        this.featureId = featureId;
        this.value = value;
    }

    public static void reset(Context context) {
        SharedPreferencesHelper.resetCompanyFeatures(context);
    }

    public String getFeatureId() {
        return featureId;
    }

    public String getValue() {
        return value;
    }

    public static void saveFeature(Context context, CompanyFeature companyFeature) {
        SharedPreferencesHelper.saveFeature(context, companyFeature);
    }

}
