package com.indiareads.ksp.model;

import java.util.List;

/**
 * Created by Kashish on 18-08-2018.
 */

public class Category {

    private String categoryId;
    private String categoryName;
    private String categoryImage;
    private int categoryType;
    private List<ChildCategory> childCategories;

    public Category(String categoryId, String categoryName, int categoryType) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryType = categoryType;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public Category(){}

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(int categoryType) {
        this.categoryType = categoryType;
    }

    public List<ChildCategory> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(List<ChildCategory> childCategories) {
        this.childCategories = childCategories;
    }
}
