package com.indiareads.ksp.model;

public class DirectoryItem {

    public static final String TYPE_FOLDER = "1";
    public static final String TYPE_FILE = "2";
    public static final String TYPE_FILE_UPLOADING = "3";
    private String objectType;
    private String id;
    private String fid;
    private String name;
    private String description;
    private String path;
    private String repoId;
    private String fileType;
    private String userId;
    private String createdDate;
    private String icon;

    public DirectoryItem() {
    }

    public String getPath() {
        return path;
    }

    public String getIcon() {
        return icon;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRepoId() {
        return repoId;
    }

    public void setRepoId(String repoId) {
        this.repoId = repoId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
