package com.indiareads.ksp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Order implements Parcelable {
    protected Order(Parcel in) {
        content_id = in.readString();
        content_type = in.readString();
        title = in.readString();
        description = in.readString();
        thumnail = in.readString();
        source = in.readString();
        cat_id = in.readString();
        status = in.readString();
        order_id = in.readString();
        user_id = in.readString();
        library_id = in.readString();
        order_status = in.readString();
        inventory_id = in.readString();
        tracking_id = in.readString();
        tracking_link = in.readString();
        courier_vendor = in.readString();
        order_date = in.readString();
        order_comments = in.readString();
        delivery_address_id = in.readString();
        pickup_address_id = in.readString();
        address_book_id = in.readString();
        address_type = in.readString();
        fullname = in.readString();
        address_line1 = in.readString();
        phone = in.readString();
        address_line2 = in.readString();
        city = in.readString();
        state = in.readString();
        pincode = in.readString();
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumnail() {
        return thumnail;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLibrary_id() {
        return library_id;
    }

    public void setLibrary_id(String library_id) {
        this.library_id = library_id;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getInventory_id() {
        return inventory_id;
    }

    public void setInventory_id(String inventory_id) {
        this.inventory_id = inventory_id;
    }

    public String getTracking_id() {
        return tracking_id;
    }

    public void setTracking_id(String tracking_id) {
        this.tracking_id = tracking_id;
    }

    public String getTracking_link() {
        return tracking_link;
    }

    public void setTracking_link(String tracking_link) {
        this.tracking_link = tracking_link;
    }

    public String getCourier_vendor() {
        return courier_vendor;
    }

    public void setCourier_vendor(String courier_vendor) {
        this.courier_vendor = courier_vendor;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_comments() {
        return order_comments;
    }

    public void setOrder_comments(String order_comments) {
        this.order_comments = order_comments;
    }

    public String getDelivery_address_id() {
        return delivery_address_id;
    }

    public void setDelivery_address_id(String delivery_address_id) {
        this.delivery_address_id = delivery_address_id;
    }

    public String getPickup_address_id() {
        return pickup_address_id;
    }

    public void setPickup_address_id(String pickup_address_id) {
        this.pickup_address_id = pickup_address_id;
    }

    public String getAddress_book_id() {
        return address_book_id;
    }

    public void setAddress_book_id(String address_book_id) {
        this.address_book_id = address_book_id;
    }

    public String getAddress_type() {
        return address_type;
    }

    public void setAddress_type(String address_type) {
        this.address_type = address_type;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress_line1() {
        return address_line1;
    }

    public void setAddress_line1(String address_line1) {
        this.address_line1 = address_line1;
    }

    public String getAddress_line2() {
        return address_line2;
    }

    public void setAddress_line2(String address_line2) {
        this.address_line2 = address_line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    private String content_id = "";
    private String content_type = "";
    private String title = "";
    private String description = "";
    private String thumnail = "";
    private String source = "";
    private String cat_id = "";
    private String status = "";
    private String order_id = "";
    private String user_id = "";
    private String library_id = "";
    private String order_status = "";
    private String inventory_id = "";
    private String tracking_id = "";
    private String tracking_link = "";
    private String courier_vendor = "";
    private String order_date = "";
    private String order_comments = "";
    private String delivery_address_id = "";
    private String pickup_address_id = "";
    private String address_book_id = "";
    private String address_type = "";
    private String fullname = "";
    private String address_line1 = "";

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String phone = "";

    public Order(){}

    public Order(String content_id, String content_type,
                 String title, String description,
                 String thumnail, String source,
                 String cat_id, String status, String order_id,
                 String user_id, String library_id,
                 String order_status, String inventory_id,
                 String tracking_id, String tracking_link,
                 String courier_vendor, String order_date,
                 String order_comments, String delivery_address_id,
                 String pickup_address_id, String address_book_id,
                 String address_type, String fullname, String address_line1,
                 String address_line2, String city, String state,
                 String pincode, String phone) {
        this.phone = phone;
        this.content_id = content_id;
        this.content_type = content_type;
        this.title = title;
        this.description = description;
        this.thumnail = thumnail;
        this.source = source;
        this.cat_id = cat_id;
        this.status = status;
        this.order_id = order_id;
        this.user_id = user_id;
        this.library_id = library_id;
        this.order_status = order_status;
        this.inventory_id = inventory_id;
        this.tracking_id = tracking_id;
        this.tracking_link = tracking_link;
        this.courier_vendor = courier_vendor;
        this.order_date = order_date;
        this.order_comments = order_comments;
        this.delivery_address_id = delivery_address_id;
        this.pickup_address_id = pickup_address_id;
        this.address_book_id = address_book_id;
        this.address_type = address_type;
        this.fullname = fullname;
        this.address_line1 = address_line1;
        this.address_line2 = address_line2;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
    }

    private String address_line2 = "";
    private String city = "";
    private String state = "";
    private String pincode = "";

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(content_id);
        parcel.writeString(content_type);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(thumnail);
        parcel.writeString(source);
        parcel.writeString(cat_id);
        parcel.writeString(status);
        parcel.writeString(order_id);
        parcel.writeString(user_id);
        parcel.writeString(library_id);
        parcel.writeString(order_status);
        parcel.writeString(inventory_id);
        parcel.writeString(tracking_id);
        parcel.writeString(tracking_link);
        parcel.writeString(courier_vendor);
        parcel.writeString(order_date);
        parcel.writeString(order_comments);
        parcel.writeString(delivery_address_id);
        parcel.writeString(pickup_address_id);
        parcel.writeString(address_book_id);
        parcel.writeString(address_type);
        parcel.writeString(fullname);
        parcel.writeString(address_line1);
        parcel.writeString(phone);
        parcel.writeString(address_line2);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeString(pincode);
    }
}
