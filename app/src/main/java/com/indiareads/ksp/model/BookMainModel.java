package com.indiareads.ksp.model;

import java.util.ArrayList;

public class BookMainModel extends BaseParentModel {

    public String headingText;
    public ArrayList<ExploreModel> arrayList;

    public String setValue;

    public String getSetValue() {
        return setValue;
    }

    public void setSetValue(String setValue) {
        this.setValue = setValue;
    }

    public String getHeadingText() {
        return headingText;
    }

    public void setHeadingText(String headingText) {
        this.headingText = headingText;
    }

    public ArrayList<ExploreModel> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<ExploreModel> arrayList) {
        this.arrayList = arrayList;
    }
}
