package com.indiareads.ksp.model;

public class BannerModel extends BaseParentModel{
    private String imgUrl;
    private String intentStatus;
    private String intentType;
    private String intentParameter1;
    private String intentParameter2;
    private String intentParameter3;

    public void setIntentParameter3(String intentParameter3) {
        this.intentParameter3 = intentParameter3;
    }

    public String getIntentParameter3() {
        return intentParameter3;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getIntentStatus() {
        return intentStatus;
    }

    public void setIntentStatus(String intentStatus) {
        this.intentStatus = intentStatus;
    }

    public String getIntentType() {
        return intentType;
    }

    public void setIntentType(String intentType) {
        this.intentType = intentType;
    }

    public String getIntentParameter1() {
        return intentParameter1;
    }

    public void setIntentParameter1(String intentParameter1) {
        this.intentParameter1 = intentParameter1;
    }

    public String getIntentParameter2() {
        return intentParameter2;
    }

    public void setIntentParameter2(String intentParameter2) {
        this.intentParameter2 = intentParameter2;
    }
}
