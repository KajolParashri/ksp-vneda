package com.indiareads.ksp.model;

public class MenuModel {
    public String menuName;
    public int menuIcon;
    public String type;
    public int menuNumberIndex;

    public MenuModel(String menuName, int menuIcon, String type, int index) {
        this.menuIcon = menuIcon;
        this.menuName = menuName;
        this.type = type;
        this.menuNumberIndex = index;
    }

    public int getMenuNumberIndex() {
        return menuNumberIndex;
    }

    public void setMenuNumberIndex(int menuNumberIndex) {
        this.menuNumberIndex = menuNumberIndex;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public int getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(int menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
