package com.indiareads.ksp.model;

public class ExploreModel {

    public String item_Id;

    public String thumnail;
    public String title;
    public String intent_type;
    public String content_type;
    public String intent_parameter_1;
    public String intent_parameter_2;
    public String intent_parameter_3;
    public boolean isClickable;


    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getItem_Id() {
        return item_Id;
    }

    public void setItem_Id(String item_Id) {
        this.item_Id = item_Id;
    }

    public String getThumnail() {
        return thumnail;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntent_type() {
        return intent_type;
    }

    public void setIntent_type(String intent_type) {
        this.intent_type = intent_type;
    }

    public String getIntent_parameter_1() {
        return intent_parameter_1;
    }

    public void setIntent_parameter_1(String intent_parameter_1) {
        this.intent_parameter_1 = intent_parameter_1;
    }

    public String getIntent_parameter_2() {
        return intent_parameter_2;
    }

    public void setIntent_parameter_2(String intent_parameter_2) {
        this.intent_parameter_2 = intent_parameter_2;
    }

    public String getIntent_parameter_3() {
        return intent_parameter_3;
    }

    public void setIntent_parameter_3(String intent_parameter_3) {
        this.intent_parameter_3 = intent_parameter_3;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }
}
