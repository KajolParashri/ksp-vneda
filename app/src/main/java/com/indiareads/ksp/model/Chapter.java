package com.indiareads.ksp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Chapter implements Parcelable{

    private String chapterId;
    private String chapterName;
    private String chapterData;
    private String chapterStatus;
    private String courseName;

    public Chapter(){}

    public Chapter(String chapterId, String chapterName, String chapterData, String chapterStatus, String courseName) {
        this.chapterId = chapterId;
        this.chapterName = chapterName;
        this.chapterData = chapterData;
        this.chapterStatus = chapterStatus;
        this.courseName = courseName;
    }

    private Chapter(Parcel in){
        String[] data = new String[5];

        in.readStringArray(data);
        // the order needs to be the same as in writeToParcel() method
        this.chapterId = data[0];
        this.chapterName = data[1];
        this.chapterData = data[2];
        this.chapterStatus = data[3];
        this.courseName = data[4];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.chapterId,
                this.chapterName,
                this.chapterData,
        this.chapterStatus,
                this.courseName});
    }

    public static final Creator CREATOR = new Creator() {
        public Chapter createFromParcel(Parcel in) {
            return new Chapter(in);
        }

        public Chapter[] newArray(int size) {
            return new Chapter[size];
        }
    };

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterData() {
        return chapterData;
    }

    public void setChapterData(String chapterData) {
        this.chapterData = chapterData;
    }

    public String getChapterStatus() {
        return chapterStatus;
    }

    public void setChapterStatus(String chapterStatus) {
        this.chapterStatus = chapterStatus;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}
