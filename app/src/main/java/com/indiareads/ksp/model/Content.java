package com.indiareads.ksp.model;

public class Content {

    public static final String CONTENT_STATUS_TYPE_CREATED = "1";
    public static final String CONTENT_STATUS_TYPE_CURATED = "2";
    public static final String CONTENT_STATUS_TYPE_LIVE = "3";
    public static final String DATA = "data";
    public static final String TITLE = "title";

    public String order_id;
    public String delivery_address_id;

    private User user;
    private Source source;
    public String returnBtnText;
    public String thumnail;
    private String createdDate;
    private String title;
    private String thumbnail;
    public String contentEbook;
    public String contentSummary;
    private String description;
    private String contentType;
    private String likeStatus;
    private String contentId;
    private String orderStatus;
    private String likeCount;
    private String commentCount;
    private String avgContentRating;
    private String rating;
    private String bookShelfType;
    private String data;
    private String childCatName;
    private String parentCatName;
    private String contentCompanyId;
    private String childCatId;
    private String parentCatId;
    private String statusType;
    private String adminDiscard;
    private String userIdSource;
    private String sourceName;
    private String userSourceType;
    private String authorName;

    public String getBookShelfType() {
        return bookShelfType;
    }

    public void setBookShelfType(String bookShelfType) {
        this.bookShelfType = bookShelfType;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getReturnBtnText() {
        return returnBtnText;
    }

    public void setReturnBtnText(String returnBtnText) {
        this.returnBtnText = returnBtnText;
    }

    public String getThumnail() {
        return thumnail;
    }

    public String getContentEbook() {
        return contentEbook;
    }

    public void setContentEbook(String contentEbook) {
        this.contentEbook = contentEbook;
    }

    public String getContentSummary() {
        return contentSummary;
    }

    public void setContentSummary(String contentSummary) {
        this.contentSummary = contentSummary;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDelivery_address_id() {
        return delivery_address_id;
    }

    public void setDelivery_address_id(String delivery_address_id) {
        this.delivery_address_id = delivery_address_id;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUserIdSource() {
        return userIdSource;
    }

    public void setUserIdSource(String userIdSource) {
        this.userIdSource = userIdSource;
    }

    public String getUserSourceType() {
        return userSourceType;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setUserSourceType(String userSourceType) {
        this.userSourceType = userSourceType;
    }

    public String getAdminDiscard() {
        return adminDiscard;
    }

    public void setAdminDiscard(String adminDiscard) {
        this.adminDiscard = adminDiscard;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public User getUser() {
        return user;
    }

    public String getParentCatId() {
        return parentCatId;
    }

    public void setParentCatId(String parentCatId) {
        this.parentCatId = parentCatId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(String likeStatus) {
        this.likeStatus = likeStatus;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getAvgContentRating() {
        return avgContentRating;
    }

    public void setAvgContentRating(String avgContentRating) {
        this.avgContentRating = avgContentRating;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getChildCatName() {
        return childCatName;
    }

    public void setChildCatName(String childCatName) {
        this.childCatName = childCatName;
    }

    public String getParentCatName() {
        return parentCatName;
    }

    public void setParentCatName(String parentCatName) {
        this.parentCatName = parentCatName;
    }

    public String getChildCatId() {
        return childCatId;
    }

    public void setChildCatId(String childCatId) {
        this.childCatId = childCatId;
    }

    public String getContentCompanyId() {
        return contentCompanyId;
    }

    public void setContentCompanyId(String contentCompanyId) {
        this.contentCompanyId = contentCompanyId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }
}
