package com.indiareads.ksp.model;

public class Source {
    private String sourceId;
    private String sourceName;
    private String sourceProfilePic;

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSourceProfilePic() {
        return sourceProfilePic;
    }

    public void setSourceProfilePic(String sourceProfilePic) {
        this.sourceProfilePic = sourceProfilePic;
    }
}
