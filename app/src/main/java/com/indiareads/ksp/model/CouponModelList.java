package com.indiareads.ksp.model;

public class CouponModelList extends BaseParentModel {
    private String couponName;
    private String couponValidity;
    private String couponCode;
    private String couponDetails;

    public String getCouponName() {
        return couponName;
    }

    public String getCouponDetails() {
        return couponDetails;
    }

    public void setCouponDetails(String couponDetails) {
        this.couponDetails = couponDetails;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponValidity() {
        return couponValidity;
    }

    public void setCouponValidity(String couponValidity) {
        this.couponValidity = couponValidity;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
}
