package com.indiareads.ksp.model;

public class CouponModel {

    public String couponImage;
    public String couponId;

    public CouponModel() {
    }

    public String getCouponImage() {
        return couponImage;
    }

    public void setCouponImage(String couponImage) {
        this.couponImage = couponImage;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }
}
