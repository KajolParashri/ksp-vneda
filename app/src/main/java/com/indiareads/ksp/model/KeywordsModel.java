package com.indiareads.ksp.model;

public class KeywordsModel {

    public String _name;
    public String _id;
    public String content_type;

    public String getContentType() {
        return content_type;
    }

    public void setContentType(String contentType) {
        this.content_type = contentType;
    }

    public KeywordsModel() {
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
