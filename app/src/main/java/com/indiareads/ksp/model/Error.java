package com.indiareads.ksp.model;

import android.util.Log;

import com.android.volley.NetworkError;
import com.android.volley.VolleyError;
import com.indiareads.ksp.utils.Logger;

public class Error {

    public static String ERROR_TYPE_NO_CONNECTION_ERROR = "Network Error";
    public static String ERROR_TYPE_SERVER_ERROR = "Server Error";
    public static String ERROR_TYPE_EXCEPTION_ERROR = "Exception Error";
    public static String ERROR_TYPE_NORMAL_ERROR = "Normal Error";

    private String errorClassName;
    private String containerClassName;
    private String containerFunctionName;
    private String type;
    private String message;

    private Error(String errorClassName, String containerClassName, String containerFunctionName, String type, String message)
    {
        this.errorClassName = errorClassName;
        this.containerClassName = containerClassName;
        this.containerFunctionName = containerFunctionName;
        this.type = type;
        this.message = message;

        Logger.error(this);

        sendErrorRequest();
    }

    public static Error generateError(String errorClassName, String containerClassName, String containerFunctionName, String type, String message) {
        return new Error(errorClassName,containerClassName,containerFunctionName,type,message);
    }

    public static String getErrorTypeFromVolleyError(VolleyError volleyError)
    {
        if (volleyError instanceof NetworkError)
            return Error.ERROR_TYPE_NO_CONNECTION_ERROR;
        else
            return Error.ERROR_TYPE_SERVER_ERROR;

    }

    private void sendErrorRequest()
    {

    }

    public String getErrorClassName() {
        return errorClassName;
    }

    public String getContainerClassName() {
        return containerClassName;
    }

    public String getContainerFunctionName() {
        return containerFunctionName;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
}
