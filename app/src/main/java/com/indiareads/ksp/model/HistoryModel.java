package com.indiareads.ksp.model;

public class HistoryModel {

    public String dealId;
    public String historyTitle;
    public String burnedCoins;

    public HistoryModel() {
    }

    public String getHistoryTitle() {
        return historyTitle;
    }

    public void setHistoryTitle(String historyTitle) {
        this.historyTitle = historyTitle;
    }

    public String getBurnedCoins() {
        return burnedCoins;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public void setBurnedCoins(String burnedCoins) {
        this.burnedCoins = burnedCoins;
    }
}
