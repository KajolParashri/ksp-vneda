package com.indiareads.ksp.model;

/**
 * Created by Kashish on 05-08-2018.
 */

public class Comment {

    private String fullUserName;
    private String profilePic;
    private String designation;
    private String createdAt;
    private String comment;
    private String userId;
    private String companyId;
    private int commentType;

    public Comment(){}

    public Comment(String fullUserName, String profilePic,
                   String designation, String createdAt,
                   String comment, String userId,
                   String companyId, int commentType) {
        this.fullUserName = fullUserName;
        this.profilePic = profilePic;
        this.designation = designation;
        this.createdAt = createdAt;
        this.comment = comment;
        this.userId = userId;
        this.companyId = companyId;
        this.commentType = commentType;
    }

    public int getCommentType() {
        return commentType;
    }

    public void setCommentType(int commentType) {
        this.commentType = commentType;
    }

    public String getFullUserName() {
        return fullUserName;
    }

    public void setFullUserName(String fullUserName) {
        this.fullUserName = fullUserName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

}
