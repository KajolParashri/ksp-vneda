package com.indiareads.ksp.model;

public class ArticlePostMainModel {

    String content_main_image;
    int content_type_icon;
    String content_title;
    boolean isClickable;


    public ArticlePostMainModel(String content_main_image, int content_type_icon, String content_title, boolean isClickable) {
        this.content_main_image = content_main_image;
        this.content_type_icon = content_type_icon;
        this.content_title = content_title;
        this.isClickable = isClickable;

    }

    public String getContent_main_image() {
        return content_main_image;
    }

    public int getContent_type_icon() {
        return content_type_icon;
    }

    public String getContent_title() {
        return content_title;
    }

    public boolean isClickable() {
        return isClickable;
    }
}
