package com.indiareads.ksp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;

/**
 * Created by Kashish on 13-10-2018.
 */

public class Params implements Parcelable {

    public Params() {
    }

    private String mediaType = "";

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    public String getFeatImageName() {
        return featImageName;
    }

    public void setFeatImageName(String featImageName) {
        this.featImageName = featImageName;
    }

    public String getFeatUri() {
        return featUri;
    }

    public void setFeatUri(String featUri) {
        this.featUri = featUri;
    }

    public String getMediaUri() {
        return mediaUri;
    }

    public void setMediaUri(String mediaUri) {
        this.mediaUri = mediaUri;
    }

    public String getMediaURL() {
        return mediaURL;
    }

    public void setMediaURL(String mediaURL) {
        this.mediaURL = mediaURL;
    }

    public String getMediaFeatImage() {
        return mediaFeatImage;
    }

    public void setMediaFeatImage(String mediaFeatImage) {
        this.mediaFeatImage = mediaFeatImage;
    }

    public String getMediaTitle() {
        return mediaTitle;
    }

    public void setMediaTitle(String mediaTitle) {
        this.mediaTitle = mediaTitle;
    }

    public String getMediaTranscript() {
        return mediaTranscript;
    }

    public void setMediaTranscript(String mediaTranscript) {
        this.mediaTranscript = mediaTranscript;
    }

    public String getMediaISBNno() {
        return mediaISBNno;
    }

    public void setMediaISBNno(String mediaISBNno) {
        this.mediaISBNno = mediaISBNno;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMediaDescription() {
        return mediaDescription;
    }

    public void setMediaDescription(String mediaDescription) {
        this.mediaDescription = mediaDescription;
    }

    public JsonArray getExistingArray() {
        return existingArray;
    }

    public void setExistingArray(JsonArray existingArray) {
        this.existingArray = existingArray;
    }

    public JsonArray getNewArray() {
        return newArray;
    }

    public void setNewArray(JsonArray newArray) {
        this.newArray = newArray;
    }

    public String getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public String getPrivacyStatus() {
        return privacyStatus;
    }

    public void setPrivacyStatus(String privacyStatus) {
        this.privacyStatus = privacyStatus;
    }

    public static Creator<Params> getCREATOR() {
        return CREATOR;
    }

    private String mediaName = "";
    private String featImageName = "";
    private String featUri = "";
    private String mediaUri = "";
    private String mediaURL = "";
    private String mediaFeatImage = "";
    private String mediaTitle = "";
    private String mediaDescription = "";
    private String privacyStatus;
    private String userId = "";
    private String mediaTranscript = "";
    private String mediaISBNno = "";
    private JsonArray existingArray = new JsonArray();
    private JsonArray newArray = new JsonArray();
    private String selectedCategory;
    private String contentType;
    private int draftStatus;
    private String setFeatImageURL;

    public String getSetFeatImageURL() {
        return setFeatImageURL;
    }

    public void setSetFeatImageURL(String setFeatImageURL) {
        this.setFeatImageURL = setFeatImageURL;
    }

    public int getDraftStatus() {
        return draftStatus;
    }

    public void setDraftStatus(int draftStatus) {
        this.draftStatus = draftStatus;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Params(String mediaType, String mediaName, String featImageName,
                  String featUri, String mediaUri, String mediaURL, String mediaFeatImage,
                  String mediaTitle, String mediaTranscript, String mediaISBNno,
                  String userId, String mediaDescription, JsonArray existingArray,
                  JsonArray newArray, String selectedCategory, String privacyStatus, int draftStatus, String setFeatImageURL) {
        this.mediaType = mediaType;
        this.mediaName = mediaName;
        this.featImageName = featImageName;
        this.featUri = featUri;
        this.mediaUri = mediaUri;
        this.mediaURL = mediaURL;
        this.mediaFeatImage = mediaFeatImage;
        this.mediaTitle = mediaTitle;
        this.mediaDescription = mediaDescription;
        this.privacyStatus = privacyStatus;
        this.userId = userId;
        this.mediaTranscript = mediaTranscript;
        this.mediaISBNno = mediaISBNno;
        this.setFeatImageURL = setFeatImageURL;
        this.draftStatus = draftStatus;
        this.existingArray = existingArray;
        this.newArray = newArray;
        this.selectedCategory = selectedCategory;
    }

    private Params(Parcel in) {
        mediaType = in.readString();
        mediaName = in.readString();
        featImageName = in.readString();
        featUri = in.readString();
        mediaUri = in.readString();
        mediaURL = in.readString();
        mediaFeatImage = in.readString();
        mediaTitle = in.readString();
        mediaDescription = in.readString();
        privacyStatus = in.readString();
        userId = in.readString();
        mediaTranscript = in.readString();
        mediaISBNno = in.readString();
        selectedCategory = in.readString();
        setFeatImageURL = in.readString();
        draftStatus = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mediaType);
        dest.writeString(mediaName);
        dest.writeString(featImageName);
        dest.writeString(featUri);
        dest.writeString(mediaUri);
        dest.writeString(mediaURL);
        dest.writeString(mediaFeatImage);
        dest.writeString(mediaTitle);
        dest.writeString(mediaDescription);
        dest.writeString(privacyStatus);
        dest.writeString(userId);
        dest.writeString(mediaTranscript);
        dest.writeString(mediaISBNno);
        dest.writeInt(draftStatus);
        dest.writeString(selectedCategory);
        dest.writeString(setFeatImageURL);
    }

    @Override
    public String toString() {
        return "Params{" +
                "mediaTitle='" + mediaTitle + '\'' +
                ", mediaDescription='" + mediaDescription + '\'' +
                ", privacyStatus='" + privacyStatus + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Params> CREATOR = new Creator<Params>() {
        @Override
        public Params createFromParcel(Parcel in) {
            return new Params(in);
        }

        @Override
        public Params[] newArray(int size) {
            return new Params[size];
        }
    };
}
