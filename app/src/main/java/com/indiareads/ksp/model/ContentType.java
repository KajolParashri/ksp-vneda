package com.indiareads.ksp.model;

import com.indiareads.ksp.R;
import com.indiareads.ksp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ContentType {

    private String contentTypeId;
    private String contentTypeName;

    public ContentType(String contentTypeId, String contentTypeName) {
        this.contentTypeId = contentTypeId;
        this.contentTypeName = contentTypeName;
    }

    public String getContentTypeId() {
        return contentTypeId;
    }

    public void setContentTypeId(String contentTypeId) {
        this.contentTypeId = contentTypeId;
    }

    public String getContentTypeName() {
        return contentTypeName;
    }

    public void setContentTypeName(String contentTypeName) {
        this.contentTypeName = contentTypeName;
    }

    public static List<ContentType> getContentTypeList() {
        List<ContentType> contentTypeList = new ArrayList<>();

        contentTypeList.add(new ContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ARTICLE), Constants.Content.ARTICLE));
        contentTypeList.add(new ContentType(String.valueOf(Constants.Content.CONTENT_TYPE_VIDEO), Constants.Content.VIDEO));
        contentTypeList.add(new ContentType(String.valueOf(Constants.Content.CONTENT_TYPE_COURSE), Constants.Content.COURSE));
        contentTypeList.add(new ContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PODCAST), Constants.Content.PODCAST));
        contentTypeList.add(new ContentType(String.valueOf(Constants.Content.CONTENT_TYPE_INFOGRAPHIC), Constants.Content.INFOGRAPHIC));
        contentTypeList.add(new ContentType(String.valueOf(Constants.Content.CONTENT_TYPE_EBOOK), Constants.Content.EBOOK));
        contentTypeList.add(new ContentType(String.valueOf(Constants.Content.CONTENT_TYPE_BOOK_LIBRARY), Constants.Content.BOOK_LIBRARY));

        return contentTypeList;
    }
}
