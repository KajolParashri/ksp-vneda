package com.indiareads.ksp.model;

public class Directory {

    private String id;
    private String name;
    private String parentFid;

    public Directory() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentFid() {
        return parentFid;
    }

    public void setParentFid(String parentFid) {
        this.parentFid = parentFid;
    }
}
