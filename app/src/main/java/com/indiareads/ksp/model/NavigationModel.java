package com.indiareads.ksp.model;

public class NavigationModel {
    public String menuName;
    public String menuType;
    public Class redirectClass;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public Class getRedirectClass() {
        return redirectClass;
    }

    public void setRedirectClass(Class redirectClass) {
        this.redirectClass = redirectClass;
    }
}
