package com.indiareads.ksp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class UploadParams implements Parcelable {

    public static final String TAG = UploadParams.class.getSimpleName();

    public static final String FILE_NAME = "File Name";
    public static final String ENTITY_ID = "Entity Id";
    public static final String ACTION = "Action";
    public static final String REQUEST_OBJECT = "Request Object";
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UploadParams> CREATOR = new Parcelable.Creator<UploadParams>() {
        @Override
        public UploadParams createFromParcel(Parcel in) {
            return new UploadParams(in);
        }

        @Override
        public UploadParams[] newArray(int size) {
            return new UploadParams[size];
        }
    };
    private String action;
    private String entityId;
    private String fileName;
    private String path;
    private String key;
    private JSONObject jsonObject;

    public UploadParams(String action, String entityId, String fileName, String path, String key, JSONObject jsonObject) {
        this.action = action;
        this.entityId = entityId;
        this.fileName = fileName;
        this.path = path;
        this.key = key;
        this.jsonObject = jsonObject;
    }

    protected UploadParams(Parcel in) {
        action = in.readString();
        entityId = in.readString();
        fileName = in.readString();
        path = in.readString();
        key = in.readString();
        try {
            jsonObject = in.readByte() == 0x00 ? null : new JSONObject(in.readString());
        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "UploadParams", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(action);
        dest.writeString(entityId);
        dest.writeString(fileName);
        dest.writeString(path);
        dest.writeString(key);
        if (jsonObject == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeString(jsonObject.toString());
        }
    }

    @Override
    public String toString() {
        return action + " " + entityId + " " + fileName + " " + path + " " + key + " " + jsonObject.toString();
    }
}