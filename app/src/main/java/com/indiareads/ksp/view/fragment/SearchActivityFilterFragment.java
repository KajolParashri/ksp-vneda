package com.indiareads.ksp.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.FilterFragmentRecyclerAdapter;
import com.indiareads.ksp.adapters.FilterSearchByRecyclerAdapter;
import com.indiareads.ksp.adapters.SearchActivityViewPagerAdapter;
import com.indiareads.ksp.listeners.OnFilterAppliedListener;
import com.indiareads.ksp.listeners.OnFilterCategoryClickListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Category;
import com.indiareads.ksp.model.ChildCategory;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchActivityFilterFragment extends Fragment {

    public static final String FILTER_MY_COMPANY = "Filter My Company";
    public static final String FILTER_MY_COMPANY_VALUE = "0";
    public static final String FILTER_CATEGORIES = "Filter Categories";
    public static final String FILTER_SEARCH_BY = "Filter Search By";
    public static final String TAG = SearchActivityFilterFragment.class.getSimpleName();

    private View mFragmentRootView;

    ImageView closeButton;
    TextView clearButton;
    TextView cancelButton;
    TextView applyButton;

    private RecyclerView categoriesRecyclerView;
    private RecyclerView searchByRecyclerView;

    private FilterFragmentRecyclerAdapter categoryFilterRecyclerAdapter;
    private FilterSearchByRecyclerAdapter searchByFilterRecyclerAdapter;

    private List<Category> parentCategories;
    private List<ChildCategory> searchByOptions;

    private ArrayList<String> filterMyCompany;
    private ArrayList<String> filterCategories;
    private ArrayList<String> filterSearchBy;

    private int currentFragmentPosition;

    private OnFilterAppliedListener onFilterAppliedListener;

    public SearchActivityFilterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentRootView = inflater.inflate(R.layout.fragment_search_activity_filter, container, false);

        initViews();
        setClickListeners();
        getFilters();
        showEligibleFilters();

        return mFragmentRootView;
    }

    private void showEligibleFilters() {
        if (currentFragmentPosition == 2 || currentFragmentPosition == 4 || currentFragmentPosition == 6 || currentFragmentPosition == 7)
            getCategories();
        else
            hideCategoriesLayout();

        if (currentFragmentPosition == 7)
            getSearchBy();
        else
            hideSearchByLayout();
    }

    private void hideSearchByLayout() {
        mFragmentRootView.findViewById(R.id.fragment_filter_searchby).setVisibility(View.GONE);
        searchByRecyclerView.setVisibility(View.GONE);
    }

    private void hideCategoriesLayout() {
        mFragmentRootView.findViewById(R.id.fragment_filter_categories).setVisibility(View.GONE);
        categoriesRecyclerView.setVisibility(View.GONE);
    }

    private void getFilters() {

        filterMyCompany = getArguments().getStringArrayList(FILTER_MY_COMPANY);
        filterCategories = getArguments().getStringArrayList(FILTER_CATEGORIES);
        filterSearchBy = getArguments().getStringArrayList(FILTER_SEARCH_BY);
        currentFragmentPosition = getArguments().getInt(SearchActivityViewPagerAdapter.CURRENT_POSITION);

        setMyCompanyFilter();
    }

    private void setMyCompanyFilter() {
        AppCompatCheckBox checkBox = mFragmentRootView.findViewById(R.id.fragment_filter_mycompany_checkbox);

        if (filterMyCompany.size() > 0) {
            checkBox.setChecked(true);
        }

    }

    private void setCategoriesRecyclerViewClickListener() {

        categoryFilterRecyclerAdapter.setOnClickListener(new OnFilterCategoryClickListener() {
            @Override
            public void onParentCategoryClicked(View view, int position) {
                CommonMethods.toggleVisibility(view.findViewById(R.id.fragment_filter_child_recyclerview));
            }

            @Override
            public void onChildCategoryClicked(View view, int parentCategoryposition, int childCategoryposition) {
                AppCompatCheckBox checkBox = view.findViewById(R.id.fragment_filter_child_category_checkbox);

                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);

                    filterCategories.remove(parentCategories.get(parentCategoryposition).getChildCategories().get(childCategoryposition).getCategoryId());
                } else {
                    checkBox.setChecked(true);

                    filterCategories.add(parentCategories.get(parentCategoryposition).getChildCategories().get(childCategoryposition).getCategoryId());
                }
            }
        });
    }

    public void setSearchByRecyclerViewClickListener() {

        searchByFilterRecyclerAdapter.setOnClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                filterSearchBy.clear();
                filterSearchBy.add(searchByOptions.get(position).getCategoryId());
                searchByFilterRecyclerAdapter.notifyDataSetChanged();

            }
        });
    }

    private void getSearchBy() {

        searchByOptions = new ArrayList<>();

        ChildCategory titleChildCategory = new ChildCategory();
        titleChildCategory.setCategoryId(Constants.Category.CATEGORY_SEARCH_BY_TITLE);
        titleChildCategory.setCategoryName(getActivity().getString(R.string.title));

        ChildCategory authorChildCategory = new ChildCategory();
        authorChildCategory.setCategoryId(Constants.Category.CATEGORY_SEARCH_BY_AUTHOR);
        authorChildCategory.setCategoryName(getActivity().getString(R.string.author));

        ChildCategory isbnChildCategory = new ChildCategory();
        isbnChildCategory.setCategoryId(Constants.Category.CATEGORY_SEARCH_BY_ISBN);
        isbnChildCategory.setCategoryName(getActivity().getString(R.string.isbn));

        searchByOptions.add(titleChildCategory);
        searchByOptions.add(authorChildCategory);
        searchByOptions.add(isbnChildCategory);

        setSearchByLayout();
    }

    private void setSearchByLayout() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        searchByRecyclerView.setLayoutManager(linearLayoutManager);
        searchByFilterRecyclerAdapter = new FilterSearchByRecyclerAdapter(getActivity(), searchByOptions, filterSearchBy);
        searchByRecyclerView.setAdapter(searchByFilterRecyclerAdapter);
        setSearchByRecyclerViewClickListener();

    }

    private void getCategories() {
        try {

            CommonMethods.showProgressBar(mFragmentRootView, R.id.activity_listing_fragment_filter_root_layout);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_PARENT_AND_CHILD_CATEGORY);

            Logger.debug(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.CONTENT_LISTING_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.debug(TAG, response.toString());

                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                            parentCategories = new ArrayList<>();

                            JSONArray parentArray = response.getJSONArray(Constants.Network.DATA);

                            for (int i = 0; i < parentArray.length(); i++) {
                                JSONObject parentObject = parentArray.getJSONObject(i);

                                Category category = new Category();
                                category.setCategoryId(parentObject.getString(Constants.Network.CAT_ID));
                                category.setCategoryName(parentObject.getString(Constants.Network.CAT_NAME));

                                List<ChildCategory> childCategories = new ArrayList<>();

                                JSONArray childArray = parentObject.getJSONArray(Constants.Network.CHILD);

                                for (int j = 0; j < childArray.length(); j++) {
                                    JSONObject childObject = childArray.getJSONObject(j);

                                    ChildCategory childCategory = new ChildCategory();
                                    childCategory.setCategoryId(childObject.getString(Constants.Network.ID));
                                    childCategory.setCategoryName(childObject.getString(Constants.Network.NAME));

                                    childCategories.add(childCategory);
                                }

                                category.setChildCategories(childCategories);

                                parentCategories.add(category);
                            }

                            setCategoriesLayout();
                            CommonMethods.hideProgressBar(mFragmentRootView, R.id.activity_listing_fragment_filter_root_layout);

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG, "getCategories", Error.ERROR_TYPE_SERVER_ERROR, "400");
                            showErrorUI(error);
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), TAG, "getCategories", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        showErrorUI(error);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError e) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG, "getCategories", Error.getErrorTypeFromVolleyError(e), e.getMessage());
                    showErrorUI(error);
                }
            });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG, "getCategories", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            showErrorUI(error);
        }

    }

    private void setCategoriesLayout() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        categoriesRecyclerView.setLayoutManager(linearLayoutManager);
        categoryFilterRecyclerAdapter = new FilterFragmentRecyclerAdapter(getActivity(), parentCategories, filterCategories);
        categoriesRecyclerView.setAdapter(categoryFilterRecyclerAdapter);
        setCategoriesRecyclerViewClickListener();
    }

    private void showErrorUI(Error error) {
        CommonMethods.hideProgressBar(mFragmentRootView, R.id.activity_listing_fragment_filter_root_layout);
        CommonMethods.showErrorView(SearchActivityFilterFragment.this, R.id.activity_listing_fragment_filter_root_layout, error);
    }

    private void setClickListeners() {

        setMyCompanyClickListener();
        setCloseClickListener();
        setCancelClickListener();
        setClearClickListener();
        setApplyClickListener();

    }

    private void setCloseClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishFragment();
            }
        });
    }

    private void setCancelClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishFragment();
            }
        });
    }

    private void setClearClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterMyCompany.clear();
                filterCategories.clear();
            }
        });
    }

    private void setApplyClickListener() {

        mFragmentRootView.findViewById(R.id.fragment_filter_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle arguments = new Bundle();
                arguments.putStringArrayList(FILTER_MY_COMPANY, filterMyCompany);
                arguments.putStringArrayList(FILTER_CATEGORIES, filterCategories);
                arguments.putStringArrayList(FILTER_SEARCH_BY, filterSearchBy);

                onFilterAppliedListener.OnFilterApplied(arguments);

                finishFragment();
            }
        });
    }

    private void setMyCompanyClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_mycompany).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatCheckBox checkBox = mFragmentRootView.findViewById(R.id.fragment_filter_mycompany_checkbox);

                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);

                    filterMyCompany.remove(FILTER_MY_COMPANY_VALUE);
                } else {
                    checkBox.setChecked(true);

                    filterMyCompany.add(FILTER_MY_COMPANY_VALUE);
                }
            }
        });
    }

    public void setOnFilterAppliedListener(OnFilterAppliedListener onFilterAppliedListener) {
        this.onFilterAppliedListener = onFilterAppliedListener;
    }

    private void initViews() {

        categoriesRecyclerView = mFragmentRootView.findViewById(R.id.fragment_filter_categories_recyclerview);
        searchByRecyclerView = mFragmentRootView.findViewById(R.id.fragment_filter_searchby_recyclerview);
        closeButton = mFragmentRootView.findViewById(R.id.fragment_filter_close);
        clearButton = mFragmentRootView.findViewById(R.id.fragment_filter_clear);
        cancelButton = mFragmentRootView.findViewById(R.id.fragment_filter_cancel);
        applyButton = mFragmentRootView.findViewById(R.id.fragment_filter_apply);
    }

    private void finishFragment() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}