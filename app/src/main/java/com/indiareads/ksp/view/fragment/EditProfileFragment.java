package com.indiareads.ksp.view.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnFileCopiedListener;
import com.indiareads.ksp.listeners.OnUpdateUserDetailsListener;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CircleTransform;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.FilePicker;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static com.indiareads.ksp.utils.CommonMethods.displayToast;
import static com.indiareads.ksp.utils.CommonMethods.getStringSizeLengthFile;
import static com.indiareads.ksp.utils.Constants.AWS.BUCKET_NAME;
import static com.indiareads.ksp.utils.FilePicker.REQUEST_CODE_CHOOSE_IMAGE;
import static com.indiareads.ksp.utils.SharedPreferencesHelper.getCurrentUser;
import static com.indiareads.ksp.utils.Urls.baseAWSUrl;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment {

    private static final int REQUEST_CODE_PERMISSION_CAMERA = 102;

    private static String TAG = EditProfileFragment.class.getSimpleName();
    int IMAGE_TYPE;
    View mFragmentRootView;
    ImageView profileImageView;
    EditText fullNameEditText;
    EditText designationEditText;
    static final int READCODE = 111;
    EditText dateOfBirthEditText;
    TextView changeImage;
    TextView changePass;
    RadioGroup genderRadioGroup;
    Button saveButton;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    ProgressBar progressBar;
    OnUpdateUserDetailsListener mOnUpdateUserDetailsListener;

    User mUser;
    String mCurrentPhotoPath = "";
    String imageName = "";

    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mFragmentRootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        //ignoreCode();
        getViews();
        setDateOfBirthEditTextClickListener();
        setSaveButtonClickListener();
        fetchData();

        return mFragmentRootView;
    }


    public void captureFromCamera() {

        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String imageFileName = timeStamp;

        imageName = imageFileName + ".jpg";
        // Creating Custom URI in Camera Code
        Intent m_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), imageName);
        Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);
        // Custom URI in "uri" Object
        Logger.error("CameraCode", "uri" + uri.toString());
        m_intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
        getActivity().startActivityForResult(m_intent, REQUEST_IMAGE_CAPTURE);

        /*
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Logger.info(TAG, "IOException");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                Uri contentUri = getUriForFile(getContext(), "com.indiareads.ksp.provider", photoFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
                cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                getActivity().startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
        }*/


    }

/*    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        Logger.info(TAG, "ONEditFragment");

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            Logger.info(TAG, "ONEditFragment");
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                Uri uri = result.getUri();
                File file = new File(uri.getPath());

                File compressFile = CommonMethods.fileCompress(file, getActivity());
                long size = compressFile.length();
                String sizeStr = getStringSizeLengthFile(size);
                Logger.info("Copied", sizeStr);

                progressBar.setVisibility(View.VISIBLE);
                Logger.info(TAG, uri.toString());
                Uri compressedURI = Uri.fromFile(compressFile);

                uploadFile(compressedURI);

            } else {

                if (result != null) {
                    Exception error = result.getError();
                    Logger.info(TAG, error.getMessage());
                }
            }

        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
/*
                Logger.info(TAG, "OneEditFragment");
                Uri uri = Uri.parse(mCurrentPhotoPath);
                Logger.info("Copied", uri.toString());
                copyFileToLocalStorage(uri);
*/

                File file = new File(Environment.getExternalStorageDirectory(), imageName);
                Logger.error("CameraCode", "uri " + file.getAbsolutePath());

                //Uri of camera image
                Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);
                Logger.error("CameraCode", "uri Main " + uri);
                copyFileToLocalStorage(uri);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == FilePicker.REQUEST_CODE_CHOOSE_IMAGE) {
            if (resultCode == RESULT_OK) {

                if (data != null) {
                    Uri uri = data.getData();
                    copyFileToLocalStorage(uri);

                } else {
                    CommonMethods.displayToast(getActivity(), getActivity().getString(R.string.something_went_wrong));
                }
            }
        }
    }


    private void openFilePicker(Uri imageUri) {

        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAllowFlipping(true)
                .setAllowCounterRotation(true)
                .setShowCropOverlay(true)
                .setAspectRatio(5, 5)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(getActivity());
    }


    private void onUploadImageClick(int type) {
        IMAGE_TYPE = type;

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, READCODE);

        } else {
            showSelectImageDialog();
            //
        }
    }

    private void setDateOfBirthEditTextClickListener() {

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                monthOfYear++;

                String month, day;

                if (monthOfYear < 10)
                    month = "0" + monthOfYear;
                else
                    month = String.valueOf(monthOfYear);

                if (dayOfMonth < 10)
                    day = "0" + dayOfMonth;
                else
                    day = String.valueOf(dayOfMonth);

                dateOfBirthEditText.setText(year + "-" + month + "-" + day);
            }
        };

        dateOfBirthEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

    }

    private void setSaveButtonClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_edit_profile_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateDetails())
                    sendSaveDetailsRequest();
            }
        });

    }

    private boolean validateDetails() {

        if (fullNameEditText.getText().toString().trim().isEmpty())
            fullNameEditText.setError(getActivity().getString(R.string.please_enter_valid_name));
        else if (designationEditText.getText().toString().trim().isEmpty())
            designationEditText.setError(getActivity().getString(R.string.please_enter_designation));
        else {
            String[] name = fullNameEditText.getText().toString().trim().split(" ");
            mUser.setFirstName(name[0]);
            if (name.length > 1)
                mUser.setLastName(name[1]);
            else
                mUser.setLastName("");
            mUser.setFullName(mUser.getFirstName() + " " + mUser.getLastName());

            mUser.setDesignation(designationEditText.getText().toString().trim());

            if (genderRadioGroup.getCheckedRadioButtonId() == R.id.fragment_edit_profile_male)
                mUser.setGender(getActivity().getString(R.string.male));
            else
                mUser.setGender(getActivity().getString(R.string.female));

            mUser.setBirthDate(dateOfBirthEditText.getText().toString());

            return true;
        }
        return false;
    }

    public void changePasswordRequest(String passNew, String passDup,
                                      final ProgressBar progressBar, final BottomSheetDialog bottomSheetDialog) {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.CHANGE_PASS);
            jsonObject.put(Constants.Network.DUPLICATE_PASS, passDup);
            jsonObject.put(Constants.Network.NEW_PASS, passNew);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getActivity()).getUserId());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.info(TAG, response.toString());

                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            progressBar.setVisibility(View.GONE);
                            enableTouch();
                            CommonMethods.displayToast(getActivity(), getActivity().getString(R.string.profile_updated_successfully));
                            updateUserDetails();
                            updateProfileFragment();
                            bottomSheetDialog.dismiss();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {

                            showErrMsg();
                            enableTouch();
                            progressBar.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        Logger.error(TAG, e.getMessage());
                        showErrMsg();
                        enableTouch();
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.error(TAG, error.getMessage());

                    enableTouch();
                    showErrMsg();
                    progressBar.setVisibility(View.GONE);
                }
            });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Logger.error(TAG, e.getMessage());

            showErrMsg();
            enableTouch();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void sendSaveDetailsRequest() {

        CommonMethods.showProgressBar(mFragmentRootView, R.id.fragment_edit_profile_root_layout);

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_UPDATE_USER_DETAILS_EDIT_PROFILE);
            jsonObject.put(Constants.Network.BIRTH_DATE, mUser.getBirthDate());
            jsonObject.put(Constants.Network.DESIGNATION, mUser.getDesignation());
            jsonObject.put(Constants.Network.FIRST_NAME, mUser.getFirstName());
            jsonObject.put(Constants.Network.GENDER, mUser.getGender());
            jsonObject.put(Constants.Network.LAST_NAME, mUser.getLastName());
            jsonObject.put(Constants.Network.PHONE, mUser.getPhone());
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getActivity()).getUserId());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.info(TAG, response.toString());

                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            CommonMethods.displayToast(getActivity(), getActivity().getString(R.string.profile_updated_successfully));
                            updateUserDetails();
                            updateProfileFragment();
                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            showErrMsg();
                        }

                        CommonMethods.hideProgressBar(mFragmentRootView, R.id.fragment_edit_profile_root_layout);

                    } catch (JSONException e) {
                        Logger.error(TAG, e.getMessage());
                        showErrMsg();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.error(TAG, error.getMessage());

                    showErrMsg();
                    CommonMethods.hideProgressBar(mFragmentRootView, R.id.fragment_edit_profile_root_layout);
                }
            });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Logger.error(TAG, e.getMessage());

            showErrMsg();
            CommonMethods.hideProgressBar(mFragmentRootView, R.id.fragment_edit_profile_root_layout);
        }
    }

    private void updateProfileFragment() {

        if (mOnUpdateUserDetailsListener == null)
            showErrMsg();
        else
            mOnUpdateUserDetailsListener.OnUpdateUserDetails(mUser);

    }

    private void updateUserDetails() {

        User currentUser = User.getCurrentUser(getActivity());
        currentUser.setFirstName(mUser.getFirstName());
        currentUser.setLastName(mUser.getLastName());
        currentUser.setDesignation(mUser.getDesignation());
        currentUser.setGender(mUser.getGender());
        currentUser.setBirthDate(mUser.getBirthDate());

        User.setAsCurrentUser(getActivity(), currentUser);
    }


    public void showChnagePass() {

        final BottomSheetDialog myDialog = new BottomSheetDialog(getActivity());
        myDialog.setContentView(R.layout.change_pass);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button closeBtn = myDialog.findViewById(R.id.closeBtn);
        final ProgressBar progress_bar = myDialog.findViewById(R.id.progress_bar);
        progress_bar.setVisibility(View.GONE);
        final EditText newPass = myDialog.findViewById(R.id.newPass);
        final EditText dupPass = myDialog.findViewById(R.id.dupPass);

        Button sbmitbtnn = myDialog.findViewById(R.id.sbmitbtnn);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        sbmitbtnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String passNew = newPass.getText().toString();
                String passDup = dupPass.getText().toString();


                if (passNew.endsWith(passDup)) {
                    progress_bar.setVisibility(View.VISIBLE);

                    disableTouch();
                    changePasswordRequest(passNew, passDup, progress_bar, myDialog);
                } else {
                    Toast.makeText(getActivity(), "Password Must be Same", Toast.LENGTH_SHORT).show();
                }
            }
        });
        myDialog.show();

    }

    public void showSelectImageDialog() {

        final BottomSheetDialog myDialog = new BottomSheetDialog(getActivity());

        myDialog.setContentView(R.layout.select_image);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView gallaryBtn = myDialog.findViewById(R.id.gallaryBtn);
        ImageView cameraBtn = myDialog.findViewById(R.id.cameraBtn);

        ImageView closeDialog = myDialog.findViewById(R.id.closeDialog);
        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //captureFromCamera();
                checkCamera();
                myDialog.dismiss();
            }
        });

        gallaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FilePicker.chooseFile(getActivity(), REQUEST_CODE_CHOOSE_IMAGE);
                myDialog.dismiss();
            }
        });
        myDialog.show();

    }

    public void checkCamera() {

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // Permission are not Granted. Ask Permission Code

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_CODE_PERMISSION_CAMERA);
        } else {
            Logger.error("CameraCode", "MethodCall");
            captureFromCamera();
        }
    }

    public void disableTouch() {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void enableTouch() {
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void getViews() {
        progressBar = mFragmentRootView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        changePass = mFragmentRootView.findViewById(R.id.changePass);
        changeImage = mFragmentRootView.findViewById(R.id.changeImage);
        profileImageView = mFragmentRootView.findViewById(R.id.fragment_edit_profile_image);
        fullNameEditText = mFragmentRootView.findViewById(R.id.fragment_edit_profile_name);
        designationEditText = mFragmentRootView.findViewById(R.id.fragment_edit_profile_designation);
        dateOfBirthEditText = mFragmentRootView.findViewById(R.id.fragment_edit_profile_dob);
        genderRadioGroup = mFragmentRootView.findViewById(R.id.fragment_edit_profile_gender);
        saveButton = mFragmentRootView.findViewById(R.id.fragment_edit_profile_save);

        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUploadImageClick(0);
            }
        });

        changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChnagePass();
            }
        });


        User user = User.getCurrentUser(getActivity());

        String featureId = user.getCompanyFeaturesFeatureId();
        String featureValue = user.getCompanyFeaturesValue();
        Logger.error("CHECKFeature", featureId + " , " + featureValue);
        if (featureId.equalsIgnoreCase("2") && featureValue.equalsIgnoreCase("1")) {
            changePass.setVisibility(View.GONE);
        } else {
            changePass.setVisibility(View.VISIBLE);
        }


        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imagePath = Urls.AWS_PROFILE_IMAGE_PATH + mUser.getProfilePic();
                //      viewImage(imagePath);
            }
        });
    }

    private void fetchData() {

        CommonMethods.showProgressBar(mFragmentRootView, R.id.fragment_edit_profile_root_layout);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getActivity()).getUserId());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_CURRENT_DETAILS);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.info(TAG, response.toString());

                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                            JSONObject data = response.getJSONArray(Constants.Network.DATA).getJSONObject(0);

                            mUser = new User();
                            mUser.setUserId(data.getString(Constants.Network.USER_ID));
                            mUser.setFirstName(data.getString(Constants.Network.FIRST_NAME));
                            mUser.setLastName(data.getString(Constants.Network.LAST_NAME));
                            mUser.setDesignation(data.getString(Constants.Network.DESIGNATION));
                            mUser.setBirthDate(data.getString(Constants.Network.BIRTH_DATE));
                            mUser.setLandline(data.getString(Constants.Network.LANDLINE));
                            mUser.setMobile(data.getString(Constants.Network.MOBILE));
                            mUser.setProfilePic(data.getString(Constants.Network.PROFILE_PIC));
                            mUser.setGender(data.getString(Constants.Network.GENDER));
                            mUser.setLocation(data.getString(Constants.Network.LOCATION));

                            setUserDetails();
                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            showErrMsg();
                        }
                        CommonMethods.hideProgressBar(mFragmentRootView, R.id.fragment_edit_profile_root_layout);

                    } catch (JSONException e) {
                        Logger.error(TAG, e.getMessage());
                        showErrMsg();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.error(TAG, error.getMessage());

                    showErrMsg();
                    CommonMethods.hideProgressBar(mFragmentRootView, R.id.fragment_edit_profile_root_layout);
                }
            });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error(TAG, e.getMessage());

            showErrMsg();
            CommonMethods.hideProgressBar(mFragmentRootView, R.id.fragment_edit_profile_root_layout);
        }
    }

    private void setUserDetails() {

        Picasso.get().load(Urls.AWS_PROFILE_IMAGE_PATH + mUser.getProfilePic()).error(R.drawable.home_bottom_account_circle)
                .placeholder(R.drawable.home_bottom_account_circle)
                .transform(new CircleTransform()).into(profileImageView);

        if (mUser.getFirstName() != null && !mUser.getFirstName().equals("") && !mUser.getFirstName().equals("null")) {
            fullNameEditText.setText(mUser.getFirstName());
        }
        fullNameEditText.append(" ");
        if (mUser.getLastName() != null && !mUser.getLastName().equals("") && !mUser.getLastName().equals("null")) {
            fullNameEditText.append(mUser.getLastName());
        }

        if (mUser.getDesignation() != null && !mUser.getDesignation().equals("") && !mUser.getDesignation().equals("null")) {
            designationEditText.setText(mUser.getDesignation());
        }
        if (mUser.getBirthDate() != null && !mUser.getBirthDate().equals("") && !mUser.getBirthDate().equals("null")) {
            dateOfBirthEditText.setText(mUser.getBirthDate());
        }

        if (mUser.getGender().equals(Constants.Network.MALE))
            ((AppCompatRadioButton) mFragmentRootView.findViewById(R.id.fragment_edit_profile_male)).setChecked(true);
        else
            ((AppCompatRadioButton) mFragmentRootView.findViewById(R.id.fragment_edit_profile_female)).setChecked(true);
    }

    private void showErrMsg() {
        Logger.error(TAG, "Not updated");
    }

    public void setOnUpdateUserDetailsListener(Fragment fragment) {
        if (fragment instanceof OnUpdateUserDetailsListener)
            this.mOnUpdateUserDetailsListener = (OnUpdateUserDetailsListener) fragment;
        else
            showErrMsg();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }

    private void uploadFile(final Uri fileUri) {

        File file = new File(fileUri.getPath());
        String fileExt = MimeTypeMap.getFileExtensionFromUrl(file.toString());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");
        String formattedDate = df.format(c.getTime());

        final String UPLOAD_FILE_NAME = getCurrentUser(getActivity())
                .getUserId() + "_" + formattedDate + "." + fileExt;

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getContext(),
                "ap-south-1:345799b2-3506-4245-a702-3a7e9d4c0071", // Identity pool ID
                Regions.AP_SOUTH_1 // Region
        );
        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider, Region.getRegion(Regions.AP_SOUTH_1));
        TransferNetworkLossHandler.getInstance(getContext());

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .defaultBucket(BUCKET_NAME)
                        .context(getActivity())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

        ///       https://s3.ap-south-1.amazonaws.com/ireadksp/ksp/user_profile/233348/233348_20180920020534.jpg
        final String URLImage = "ksp/user_profile/" + User.getCurrentUser(getActivity()).getUserId() + "/" + UPLOAD_FILE_NAME;
        final String localImage = User.getCurrentUser(getActivity()).getUserId() + "/" + UPLOAD_FILE_NAME;
        TransferObserver uploadObserver =
                transferUtility.upload(URLImage, file, CannedAccessControlList.PublicRead);

        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    Logger.info(TAG, "Uploading completed !!" + "--" + URLImage);
                    Logger.info("Copied", "Uploading completed !!" + URLImage);
                    String imageProfile = User.getCurrentUser(getActivity()).getUserId() + "/" + UPLOAD_FILE_NAME;
                    uploadRequsst(imageProfile);

                    try {
                        File file = new File(fileUri.getPath());
                        if (file.exists()) {

                        }
                        // file.delete();
                    } catch (Exception e) {
                        Logger.error(TAG, e.getMessage());
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;
                Logger.info(TAG, String.valueOf(percentDone) + "% uploading complete");
            }

            @Override
            public void onError(int id, Exception ex) {
                progressBar.setVisibility(View.GONE);
                displayToast(getActivity(), "ERROR: " + ex.getMessage() + ". Please try again");
                Logger.info(TAG, ex.getMessage() + " is the error while uploading");

                try {
                    File file = new File(fileUri.getPath());
                    if (file.exists()) {

                    }
                    //  file.delete();
                } catch (Exception e) {
                    Logger.error(TAG, e.getMessage());
                }
            }
        });

    }

    private void copyFileToLocalStorage(final Uri uri) {

        FilePicker.copyFileFromUri(getActivity(), uri, new OnFileCopiedListener() {
            @Override
            public void onFileCopied(File file) {
                Logger.error("CameraCode", "copied" + file.getAbsolutePath());
                openFilePicker(uri);
            }

            @Override
            public void onFileCopyFailed(Error error) {
                Logger.info("Copied", "Error");
                Logger.error("CameraCode", "copyError");
//                Toast.makeText(getActivity(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void uploadRequsst(final String fileName) {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_UPDATE_PROFILE_PIC);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getActivity()).getUserId());
            jsonObject.put(Constants.Network.PROFILE_PIC, fileName);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Logger.info(TAG, response.toString());
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    final String URLImage = "ksp/user_profile/" + fileName;
                                    final String localImage = fileName;

                                    String url = baseAWSUrl + "/" + URLImage;
                                    Picasso.get().load(url).transform(new CircleTransform()).into(profileImageView);
                                    progressBar.setVisibility(View.GONE);

                                    User currentUser = User.getCurrentUser(getActivity());
                                    currentUser.setProfilePic("/" + localImage);
                                    User.setAsCurrentUser(getActivity(), currentUser);

                                    mUser.setProfilePic(url);
                                    currentUser.setProfilePic(url);

                                    if (mOnUpdateUserDetailsListener == null)
                                        showErrMsg();
                                    else
                                        mOnUpdateUserDetailsListener.OnUpdateUserDetails(currentUser);
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Logger.error(TAG, "400");
                                }
                            } catch (JSONException e) {
                                Logger.error(TAG, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  Logger.error(TAG, error.get());
                        }
                    });

            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error(TAG, e.getMessage());
        }
    }

}

