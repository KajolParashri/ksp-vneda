package com.indiareads.ksp.view.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.ChaptersAdapter;
import com.indiareads.ksp.listeners.OnChapterButtonsClickListener;
import com.indiareads.ksp.model.Chapter;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;
import static com.indiareads.ksp.utils.Constants.COURSE.ACTION;
import static com.indiareads.ksp.utils.Constants.COURSE.CONTENT_ID;
import static com.indiareads.ksp.utils.Constants.COURSE.PRIVACY_STATUS;
import static com.indiareads.ksp.utils.Constants.COURSE.TITLE;
import static com.indiareads.ksp.utils.Constants.Content.COURSE;
import static com.indiareads.ksp.utils.Constants.Network.CHAPTERS;
import static com.indiareads.ksp.utils.Constants.Network.CHAPTER_NAME;
import static com.indiareads.ksp.utils.Constants.Network.DATA;
import static com.indiareads.ksp.utils.Constants.Network.ID;
import static com.indiareads.ksp.utils.Constants.Network.RESPONSE_CODE;
import static com.indiareads.ksp.utils.Constants.Network.RESPONSE_OK;

public class SetupCourseActivity extends AppCompatActivity implements OnChapterButtonsClickListener {


    private final static String TAG = SetupCourseActivity.class.getSimpleName();
    private static final int TYPE_GET_EXISTING_CHAPTERS = 1;
    private static final int TYPE_ADD_NEW_CHAPTER = 2;
    private static final int TYPE_RENAME_CHAPTER = 3;
    private static final int TYPE_DELETE_CHAPTER = 4;
    String content_type = "";
    private static final String PUBLIC = "1";
    private static final String PRIVATE = "2";

    private List<Chapter> mChaptersList;
    private ChaptersAdapter mChaptersAdapter;
    private RecyclerView mChaptersRecyclerView;

    private AlertDialog mAlertDialog;
    boolean isCheck = false;
    private String data;
    private String courseName;
    RelativeLayout draftLayout;
    String title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_course);

        getIntentData();
        setupLoadingAlertDialog();
        initViews();
        getExistingChapters();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void getIntentData() {

        data = getIntent().getStringExtra("data");
        courseName = getIntent().getStringExtra("course_name");
        content_type = getIntent().getStringExtra("content_type");
        title = getIntent().getStringExtra("title");

    }

    private void initViews() {
        Toolbar mToolbar = findViewById(R.id.activity_course_setup_toolbar);
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        mToolbar.setTitle(title);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        draftLayout = findViewById(R.id.draftLayout);

        draftLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SetupCourseActivity.this, R.string.article_draft_success, Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        Button btnAddChapter = findViewById(R.id.activity_course_fab_add_chapter);

        mChaptersList = new ArrayList<>();

        mChaptersRecyclerView = findViewById(R.id.activity_setup_ccourse_chapters_rv);

        btnAddChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewChapter();
            }
        });
    }

    private void setupRecyclerView() {
        mChaptersAdapter = new ChaptersAdapter(mChaptersList, this);
        mChaptersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mChaptersRecyclerView.setAdapter(mChaptersAdapter);
        mChaptersAdapter.notifyDataSetChanged();

    }

    private void getExistingChapters() {
        mAlertDialog.show();
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, "show_existing_chapters");
            jsonObject.put(CONTENT_ID, data);

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {
                            mChaptersRecyclerView.setAdapter(null);
                            mAlertDialog.dismiss();
                            JSONObject mJsonObject = response.getJSONObject(DATA);
                            JSONArray jsonArray = mJsonObject.getJSONArray(CHAPTERS);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                Chapter chapter = new Chapter();
                                chapter.setChapterName(jsonObject1.getString(CHAPTER_NAME));
                                chapter.setChapterData(DATA);
                                chapter.setCourseName(courseName);
                                chapter.setChapterId(jsonObject1.getString(ID));
                                mChaptersList.add(chapter);
                            }
                            setupRecyclerView();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(SetupCourseActivity.this);
                        } else {
                            mAlertDialog.dismiss();
                            showErrorAlertDialog("Unknown error!!", TYPE_GET_EXISTING_CHAPTERS);
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                    "addChapter", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                        }
                    } catch (JSONException e) {
                        mAlertDialog.dismiss();
                        showErrorAlertDialog("Unknown error!!", TYPE_GET_EXISTING_CHAPTERS);
                        Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                "addChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    mAlertDialog.dismiss();
                    showErrorAlertDialog(volleyError.getMessage(), TYPE_GET_EXISTING_CHAPTERS);
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                            "addChapter", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });
            VolleySingleton.getInstance(SetupCourseActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            mAlertDialog.dismiss();
            showErrorAlertDialog(e.getMessage(), TYPE_GET_EXISTING_CHAPTERS);
            Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                    "addChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    private void setupLoadingAlertDialog() {
        mAlertDialog = new AlertDialog.Builder(this).create();

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.new_chapter_alert_dialog_layout, null);

        mAlertDialog.setView(dialogView);
        mAlertDialog.findViewById(R.id.add_chapter_alert_dialog_text);
    }

    private void showErrorAlertDialog(String message, final int type) {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error!");
        alertDialog.setMessage(message);

        if (type != TYPE_DELETE_CHAPTER && type != TYPE_RENAME_CHAPTER) {
            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (type) {
                        case TYPE_ADD_NEW_CHAPTER:
                            addNewChapter();
                            break;
                        case TYPE_GET_EXISTING_CHAPTERS:
                            getExistingChapters();
                            break;
                    }
                }
            });
        }

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void showRenameDialog(final Chapter chapter, final int position) {
        final AlertDialog dialogBuilder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.add_link_article_layout, null);
        dialogBuilder.setView(dialogView);

        dialogView.findViewById(R.id.add_link_desc).setVisibility(View.GONE);
        dialogView.findViewById(R.id.seperator).setVisibility(View.GONE);
        final EditText chapterNewName = dialogView.findViewById(R.id.add_link_url);
        ImageButton btn_confirm_link = dialogView.findViewById(R.id.btn_confirm_link);
        chapterNewName.setText(chapter.getChapterName());

        btn_confirm_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (chapterNewName.getText().toString().length() == 0) {
                    Toast.makeText(SetupCourseActivity.this, "Please Enter Chapter Name", Toast.LENGTH_SHORT).show();
                } else {
                    renameChapter(chapterNewName.getText().toString(), chapter, position);
                    dialogBuilder.dismiss();
                }
            }
        });
        dialogBuilder.show();
    }

    public void renameChapter(final String newName, final Chapter chapter, final int position) {

        mAlertDialog.show();

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, "rename_chapter_name");
            jsonObject.put("renamed_chapter_name", newName);
            jsonObject.put("renamed_chapter_id", chapter.getChapterId());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {

                            chapter.setChapterName(newName);
                            mChaptersAdapter.notifyItemChanged(position);
                            mAlertDialog.dismiss();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(SetupCourseActivity.this);
                        } else {

                            mAlertDialog.dismiss();
                            showErrorAlertDialog("Unknown error!!", TYPE_RENAME_CHAPTER);
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                    "addChapter", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                        }

                    } catch (JSONException e) {
                        mAlertDialog.dismiss();
                        showErrorAlertDialog("Unknown error!!", TYPE_RENAME_CHAPTER);
                        Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                "addChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    mAlertDialog.dismiss();
                    showErrorAlertDialog(volleyError.getMessage(), TYPE_RENAME_CHAPTER);
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                            "addChapter", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });

            VolleySingleton.getInstance(SetupCourseActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {

            mAlertDialog.dismiss();
            showErrorAlertDialog(e.getMessage(), TYPE_RENAME_CHAPTER);
            Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                    "addChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

        }
    }

    public static int getRandomNumber() {
        final int random = new Random().nextInt(6) + 1; //
        return random;
    }

    private void addNewChapter() {
        mAlertDialog.show();
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, "create_new_chapter");
            jsonObject.put(CONTENT_ID, data);

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {
                            mAlertDialog.dismiss();
                            Chapter chapter = new Chapter();
                            chapter.setCourseName(courseName);
                            chapter.setChapterId(response.getString(DATA));
                            chapter.setChapterName(" Untitled ");

                            mChaptersList.add(chapter);
                            mChaptersAdapter.notifyItemInserted(mChaptersList.size());

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(SetupCourseActivity.this);
                        } else {

                            mAlertDialog.dismiss();
                            showErrorAlertDialog("Unknown error!!", TYPE_ADD_NEW_CHAPTER);
                        }

                    } catch (JSONException e) {
                        mAlertDialog.dismiss();
                        showErrorAlertDialog("Unknown error!!", TYPE_ADD_NEW_CHAPTER);
                        Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                "addChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    mAlertDialog.dismiss();
                    showErrorAlertDialog(volleyError.getMessage(), TYPE_ADD_NEW_CHAPTER);
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                            "addChapter", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });
            VolleySingleton.getInstance(SetupCourseActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            mAlertDialog.dismiss();
            showErrorAlertDialog(e.getMessage(), TYPE_ADD_NEW_CHAPTER);
            Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                    "addChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    private void deleteChapter(final Chapter chapter, final int position) {
        if (mChaptersList.size() > 1) {

            mAlertDialog.show();
            try {
                final JSONObject jsonObject = new JSONObject();
                jsonObject.put(ACTION, "delete_chapter");
                jsonObject.put("chapter_id_reference", chapter.getChapterId());
                jsonObject.put(CONTENT_ID, data);

                JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                        Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Logger.info(TAG, response.toString());

                            if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {

                                mChaptersList.remove(position);
                                mChaptersAdapter.notifyDataSetChanged();
                                mAlertDialog.dismiss();


                            } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                SideBarPanel.logout(SetupCourseActivity.this);
                            } else {
                                mAlertDialog.dismiss();
                                Error error = Error.generateError(HttpResponse.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                        "deleteChapter", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                                showErrorAlertDialog("Unknown error", TYPE_DELETE_CHAPTER);

                            }
                        } catch (JSONException e) {
                            mAlertDialog.dismiss();
                            Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                    "deleteChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                            showErrorAlertDialog(error.getMessage(), TYPE_DELETE_CHAPTER);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        mAlertDialog.dismiss();
                        showErrorAlertDialog(volleyError.getMessage(), TYPE_DELETE_CHAPTER);
                        Error error = Error.generateError(VolleyError.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                "deleteChapter", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                    }
                });
                VolleySingleton.getInstance(SetupCourseActivity.this).addToRequestQueue(jsonObjectRequest);
            } catch (JSONException e) {
                mAlertDialog.dismiss();
                showErrorAlertDialog(e.getMessage(), TYPE_DELETE_CHAPTER);
                Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                        "deleteChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                showErrorAlertDialog(error.getMessage(), TYPE_DELETE_CHAPTER);
            }
        } else {
            displayToast(this, "There should be atleast one chapter");
        }
    }

    private void publishCourse(String privacyStatus) {
        mAlertDialog.show();
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, "publish_course");
            jsonObject.put(CONTENT_ID, data);
            jsonObject.put(PRIVACY_STATUS, privacyStatus);

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {

                            Logger.info(TAG, "Video published successfully");

                            String content_id = response.getString(Constants.Network.DATA);

                            Intent intent = new Intent(getApplicationContext(), KeyWordsEntittyActivity.class);
                            intent.putExtra("content_type", content_type);
                            intent.putExtra("content_id", data);
                            getBaseContext().startActivity(intent);

                            displayToast(SetupCourseActivity.this, getString(R.string.article_publish_success));
                            finish();


                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(SetupCourseActivity.this);
                        } else {
                            mAlertDialog.dismiss();
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                    "publishCourse", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                            showErrorAlertDialog("Unknown error", TYPE_DELETE_CHAPTER);

                        }
                    } catch (
                            JSONException e) {
                        mAlertDialog.dismiss();
                        Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                                "publishCourse", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        showErrorAlertDialog(error.getMessage(), TYPE_DELETE_CHAPTER);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    mAlertDialog.dismiss();
                    showErrorAlertDialog(volleyError.getMessage(), TYPE_DELETE_CHAPTER);
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                            "publishCourse", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });
            VolleySingleton.getInstance(SetupCourseActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (
                JSONException e) {
            mAlertDialog.dismiss();
            showErrorAlertDialog(e.getMessage(), TYPE_DELETE_CHAPTER);
            Error error = Error.generateError(JSONException.class.getSimpleName(), SetupCourseActivity.class.getSimpleName(),
                    "publishCourse", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            showErrorAlertDialog(error.getMessage(), TYPE_DELETE_CHAPTER);
        }

    }

    private void setupDialog() {

        final BottomSheetDialog mDialog = new BottomSheetDialog(this);

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.editor_publish_confirmation_dialog);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        CheckBox isChecked = mDialog.findViewById(R.id.statusCheck);
        Button mPublishPublic = mDialog.findViewById(R.id.btn_publish);

        mPublishPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCheck) {
                    publishCourse(PUBLIC);
                    mDialog.dismiss();
                } else {
                    publishCourse(PRIVATE);
                    mDialog.dismiss();
                }
            }
        });

        isChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    isCheck = true;
                } else if (!compoundButton.isChecked()) {
                    isCheck = false;

                }
            }
        });

        mDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_setup_course_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_submit_course:
                setupDialog();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEditBtnClick(Chapter chapter, int position) {
        Intent intent = new Intent(SetupCourseActivity.this, EditorActivity.class);
        intent.putExtra("type", COURSE);
        intent.putExtra("intenttype", "chapter");
        intent.putExtra(DATA, mChaptersList.get(position).getChapterId());
        intent.putExtra(TITLE, mChaptersList.get(position).getChapterName());
        intent.putExtra("content_type", "3");
        startActivity(intent);
    }

    @Override
    public void onRenameBtnClick(Chapter chapter, int position) {
        showRenameDialog(chapter, position);
    }

    @Override
    public void onDeleteBtnClick(Chapter chapter, int position) {
        deleteChapter(chapter, position);
    }
}
