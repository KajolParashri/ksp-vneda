package com.indiareads.ksp.view.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.CourseChaptersAdapter;
import com.indiareads.ksp.adapters.LikeAdapter;
import com.indiareads.ksp.adapters.SimilarContentAdapter;
import com.indiareads.ksp.listeners.OnContentLikeCommentListener;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.model.Chapter;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.LibraryBook;
import com.indiareads.ksp.model.Source;
import com.indiareads.ksp.model.SuggestionModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.HTMLUtils;
import com.indiareads.ksp.utils.LikeCommentHelper;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.HomeFragment;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;
import static com.indiareads.ksp.utils.CommonMethods.displayToast;
import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;
import static com.indiareads.ksp.utils.Constants.PLACE_ORDER_SELECT_ADDRESS;

public class ProductActivity extends AppCompatActivity implements OnContentLikeCommentListener, OnRefreshListener {

    public static final String TAG = ProductActivity.class.getSimpleName();

    Intent resultIntent;
    private User mUser;
    private Source source;

    private Content mContent;
    private String mContentId, mContentType, mContentStatusType;
    private NestedScrollView mNestedScrollView;

    RatingBar mainRatingBar;
    private YouTubePlayer mPlayer = null;
    private SimilarContentAdapter similarContentAdapter;

    private List<Content> similarContents;
    private RecyclerView similarContentRecyclerView;
    private LinearLayoutManager similarContentLinearLayoutManager;

    private int similarContentPageNo;
    private boolean similarContentLoading;
    private boolean doSimilarContentPagination;

    //User
    private ImageView mUserImage;
    private TextView mUserName;
    private TextView mUserDesignation;

    TextView deliveryTime;
    LinearLayout layoutRating;

    //Content
    private TextView mCategoryParent;
    private TextView mCategoryChild;
    private TextView mContentTitle;

    private TextView mContentDescription;
    private WebView mArticleView;
    private ImageView mInfographicImage;

    private SimpleExoPlayerView mExoplayerView;
    private RecyclerView mCoursesRecyclerView;
    TextView descCourse;

    public static final String FONT_FAMILY = "Montserrat";

    public static final String REGULAR_FONT = "fonts/Montserrat-Regular.ttf";

    private List<Chapter> mChapters;
    private RatingBar mAvgContentRating;
    private String articleContentDataString = "";

    //Like
    RelativeLayout dummyLayuot;
    TextView dummyTitle;
    private LinearLayout likeLayout;

    private ImageView mLikeImage;
    ProgressBar progressBar;
    private TextView mLikeText;

    private TextView mLikeCount;
    private ImageView mCommentImage;
    private LinearLayout commentLayout;

    //Comment
    private TextView mCommentText;
    private float avgRating = 0;
    private TextView mCommentCount, rateText, title_over_lap;

    private RelativeLayout title_over_layout;
    private ImageView backBtnTool, backBtn, mainFeatureImage;
    private SimpleExoPlayer mExoplayer;

    private AppBarLayout appBar;
    private BottomSheetDialog dialogLikes;
    private RelativeLayout wishListLay, placeOrderLay;

    private TextView notifyText;
    private Button add_To_wishList;

    private boolean paused = false;
    private long onScreenTimeInMilliseconds = 0;
    private int FULLSCREEN_ACT_REQ_CODE = 100;
    private Handler customHandler = new Handler();
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            onScreenTimeInMilliseconds = onScreenTimeInMilliseconds + 1000;
            if (!paused) {
                customHandler.postDelayed(this, 1000);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_product_page);

        getDataFromIntent();
        initialiseDialog();
        initViews();

        if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_CREATED)) {
            fetchData();
        } else if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_CURATED)) {
            fetchCuratedData();
        } else {
            fetchLiveData();
        }

        initialiseIntent();
        setCollapsingToolbar(appBar);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String name = CommonMethods.getContentNameFromType(mContentType);
        rateText.setText("Rate " + name + ": ");
    }

    public void setCollapsingToolbar(AppBarLayout appBarLayout) {

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {

                    title_over_layout.setVisibility(View.VISIBLE);
                    String title = mContent.getTitle();


                    title = String.valueOf(Html.fromHtml(title));


                    if (title.length() < 30) {
                        title_over_lap.setText(title);
                    } else {
                        title = title.substring(0, 30);
                        title = title + "..";
                        title_over_lap.setText(title);
                    }
                    backBtnTool.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                } else {

                    title_over_layout.setVisibility(View.GONE);

                }
            }
        });

    }

    private void fetchLiveData() {
        if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_ARTICLE))) {

            sendMediumRequest();

        } else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_VIDEO))) {

            sendYoutubeRequest();

        } else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_PODCAST))) {

            sendListenNotesRequest();

        } else {
            Error error = Error.generateError(HttpResponse.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchLiveData", Error.ERROR_TYPE_NORMAL_ERROR, "Unsupported Content Type");
            displayErrorUI(error);
        }
    }

    private void sendContentRatingRequest(float rating) {
        progressBar.setVisibility(View.VISIBLE);
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.SUBMITRATING);
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
            jsonObject.put(Constants.Network.RATING, rating);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                progressBar.setVisibility(View.GONE);
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    String message = response.getString(Constants.Network.RESPONSE_MESSAGE);
                                    Toast.makeText(ProductActivity.this, message, Toast.LENGTH_SHORT).show();

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {

                                    Error error = Error.generateError(HttpResponse.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                            "fetchData", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));
                                    displayErrorUI(error);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                displayErrorUI(error);
                                progressBar.setVisibility(View.GONE);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                            displayErrorUI(error);
                            progressBar.setVisibility(View.GONE);
                        }
                    });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            progressBar.setVisibility(View.GONE);
            Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
            displayErrorUI(error);
        }
    }


    private void sendListenNotesRequest() {
        CommonMethods.showProgressBar(this, R.id.activity_product_root_layout);
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_PODCAST_DETAIL);
            jsonObject.put(Constants.Network.ID, mContentId);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.PODCAST_LISTENNOTES_PRODUCT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    Logger.info(TAG, response.toString());

                                    JSONObject jsonObject = response.getJSONObject(Constants.Network.DATA);

                                    Logger.error("ResponseData", jsonObject.toString());
                                    source = new Source();
                                    source.setSourceId(jsonObject.getString(Constants.Network.USER_ID_SOURCE));
                                    source.setSourceName(jsonObject.getString(Constants.Network.SOURCE_NAME));
                                    source.setSourceProfilePic("");

                                    mContent = new Content();
                                    mContent.setContentId(mContentId);
                                    mContent.setTitle(jsonObject.getString(Constants.Network.TITLE));
                                    if (jsonObject.has(Constants.Network.THUMNAIL)) {
                                        mContent.setThumnail(jsonObject.getString(Constants.Network.THUMNAIL));
                                    }
                                    mContent.setDescription(jsonObject.getString(Constants.Network.DESCRIPTION));
                                    mContent.setData(jsonObject.getString(Constants.Network.URL));
                                    mContent.setSource(source);

                                    setContentInfo(mContent);
                                    setSourceInfo();
                                    setUpSimilarContent(jsonObject);

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {
                                    Error error = Error.generateError(HttpResponse.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                            "fetchData", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));

                                    displayErrorUI(error);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }
                            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
            displayErrorUI(error);
        }
    }

    private void sendYoutubeRequest() {
        CommonMethods.showProgressBar(this, R.id.activity_product_root_layout);
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.SEARCH, mContentId);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.YOUTUBE_PRODUCT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @RequiresApi(api = Build.VERSION_CODES.O)
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getString(Constants.Network.TITLE) != null) {

                                    Logger.info(TAG, response.toString());

                                    source = new Source();
                                    source.setSourceId("13");
                                    source.setSourceName("Youtube");
                                    source.setSourceProfilePic("");

                                    mContent = new Content();
                                    mContent.setContentId(mContentId);
                                    mContent.setTitle(response.getString(Constants.Network.TITLE));
                                    if (response.has(Constants.Network.THUMNAIL)) {
                                        mContent.setThumnail(response.getString(Constants.Network.THUMNAIL));
                                    }
                                    mContent.setDescription(response.getString(Constants.Network.DESCRIPTION));
                                    mContent.setData(response.getString(Constants.Network.URL));
                                    mContent.setSource(source);

                                    setContentInfo(mContent);
                                    //   mCreateDate.setText(mContent.getCreatedDate());
                                    setSourceInfo();
                                    setUpSimilarContent(response);

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {
                                    Error error = Error.generateError(HttpResponse.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                            "fetchData", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));

                                    displayErrorUI(error);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }
                            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
            displayErrorUI(error);
        }
    }

    private void sendMediumRequest() {
        CommonMethods.showProgressBar(this, R.id.activity_product_root_layout);
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.SEARCH, mContentId.substring(20));

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.MEDIUM_PRODUCT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getString(Constants.Network.TITLE) != null) {

                                    Logger.info(TAG, response.toString());

                                    source = new Source();
                                    source.setSourceId("11");
                                    source.setSourceName("Medium");
                                    source.setSourceProfilePic("");

                                    mContent = new Content();
                                    mContent.setContentId(mContentId);
                                    if (response.has(Constants.Network.THUMNAIL)) {
                                        mContent.setThumnail(response.getString(Constants.Network.THUMNAIL));
                                    }
                                    mContent.setTitle(response.getString(Constants.Network.TITLE));
                                    mContent.setData(response.getString(Constants.Network.DESCRIPTION));
                                    mContent.setSource(source);

                                    setContentInfo(mContent);
                                    //mCreateDate.setText(mContent.getCreatedDate());
                                    setSourceInfo();
                                    setUpSimilarContent(response);

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {
                                    Error error = Error.generateError(HttpResponse.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                            "fetchData", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));

                                    displayErrorUI(error);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }
                            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
            displayErrorUI(error);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        paused = false;
        if (articleContentDataString != null) {
            setWebViewWithImageFit(articleContentDataString, mArticleView);
            mArticleView.setWebViewClient(new MyWebViewClient());
        }

        customHandler.postDelayed(updateTimerThread, 0);
    }

    private void sendPostConsumedRequest() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_POST_CONSUMED);
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);
            jsonObject.put(Constants.Network.TOTAL_TIME, (int) (onScreenTimeInMilliseconds / 1000));

            Logger.info("HITCONSUME", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.info("HITCONSUME", response.toString());
                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                }

                            } catch (Exception e) {
                                Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "sendPostConsumedRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    private void initialiseIntent() {
        resultIntent = new Intent();
        if (getIntent().hasExtra(HomeFragment.HOME_ADAPTER_POSITION))
            resultIntent.putExtra(HomeFragment.HOME_ADAPTER_POSITION, getIntent().getIntExtra(HomeFragment.HOME_ADAPTER_POSITION, -1));
        setResult(Activity.RESULT_OK, resultIntent);
    }

    private void getDataFromIntent() {
        mContentId = getIntent().getStringExtra(Constants.Network.CONTENT_ID);
        mContentType = getIntent().getStringExtra(Constants.Network.CONTENT_TYPE);
        mContentStatusType = getIntent().getStringExtra(Constants.Network.CONTENT_STATUS_TYPE);
    }

    private void initViews() {

        mainFeatureImage = findViewById(R.id.mainFeatureImage);
        mAvgContentRating = findViewById(R.id.activity_product_avg_rating);

        layoutRating = findViewById(R.id.layoutRating);
        title_over_layout = findViewById(R.id.title_over_layout);
        title_over_lap = findViewById(R.id.title_over_lap);
        backBtnTool = findViewById(R.id.backBtnTool);
        backBtn = findViewById(R.id.backBtn);
        rateText = findViewById(R.id.rateText);

        mArticleView = findViewById(R.id.activity_product_article);

        mNestedScrollView = findViewById(R.id.activity_product_root_scroll_view);
        appBar = findViewById(R.id.appBar);
        //User
        mUserImage = findViewById(R.id.home_post_item_profile_image);
        mUserName = findViewById(R.id.home_post_item_person_name);
        //mCreateDate = findViewById(R.id.home_post_item_createdate);

        mUserDesignation = findViewById(R.id.home_post_item_person_designation);

        //Content
        mContentTitle = findViewById(R.id.activity_product_title);
        mContentDescription = findViewById(R.id.activity_product_description);

        //Like
        likeLayout = findViewById(R.id.like_layout);
        mLikeImage = findViewById(R.id.like_image);
        progressBar = findViewById(R.id.progressBar);
        mLikeText = findViewById(R.id.like_text);
        mLikeCount = findViewById(R.id.like_count);

        //Comment
        commentLayout = findViewById(R.id.comment_layout);
        mCommentImage = findViewById(R.id.comment_image);
        mCommentText = findViewById(R.id.comment_text);
        mCommentCount = findViewById(R.id.comment_count);


        mLikeCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = mContent.getTitle();

                title = title.replace("'", "''");

                title = String.valueOf(Html.fromHtml(title));


                getLikesShow(title, mContent.getContentId(), 1, mContentStatusType);
            }
        });
        mainRatingBar = findViewById(R.id.activity_product_rating_bar);
    }


    private void getLikesShow(final String mainTitle, String content_id, int pageNumber, String content_status_type) {
        try {

            final ArrayList<User> likes = new ArrayList<>();
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.CONTENT_ID, content_id);
            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, content_status_type);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.LOAD_CONTENT_LIKES);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                Logger.info(TAG, response.toString());

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONArray data = response.getJSONArray(Constants.Network.DATA);

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject dataMain = data.getJSONObject(i);
                                        User user = new User();
                                        user.setFullName(dataMain.getString(Constants.Network.FULL_USER_NAME));
                                        user.setProfilePic(dataMain.getString(Constants.Network.PROFILE_PIC));
                                        user.setDesignation(dataMain.getString(Constants.Network.DESIGNATION));
                                        user.setCreatedContents(dataMain.getString(Constants.Network.CREATED_DATE));
                                        user.setUserId(dataMain.getString(Constants.Network.USER_ID));
                                        likes.add(user);
                                    }

                                    showLikes(likes, "" + mainTitle);
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {
                                    Logger.error(TAG, "400");
                                }
                            } catch (
                                    JSONException e) {
                                Logger.error(TAG, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Logger.error(TAG, error.getMessage());
                        }//pageNumber
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        } catch (
                JSONException e) {
            Logger.error(TAG, e.getMessage());
        }
    }

    public void initialiseDialog() {
        dialogLikes = new BottomSheetDialog(this);
        dialogLikes.setContentView(R.layout.likes_single_item);
        dialogLikes.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogLikes.setCanceledOnTouchOutside(false);
    }

    public void showLikes(ArrayList<User> listData, String titleMesg) {
        RecyclerView recyclerView = dialogLikes.findViewById(R.id.like_recycler);
        TextView titleMessage = dialogLikes.findViewById(R.id.titleMessage);
        if (titleMesg.length() <= 15) {
            titleMessage.setText("Likes : " + titleMesg);
        } else {
            titleMesg = titleMesg.substring(0, 15);
            titleMessage.setText("Likes : " + titleMesg + "...");
        }
        LinearLayoutManager LayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(LayoutManagaer);
        ImageView cross = dialogLikes.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLikes.dismiss();
            }
        });
        LikeAdapter adapter = new LikeAdapter(listData);
        recyclerView.setAdapter(adapter);

        if (!dialogLikes.isShowing()) {
            dialogLikes.show();
        }
    }

    public void getBookStatus(String isbn) {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.GET_DELI_TIME);
            jsonObject.put(Constants.Network.ISBN, isbn);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.ORDER_PLACE,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    String data = response.getString(Constants.Network.DATA);

                                    wishListLay.setVisibility(View.GONE);
                                    placeOrderLay.setVisibility(View.VISIBLE);
                                    if (data.equals("101")) {
                                        if (deliveryTime != null) {
                                            deliveryTime.setText("Delivery within 2-3 Days");
                                        }
                                    } else if (data.equals("102")) {
                                        if (deliveryTime != null) {
                                            deliveryTime.setText("Delivery within 3-5 Days");
                                        }
                                    } else if (data.equals("103")) {
                                        if (deliveryTime != null) {
                                            deliveryTime.setText("Delivery within 5-7 Days");
                                        }
                                    }

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {
                                    Toast.makeText(ProductActivity.this, "Book not Available", Toast.LENGTH_SHORT).show();
                                    wishListLay.setVisibility(View.VISIBLE);
                                    placeOrderLay.setVisibility(View.GONE);

                                    add_To_wishList.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            addToWishList();
                                            //            Toast.makeText(ProductActivity.this, "Added to Wishlist", Toast.LENGTH_SHORT).show();
                                            add_To_wishList.setBackgroundResource(R.drawable.btn_background_light);
                                        }
                                    });
                                    Error error = Error.generateError(HttpResponse.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                            "fetchData", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));

                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }
                            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
            displayErrorUI(error);
        }

    }

    public void addToWishList() {

        final Dialog mAlertDialog = new Dialog(ProductActivity.this);
        mAlertDialog.setContentView(R.layout.progress_dialog_layout);
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, "addtowishlist");
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.ORDER_PLACE,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                mAlertDialog.dismiss();
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    String data = response.getString(Constants.Network.DATA);
                                    if (data.equals("1")) {
                                        displayToast(ProductActivity.this, "Added to Wishlist");
                                    } else {
                                        displayToast(ProductActivity.this, "Book is Already added to Wishlist");
                                    }

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {

                                }
                            } catch (Exception e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
                                displayErrorUI(error);
                                mAlertDialog.dismiss();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            mAlertDialog.dismiss();
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new

                    DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).

                    addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            mAlertDialog.dismiss();
            Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
            displayErrorUI(error);
        }

    }

    public void addToBookShelf() {

        final Dialog mAlertDialog = new Dialog(ProductActivity.this);
        mAlertDialog.setContentView(R.layout.progress_dialog_layout);
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, "add_to_bookshelf");
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.ORDER_PLACE,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                mAlertDialog.dismiss();
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    String data = response.getString(Constants.Network.DATA);
                                    if (data.equals("1")) {
                                        displayToast(ProductActivity.this, "Added to BookShelf");
                                    } else {
                                        displayToast(ProductActivity.this, "Book is Already added to BookShelf");
                                    }

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {

                                }
                            } catch (Exception e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
                                displayErrorUI(error);
                                mAlertDialog.dismiss();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            mAlertDialog.dismiss();
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new

                    DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).

                    addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            mAlertDialog.dismiss();
            Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
            displayErrorUI(error);
        }

    }

    private void fetchData() {
        CommonMethods.showProgressBar(this, R.id.activity_product_root_layout);

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_CONTENT);
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);

            Logger.info("response", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                //intentNotification
                                Logger.info("response", response.toString());

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    //Adding jsonArray data into a list of jsonObjects
                                    JSONObject jsonObject1 = response.getJSONObject(Constants.Network.CONTENT);

                                    User user = new User();
                                    user.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                    user.setFullName(jsonObject1.getString(Constants.Network.FULL_USER_NAME));
                                    user.setProfilePic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                    user.setDesignation(jsonObject1.getString(Constants.Network.DESIGNATION));

                                    Content content = new Content();
                                    content.setUser(user);
                                    content.setContentId(mContentId);
                                    content.setCreatedDate(jsonObject1.getString(Constants.Comment.CREATED_AT));
                                    content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                    content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));
                                    content.setContentType(mContentType);

                                    if (jsonObject1.has(Constants.Network.THUMNAIL)) {
                                        content.setThumnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                    }

                                    content.setLikeStatus(jsonObject1.getString(Constants.Network.LIKED_STATUS));
                                    content.setLikeCount(jsonObject1.getString(Constants.Network.LIKE_COUNT));
                                    content.setCommentCount(jsonObject1.getString(Constants.Network.COMMENT_COUNT));
                                    content.setAvgContentRating(jsonObject1.getString(Constants.Network.AVG_CONTENT_RATING));

                                    String rating = jsonObject1.getString(Constants.Network.RATING);
                                    avgRating = Float.parseFloat(rating);

                                    if (mainRatingBar != null) {
                                        mainRatingBar.setRating(avgRating);
                                    }

                                    content.setChildCatName(jsonObject1.getString(Constants.Network.CHILD_CAT_NAME));
                                    content.setParentCatName(jsonObject1.getString(Constants.Network.PARENT_CAT_NAME));
                                    content.setChildCatId(jsonObject1.getString(Constants.Network.CHILD_CAT_ID));
                                    content.setParentCatId(jsonObject1.getString(Constants.Network.PARENT_CAT_ID));
                                    content.setContentCompanyId(jsonObject1.getString(Constants.Network.CONTENT_COMPANY_ID));

                                    int ContentType = Integer.parseInt(mContentType);

                                    if (ContentType == Constants.Content.CONTENT_TYPE_COURSE) {
                                        mChapters = new ArrayList<>();
                                        JSONArray chapterArray = jsonObject1.getJSONArray(Constants.Network.CHAPTERS);

                                        for (int i = 0; i < chapterArray.length(); i++) {
                                            JSONObject chapterObject = chapterArray.getJSONObject(i);
                                            mChapters.add(new Chapter(chapterObject.getString(Constants.Network.CHAPTER_ID),
                                                    chapterObject.getString(Constants.Network.CHAPTER_NAME),
                                                    chapterObject.getString(Constants.Network.CHAPTER_DATA),
                                                    chapterObject.getString(Constants.Network.CHAPTER_STATUS),
                                                    content.getTitle()));
                                        }

                                    } else {
                                        content.setData(jsonObject1.getString(Constants.Network.DATA));
                                    }

                                    mUser = user;
                                    mContent = content;
                                    articleContentDataString = mContent.getData();

                                    setContentInfo(mContent);

                                    setUserInfo();
                                    setLikeCommentOnClickListeners();
                                    setUpSimilarContent(response);


                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {
                                    Error error = Error.generateError(HttpResponse.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                            "fetchData", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));

                                    displayErrorUI(error);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }
                            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {

            Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
            displayErrorUI(error);

        }
    }

    private void fetchCuratedData() {
        CommonMethods.showProgressBar(this, R.id.activity_product_root_layout);

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_CURATED_CONTENT);
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);

            Logger.info("response", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT_CURATED_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    Logger.info("response", response.toString());
                                    Logger.info("", response.toString());

                                    //Adding jsonArray data into a list of jsonObjects
                                    JSONObject jsonObject1 = response.getJSONObject(Constants.Network.CONTENT);

                                    source = new Source();
                                    source.setSourceId(jsonObject1.getString(Constants.Network.SOURCE_ID));
                                    source.setSourceName(jsonObject1.getString(Constants.Network.SOURCE_NAME));
                                    source.setSourceProfilePic(jsonObject1.getString(Constants.Network.SOURCE_PROFILE_PIC));

                                    mContent = new Content();
                                    mContent.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                    mContent.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));
                                    mContent.setData(jsonObject1.getString(Constants.Network.DATA));
                                    mContent.setRating(jsonObject1.getString(Constants.Network.RATING));
                                    mContent.setContentType(mContentType);

                                    if (jsonObject1.has(Constants.Network.THUMNAIL)) {
                                        mContent.setThumnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                    }

                                    mContent.setChildCatName(jsonObject1.getString(Constants.Network.CHILD_CAT_NAME));
                                    mContent.setParentCatName(jsonObject1.getString(Constants.Network.PARENT_CAT_NAME));
                                    mContent.setChildCatId(jsonObject1.getString(Constants.Network.CHILD_CAT_ID));
                                    mContent.setParentCatId(jsonObject1.getString(Constants.Network.PARENT_CAT_ID));
                                    mContent.setAvgContentRating(jsonObject1.getString(Constants.Network.AVG_CONTENT_RATING));
                                    mContent.setSource(source);

                                    String rating = jsonObject1.getString(Constants.Network.RATING);
                                    avgRating = Float.parseFloat(rating);
                                    if (mainRatingBar != null) {
                                        mainRatingBar.setRating(avgRating);
                                    }


                                    setContentInfo(mContent);

                                    setSourceInfo();
                                    setUpSimilarContent(response);


                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {
                                    Error error = Error.generateError(HttpResponse.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                            "fetchData", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));

                                    displayErrorUI(error);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }
                            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(ProductActivity.this, R.id.activity_product_root_layout);
            displayErrorUI(error);
        }
    }

    public void showDummy(ImageView imageView) {
        imageView.setVisibility(View.GONE);
        dummyLayuot.setVisibility(View.VISIBLE);

        int color = generateRandomColor();

        dummyLayuot.setBackgroundColor(color);

        String title = mContent.getTitle();

        title = title.replace("'", "''");

        title = String.valueOf(Html.fromHtml(title));


        if (title.length() > 30) {

            title = title.substring(0, 30);
            dummyTitle.setText(title + "..");

        } else {
            dummyTitle.setText(title);
        }
    }

    private void setSourceInfo() {
        findViewById(R.id.layout_source_profile).setVisibility(View.VISIBLE);

        ((TextView) findViewById(R.id.source_name)).setText(source.getSourceName());
        String URL = Urls.AWS_CURATED_PROFILE_IMAGE_PATH + source.getSourceProfilePic();
        ImageView source_profile_image = findViewById(R.id.source_profile_image);
        CommonMethods.loadSourceImageWithGlide(this, URL, source_profile_image);
    }

    private void setUserInfo() {
        findViewById(R.id.layout_user_profile).setVisibility(View.VISIBLE);
        mUserName.setText(mUser.getFullName());
        if (mUser.getDesignation() != null && !mUser.getDesignation().equalsIgnoreCase("null"))
            mUserDesignation.setText(mUser.getDesignation());
        else
            mUserDesignation.setText("");
        ImageView home_post_item_profile_image = findViewById(R.id.home_post_item_profile_image);
        CommonMethods.loadProfileImageWithGlide(this, mUser.getProfilePic(), home_post_item_profile_image);
    }

    private void setCategoryInfo() {
        mCategoryParent = findViewById(R.id.product_category_parent);
        mCategoryChild = findViewById(R.id.product_category_child);

        mCategoryParent.setText(mContent.getParentCatName());
        mCategoryChild.setText(mContent.getChildCatName());
    }


    private void setContentInfo(Content contentInfo) {
        if (!mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_LIVE)) {
            setCategoryInfo();
        } else {
            findViewById(R.id.layout_category).setVisibility(View.GONE);
        }

        setContentLayout(contentInfo);

        if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_CREATED)) {
            findViewById(R.id.layout_like_comment).setVisibility(View.VISIBLE);
            setLikeInfo();
            setCommentInfo();
        } else {
            findViewById(R.id.layout_like_comment).setVisibility(View.GONE);
        }
    }

    private void setContentLayout(Content mContent) {
        String title = mContent.getTitle();


        // title = title.replace("'", "''");

        title = String.valueOf(Html.fromHtml(title));

        mContentTitle.setText(title);

        String url = mContent.getThumnail();
        if (url != null && !url.equals("")) {
            Picasso.get().load(url).into(mainFeatureImage);
        }

        if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_ARTICLE) {
            setArticleInfo();
        } else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_INFOGRAPHIC) {
            setInfographicInfo();
        } else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_VIDEO) {
            setVideoInfo();
        } else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_PODCAST) {
            setPodcastInfo();
        } else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_COURSE) {
            setCourseInfo();
        } else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_BOOK_LIBRARY) {
            setLibraryBookInfo();
        } else {
            setEBookInfo(mContent);
        }

        if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_LIVE)) {
            mAvgContentRating.setVisibility(View.GONE);
            layoutRating.setVisibility(View.GONE);
        } else {
            mAvgContentRating.setVisibility(View.VISIBLE);
            layoutRating.setVisibility(View.VISIBLE);

            mainRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    sendContentRatingRequest(rating);
                }
            });

            setRating();
            setDescription();
        }

        if (mUser != null && mUser.getUserId().equals(User.getCurrentUser(this).getUserId())) {
            layoutRating.setVisibility(View.GONE);
        } else {
            layoutRating.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //No call for super(). Bug on API Level > 11.
    }

    public void setLibraryBookInfo() {

        final LibraryBook libraryBook = createBookObject();

        findViewById(R.id.activity_product_media_book_layout).setVisibility(View.VISIBLE);
        ImageView thumbNailLoader = findViewById(R.id.thumbNailLoader);
        Button bookshelf = findViewById(R.id.bookshelf);
        Button bookshelff = findViewById(R.id.bookshelff);

        bookshelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToBookShelf();
            }
        });
        bookshelff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToBookShelf();
            }
        });

        dummyLayuot = findViewById(R.id.dummyLayout);
        dummyTitle = findViewById(R.id.dummyTitle);


        LinearLayout summaryLay = findViewById(R.id.summaryLay);
        Button readEBook = findViewById(R.id.readEBook);
        Button readBookSummary = findViewById(R.id.readBookSummary);

        if (libraryBook.getEbookData().equals("0") && libraryBook.getSummaryData().equals("0")) {
            summaryLay.setVisibility(View.GONE);
        }
        if (libraryBook.getEbookData().equals("0")) {
            readEBook.setVisibility(View.GONE);
        } else {
            readEBook.setVisibility(View.VISIBLE);
            readEBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String mUrl = libraryBook.getEbookData();

                    if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_CURATED)) {
                        mUrl = Urls.AWS_CURATED_CONTENT_PATH + mUrl;
                    }

                    Toast.makeText(ProductActivity.this, "" + mUrl, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ProductActivity.this, PDFWebViewActivity.class);
                    intent.putExtra(Content.DATA, mUrl);
                    intent.putExtra(Content.TITLE, mContent.getTitle());
                    intent.putExtra(Constants.Network.CONTENT_ID, "");
                    intent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, "");
                    intent.putExtra(Constants.Network.CONTENT_TYPE, "");
                    startActivity(intent);
                }
            });
        }
        if (libraryBook.getSummaryData().equals("0")) {
            readBookSummary.setVisibility(View.GONE);
        } else {
            readBookSummary.setVisibility(View.VISIBLE);
            readBookSummary.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String mUrl = libraryBook.getSummaryData();

                    if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_CURATED)) {
                        mUrl = Urls.AWS_CURATED_CONTENT_PATH + mUrl;
                    }

                    Intent intent = new Intent(ProductActivity.this, PdfOpenerActivity.class);
                    intent.putExtra(Content.DATA, mUrl);
                    intent.putExtra(Content.TITLE, mContent.getTitle());
                    startActivity(intent);
                    finish();
                }
            });
        }

        // ISBN
        if (libraryBook.getIsbn() != null && !libraryBook.getIsbn().equals("")) {
            ((TextView) findViewById(R.id.book_isbn_tv)).setText(libraryBook.getIsbn());
        } else {
            ((TextView) findViewById(R.id.book_isbn_tv)).setText("N/A");
        }


        // AUTHOR
        if (libraryBook.getContributorName() != null && !libraryBook.getContributorName().equals("")) {
            ((TextView) findViewById(R.id.book_author_tv)).setText(libraryBook.getContributorName());
        } else {
            ((TextView) findViewById(R.id.book_author_tv)).setText("N/A");
        }

        //WEIGHT
        if (libraryBook.getWeight() != null && !libraryBook.getWeight().equals("")) {
            ((TextView) findViewById(R.id.book_weight_tv)).setText(libraryBook.getWeight());
        } else {
            ((TextView) findViewById(R.id.book_weight_tv)).setText("N/A");
        }
        // BINDING
        if (libraryBook.getProductForm() != null && !libraryBook.getProductForm().equals("")) {
            ((TextView) findViewById(R.id.book_binding_tv)).setText(libraryBook.getProductForm());
        } else {
            ((TextView) findViewById(R.id.book_binding_tv)).setText("N/A");
        }

        //PageNumber
        if (libraryBook.getPageNo() != null && !libraryBook.getPageNo().equals("")) {
            ((TextView) findViewById(R.id.book_pages_tv)).setText(libraryBook.getPageNo());
        } else {
            ((TextView) findViewById(R.id.book_pages_tv)).setText("N/A");
        }

        if (libraryBook.getPublisherName() != null && !libraryBook.getPublisherName().equals("")) {
            ((TextView) findViewById(R.id.book_publisher_tv)).setText(libraryBook.getPublisherName());
        } else {
            ((TextView) findViewById(R.id.book_publisher_tv)).setText("N/A");
        }

        if (libraryBook.getTextLanguage() != null && !libraryBook.getTextLanguage().equals("")) {
            ((TextView) findViewById(R.id.book_language_tv)).setText(libraryBook.getTextLanguage());
        } else {
            ((TextView) findViewById(R.id.book_language_tv)).setText("N/A");
        }

        wishListLay = findViewById(R.id.wishListLay);
        placeOrderLay = findViewById(R.id.placeOrderLay);
        deliveryTime = findViewById(R.id.deliveryTime);
        notifyText = findViewById(R.id.notifyText);

        //Notify Me!
        String first = "when Book is Available";
        String next = "<font color='#EE0000'>Notify Me! </font>";
        String strClick = "Notify Me! ";
        notifyText.setText(Html.fromHtml(next + first));

        String str = Html.fromHtml(next + first).toString();
        SpannableString spannableString = new SpannableString(str);

        // make sure the String is exist, if it doesn't exist
        // it will throw IndexOutOfBoundException

        int startPosition = str.indexOf(strClick);
        int endPosition = str.lastIndexOf(strClick) + strClick.length();

        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false); // <-- this will remove automatic underline in set span
            }

            @Override
            public void onClick(View widget) {
                addToWishList();
            }
        }, startPosition, endPosition, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        notifyText.setText(spannableString);
        notifyText.setMovementMethod(LinkMovementMethod.getInstance());

        add_To_wishList = findViewById(R.id.add_To_wishList);
        wishListLay.setVisibility(View.GONE);
        placeOrderLay.setVisibility(View.GONE);

        if (libraryBook.getThumbnail() != null && !libraryBook.getThumbnail().equals("") && !libraryBook.getThumbnail().equals("0")) {

            String imagePath = Urls.BOOK_COVER_IMAGE_BASE + libraryBook.getThumbnail();
            Picasso.get().load(imagePath).into(mainFeatureImage);
            CommonMethods.loadNormalImageWithGlide(this, imagePath, thumbNailLoader);

        } else {
            showDummy(thumbNailLoader);
        }

        getBookStatus(libraryBook.getIsbn());

        findViewById(R.id.activity_product_place_order_buttton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductActivity.this, SelectAddressActivity.class);
                intent.putExtra(LibraryBook.ISBN, libraryBook.getIsbn());
                intent.putExtra(LibraryBook.CONTENT_ID, libraryBook.getContentId());
                intent.putExtra(LibraryBook.TYPE, PLACE_ORDER_SELECT_ADDRESS);
                startActivity(intent);
            }
        });
        add_To_wishList = findViewById(R.id.add_To_wishList);
        if (add_To_wishList.getVisibility() == View.VISIBLE) {
            add_To_wishList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addToWishList();
                }
            });
        }
        //addToWishList
    }
/*
    public String getURLMain(Content mContent) {
        String URL = "";
        try {

            if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_CURATED)) {
                JSONObject bookObject = new JSONObject(mContent.getData());
                JSONObject jsonObjectMain = bookObject.getJSONObject("0");
                String last_page = bookObject.getString("last_page");
                Logger.error("JSONEBOOK", bookObject.toString());
                URL = jsonObjectMain.getString(Constants.Network.DATA);
            }

        } catch (Exception e) {

        }
        return URL;
    }*/

    public LibraryBook createBookObject() {
        LibraryBook libraryBook = new LibraryBook();
        try {
            JSONObject bookObject = new JSONObject(mContent.getData());

            libraryBook.setId(bookObject.getJSONObject("0").getString(Constants.Network.ID));
            libraryBook.setContentId(bookObject.getJSONObject("0").getString(Constants.Network.CONTENT_ID));
            libraryBook.setIsbn(bookObject.getJSONObject("0").getString(Constants.Network.ISBN));
            libraryBook.setContributorName(bookObject.getJSONObject("0").getString(Constants.Network.CONTRIBUTOR_NAME));
            libraryBook.setWeight(bookObject.getJSONObject("0").getString(Constants.Network.WEIGHT));
            libraryBook.setProductForm(bookObject.getJSONObject("0").getString(Constants.Network.PRODUCT_FORM));
            libraryBook.setPageNo(bookObject.getJSONObject("0").getString(Constants.Network.PAGE_NO));
            libraryBook.setPublisherName(bookObject.getJSONObject("0").getString(Constants.Network.PUBLISHER_NAME));
            libraryBook.setPublisherPlace(bookObject.getJSONObject("0").getString(Constants.Network.PUBLICATION_PLACE));
            libraryBook.setTextLanguage(bookObject.getJSONObject("0").getString(Constants.Network.TEXT_LANGUAGE));
            libraryBook.setPrice(bookObject.getJSONObject("0").getString(Constants.Network.PRICE));
            libraryBook.setThumbnail(bookObject.getString(Constants.Network.THUMNAIL));
            libraryBook.setDescription(bookObject.getString(Constants.Network.DESCRIPTION));
            libraryBook.setDescription(bookObject.getString(Constants.Network.DESCRIPTION));
            libraryBook.setEbookData(bookObject.getString(Constants.Network.E_BOOK_DATA));
            libraryBook.setSummaryData(bookObject.getString(Constants.Network.SUMMARY_DATA));
            //ebook_data
            //summary_data

        } catch (Exception e) {
            Logger.error(TAG, e.getMessage());
        }

        return libraryBook;
    }

    public void setEBookInfo(final Content mContent) {
        Button viewEBookButton = findViewById(R.id.activity_product_view_ebook_buttton);
        viewEBookButton.setVisibility(View.VISIBLE);
        viewEBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewEBook(mContent);
            }
        });
    }

    private void viewEBook(Content mContent) {
        try {

            JSONObject bookObject = new JSONObject(mContent.getData());
            JSONObject jsonObjectMain = bookObject.getJSONObject("0");
            String last_page = bookObject.getString("last_page");
            Logger.error("JSONEBOOK", bookObject.toString());
            String mUrl = jsonObjectMain.getString(Constants.Network.DATA);

            if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_CURATED)) {
                mUrl = Urls.AWS_CURATED_CONTENT_PATH + mUrl;
            }

            if (mUrl.endsWith(".html") || mUrl.endsWith(".htm")) {
                Intent intent = new Intent(this, ViewEbookActivity.class);
                intent.putExtra(Content.DATA, mUrl);
                intent.putExtra(Content.TITLE, mContent.getTitle());
                intent.putExtra(Constants.Network.PAGE_NO, last_page);
                intent.putExtra(Constants.Network.CONTENT_ID, mContentId);
                intent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
                intent.putExtra(Constants.Network.CONTENT_TYPE, mContentType);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, PdfOpenerActivity.class);
                intent.putExtra(Content.DATA, mUrl);
                intent.putExtra(Content.TITLE, mContent.getTitle());
                intent.putExtra(Constants.Network.PAGE_NO, last_page);
                intent.putExtra(Constants.Network.CONTENT_ID, mContentId);
                intent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
                intent.putExtra(Constants.Network.CONTENT_TYPE, mContentType);
                startActivity(intent);
            }
        } catch (Exception e) {

        }
    }

    private void setDescription() {
        if (mContent.getDescription() != null && !mContent.getDescription().isEmpty()) {

            if (mContentType.equals("1") || mContentType.equals("3")) {
                mContentDescription.setVisibility(View.GONE);
            } else {
                mContentDescription.setVisibility(View.VISIBLE);
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(getString(R.string.description) + " : " + mContent.getDescription());
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, 11, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                mContentDescription.setText(spannableStringBuilder);
            }
        }
    }

    private void setRating() {

        mAvgContentRating = findViewById(R.id.activity_product_avg_rating);

        LayerDrawable stars = (LayerDrawable) mAvgContentRating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        float rating = Float.valueOf(mContent.getAvgContentRating());
        if (rating > 3) {
            mAvgContentRating.setRating(rating);
        } else {
            mAvgContentRating.setRating(3);
        }
    }

    private void setCourseInfo() {

        findViewById(R.id.activity_product_course_view).setVisibility(View.VISIBLE);
        mCoursesRecyclerView = findViewById(R.id.activity_product_courses);
        descCourse = findViewById(R.id.descCourse);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            descCourse.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }
        String descFormatted = Html.fromHtml(mContent.getDescription()).toString();
        descCourse.setText(descFormatted);

        mCoursesRecyclerView.setLayoutManager(new LinearLayoutManager(ProductActivity.this));
        CourseChaptersAdapter courseChaptersAdapter = new CourseChaptersAdapter(ProductActivity.this, mChapters);
        mCoursesRecyclerView.setAdapter(courseChaptersAdapter);
    }

    public void setWebViewWithImageFit(String content, WebView mArticleView) {


        String contentMain = HTMLUtils.getLocalFontInHTML(articleContentDataString, FONT_FAMILY, REGULAR_FONT);
        // content is the content of the HTML or XML.
        String stringToAdd = "height=\"auto\" width=\"100%\" ";

        // Create a StringBuilder to insert string in the middle of content.
        StringBuilder sb = new StringBuilder(contentMain);

        int i = 0;
        int cont = 0;

        // Check for the "src" substring, if it exists, take the index where
        while (i != -1) {
            i = contentMain.indexOf("src", i + 1);
            if (i != -1) sb.insert(i + (cont * stringToAdd.length()), stringToAdd);
            //   if (i != -1) sb.insert(i + cont * heightToAdd.length(), heightToAdd);
            ++cont;
        }

        String contents = HTMLUtils.getLocalFontInHTML(sb.toString(), FONT_FAMILY, REGULAR_FONT);

        mArticleView.loadDataWithBaseURL("file:///android_asset/", contents, "text/html", "UTF-8", null);

    }

    private void setArticleInfo() {

        mArticleView.setVisibility(View.VISIBLE);
        mArticleView.clearCache(true);
        mArticleView.clearHistory();
        mArticleView.setScrollContainer(false);
        mArticleView.getSettings().setJavaScriptEnabled(true);
        mArticleView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        mArticleView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        mArticleView.getSettings().setAppCachePath(getFilesDir().getAbsolutePath() + "/cache");
        mArticleView.getSettings().setDatabaseEnabled(true);
        mArticleView.getSettings().setDatabasePath(getFilesDir().getAbsolutePath() + "/databases");

        //For Immediate text display-

        String artcile = mContent.getData();


        mArticleView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_CREATED) || mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_LIVE)) {
            articleContentDataString = mContent.getData();

            //setWebViewWithImageFit(getStyledFont(articleContentDataString), mArticleView);
            setWebViewWithImageFit(articleContentDataString, mArticleView);
        } else {

            loadPage(Urls.AWS_CURATED_CONTENT_PATH + artcile);
        }

        mArticleView.setWebViewClient(new MyWebViewClient());
    }

    public class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
            // Do something with the event here
            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals("www.google.com")) {
                // This is my web site, so do not override; let my WebView load the page
                return false;
            }
            // reject anything other
            return true;
        }

    }

    private void loadPage(String url) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    articleContentDataString = response;
                    // setWebViewWithImageFit(getStyledFont(articleContentDataString), mArticleView);

                    setWebViewWithImageFit(articleContentDataString, mArticleView);
                } catch (Exception e) {
                    Logger.error(TAG, e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ProductActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                Logger.error(TAG, error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void setVideoInfo() {
        if (mContent.getData().contains("youtube.com"))
            setUpYoutubePlayer();
        else
            setUpExoplayer();
    }

    public static String getStyledFont(String html) {
        boolean addBodyStart = !html.toLowerCase().contains("<body>");
        boolean addBodyEnd = !html.toLowerCase().contains("</body");
        return "<style type=\"text/css\">@font-face {font-family: Montserrat;" +
                "src: url(\"fonts/Montserrat_Regular.ttf\")}" +
                "body {font-family: Montserrat;font-size: medium; text-align: justify;}</style>" +
                (addBodyStart ? "<body>" : "") + html + (addBodyEnd ? "</body>" : "");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (mPlayer != null)
                mPlayer.setFullscreen(true);
        }
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (mPlayer != null)
                mPlayer.setFullscreen(false);
        }
    }

    private void setUpYoutubePlayer() {
        YouTubePlayerSupportFragment youTubePlayerFragment = new YouTubePlayerSupportFragment();
        youTubePlayerFragment.initialize(getString(R.string.youtube_api_key), new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {

                mPlayer = youTubePlayer;

                if (!wasRestored) {
                    String s[] = mContent.getData().split("/");
                    youTubePlayer.cueVideo(s[s.length - 1]);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Error error = Error.generateError("YOUTUBE", ProductActivity.class.getSimpleName(),
                        "setUpYoutubePlayer", Error.ERROR_TYPE_NORMAL_ERROR, youTubeInitializationResult.toString());
                displayErrorUI(error);
            }
        });

        getSupportFragmentManager().beginTransaction().add(R.id.activity_product_media_frame_layout, youTubePlayerFragment).commitAllowingStateLoss();
    }

    private void setPodcastInfo() {
        setUpExoplayer();
    }

    private void setUpExoplayer() {
        mExoplayerView = findViewById(R.id.activity_product_exoplayer);
        mExoplayerView.setControllerHideOnTouch(true);
        findViewById(R.id.exoplayer_layout).setVisibility(View.VISIBLE);

        RenderersFactory renderersFactory = new DefaultRenderersFactory(getApplicationContext());
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        AdaptiveTrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(trackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();

        mExoplayer = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, loadControl);
        DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(
                TAG,
                null /* listener */,
                DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                true /* allowCrossProtocolRedirects */
        );

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                getApplicationContext(),
                null /* listener */,
                httpDataSourceFactory
        );

        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        Handler mainHandler = new Handler();
        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(mContent.getData()),
                dataSourceFactory,
                extractorsFactory,
                mainHandler,
                null);
        mExoplayer.prepare(mediaSource);
        mExoplayerView.setPlayer(mExoplayer);

        final View fullscreenButton = findViewById(R.id.exoplayer_fullscreen);
        fullscreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductActivity.this, FullscreenVideoActivity.class);
                intent.putExtra("time", mExoplayer.getCurrentPosition());
                intent.putExtra("data", mContent.getData());
                ProductActivity.this.startActivityForResult(intent, FULLSCREEN_ACT_REQ_CODE);
            }
        });

        mExoplayerView.setControllerVisibilityListener(new PlaybackControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                fullscreenButton.setVisibility(visibility);
            }
        });
    }

    private void setInfographicInfo() {
        if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_CURATED))
            mContent.setData(Urls.AWS_CURATED_CONTENT_PATH + mContent.getData());
        mInfographicImage = findViewById(R.id.activity_product_infographic);
        mInfographicImage.setVisibility(View.VISIBLE);

        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(mInfographicImage);
        pAttacher.update();

        Glide.with(this)
                .load(mContent.getData())
                .into(mInfographicImage);
    }

    private void setCommentInfo() {
        resultIntent.putExtra(Constants.Network.COMMENT_COUNT, mContent.getCommentCount());
        mCommentCount.setText(mContent.getCommentCount());
    }

    private void setLikeInfo() {
        resultIntent.putExtra(Constants.Network.LIKE_COUNT, mContent.getLikeCount());
        LikeCommentHelper.handleLikeButtonConfig(mContent, ProductActivity.this, mLikeImage, mLikeText, mLikeCount);
    }

    private void setLikeCommentOnClickListeners() {

        likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LikeCommentHelper.switchLikeInfo(mContent);
                LikeCommentHelper.handleLikeButtonConfig(mContent, ProductActivity.this, mLikeImage, mLikeText, mLikeCount);
                LikeCommentHelper.likeContentItem(ProductActivity.this, mContent, ProductActivity.this);
            }
        });

        commentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LikeCommentHelper.openComments(100, ProductActivity.this, mNestedScrollView, mContent, ProductActivity.this);
            }
        });
    }

    @Override
    public void onContentLiked(Content content, String likeStatus, String likeCount) {
        setLikeInfo();
    }

    @Override
    public void onContentCommented(Content content, String commentCount, int pos) {
        mCommentCount.setText(commentCount);
        //mHomeFragmentRecyclerAdapter.notifyItemChanged(pos);
    }

    @Override
    public void onContentCommented(SuggestionModel content, String commentCount, int pos) {

    }

    public void setUpSimilarContent(JSONObject response) {
        if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_COURSE)
            ((TextView) findViewById(R.id.similar_content_title)).setText(R.string.similar_courses);
        else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_ARTICLE)
            ((TextView) findViewById(R.id.similar_content_title)).setText(R.string.similar_articles);
        else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_VIDEO)
            ((TextView) findViewById(R.id.similar_content_title)).setText(R.string.similar_videos);
        else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_INFOGRAPHIC)
            ((TextView) findViewById(R.id.similar_content_title)).setText(R.string.similar_infographics);
        else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_PODCAST)
            ((TextView) findViewById(R.id.similar_content_title)).setText(R.string.similar_podcasts);
        else if (Integer.parseInt(mContentType) == Constants.Content.CONTENT_TYPE_EBOOK)
            ((TextView) findViewById(R.id.similar_content_title)).setText(R.string.similar_ebooks);

        similarContentPageNo = 1;
        similarContentLoading = true;
        doSimilarContentPagination = true;
        initSimilarContentList();
        initSimilarContentRecyclerView();

        if (mContentStatusType.equals(Content.CONTENT_STATUS_TYPE_LIVE)) {
            fetchSimilarLiveContent(response);
        } else {
            fetchSimilarContent();
        }
    }

    private void fetchSimilarLiveContent(JSONObject response) {
        similarContents.remove(similarContents.size() - 1);
        similarContentAdapter.notifyItemRemoved(similarContents.size() - 1);

        if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_ARTICLE)))
            getSimilarMediumData(response);
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_VIDEO)))
            getSimilarYoutubeData(response);
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_PODCAST)))
            getSimilarListenNotesData(response);
    }

    private void getSimilarListenNotesData(JSONObject response) {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_PODCAST_SIMILAR);
            jsonObject.put(Constants.Network.TITLE, mContent.getTitle().split(" ")[(int) (Math.random() * mContent.getTitle().split(" ").length)]);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.PODCAST_LISTENNOTES_PRODUCT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //Adding jsonArray data into a list of jsonObjects
//                                    similarContents.remove(similarContents.size() - 1);
//                                    similarContentAdapter.notifyItemRemoved(similarContents.size() - 1);

                                    JSONArray contentArray = response.getJSONArray(Constants.Network.DATA);

                                    for (int i = 0; i < contentArray.length(); i++) {
                                        JSONObject jsonObject1 = contentArray.getJSONObject(i);

                                        Content content = new Content();
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID_S));
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                        content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));

                                        if (jsonObject1.has(Constants.Network.THUMNAIL)) {
                                            content.setThumnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        }
                                        content.setContentType(mContentType);
                                        content.setStatusType(jsonObject1.getString(Constants.Network.CONTENT_STATUS_TYPE));

                                        similarContents.add(content);
                                    }

                                    similarContentAdapter.notifyItemRangeInserted(similarContents.size() - contentArray.length(), contentArray.length());
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {

                                    doSimilarContentPagination = false;

                                    if (similarContentPageNo == 1) {
                                        hideSimilarContentLayout();
                                    } else {
                                        //no more similar contents
                                    }

                                    similarContents.remove(similarContents.size() - 1);
                                    similarContentAdapter.notifyItemRemoved(similarContents.size() - 1);
                                }

                                similarContentLoading = true;

                            } catch (JSONException e) {
                                Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchSimilarContent", Error.ERROR_TYPE_NORMAL_ERROR, e.getMessage());
                                hideSimilarContentLayout();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchSimilarContent", Error.ERROR_TYPE_NORMAL_ERROR, error.getMessage());
                            hideSimilarContentLayout();
                        }
                    });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchSimilarContent", Error.ERROR_TYPE_NORMAL_ERROR, e.getMessage());
            hideSimilarContentLayout();
        }
    }

    private void getSimilarYoutubeData(JSONObject response) {
        try {
            JSONArray titleArray = response.getJSONObject(Constants.Network.SIMILAR).getJSONArray(Constants.Network.TITLE);
            JSONArray descriptionArray = response.getJSONObject(Constants.Network.SIMILAR).getJSONArray(Constants.Network.DESCRIPTION);
            JSONArray imageArray = response.getJSONObject(Constants.Network.SIMILAR).getJSONArray(Constants.Network.IMAGE);
            JSONArray idArray = response.getJSONObject(Constants.Network.SIMILAR).getJSONArray(Constants.Network.VIDEOID);

            for (int i = 0; i < titleArray.length(); i++) {

                Content content = new Content();
                content.setContentId(idArray.getString(i));
                content.setTitle(titleArray.getString(i));
                content.setDescription(descriptionArray.getString(i));
                content.setThumnail(imageArray.getString(i));
                content.setContentType(mContentType);
                content.setStatusType(mContentStatusType);

                similarContents.add(content);
            }
            similarContentAdapter.notifyItemRangeInserted(similarContents.size() - titleArray.length(), titleArray.length());
        } catch (Exception e) {
            Logger.error(TAG, e.getMessage());
        }
    }

    private void getSimilarMediumData(JSONObject response) {
        try {
            JSONArray contentArray = response.getJSONArray(Constants.Network.SIMILAR);

            for (int i = 0; i < contentArray.length(); i++) {

                JSONObject jsonObject1 = contentArray.getJSONObject(i);
                Content content = new Content();
                content.setContentId(jsonObject1.getString(Constants.Network.LINK));
                content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));
                content.setThumnail(jsonObject1.getString(Constants.Network.IMAGESRC));
                content.setContentType(mContentType);
                content.setStatusType(mContentStatusType);

                similarContents.add(content);
            }

            similarContentAdapter.notifyItemRangeInserted(similarContents.size() - contentArray.length(), contentArray.length());

        } catch (Exception e) {
            Logger.error(TAG, e.getMessage());
        }
    }

    private void setSimilarContentRecyclerViewScrollListener() {
        similarContentRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doSimilarContentPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = similarContentLinearLayoutManager.getChildCount();
                    totalItemCount = similarContentLinearLayoutManager.getItemCount();
                    pastVisiblesItems = similarContentLinearLayoutManager.findFirstVisibleItemPosition();

                    if (similarContentLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            similarContentLoading = false;
                            addProgressBarInSimilarContentList();
                            similarContentAdapter.notifyItemInserted(similarContents.size() - 1);
                            similarContentPageNo++;
                            fetchSimilarContent();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void initSimilarContentRecyclerView() {

        similarContentRecyclerView = findViewById(R.id.activity_product_similar_recycler_view);

        similarContentLinearLayoutManager = new LinearLayoutManager(ProductActivity.this);
        similarContentRecyclerView.setLayoutManager(similarContentLinearLayoutManager);

        similarContentAdapter = new SimilarContentAdapter(ProductActivity.this, similarContents);
        similarContentRecyclerView.setAdapter(similarContentAdapter);
        ViewCompat.setNestedScrollingEnabled(similarContentRecyclerView, false);
    }

    private void initSimilarContentList() {
        similarContents = new ArrayList<>();
        addProgressBarInSimilarContentList();
    }

    private void addProgressBarInSimilarContentList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType("0");
        similarContents.add(progressBarContent);
    }

    public void fetchSimilarContent() {
        similarContentLoading = false;
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_SIMILAR_CONTENT);
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.CATEGORY_ID, mContent.getParentCatId());
            jsonObject.put(Constants.Network.TITLE, mContent.getTitle());
            jsonObject.put(Constants.Network.PAGE_NUMBER, similarContentPageNo);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //Adding jsonArray data into a list of jsonObjects
                                    similarContents.remove(similarContents.size() - 1);
                                    similarContentAdapter.notifyItemRemoved(similarContents.size() - 1);

                                    JSONArray contentArray = response.getJSONArray(Constants.Network.DATA);

                                    for (int i = 0; i < contentArray.length(); i++) {
                                        JSONObject jsonObject1 = contentArray.getJSONObject(i);

                                        Content content = new Content();
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                        if (jsonObject1.has(Constants.Network.THUMNAIL)) {
                                            content.setThumnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        }
                                        content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));
                                        content.setContentType(mContentType);
                                        content.setStatusType(jsonObject1.getString(Constants.Network.CONTENT_STATUS_TYPE));

                                        similarContents.add(content);
                                    }

                                    similarContentAdapter.notifyItemRangeInserted(similarContents.size() - contentArray.length(), contentArray.length());
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ProductActivity.this);
                                } else {

                                    doSimilarContentPagination = false;

                                    if (similarContentPageNo == 1) {
                                        hideSimilarContentLayout();
                                    } else {
                                        //no more similar contents
                                    }

                                    similarContents.remove(similarContents.size() - 1);
                                    similarContentAdapter.notifyItemRemoved(similarContents.size() - 1);
                                }

                                similarContentLoading = true;

                            } catch (JSONException e) {
                                Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "fetchSimilarContent", Error.ERROR_TYPE_NORMAL_ERROR, e.getMessage());
                                hideSimilarContentLayout();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "fetchSimilarContent", Error.ERROR_TYPE_NORMAL_ERROR, error.getMessage());
                            hideSimilarContentLayout();
                        }
                    });


            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ProductActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "fetchSimilarContent", Error.ERROR_TYPE_NORMAL_ERROR, e.getMessage());
            hideSimilarContentLayout();
        }
    }

    private void hideSimilarContentLayout() {
        findViewById(R.id.activity_product_similar_content_layout).setVisibility(View.GONE);
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(ProductActivity.this, R.id.activity_product_root_layout, error);
    }

    @Override
    public void onRefresh() {
    }

    @Override
    protected void onPause() {
        paused = true;
        customHandler.removeCallbacks(updateTimerThread);
        if (mExoplayer != null) {
            mExoplayer.setPlayWhenReady(false);
        }
        if (mContent != null) {
            articleContentDataString = mContent.getData();
        }
        mArticleView.loadData("", "text/html; charset=utf-8", "UTF-8");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        sendPostConsumedRequest();
        if (mExoplayer != null) {
            mExoplayer.stop();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FULLSCREEN_ACT_REQ_CODE && resultCode == RESULT_OK) {
            if (mExoplayer != null && data != null) {
                mExoplayer.seekTo(data.getLongExtra("time", mExoplayer.getCurrentPosition()));
            }
        }
    }
}