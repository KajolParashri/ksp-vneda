package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.AttachmentRecyclerAdapter;
import com.indiareads.ksp.adapters.MembersRecyclerAdapter;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.model.Attachment;
import com.indiareads.ksp.model.Conversation;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.BlurImage;
import com.indiareads.ksp.utils.CircleTransform;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChatInfoActivity extends AppCompatActivity implements OnRefreshListener {

    private static final String TAG = ChatInfoActivity.class.getSimpleName();

    private LinearLayoutManager mAttachmentsLinearLayoutManager;
    private AttachmentRecyclerAdapter mAttachmentsRecyclerAdapter;
    private RecyclerView mAttachmentsRecyclerView;

    private List<Attachment> mAttachments;

    private int pageNumberAttachment = 1;
    private boolean doPaginationAttachment = true;
    private boolean mAttachmentsLoading = true;

    private LinearLayoutManager mMembersLinearLayoutManager;
    private MembersRecyclerAdapter mMembersRecyclerAdapter;
    private RecyclerView mMembersRecyclerView;

    private List<User> mMembers;

    private int pageNumberMember = 1;
    private boolean doPaginationMember = true;
    private boolean mMembersLoading = true;
    Bitmap bitmap1;

    private ImageView profileImageView;
    Target target;
    private String mConversationId;
    private String mConversationName;
    private Conversation mConversation;
    String imageURL;
    ImageView profile_image;
    private int BLUR_PRECENTAGE = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_info);

        getDataFromIntent();
        setupToolbar();
        initViews();
        setTarget();
        initMembersList();
        initAttachmentsList();

        initMembersRecyclerView();
        initAttachmentsRecyclerView();

        setMembersClickListener();
        setAttachmentsClickListener();

        setMembersRecyclerViewScrollListener();
        setAttachmentsRecyclerViewScrollListener();

        getChatInfo();
        fetchMembers();
        fetchAttachments();


    }

    public void setDrawble(Bitmap bitmap) {
        profileImageView.setImageBitmap(bitmap);
    }

    public void setTarget() {
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                bitmap1 = bitmap;
                new LoaderImage().execute();
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        profileImageView.setTag(target);

    }

    public void setImageBlur(String imageURL) {


        if (mConversation.getChatType().equals(Conversation.CHAT_TYPE_GROUP))

            Picasso.get().load(imageURL).error(R.mipmap.ic_group)
                    .placeholder(R.mipmap.ic_group)
                    .into(target);

        else {

            Picasso.get().load(imageURL).error(R.mipmap.ic_profile)
                    .placeholder(R.mipmap.ic_profile)
                    .into(target);
        }
    }

    private void getChatInfo() {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONVO_ID, mConversationId);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_USER_CHAT_DETAILS);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    JSONObject data = response.getJSONObject(Constants.Network.DATA);

                                    mConversation.setChatType(data.getString(Constants.Network.CHAT_TYPE));
                                    mConversation.setChatImage(data.getString(Constants.Network.CHAT_IMAGE));

                                    setChatInfo();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ChatInfoActivity.this);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ChatInfoActivity.class.getSimpleName(),
                                        "getChatInfo", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ChatInfoActivity.class.getSimpleName(),
                                    "getChatInfo", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                        }
                    });

            VolleySingleton.getInstance(ChatInfoActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ChatInfoActivity.class.getSimpleName(),
                    "getChatInfo", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

        }
    }

    private void setChatInfo() {


        imageURL = Urls.AWS_PROFILE_IMAGE_PATH + mConversation.getChatImage();

        setImageBlur(imageURL);

        if (mConversation.getChatType().equals(Conversation.CHAT_TYPE_GROUP))

            Picasso.get().load(Urls.AWS_PROFILE_IMAGE_PATH + mConversation.getChatImage()).error(R.mipmap.ic_group)
                    .placeholder(R.mipmap.ic_group)
                    .transform(new CircleTransform())
                    .into(profile_image);

        else {

            Picasso.get().load(Urls.AWS_PROFILE_IMAGE_PATH + mConversation.getChatImage()).error(R.mipmap.ic_profile)
                    .placeholder(R.mipmap.ic_profile)
                    .transform(new CircleTransform())
                    .into(profile_image);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.activity_chat_info_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        ((TextView) findViewById(R.id.activity_chat_info_name)).setText(mConversationName);
    }


    private void getDataFromIntent() {
        mConversationId = getIntent().getStringExtra(Constants.Network.CONVO_ID);
        mConversationName = getIntent().getStringExtra(Constants.Network.USER_FULL_NAME);

        mConversation = new Conversation();
        mConversation.setId(mConversationId);
        mConversation.setChatName(mConversationName);
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(this, R.id.activity_chat_info_root_layout, error);
    }

    private void setMembersClickListener() {
        mMembersRecyclerAdapter.setOnClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                // todo

                Intent userProfileIntent = new Intent(ChatInfoActivity.this, UserDetailsActivity.class);
                userProfileIntent.putExtra(Constants.Network.USER_ID, mMembers.get(position).getUserId());
                userProfileIntent.putExtra(Constants.Network.COMPANY_ID, mMembers.get(position).getCompanyId());
                userProfileIntent.putExtra("intent", "chat");
                userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 0);
                startActivity(userProfileIntent);
            }
        });
    }

    private void setAttachmentsClickListener() {

        mAttachmentsRecyclerAdapter.setOnClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                // todo
                Logger.info(TAG, Urls.AWS_MESSAGE_IMAGE_PATH + mAttachments.get(position).getAttachmentId());
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Urls.AWS_MESSAGE_IMAGE_PATH + mAttachments.get(position).getAttachmentId()));
                    startActivity(browserIntent);
                } catch (Exception e) {
                    Toast.makeText(ChatInfoActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    Logger.info(TAG, e.getMessage());
                }

            }
        });
    }

    private void initMembersList() {
        mMembers = new ArrayList<>();
        addProgressBarInMembersList();
    }

    private void initAttachmentsList() {
        mAttachments = new ArrayList<>();
        addProgressBarInAttachmentList();
    }

    private void initViews() {
        mAttachmentsRecyclerView = findViewById(R.id.activity_chat_info_attachments);
        mMembersRecyclerView = findViewById(R.id.activity_chat_info_members);
        profile_image = findViewById(R.id.profile_image);
        profileImageView = findViewById(R.id.activity_chat_info_image);
    }

    private void setMembersRecyclerViewScrollListener() {
        //Fetching next page's data on reaching bottom
        mMembersRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPaginationMember && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mMembersLinearLayoutManager.getChildCount();
                    totalItemCount = mMembersLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mMembersLinearLayoutManager.findFirstVisibleItemPosition();

                    if (mMembersLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mMembersLoading = false;
                            addProgressBarInMembersList();
                            mMembersRecyclerAdapter.notifyItemInserted(mMembers.size() - 1);
                            pageNumberMember++;
                            fetchMembers();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void setAttachmentsRecyclerViewScrollListener() {
        //Fetching next page's data on reaching bottom
        mAttachmentsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPaginationAttachment && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mAttachmentsLinearLayoutManager.getChildCount();
                    totalItemCount = mAttachmentsLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mAttachmentsLinearLayoutManager.findFirstVisibleItemPosition();

                    if (mAttachmentsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mAttachmentsLoading = false;
                            addProgressBarInAttachmentList();
                            mAttachmentsRecyclerAdapter.notifyItemInserted(mAttachments.size() - 1);
                            pageNumberAttachment++;
                            fetchAttachments();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void fetchMembers() {
        mMembersLoading = false;
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE, pageNumberMember);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONVO_ID, mConversationId);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_MEMBERS);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mMembers.remove(mMembers.size() - 1);

                                    mMembersRecyclerAdapter.notifyItemRemoved(mMembers.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);
                                    Logger.info("members", response.toString());
                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        User user = new User();
                                        user.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                        user.setFullName(jsonObject1.getString(Constants.Network.FULL_NAME));
                                        user.setProfilePic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                        user.setDesignation(jsonObject1.getString(Constants.Network.DESIGNATION));
                                        user.setUserType(User.USER_TYPE_NORMAL);

                                        mMembers.add(user);
                                    }

                                    if (mMembers.size() == 2) {
                                        findViewById(R.id.members_layout).setVisibility(View.GONE);
                                    }

                                    mMembersRecyclerAdapter.notifyItemRangeInserted(mMembers.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ChatInfoActivity.this);
                                } else {

                                    //stop call to pagination in any case
                                    doPaginationMember = false;

                                    //to remove progress bar
                                    mMembers.remove(mMembers.size() - 1);
                                    mMembersRecyclerAdapter.notifyItemRemoved(mMembers.size() - 1);

                                    if (pageNumberMember == 1) {
                                        //show msg no contents
                                        showEmptyViewInMembersList();
                                    } else {
                                        //no more contents
                                    }
                                }

                                mMembersLoading = true;

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ChatInfoActivity.class.getSimpleName(),
                                        "fetchMembers", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ChatInfoActivity.class.getSimpleName(),
                                    "fetchMembers", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(ChatInfoActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ChatInfoActivity.class.getSimpleName(),
                    "fetchMembers", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }


    private void fetchAttachments() {
        mAttachmentsLoading = false;
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE, pageNumberAttachment);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONVO_ID, mConversationId);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_FILES_CHAT);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info("attachment", response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mAttachments.remove(mAttachments.size() - 1);

                                    mAttachmentsRecyclerAdapter.notifyItemRemoved(mAttachments.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Attachment attachment = new Attachment();
                                        attachment.setAttachmentId(jsonObject1.getString(Constants.Network.ATTACHMENT_ID));
                                        attachment.setAttachmentName(jsonObject1.getString(Constants.Network.ATTACHMENT_NAME));
                                        attachment.setFileType(jsonObject1.getString(Constants.Network.FILE_TYPE));

                                        mAttachments.add(attachment);
                                    }

                                    mAttachmentsRecyclerAdapter.notifyItemRangeInserted(mAttachments.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ChatInfoActivity.this);
                                } else {

                                    //stop call to pagination in any case
                                    doPaginationAttachment = false;

                                    //to remove progress bar
                                    mAttachments.remove(mAttachments.size() - 1);
                                    mAttachmentsRecyclerAdapter.notifyItemRemoved(mAttachments.size() - 1);

                                    if (pageNumberAttachment == 1) {
                                        //show msg no contents
                                        showEmptyViewInAttachmentList();
                                    } else {
                                        //no more contents
                                    }
                                }

                                mAttachmentsLoading = true;

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ChatInfoActivity.class.getSimpleName(),
                                        "fetchAttachments", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ChatInfoActivity.class.getSimpleName(),
                                    "fetchAttachments", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(ChatInfoActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ChatInfoActivity.class.getSimpleName(),
                    "fetchAttachments", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void showEmptyViewInMembersList() {
        User emptyUser = new User();
        emptyUser.setProfilePic(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyUser.setFullName(getString(R.string.attachment_list_empty));
        emptyUser.setUserType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mMembers.add(emptyUser);

        mMembersRecyclerView.setLayoutManager(new LinearLayoutManager(ChatInfoActivity.this));
        mMembersRecyclerAdapter.notifyItemInserted(0);
    }

    private void showEmptyViewInAttachmentList() {
        Attachment emptyAttachment = new Attachment();
        emptyAttachment.setAttachmentId(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyAttachment.setAttachmentName(getString(R.string.attachment_list_empty));
        emptyAttachment.setFileType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mAttachments.add(emptyAttachment);

        mAttachmentsRecyclerView.setLayoutManager(new LinearLayoutManager(ChatInfoActivity.this));
        mAttachmentsRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInMembersList() {
        User progressBarUser = new User();
        progressBarUser.setUserType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mMembers.add(progressBarUser);
    }

    private void addProgressBarInAttachmentList() {
        Attachment progressBarAttachment = new Attachment();
        progressBarAttachment.setFileType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mAttachments.add(progressBarAttachment);
    }

    private void initMembersRecyclerView() {
        mMembersLinearLayoutManager = new LinearLayoutManager(ChatInfoActivity.this);
        mMembersRecyclerView.setLayoutManager(mMembersLinearLayoutManager);

        mMembersRecyclerAdapter = new MembersRecyclerAdapter(ChatInfoActivity.this, mMembers);
        mMembersRecyclerView.setAdapter(mMembersRecyclerAdapter);
    }

    private void initAttachmentsRecyclerView() {
        mAttachmentsLinearLayoutManager = new LinearLayoutManager(ChatInfoActivity.this);
        mAttachmentsRecyclerView.setLayoutManager(mAttachmentsLinearLayoutManager);

        mAttachmentsRecyclerAdapter = new AttachmentRecyclerAdapter(ChatInfoActivity.this, mAttachments);
        mAttachmentsRecyclerView.setAdapter(mAttachmentsRecyclerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                ChatInfoActivity.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRefresh() {

    }

    private class LoaderImage extends AsyncTask<String, String, String> {

        private String resp;

        @Override
        protected String doInBackground(String... params) {
            bitmap1 = BlurImage.fastblur(bitmap1, 1f, BLUR_PRECENTAGE);
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            setDrawble(bitmap1);
        }

        @Override
        protected void onPreExecute() {

        }
    }
}
