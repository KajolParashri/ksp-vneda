package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.indiareads.ksp.R;

public class FullscreenVideoActivity extends AppCompatActivity {

    private static final String TAG = FullscreenVideoActivity.class.getSimpleName();
    private SimpleExoPlayer mExoplayer;
    private SimpleExoPlayerView mExoplayerView;
    private String data;
    private long time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_fullscreen_video);
        getIntentData(savedInstanceState);
        setupExoplayer();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("time", mExoplayer.getCurrentPosition());
        outState.putString("data", data);
    }

    private void getIntentData(Bundle saveInstanceState) {
        if (saveInstanceState != null) {
            data = saveInstanceState.getString("data");
            time = saveInstanceState.getLong("time");
        } else {
            data = getIntent().getStringExtra("data");
            time = getIntent().getLongExtra("time", 0);
        }
    }

    private void setupExoplayer() {
        mExoplayerView = findViewById(R.id.activity_fullscreen_exoplayer);
        mExoplayerView.setControllerHideOnTouch(true);
        mExoplayerView.setVisibility(View.VISIBLE);

        RenderersFactory renderersFactory = new DefaultRenderersFactory(getApplicationContext());
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        AdaptiveTrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(trackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();

        mExoplayer = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, loadControl);
        DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(
                TAG,
                null /* listener */,
                DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                true /* allowCrossProtocolRedirects */
        );

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                getApplicationContext(),
                null /* listener */,
                httpDataSourceFactory
        );

        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        Handler mainHandler = new Handler();
        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(data),
                dataSourceFactory,
                extractorsFactory,
                mainHandler,
                null);
        mExoplayer.seekTo(time);
        mExoplayer.prepare(mediaSource);
        mExoplayerView.setPlayer(mExoplayer);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("time", mExoplayer.getCurrentPosition());
        intent.putExtra("data", data);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mExoplayer != null) {
            mExoplayer.setPlayWhenReady(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mExoplayer != null) {
            mExoplayer.stop();
        }
    }
}
