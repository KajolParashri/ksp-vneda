package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.BuildConfig;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;

public class LoginActivity extends FragmentActivity {

    private final String TAG = LoginActivity.this.getClass().getSimpleName();
    CardView supportCard;
    //Login views
    private EditText mEmailEdittext;
    private EditText mPasswordEdittext;
    private Button mLoginSubmitButn;
    CardView forgot_Card;
    RelativeLayout progressBarLay;
    //Login details
    private String mUserEmail;
    private String mUserPassword;
    TextView Login, Support;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        fetchViewsFromXml();  //Views instantiated
        setLoginBtnClickListener();  //Login clickListener set
    }


    //Instantiating views
    public void fetchViewsFromXml() {
        supportCard = findViewById(R.id.supportCard);

        progressBarLay = findViewById(R.id.progressBarLay);
        progressBarLay.setVisibility(View.GONE);
        mEmailEdittext = findViewById(R.id.activity_login_email);
        mPasswordEdittext = findViewById(R.id.activity_login_password);
        mLoginSubmitButn = findViewById(R.id.activity_login_submit);

        forgot_Card = findViewById(R.id.forgot_Card);
        forgot_Card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangePass();
            }
        });
        mEmailEdittext.setText("");
        mPasswordEdittext.setText("");

        Login = findViewById(R.id.login);
        Support = findViewById(R.id.support);

        supportCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SupportActivity.class);
                startActivity(intent);
            }
        });
    }

    //Setting clickListeners
    public void setLoginBtnClickListener() {

        mLoginSubmitButn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Getting data from views
                progressBarLay.setVisibility(View.VISIBLE);
                mUserEmail = mEmailEdittext.getText().toString();
                mUserPassword = mPasswordEdittext.getText().toString();
                //Checking if fields empty or not
                if (validateLoginDetails()) {
                    sendLoginRequest();
                } else {
                    progressBarLay.setVisibility(View.GONE);
                }
            }
        });

    }

    private boolean validateLoginDetails() {

        if (mUserEmail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(mUserEmail).matches()) {

            CommonMethods.displayToast(LoginActivity.this, getString(R.string.enter_valid_email));
            return false;

        } else if (mUserPassword.isEmpty()) {

            CommonMethods.displayToast(LoginActivity.this, getString(R.string.enter_valid_password));
            return false;

        } else
            return true;
    }


    private void sendLoginRequest() {
        try {
            JSONObject jsonObject = new JSONObject();

            //Creating credentials jsonObject
            jsonObject.put(Constants.Network.EMAIL, mUserEmail);
            jsonObject.put(Constants.Network.PASSWORD, mUserPassword);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_LOGIN);

            Logger.error(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectRequest(Request.Method.POST, Urls.AUTHENTICATION,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Logger.error(TAG, response.toString());
                                Logger.error("FeatureIdData", response.toString());
                                if (progressBarLay.getVisibility() == View.VISIBLE) {
                                    progressBarLay.setVisibility(View.GONE);
                                }

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONObject data = response.getJSONArray(Constants.Network.DATA).getJSONObject(0);

                                    User user = new User();
                                    user.setUserId(data.getString(Constants.Network.USER_ID));
                                    user.setUserEmail(data.getString(Constants.Network.USER_EMAIL));
                                    user.setCompanyId(data.getString(Constants.Network.COMPANY_ID));
                                    user.setCompanyName(data.getString(Constants.Network.COMPANY_NAME));
                                    user.setFirstName(data.getString(Constants.Network.FIRST_NAME));
                                    user.setLastName(data.getString(Constants.Network.LAST_NAME));
                                    user.setDesignation(data.getString(Constants.Network.DESIGNATION));
                                    user.setProfilePic(data.getString(Constants.Network.PROFILE_PIC));
                                    user.setIsAdmin(data.getString(Constants.Network.IS_ADMIN));
                                    user.setBearerToken(data.getString(Constants.Network.TOKEN));

                                    user.setCompanyFeaturesId(data.getJSONArray(Constants.Network.COMPANY_FEATURES).getJSONObject(0).getString(Constants.Network.COMPANY_FEATURES_ID));
                                    user.setCompanyFeaturesCompanyId(data.getJSONArray(Constants.Network.COMPANY_FEATURES).getJSONObject(0).getString(Constants.Network.COMPANY_FEATURES_COMPANY_ID));
                                    user.setCompanyFeaturesFeatureId(data.getJSONArray(Constants.Network.COMPANY_FEATURES).getJSONObject(0).getString(Constants.Network.FEATURE_ID));
                                    user.setCompanyFeaturesValue(data.getJSONArray(Constants.Network.COMPANY_FEATURES).getJSONObject(0).getString(Constants.Network.COMPANY_FEATURES_VALUE));
                                    user.setCompanyFeaturesStatus(data.getJSONArray(Constants.Network.COMPANY_FEATURES).getJSONObject(0).getString(Constants.Network.COMPANY_FEATURES_STATUS));

                                    user.setLoadTutorials("true");
                                    User.setAsCurrentUser(LoginActivity.this, user);

                                    displayToast(LoginActivity.this, getString(R.string.login_successful));
                                    sendToHome();

                                } else {
                                    switch (response.getString(Constants.Network.RESPONSE_MESSAGE)) {
                                        case Constants.Network.INCORRECT_PASSWORD:
                                            displayToast(LoginActivity.this, Constants.Network.INCORRECT_PASSWORD_MESSAGE);
                                            break;
                                        case Constants.Network.EMAIL_NOT_REGISTERED:
                                            displayToast(LoginActivity.this, Constants.Network.EMAIL_NOT_REGISTERED_MESSAGE);
                                            break;
                                        case Constants.Network.INACTIVE_USER:
                                            displayToast(LoginActivity.this, Constants.Network.INACTIVE_USER_MESSAGE);
                                            break;
                                        case Constants.Network.SUSPENDED_USER:
                                            displayToast(LoginActivity.this, Constants.Network.SUSPENDED_USER_MESSGAE);
                                            break;
                                        case Constants.Network.SOMETHING_WENT_WRONG:
                                            displayToast(LoginActivity.this, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                                            break;
                                    }
                                    //                                CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                                }
                            } catch (JSONException e) {
                                //    e.printStackTrace();
                                if (progressBarLay.getVisibility() == View.VISIBLE) {
                                    progressBarLay.setVisibility(View.GONE);
                                }
                                displayToast(LoginActivity.this, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                                //                          CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Error occurred
//                            Logger.error(TAG, error.getMessage());
                            if (progressBarLay.getVisibility() == View.VISIBLE) {
                                progressBarLay.setVisibility(View.GONE);
                            }
                            displayToast(LoginActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);

                            //                    CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap();
                            headers.put("origin-source", "app");
                            try {
                                headers.put("access-key", Base64.encodeToString(BuildConfig.APP_PRIVATE_KEY.getBytes(StandardCharsets.UTF_8), Base64.DEFAULT));
                            } catch (Exception e) {
                                Logger.error(TAG, e.getMessage());
                            }
                            return headers;
                        }
                    };
            VolleySingleton.getInstance(LoginActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            //   Logger.error(TAG, e.getMessage());
            if (progressBarLay.getVisibility() == View.VISIBLE) {
                progressBarLay.setVisibility(View.GONE);
            }
            //  CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
            displayToast(LoginActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);
        }
    }


    public void disableTouch() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void enableTouch() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void showChangePass() {

        final BottomSheetDialog myDialog = new BottomSheetDialog(this);
        myDialog.setContentView(R.layout.forgot_pass);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final ProgressBar progress_bar = myDialog.findViewById(R.id.progress_bar);
        progress_bar.setVisibility(View.GONE);
        final EditText newMail = myDialog.findViewById(R.id.newMail);
        Button close = myDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        Button sbmitbtnn = myDialog.findViewById(R.id.sbmitbtnn);
        sbmitbtnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailNew = newMail.getText().toString();

                if (!emailNew.equals("")) {
                    progress_bar.setVisibility(View.VISIBLE);

                    disableTouch();
                    sendForgotPasswordReq(emailNew, progress_bar, myDialog);
                } else {
                    Toast.makeText(LoginActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                }
            }
        });
        myDialog.show();

    }

    private void sendForgotPasswordReq(String mUserEmail, final ProgressBar progressBar, final BottomSheetDialog sheetDialog) {
        try {
            JSONObject jsonObject = new JSONObject();
            //Creating credentials jsonObject
            jsonObject.put(Constants.Network.EMAIL, mUserEmail);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.FORGOT_PASS);

            Logger.error(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectRequest(Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                enableTouch();
                                Logger.error(TAG, response.toString());
                                if (progressBar.getVisibility() == View.VISIBLE) {
                                    progressBar.setVisibility(View.GONE);
                                }

                                String message = response.getString(Constants.Network.RESPONSE_MESSAGE);

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    Toast.makeText(getBaseContext(), "" + message, Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getBaseContext(), "" + message, Toast.LENGTH_LONG).show();
                                }

                                sheetDialog.dismiss();

                            } catch (JSONException e) {
                                sheetDialog.dismiss();
                                enableTouch();
                                e.printStackTrace();
                                if (progressBarLay.getVisibility() == View.VISIBLE) {
                                    progressBarLay.setVisibility(View.GONE);
                                }
                                displayToast(LoginActivity.this, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                                //                          CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Error occurred
                            Logger.error(TAG, error.getMessage());
                            sheetDialog.dismiss();
                            if (progressBarLay.getVisibility() == View.VISIBLE) {
                                progressBarLay.setVisibility(View.GONE);
                            }
                            enableTouch();
                            displayToast(LoginActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);

                            //                    CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                        }
                    });

            VolleySingleton.getInstance(LoginActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            sheetDialog.dismiss();
            Logger.error(TAG, e.getMessage());
            enableTouch();
            if (progressBarLay.getVisibility() == View.VISIBLE) {
                progressBarLay.setVisibility(View.GONE);
            }
            //  CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
            displayToast(LoginActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);
        }
    }

    //Send to Home if login successful
    private void sendToHome() {
        Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(homeIntent);
        finish();
    }

}
