package com.indiareads.ksp.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.MyCompanyViewPagerAdapter;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.model.Company;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.showProgressBar;

public class MyCompanyFragment extends Fragment implements OnRefreshListener {

    private static final String TAG = MyCompanyFragment.class.getSimpleName();
    public static final String PAGE_NO = "page_no";

    private View fragmentRootView;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private ImageView profileImageView;
    private TextView nameTextView;

    private TextView usersCountTextView;
    private TextView contentCreatedTextView;
    private TextView contentCompletedTextView, aboutUS;

    private Company mCompany;

    int pageNo = 0;

    public MyCompanyFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            pageNo = getArguments().getInt(PAGE_NO, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentRootView = inflater.inflate(R.layout.fragment_my_company, container, false);

        fetchViews();
        getCompanyDetails();

        return fragmentRootView;
    }


    private void fetchViews() {

        tabLayout = fragmentRootView.findViewById(R.id.activity_company_tablayout);
        viewPager = fragmentRootView.findViewById(R.id.activity_company_viewpager);
        aboutUS = fragmentRootView.findViewById(R.id.aboutUS);
        profileImageView = fragmentRootView.findViewById(R.id.activity_company_profile_imageview);
        nameTextView = fragmentRootView.findViewById(R.id.activity_company_user_name);
        usersCountTextView = fragmentRootView.findViewById(R.id.activity_company_users_count);
        contentCreatedTextView = fragmentRootView.findViewById(R.id.activity_company_content_created_count);
        contentCompletedTextView = fragmentRootView.findViewById(R.id.activity_company_content_completed_count);
    }

    private void setUpViewPager() {

        Bundle arguPost = new Bundle();
        arguPost.putString(Constants.Network.COMPANY_ID, User.getCurrentUser(getContext()).getCompanyId());
        arguPost.putString(Constants.Network.REQUIRED_CONTENT_TYPE, String.valueOf(Constants.Content.CONTENT_TYPE_POST));

        Bundle argumentsContentFragment = new Bundle();
        argumentsContentFragment.putString("Type", "Company");
        argumentsContentFragment.putString(Constants.Network.SKIP_CONTENT_TYPE, (Constants.Content.CONTENT_TYPE_POST) + "," + (Constants.Content.CONTENT_TYPE_ANNOUNCEMENT));

        Bundle argumentsRepositoryFragment = new Bundle();
        argumentsRepositoryFragment.putString(RepositoryFragment.REPO_TYPE, RepositoryFragment.REPO_TYPE_COMPANY);

        Bundle arguments = new Bundle();
        arguments.putBundle(MyCompanyViewPagerAdapter.ARG_POST_FRAGMENT, arguPost);
        arguments.putBundle(MyCompanyViewPagerAdapter.ARG_CONTENT_FRAGMENT, argumentsContentFragment);
        arguments.putBundle(MyCompanyViewPagerAdapter.CENTRAL_REPOSITORY_FRAGMENT, argumentsRepositoryFragment);

        viewPager.setAdapter(new MyCompanyViewPagerAdapter(getActivity(), getChildFragmentManager(), arguments));
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(pageNo, true);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_layout, null);
            TextView tv = view.findViewById(R.id.text1);
            tv.setText(tabLayout.getTabAt(i).getText());
            tabLayout.getTabAt(i).setCustomView(tv);
        }

    }

    public void handleResult(Fragment frag, int requestCode, int resultCode, Intent data) {
        Logger.info(TAG, "ONUserFragment");
        List<Fragment> frags = frag.getChildFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null) {
                    f.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Logger.info(TAG, "ONUserFragment");
        List<Fragment> frags = getActivity().getSupportFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null)
                    handleResult(f, requestCode, resultCode, data);
            }
        }
    }

    private void getCompanyDetails() {

        //start progress bar fragment
        showProgressBar(fragmentRootView, R.id.fragment_my_company_root_layout);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTIO_GET_COMPANY_DETAILS);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(getActivity()).getCompanyId());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.MYCOMPANY_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Logger.info(TAG, response.toString());

                    try {
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK))
//                            if(response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK))
                        {
                            mCompany = new Company();

                            JSONObject companyObject = response.getJSONObject(Constants.Network.DATA);

                            mCompany.setCompanyLogo(companyObject.getString(Constants.Network.COMPANY_LOGO));
                            mCompany.setCompanyName(companyObject.getString(Constants.Network.COMPANY_NAME));
                            mCompany.setCompanyInfo(companyObject.getString(Constants.Network.COMPANY_INFO));
                            mCompany.setTotalUsers(companyObject.getString(Constants.Network.TOTAL_USERS));
                            mCompany.setTotalContentCreated(companyObject.getString(Constants.Network.TOTAL_CONTENT_CREATED));
                            mCompany.setTotalContentConsumed(companyObject.getString(Constants.Network.TOTAL_CONTENT_CONSUMED));

                            setCompanyDetails();
                            setUpViewPager();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                                    "getCompanyDetails", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);

                            displayErrorUI(error);
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                                "getCompanyDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                        displayErrorUI(error);
                    }
                    CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_my_company_root_layout);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    Error error = Error.generateError(VolleyError.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                            "getCompanyDetails", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                    displayErrorUI(error);
                }
            });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                    "getCompanyDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);
        }
    }

    private void setCompanyDetails() {

        String logoURL = Urls.BaseImgURL + mCompany.getCompanyLogo();
        Picasso.get().load(logoURL).into(profileImageView);
        nameTextView.setText(mCompany.getCompanyName());

        usersCountTextView.setText(mCompany.getTotalUsers());
        contentCreatedTextView.setText(mCompany.getTotalContentCreated());
        contentCompletedTextView.setText(mCompany.getTotalContentConsumed());
        aboutUS.setText(mCompany.getCompanyInfo());

    }

    private void displayErrorUI(Error error) {
        CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_my_company_root_layout);
        CommonMethods.showErrorView(MyCompanyFragment.this, R.id.fragment_my_company_root_layout, error);
    }

    @Override
    public void onRefresh() {
        setUpViewPager();
        getCompanyDetails();
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}
