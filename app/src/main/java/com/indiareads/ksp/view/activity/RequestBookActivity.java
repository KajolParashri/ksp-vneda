package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.SpinnerAdapterContent;
import com.indiareads.ksp.model.ContentSpinnerModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;

public class RequestBookActivity extends AppCompatActivity {
    ArrayList<ContentSpinnerModel> contentType;
    Spinner select_spin;
    Button sbmitbtnn;
    ImageView backBtn;
    EditText isbn, author, title;
    ProgressBar progressBar;
    int pos;
    String content_type = "";
    int posDefault = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_book);
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        select_spin = findViewById(R.id.select_spin);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        setSpinnerData();
        init();
        setAdapter();

        Intent intent = getIntent();
        if (intent.hasExtra("content_type")) {
            content_type = intent.getStringExtra("content_type");
            posDefault = getIndexOf(content_type);
            select_spin.setSelection(posDefault);
        }

        String selected = select_spin.getSelectedItem().toString();
        pos = CommonMethods.getContentTypeFromName(selected);

        sbmitbtnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidation()) {
                    progressBar.setVisibility(View.VISIBLE);
                    sendRequest("" + pos);
                } else {

                    Toast.makeText(RequestBookActivity.this, "Please Enter Title", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public boolean checkValidation() {
        boolean check = true;

        if (title.getText().toString().trim().equals("")) {
            check = false;
        }

        return check;
    }

    public int getIndexOf(String name) {

        for (int i = 0; i < contentType.size(); i++) {

            if (contentType.get(i).getItemName().equals(name)) {

                posDefault = i;
                return posDefault;

            }
        }
        return posDefault;
    }

    public void init() {
        isbn = findViewById(R.id.isbn);
        author = findViewById(R.id.author);
        title = findViewById(R.id.title);

        sbmitbtnn = findViewById(R.id.sbmitbtnn);
    }

    public void setSpinnerData() {

        contentType = new ArrayList<>();

        ContentSpinnerModel article = new ContentSpinnerModel();
        article.setImage(R.drawable.article_explore);
        article.setItemName("Article");
        contentType.add(article);

        ContentSpinnerModel infoGraphics = new ContentSpinnerModel();
        infoGraphics.setImage(R.drawable.info_explore);
        infoGraphics.setItemName("Infographic");
        contentType.add(infoGraphics);

        ContentSpinnerModel video = new ContentSpinnerModel();
        video.setImage(R.drawable.video_explore);
        video.setItemName("Video");
        contentType.add(video);

        ContentSpinnerModel courses = new ContentSpinnerModel();
        courses.setImage(R.drawable.course_explore);
        courses.setItemName("Course");
        contentType.add(courses);

        ContentSpinnerModel podcast = new ContentSpinnerModel();
        podcast.setImage(R.drawable.podcast_expo);
        podcast.setItemName("Podcast");
        contentType.add(podcast);

        ContentSpinnerModel books = new ContentSpinnerModel();
        books.setImage(R.drawable.book_explore);
        books.setItemName("Books");
        contentType.add(books);
    }


    public void setAdapter() {
        SpinnerAdapterContent spinnerAdapterContent = new SpinnerAdapterContent(this, contentType);
        select_spin.setAdapter(spinnerAdapterContent);
    }

    private void sendRequest(String content_type) {

        String Isbn = isbn.getText().toString();
        String Title = title.getText().toString();
        String Author = author.getText().toString();

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.REQUEST_BOOK.TITLE, Title);
            jsonObject.put(Constants.REQUEST_BOOK.AUTHOR, Author);
            jsonObject.put(Constants.REQUEST_BOOK.ISBN, Isbn);
            jsonObject.put(Constants.REQUEST_BOOK.LANGUAGE, "ENGLISH");
            jsonObject.put(Constants.REQUEST_BOOK.EMAIL, User.getCurrentUser(RequestBookActivity.this).getUserEmail());
            jsonObject.put(Constants.REQUEST_BOOK.USRE_ID, User.getCurrentUser(RequestBookActivity.this).getUserId());
            jsonObject.put(Constants.REQUEST_BOOK.CONTENT_TYPE, content_type);
            jsonObject.put(Constants.REQUEST_BOOK.ACTION, "submitdetails_new_book");

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressBar.setVisibility(View.GONE);
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    Toast.makeText(RequestBookActivity.this, "Successfully Requeseted !", Toast.LENGTH_SHORT).show();
                                    finish();

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(RequestBookActivity.this);
                                } else {
                                    progressBar.setVisibility(View.GONE);
                                    String message = response.getString("response_message");

                                    Toast.makeText(RequestBookActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                progressBar.setVisibility(View.GONE);
                                e.printStackTrace();

                                displayToast(RequestBookActivity.this, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                                //                          CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Error occurred
                            progressBar.setVisibility(View.GONE);
                            displayToast(RequestBookActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);

                            //                    CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                        }
                    });

            VolleySingleton.getInstance(RequestBookActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            //  CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
            displayToast(RequestBookActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);
        }
    }

}
