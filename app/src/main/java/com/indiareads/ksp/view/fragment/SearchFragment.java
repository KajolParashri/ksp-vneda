package com.indiareads.ksp.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.SearchActivityViewPagerAdapter;
import com.indiareads.ksp.adapters.SearchFragmentRecyclerAdapter;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.CategoryListingActivity;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements OnRefreshListener {

    public static final String FILTER_ARGUMENTS = "Filter Arguments";
    private static final String TAG = SearchFragment.class.getSimpleName();

    private View mFragmentRootView;
    private LinearLayoutManager mLinearLayoutManager;
    private SearchFragmentRecyclerAdapter mSearchFragmentRecyclerAdapter;
    private RecyclerView mContentsRecyclerView;

    private List<Content> mContents; //Contents list

    private int pageNumber = 1;
    private boolean doPagination = true;
    private boolean mContentsLoading = true;

    private String mSearchQuery;
    private String mContentType;
    private String youtubePageToken = "";

    private JSONObject selectedFilterObject;
    private JSONArray withingCompanyArray;
    private JSONArray subCatArray;
    private JSONArray searchByArray;

    private Bundle filterArguments;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentRootView = inflater.inflate(R.layout.fragment_search, container, false);

        getDataFromArguments();
        initViews();
        initContentList();
        initRecyclerView();
        setRecyclerViewItemClickListener();
        setRecyclerViewScrollListener();
        initialiseFilterArrays();

        return mFragmentRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        fetchData();
    }

    private void initialiseFilterArrays() {
        try {
            withingCompanyArray = new JSONArray();
            subCatArray = new JSONArray();
            searchByArray = new JSONArray();

            ArrayList<String> filterMyCompany = filterArguments.getStringArrayList(SearchActivityFilterFragment.FILTER_MY_COMPANY);
            ArrayList<String> filterCategories = filterArguments.getStringArrayList(SearchActivityFilterFragment.FILTER_CATEGORIES);
            ArrayList<String> filterSearchBy = filterArguments.getStringArrayList(SearchActivityFilterFragment.FILTER_SEARCH_BY);

            for (int i = 0; i < filterMyCompany.size(); i++)
                withingCompanyArray.put(filterMyCompany.get(i));

            for (int i = 0; i < filterCategories.size(); i++)
                subCatArray.put(filterCategories.get(i));

            for (int i = 0; i < filterSearchBy.size(); i++)
                searchByArray.put(filterSearchBy.get(i));

            selectedFilterObject = new JSONObject();
            selectedFilterObject.put(Constants.Network.WITHIN_COMPANY, withingCompanyArray);
            selectedFilterObject.put(Constants.Network.SUB_CAT, subCatArray);
            selectedFilterObject.put(Constants.Network.SEARCH_BY, searchByArray);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                    "initialiseFilterArrays", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(this, R.id.fragment_search_root_layout, error);
    }

    private void getDataFromArguments() {
        mSearchQuery = getArguments().getString(SearchActivityViewPagerAdapter.SEARCH_QUERY);
        mContentType = String.valueOf(getArguments().getInt(Constants.Content.CONTENT_TYPE));
        filterArguments = getArguments().getBundle(FILTER_ARGUMENTS);

        if (filterArguments == null) {
            filterArguments = new Bundle();

            filterArguments.putStringArrayList(SearchActivityFilterFragment.FILTER_MY_COMPANY, new ArrayList<String>());
            filterArguments.putStringArrayList(SearchActivityFilterFragment.FILTER_CATEGORIES, new ArrayList<String>());
            ArrayList<String> searchByArrayList = new ArrayList<>();
            searchByArrayList.add(Constants.Category.CATEGORY_SEARCH_BY_TITLE);
            filterArguments.putStringArrayList(SearchActivityFilterFragment.FILTER_SEARCH_BY, searchByArrayList);
        }
    }

    private void setRecyclerViewItemClickListener() {

        mSearchFragmentRecyclerAdapter.setOnClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent productIntent = new Intent(getContext(), ProductActivity.class);
                productIntent.putExtra(Constants.Network.CONTENT_ID, mContents.get(position).getContentId());
                productIntent.putExtra(Constants.Network.CONTENT_TYPE, mContents.get(position).getContentType());
                productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, mContents.get(position).getStatusType());
                productIntent.putExtra(Constants.Network.SEARCH, mSearchQuery);
                startActivity(productIntent);
            }
        });
    }

    private void initContentList() {
        mContents = new ArrayList<>();
        addProgressBarInList();
    }

    private void initViews() {
        mContentsRecyclerView = mFragmentRootView.findViewById(R.id.fragment_search_recyclerview);
    }

    private void setRecyclerViewScrollListener() {
        //Fetching next page's data on reaching bottom
        mContentsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mLinearLayoutManager.getChildCount();
                    totalItemCount = mLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                    if (mContentsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mContentsLoading = false;
                            addProgressBarInList();
                            mSearchFragmentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);
                            pageNumber++;
                            fetchData();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void fetchData() {
        mContentsLoading = false;
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.SEARCH_MAIN);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(getContext()).getCompanyId());
            jsonObject.put(Constants.Network.PAGE, pageNumber);
            jsonObject.put(Constants.Network.YOUTUBE_PAGE_TOKEN, youtubePageToken);
            jsonObject.put(Constants.Network.SELECTED_FILTERS, selectedFilterObject);
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);
            jsonObject.put(Constants.Network.KEY, mSearchQuery);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.SEARCH_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mContents.remove(mContents.size() - 1);

                                    mSearchFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Content content = new Content();
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                        content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID_S));
                                        content.setContentType(jsonObject1.getString(Constants.Network.CONTENT_TYPE));
                                        content.setUserIdSource(jsonObject1.getString(Constants.Network.USER_ID_SOURCE));
                                        content.setThumbnail(jsonObject1.getString(Constants.Network.THUMBNAIL));
                                        content.setSourceName(jsonObject1.getString(Constants.Network.SOURCE_NAME));
                                        content.setAuthorName(jsonObject1.getString(Constants.AUTHOR));
                                        content.setUserSourceType(jsonObject1.getString(Constants.Network.USER_SOURCE_TYPE));
                                        content.setStatusType(jsonObject1.getString(Constants.Network.CONTENT_STATUS_TYPE));
                                        content.setRating(jsonObject1.getString(Constants.Network.RATING));
                                        mContents.add(content);

                                        if (jsonObject1.has(Constants.Network.PAGE_TOKEN))
                                            youtubePageToken = jsonObject1.getString(Constants.Network.PAGE_TOKEN);
                                    }
                                    mSearchFragmentRecyclerAdapter.notifyItemRangeInserted(mContents.size() - jsonArray.length(), jsonArray.length());
                                    if (mContents.size() < 5) {
                                        mContentsLoading = false;
                                        addProgressBarInList();
                                        mSearchFragmentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);
                                        pageNumber++;
                                        fetchData();
                                    }
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    doPagination = false;
                                    mContents.remove(mContents.size() - 1);
                                    mSearchFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                                    if (pageNumber == 1) {
                                        //show msg no contents
                                        showEmptyView();
                                    } else {
                                        //no more contents
                                    }
                                }
                                mContentsLoading = true;
                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), SearchFragment.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), SearchFragment.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), SearchFragment.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void showEmptyView() {
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyContent.setTitle(this.getString(R.string.content_list_empty));
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mContents.add(emptyContent);

        mContentsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mSearchFragmentRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mContents.add(progressBarContent);
    }

    private void initRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mContentsRecyclerView.setLayoutManager(mLinearLayoutManager);
        mSearchFragmentRecyclerAdapter = new SearchFragmentRecyclerAdapter(getActivity(), mContents);
        mContentsRecyclerView.setAdapter(mSearchFragmentRecyclerAdapter);
    }

    @Override
    public void onRefresh() {
        getDataFromArguments();
        initContentList();
        fetchData();
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}
