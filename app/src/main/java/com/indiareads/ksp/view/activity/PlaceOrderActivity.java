package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Address;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;
import static com.indiareads.ksp.utils.CommonMethods.loadNormalImageWithGlide;
import static com.indiareads.ksp.utils.Constants.Network.ACTION;
import static com.indiareads.ksp.utils.Constants.Network.ADDRESS_BOOK_ID;
import static com.indiareads.ksp.utils.Constants.Network.CONTENT_ID;
import static com.indiareads.ksp.utils.Urls.baseAWSUrl;

public class PlaceOrderActivity extends AppCompatActivity {

    private String mData;
    private String mContentId;
    private String mAddressBookId;
    ImageView delt_img;
    TextView titleDummy;
    private LinearLayout mOrderPlaceLinearLayout;
    private TextView fullName, addressLine1, addressLine2, city, phone;
    private TextView bookName, bookAuthor;
    private ImageView bookImage;
    TextView newOrder, returnOrder;
    private TextView mOrderId, mOrderDate, continueBrowsing;
    String messageLay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_place_order);
        getIntentData();
        initViews();

        getThankYouMessage(mAddressBookId);
        setOrderDetails();
        setOnClickListern();
    }

    public void setOnClickListern() {

        continueBrowsing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlaceOrderActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(PlaceOrderActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void initViews() {
        continueBrowsing = findViewById(R.id.continueBrowsing);
        mOrderPlaceLinearLayout = findViewById(R.id.OrderPlaceSuccessLinearLayout);
        titleDummy = findViewById(R.id.titleDummy);

        newOrder = findViewById(R.id.newOrder);
        returnOrder = findViewById(R.id.returnOrder);

        delt_img = findViewById(R.id.delt_img);
        delt_img.setVisibility(View.GONE);

        fullName = findViewById(R.id.address_item_fullname);
        addressLine1 = findViewById(R.id.address_item_line1);
        addressLine2 = findViewById(R.id.address_item_line2);
        city = findViewById(R.id.address_item_city);
        phone = findViewById(R.id.address_item_phone);

        mOrderId = findViewById(R.id.order_id);
        mOrderDate = findViewById(R.id.order_date);

        bookImage = findViewById(R.id.book_image);
        bookName = findViewById(R.id.book_name);
        bookAuthor = findViewById(R.id.book_author);

        if (messageLay.equals("return")) {

            newOrder.setVisibility(View.GONE);
            returnOrder.setVisibility(View.VISIBLE);
            titleDummy.setVisibility(View.GONE);

        } else {

            newOrder.setVisibility(View.VISIBLE);
            returnOrder.setVisibility(View.GONE);
            titleDummy.setVisibility(View.VISIBLE);
        }
    }

    private void setAddressData(Address addressData) {
        fullName.setText(addressData.fullname);
        addressLine1.setText(addressData.addressLine1);
        if (!addressData.addressLine2.isEmpty())
            addressLine2.setText(addressData.addressLine2);
        else
            addressLine2.setVisibility(View.GONE);
        city.setText(addressData.city + ", " + addressData.state + " - " + addressData.pincode);
        phone.setText("Mobile: " + addressData.phone);
    }

    private void setOrderDetails() {
        mOrderId.setText("Order ID: " + mData);
        try {
            String timeStamp = new SimpleDateFormat("dd/MM/YYYY").format(Calendar.getInstance().getTime());
            mOrderDate.setText("Order Date: " + timeStamp);
            Logger.info("PLACEORDER", "" + "Internal Date Set");
        } catch (Exception e) {
            Logger.info("PLACEORDER", "" + "Date Error");

        }
    }

    private void getIntentData() {
        mContentId = getIntent().getStringExtra("content_id");
        mData = getIntent().getStringExtra("data");

        messageLay = getIntent().getStringExtra("messageLay");
        mAddressBookId = getIntent().getStringExtra("address_book_id");
        Logger.info("PLACEORDER", " GetIntent");
    }

    private void getThankYouMessage(String data) {
        CommonMethods.showProgressBar(this, R.id.place_order_root_layout);
        try {
            JSONObject mJsonObject = new JSONObject();
            mJsonObject.put(ACTION, "get_address_and_content");
            mJsonObject.put(CONTENT_ID, mContentId);
            mJsonObject.put(ADDRESS_BOOK_ID, data);
            mJsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(PlaceOrderActivity.this).getUserId());

            Logger.info("PLACE_ORDER", "" + mJsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.ORDER_PLACE,
                    mJsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.info("PLACE_ORDER", "" + response.toString());
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                            Address address = new Address();
                            JSONObject jsonObject1 = response.getJSONObject("data");
                            JSONArray jsonArray = jsonObject1.getJSONArray("address");
                            JSONObject addressJson = jsonArray.getJSONObject(0);
                            address.fullname = addressJson.getString("fullname");
                            address.addressBookId = addressJson.getString("address_book_id");
                            address.type = addressJson.getString("address_type");
                            address.addressLine1 = addressJson.getString("address_line1");
                            address.addressLine2 = addressJson.getString("address_line2");
                            address.city = addressJson.getString("city");
                            address.state = addressJson.getString("state");
                            address.pincode = addressJson.getString("pincode");
                            address.phone = addressJson.getString("phone");
                            setAddressData(address);

                            JSONArray jsonArray1 = jsonObject1.getJSONArray("content");
                            JSONObject contentJson = jsonArray1.getJSONObject(0);
                            if (!contentJson.getString("contributor_name1").isEmpty()) {
                                bookAuthor.setText("Author: " + contentJson.getString("contributor_name1"));
                            } else bookAuthor.setVisibility(View.GONE);
                            bookName.setText(contentJson.getString("title"));

                            String url = contentJson.getString("thumnail");
                            if (!url.equals("") || !url.equals("0")) {
                                loadNormalImageWithGlide(PlaceOrderActivity.this,
                                        baseAWSUrl + "/book_i/" + url, bookImage);
                            }

                            CommonMethods.hideProgressBar(PlaceOrderActivity.this, R.id.place_order_root_layout);
                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(PlaceOrderActivity.this);
                        } else {
                            Logger.info("PLACE_ORDER", "" + "Error Else");
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                                    "getThankYouMessage", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));
                            CommonMethods.hideProgressBar(PlaceOrderActivity.this, R.id.place_order_root_layout);
                            displayErrorUI(error);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Logger.info("PLACE_ORDER", "" + "Error Catch");
                        Error error = Error.generateError(JSONException.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                                "getThankYouMessage", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        CommonMethods.hideProgressBar(PlaceOrderActivity.this, R.id.place_order_root_layout);
                        displayErrorUI(error);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.info("PLACE_ORDER", "" + "Server Error");
                    Error error1 = Error.generateError(VolleyError.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                            "getThankYouMessage", Error.getErrorTypeFromVolleyError(error), error.getMessage());
                    displayToast(PlaceOrderActivity.this, "Error contacting servers");
                    CommonMethods.hideProgressBar(PlaceOrderActivity.this, R.id.place_order_root_layout);
                    displayErrorUI(error1);

                }
            });
            VolleySingleton.getInstance(PlaceOrderActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.info("PLACE_ORDER", "" + "Server Error Major");
            e.printStackTrace();
            Error error1 = Error.generateError(VolleyError.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                    "getThankYouMessage", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            CommonMethods.hideProgressBar(PlaceOrderActivity.this, R.id.place_order_root_layout);
            displayErrorUI(error1);
        }
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(PlaceOrderActivity.this, R.id.place_order_root_layout, error);
    }

}
