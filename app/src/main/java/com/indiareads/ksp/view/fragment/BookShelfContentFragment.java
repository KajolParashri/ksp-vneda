package com.indiareads.ksp.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.BookShelfRecyclerAdapter;
import com.indiareads.ksp.listeners.OnOrderClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SharedPreferencesHelper;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.SelectAddressActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;
import static com.indiareads.ksp.utils.Constants.Network.GET_BOOKSHELF_CONTENT;
import static com.indiareads.ksp.utils.Constants.RETURN_BOOK_ADDRESS_CODE;
import static com.indiareads.ksp.utils.Constants.RETURN_ORDER_SELECT_ADDRESS;

public class BookShelfContentFragment extends Fragment {

    private static final String TAG = BookShelfContentFragment.class.getSimpleName();

    private View mFragmentRootView; //Root view
    private LinearLayoutManager linearLayoutManager;
    private BookShelfRecyclerAdapter mContentFragmentRecyclerAdapter;
    private RecyclerView mContentsRecyclerView;

    private List<Content> mContents; //Contents list
    private String mContentType;

    public BookShelfContentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getDataFromIntent();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int position = data.getIntExtra("position", -1);
        if (resultCode == RETURN_BOOK_ADDRESS_CODE && position != -1) {

            String addressBookId = data.getStringExtra("address_book_id");
            returnOrder(addressBookId, position);
        }
    }

    private void getDataFromIntent() {

        if (getArguments() != null)
            mContentType = getArguments().getString(Constants.Network.SHELF_TYPE);
        else
            showErrMsg();
    }

    private void showErrMsg() {
        Logger.error(TAG, "ERROR");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentRootView = inflater.inflate(R.layout.fragment_reading_shelf_content, container, false);

        initViews();
        initContentList();
        initRecyclerView();
        fetchData();

        return mFragmentRootView;
    }

    private void initContentList() {
        mContents = new ArrayList<>();
        addProgressBarInList();
    }

    private void initViews() {

        mContentsRecyclerView = mFragmentRootView.findViewById(R.id.reading_shelf_content_fragment_recycler_view);
    }

    private void fetchData() {
        try {
            final JSONObject jsonObject = new JSONObject();
            String mUserId = User.getCurrentUser(getActivity()).getUserId();

            jsonObject.put(Constants.Network.USER_ID, mUserId);
            jsonObject.put(Constants.Network.SHELF_TYPE, mContentType);
            jsonObject.put(Constants.Network.ACTION, GET_BOOKSHELF_CONTENT);

            Logger.error(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.bookShelfUrl,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.error(TAG, response.toString());
                            try {
                                if (response.getString(Constants.Network.RESPONSE_MESSAGE).equalsIgnoreCase("Content fetched Successfully !")) {

                                    mContents.remove(mContents.size() - 1);
                                    mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        Content content = new Content();
                                        content.setContentType(jsonObject1.getString(Constants.Network.CONTENT_TYPE));
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                        content.setReturnBtnText("Return");
                                        if (jsonObject1.has(Constants.Network.ORDER_STATUS)) {
                                            content.setOrderStatus(jsonObject1.getString(Constants.Network.ORDER_STATUS));
                                        }
                                        if (jsonObject1.has(Constants.Order.ORDER_ID)) {
                                            content.setOrder_id(jsonObject1.getString(Constants.Order.ORDER_ID));
                                        }
                                        if (jsonObject1.has(Constants.Network.SHELF_TYPE)) {
                                            content.setBookShelfType(jsonObject1.getString(Constants.Network.SHELF_TYPE));
                                        }
                                        if (jsonObject1.has(Constants.Network.SOURCE_NAME)) {
                                            content.setSourceName(jsonObject1.getString(Constants.Network.SOURCE_NAME));
                                            content.setAuthorName(jsonObject1.getString(Constants.AUTHOR));
                                        }
                                        content.setThumbnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        if (jsonObject1.has(Constants.Network.RATING)) {
                                            content.setRating(jsonObject1.getString(Constants.Network.RATING));
                                        }
                                        content.setStatusType(jsonObject1.getString(Constants.Network.STATUS));
                                        mContents.add(content);
                                    }
                                    mContentFragmentRecyclerAdapter.notifyItemRangeInserted(mContents.size() - jsonArray.length(), jsonArray.length());
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    mContents.remove(mContents.size() - 1);
                                    mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                                }
                            } catch (JSONException e) {
                                Logger.error(TAG, "In OnResponse" + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //to remove progress bar
                            mContents.remove(mContents.size() - 1);
                            mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                            if (error instanceof NetworkError)
                                showEmptyView(R.drawable.ic_signal_wifi_off, getString(R.string.connection_error_message));
                            else
                                showEmptyView(R.drawable.ic_error_outline, getString(R.string.something_went_wrong));
                            Logger.error(TAG, error.getMessage() + " is the volley error ");
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Logger.error(TAG, "In fetchData" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void returnOrder(String addressBookId, final int pos) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("action", "pickuprequest");
            jsonObject.put("user_id", SharedPreferencesHelper.getCurrentUser(getContext()).getUserId());
            jsonObject.put("address_book_id", addressBookId);
            jsonObject.put("order_id", mContents.get(pos).getOrder_id());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.ORDER_PLACE,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getString(Constants.Network.RESPONSE_CODE)
                                .equals(Constants.Network.RESPONSE_OK)) {
                            displayToast(getContext(), "Pickup requested");

                            Content content = mContents.get(pos);
                            content.setReturnBtnText("Pickup Requested");
                            mContents.set(pos, content);
                            mContentFragmentRecyclerAdapter.notifyItemChanged(pos);

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        displayToast(getContext(), "Unexpected error occurred, Please try again later.");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    displayToast(getContext(), "Unexpected error occurred, Please try again later.");
                }
            });

            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
            displayToast(getContext(), "Unexpected error occurred, Please try again later.");
        }
    }

    private void showEmptyView(int imageId, String msg) {
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(imageId));
        emptyContent.setTitle(msg);
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mContents.add(emptyContent);

        mContentsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mContentFragmentRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mContents.add(progressBarContent);
    }

    private void initRecyclerView() {

        OnOrderClickListener mListener = new OnOrderClickListener() {
            @Override
            public void OnReturnClick(int position) {
                Content content = mContents.get(position);
                Intent orderIntent = new Intent(getActivity(), SelectAddressActivity.class);
                orderIntent.putExtra("content_id", content.getContentId());
                orderIntent.putExtra("isbn_no", "");
                orderIntent.putExtra("type", RETURN_ORDER_SELECT_ADDRESS);
                orderIntent.putExtra("position", position); //Do not change
                startActivityForResult(orderIntent, RETURN_BOOK_ADDRESS_CODE);
            }

            @Override
            public void OnCancelClick(int position) {

            }
        };

        linearLayoutManager = new LinearLayoutManager(getActivity());
        mContentsRecyclerView.setLayoutManager(linearLayoutManager);
        mContentFragmentRecyclerAdapter = new BookShelfRecyclerAdapter(getActivity(), mContents, mListener);
        mContentsRecyclerView.setAdapter(mContentFragmentRecyclerAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}
