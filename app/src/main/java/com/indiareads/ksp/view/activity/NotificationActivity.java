package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.NotificationAdapter;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.NotificationModel;
import com.indiareads.ksp.model.SuggestionModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;

public class NotificationActivity extends AppCompatActivity {

    private static final String TAG = NotificationActivity.class.getSimpleName();
    RecyclerView listNoti;
    NotificationAdapter notificationAdapter;
    ArrayList<NotificationModel> arrayList;
    ArrayList<NotificationModel> parentArrayList;
    ImageView back;
    LinearLayoutManager manager;
    ShimmerFrameLayout shimmerFrameLayout;
    private int pageNumber = 1;
    private boolean doPagination = true;
    private boolean mContentsLoading = true;
    int totalData = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        initViews();
        CommonMethods.showProgressBar(NotificationActivity.this, R.id.rootLayoutNoti);
        getNotification();
    }

    public void initViews() {

        shimmerFrameLayout = findViewById(R.id.shimmer_view_container);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        listNoti = findViewById(R.id.listNoti);
        manager = new LinearLayoutManager(this);
        listNoti.setLayoutManager(manager);
//        setRecyclerViewScrollListener(manager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider_xml));
        listNoti.addItemDecoration(itemDecorator);

        parentArrayList = new ArrayList<>();
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmerAnimation();
        listNoti.setVisibility(View.GONE);
    }

    public void setAdapter() {

        notificationAdapter = new NotificationAdapter(NotificationActivity.this, parentArrayList, new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                if (parentArrayList.get(position).getEntityType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_POST))) {
                    Intent userProfileIntent = new Intent(NotificationActivity.this, UserDetailsActivity.class);
                    userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 0);
                    userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(NotificationActivity.this).getUserId());
                    userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(NotificationActivity.this).getCompanyId());
                    startActivity(userProfileIntent);
                } else if (parentArrayList.get(position).getEntityType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_SUGGESTION))) {
                    Intent productIntent = new Intent(NotificationActivity.this, SuggestionBoardActi.class);

                    productIntent.putExtra(SuggestionModel.SUGGESTION_ID, parentArrayList.get(position).getEntityId());
                    Logger.info("intentNotification", productIntent.toString());
                    startActivity(productIntent);
                } else {

                    Intent productIntent = new Intent(NotificationActivity.this, ProductActivity.class);
                    productIntent.putExtra(Constants.Network.CONTENT_ID, parentArrayList.get(position).getEntityId());
                    productIntent.putExtra(Constants.Network.CONTENT_TYPE, parentArrayList.get(position).getEntityType());
                    productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);

                    Logger.info("intentNotificationPage", productIntent.getStringExtra(Constants.Network.CONTENT_ID));
                    Logger.info("intentNotificationId", productIntent.getStringExtra(Constants.Network.CONTENT_TYPE));
                    Logger.info("intentNotificationCompid", productIntent.getStringExtra(Constants.Network.CONTENT_STATUS_TYPE));

                    startActivity(productIntent);
                }
            }
        });

        listNoti.setAdapter(notificationAdapter);
    }

    public void addProgress() {
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setItemType("Progress");
        arrayList.add(notificationModel);
        notificationAdapter.notifyItemInserted(arrayList.size() - 1);
    }

    public void removeProgress() {
        arrayList.remove(arrayList.size() - 1);
        notificationAdapter.notifyItemRemoved(arrayList.size() - 1);
    }

    public void getNotification() {

        parentArrayList = new ArrayList<>();
        arrayList = new ArrayList<>();

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, "get_notification");
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.EXTRA_API,
                    jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Logger.info(TAG, response.toString());

                    try {

                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            JSONArray data = response.getJSONArray(Constants.Network.DATA);

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject jsonObjectData = data.getJSONObject(i);
                                String user_id = jsonObjectData.getString("user_id");
                                String status = jsonObjectData.getString("status");
                                String created_date = jsonObjectData.getString("created_date");
                                String entity_value = jsonObjectData.getString("entity_value");
                                String types = jsonObjectData.getString("types");
                                JSONArray user_detail = jsonObjectData.getJSONArray("user_detail");
                                JSONObject user_detailOb = user_detail.getJSONObject(0);
                                String full_name = user_detailOb.getString("full_name");
                                String profile_pic = user_detailOb.getString("profile_pic");

                                NotificationModel notificationModel = new NotificationModel();
                                notificationModel.setUserId(user_id);
                                notificationModel.setCreatedDate(created_date);
                                notificationModel.setActivityValue(entity_value);
                                notificationModel.setProfileURL(profile_pic);
                                notificationModel.setUserName(full_name);
                                notificationModel.setActivityType(types);
                                notificationModel.setActivityStatus(status);
                                notificationModel.setItemType("Item");
                                notificationModel.setEntityType(jsonObjectData.getString("entity_type"));
                                notificationModel.setEntityId(jsonObjectData.getString("entity_id"));
                                parentArrayList.add(notificationModel);

                            }

                            shimmerFrameLayout.setVisibility(View.GONE);
                            listNoti.setVisibility(View.VISIBLE);
                            setAdapter();
                            readNotification();
                            setRecyclerViewScrollListener(manager);

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(NotificationActivity.this);
                        } else {
                            shimmerFrameLayout.setVisibility(View.GONE);
                            listNoti.setVisibility(View.VISIBLE);
                        }

                        shimmerFrameLayout.setVisibility(View.GONE);
                        listNoti.setVisibility(View.VISIBLE);
                        CommonMethods.hideProgressBar(NotificationActivity.this, R.id.rootLayoutNoti);

                    } catch (Exception e) {

                        shimmerFrameLayout.setVisibility(View.GONE);
                        listNoti.setVisibility(View.VISIBLE);
                        CommonMethods.hideProgressBar(NotificationActivity.this, R.id.rootLayoutNoti);

                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    shimmerFrameLayout.setVisibility(View.GONE);
                    listNoti.setVisibility(View.VISIBLE);
                    CommonMethods.hideProgressBar(NotificationActivity.this, R.id.rootLayoutNoti);
                }
            });

            VolleySingleton.getInstance(NotificationActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {

            e.printStackTrace();
            shimmerFrameLayout.setVisibility(View.GONE);
            listNoti.setVisibility(View.VISIBLE);
            CommonMethods.hideProgressBar(NotificationActivity.this, R.id.rootLayoutNoti);
        }
    }

    private void setRecyclerViewScrollListener(final LinearLayoutManager mLinearLayoutManager) {

        listNoti.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mLinearLayoutManager.getChildCount();
                    totalItemCount = mLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                    if (mContentsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 1) {
                            mContentsLoading = false;
                            addProgress();

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    getMoreData(6, totalData);
                                    //Do something after 100ms
                                }
                            }, 1500);

                        }
                    }
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

        });

    }

    public void getMoreData(int visibleItemCount, int totalSize) {

        removeProgress();

        int currentData = arrayList.size();
        int fetchItems = currentData + visibleItemCount;

        if (currentData + visibleItemCount < totalSize) {
            for (int i = currentData; i < fetchItems; i++) {
                NotificationModel no = parentArrayList.get(i);
                arrayList.add(no);
                notificationAdapter.notifyItemInserted(currentData);
            }
        } else {
            int remaining = totalSize - currentData;
            remaining = currentData + remaining;
            for (int i = currentData; i < remaining; i++) {
                NotificationModel no = parentArrayList.get(i);
                arrayList.add(no);
                notificationAdapter.notifyItemInserted(currentData);
            }
        }
        mContentsLoading = true;
    }

    public void readNotification() {

        try {

            JSONObject jsonObject = new JSONObject();
            //Creating credentials jsonObject
            jsonObject.put(Constants.Network.ACTION, "read_notification");
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.EXTRA_API,
                    jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    try {
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {


                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(NotificationActivity.this);
                        } else {

                            String message = response.getString("response_message");
                            CommonMethods.displayToast(NotificationActivity.this, message);
                        }

                        CommonMethods.hideProgressBar(NotificationActivity.this, R.id.rootLayoutNoti);
                    } catch (Exception e) {

                        CommonMethods.displayToast(NotificationActivity.this, e.getMessage());
                        CommonMethods.hideProgressBar(NotificationActivity.this, R.id.rootLayoutNoti);
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    displayToast(NotificationActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);
                    CommonMethods.hideProgressBar(NotificationActivity.this, R.id.rootLayoutNoti);
                }
            });

            VolleySingleton.getInstance(NotificationActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
            CommonMethods.hideProgressBar(NotificationActivity.this, R.id.rootLayoutNoti);
            displayToast(NotificationActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);
        }
    }
}
