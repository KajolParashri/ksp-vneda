package com.indiareads.ksp.view.activity;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class RedeemDetailsActivity extends AppCompatActivity {
    TextView couponMain, activity_company_user_name;
    Button btnRedeem, noSufficientConis;
    ImageView backBtn;
    Context context;
    TextView validity, details, termsAndConditon;
    RelativeLayout couponMainText;
    String partner_id = "", pointsTotal;
    ImageView activity_company_profile_imageview;
    TextView coinsNeeded;
    Dialog mAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_details);

        context = this;
        Intent intentAction = getIntent();
        if (intentAction.hasExtra("partner_id")) {
            pointsTotal = intentAction.getStringExtra("pointsTotal");
            partner_id = intentAction.getStringExtra("partner_id");
        }
        coinsNeeded = findViewById(R.id.coinsNeeded);
        activity_company_profile_imageview = findViewById(R.id.activity_company_profile_imageview);
        activity_company_user_name = findViewById(R.id.activity_company_user_name);
        backBtn = findViewById(R.id.backBtn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        couponMain = findViewById(R.id.couponMain);
        btnRedeem = findViewById(R.id.btnRedeem);
        noSufficientConis = findViewById(R.id.noSufficientConis);

        validity = findViewById(R.id.validity);
        //   details = findViewById(R.id.details);
        termsAndConditon = findViewById(R.id.termsAndConditon);

        couponMainText = findViewById(R.id.couponMainText);
        couponMainText.setVisibility(View.GONE);

        termsAndConditon = findViewById(R.id.termsAndConditon);
        termsAndConditon.setMovementMethod(LinkMovementMethod.getInstance());

        btnRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redeemCoupon(partner_id);
            }
        });

        btnRedeem.setVisibility(View.GONE);
        noSufficientConis.setVisibility(View.GONE);

        couponMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String couponCode = couponMain.getText().toString();
                copyToClipBoard(couponCode);
                Toast.makeText(RedeemDetailsActivity.this, "Copied to Clipboard", Toast.LENGTH_SHORT).show();
            }
        });

        getRedeemDetail(partner_id);
    }

    public void getRedeemDetail(String partner_id) {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, "display_partner_details");
            jsonObject.put("partner_id", partner_id);

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.REWARDS_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Logger.info("TAG", response.toString());

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONObject data = response.getJSONObject(Constants.Network.DATA);

                                    String image_url = data.getString("image_url");

                                    String logo_url = data.getString("logo_url");
                                    Picasso.get().load(logo_url).into(activity_company_profile_imageview);

                                    String deal_title = data.getString("deal_title");
                                    activity_company_user_name.setText(deal_title);


                                    String min_points_needed = data.getString("min_points_needed");
                                    coinsNeeded.setText(min_points_needed + " Points");

                                    int minimum = Integer.valueOf(min_points_needed);
                                    int total = Integer.valueOf(pointsTotal);

                                    if (total > minimum) {
                                        btnRedeem.setVisibility(View.VISIBLE);
                                        noSufficientConis.setVisibility(View.GONE);
                                    } else {
                                        btnRedeem.setVisibility(View.GONE);
                                        noSufficientConis.setVisibility(View.VISIBLE);
                                    }

                                    String deal_details = data.getString("deal_details");
                                    termsAndConditon.setText("Details :\n\n" + deal_details);

                                    String expiry_date = data.getString("expiry_date");

                                    String[] date = expiry_date.split(" ");

                                    validity.setText("Valid till : " + date[0]);

                                    String terms_and_conditions = data.getString("terms_and_conditions");


                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(RedeemDetailsActivity.this);
                                } else {
                                    Logger.error("TAG", "400");
                                }
                            } catch (JSONException e) {
                                Logger.error("TAG", e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Logger.error("TAG", error.getMessage());
                        }
                    });

            VolleySingleton.getInstance(RedeemDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error("TAG", e.getMessage());
        }
    }

    public void redeemCoupon(String partner_id) {
        try {
            mAlertDialog = new Dialog(RedeemDetailsActivity.this);
            mAlertDialog.setContentView(R.layout.progress_dialog_layout);
            mAlertDialog.setCancelable(false);
            mAlertDialog.show();

            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, "redeem_coupon");
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put("partner_id", partner_id);

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.REWARDS_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            mAlertDialog.dismiss();
                            try {
                                Logger.info("TAG", response.toString());

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONObject data = response.getJSONObject(Constants.Network.DATA);
                                    String status = data.getString("status");
                                    String couponCode = data.getString("coupon_code");
                                    if (status.equals("1")) {
                                        couponMainText.setVisibility(View.VISIBLE);
                                        couponMain.setText(couponCode);
                                        Logger.info("TAG", data.toString());
                                    } else {
                                        CommonMethods.displayToast(RedeemDetailsActivity.this, "Coupon Expired !");
                                    }

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(RedeemDetailsActivity.this);
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_ERROR)) {
                                    CommonMethods.displayToast(RedeemDetailsActivity.this, "Coupon Expired !");
                                }
                            } catch (JSONException e) {
                                Logger.error("TAG", e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mAlertDialog.dismiss();
                            Logger.error("TAG", error.getMessage());
                        }
                    });

            VolleySingleton.getInstance(RedeemDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            mAlertDialog.dismiss();
            Logger.error("TAG", e.getMessage());
        }
    }

    public void copyToClipBoard(String text) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copied Text", text);
        clipboard.setPrimaryClip(clip);
    }
}
