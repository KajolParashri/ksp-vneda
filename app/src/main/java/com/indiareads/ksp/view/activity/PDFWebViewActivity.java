package com.indiareads.ksp.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class PDFWebViewActivity extends AppCompatActivity {
    private static final String TAG = PDFWebViewActivity.class.getSimpleName();
    private String mPdfUrl = "";
    private ViewPager viewPager;
    private File file;
    TextView continueRead;
    private String mContentId, mContentType, mContentStatusType;
    private Handler handler;
    private Runnable runnable;
    TextView pageCount;
    String pageNumber = "1";
    String currentPage = "1";
    int max = 0;
    private boolean paused = false;
    private long onScreenTimeInMilliseconds = 0;
    private Handler customHandler = new Handler();
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            onScreenTimeInMilliseconds = onScreenTimeInMilliseconds + 1000;
            if (!paused) {
                customHandler.postDelayed(this, 1000);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfweb_view);

        getIntentData();
        initViews();

        if (checkIfAlreadyhavePermission()) {

        } //loadPdf(mPdfUrl);
        else
            requestForSpecificPermission();
    }

    @Override
    protected void onPause() {
        super.onPause();
        paused = true;
        customHandler.removeCallbacks(updateTimerThread);
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkIfAlreadyhavePermission() {
        int permission_read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permission_write = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return ((permission_read) == PackageManager.PERMISSION_GRANTED && permission_write == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sendCountPage();
    }

    private void getIntentData() {
        Intent intent = getIntent();
        mPdfUrl = intent.getStringExtra(Content.DATA);
        pageNumber = intent.getStringExtra(Constants.Network.PAGE_NO);
        mContentId = intent.getStringExtra(Constants.Network.CONTENT_ID);
        mContentStatusType = intent.getStringExtra(Constants.Network.CONTENT_STATUS_TYPE);
        mContentType = intent.getStringExtra(Constants.Network.CONTENT_TYPE);

        Logger.info("Data", mContentId + "," + mContentStatusType + "," + mContentType);
    }

    private void initViews() {

        viewPager = findViewById(R.id.viewPager);
        pageCount = findViewById(R.id.pageCount);

        continueRead = findViewById(R.id.continueRead);
        continueRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueRead.setVisibility(View.GONE);
                viewPager.setCurrentItem(Integer.parseInt(pageNumber));
            }
        });

        continueRead.setVisibility(View.GONE);

        Toolbar mToolbar = findViewById(R.id.activity_pdf_toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getIntent().getStringExtra(Content.TITLE));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void sendPostConsumedRequest() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_POST_CONSUMED);
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);
            jsonObject.put(Constants.Network.TOTAL_TIME, (int) (onScreenTimeInMilliseconds / 1000));

            Logger.info("HITCONSUME", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.info("HITCONSUME", response.toString());
                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(PDFWebViewActivity.this);
                                }

                            } catch (Exception e) {
                                Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "sendPostConsumedRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(PDFWebViewActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }


    private void sendCountPage() {
        try {

            findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.SAVE_BOOK_PAGE);
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.PAGE, currentPage);

            Logger.info("HITCONSUME", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            findViewById(R.id.progressBar).setVisibility(View.GONE);
                            Logger.info("HITCONSUME", response.toString());
                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    finish();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(PDFWebViewActivity.this);
                                }

                            } catch (Exception e) {
                                findViewById(R.id.progressBar).setVisibility(View.GONE);
                                Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                            } finally {
                                finish();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            findViewById(R.id.progressBar).setVisibility(View.GONE);
                            Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "sendPostConsumedRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                            finish();
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(PDFWebViewActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        paused = false;
        customHandler.postDelayed(updateTimerThread, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                sendCountPage();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
            //            loadPdf(mPdfUrl);
            else {
                Toast.makeText(this, R.string.please_allow_permission_to_view_ebook, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        sendPostConsumedRequest();
        super.onDestroy();
        if (file != null)
            file.delete();
    }
}
