package com.indiareads.ksp.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.SuggestionRecyclerAdapter;
import com.indiareads.ksp.listeners.OnContentLikeCommentListener;
import com.indiareads.ksp.listeners.OnHomeFragmentClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.SuggestionModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.LikeCommentHelper;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.MyProgresbar;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.ContentFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SuggestionBoardActi extends AppCompatActivity implements OnContentLikeCommentListener {
    RecyclerView recycler;
    ImageView backButton;
    TextView postOrAskButton;
    EditText postEdit;
    OnContentLikeCommentListener onContentLikeCommentListener;
    private LinearLayoutManager mLinearLayoutManager;
    private SuggestionRecyclerAdapter suggestionRecyclerAdapter;
    private List<SuggestionModel> suggestionsList; //Contents list
    private int pageNumber = 1;

    private boolean doPagination = true;
    private boolean mContentsLoading = true;
    MyProgresbar myProgresbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion_board);

        recycler = findViewById(R.id.recycler);

        backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        postOrAskButton = findViewById(R.id.postOrAskButton);
        postEdit = findViewById(R.id.postEdit);

        postOrAskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String post = postEdit.getText().toString();
                post = post.trim();

                post = post.replace("'", "''");
                post = String.valueOf(Html.fromHtml(post));

                if (post.length() > 0) {
                    showProgressBar();
                    postSuggestion(post);
                } else {

                    Toast.makeText(SuggestionBoardActi.this, R.string.addSuggestion, Toast.LENGTH_SHORT).show();
                }
            }
        });

        initContentList();
        initRecyclerView();
        fetchData();

    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(SuggestionBoardActi.this, R.id.suggestion_frag, error);
    }

    private void initContentList() {
        suggestionsList = new ArrayList<>();
        addProgressBarInList();
    }

    public void showProgressBar() {
        if (myProgresbar == null)
            myProgresbar = MyProgresbar.show(this, R.style.Theme_MyprogressDialog_yellow);
    }

    public void hideProgressBar() {
        if (myProgresbar != null) {
            myProgresbar.dismiss();
            myProgresbar = null;
        }
    }

    private void showEmptyView() {
        SuggestionModel emptyContent = new SuggestionModel();
        emptyContent.setTitle(getString(R.string.content_list_empty));
        emptyContent.setContent_type((Constants.Content.CONTENT_TYPE_ERROR));
        suggestionsList.add(emptyContent);

        recycler.setLayoutManager(new LinearLayoutManager(this));
        suggestionRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInList() {
        SuggestionModel progressBarContent = new SuggestionModel();
        progressBarContent.setContent_type((Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        suggestionsList.add(progressBarContent);
    }

    private void initRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(mLinearLayoutManager);

        suggestionRecyclerAdapter = new SuggestionRecyclerAdapter(this, suggestionsList, new OnHomeFragmentClickListener() {
            @Override
            public void onLikeClicked(View view, int position) {

                SuggestionModel likedContent = suggestionsList.get(position);
                LikeCommentHelper.switchSuggs_LikeInfo(likedContent);
                LikeCommentHelper.handleSugges_LikeButtonConfig(likedContent, SuggestionBoardActi.this, view);
                LikeCommentHelper.upvoteSuggestoin(likedContent.getContent_id(), SuggestionBoardActi.this);
            }

            @Override
            public void onLikeCountClicked(View view, int position) {

            }

            @Override
            public void onCommentClicked(View view, int position) {
                LikeCommentHelper.openSuggestionComments(position, SuggestionBoardActi.this, view, suggestionsList.get(position), SuggestionBoardActi.this);
            }

            @Override
            public void onProfileClicked(View view, int position) {

            }

            @Override
            public void onViewClicked(View view, int position) {

            }

            @Override
            public void onDeleteClicked(View view, int position) {

            }
        });
        recycler.setAdapter(suggestionRecyclerAdapter);
    }

    private void fetchData() {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.SESSION_USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_DISPLAY_SUGGEST);

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.MYCOMPANY_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info("TAG", response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    suggestionsList.remove(suggestionsList.size() - 1);

                                    suggestionRecyclerAdapter.notifyItemRemoved(suggestionsList.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        SuggestionModel content = new SuggestionModel();
                                        content.setProfile_pic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                        content.setUser_full_name(jsonObject1.getString(Constants.Network.USER_FULL_NAME));
                                        content.setLikeStatus(jsonObject1.getString(Constants.Network.LIKED_STATUS));
                                        content.setDesignation(jsonObject1.getString(Constants.Network.DESIGNATION));
                                        content.setContent_type(3);
                                        content.setContent_id(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                        content.setCreate_date(jsonObject1.getString(Constants.Network.CREATED_DATE));
                                        content.setLike_count(jsonObject1.getString(Constants.Network.LIKE_COUNT));
                                        content.setComment_count(jsonObject1.getString(Constants.Network.COMMENT_COUNT));
                                        content.setSuggestion(jsonObject1.getString(Constants.Network.TITLE));

                                        suggestionsList.add(content);
                                    }

                                    suggestionRecyclerAdapter.notifyItemRangeInserted(suggestionsList.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(SuggestionBoardActi.this);
                                } else {

                                    //stop call to pagination in any case
                                    doPagination = false;

                                    //to remove progress bar
                                    suggestionsList.remove(suggestionsList.size() - 1);
                                    suggestionRecyclerAdapter.notifyItemRemoved(suggestionsList.size() - 1);

                                    if (pageNumber == 1) {
                                        //show msg no contents
                                        showEmptyView();
                                    } else {

                                        suggestionsList.remove(suggestionsList.size() - 1);
                                        suggestionRecyclerAdapter.notifyItemRemoved(suggestionsList.size() - 1);
                                        //no more contents
                                    }
                                }

                                mContentsLoading = true;

                            } catch (JSONException e) {

                                suggestionsList.remove(suggestionsList.size() - 1);
                                suggestionRecyclerAdapter.notifyItemRemoved(suggestionsList.size() - 1);
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                e.printStackTrace();
                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Toast.makeText(SuggestionBoardActi.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                            suggestionsList.remove(suggestionsList.size() - 1);
                            suggestionRecyclerAdapter.notifyItemRemoved(suggestionsList.size() - 1);
                            volleyError.printStackTrace();
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            jsonObjectRequest.setTag("TAG");
            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {


            Toast.makeText(SuggestionBoardActi.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            suggestionsList.remove(suggestionsList.size() - 1);
            suggestionRecyclerAdapter.notifyItemRemoved(suggestionsList.size() - 1);
            Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }


    private void postSuggestion(String suggestion) {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.SUGGESTION, suggestion);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_PUBLISH_SUGGEST);

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.MYCOMPANY_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            hideProgressBar();

                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    postEdit.setText("");
                                    initContentList();
                                    initRecyclerView();
                                    fetchData();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(SuggestionBoardActi.this);
                                }
                            } catch (Exception e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                e.printStackTrace();
                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            hideProgressBar();
                            volleyError.printStackTrace();
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            jsonObjectRequest.setTag("TAG");
            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {

            hideProgressBar();
            Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);
        }
    }

    @Override
    public void onContentLiked(Content content, String likeStatus, String likeCount) {

    }

    @Override
    public void onContentCommented(Content content, String commentCount, int pos) {

    }

    @Override
    public void onContentCommented(SuggestionModel content, String commentCount, int pos) {
        suggestionsList.set(pos, content);
        suggestionRecyclerAdapter.notifyItemChanged(pos);
    }
}
