package com.indiareads.ksp.view.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.CouponAdapter;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.CouponModelList;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyCoupons extends AppCompatActivity {
    RecyclerView recyclerView;
    CouponAdapter couponAdapter;
    ArrayList<CouponModelList> arrayList;
    ImageView backBtn;
    RelativeLayout noCoupons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_coupons);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        noCoupons = findViewById(R.id.noCoupons);

        getMyCoupons();

    }

    public void getMyCoupons() {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, "get_my_coupons");
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());


            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.REWARDS_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            arrayList = new ArrayList<>();
                            try {
                                Logger.info("TAG", response.toString());

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONArray data = response.getJSONArray(Constants.Network.DATA);
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject coupons = data.getJSONObject(i);

                                        CouponModelList couponModelList = new CouponModelList();
                                        couponModelList.setCouponCode(coupons.getString("coupon_code"));
                                        couponModelList.setCouponName(coupons.getString("deal_title"));
                                        couponModelList.setCouponValidity("Valid till : " + coupons.getString("expiry_date"));
                                        couponModelList.setCouponDetails("Detail: \n\n" + coupons.getString("deal_details"));

                                        arrayList.add(couponModelList);
                                    }
                                    setCouponAdapter();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(MyCoupons.this);
                                } else {
                                    Logger.error("TAG", "400");
                                }
                            } catch (JSONException e) {
                                Logger.error("TAG", e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Logger.error("TAG", error.getMessage());
                        }
                    });

            VolleySingleton.getInstance(MyCoupons.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error("TAG", e.getMessage());
        }
    }


    public void setCouponAdapter() {
        couponAdapter = new CouponAdapter(this, arrayList, new OnItemClick() {
            @Override
            public void onArticleClick(String tileName) {

            }

            @Override
            public void onInfoClick(String tileName) {

            }

            @Override
            public void onCategoryClick(int pos) {
                CouponModelList couponModelList = arrayList.get(pos);
                String details = couponModelList.getCouponDetails();

                showDetails(details);
            }
        });
        recyclerView.setAdapter(couponAdapter);

        if (arrayList.size() == 0) {
            noCoupons.setVisibility(View.VISIBLE);
        }else {
            noCoupons.setVisibility(View.GONE);
        }
    }

    public void showDetails(String details) {
        final BottomSheetDialog dialogLikes = new BottomSheetDialog(this);
        dialogLikes.setContentView(R.layout.detail_item_terms);
        dialogLikes.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogLikes.setCanceledOnTouchOutside(false);

        TextView detail_tv = dialogLikes.findViewById(R.id.detail_item_main);
        detail_tv.setText(details);

        ImageView close = dialogLikes.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLikes.dismiss();
            }
        });

        dialogLikes.show();
    }
}
