package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.indiareads.ksp.R;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;

import java.util.List;

public class UserDetailsActivity extends AppCompatActivity {

    private static final String TAG = UserDetailsActivity.class.getSimpleName();
    String mUserId;
    String mCompanyId;

    int pageNo;
    String strIntent = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        try {
            getDataFromIntent();
        } catch (Exception e) {

        }
        try {
            inflateFragment();
        } catch (Exception e) {

        }

    }

    private void getDataFromIntent() {
        if (getIntent().getAction() != null && getIntent().getAction().equals("ACTION_VIEW_USER")) {
            pageNo = Integer.parseInt(getIntent().getStringExtra(UserDetailsFragment.PAGE_NO));
        } else {
            pageNo = getIntent().getIntExtra(UserDetailsFragment.PAGE_NO, 0);
        }
        Intent ii = getIntent();
        if (ii.hasExtra("intent")) {
            strIntent = ii.getStringExtra("intent");
        }
        mUserId = ii.getStringExtra(Constants.Network.USER_ID);
        mCompanyId = ii.getStringExtra(Constants.Network.COMPANY_ID);

        Logger.error("POSTINTENT_USERActi", mUserId + " < UserID    Company ID >" + mCompanyId);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.info(TAG, "ONUserActivity");
        List<Fragment> frags = getSupportFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null)
                    handleResult(f, requestCode, resultCode, data);
            }
        }
    }

    public void handleResult(Fragment frag, int requestCode, int resultCode, Intent data) {
        Logger.info(TAG, "ONUserActivity");

        List<Fragment> frags = frag.getChildFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null) {
                    f.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    private void inflateFragment() {
        Bundle args = new Bundle();
        args.putInt(UserDetailsFragment.PAGE_NO, pageNo);
        args.putString(Constants.Network.USER_ID, mUserId);
        args.putString(Constants.Network.COMPANY_ID, mCompanyId);

        UserDetailsFragment userDetailsFragment = new UserDetailsFragment();
        userDetailsFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction().replace(R.id.activity_user_details_root_layout, userDetailsFragment).commit();
    }

}
