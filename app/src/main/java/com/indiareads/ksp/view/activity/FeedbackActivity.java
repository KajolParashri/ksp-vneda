package com.indiareads.ksp.view.activity;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.MyCompanyFragment;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class FeedbackActivity extends AppCompatActivity {
    EditText feedbackEdit;
    ImageView backButton;
    ProgressBar progress_bar;
    Button submitFeedbackBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initViews();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        submitFeedbackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String feecback = feedbackEdit.getText().toString();

                if (!feecback.equals("")) {
                    progress_bar.setVisibility(View.VISIBLE);
                    submitFeedback("", feecback);
                } else {
                    Toast.makeText(FeedbackActivity.this, "Please Enter Feedback" +
                            "", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void submitFeedback(String emailAdd, String mesg) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.INSERT_FEEDBACK);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.EMAIL, User.getCurrentUser(this).getUserEmail());
            jsonObject.put(Constants.Network.MESSAGE, mesg);

            Logger.info("Feedback", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.FEEDBACK_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progress_bar.setVisibility(View.GONE);
                    Logger.info("Feedback", response.toString());

                    try {
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            finish();
                            Toast.makeText(FeedbackActivity.this, "Thanks for your valuable Feedback", Toast.LENGTH_SHORT).show();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(FeedbackActivity.this);
                        } else {
                            progress_bar.setVisibility(View.GONE);
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                                    "getCompanyDetails", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);

                            displayErrorUI(error);
                        }
                    } catch (JSONException e) {
                        progress_bar.setVisibility(View.GONE);
                        Error error = Error.generateError(JSONException.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                                "getCompanyDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        Logger.info("Feedback", e.getMessage());
                        displayErrorUI(error);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progress_bar.setVisibility(View.GONE);
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                            "getCompanyDetails", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                    Logger.info("Feedback", error.getMessage());
                    displayErrorUI(error);
                }
            });
            jsonObjectRequest.setTag("Feedback");
            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Logger.info("Feedback", e.getMessage());
            progress_bar.setVisibility(View.GONE);
            Error error = Error.generateError(JSONException.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                    "getCompanyDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);
        }
    }

    private void displayErrorUI(Error error) {
        // CommonMethods.hideProgressBar(this, R.id.fragment_my_company_root_layout);
        CommonMethods.showErrorView(FeedbackActivity.this, R.id.frame_layout, error);
    }

    public void initViews() {
        feedbackEdit = findViewById(R.id.feedbackEdit);
        backButton = findViewById(R.id.backButton);
        submitFeedbackBtn = findViewById(R.id.submitFeedbackBtn);
        progress_bar = findViewById(R.id.progress_bar);
        progress_bar.setVisibility(View.GONE);
    }


}
