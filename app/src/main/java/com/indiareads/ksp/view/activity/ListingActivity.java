package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.ContentFragmentRecyclerAdapter;
import com.indiareads.ksp.adapters.PopularContentAdapter;
import com.indiareads.ksp.listeners.OnDraftedContentItemClickListener;
import com.indiareads.ksp.listeners.OnFilterAppliedListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.ContentFragment;
import com.indiareads.ksp.view.fragment.ListingActivityFilterFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListingActivity extends AppCompatActivity implements OnRefreshListener, OnFilterAppliedListener {

    private static final String TAG = ListingActivity.class.getSimpleName();

    private ListingActivityFilterFragment listingActivityFilterFragment;
    private ContentFragmentRecyclerAdapter mContentRecyclerAdapter;
    private RecyclerView mContentsRecyclerView;
    private PopularContentAdapter mPopularContentRecyclerAdapter;
    private RecyclerView mPopularContentRecyclerView;
    private NestedScrollView mNestedScrollView;
    LinearLayoutManager linearLayoutManager;
    private List<Content> mPopularContents;
    private List<Content> mContents;

    private int pageNumber = 1;
    private boolean doPagination = true;
    private boolean mContentsLoading = true;
    private String mContentType;

    private JSONObject selectedFilterObject;
    private JSONArray withingCompanyArray;
    private JSONArray subCatArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        initViews();

        getDataFromIntent();

        setUpToolbar();
        setContentTypeDescription();

        initContentList();
        initPopularContentList();

        initContentRecyclerView();
        initPopularContentRecyclerView();

        setContentItemClickListener();
        setPopularContentItemClickListener();
        setOnFilterClickListner();

        setContentRecyclerViewScrollListener();

        initialiseFilterArrays();

        fetchContentData();
        fetchPopularContentData();
    }

    private void setOnFilterClickListner() {
        findViewById(R.id.activity_listing_filter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    listingActivityFilterFragment = new ListingActivityFilterFragment();

                    Bundle arguments = new Bundle();

                    ArrayList<String> filterMyCompany = new ArrayList<>();
                    ArrayList<String> filterCategories = new ArrayList<>();

                    for (int i = 0; i < withingCompanyArray.length(); i++)
                        filterMyCompany.add(withingCompanyArray.getString(i));
                    arguments.putStringArrayList(ListingActivityFilterFragment.FILTER_MY_COMPANY, filterMyCompany);

                    for (int i = 0; i < subCatArray.length(); i++)
                        filterCategories.add(subCatArray.getString(i));
                    arguments.putStringArrayList(ListingActivityFilterFragment.FILTER_CATEGORIES, filterCategories);

                    listingActivityFilterFragment.setArguments(arguments);
                    listingActivityFilterFragment.setOnFilterAppliedListener(ListingActivity.this);

                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.activity_listing_root_layout, listingActivityFilterFragment)
                            .addToBackStack(ListingActivityFilterFragment.class.getSimpleName())
                            .commit();

                } catch (JSONException e) {
                    Error error = Error.generateError(JSONException.class.getSimpleName(), ListingActivity.class.getSimpleName(),
                            "setOnFilterClickListner", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                    displayErrorUI(error);
                }
            }
        });
    }

    private void setContentTypeDescription() {
        ((TextView) findViewById(R.id.activity_listing_content_type_description)).setVisibility(View.GONE);
        //             .setText(CommonMethods.getContentTypeDescriptionFromContentType(mContentType));
    }

    private void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.activity_listing_toolbar);
        toolbar.setTitle(CommonMethods.getContentTypeNameFromContentType(mContentType));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initialiseFilterArrays() {
        try {
            withingCompanyArray = new JSONArray();
            subCatArray = new JSONArray();
            selectedFilterObject = new JSONObject();
            selectedFilterObject.put(Constants.Network.WITHIN_COMPANY, withingCompanyArray);
            selectedFilterObject.put(Constants.Network.CATEGORY, subCatArray);
        } catch (JSONException e) {

            Error error = Error.generateError(JSONException.class.getSimpleName(), ListingActivity.class.getSimpleName(),
                    "initialiseParameters", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void getDataFromIntent() {
        if (getIntent().getStringExtra(Constants.Content.CONTENT_TYPE) != null) {
            mContentType = getIntent().getStringExtra(Constants.Content.CONTENT_TYPE);
        } else {
            Error error = Error.generateError(NullPointerException.class.getSimpleName(), ListingActivity.class.getSimpleName(),
                    "getDataFromIntent", Error.ERROR_TYPE_NORMAL_ERROR, "Arguments Null");

            displayErrorUI(error);
        }
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(this, R.id.activity_listing_root_layout, error);
    }

    private void initViews() {

        mPopularContentRecyclerView = findViewById(R.id.activity_listing_popular_content_recyclerview);
        mContentsRecyclerView = findViewById(R.id.activity_listing_content_recyclerview);
        mNestedScrollView = findViewById(R.id.activity_listing_nestedscrollview);
    }

    private void setContentItemClickListener() {

        mContentRecyclerAdapter.setOnClickListener(new OnDraftedContentItemClickListener() {
            @Override
            public void onEditClick(View v, int position) {

            }

            @Override
            public void onDeleteClick(View v, int position) {

            }

            @Override
            public void onItemClick(View v, int position) {

                Intent productIntent = new Intent(ListingActivity.this, ProductActivity.class);
                productIntent.putExtra(Constants.Network.CONTENT_ID, mContents.get(position).getContentId());
                productIntent.putExtra(Constants.Network.CONTENT_TYPE, mContents.get(position).getContentType());
                productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, mContents.get(position).getStatusType());
                startActivity(productIntent);

            }

            @Override
            public void onLikeCOunt(View v, int position) {

            }
        });
    }

    private void setPopularContentItemClickListener() {
        mPopularContentRecyclerAdapter.setOnClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {

                Intent productIntent = new Intent(ListingActivity.this, ProductActivity.class);
                productIntent.putExtra(Constants.Network.CONTENT_ID, mPopularContents.get(position).getContentId());
                productIntent.putExtra(Constants.Network.CONTENT_TYPE, mPopularContents.get(position).getContentType());
                productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, mPopularContents.get(position).getStatusType());
                startActivity(productIntent);

            }
        });
    }


    private void initContentList() {
        mContents = new ArrayList<>();
        addProgressBarInContentList();
    }

    private void initPopularContentList() {
        mPopularContents = new ArrayList<>();
        addProgressBarInPopularContentList();
    }

    private void setContentRecyclerViewScrollListener() {
        mNestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = mNestedScrollView.getChildAt(mNestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (mNestedScrollView.getHeight() + mNestedScrollView.getScrollY()));

                if (doPagination && diff == 0) {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (mContentsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mContentsLoading = false;
                            addProgressBarInContentList();
                            mContentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);
                            pageNumber++;
                            fetchContentData();
                        }
                    }
                }
            }
        });

    }

    private void fetchContentData() {
        mContentsLoading = false;
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_CONTENT_FOR_LISTING);
            jsonObject.put(Constants.Network.SELECTED_FILTERS, selectedFilterObject);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT_LISTING_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mContents.remove(mContents.size() - 1);

                                    mContentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Content content = new Content();
                                        content.setAuthorName(jsonObject1.getString(Constants.AUTHOR));//source_name
                                        content.setSourceName(jsonObject1.getString(Constants.Network.SOURCE_NAME));
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));

                                        String title = jsonObject1.getString(Constants.Network.TITLE);

                                        title = String.valueOf(Html.fromHtml(title));
                                        content.setTitle(title);

                                        content.setThumnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));
                                        content.setContentType(jsonObject1.getString(Constants.Network.CONTENT_TYPE));
                                        content.setStatusType(jsonObject1.getString(Constants.Network.CONTENT_STATUS_TYPE));
                                        content.setAvgContentRating(jsonObject1.getString(Constants.Network.RATING));

                                        mContents.add(content);
                                    }

                                    mContentRecyclerAdapter.notifyItemRangeInserted(mContents.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ListingActivity.this);
                                } else {

                                    //stop call to pagination in any case
                                    doPagination = false;

                                    //to remove progress bar
                                    mContents.remove(mContents.size() - 1);
                                    mContentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    if (pageNumber == 1) {
                                        //show msg no contents
                                        showEmptyViewInContentList();
                                    } else {
                                        //no more contents
                                    }
                                }

                                mContentsLoading = true;

                            } catch (JSONException e) {

                                Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                        "fetchContentData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                    "fetchContentData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                    "fetchContentData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);

        }
    }

    private void fetchPopularContentData() {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_POPULAR_CONTENT);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT_LISTING_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info("Popular", response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mPopularContents.remove(mPopularContents.size() - 1);

                                    mPopularContentRecyclerAdapter.notifyItemRemoved(mPopularContents.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONArray(i).getJSONObject(0);

                                        Content content = new Content();
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                        String title = jsonObject1.getString(Constants.Network.TITLE);

                                        title = String.valueOf(Html.fromHtml(title));
                                        content.setTitle(title);
                                        //thumnail
                                        content.setThumnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        content.setContentType(mContentType);
                                        content.setStatusType(jsonObject1.getString(Constants.Network.CONTENT_STATUS_TYPE));
                                        content.setAvgContentRating(jsonObject1.getString(Constants.Network.RATING));

                                        mPopularContents.add(content);
                                    }

                                    mPopularContentRecyclerAdapter.notifyItemRangeInserted(mPopularContents.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {

                                    SideBarPanel.logout(ListingActivity.this);

                                } else {

                                    showEmptyViewInPopularContentList();
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                        "fetchPopularContentData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                    "fetchPopularContentData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(this).

                    addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                    "fetchPopularContentData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }


    private void showEmptyViewInContentList() {
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyContent.setTitle(getString(R.string.content_list_empty));
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));

        mContents.add(emptyContent);

        mContentsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mContentRecyclerAdapter.notifyItemInserted(0);
    }

    private void showEmptyViewInPopularContentList() {
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyContent.setTitle(getString(R.string.content_list_empty));
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));

        mPopularContents.add(emptyContent);

        mPopularContentRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mPopularContentRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInContentList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mContents.add(progressBarContent);
    }

    private void addProgressBarInPopularContentList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mPopularContents.add(progressBarContent);
    }

    private void initContentRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(this);
        mContentsRecyclerView.setLayoutManager(linearLayoutManager);

        mContentRecyclerAdapter = new ContentFragmentRecyclerAdapter(this, mContents);
        mContentsRecyclerView.setAdapter(mContentRecyclerAdapter);

        ViewCompat.setNestedScrollingEnabled(mContentsRecyclerView, false);
    }

    private void initPopularContentRecyclerView() {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mPopularContentRecyclerView.setLayoutManager(mLinearLayoutManager);

        mPopularContentRecyclerAdapter = new PopularContentAdapter(this, mPopularContents);
        mPopularContentRecyclerView.setAdapter(mPopularContentRecyclerAdapter);
    }

    @Override
    public void onRefresh() {

        fetchContentData();
        fetchPopularContentData();
    }

    @Override
    public void OnFilterApplied(Bundle arguments) {

        withingCompanyArray.remove(0);
        ArrayList<String> filterMyCompany = arguments.getStringArrayList(ListingActivityFilterFragment.FILTER_MY_COMPANY);
        for (int i = 0; i < filterMyCompany.size(); i++)
            withingCompanyArray.put(filterMyCompany.get(i));

        for (int i = 0; i < subCatArray.length(); i++)
            subCatArray.remove(i);
        ArrayList<String> filterCategories = arguments.getStringArrayList(ListingActivityFilterFragment.FILTER_CATEGORIES);
        for (int i = 0; i < filterCategories.size(); i++)
            subCatArray.put(filterCategories.get(i));

        int oldSize = mContents.size();
        mContents.clear();
        mContentRecyclerAdapter.notifyItemRangeRemoved(0, oldSize);
        pageNumber = 1;
        doPagination = true;
        mContentsLoading = true;
        addProgressBarInContentList();

        fetchContentData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
