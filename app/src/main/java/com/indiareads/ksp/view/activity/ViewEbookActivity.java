package com.indiareads.ksp.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.CustomWebView;
import com.indiareads.ksp.utils.HTMLUtils;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import static com.indiareads.ksp.view.activity.ProductActivity.FONT_FAMILY;
import static com.indiareads.ksp.view.activity.ProductActivity.REGULAR_FONT;

public class ViewEbookActivity extends AppCompatActivity {

    private static final String TAG = ViewEbookActivity.class.getSimpleName();
    public static String mUrl;
    private String mTitle;
    TextView pageTitle;
    private String mContentId, mContentType, mContentStatusType;
    private Handler mHandler = new Handler();
    static int currentPage;
    String pageNumber = "";
    private CustomWebView webView;
    ImageView imageBack;

    String url = "";
    static TextView pageCount;
    static ProgressBar progressBar;
    TextView continueRead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_ebook);
        getDataFromIntent();
        initViews();
        setupToolbar();
        setUpHorizontalWebview();
    }

    private void setupToolbar() {

        progressBar = findViewById(R.id.progressBar);
        if (mTitle.length() > 40) {
            mTitle = mTitle.substring(0, 40);
            pageTitle.setText(mTitle + "..");
        } else {
            pageTitle.setText(mTitle);
        }

    }

    private void sendCountPage() {
        try {

            findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.SAVE_BOOK_PAGE);
            jsonObject.put(Constants.Network.CONTENT_ID, mContentId);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, mContentStatusType);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.PAGE, currentPage);

            Logger.info("HITCONSUME", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            findViewById(R.id.progressBar).setVisibility(View.GONE);
                            Logger.info("HITCONSUME", response.toString());
                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    finish();

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ViewEbookActivity.this);
                                }

                            } catch (Exception e) {
                                findViewById(R.id.progressBar).setVisibility(View.GONE);
                                Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            findViewById(R.id.progressBar).setVisibility(View.GONE);
                            Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "sendPostConsumedRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(ViewEbookActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    public void setUpHorizontalWebview() {
        pageCount = findViewById(R.id.pageCount);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        if (mUrl.endsWith(".html") || mUrl.endsWith(".htm")) {
            url = mUrl;
        }

        Ion.with(getApplicationContext()).load(url).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {


                String text;

                text = "<html><body  style=\"text-align:justify;\">";
                text += result;
                text += "</body></html>";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    webView.setWebContentsDebuggingEnabled(true);
                }

                if (mUrl.endsWith(".html") || mUrl.endsWith(".htm")) {

                    String contents = HTMLUtils.getLocalFontInBook(text, FONT_FAMILY, REGULAR_FONT);
                    webView.loadDataWithBaseURL("file:///android_asset/", contents, "text/html", "UTF-8", null);

                } else {
                    webView.loadDataWithBaseURL("file://" + Environment.getExternalStorageDirectory(), text, "text/html", "utf-8", null);
                }
            }
        });
    }


    public static void hideprogress() {
        progressBar.setVisibility(View.GONE);
    }

    public static void setCount(String setText) {

        pageCount.setText("Page No: " + setText);
    }

    public static void CurrentCount(int pos) {
        currentPage = pos;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

    final class MyJavaScriptInterface {
        public void ProcessJavaScript(final String scriptname, final String args) {
            mHandler.post(new Runnable() {
                public void run() {
                    //Do your activities
                }
            });
        }

    }

    @SuppressLint("JavascriptInterface")
    private void initViews() {

        continueRead = findViewById(R.id.continueRead);
        continueRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //webView.turnToContinue(pageNumber);
            }
        });
        if (pageNumber.equals("0")) {
            continueRead.setVisibility(View.GONE);
        }
        pageTitle = findViewById(R.id.pageTitle);
        imageBack = findViewById(R.id.imageBack);
        webView = findViewById(R.id.webview);
        webView.setBackgroundColor(0);
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "Android");
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCountPage();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sendCountPage();
    }

    private void getDataFromIntent() {

        mUrl = getIntent().getStringExtra(Content.DATA);
        mTitle = getIntent().getStringExtra(Content.TITLE);
        Intent intent = getIntent();

        pageNumber = intent.getStringExtra(Constants.Network.PAGE_NO);
        mContentId = intent.getStringExtra(Constants.Network.CONTENT_ID);
        mContentStatusType = intent.getStringExtra(Constants.Network.CONTENT_STATUS_TYPE);
        mContentType = intent.getStringExtra(Constants.Network.CONTENT_TYPE);
    }
}
