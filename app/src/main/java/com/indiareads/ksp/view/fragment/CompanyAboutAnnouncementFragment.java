package com.indiareads.ksp.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.AnnouncementRecyclerAdapter;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CompanyAboutAnnouncementFragment extends Fragment implements OnRefreshListener {

    private static final String TAG = ContentFragment.class.getSimpleName();

    private View mFragmentRootView; //Root view
    private LinearLayoutManager mLinearLayoutManager;
    private AnnouncementRecyclerAdapter mAnnouncementRecyclerAdapter;
    private RecyclerView mAnnouncementRecyclerView;

    private List<Content> mAnnouncements; //Contents list

    private int pageNumber = 1;
    private boolean doPagination = true;
    private boolean mLoading = true;

    public CompanyAboutAnnouncementFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentRootView = inflater.inflate(R.layout.fragment_company_about_announcement, container, false);

        initViews();
        initContentList();
        initRecyclerView();
        setRecyclerViewScrollListener();
        fetchData();

        return mFragmentRootView;
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(this, R.id.fragment_about_announcement_root_layout, error);
    }

    private void initContentList() {
        mAnnouncements = new ArrayList<>();
        addProgressBarInList();
    }

    private void initViews() {
        mAnnouncementRecyclerView = mFragmentRootView.findViewById(R.id.fragment_about_announcement_recyclerview);
    }

    private void setRecyclerViewScrollListener() {
        //Fetching next page's data on reaching bottom
        mAnnouncementRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mLinearLayoutManager.getChildCount();
                    totalItemCount = mLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                    if (mLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mLoading = false;
                            addProgressBarInList();
                            mAnnouncementRecyclerAdapter.notifyItemInserted(mAnnouncements.size() - 1);
                            pageNumber++;
                            fetchData();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void fetchData() {
        mLoading = false;
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(getActivity()).getCompanyId());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_FETCH_ANNOUNCEMENTS);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.MYCOMPANY_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mAnnouncements.remove(mAnnouncements.size() - 1);

                                    mAnnouncementRecyclerAdapter.notifyItemRemoved(mAnnouncements.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Content content = new Content();
                                        content.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ANNOUNCEMENT));
                                        content.setContentId(jsonObject1.getString(Constants.Network.ANNOUNCEMENT_ID));
                                        content.setCreatedDate(jsonObject1.getString(Constants.Network.CREATED_DATE));
                                        content.setDescription(jsonObject1.getString(Constants.Network.ANNOUNCEMENT));

                                        mAnnouncements.add(content);
                                    }

                                    mAnnouncementRecyclerAdapter.notifyItemRangeInserted(mAnnouncements.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {

                                    //stop call to pagination in any case
                                    doPagination = false;

                                    //to remove progress bar
                                    mAnnouncements.remove(mAnnouncements.size() - 1);
                                    mAnnouncementRecyclerAdapter.notifyItemRemoved(mAnnouncements.size() - 1);

                                    if (pageNumber == 1) {
                                        //show msg no contents
                                        showEmptyView();
                                    } else {
                                        //no more contents
                                    }
                                }

                                mLoading = true;

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void showEmptyView() {
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyContent.setTitle(getString(R.string.content_list_empty));
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mAnnouncements.add(emptyContent);

        mAnnouncementRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAnnouncementRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mAnnouncements.add(progressBarContent);
    }

    private void initRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mAnnouncementRecyclerView.setLayoutManager(mLinearLayoutManager);

        mAnnouncementRecyclerAdapter = new AnnouncementRecyclerAdapter(getActivity(), mAnnouncements);
        mAnnouncementRecyclerView.setAdapter(mAnnouncementRecyclerAdapter);
    }

    @Override
    public void onRefresh() {
        fetchData();
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getContext()).cancelRequests(TAG);
    }
}
