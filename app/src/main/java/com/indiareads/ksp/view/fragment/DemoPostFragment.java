package com.indiareads.ksp.view.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.HomeFragmentRecyclerAdapter;
import com.indiareads.ksp.adapters.LikeAdapter;
import com.indiareads.ksp.listeners.OnContentLikeCommentListener;
import com.indiareads.ksp.listeners.OnHomeFragmentClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.SuggestionModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.LikeCommentHelper;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.view.activity.UserDetailsActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class DemoPostFragment extends Fragment implements OnContentLikeCommentListener {

    public static final String HOME_ADAPTER_POSITION = "Adapter Position";
    private static final String TAG = DemoPostFragment.class.getSimpleName();
    private View mHomeFragmentRootView; //Root view
    private LinearLayoutManager mLinearLayoutManager;
    private HomeFragmentRecyclerAdapter mHomeFragmentRecyclerAdapter;
    private List<User> mUsers; //Users list
    private List<Content> mContents; //Contents list
    private RecyclerView mHomeRecyclerView;
    private int pageNumber = 1;
    private boolean doPagination = true;
    private String userId;
    private String companyId;
    private String skipContentType;
    private String requiredContentType;
    private boolean mContentsLoading = true;
    Dialog mAlertDialog;
    BottomSheetDialog dialogLikes;

    public DemoPostFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

            if (getArguments() != null) {
                userId = getArguments().getString(Constants.Network.USER_ID, "");
                companyId = getArguments().getString(Constants.Network.COMPANY_ID, "");
                skipContentType = getArguments().getString(Constants.Network.SKIP_CONTENT_TYPE, "");
                requiredContentType = getArguments().getString(Constants.Network.REQUIRED_CONTENT_TYPE, "");
            }

        } catch (Exception e) {

        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mHomeFragmentRootView = inflater.inflate(R.layout.fragment_post, container, false);

        initViews();
        initContentList();
        initRecyclerView();
        setHomeFragmentClickListener();
        initialiseDialog();
        setRecyclerViewScrollListener();
        //setSwipeRefreshLayoutListener();
        fetchData();

        return mHomeFragmentRootView;
    }

    public void showProgressBar() {
        mAlertDialog = new Dialog(getActivity());
        mAlertDialog.setContentView(R.layout.progress_dialog_layout);
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();
    }

    public void hideProgressBar() {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }

    private void setHomeFragmentClickListener() {
        mHomeFragmentRecyclerAdapter.setOnClickListener(new OnHomeFragmentClickListener() {
            @Override
            public void onLikeClicked(View view, int position) {

                Content likedContent = mContents.get(position);

                LikeCommentHelper.switchLikeInfo(likedContent);
                LikeCommentHelper.handleLikeButtonConfig(likedContent, getActivity(), view);
                LikeCommentHelper.likeContentItem(getContext(), likedContent, DemoPostFragment.this);

            }

            @Override
            public void onViewClicked(View view, int position) {
                Intent productIntent = new Intent(getContext(), ProductActivity.class);
                productIntent.putExtra(HOME_ADAPTER_POSITION, position);
                productIntent.putExtra(Constants.Network.CONTENT_ID, mContents.get(position).getContentId());
                productIntent.putExtra(Constants.Network.CONTENT_TYPE, mContents.get(position).getContentType());
                productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);
                startActivityForResult(productIntent, Constants.RequestCode.REQUEST_LIKE_COMMENT_COUNT);
            }

            @Override
            public void onDeleteClicked(View view, int position) {
                String content_Id = mContents.get(position).getContentId();
                deleteData(content_Id, position);

            }

            @Override
            public void onLikeCountClicked(View view, int position) {
                String mainTitle = mContents.get(position).getTitle();
                getLikesShow(mainTitle, mContents.get(position).getContentId(), 1, "1");
            }

            @Override
            public void onCommentClicked(View view, int position) {
                Content content = mContents.get(position);
                LikeCommentHelper.openComments(position, getContext(), view, content, DemoPostFragment.this);
            }

            @Override
            public void onProfileClicked(View view, int position) {

                if (!userId.equals(mUsers.get(position).getUserId())) {
                    Intent userProfileIntent = new Intent(getActivity(), UserDetailsActivity.class);
                    userProfileIntent.putExtra(Constants.Network.USER_ID, mUsers.get(position).getUserId());
                    userProfileIntent.putExtra(Constants.Network.COMPANY_ID, mUsers.get(position).getCompanyId());
                    userProfileIntent.putExtra("intent", "chat");
                    userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 0);
                    startActivity(userProfileIntent);
                }
            }
        });


    }

    public void initialiseDialog() {
        dialogLikes = new BottomSheetDialog(getActivity());
        dialogLikes.setContentView(R.layout.likes_single_item);
        dialogLikes.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogLikes.setCanceledOnTouchOutside(false);
    }

    public void showLikes(ArrayList<User> listData, String titleMesg) {

        RecyclerView recyclerView = dialogLikes.findViewById(R.id.like_recycler);
//        recyclerView.setHasFixedSize(true);
        TextView titleMessage = dialogLikes.findViewById(R.id.titleMessage);
        if (titleMesg.length() <= 15) {
            titleMessage.setText("Likes : " + titleMesg);
        } else {
            titleMesg = titleMesg.substring(0, 15);
            titleMessage.setText("Likes : " + titleMesg + "...");
        }
//        recyclerView.setItemViewCacheSize(1000);
        LinearLayoutManager LayoutManagaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(LayoutManagaer);
        ImageView cross = dialogLikes.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLikes.dismiss();
            }
        });
        LikeAdapter adapter = new LikeAdapter(listData);
        recyclerView.setAdapter(adapter);

        if (!dialogLikes.isShowing()) {
            dialogLikes.show();
        }

    }

    private void getLikesShow(final String mainTitle, String content_id, int pageNumber, String content_status_type) {
        try {

            final ArrayList<User> likes = new ArrayList<>();
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.CONTENT_ID, content_id);
            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, content_status_type);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.LOAD_CONTENT_LIKES);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                Logger.info(TAG, response.toString());

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONArray data = response.getJSONArray(Constants.Network.DATA);

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject dataMain = data.getJSONObject(i);
                                        User user = new User();
                                        user.setFullName(dataMain.getString(Constants.Network.FULL_USER_NAME));
                                        user.setProfilePic(dataMain.getString(Constants.Network.PROFILE_PIC));
                                        user.setDesignation(dataMain.getString(Constants.Network.DESIGNATION));
                                        user.setCreatedContents(dataMain.getString(Constants.Network.CREATED_DATE));
                                        user.setUserId(dataMain.getString(Constants.Network.USER_ID));
                                        likes.add(user);
                                    }

                                    showLikes(likes, "" + mainTitle);
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Logger.error(TAG, "400");
                                }
                            } catch (JSONException e) {
                                Logger.error(TAG, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Logger.error(TAG, error.getMessage());
                        }
                    });

            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error(TAG, e.getMessage());
        }
    }

    private void initContentList() {

        mUsers = new ArrayList<>();
        mContents = new ArrayList<>();

        addProgressBarInList();
    }

    private void initViews() {
        mHomeRecyclerView = mHomeFragmentRootView.findViewById(R.id.fragment_home_recycler_view);
        mHomeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void setRecyclerViewScrollListener() {
        //Fetching next page's data on reaching bottom
        mHomeRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mLinearLayoutManager.getChildCount();
                    totalItemCount = mLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                    if (mContentsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mContentsLoading = false;
                            addProgressBarInList();
                            mHomeFragmentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);
                            pageNumber++;
                            fetchData();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void fetchData() {

        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            if (!userId.equals(""))
                jsonObject.put(Constants.Network.USER_ID, userId);
            if (!companyId.equals(""))
                jsonObject.put(Constants.Network.COMPANY_ID, companyId);
            jsonObject.put(Constants.Network.SKIP_CONTENT_TYPE, skipContentType);
            jsonObject.put(Constants.Network.REQUIRED_CONTENT_TYPE, requiredContentType);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_DISPLAY_CONTENTS);
            jsonObject.put(Constants.Network.SESSION_USER_ID, User.getCurrentUser(getContext()).getUserId());
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);

            Logger.error(TAG, jsonObject.toString());
            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.HOMEPAGE_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.error(TAG, response.toString());
                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mUsers.remove(mUsers.size() - 1);
                                    mContents.remove(mContents.size() - 1);

                                    mHomeFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        User user = new User();
                                        user.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                        user.setFullName(jsonObject1.getString(Constants.Network.USER_FULL_NAME));
                                        user.setProfilePic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                        user.setDesignation(jsonObject1.getString(Constants.Network.DESIGNATION));

                                        Content content = new Content();
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                        content.setCreatedDate(jsonObject1.getString(Constants.Network.CREATED_DATE));
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                        content.setThumbnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));
                                        content.setContentType(jsonObject1.getString(Constants.Network.CONTENT_TYPE));
                                        content.setLikeStatus(jsonObject1.getString(Constants.Network.LIKED_STATUS));
                                        content.setLikeCount(jsonObject1.getString(Constants.Network.LIKE_COUNT));
                                        content.setCommentCount(jsonObject1.getString(Constants.Network.COMMENT_COUNT));
                                        content.setAvgContentRating(jsonObject1.getString(Constants.Network.AVG_CONTENT_RATING));
                                        content.setUser(user);

                                        mUsers.add(user);
                                        mContents.add(content);
                                    }

                                    mHomeFragmentRecyclerAdapter.notifyItemRangeInserted(mUsers.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {

                                    //stop call to pagination in any case
                                    doPagination = false;

                                    //to remove progress bar
                                    mUsers.remove(mUsers.size() - 1);
                                    mContents.remove(mContents.size() - 1);
                                    mHomeFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    if (pageNumber == 1) {
                                        //show msg no contents
                                        showEmptyView(R.mipmap.ic_fevicon, getString(R.string.content_list_empty));
                                    } else {
                                        //show msg no more posts
                                    }
                                }

                                mContentsLoading = true;

                            } catch (JSONException e) {
                                Logger.error(TAG, "In OnResponse" + e.getMessage());
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            doPagination = false;

                            //to remove progress bar
                            mUsers.remove(mUsers.size() - 1);
                            mContents.remove(mContents.size() - 1);
                            mHomeFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                            if (error instanceof NetworkError)
                                showEmptyView(R.drawable.ic_signal_wifi_off, getString(R.string.connection_error_message));
                            else
                                showEmptyView(R.drawable.ic_error_outline, getString(R.string.something_went_wrong));

                            mContentsLoading = true;

                            Logger.info(TAG, error.getMessage() + " is the volley error ");
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    50000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Logger.error(TAG, "In fetchData" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showEmptyView(int imageId, String msg) {
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(imageId));
        emptyContent.setTitle(msg);
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mContents.add(emptyContent);

        mUsers.add(new User());
        mHomeFragmentRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType("0");
        mContents.add(progressBarContent);
        mUsers.add(new User());
    }

    private void initRecyclerView() {

        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mHomeRecyclerView.setLayoutManager(mLinearLayoutManager);
        boolean showEditOptions = false;
        if (userId.equals(User.getCurrentUser(getContext()).getUserId()))
            showEditOptions = true;
        mHomeFragmentRecyclerAdapter = new HomeFragmentRecyclerAdapter(mUsers, mContents, showEditOptions);
        mHomeRecyclerView.setAdapter(mHomeFragmentRecyclerAdapter);
    }

    @Override
    public void onContentLiked(Content content, String likeStatus, String likeCount) {

    }

    @Override
    public void onContentCommented(Content content, String commentCount, int pos) {

    }

    @Override
    public void onContentCommented(SuggestionModel content, String commentCount, int pos) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.RequestCode.REQUEST_LIKE_COMMENT_COUNT) {
            if (resultCode == RESULT_OK) {
                Logger.error(TAG, requestCode + " " + resultCode);
                Objects.requireNonNull(mContents.get(data.getIntExtra(HOME_ADAPTER_POSITION, -1))).setLikeCount(data.getStringExtra(Constants.Network.LIKE_COUNT));
                Logger.error(TAG, HOME_ADAPTER_POSITION + " " + data.getStringExtra(Constants.Network.LIKE_COUNT));
            }
        }
    }

    private void deleteData(String content_id, final int pos) {

        try {

            showProgressBar();
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.CONTENT_ID, content_id);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.DELETE_CONTENT);

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            hideProgressBar();
                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    if (mContents.size() > 0) {
                                        mContents.remove(pos);
                                        mHomeFragmentRecyclerAdapter.notifyItemRemoved(pos);
                                    }

                                    Toast.makeText(getActivity(), "Post Successfully Deleted", Toast.LENGTH_SHORT).show();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {

                                }


                            } catch (JSONException e) {

                                Logger.error(TAG, "In OnResponse" + e.getMessage());
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideProgressBar();

                            //to remove progress bar

                            if (error instanceof NetworkError)
                                showEmptyView(R.drawable.ic_signal_wifi_off, getString(R.string.connection_error_message));
                            else
                                showEmptyView(R.drawable.ic_error_outline, getString(R.string.something_went_wrong));

                            Logger.error(TAG, error.getMessage() + " is the volley error ");
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            hideProgressBar();
            Logger.error(TAG, "In fetchData" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}
