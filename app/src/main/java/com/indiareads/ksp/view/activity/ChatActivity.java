package com.indiareads.ksp.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.ChatActivityRecyclerAdapter;
import com.indiareads.ksp.listeners.OnFileCopiedListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Chat;
import com.indiareads.ksp.model.Conversation;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.UploadParams;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.service.UploadService;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.FileExtensionUtil;
import com.indiareads.ksp.utils.FilePicker;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class ChatActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_PERMISSION_STORAGE = 101;
    private static final int REQUEST_CODE_PERMISSION_CAMERA = 102;
    private static String TAG = ChatActivity.class.getSimpleName();
    public static String currentConversationId = "";
    private Button sendButton;
    private EditText editText;
    String fileNameCamera = "";
    private WrapContentLinearLayoutManager mLinearLayoutManager;
    private ChatActivityRecyclerAdapter mChatActivityRecylerAdapter;
    private RecyclerView mChatRecyclerView;
    private int pageNumber = 1;
    private ArrayList<Chat> mChats;
    private boolean doPagination = true;
    private boolean mChatsLoading = false;
    private String mConversationId;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private String mConversationName;
    String intentNavigation;
    private PubNub pubnub;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                // on Message Receive Adding to List
                UploadParams uploadParams = intent.getParcelableExtra(UploadService.UPLOAD_PARAMS);
                String status = intent.getStringExtra(UploadService.STATUS);

                Logger.info(TAG, "UPLOAD PARAMS: " + uploadParams.toString());

                String messageId = uploadParams.getEntityId();
                String attachmentId = uploadParams.getPath();

                Logger.debug(TAG, messageId + " " + attachmentId);

                if (mChats != null) {

                    Chat chat = null;
                    for (int i = 0; i < mChats.size(); i++) {
                        if (mChats.get(i).getId().equals(messageId)) {
                            chat = mChats.get(i);
                            break;
                        }
                    }

                    if (chat != null) {
                        chat.setAttachmentId(attachmentId);

                        if (status.equals(UploadService.STATUS_FAILED))
                            setMessageAsError(chat);
                        else if (status.equals(UploadService.STATUS_SUCCESS)) {
                            setMessageAsSuccess(chat);
                        }
                    }
                }
                File file = new File(Constants.File.ROOT_DIR_PATH + "/" + uploadParams.getFileName());
                if (file.exists())
                    file.delete();
            } catch (Exception e) {
                Logger.error(TAG, e.getMessage());
                e.printStackTrace();
            }
        }
    };


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ignoreCode();
        //getting Data from previos Activity
        getDataFromIntent();
        setupToolbar();
        // initialising Views
        initViews();
        initChatsList();
        // Set Adapter
        initChatRecyclerView();
        // Pagination on Scroll
        setRecyclerViewScrollListener();
        setRecyclerViewItemClickListener();
        // Api calling for Fetch Previos Chat
        fetchData();
        // Set on Click Listener for Send and Add File
        setClickListeners();
        initPubnub();
    }

    private void initPubnub() {
        pubnub = HomeActivity.getPubnubInstance();

        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {
//                Logger.info(TAG, "Pubnub Status:" + status);
                if (!(status.getCategory() == PNStatusCategory.PNConnectedCategory)) {
                    ChatActivity.this.pubnub.reconnect();
                }
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {
                Logger.info(TAG, "Pubnub Message:" + message.getMessage().toString());
                try {
                    JSONObject jsonObject1 = new JSONObject(message.getMessage().toString());

                    if (jsonObject1.getString(Constants.Network.USER_ID).equals(User.getCurrentUser(ChatActivity.this).getUserId()) &&
                            jsonObject1.has(Constants.Network.ORIGIN) &&
                            jsonObject1.getString(Constants.Network.ORIGIN).equals(Constants.Pubnub.PUBLISH_ORIGIN_ANDROID))
                        return;

                    final Chat chat = new Chat();
                    chat.setId(String.valueOf(System.currentTimeMillis()));
                    chat.setSenderId(jsonObject1.getString(Constants.Network.USER_ID));
                    chat.setMessage(jsonObject1.getString(Constants.Network.MAIN_MESSAGE));
                    chat.setAttachmentId(jsonObject1.getString(Constants.Network.ATTACHMENT_ID));
                    chat.setMessageType(Constants.Message.MESSAGE_TYPE_NORMAL);

                    if (!chat.getAttachmentId().equals(String.valueOf(Constants.Message.MESSAGE_TYPE_TEXT_VALUE))) {
                        chat.setAttachmentName(jsonObject1.getString(Constants.Network.ATTACHMENT_NAME));
                        chat.setMessageType(Constants.Message.MESSAGE_TYPE_FILE);
                    }

                    chat.setCreatedDate(CommonMethods.getCurrentDate("yy:MM:dd HH:mm:ss"));
                    chat.setStatus(Constants.Message.MESSAGE_STATUS_TYPE_NORMAL);

                    chat.setUserFullname(jsonObject1.getString(Constants.Network.FULLNAME));
                    chat.setUserImage(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                    chat.setSyncStatus(Constants.Message.SYNC_STATUS_DONE);

                    mChats.add(0, chat);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mChatActivityRecylerAdapter.notifyItemInserted(0);
                            mLinearLayoutManager.scrollToPosition(0);
                        }
                    });

                } catch (JSONException e) {
                    Logger.error(TAG, e.getMessage());
                }
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {
                Logger.info(TAG, "Pubnub Presence:" + presence);
            }
        });

        pubnub.subscribe().channels(Arrays.asList(mConversationId)).execute();
    }

    @SuppressLint("NewApi")
    public void ignoreCode() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    private void setClickListeners() {
        setSendButtonClickListener();
        setAddButtonClickListener();
    }

    private void setAddButtonClickListener() {
        findViewById(R.id.activity_chat_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Checking the Camera and Storage Permission
                // Permission are Granted. Calling Select File Dialog
                showSelectImageDialog();
                //
            }
        });
    }


    public void checkGallery() {
        if (ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // Permission are not Granted. Ask Permission Code
            ActivityCompat.requestPermissions(ChatActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_CODE_PERMISSION_STORAGE);
        } else {
            FilePicker.chooseFile(ChatActivity.this, FilePicker.REQUEST_CODE_CHOOSE_FILE);
        }
    }

    public void checkCamera() {

        if (ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // Permission are not Granted. Ask Permission Code
            ActivityCompat.requestPermissions(ChatActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_CODE_PERMISSION_CAMERA);
        } else {
            captureFromCamera();
        }
    }

    private void setSendButtonClickListener() {
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareMessage();
            }
        });
    }

    @SuppressLint("NewApi")
    private void prepareMessage() {
        String msg = editText.getText().toString().trim();
        msg = msg.replace("'", "''");

        msg = String.valueOf(Html.fromHtml(msg));


        if (!msg.isEmpty()) {
            Chat chat = createChatObject(msg);
            sendMessageRequest(chat);
            editText.setText("");
        }

    }

    private Chat createChatObject(String msg) {
        Chat chat = new Chat();
        chat.setSenderId(User.getCurrentUser(ChatActivity.this).getUserId());
        chat.setMessage(msg);
        chat.setAttachmentId(Constants.Message.MESSAGE_TYPE_TEXT_VALUE);
        chat.setMessageType(Constants.Message.MESSAGE_TYPE_NORMAL);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yy:MM:dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        chat.setCreatedDate(formattedDate);
        chat.setStatus(Constants.Message.MESSAGE_STATUS_TYPE_NORMAL);
        chat.setUserImage(User.getCurrentUser(ChatActivity.this).getProfilePic());
        chat.setUserFullname(User.getCurrentUser(ChatActivity.this).getFullName());
        chat.setSyncStatus(Constants.Message.SYNC_STATUS_IN_PROGRESS);
        mChats.add(0, chat);
        mChatActivityRecylerAdapter.notifyItemRangeInserted(0, 1);
        mLinearLayoutManager.scrollToPosition(0);
        return chat;
    }

    private void sendMessageRequest(final Chat chat) {

        // API Calling for Sending Message to Server
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONVO_ID, mConversationId);
            jsonObject.put(Constants.Network.ATTACHMENT_ID, chat.getAttachmentId());

            if (fileNameCamera.equals("")) {
                jsonObject.put(Constants.Network.ATTACHMENT_NAME, chat.getAttachmentName());
            } else {
                jsonObject.put(Constants.Network.ATTACHMENT_NAME, fileNameCamera);
            }

            jsonObject.put(Constants.Network.MESSAGE_TYPE, chat.getMessageType());
            jsonObject.put(Constants.Network.MESSAGE, chat.getMessage());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_SEND_MESSAGE_CHAT);

            // Request Building to POST Request to Server
            Logger.info(TAG, jsonObject.toString());
            Logger.error("FilePicker", "JSONRequest" + jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            Logger.error("FilePicker", "JSONRequest" + response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    JSONObject data = response.getJSONArray(Constants.Network.DATA).getJSONObject(0);
                                    chat.setId(data.getString(Constants.Network.ID));

                                    if (chat.getAttachmentId().equals(Constants.Message.MESSAGE_TYPE_TEXT_VALUE)) {
                                        chat.setSyncStatus(Constants.Message.SYNC_STATUS_DONE);
                                        publishOnPubnub(chat);
                                    } else {
                                        JSONObject jsonObject = getMessageUpdateRequest(chat);
                                        String filenme = "";
                                        if (fileNameCamera.equals("")) {
                                            filenme = chat.getAttachmentName();
                                        } else {
                                            filenme = fileNameCamera;
                                        }

                                        Logger.error("FilePicker", "responseFileName" + filenme.toString());
                                        UploadParams uploadParams = new UploadParams(Constants.Network.ACTION_UPDATE_ATTACHMENT
                                                , chat.getId()
                                                , filenme
                                                , ""
                                                , Constants.AWS.MEDIA_KEY
                                                , jsonObject);

                                        Logger.info(TAG, "UPLOAD PARAMS: " + uploadParams.toString());

                                        Intent uploadIntent = new Intent(ChatActivity.this, UploadService.class);
                                        uploadIntent.putExtra(UploadService.UPLOAD_PARAMS, uploadParams);
                                        startService(uploadIntent);
                                        mChatActivityRecylerAdapter.notifyItemChanged(mChats.indexOf(chat));
                                        fileNameCamera = "";
                                    }
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ChatActivity.this);
                                } else {
                                    Error.generateError(NullPointerException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                            "sendMessageRequest", Error.ERROR_TYPE_SERVER_ERROR, "Message not sent");
                                    setMessageAsError(chat);
                                }

                            } catch (JSONException e) {
                                Error.generateError(JSONException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                        "sendMessageRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                setMessageAsError(chat);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error.generateError(VolleyError.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                    "sendMessageRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            setMessageAsError(chat);
                        }
                    });

            jsonObjectRequest.setShouldCache(false);
            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            setMessageAsError(chat);
        }
    }

    private void publishOnPubnub(Chat chat) {
        try {
            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty(Constants.Network.TITLE, Constants.Pubnub.PUBLISH_TYPE_CHAT);
            jsonObject.addProperty(Constants.Network.MAIN_MESSAGE, chat.getMessage());
            jsonObject.addProperty(Constants.Network.FULLNAME, chat.getUserFullname());
            jsonObject.addProperty(Constants.Network.PROFILE_PIC, chat.getUserImage());
            jsonObject.addProperty(Constants.Network.USER_ID, chat.getSenderId());
            jsonObject.addProperty(Constants.Network.ATTACHMENT_ID, chat.getAttachmentId());
            jsonObject.addProperty(Constants.Network.ORIGIN, Constants.Pubnub.PUBLISH_ORIGIN_ANDROID);

            pubnub.publish()
                    .channel(mConversationId)
                    .message(jsonObject)
                    .async(
                            new PNCallback<PNPublishResult>() {
                                @Override
                                public void onResponse(PNPublishResult result, PNStatus status) {
                                    try {
                                        if (!status.isError()) {
                                            Logger.info(TAG, "publish(" + result.toString() + ")");
                                        } else {
                                            Logger.error(TAG, "publishErr(" + status.toString() + ")");
                                            if (status.getCategory() == PNStatusCategory.PNDisconnectedCategory) {
                                                pubnub.disconnect();
                                                pubnub.reconnect();
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                    );
        } catch (Exception e) {
            Logger.error(TAG, e.getMessage());
        }
    }

    private JSONObject getMessageUpdateRequest(Chat chat) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_UPDATE_ATTACHMENT);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.ATTACHMENT_ID, chat.getAttachmentId());
            jsonObject.put(Constants.Network.MESSAGE_ID, chat.getId());
        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "getMessageUpdateRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.displayToast(this, getString(R.string.something_went_wrong));
        }
        return jsonObject;
    }

    private void setMessageAsSuccess(Chat chat) {
        int chatPos = mChats.indexOf(chat);
        chat.setSyncStatus(Constants.Message.SYNC_STATUS_DONE);
        mChatActivityRecylerAdapter.notifyItemChanged(chatPos);
        publishOnPubnub(chat);
    }

    private void setMessageAsError(Chat chat) {
        int chatPos = mChats.indexOf(chat);
        chat.setSyncStatus(Constants.Message.SYNC_STATUS_FAILED);
        mChatActivityRecylerAdapter.notifyItemChanged(chatPos);
    }

    private void getDataFromIntent() {
        mConversationId = getIntent().getStringExtra(Constants.Network.CONVO_ID);
        mConversationName = getIntent().getStringExtra(Constants.Network.USER_FULL_NAME);
        if (getIntent().hasExtra("intent")) {
            intentNavigation = getIntent().getStringExtra("intent");
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.activity_chat_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mConversationName);
    }

    private void initViews() {
        mChatRecyclerView = findViewById(R.id.activity_chat_recyclerview);
        sendButton = findViewById(R.id.activity_chat_send);
        editText = findViewById(R.id.activity_chat_edittext);
    }

    private void initChatsList() {
        mChats = new ArrayList<>();
        addProgressBarInChatList();
    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(this);
    }

    private void addProgressBarInChatList() {
        // Adding Progress Bar while Loading Chat
        Chat progressBarChat = new Chat();
        progressBarChat.setMessageType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mChats.add(progressBarChat);
    }

    private void initChatRecyclerView() {
        mChatRecyclerView = findViewById(R.id.activity_chat_recyclerview);
        mLinearLayoutManager = new WrapContentLinearLayoutManager(this);
        mLinearLayoutManager.setReverseLayout(true);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mChatRecyclerView.setLayoutManager(mLinearLayoutManager);

        mChatActivityRecylerAdapter = new ChatActivityRecyclerAdapter(this, mChats);
        mChatRecyclerView.setAdapter(mChatActivityRecylerAdapter);
    }


    public void showSelectImageDialog() {

        final BottomSheetDialog myDialog = new BottomSheetDialog(this);

        myDialog.setContentView(R.layout.select_image);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView titleMain = myDialog.findViewById(R.id.titleMain);
        ImageView gallaryBtn = myDialog.findViewById(R.id.gallaryBtn);
        ImageView cameraBtn = myDialog.findViewById(R.id.cameraBtn);
        titleMain.setText("Select File");
        ImageView closeDialog = myDialog.findViewById(R.id.closeDialog);
        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Capture from Camera Code
                checkCamera();
                myDialog.dismiss();
            }
        });

        gallaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Select file from Gallery Code
                checkGallery();
                myDialog.dismiss();
            }
        });

        myDialog.show();

    }

    public void captureFromCamera() {

        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String imageFileName = timeStamp;

        fileNameCamera = imageFileName + ".jpg";
        // Creating Custom URI in Camera Code
        Intent m_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), fileNameCamera);
        Uri uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file);
        // Custom URI in "uri" Object
        m_intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(m_intent, REQUEST_IMAGE_CAPTURE);
    }

    private void setRecyclerViewItemClickListener() {
        mChatActivityRecylerAdapter.setOnRecyclerItemClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Chat chat = mChats.get(position);
                if (chat.getMessageType().equals(Constants.Message.MESSAGE_TYPE_FILE) && chat.getSyncStatus().equals(Constants.Message.SYNC_STATUS_DONE)) {
                    viewItem(chat);
                }
            }
        });
    }

    private void viewItem(Chat chat) {
        // On Click when Click on Any Attachment of Chat
        Logger.info(TAG, Urls.AWS_MESSAGE_IMAGE_PATH + chat.getAttachmentId());
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Urls.AWS_MESSAGE_IMAGE_PATH + chat.getAttachmentId()));
            startActivity(browserIntent);
        } catch (Exception e) {
            Toast.makeText(ChatActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            Logger.info(TAG, e.getMessage());
        }
    }

    private void setRecyclerViewScrollListener() {

        mChatRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPagination && dy < 0) //check for scroll up
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mLinearLayoutManager.getChildCount();
                    totalItemCount = mLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                    if (!mChatsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mChatsLoading = true;
                            addProgressBarInChatList();
                            mChatActivityRecylerAdapter.notifyItemRangeInserted(mChats.size() - 1, mChats.size() - 2);
                            pageNumber++;
                            fetchData();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    // Fetching All Previons Chat
    private void fetchData() {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE, pageNumber);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_CHAT);
            jsonObject.put(Constants.Network.CONVO_ID, mConversationId);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mChats.remove(mChats.size() - 1);

                                    mChatActivityRecylerAdapter.notifyItemRemoved(mChats.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Chat chat = new Chat();
                                        chat.setId(jsonObject1.getString(Constants.Network.ID));
                                        chat.setSenderId(jsonObject1.getString(Constants.Network.SENDER_ID));
                                        chat.setMessage(jsonObject1.getString(Constants.Network.MESSAGE));
                                        chat.setAttachmentId(jsonObject1.getString(Constants.Network.ATTACHMENT_ID));
                                        chat.setAttachmentName(jsonObject1.getString(Constants.Network.ATTACHMENT_NAME));
                                        chat.setMessageType(jsonObject1.getString(Constants.Network.MESSAGE_TYPE));
                                        chat.setReadStatus(jsonObject1.getString(Constants.Network.READ_STATUS));
                                        chat.setDeleteStatusR(jsonObject1.getString(Constants.Network.DELETE_STATUS_R));
                                        chat.setDeleteStatusS(jsonObject1.getString(Constants.Network.DELETE_STATUS_S));
                                        chat.setCreatedDate(jsonObject1.getString(Constants.Network.CREATED_DATE));
                                        chat.setStatus(jsonObject1.getString(Constants.Network.STATUS));
                                        chat.setSenderOrReceiver(jsonObject1.getString(Constants.Network.SENDER_OR_RECEIVER));

                                        if (jsonObject1.has(Constants.Network.USER_FULLNAME)) {
                                            chat.setUserImage(jsonObject1.getString(Constants.Network.USER_IMAGE));
                                            chat.setUserFullname(jsonObject1.getString(Constants.Network.USER_FULLNAME));
                                        } else {
                                            chat.setUserImage(User.getCurrentUser(ChatActivity.this).getProfilePic());
                                            chat.setUserFullname(User.getCurrentUser(ChatActivity.this).getFullName());
                                        }

                                        try {
                                            if (!chat.getAttachmentId().equals(Constants.Message.MESSAGE_TYPE_TEXT_VALUE)) {
                                                if (chat.getAttachmentId().contains(Constants.Message.TEMP_ATTACHMENT) &&
                                                        (!chat.getSenderId().trim().equals(User.getCurrentUser(ChatActivity.this).getUserId().trim()))) {
                                                    jsonArray.remove(i);
                                                    continue;
                                                } else {
                                                    if (chat.getAttachmentId().contains(Constants.Message.TEMP_ATTACHMENT)) {
                                                        String elapsedTime = CommonMethods.getElapsedTime(chat.getCreatedDate());
                                                        if (elapsedTime.contains("moments") || (elapsedTime.contains("minutes") && Integer.parseInt(elapsedTime.split(" ")[0]) < 5)) {
                                                            chat.setSyncStatus(Constants.Message.SYNC_STATUS_IN_PROGRESS);
                                                        } else {
                                                            jsonArray.remove(i);
                                                            continue;
                                                        }
                                                    } else {
                                                        chat.setSyncStatus(Constants.Message.SYNC_STATUS_DONE);
                                                    }
                                                }
                                            } else {
                                                chat.setSyncStatus(Constants.Message.SYNC_STATUS_DONE);
                                            }
                                        } catch (Exception e) {
                                            Logger.error(TAG, e.getMessage());
                                            jsonArray.remove(i);
                                            continue;
                                        }
                                        mChats.add(chat);
                                    }

                                    mChatActivityRecylerAdapter.notifyItemRangeInserted(mChats.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ChatActivity.this);
                                } else {

                                    //stop call to pagination in any case
                                    doPagination = false;

                                    //to remove progress bar
                                    mChats.remove(mChats.size() - 1);
                                    mChatActivityRecylerAdapter.notifyItemRemoved(mChats.size() - 1);

                                    if (pageNumber == 1) {

                                        if (getIntent().getStringExtra(Constants.Network.CHAT_TYPE) != null &&
                                                getIntent().getStringExtra(Constants.Network.CHAT_TYPE).equals(Conversation.CHAT_TYPE_GROUP)) {
                                            Chat chat = createChatObject("Hi, welcome to " + mConversationName);
                                            sendMessageRequest(chat);
                                        }

                                    } else {
                                        //no more contents
                                    }
                                }

                                mChatsLoading = false;

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), ChatActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(this, R.id.activity_chat_root_layout, error);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == FilePicker.REQUEST_CODE_CHOOSE_FILE) {
            if (resultCode == RESULT_OK) {
                if (resultData != null) {
                    Uri uri = resultData.getData();
                    ArrayList<String> list1 = FileExtensionUtil.checkForRepo();
                    String fileExtension1 = CommonMethods.getExtensionFile(this, uri);
                    if (list1.contains(fileExtension1)) {
                        copyFileToLocalStorage(uri);
                    } else {
                        CommonMethods.displayToast(ChatActivity.this, ChatActivity.this.getString(R.string.file_not_supported));
                    }
                } else {
                    CommonMethods.displayToast(ChatActivity.this, ChatActivity.this.getString(R.string.something_went_wrong));
                }
            }
        } else if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                //File object of camera image
                File file = new File(Environment.getExternalStorageDirectory(), fileNameCamera);
                Logger.info("file", file.getAbsolutePath());
                //Uri of camera image
                Uri uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file);
                Logger.info("file", uri.toString());
                copyFileToLocalStorage(uri);
            }
        }
    }

    private void copyFileToLocalStorage(final Uri uri) {
        Logger.error("FilePicker", "OnActi" + uri.toString());
        final Dialog dialog = new Dialog(ChatActivity.this);
        dialog.setContentView(R.layout.progress_dialog_layout);
        dialog.setCancelable(false);
        dialog.show();
        //  Uri uri = Uri.parse(uri);
        FilePicker.copyFileFromUri(this, uri, new OnFileCopiedListener() {
            @Override
            public void onFileCopied(File file) {
                Logger.error("FilePicker", "FilePath" + file.getAbsolutePath());

                Chat fileChat = createFileChatObject(uri);
                Logger.info("ONEditFragmentCamera", fileChat.getAttachmentName() + "asdas" + uri);
                sendMessageRequest(fileChat);
                dialog.dismiss();
            }

            @Override
            public void onFileCopyFailed(Error error) {
//                CommonMethods.displayToast(ChatActivity.this, ChatActivity.this.getString(R.string.something_went_wrong));
                dialog.dismiss();
            }
        });
    }

    private Chat createFileChatObject(Uri uri) {
        try {
            Chat chat = new Chat();
            chat.setSenderId(User.getCurrentUser(ChatActivity.this).getUserId());
            chat.setMessage("");
            Logger.error("FilePicker", "FileNameCreate" + fileNameCamera);
            if (fileNameCamera.equals("")) {
                chat.setAttachmentName(FilePicker.getFileName(ChatActivity.this.getContentResolver(), uri));
            } else {
                chat.setAttachmentName(fileNameCamera);
            }

            chat.setAttachmentId(Constants.Message.TEMP_ATTACHMENT);
            chat.setMessageType(Constants.Message.MESSAGE_TYPE_FILE);

            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yy:MM:dd HH:mm:ss");
            String formattedDate = df.format(c.getTime());

            chat.setCreatedDate(formattedDate);
            chat.setStatus(Constants.Message.MESSAGE_STATUS_TYPE_NORMAL);
            chat.setUserImage(User.getCurrentUser(ChatActivity.this).getProfilePic());
            chat.setUserFullname(User.getCurrentUser(ChatActivity.this).getFullName());
            chat.setSyncStatus(Constants.Message.SYNC_STATUS_IN_PROGRESS);

            mChats.add(0, chat);

            mChatActivityRecylerAdapter.notifyItemInserted(0);

            mLinearLayoutManager.scrollToPosition(0);

            return chat;
        } catch (Exception e) {
            Error.generateError(Exception.class.getSimpleName(), TAG
                    , "createFileChatObject", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is paused.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
        currentConversationId = "";
        super.onPause();
    }

    @Override
    protected void onResume() {
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(Constants.Network.ACTION_UPDATE_ATTACHMENT));
        currentConversationId = mConversationId;
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat_activity, menu);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_PERMISSION_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                FilePicker.chooseFile(ChatActivity.this, FilePicker.REQUEST_CODE_CHOOSE_FILE);
            else
                CommonMethods.displayToast(ChatActivity.this, getString(R.string.permissions_denied));
        } else if (requestCode == REQUEST_CODE_PERMISSION_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                captureFromCamera();
            else
                CommonMethods.displayToast(ChatActivity.this, getString(R.string.permissions_denied));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.activity_chat_menu_info:
                Intent chatIntent = new Intent(this, ChatInfoActivity.class);
                chatIntent.putExtra(Constants.Network.CONVO_ID, mConversationId);
                chatIntent.putExtra(Constants.Network.USER_FULL_NAME, mConversationName);
                startActivity(chatIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pubnub.unsubscribe().channels(Arrays.asList(mConversationId)).execute();
        pubnub.disconnect();
    }

    public class WrapContentLinearLayoutManager extends LinearLayoutManager {

        public WrapContentLinearLayoutManager(Context context) {
            super(context);
        }

        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Logger.error(TAG, e.getMessage());
            }
        }
    }
}