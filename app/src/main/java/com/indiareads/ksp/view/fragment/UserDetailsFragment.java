package com.indiareads.ksp.view.fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.UserDetailsViewPagerAdapter;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.listeners.OnUpdateUserDetailsListener;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CircleTransform;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.indiareads.ksp.utils.Constants.Network.CODE;

public class UserDetailsFragment extends Fragment implements OnRefreshListener, OnUpdateUserDetailsListener {

    private static final String TAG = UserDetailsFragment.class.getSimpleName();
    public static final String PAGE_NO = "page_no";
    private View fragmentRootView;


    private String mUserId;
    private String mCompanyId;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    String imagePathLatest = "";
    private ImageView profileImageView;
    private TextView nameTextView;
    private TextView designationTextView;
    private TextView rankTextView;
    private TextView pointsTextView;
    private TextView contentCreatedTextView;
    private TextView contentCompletedTextView;
    int pageNo;
    private User mUser;
    RelativeLayout linear;
    LinearLayout linearLayout;

    private BroadcastReceiver broadcastReceiver;

    public UserDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getDataFromArguments();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentRootView = inflater.inflate(R.layout.fragment_user_details, container, false);

        fetchViews();
        setUpViewPager();
        getUserDetails();
        setListeners();


        return fragmentRootView;
    }

    private void setListeners() {
        fragmentRootView.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null)
                    getActivity().finish();
            }
        });
    }

    private void getDataFromArguments() {
        if (getArguments() != null) {
            pageNo = getArguments().getInt(PAGE_NO, 0);
            mUserId = getArguments().getString(Constants.Network.USER_ID, "");
            mCompanyId = getArguments().getString(Constants.Network.COMPANY_ID, "");

            Logger.error("POSTINTENT_USERFRAG", mUserId + " < UserID    Company ID >" + mCompanyId);
        } else {
            Error error = Error.generateError(NullPointerException.class.getSimpleName(), UserDetailsFragment.class.getSimpleName(),
                    "getDataFromArguments", Error.ERROR_TYPE_NORMAL_ERROR, "Arguments Null");

            displayErrorUI(error);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver((broadcastReceiver),
//                new IntentFilter("URL")
//        );
    }

    private void fetchViews() {
        linearLayout = fragmentRootView.findViewById(R.id.linearLayout);
        tabLayout = fragmentRootView.findViewById(R.id.activity_user_details_tablayout);
        viewPager = fragmentRootView.findViewById(R.id.activity_user_details_viewpager);
        linear = fragmentRootView.findViewById(R.id.linear);


        profileImageView = fragmentRootView.findViewById(R.id.activity_user_details_profile_imageview);
        nameTextView = fragmentRootView.findViewById(R.id.activity_user_details_user_name);
        designationTextView = fragmentRootView.findViewById(R.id.activity_user_details_designation);
        rankTextView = fragmentRootView.findViewById(R.id.activity_user_details_rank);
        pointsTextView = fragmentRootView.findViewById(R.id.activity_user_details_points);
        contentCreatedTextView = fragmentRootView.findViewById(R.id.activity_user_details_content_created_count);
        contentCompletedTextView = fragmentRootView.findViewById(R.id.activity_user_details_content_completed_count);

        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewImage();
            }
        });
        nameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserId.equals(User.getCurrentUser(getActivity()).getUserId()))
                    viewPager.setCurrentItem(7);
            }
        });

    }

    public void viewImage() {
        final Dialog settingsDialog = new Dialog(getActivity());
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout
                , null));

        ImageView imageView = settingsDialog.findViewById(R.id.imageProfile);
        Picasso.get().load(imagePathLatest).into(imageView);

        ImageView cross = settingsDialog.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });
        settingsDialog.show();
    }

    private void setUpViewPager() {

        List<String> tabTitles = new ArrayList<>();

        //
        Bundle argumentsPost = new Bundle();
        argumentsPost.putString(Constants.Network.USER_ID, mUserId);
        argumentsPost.putString(Constants.Network.REQUIRED_CONTENT_TYPE, String.valueOf(Constants.Content.CONTENT_TYPE_POST));
        //
        Bundle argumentsContentFragment = new Bundle();
        argumentsContentFragment.putString(Constants.Network.USER_ID, mUserId);
        String userMain = User.getCurrentUser(getActivity()).getUserId();

        if (mUserId.equals(userMain)) {
            argumentsContentFragment.putString("Type", "Profile");
        } else {
            argumentsContentFragment.putString("Type", "Another_Profile");
        }
        argumentsContentFragment.putString(Constants.Network.SKIP_CONTENT_TYPE, String.valueOf(Constants.Content.CONTENT_TYPE_POST) + "," + String.valueOf(Constants.Content.CONTENT_TYPE_ANNOUNCEMENT));
        //
        Bundle argumentsRepositoryFragment = new Bundle();
        argumentsRepositoryFragment.putString(RepositoryFragment.REPO_TYPE, RepositoryFragment.REPO_TYPE_PERSONAL);
        //
        Bundle argumentsBookFragment = new Bundle();
        argumentsBookFragment.putString(Constants.Network.USER_ID, mUserId);
        //
        Bundle arguments = new Bundle();
        arguments.putBundle(UserDetailsViewPagerAdapter.ARG_POST_FRAGMENT, argumentsPost);
        arguments.putBundle(UserDetailsViewPagerAdapter.ARG_CONTENT_FRAGMENT, argumentsContentFragment);
        arguments.putBundle(UserDetailsViewPagerAdapter.MY_REPOSITORY_FRAGMENT, argumentsRepositoryFragment);
        arguments.putBundle(UserDetailsViewPagerAdapter.ARG_BOOK_SHELF_FRAGMENT, argumentsBookFragment);

        if (mUserId.equals(User.getCurrentUser(getActivity()).getUserId())) {

            tabTitles.add(getString(R.string.my_posts));
            tabTitles.add(getString(R.string.my_contents));
            tabTitles.add(getString(R.string.reading_shelf));
            tabTitles.add(getString(R.string.bookshelf));
            tabTitles.add(getString(R.string.my_orders));
            tabTitles.add(getString(R.string.drafted_content));
            tabTitles.add(getString(R.string.my_repository));
            tabTitles.add(getString(R.string.edit_profile));
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        } else {

            tabTitles.add(getString(R.string.posts));
            tabTitles.add(getString(R.string.contents));

            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        }

        viewPager.setAdapter(new UserDetailsViewPagerAdapter(getChildFragmentManager(), this, tabTitles, arguments));
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_layout, null);
            TextView tv = view.findViewById(R.id.text1);
            tv.setText(tabLayout.getTabAt(i).getText());
            tabLayout.getTabAt(i).setCustomView(tv);
        }

        if (pageNo > 0) {
            viewPager.setCurrentItem(pageNo);
        }

    }

    public void handleResult(Fragment frag, int requestCode, int resultCode, Intent data) {
        Logger.info(TAG, "ONUserFragment");
        List<Fragment> frags = frag.getChildFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null) {
                    f.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Logger.info(TAG, "ONUserFragment");
        List<Fragment> frags = getActivity().getSupportFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null)
                    handleResult(f, requestCode, resultCode, data);
            }
        }
    }

    private void getUserDetails() {

        //start progress bar fragment
        if (pageNo == 0)
            CommonMethods.showProgressBar(fragmentRootView, R.id.fragment_user_details_root_layout);
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTIO_GET_USER_DETAILS);
            jsonObject.put(Constants.Network.USER_ID, mUserId);
            jsonObject.put(Constants.Network.COMPANY_ID, mCompanyId);

            Logger.info(TAG, jsonObject.toString());
            Logger.error("POSTINTENT_USERFRAG", jsonObject.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.HOMEPAGE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Logger.info(TAG, response.toString());
                    Logger.error("POSTINTENT_USERFRAG", response.toString());
                    try {
                        if (response.getString(CODE).equals(Constants.Network.RESPONSE_OK))
//                            if(response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK))
                        {
                            mUser = new User();

                            JSONObject userObject = response.getJSONObject(Constants.Network.DATA);

                            mUser.setProfilePic(userObject.getString(Constants.Network.PROFILE_PIC));
                            mUser.setFullName(userObject.getString(Constants.Network.USER_FULL_NAME));
                            mUser.setDesignation(userObject.getString(Constants.Network.DESIGNATION));
                            mUser.setRank(userObject.getString(Constants.Network.USER_RANK));
                            JSONArray jsonArray = userObject.getJSONArray(Constants.Network.USER_TOTAL_POINTS);
                            if (jsonArray.length() > 0) {
                                mUser.setPoints(jsonArray.getJSONObject(0).getString(Constants.Network.TOTAL_POINTS));
                            }
                            mUser.setCreatedContents(userObject.getString(Constants.Network.CONTENT_CREATED_COUNT));
                            mUser.setConsumedContents(userObject.getString(Constants.Network.CONTENT_CONSUMED_COUNT));
                            imagePathLatest = Urls.AWS_PROFILE_IMAGE_PATH + mUser.getProfilePic();
                            setUserDetails();
                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), UserDetailsFragment.class.getSimpleName(),
                                    "getUserDetails", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);

                            displayErrorUI(error);
                        }
                        CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_user_details_root_layout);
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), UserDetailsFragment.class.getSimpleName(),
                                "getUserDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        Logger.error("POSTINTENT_USERFRAG", e.getMessage());
                        displayErrorUI(error);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    Error error = Error.generateError(VolleyError.class.getSimpleName(), UserDetailsFragment.class.getSimpleName(),
                            "getUserDetails", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                    displayErrorUI(error);
                    Logger.error("POSTINTENT_USERFRAG", error.getMessage());
                    CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_user_details_root_layout);
                }
            });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), UserDetailsFragment.class.getSimpleName(),
                    "getUserDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);
            Logger.error("POSTINTENT_USERFRAG", e.getMessage());
            CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_user_details_root_layout);
        }
    }

    private void setUserDetails() {
        try {
            if (mUser.getProfilePic() != null && !mUser.getProfilePic().equalsIgnoreCase("null"))
                CommonMethods.loadProfileImageWithGlide(getActivity(), mUser.getProfilePic(), profileImageView);

            nameTextView.setText(mUser.getFullName());
            designationTextView.setText(mUser.getDesignation());

            rankTextView.setText(mUser.getRank());
            if (mUser.getPoints() != null) {
                pointsTextView.setText(mUser.getPoints());
            } else {
                pointsTextView.setText("0");
            }

            contentCreatedTextView.setText(mUser.getCreatedContents());
            contentCompletedTextView.setText(mUser.getConsumedContents());

        } catch (Exception e) {

        }
    }

    private void displayErrorUI(Error error) {
        CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_user_details_root_layout);
        CommonMethods.showErrorView(UserDetailsFragment.this, R.id.fragment_user_details_root_layout, error);
    }

    @Override
    public void onRefresh() {
        setUpViewPager();
        getUserDetails();
    }

    @Override
    public void OnUpdateUserDetails(User user) {
        Logger.info(TAG, "onUpdateUserDetails:" + user.getProfilePic());
        imagePathLatest = user.getProfilePic();
        Picasso.get().load(user.getProfilePic()).transform(new CircleTransform()).into(profileImageView);
        nameTextView.setText(user.getFullName());
        designationTextView.setText(user.getDesignation());
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}