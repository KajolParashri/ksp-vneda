package com.indiareads.ksp.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.PointsAdapter;
import com.indiareads.ksp.adapters.PointsUserRecyclerAdapter;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.PointsModel;
import com.indiareads.ksp.model.PointsUserModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.MyProgresbar;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.ContentFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LeaderBoardActivity extends AppCompatActivity {
    ArrayList<PointsModel> created;
    CardView created_Rel, curated_Rel;
    ArrayList<PointsModel> curated;
    ImageView createdDown, curatedDown;
    TextView content_count_text, created_total, content_consume_count_text, consume_total;
    TextView rank_text, points;
    TextView dividerCreate, dividerCurate;
    RecyclerView leader_recycler, created_recycler, consumed_recycler;
    MyProgresbar myProgresbar;
    String created_status = "hide", curated_status = "hide";
    ArrayList<PointsUserModel> arrayList;
    ImageView backBtn;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);
        initViews();

        showProgressBar();
        fetchData();

        fetchTopTen();
    }

    public void showProgressBar() {
        if (myProgresbar == null)
            myProgresbar = MyProgresbar.show(this, R.style.Theme_MyprogressDialog_yellow);
    }

    public void hideProgressBar() {
        if (myProgresbar != null) {
            myProgresbar.dismiss();
            myProgresbar = null;
        }
    }


    public void initViews() {
        backBtn = findViewById(R.id.backBtn);
        created_Rel = findViewById(R.id.created_Rel);
        curated_Rel = findViewById(R.id.curated_Rel);
        dividerCreate = findViewById(R.id.dividerCreate);
        dividerCurate = findViewById(R.id.dividerCurate);
        dividerCurate.setVisibility(View.GONE);
        dividerCreate.setVisibility(View.GONE);

        createdDown = findViewById(R.id.createdDown);
        curatedDown = findViewById(R.id.curatedDown);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        created_Rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (created_status.equals("hide")) {

                    dividerCreate.setVisibility(View.VISIBLE);
                    created_recycler.setVisibility(View.VISIBLE);
                    PointsAdapter pointsAdapter = new PointsAdapter(LeaderBoardActivity.this, created);
                    created_recycler.setAdapter(pointsAdapter);
                    created_status = "visible";
                    createdDown.setRotation(180);

                } else if (created_status.equals("visible")) {

                    dividerCreate.setVisibility(View.GONE);
                    created_recycler.setVisibility(View.GONE);
                    created_status = "hide";
                    createdDown.setRotation(0);

                }
            }
        });


        curated_Rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (curated_status.equals("hide")) {

                    consumed_recycler.setVisibility(View.VISIBLE);
                    dividerCurate.setVisibility(View.VISIBLE);
                    PointsAdapter pointsAdapter = new PointsAdapter(LeaderBoardActivity.this, curated);
                    consumed_recycler.setAdapter(pointsAdapter);

                    curated_status = "visible";
                    curatedDown.setRotation(180);

                } else if (curated_status.equals("visible")) {

                    dividerCurate.setVisibility(View.GONE);
                    consumed_recycler.setVisibility(View.GONE);
                    curated_status = "hide";
                    curatedDown.setRotation(0);
                }
            }
        });

        content_count_text = findViewById(R.id.content_count_text);
        created_total = findViewById(R.id.created_total);
        content_consume_count_text = findViewById(R.id.content_consume_count_text);
        consume_total = findViewById(R.id.consume_total);
        rank_text = findViewById(R.id.rank_text);
        points = findViewById(R.id.points);

        leader_recycler = findViewById(R.id.leader_recycler);
        leader_recycler.setLayoutManager(new LinearLayoutManager(this));

        created_recycler = findViewById(R.id.created_recycler);
        created_recycler.setLayoutManager(new LinearLayoutManager(this));
        created_recycler.setVisibility(View.GONE);


        consumed_recycler = findViewById(R.id.consumed_recycler);
        consumed_recycler.setLayoutManager(new LinearLayoutManager(this));
        consumed_recycler.setVisibility(View.GONE);

        created = new ArrayList<>();
        curated = new ArrayList<>();


    }

    private void fetchData() {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.GET_POINTS);

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.LEADERBOARD_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info("TAG", response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    JSONObject data = response.getJSONObject("data");

                                    JSONObject data8 = data.getJSONObject("8");
                                    Logger.info("TAG", data8.toString());
                                    rank_text.setText(data8.getString(Constants.LeaderBoard.RANK));
                                    points.setText(data8.getString(Constants.LeaderBoard.TOTAL_POINTS));

                                    content_count_text.setText("You Have Created " + data8.getString(Constants.LeaderBoard.COUNT_CREATE) + " Contents");
                                    created_total.setText(data8.getString(Constants.LeaderBoard.POINTS_CREATE));
                                    content_consume_count_text.setText("You Have Consumed " + data8.getString(Constants.LeaderBoard.COUNT_CONSUME) + " Contents");
                                    consume_total.setText(data8.getString(Constants.LeaderBoard.POINTS_CONSUME));


                                    for (int i = 1; i < 7; i++) {
                                        String mainItem = getJSONObject(i);
                                        JSONObject jsonObject1 = data.getJSONObject(mainItem);

                                        PointsModel pointsModel = new PointsModel();

                                        pointsModel.setSUM(jsonObject1.getString(Constants.LeaderBoard.CREATE_POINTS_INDIVIDUAL));
                                        String contentName = jsonObject1.getString(Constants.LeaderBoard.CONTENT_TYPE);
                                        String content_type = CommonMethods.getContentNameFromType(contentName);
                                        String ss=content_type+"s";
                                        ss=ss.replace("ss","s");
                                        pointsModel.setContent_name(ss);
                                        pointsModel.setCount(jsonObject1.getString(Constants.LeaderBoard.COUNT));
                                        created.add(pointsModel);

                                        PointsModel pointsModel1 = new PointsModel();
                                        pointsModel1.setSUM(jsonObject1.getString(Constants.LeaderBoard.CONSUME_POINTS));
                                        String contentName1 = jsonObject1.getString(Constants.LeaderBoard.CONTENT_TYPE);
                                        String content_type1 = CommonMethods.getContentNameFromType(contentName1);
                                        String sss=content_type1+"s";
                                        sss=sss.replace("ss","s");
                                        pointsModel1.setContent_name(sss);
                                        pointsModel1.setCount(jsonObject1.getString(Constants.LeaderBoard.CONSUME_COUNT));
                                        curated.add(pointsModel1);

                                    }

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(LeaderBoardActivity.this);
                                }
                            } catch (Exception e) {
                                hideProgressBar();
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                e.printStackTrace();
                                Logger.info("TAG", "execption");
                            }

                        }

                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    hideProgressBar();
                                    Logger.info("TAG", "execption");
                                    Toast.makeText(LeaderBoardActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                                    volleyError.printStackTrace();
                                    Error error = Error.generateError(VolleyError.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                            "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                                }
                            });

            jsonObjectRequest.setTag("TAG");
            VolleySingleton.getInstance(this).

                    addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {

            hideProgressBar();
            Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

        }
    }

    public String getJSONObject(int i) {
        String key = "";
        if (i == 1) {
            key = "1";
        } else if (i == 2) {
            key = "2";
        } else if (i == 3) {
            key = "3";
        } else if (i == 4) {
            key = "4";
        } else if (i == 5) {
            key = "5";
        } else if (i == 6) {
            key = "7";
        }
        return key;
    }

    private void fetchTopTen() {
        try {
            arrayList = new ArrayList<>();
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.GET_TOP_TEN);

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.LEADERBOARD_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info("TAG", response.toString());
                            hideProgressBar();
                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONArray data = response.getJSONArray(Constants.Network.DATA);
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject listData = data.getJSONObject(i);
                                        PointsUserModel model = new PointsUserModel();
                                        model.setContentType(3);
                                        //"user_id":"233338","company_id":"92
                                        model.setCompanyId(listData.getString(Constants.LeaderBoard.COMPANY_ID));
                                        model.setUserId(listData.getString(Constants.LeaderBoard.USER_ID));
                                        model.setUserName(listData.getString(Constants.LeaderBoard.FIRST_NAME));
                                        model.setUserPoints("Points " + listData.getString(Constants.LeaderBoard.SCORE));
                                        model.setUserImage(listData.getString(Constants.LeaderBoard.PROFILE_PIC));
                                        arrayList.add(model);
                                    }

                                    PointsUserRecyclerAdapter pointsUserRecyclerAdapter = new PointsUserRecyclerAdapter(LeaderBoardActivity.this, arrayList);
                                    leader_recycler.setAdapter(pointsUserRecyclerAdapter);
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(LeaderBoardActivity.this);
                                }

                            } catch (Exception e) {
                                hideProgressBar();
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                e.printStackTrace();
                            }

                        }

                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    hideProgressBar();
                                    volleyError.printStackTrace();
                                }
                            });

            jsonObjectRequest.setTag("TAG");
            VolleySingleton.getInstance(this).

                    addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {
            hideProgressBar();
        }
    }

}
