package com.indiareads.ksp.view.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.BookShelfPagerAdapter;
import com.indiareads.ksp.adapters.ReadingShelfPagerAdapter;
import com.indiareads.ksp.adapters.UserDetailsViewPagerAdapter;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookShelfFragment extends Fragment {

    ViewPager viewPager;
    TabLayout tabLayout;
    String mUserId;

    public BookShelfFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mFragmentRootView = inflater.inflate(R.layout.fragment_book_shelf, container, false);
        mUserId = User.getCurrentUser(getActivity()).getUserId();
        setUpViewPager(mFragmentRootView);

        return mFragmentRootView;
    }

    private void setUpViewPager(View mFragmentRootView) {

        viewPager = mFragmentRootView.findViewById(R.id.book_shelf_viewpager);
        tabLayout = mFragmentRootView.findViewById(R.id.book_shelf_tablayout);
        viewPager.setAdapter(new BookShelfPagerAdapter(getChildFragmentManager(), getActivity()));
        viewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_layout, null);
            TextView tv = view.findViewById(R.id.text1);
            tv.setText(tabLayout.getTabAt(i).getText());
            tabLayout.getTabAt(i).setCustomView(tv);
        }
    }

}
