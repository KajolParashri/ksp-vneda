package com.indiareads.ksp.view.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnFileCopiedListener;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.FileExtensionUtil;
import com.indiareads.ksp.utils.FilePicker;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import jp.wasabeef.richeditor.RichEditor;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;
import static com.indiareads.ksp.utils.CommonMethods.getStringSizeLengthFile;
import static com.indiareads.ksp.utils.Constants.AWS.BUCKET_NAME;
import static com.indiareads.ksp.utils.Constants.Article.ACTION;
import static com.indiareads.ksp.utils.Constants.Article.ARTICLE;
import static com.indiareads.ksp.utils.Constants.Article.CONTENT_ID;
import static com.indiareads.ksp.utils.Constants.Article.DRAFT_STATUS;
import static com.indiareads.ksp.utils.Constants.Article.PRIVACY_STATUS;
import static com.indiareads.ksp.utils.Constants.Article.SAVE_ARTICLE_DATA;
import static com.indiareads.ksp.utils.Constants.Article.TITLE;
import static com.indiareads.ksp.utils.Constants.Content.COURSE;
import static com.indiareads.ksp.utils.Constants.Network.DATA;
import static com.indiareads.ksp.utils.Constants.Network.RESPONSE_CODE;
import static com.indiareads.ksp.utils.Constants.Network.RESPONSE_OK;
import static com.indiareads.ksp.utils.FilePicker.REQUEST_CODE_CHOOSE_AUDIO;
import static com.indiareads.ksp.utils.FilePicker.REQUEST_CODE_CHOOSE_IMAGE;
import static com.indiareads.ksp.utils.FilePicker.REQUEST_CODE_CHOOSE_PDF;
import static com.indiareads.ksp.utils.FilePicker.REQUEST_CODE_CHOOSE_VIDEO;
import static com.indiareads.ksp.utils.FilePicker.chooseFile;
import static com.indiareads.ksp.utils.SharedPreferencesHelper.getCurrentUser;
import static com.indiareads.ksp.utils.Urls.baseAWSUrl;

public class EditorActivity extends AppCompatActivity {

    private static final String TAG = EditorActivity.class.getSimpleName();

    private static final int TYPE_IMAGE = 8771;
    private static final int TYPE_VIDEO = 8772;
    private static final int TYPE_AUDIO = 8773;
    private static final int TYPE_PDF = 8774;
    private String data;
    private String title;
    String fileUploadStatus = "false";
    boolean isCheck = false;
    private RichEditor mEditor;
    BottomSheetDialog mDialog;
    private static final String PUBLIC = "1";
    private static final String PRIVATE = "2";

    private String whichData;
    private String content_type = "1";
    private Dialog mMediaDialog;
    private EditText mEnterUrl;

    private Dialog mLinkDialog;
    private EditText mLinkDesc;
    private EditText mLinkURL;
    String intenttype = "";
    private Dialog mAlertDialog;

    private Dialog mCodeDialog;
    private EditText mAddCode;
    FrameLayout mRootLayout;
    private RelativeLayout draftLayout;
    private String mCurrentHtmlData = "";

    private String typeIntent = "";
    ProgressBar progressBar;
    public int draftValue = 0;

    private boolean insertAction = false;
    ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        checkIfPermissionToBeAsked();
        getIntentData();
        setupToolbar();
        setupEditor();

        showAppropriateExistingData();
    }

    private void getIntentData() {
        Intent intent = getIntent();

        if (intent.hasExtra("typeIntent")) {
            typeIntent = intent.getStringExtra("typeIntent");
        }

        whichData = intent.getStringExtra("type");
        title = intent.getStringExtra(TITLE);
        data = intent.getStringExtra("data");
        content_type = intent.getStringExtra("content_type");

    }

    public void showProgressBar() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgressBar() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setupToolbar() {
        backBtn = findViewById(R.id.backBtn);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        draftLayout = findViewById(R.id.draftLayout);
        mRootLayout = findViewById(R.id.activity_editor_root_layout);
        Toolbar mToolbar = findViewById(R.id.activity_editor_toolbar);

        if (title != null) {
            if (title.length() > 12) {
                String newArticleTitle = title.substring(0, 12);
                mToolbar.setTitle(newArticleTitle + "...");
            } else {
                mToolbar.setTitle(title);
            }
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setSupportActionBar(mToolbar);
        //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        mAlertDialog = new Dialog(EditorActivity.this);
        mAlertDialog.setContentView(R.layout.progress_dialog_layout);
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();

        draftLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                draftValue = 1;
                publish(PRIVATE);
            }
        });

        if (whichData.equals(COURSE)) {
            draftLayout.setVisibility(View.GONE);
        }
    }

    private boolean checkIfAlreadyhavePermission() {
        int permission_read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permission_write = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        return ((permission_read) == PackageManager.PERMISSION_GRANTED && permission_write == PackageManager.PERMISSION_GRANTED);
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }

    private void checkIfPermissionToBeAsked() {

        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
        }

    }

    private void setupEditor() {
        mEditor = findViewById(R.id.editor);
        mEditor.setEditorHeight(200);
        mEditor.setEditorFontSize(22);
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setPlaceholder("Insert text here...");
        mEditor.focusEditor();

        findViewById(R.id.action_undo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.undo();
            }
        });


        findViewById(R.id.action_redo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.redo();
            }
        });

        findViewById(R.id.action_bold).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBold();
            }
        });

        findViewById(R.id.action_italic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setItalic();
            }
        });

        findViewById(R.id.action_strikethrough).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setStrikeThrough();
            }
        });

        findViewById(R.id.action_underline).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setUnderline();
            }
        });

        findViewById(R.id.action_heading1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(1);
            }
        });

        findViewById(R.id.action_heading2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(2);
            }
        });

        findViewById(R.id.action_heading3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(3);
            }
        });

        findViewById(R.id.action_heading4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(4);
            }
        });

        findViewById(R.id.action_heading5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(5);
            }
        });

        findViewById(R.id.action_heading6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(6);
            }
        });

        findViewById(R.id.action_txt_color).setOnClickListener(new View.OnClickListener() {
            private boolean isChanged;

            @Override
            public void onClick(View v) {
                mEditor.setTextColor(isChanged ? Color.BLACK : Color.RED);
                isChanged = !isChanged;
            }
        });

        findViewById(R.id.action_bg_color).setOnClickListener(new View.OnClickListener() {
            private boolean isChanged;

            @Override
            public void onClick(View v) {
                mEditor.setTextBackgroundColor(isChanged ? Color.WHITE : Color.YELLOW);
                isChanged = !isChanged;
            }
        });

        findViewById(R.id.action_indent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setIndent();
            }
        });

        findViewById(R.id.action_outdent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setOutdent();
            }
        });

        findViewById(R.id.action_align_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignLeft();
            }
        });

        findViewById(R.id.action_align_center).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignCenter();
            }
        });

        findViewById(R.id.action_align_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignRight();
            }
        });

        findViewById(R.id.action_blockquote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBlockquote();
            }
        });

        findViewById(R.id.action_insert_bullets).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBullets();
            }
        });

        findViewById(R.id.action_insert_numbers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setNumbers();
            }
        });

        findViewById(R.id.action_insert_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkIfPermissionToBeAsked();
                setupMediaDialog(TYPE_IMAGE);
            }
        });

        findViewById(R.id.action_insert_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkIfPermissionToBeAsked();
                setupMediaDialog(TYPE_VIDEO);
            }
        });

        findViewById(R.id.action_insert_audio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkIfPermissionToBeAsked();
                setupMediaDialog(TYPE_AUDIO);
            }
        });

        findViewById(R.id.action_insert_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupLinkDialog();
            }
        });

        findViewById(R.id.action_insert_checkbox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertTodo();
            }
        });

        findViewById(R.id.action_insert_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupCodeDialog();
            }
        });

        mEditor.setOnTextChangeListener(new RichEditor.OnTextChangeListener() {
            @Override
            public void onTextChange(String text) {
                Logger.info(TAG, mEditor.getHtml());
                if (insertAction) {
                    String html = mEditor.getHtml();
                    int endingPos = html.indexOf("<br><br>") + 7;
                    String finalHtml = html.substring(endingPos + 1);
                    finalHtml = finalHtml + html.substring(0, endingPos + 1);
                    Logger.info(TAG, finalHtml);
                    mEditor.setHtml(finalHtml);
                    insertAction = false;
                }
            }
        });
    }

    private void setupDialog() {

        mDialog = new BottomSheetDialog(this);

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.editor_publish_confirmation_dialog);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        CheckBox isChecked = mDialog.findViewById(R.id.statusCheck);
        Button mPublishPublic = mDialog.findViewById(R.id.btn_publish);

        mPublishPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                draftValue = 0;

                if (isCheck) {
                    publish(PUBLIC);

                    if (mDialog != null && mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                } else {
                    publish(PRIVATE);

                    if (mDialog != null && mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                }
            }
        });

        isChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    isCheck = true;
                } else if (!compoundButton.isChecked()) {
                    isCheck = false;
                }
            }
        });

        User user = User.getCurrentUser(this);

        String featureId = user.getCompanyFeaturesFeatureId();
        String featureValue = user.getCompanyFeaturesValue();
        if (featureId.equalsIgnoreCase("1") && featureValue.equalsIgnoreCase("1")) {
            isChecked.setVisibility(View.VISIBLE);
        } else {
            isChecked.setVisibility(View.GONE);
        }
        mDialog.show();
    }

    private void setupMediaDialog(final int type) {

        mMediaDialog = new Dialog(this);
        mMediaDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mMediaDialog.setContentView(R.layout.upload_media_dialog);
        mMediaDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
/*
        //
        //final ImageView thumbNailLoder = mMediaDialog.findViewById(R.id.thumbNailLoader);*/

        Button mChooseFromGallery = mMediaDialog.findViewById(R.id.btn_choose_from_gallery);
        mEnterUrl = mMediaDialog.findViewById(R.id.btn_enter_url);
        ImageButton mConfirmUrl = mMediaDialog.findViewById(R.id.btn_confirm_url);

        mChooseFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (type) {
                    case TYPE_IMAGE:
                        bringImagePicker();
                        mMediaDialog.dismiss();
                        break;
                    case TYPE_VIDEO:
                        bringVideoChooser();
                        mMediaDialog.dismiss();
                        break;
                    case TYPE_AUDIO:
                        bringAudioPicker();
                        mMediaDialog.dismiss();
                        break;
                    case TYPE_PDF:
                        bringPDFPicker();
                        mMediaDialog.dismiss();
                        break;
                }
            }
        });

        mConfirmUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String enteredUrl = mEnterUrl.getText().toString();

                if (!mEnterUrl.getText().toString().equals("")) {

                    if (!enteredUrl.contains("https://")) {
                        enteredUrl = "https://" + enteredUrl;
                    }
                    insertAction = true;
                    switch (type) {
                        case TYPE_IMAGE:
                            mEditor.insertImage(enteredUrl, "");
                            mMediaDialog.dismiss();
                            break;
                        case TYPE_VIDEO:
                            checkWhichVideoUrl(enteredUrl);
                            mMediaDialog.dismiss();
                            break;
                        case TYPE_AUDIO:
                            mEditor.insertAudio(enteredUrl);
                            mMediaDialog.dismiss();
                            break;
                    }
                } else {
                    displayToast(EditorActivity.this, "Please enter URL");
                }
            }
        });

        mMediaDialog.show();

    }

    private void setupLinkDialog() {
        mLinkDialog = new Dialog(this);
        mLinkDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mLinkDialog.setContentView(R.layout.add_link_article_layout);
        mLinkDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView renameheading = mLinkDialog.findViewById(R.id.renameheading);
        renameheading.setVisibility(View.GONE);
        mLinkDesc = mLinkDialog.findViewById(R.id.add_link_desc);
        mLinkURL = mLinkDialog.findViewById(R.id.add_link_url);
        ImageButton mConfirmLink = mLinkDialog.findViewById(R.id.btn_confirm_link);

        mConfirmLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String link = mLinkURL.getText().toString().trim();

                if (URLUtil.isValidUrl(link)) {
                    if (!mLinkDesc.getText().equals("") && !mLinkURL.getText().equals("")) {
                        String desc = mLinkDesc.getText().toString();
                        mEditor.insertLink(link, desc);
                        mLinkDialog.dismiss();
                    } else {
                        displayToast(EditorActivity.this, "Both are required fields");
                    }
                } else {
                    Toast.makeText(EditorActivity.this, "Incorrect URL", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mLinkDialog.show();
    }

    public void checkValidURL() {

    }

    private void setupCodeDialog() {
        mCodeDialog = new Dialog(this);
        mCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mCodeDialog.setContentView(R.layout.add_code_article_layout);
        mCodeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mCodeDialog.findViewById(R.id.layout_code);

        mAddCode = mCodeDialog.findViewById(R.id.add_code);
        Button mSubmitCode = mCodeDialog.findViewById(R.id.submit_code);

        mSubmitCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mAddCode.getText().equals("")) {
                    String code = mAddCode.getText().toString();
                    mEditor.insertCode(code);
                    mCodeDialog.dismiss();
                } else {
                    displayToast(EditorActivity.this, "Both are required fields");
                }
            }
        });
        mCodeDialog.show();
    }

    private void checkWhichVideoUrl(String url) {
        if (url.contains("youtube") || url.contains("youtu.be")) {

            if (url.contains("embed")) {
                mEditor.insertYoutubeVideo(url);
            } else {
                String item = "https://www.youtube.com/embed/";

                String ss = url;
                if (ss.contains("v=")) {
                    ss = ss.substring(ss.indexOf("v=") + 2);
                    item += ss;
                } else {
                    ss = ss.substring(ss.indexOf("e/") + 2);
                    item += ss;
                }
                mEditor.insertYoutubeVideo(item);
            }

        } else {
            Toast.makeText(this, "Only Youtube videos allowed", Toast.LENGTH_SHORT).show();
            //mEditor.insertVideo(url);
        }
    }

    private void publish(String articleStatusType) {
        if (mDialog != null) {
            mDialog.dismiss();
        }

        if (!mEditor.getHtml().isEmpty()) {

            //showProgressBar();

            try {
                String html = mEditor.getHtml();
                html = html.replace("&nbsp;", "");

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(ACTION, SAVE_ARTICLE_DATA);
                jsonObject.put(DRAFT_STATUS, draftValue);
                jsonObject.put(CONTENT_ID, data);
                jsonObject.put(ARTICLE, html);
                jsonObject.put(PRIVACY_STATUS, articleStatusType);

                JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                        Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Logger.info(TAG, response.toString());

                            if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {

                                if (draftValue == 0) {
                                    Intent intent = new Intent(getApplicationContext(), KeyWordsEntittyActivity.class);
                                    intent.putExtra("content_type", content_type);
                                    intent.putExtra("content_id", data);
                                    startActivity(intent);

                                    displayToast(EditorActivity.this, getString(R.string.article_publish_success));
                                    finish();
                                } else {
                                    displayToast(EditorActivity.this, getString(R.string.article_draft_success));
                                    finish();
                                }

                            } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                SideBarPanel.logout(EditorActivity.this);
                            } else {

                                Error error = Error.generateError(HttpResponse.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                        "publish", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                                displayErrorUI(error);

                            }
                        } catch (JSONException e) {

                            Error error = Error.generateError(JSONException.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                    "publish", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                            displayErrorUI(error);

                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Error error = Error.generateError(VolleyError.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                "publish", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                        displayErrorUI(error);

                    }
                });

                VolleySingleton.getInstance(EditorActivity.this).addToRequestQueue(jsonObjectRequest);

            } catch (Exception e) {

                Error error = Error.generateError(JSONException.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                        "publish", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                displayErrorUI(error);

            }

        } else {

            displayToast(this, getString(R.string.empty_article));
        }
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(EditorActivity.this, R.id.activity_editor_root_layout, error);
    }

    private void bringImagePicker() {
        chooseFile(EditorActivity.this, REQUEST_CODE_CHOOSE_IMAGE);
    }

    private void bringVideoChooser() {
        chooseFile(EditorActivity.this, REQUEST_CODE_CHOOSE_VIDEO);
    }

    private void bringAudioPicker() {
        chooseFile(EditorActivity.this, REQUEST_CODE_CHOOSE_AUDIO);
    }

    private void bringPDFPicker() {
        chooseFile(EditorActivity.this, REQUEST_CODE_CHOOSE_PDF);
    }

    private void uploadFile(final File file, final int TYPE) {
        fileUploadStatus = "true";
        showProgressBar();

        long sizeOriginal = file.length();
        String sizeOri = getStringSizeLengthFile(sizeOriginal);
        Logger.debug("sizeCom", sizeOri);

        String fileExt = MimeTypeMap.getFileExtensionFromUrl(file.toString());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");
        String formattedDate = df.format(c.getTime());

        final String UPLOAD_FILE_NAME = getCurrentUser(this)
                .getUserId() + "_" + formattedDate + "_" + file.getName();

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "ap-south-1:345799b2-3506-4245-a702-3a7e9d4c0071", // Identity pool ID
                Regions.AP_SOUTH_1 // Region
        );
        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider, Region.getRegion(Regions.AP_SOUTH_1));
        TransferNetworkLossHandler.getInstance(this);

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .defaultBucket(BUCKET_NAME)
                        .context(getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

        TransferObserver uploadObserver =
                transferUtility.upload(Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(EditorActivity.this).getUserId() + "/" + UPLOAD_FILE_NAME, file, CannedAccessControlList.PublicRead);

        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    hideProgressBar();
                    fileUploadStatus = "false";

                    displayToast(EditorActivity.this, "Uploaded !!!");
                    Logger.info(TAG, "Uploading completed !!" + "--" + baseAWSUrl + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(EditorActivity.this).getUserId() + "/" + UPLOAD_FILE_NAME);
                    insertAction = true;

                    switch (TYPE) {

                        case TYPE_IMAGE:
                            mEditor.insertImage(baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(EditorActivity.this).getUserId() + "/" + UPLOAD_FILE_NAME, "");
                            break;

                        case TYPE_VIDEO:
                            mEditor.insertVideo(baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(EditorActivity.this).getUserId() + "/" + UPLOAD_FILE_NAME);
                            break;

                        case TYPE_AUDIO:
                            mEditor.insertAudio(baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(EditorActivity.this).getUserId() + "/" + UPLOAD_FILE_NAME);
                            break;

                        case TYPE_PDF:
                            mEditor.insertImagePDF(R.drawable.article_explore);
                            break;
                    }
                    try {
                        if (file.exists())
                            file.delete();
                    } catch (Exception e) {
                        Logger.error(TAG, e.getMessage());
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;
                Logger.info(TAG, String.valueOf(percentDone) + "% uploading complete");
            }

            @Override
            public void onError(int id, Exception ex) {
                fileUploadStatus = "false";
                displayToast(EditorActivity.this, "ERROR: " + ex.getMessage() + ". Please try again");
                Logger.info(TAG, ex.getMessage() + " is the error while uploading");
                //hideProgressBar();
                try {
                    if (file.exists())
                        file.delete();
                } catch (Exception e) {
                    Logger.error(TAG, e.getMessage());
                }
            }
        });

    }

    private void submitChapter() {

        if (!mEditor.getHtml().isEmpty()) {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.COURSE.ACTION, "save_chapter_data");

                jsonObject.put("chapter_data", mEditor.getHtml());
                jsonObject.put("chapter_id_reference", data);

                JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                        Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Logger.info(TAG, response.toString());
                            if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {
                                displayToast(EditorActivity.this, getString(R.string.chapter_created));
                                finish();


                            } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                SideBarPanel.logout(EditorActivity.this);
                            } else {
                                Error error = Error.generateError(HttpResponse.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                        "submitChapter", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                                displayErrorUI(error);
                            }

                        } catch (JSONException e) {
                            Error error = Error.generateError(JSONException.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                    "submitChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                            displayErrorUI(error);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Error error = Error.generateError(VolleyError.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                "submitChapter", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                        displayErrorUI(error);
                    }
                });
                VolleySingleton.getInstance(EditorActivity.this).addToRequestQueue(jsonObjectRequest);
            } catch (JSONException e) {
                Error error = Error.generateError(JSONException.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                        "submitChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                displayErrorUI(error);
            }
        }
    }

    private void showExistingChapterData() {
        //showProgressBar();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.COURSE.ACTION, "show_chapter_data");
            jsonObject.put("chapter_id_reference", data);

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        //              hideProgressBar();
                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {
                            Logger.info(TAG, "Successfully fetched existing data");
                            //                hideProgressBar();
                            String existingHtml = response.getString(DATA);

                            mEditor.setHtml(existingHtml);
                            mAlertDialog.dismiss();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(EditorActivity.this);
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                    "submitChapter", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                            displayErrorUI(error);
                            mAlertDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        //          hideProgressBar();
                        Error error = Error.generateError(JSONException.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                "submitChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        displayErrorUI(error);
                        mAlertDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //    hideProgressBar();
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                            "submitChapter", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                    displayErrorUI(error);
                    mAlertDialog.dismiss();
                }
            });

            VolleySingleton.getInstance(EditorActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            //  hideProgressBar();
            Error error = Error.generateError(JSONException.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                    "submitChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);
            mAlertDialog.dismiss();
        }
    }

    //{ "action":"show_article_data", "content_id":"654" }
    private void showExistingArticleData() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.COURSE.ACTION, "show_article_data");
            jsonObject.put("content_id", data);

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {
                            Logger.info(TAG, "Successfully fetched existing data");

                            JSONObject jsonObject1 = response.getJSONObject(DATA);
                            JSONArray articleDataArray = jsonObject1.getJSONArray("article_data");
                            JSONObject articleData = articleDataArray.getJSONObject(0);
                            String existingHtml = articleData.getString("data");

                            String newData = "";

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                newData = String.valueOf(Html.fromHtml(existingHtml, Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                newData = String.valueOf(Html.fromHtml(existingHtml));
                            }

                            mEditor.setHtml(newData);
                            mAlertDialog.dismiss();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(EditorActivity.this);
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                    "submitChapter", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                            displayErrorUI(error);
                            mAlertDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                                "submitChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        displayErrorUI(error);
                        mAlertDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                            "submitChapter", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                    displayErrorUI(error);

                    mAlertDialog.dismiss();
                }
            });

            VolleySingleton.getInstance(EditorActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), EditorActivity.class.getSimpleName(),
                    "submitChapter", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);
            mAlertDialog.dismiss();
        }
    }

    private void showAppropriateExistingData() {

        if (typeIntent.equalsIgnoreCase("")) {
            switch (whichData) {
                case Constants.Content.ARTICLE:
                    content_type = "1";
                    showExistingArticleData();
                    break;
                case Constants.Content.COURSE:
                    content_type = "3";
                    showExistingChapterData();

            }
        } else {
            mAlertDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_menu_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_next:

                if (whichData.equals(Constants.Content.ARTICLE))
                    if (fileUploadStatus.equals("false")) {
                        setupDialog();
                    } else {
                        Toast.makeText(EditorActivity.this, "File Upload is In Progress", Toast.LENGTH_SHORT).show();
                    }
                else if (whichData.equals(COURSE)) {
                    submitChapter();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            ArrayList<String> list = FileExtensionUtil.checkForCourse();
            String fileExtension = CommonMethods.getExtensionFile(this, data.getData());
            if (list.contains(fileExtension)) {
                switch (requestCode) {
                    case REQUEST_CODE_CHOOSE_IMAGE:
                        copyFileToLocalStorage(data.getData(), REQUEST_CODE_CHOOSE_IMAGE);
                        break;
                    case REQUEST_CODE_CHOOSE_VIDEO:
                        copyFileToLocalStorage(data.getData(), REQUEST_CODE_CHOOSE_VIDEO);
                        break;
                    case REQUEST_CODE_CHOOSE_AUDIO:
                        copyFileToLocalStorage(data.getData(), REQUEST_CODE_CHOOSE_AUDIO);
                        break;
                    case REQUEST_CODE_CHOOSE_PDF:
                        copyFileToLocalStorage(data.getData(), REQUEST_CODE_CHOOSE_PDF);
                }

            } else {
                Toast.makeText(this, "Invalid file type", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void copyFileToLocalStorage(final Uri uri, final int REQUEST_CODE) {

        final Dialog dialog = new Dialog(EditorActivity.this);
        dialog.setContentView(R.layout.progress_dialog_layout);
        dialog.setCancelable(false);
        dialog.show();

        FilePicker.copyFileFromUri(this, uri, new OnFileCopiedListener() {
            @Override
            public void onFileCopied(File file) {

                switch (REQUEST_CODE) {
                    case REQUEST_CODE_CHOOSE_VIDEO:
                        uploadFile(file, TYPE_VIDEO);
                        break;
                    case REQUEST_CODE_CHOOSE_AUDIO:
                        uploadFile(file, TYPE_AUDIO);
                        break;
                    case REQUEST_CODE_CHOOSE_IMAGE:
                        uploadFile(file, TYPE_IMAGE);
                        break;
                    case REQUEST_CODE_CHOOSE_PDF:
                        uploadFile(file, TYPE_PDF);
                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFileCopyFailed(Error error) {
                CommonMethods.displayToast(EditorActivity.this, EditorActivity.this.getString(R.string.something_went_wrong));
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                } else {
                    //not granted
                    displayToast(EditorActivity.this, getString(R.string.permissions_denied));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mEditor.setHtml("");
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCurrentHtmlData = mEditor.getHtml() + "";
        mEditor.setHtml("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        mEditor.setHtml(mCurrentHtmlData);

    }
}
