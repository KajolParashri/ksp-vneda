package com.indiareads.ksp.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.view.activity.CreateContentActivity;

import static com.indiareads.ksp.utils.Constants.Content.ARTICLE;
import static com.indiareads.ksp.utils.Constants.Content.COURSE;
import static com.indiareads.ksp.utils.Constants.Content.EBOOK;
import static com.indiareads.ksp.utils.Constants.Content.INFOGRAPHIC;
import static com.indiareads.ksp.utils.Constants.Content.PODCAST;
import static com.indiareads.ksp.utils.Constants.Content.VIDEO;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateFragment extends Fragment {

    private static final String TAG = CreateFragment.class.getSimpleName();
    private View mMainView;
    private CardView mInfographic, mVideo, mArticle, mCourse, mPodcats, mEbook;
    public String content_type = "";

    public CreateFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mMainView = inflater.inflate(R.layout.fragment_create, container, false);

        Logger.info(TAG, "onCreateView");
        initViews();
        setOnClickListeners();

        return mMainView;
    }

    private void initViews() {
        mInfographic = mMainView.findViewById(R.id.card_infographic);
        mVideo = mMainView.findViewById(R.id.card_video);
        mArticle = mMainView.findViewById(R.id.card_article);
        mCourse = mMainView.findViewById(R.id.card_course);
        mPodcats = mMainView.findViewById(R.id.card_podcast);
        mEbook = mMainView.findViewById(R.id.card_ebook);
    }

    private void setOnClickListeners() {
        mInfographic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(INFOGRAPHIC);
            }
        });
        mVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(VIDEO);
            }
        });
        mArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(ARTICLE);
            }
        });
        mCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(COURSE);
            }
        });
        mPodcats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(PODCAST);
            }
        });
        mEbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(EBOOK);
            }
        });

    }

    private void startLandingActivity(String content) {
        int content_type = CommonMethods.getContentTypeFromName(content);
        Intent landingIntent = new Intent(getContext(), CreateContentActivity.class);
        landingIntent.putExtra("content_type", "" + content_type);
        landingIntent.putExtra("content", content);

        startActivity(landingIntent);
    }


}
