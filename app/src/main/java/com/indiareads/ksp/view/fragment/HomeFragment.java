package com.indiareads.ksp.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.BannerSliderAdapter;
import com.indiareads.ksp.adapters.HomeFragmentRecyclerAdapter;
import com.indiareads.ksp.adapters.LikeAdapter;
import com.indiareads.ksp.explore.fragment.DynamicExploreActivity;
import com.indiareads.ksp.listeners.OnContentLikeCommentListener;
import com.indiareads.ksp.listeners.OnHomeFragmentClickListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.BannerModel;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.SuggestionModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CircleTransform;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.LikeCommentHelper;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.CategoryListingActivity;
import com.indiareads.ksp.view.activity.CreatePostActivity;
import com.indiareads.ksp.view.activity.HomeActivity;
import com.indiareads.ksp.view.activity.ListingActivity;
import com.indiareads.ksp.view.activity.MessageActivity;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.view.activity.SearchActivity;
import com.indiareads.ksp.view.activity.UserDetailsActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_NAME;
import static com.indiareads.ksp.utils.Constants.Network.CATEGORY_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements OnContentLikeCommentListener {
    private CardView mInfographic, mVideo, mArticle, mCourse, mPodcats, mEbook;

    public static final String HOME_ADAPTER_POSITION = "Adapter Position";
    private static final String TAG = HomeFragment.class.getSimpleName();
    private View mHomeFragmentRootView; //Root view
    private WrapContentLinearLayoutManager mLinearLayoutManager;
    private HomeFragmentRecyclerAdapter mHomeFragmentRecyclerAdapter;
    private List<User> mUsers; //Users list
    private List<Content> mContents; //Contents list
    private RecyclerView mHomeRecyclerView;
    SwipeRefreshLayout pullToRefresh;
    private int pageNumber = 1;
    private boolean doPagination = true;
    private String userId;
    boolean isFirstTime = false;
    NestedScrollView nestedScoller;
    private String skipContentType;
    private String requiredContentType;
    private boolean mContentsLoading = true;
    ArrayList<BannerModel> bannerModels;
    TextView marathi_books, hindi_books, recommended, popular_books, top_100, summary_item;
    RelativeLayout post_layout;
    RecyclerView bannerRecyclerView;
    TextView create_article, create_post;
    ImageView userImage;
    TextView content;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            userId = getArguments().getString(Constants.Network.USER_ID, "");
            skipContentType = getArguments().getString(Constants.Network.SKIP_CONTENT_TYPE, "");
            requiredContentType = getArguments().getString(Constants.Network.REQUIRED_CONTENT_TYPE, "");
        }
    }

    public boolean scrollView() {
        boolean checkMate = false;

        if (nestedScoller != null) {
            if (!nestedScoller.canScrollVertically(-1)) {
                // top of scroll view
                checkMate = true;
            } else {
                checkMate = false;
                nestedScoller.fullScroll(View.FOCUS_UP);
                nestedScoller.smoothScrollTo(0, 0);
            }
        }
        return checkMate;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mHomeFragmentRootView = inflater.inflate(R.layout.fragment_home, container, false);

        initViews();
        initContentList();
        initRecyclerView();

        setHomeFragmentClickListener();
        setScrollListener(nestedScoller);
        fetchData();

        getBannerData();
        setOnClickListeners();
        refreshListener();

        create_article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity) getActivity()).setUpFragmentLayout(R.id.bottom_create);
            }
        });

        return mHomeFragmentRootView;
    }

    public void setScrollListener(NestedScrollView nestedScrollView) {
        try {
            nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {

                            if (doPagination) //check for scroll down
                            {
                                if (mContentsLoading) {
                                    mContentsLoading = false;
                                    if (mContents.size() != 1) {
                                        addProgressBarInListPagination();
                                    }
                                    mHomeFragmentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);
                                    mHomeRecyclerView.scrollToPosition(mContents.size() - 1);
                                    pageNumber++;
                                    fetchData();
                                }
                            }
                        }
                    }
                }
            });

        } catch (Exception e) {

        }
    }

    public void showLikes(ArrayList<User> listData, String titleMesg) {

        final BottomSheetDialog dialogLikes = new BottomSheetDialog(getActivity());
        dialogLikes.setContentView(R.layout.likes_single_item);
        dialogLikes.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogLikes.setCanceledOnTouchOutside(false);

        RecyclerView recyclerView = dialogLikes.findViewById(R.id.like_recycler);

        TextView titleMessage = dialogLikes.findViewById(R.id.titleMessage);
        if (titleMesg.length() <= 15) {
            titleMessage.setText("Likes : " + titleMesg);
        } else {
            titleMesg = titleMesg.substring(0, 15);
            titleMessage.setText("Likes : " + titleMesg + "...");
        }

        LinearLayoutManager LayoutManagaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(LayoutManagaer);
        ImageView cross = dialogLikes.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLikes.dismiss();
            }
        });
        LikeAdapter adapter = new LikeAdapter(listData);
        recyclerView.setAdapter(adapter);

        if (!dialogLikes.isShowing()) {
            dialogLikes.show();
        }
    }

    private void getLikesShow(final String mainTitle, String content_id, int pageNumber, String content_status_type) {
        try {
            final ArrayList<User> likes = new ArrayList<>();
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.LOAD_CONTENT_LIKES);
            jsonObject.put(Constants.Network.CONTENT_ID, content_id);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, content_status_type);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                Logger.info(TAG, response.toString());

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONArray data = response.getJSONArray(Constants.Network.DATA);

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject dataMain = data.getJSONObject(i);
                                        User user = new User();
                                        user.setFullName(dataMain.getString(Constants.Network.FULL_USER_NAME));
                                        user.setProfilePic(dataMain.getString(Constants.Network.PROFILE_PIC));
                                        user.setDesignation(dataMain.getString(Constants.Network.DESIGNATION));
                                        user.setCreatedContents(dataMain.getString(Constants.Network.CREATED_DATE));
                                        user.setUserId(dataMain.getString(Constants.Network.USER_ID));
                                        likes.add(user);
                                    }
                                    showLikes(likes, mainTitle);
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
//                                    showLikes(likes, mainTitle);
                                    Logger.error(TAG, "400");
                                }
                            } catch (JSONException e) {
                                Logger.error(TAG, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error != null) {
                                Logger.error(TAG, "errorException");
                            }
                        }
                    });

            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error(TAG, e.getMessage());
        }
    }

    private void getBannerData() {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(getActivity()).getCompanyId());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_HOME_BANNER_DATA);
            Logger.info("banner", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.DYNAMIC_CONTENT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Logger.info("banner", response.toString());
                                //    Toast.makeText(getActivity(), "" + response.toString(), Toast.LENGTH_SHORT).show();

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONArray data = response.getJSONArray(Constants.Network.DATA);
                                    bannerModels = new ArrayList<>();
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject1 = data.getJSONObject(i);
                                        BannerModel bannerModel = new BannerModel();
                                        bannerModel.setImgUrl(Urls.baseAWSUrl + "/" + jsonObject1.getString(Constants.Network.URL));
                                        bannerModel.setIntentStatus(jsonObject1.getString(Constants.Network.INTENT_STATUS));
                                        bannerModel.setIntentType(jsonObject1.getString(Constants.Network.INTENT_TYPE));
                                        bannerModel.setIntentParameter1(jsonObject1.getString(Constants.Network.INTENT_PARAMETER_1));
                                        bannerModel.setIntentParameter2(jsonObject1.getString(Constants.Network.INTENT_PARAMETER_2));
                                        bannerModels.add(bannerModel);
                                    }
                                    setBannerData();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Logger.error(TAG, "400");
                                }
                            } catch (JSONException e) {
                                Logger.error(TAG, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            Logger.error(TAG, error.getMessage());
                        }
                    });

            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error(TAG, e.getMessage());
        }
    }


    public void setBannerData() {
        BannerSliderAdapter bannerSliderAdapter = new BannerSliderAdapter(getActivity(), bannerModels, new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                BannerModel bannerModel = bannerModels.get(position);
                if (bannerModel.getIntentType().equals("1")) {

                    String typeSET = bannerModel.getIntentParameter1();
                    if (typeSET.equals("SET1")) {
                        ((HomeActivity) getActivity()).setUpFragmentLayout(R.id.bottom_books);
                    } else if (typeSET.equals("SET2")) {
                        ((HomeActivity) getActivity()).setUpFragmentLayout(R.id.bottom_explore);
                    } else {
                        Intent intent = new Intent(getActivity(), DynamicExploreActivity.class);
                        intent.putExtra("SETVALUE", bannerModel.getIntentParameter1());
                        startActivity(intent);
                    }
                    /*((HomeActivity) getActivity()).setUpFragmentLayout(R.id.bottom_explore);*/
                } else if (bannerModel.getIntentType().equals("2")) {
                    Intent intent = new Intent(getActivity(), CategoryListingActivity.class);
                    intent.putExtra(CATEGORY_ID, bannerModel.getIntentParameter1());
                    intent.putExtra(CATEGORY_NAME, bannerModel.getIntentParameter2());
                    startActivity(intent);
                } else if (bannerModel.getIntentType().equals("3")) {
                    Intent intent = new Intent(getActivity(), ListingActivity.class);
                    intent.putExtra(Constants.Content.CONTENT_TYPE, bannerModel.getIntentParameter1());
                    startActivity(intent);
                } else if (bannerModel.getIntentType().equals("4")) {
                    ((HomeActivity) getActivity()).setUpFragmentLayout(R.id.bottom_mycompany);
                } else if (bannerModel.getIntentType().equals("5")) {
                    Intent userProfileIntent = new Intent(getActivity(), UserDetailsActivity.class);
                    userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 0);
                    userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(getActivity()).getUserId());
                    userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(getActivity()).getCompanyId());
                    startActivity(userProfileIntent);
                } else if (bannerModel.getIntentType().equals("6")) {
                    startActivity(new Intent(getActivity(), MessageActivity.class));
                } else if (bannerModel.getIntentType().equals("7")) {
                    Intent userProfileIntent = new Intent(getActivity(), UserDetailsActivity.class);
                    userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 4);
                    userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(getActivity()).getUserId());
                    userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(getActivity()).getCompanyId());
                    startActivity(userProfileIntent);
                } else if (bannerModel.getIntentType().equals("8")) {
                    ((HomeActivity) getActivity()).setUpFragmentLayout(R.id.bottom_create);
                } else if (bannerModel.getIntentType().equals("9")) {
                    Intent searchActivityIntent = new Intent(getActivity(), SearchActivity.class);
                    searchActivityIntent.putExtra(SearchActivity.SEARCH_QUERY, bannerModel.getIntentParameter1());
                    startActivity(searchActivityIntent);
                } else if (bannerModel.getIntentType().equals("10")) {
                    Intent userProfileIntent = new Intent(getActivity(), UserDetailsActivity.class);
                    userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 6);
                    userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(getActivity()).getUserId());
                    userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(getActivity()).getCompanyId());
                    startActivity(userProfileIntent);
                } else if (bannerModel.getIntentType().equals("11")) {
                    ((HomeActivity) getActivity()).setUpFragmentLayout(R.id.bottom_mycompany);
                } else if (bannerModel.getIntentType().equals("12")) {
                    //leadership board
                }
            }
        });

        bannerRecyclerView.setAdapter(bannerSliderAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            ((HomeActivity) getActivity()).getAccountDetailsRequest();
        }
    }

    private void setHomeFragmentClickListener() {
        mHomeFragmentRecyclerAdapter.setOnClickListener(new OnHomeFragmentClickListener() {
            @Override
            public void onLikeClicked(View view, int position) {
                Content likedContent = mContents.get(position);
                LikeCommentHelper.switchLikeInfo(likedContent);
                LikeCommentHelper.handleLikeButtonConfig(likedContent, getActivity(), view);
                LikeCommentHelper.likeContentItem(getContext(), likedContent, HomeFragment.this);
            }

            @Override
            public void onViewClicked(View view, int position) {
                Intent productIntent = new Intent(getContext(), ProductActivity.class);
                productIntent.putExtra(HOME_ADAPTER_POSITION, position);
                productIntent.putExtra(Constants.Network.CONTENT_ID, mContents.get(position).getContentId());
                productIntent.putExtra(Constants.Network.CONTENT_TYPE, mContents.get(position).getContentType());
                productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);
                startActivityForResult(productIntent, Constants.RequestCode.REQUEST_LIKE_COMMENT_COUNT);
            }

            @Override
            public void onDeleteClicked(View view, int position) {

            }

            @Override
            public void onLikeCountClicked(View view, int position) {

                String mainTitle = mContents.get(position).getTitle();
                getLikesShow(mainTitle, mContents.get(position).getContentId(), 1, mContents.get(position).getStatusType());
            }

            @Override
            public void onCommentClicked(View view, int position) {
                Content content = mContents.get(position);
                LikeCommentHelper.openComments(position, getContext(), view, content, HomeFragment.this);
            }

            @Override
            public void onProfileClicked(View view, int position) {

                Intent userProfileIntent = new Intent(getActivity(), UserDetailsActivity.class);
                userProfileIntent.putExtra(Constants.Network.USER_ID, mUsers.get(position).getUserId());
                userProfileIntent.putExtra(Constants.Network.COMPANY_ID, mContents.get(position).getContentCompanyId());
                startActivity(userProfileIntent);

            }
        });

    }

    private void initContentList() {

        mUsers = new ArrayList<>();
        mContents = new ArrayList<>();
        addProgressBarInList();

    }

    private void loadImageProfile(ImageView profileImageView) {
        Picasso.get().load(Urls.AWS_PROFILE_IMAGE_PATH + User.getCurrentUser(getActivity())
                .getProfilePic()).error(R.mipmap.ic_profile).placeholder(R.mipmap.ic_profile)
                .transform(new CircleTransform()).into(profileImageView);
    }


    private void startLandingActivity(int contentType) {
        Intent landingIntent = new Intent(getActivity(), ListingActivity.class);
        landingIntent.putExtra(Constants.Content.CONTENT_TYPE, String.valueOf(contentType));
        startActivity(landingIntent);
    }

    private void setOnClickListeners() {

        mInfographic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(Constants.Content.CONTENT_TYPE_INFOGRAPHIC);
            }
        });
        mVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(Constants.Content.CONTENT_TYPE_VIDEO);
            }
        });
        mArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(Constants.Content.CONTENT_TYPE_ARTICLE);
            }
        });
        mCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(Constants.Content.CONTENT_TYPE_COURSE);
            }
        });
        mPodcats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(Constants.Content.CONTENT_TYPE_PODCAST);
            }
        });
        mEbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLandingActivity(Constants.Content.CONTENT_TYPE_EBOOK);
            }
        });
    }

    public void refreshListener() {
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                //Here you can update your data from internet or from local SQLite data
                int oldSize = mContents.size();
                mContents.clear();
                mUsers.clear();
                mHomeFragmentRecyclerAdapter.notifyItemRangeRemoved(0, oldSize);
                addProgressBarInList();
                mHomeFragmentRecyclerAdapter.notifyItemInserted(0);
                pageNumber = 1;
                doPagination = true;
                mContentsLoading = false;
                fetchData();
            }
        });
    }

    public void setClickListener(TextView textView, final String SETValue) {

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DynamicExploreActivity.class);
                intent.putExtra("SETVALUE", SETValue);
                startActivity(intent);
            }
        });
    }

    public void initViews() {

        marathi_books = mHomeFragmentRootView.findViewById(R.id.marathi_books);
        hindi_books = mHomeFragmentRootView.findViewById(R.id.hindi_books);
        recommended = mHomeFragmentRootView.findViewById(R.id.recommended);
        popular_books = mHomeFragmentRootView.findViewById(R.id.popular_books);
        top_100 = mHomeFragmentRootView.findViewById(R.id.top_100);

        summary_item = mHomeFragmentRootView.findViewById(R.id.summary_item);

        pullToRefresh = mHomeFragmentRootView.findViewById(R.id.home_swipe_refresh);

        setClickListener(marathi_books, "SET7");
        setClickListener(hindi_books, "SET6");
        setClickListener(recommended, "SET5");
        setClickListener(popular_books, "SET4");
        setClickListener(top_100, "SET3");
        setClickListener(summary_item, "SET14");

        mInfographic = mHomeFragmentRootView.findViewById(R.id.card_infographic);
        mVideo = mHomeFragmentRootView.findViewById(R.id.card_video);
        mArticle = mHomeFragmentRootView.findViewById(R.id.card_article);
        mCourse = mHomeFragmentRootView.findViewById(R.id.card_course);
        mPodcats = mHomeFragmentRootView.findViewById(R.id.card_podcast);
        mEbook = mHomeFragmentRootView.findViewById(R.id.card_ebook);
        content = mHomeFragmentRootView.findViewById(R.id.content);

        content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreatePostActivity.class);
                intent.putExtra("type", "post");
                startActivity(intent);
            }
        });

        create_post = mHomeFragmentRootView.findViewById(R.id.create_post);

        create_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreatePostActivity.class);
                intent.putExtra("type", "post");
                startActivity(intent);
            }
        });

        userImage = mHomeFragmentRootView.findViewById(R.id.userImage);
        loadImageProfile(userImage);
        post_layout = mHomeFragmentRootView.findViewById(R.id.post_layout);

        create_article = mHomeFragmentRootView.findViewById(R.id.create_article);
        create_post = mHomeFragmentRootView.findViewById(R.id.create_post);

        mHomeRecyclerView = mHomeFragmentRootView.findViewById(R.id.fragment_home_recycler_view);
        bannerRecyclerView = mHomeFragmentRootView.findViewById(R.id.viewpagernew);

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        bannerRecyclerView.setLayoutManager(layoutManager2);

        nestedScoller = mHomeFragmentRootView.findViewById(R.id.nestedScoller);
        //scrollView(nestedScoller);
    }

    private void setRecyclerViewScrollListener() {
        //Fetching next page's data on reaching bottom
        mHomeRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mLinearLayoutManager.getChildCount();
                    totalItemCount = mLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                    if (mContentsLoading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {

                            mContentsLoading = false;
                            addProgressBarInList();
                            mHomeFragmentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);
                            pageNumber++;
                            fetchData();

                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

        });
    }

    private void fetchData() {

        try {

            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.USER_ID, userId);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(getActivity()).getCompanyId());
            jsonObject.put(Constants.Network.SKIP_CONTENT_TYPE, skipContentType);
            jsonObject.put(Constants.Network.REQUIRED_CONTENT_TYPE, requiredContentType);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_DISPLAY_CONTENTS);
            jsonObject.put(Constants.Network.SESSION_USER_ID, User.getCurrentUser(getContext()).getUserId());
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);

            Logger.error(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.HOMEPAGE_API,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Logger.error(TAG, response.toString());
                    try {
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            //to remove progress bar
                            // C/ontent contentSS = mContents.get(mContents.size() - 1);
                            // if (mContents.size() != 1) {
                            mUsers.remove(mUsers.size() - 1);
                            mContents.remove(mContents.size() - 1);

                            mHomeFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                            //Getting jsonArray from response object
                            JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                            //Adding jsonArray data into a list of jsonObjects
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                User user = new User();
                                user.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                user.setFullName(jsonObject1.getString(Constants.Network.USER_FULL_NAME));
                                user.setProfilePic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                user.setDesignation(jsonObject1.getString(Constants.Network.DESIGNATION));

                                Content content = new Content();
                                content.setStatusType(jsonObject1.getString(Constants.Network.CONTENT_STATUS_TYPE));
                                content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                content.setCreatedDate(jsonObject1.getString(Constants.Network.CREATED_DATE));
                                content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                content.setThumbnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));
                                content.setContentType(jsonObject1.getString(Constants.Network.CONTENT_TYPE));
                                content.setLikeStatus(jsonObject1.getString(Constants.Network.LIKED_STATUS));
                                content.setLikeCount(jsonObject1.getString(Constants.Network.LIKE_COUNT));
                                content.setCommentCount(jsonObject1.getString(Constants.Network.COMMENT_COUNT));
                                content.setAvgContentRating(jsonObject1.getString(Constants.Network.AVG_CONTENT_RATING));

                                mUsers.add(user);
                                mContents.add(content);
                            }

                            mHomeFragmentRecyclerAdapter.notifyItemRangeInserted(mUsers.size() - jsonArray.length(), jsonArray.length());

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {

                            //stop call to pagination in any case
                            doPagination = false;

                            //to remove progress bar
                            Content content = mContents.get(mContents.size() - 1);
                            if (content.getContentType().equalsIgnoreCase(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
                                mUsers.remove(mUsers.size() - 1);
                                mContents.remove(mContents.size() - 1);
                                mHomeFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                            }


                            if (pageNumber == 1) {
                                //show msg no contents
                                showEmptyView(R.mipmap.ic_fevicon, getString(R.string.content_list_empty));
                            } else {
                                //show msg no more posts
                            }
                        }

                        if (pullToRefresh.isRefreshing())
                            pullToRefresh.setRefreshing(false);

                        mContentsLoading = true;

                    } catch (Exception e) {
                        Logger.error(TAG, "In OnResponse" + e.getMessage());
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    doPagination = false;

                    Content content = mContents.get(mContents.size() - 1);
                    if (content.getContentType().equalsIgnoreCase(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
                        mUsers.remove(mUsers.size() - 1);
                        mContents.remove(mContents.size() - 1);
                        mHomeFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                    }

                    if (error instanceof NetworkError) {
                        showEmptyView(R.drawable.ic_signal_wifi_off, getString(R.string.connection_error_message));
                    } else {
                        showEmptyView(R.drawable.ic_error_outline, getString(R.string.something_went_wrong));
                    }

                    mContentsLoading = true;
                    Logger.info(TAG, error.getMessage() + " is the volley error ");
                }
            });

            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(

                    getContext()).

                    addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {

            Logger.error(TAG, "In fetchData" + e.getMessage());
            e.printStackTrace();
        }

    }

    private void showEmptyView(int imageId, String msg) {

        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(imageId));
        emptyContent.setTitle(msg);
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mContents.add(emptyContent);
        mUsers.add(new User());
        mHomeFragmentRecyclerAdapter.notifyItemInserted(0);

    }

    private void addProgressBarInList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mContents.add(progressBarContent);
        mUsers.add(new User());
    }


    private void addProgressBarInListPagination() {

        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_DATALOAD));
        mContents.add(progressBarContent);
        mUsers.add(new User());
    }

    private void initRecyclerView() {
        mLinearLayoutManager = new WrapContentLinearLayoutManager(getActivity());
        mHomeRecyclerView.setLayoutManager(mLinearLayoutManager);
        mHomeFragmentRecyclerAdapter = new HomeFragmentRecyclerAdapter(mUsers, mContents, false);
        mHomeRecyclerView.setAdapter(mHomeFragmentRecyclerAdapter);
        // addProgressBarInList();
    }

    @Override
    public void onContentLiked(Content content, String likeStatus, String likeCount) {

    }

    @Override
    public void onContentCommented(Content content, String commentCount, int pos) {
        mContents.set(pos, content);
        mHomeFragmentRecyclerAdapter.notifyItemChanged(pos);
    }

    @Override
    public void onContentCommented(SuggestionModel content, String commentCount, int pos) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.RequestCode.REQUEST_LIKE_COMMENT_COUNT) {

            if (resultCode == RESULT_OK) {
                Logger.error(TAG, requestCode + " " + resultCode);
                Objects.requireNonNull(mContents.get(data.getIntExtra(HOME_ADAPTER_POSITION, -1))).setLikeCount(data.getStringExtra(Constants.Network.LIKE_COUNT));
                Logger.error(TAG, HOME_ADAPTER_POSITION + " " + data.getStringExtra(Constants.Network.LIKE_COUNT));
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }

    public class WrapContentLinearLayoutManager extends LinearLayoutManager {

        public WrapContentLinearLayoutManager(Context context) {
            super(context);
        }

        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Logger.error(TAG, e.getMessage());
            }
        }
    }
}
