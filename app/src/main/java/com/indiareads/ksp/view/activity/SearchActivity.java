package com.indiareads.ksp.view.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.SearchActivityViewPagerAdapter;
import com.indiareads.ksp.adapters.SpinnerAdapterContent;
import com.indiareads.ksp.listeners.OnFilterAppliedListener;
import com.indiareads.ksp.model.ContentSpinnerModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.SearchActivityFilterFragment;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;

public class SearchActivity extends AppCompatActivity implements OnFilterAppliedListener {

    public static final String SEARCH_QUERY = "Search_Query";
    int viewPagerPosition = 7;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private EditText searchEdittext;
    private String query;
    ArrayList<String> tabTitles;
    private List<Bundle> filterArguments;
    private SearchActivityViewPagerAdapter searchActivityViewPagerAdapter;

    //DialogBoxViews
    ArrayList<ContentSpinnerModel> contentType;
    Spinner select_spin;
    EditText isbn, author, title;
    TextView isbnText;
    ProgressBar progressBar;
    int pos;
    int posDefault = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        getDataFromIntent();
        initViews();
        setUpToolbar();
        initialiseFilter();
        setUpViewPager();
        setUpSearchBar();
        sgetTitle();
    }

    private void initialiseFilter() {
        filterArguments = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            Bundle filterArgument = new Bundle();
            filterArgument.putStringArrayList(SearchActivityFilterFragment.FILTER_MY_COMPANY, new ArrayList<String>());
            filterArgument.putStringArrayList(SearchActivityFilterFragment.FILTER_CATEGORIES, new ArrayList<String>());
            ArrayList<String> searchByArrayList = new ArrayList<>();
            searchByArrayList.add(Constants.Category.CATEGORY_SEARCH_BY_TITLE);
            filterArgument.putStringArrayList(SearchActivityFilterFragment.FILTER_SEARCH_BY, searchByArrayList);
            filterArgument.putInt(SearchActivityViewPagerAdapter.CURRENT_POSITION, i);

            filterArguments.add(filterArgument);
        }
    }

    private void setUpSearchBar() {

        searchEdittext.setText(query);
        searchEdittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                query = v.getText().toString();

                searchActivityViewPagerAdapter = null;
                viewPager.setAdapter(searchActivityViewPagerAdapter);

                setUpViewPager();
                CommonMethods.hideSoftInput(SearchActivity.this);
                return true;
            }
        });

    }

    private void getDataFromIntent() {
        query = getIntent().getStringExtra(SEARCH_QUERY);
    }

    private void initViews() {

        toolbar = findViewById(R.id.activity_search_toolbar);
        tabLayout = findViewById(R.id.activity_search_tablayout);
        viewPager = findViewById(R.id.activity_search_viewpager);
        searchEdittext = findViewById(R.id.activity_search_search_bar);

    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpViewPager() {
        searchActivityViewPagerAdapter = new SearchActivityViewPagerAdapter(getSupportFragmentManager(), SearchActivity.this, query, filterArguments);
        viewPager.setAdapter(searchActivityViewPagerAdapter);
        viewPager.setCurrentItem(viewPagerPosition);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(7);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_search_menu, menu);
        return true;
    }

    public void sgetTitle() {
        tabTitles = new ArrayList<>();
        tabTitles.add(getString(R.string.all));
        tabTitles.add(getString(R.string.articles));
        tabTitles.add(getString(R.string.ebooks));
        tabTitles.add(getString(R.string.videos));
        tabTitles.add(getString(R.string.courses));
        tabTitles.add(getString(R.string.podcasts));
        tabTitles.add(getString(R.string.infographics));
        tabTitles.add(getString(R.string.library_books));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                int position = viewPager.getCurrentItem();
                showRequestDialog(position);
                return true;
            case R.id.activity_search_menu_filter:
                inflateFilterFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void inflateFilterFragment() {

        SearchActivityFilterFragment searchActivityFilterFragment = new SearchActivityFilterFragment();
        searchActivityFilterFragment.setArguments(filterArguments.get(viewPager.getCurrentItem()));
        searchActivityFilterFragment.setOnFilterAppliedListener(SearchActivity.this);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_search_root_layout, searchActivityFilterFragment)
                .addToBackStack(SearchActivityFilterFragment.class.getSimpleName())
                .commit();

    }

    @Override
    public void OnFilterApplied(Bundle arguments) {

        viewPagerPosition = viewPager.getCurrentItem();
        filterArguments.set(viewPagerPosition, arguments);
        setUpViewPager();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            int position = viewPager.getCurrentItem();
            showRequestDialog(position);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void showRequestDialog(int position) {

        final Dialog mPublishDialog = new Dialog(this);
        mPublishDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mPublishDialog.setCancelable(false);
        mPublishDialog.setContentView(R.layout.request_book_view);

        ImageView cross = mPublishDialog.findViewById(R.id.cross);
        select_spin = mPublishDialog.findViewById(R.id.select_spin);
        progressBar = mPublishDialog.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        Button sbmitbtnn = mPublishDialog.findViewById(R.id.sbmitbtnn);

        setSpinnerData();
        init(mPublishDialog);

        final String titlesss = tabTitles.get(position);
        position = getIndexOf(titlesss);

        pos = CommonMethods.getContentTypeFromName(titlesss);

        if (titlesss.equalsIgnoreCase("Library Books")) {
            isbn.setVisibility(View.VISIBLE);
            isbnText.setVisibility(View.VISIBLE);
        } else {
            isbn.setVisibility(View.GONE);
            isbnText.setVisibility(View.GONE);
        }

        select_spin.setSelection(position);

        setAdapter();

        sbmitbtnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidation()) {
                    progressBar.setVisibility(View.VISIBLE);
                    sendRequest("" + pos);
                } else {
                    Toast.makeText(SearchActivity.this, "Please Fill Details First", Toast.LENGTH_SHORT).show();
                }
            }
        });


        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPublishDialog.dismiss();
                finish();
            }
        });

        mPublishDialog.show();
    }


    public boolean checkValidation() {
        boolean check = true;

        if (title.getText().toString().equals("")) {
            check = false;
        }

        return check;
    }

    public int getIndexOf(String name) {

        for (int i = 0; i < contentType.size(); i++) {

            if (name.contains(contentType.get(i).getItemName())) {

                posDefault = i;
                return posDefault;

            }
        }
        return posDefault;
    }

    public void init(Dialog bottomSheetDialog) {
        isbn = bottomSheetDialog.findViewById(R.id.isbn);
        author = bottomSheetDialog.findViewById(R.id.author);
        title = bottomSheetDialog.findViewById(R.id.title);
        isbnText = bottomSheetDialog.findViewById(R.id.isbnText);
//        sbmitbtnn = findViewById(R.id.sbmitbtnn);
    }

    public void setSpinnerData() {

        contentType = new ArrayList<>();

        ContentSpinnerModel article = new ContentSpinnerModel();
        article.setImage(R.drawable.article_explore);
        article.setItemName("Article");
        contentType.add(article);

        ContentSpinnerModel infoGraphics = new ContentSpinnerModel();
        infoGraphics.setImage(R.drawable.info_explore);
        infoGraphics.setItemName("Infographic");
        contentType.add(infoGraphics);

        ContentSpinnerModel video = new ContentSpinnerModel();
        video.setImage(R.drawable.video_explore);
        video.setItemName("Video");
        contentType.add(video);

        ContentSpinnerModel courses = new ContentSpinnerModel();
        courses.setImage(R.drawable.course_explore);
        courses.setItemName("Course");
        contentType.add(courses);

        ContentSpinnerModel podcast = new ContentSpinnerModel();
        podcast.setImage(R.drawable.podcast_expo);
        podcast.setItemName("Podcast");
        contentType.add(podcast);

        ContentSpinnerModel books = new ContentSpinnerModel();
        books.setImage(R.drawable.book_explore);
        books.setItemName("E Books");
        contentType.add(books);

        ContentSpinnerModel library = new ContentSpinnerModel();
        library.setImage(R.drawable.book_explore);
        library.setItemName("Books");
        contentType.add(library);
    }


    public void setAdapter() {
        SpinnerAdapterContent spinnerAdapterContent = new SpinnerAdapterContent(this, contentType);
        select_spin.setAdapter(spinnerAdapterContent);
    }

    private void sendRequest(String content_type) {
        String Isbn = "";
        if (isbn.getVisibility() == View.VISIBLE) {
            Isbn = isbn.getText().toString();
        }
        String Title = title.getText().toString();
        String Author = author.getText().toString();

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.REQUEST_BOOK.TITLE, Title);
            jsonObject.put(Constants.REQUEST_BOOK.AUTHOR, Author);
            jsonObject.put(Constants.REQUEST_BOOK.ISBN, Isbn);
            jsonObject.put(Constants.REQUEST_BOOK.LANGUAGE, "ENGLISH");
            jsonObject.put(Constants.REQUEST_BOOK.EMAIL, User.getCurrentUser(this).getUserEmail());
            jsonObject.put(Constants.REQUEST_BOOK.USRE_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.REQUEST_BOOK.CONTENT_TYPE, content_type);
            jsonObject.put(Constants.REQUEST_BOOK.ACTION, "submitdetails_new_book");

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressBar.setVisibility(View.GONE);
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    Toast.makeText(SearchActivity.this, "Successfully Requeseted", Toast.LENGTH_SHORT).show();
                                    finish();

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(SearchActivity.this);
                                } else {
                                    progressBar.setVisibility(View.GONE);
                                    String message = response.getString("response_message");
                                    Toast.makeText(SearchActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                progressBar.setVisibility(View.GONE);
                                e.printStackTrace();

                                displayToast(SearchActivity.this, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                                //                          CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Error occurred
                            progressBar.setVisibility(View.GONE);
                            displayToast(SearchActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);

                            //                    CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                        }
                    });

            VolleySingleton.getInstance(SearchActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            //  CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
            displayToast(SearchActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);
        }
    }
}