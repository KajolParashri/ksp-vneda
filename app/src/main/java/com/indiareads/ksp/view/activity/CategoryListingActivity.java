package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.ContentFragmentRecyclerAdapter;
import com.indiareads.ksp.adapters.PopularContentAdapter;
import com.indiareads.ksp.listeners.OnDraftedContentItemClickListener;
import com.indiareads.ksp.listeners.OnFilterAppliedListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.CategoryListingActivityFilterFragment;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CategoryListingActivity extends AppCompatActivity implements OnRefreshListener, OnFilterAppliedListener {

    private static final String TAG = CategoryListingActivity.class.getSimpleName();

    private CategoryListingActivityFilterFragment categoryListingActivityFilterFragment;
    private ContentFragmentRecyclerAdapter mContentRecyclerAdapter;
    private RecyclerView mContentsRecyclerView;
    private PopularContentAdapter mPopularContentRecyclerAdapter;
    private RecyclerView mPopularContentRecyclerView;
    private NestedScrollView mNestedScrollView;

    private List<Content> mPopularContents;
    LinearLayoutManager linearLayoutManager;
    private List<Content> mContents;

    private int pageNumber = 1;
    private boolean doPagination = true;
    private boolean mContentsLoading = true;
    private String mCategoryId, mCategoryName;

    private JSONObject selectedFilterObject;
    private JSONArray withingCompanyArray;
    private JSONArray subCatArray;
    private JSONArray contentTypeArray;
    TextView headingPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_listing);

        initViews();
// get Intent Data from Previos Activity
        getDataFromIntent();

        setUpToolbar();
        //   setContentTypeDescription();

        initContentList();
        initPopularContentList();

        // initialse and Set Adapter on List
        initContentRecyclerView();
        initPopularContentRecyclerView();

        setContentItemClickListener();
        // Pagination Code on Scroll
        setContentRecyclerViewScrollListener();
        // ClickListner
        setOnSortClickListner();
        setOnFilterClickListner();

        initialiseFilterArrays();

        // API network Calling Content List
        fetchContentData();
        // API network Calling Similar Content List
        fetchPopularContentData();
    }

    private void setOnFilterClickListner() {

        findViewById(R.id.activity_category_listing_filter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    categoryListingActivityFilterFragment = new CategoryListingActivityFilterFragment();

                    Bundle arguments = new Bundle();

                    ArrayList<String> filterMyCompany = new ArrayList<>();
                    ArrayList<String> filterCategories = new ArrayList<>();
                    ArrayList<String> filterContentTypes = new ArrayList<>();

                    for (int i = 0; i < withingCompanyArray.length(); i++)
                        filterMyCompany.add(withingCompanyArray.getString(i));
                    arguments.putStringArrayList(CategoryListingActivityFilterFragment.FILTER_MY_COMPANY, filterMyCompany);

                    for (int i = 0; i < subCatArray.length(); i++)
                        filterCategories.add(subCatArray.getString(i));
                    arguments.putStringArrayList(CategoryListingActivityFilterFragment.FILTER_CATEGORIES, filterCategories);

                    for (int i = 0; i < contentTypeArray.length(); i++)
                        filterContentTypes.add(contentTypeArray.getString(i));
                    arguments.putStringArrayList(CategoryListingActivityFilterFragment.FILTER_CONTENT_TYPES, filterContentTypes);

                    categoryListingActivityFilterFragment.setArguments(arguments);
                    categoryListingActivityFilterFragment.setOnFilterAppliedListener(CategoryListingActivity.this);

                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.activity_category_listing_root_layout, categoryListingActivityFilterFragment)
                            .addToBackStack(CategoryListingActivityFilterFragment.class.getSimpleName())
                            .commit();

                } catch (JSONException e) {
                    Error error = Error.generateError(JSONException.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                            "setOnFilterClickListner", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                    displayErrorUI(error);
                }
            }
        });
    }

    private void setOnSortClickListner() {
    }

    private void setUpToolbar() {

        Toolbar toolbar = findViewById(R.id.activity_category_listing_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initialiseFilterArrays() {
        try {
            withingCompanyArray = new JSONArray();
            subCatArray = new JSONArray();
            contentTypeArray = new JSONArray();

            selectedFilterObject = new JSONObject();
            selectedFilterObject.put(Constants.Network.WITHIN_COMPANY, withingCompanyArray);
            selectedFilterObject.put(Constants.Network.SUB_CAT, subCatArray);
            selectedFilterObject.put(Constants.Network.CONTENT_TYPE, contentTypeArray);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                    "initialiseFilterArrays", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void getDataFromIntent() {
        if (getIntent().getStringExtra(Constants.Category.CATEGORY_ID) != null) {
            mCategoryId = getIntent().getStringExtra(Constants.Category.CATEGORY_ID);
            mCategoryName = getIntent().getStringExtra(Constants.Category.CATEGORY_NAME);
            headingPage.setText(mCategoryName);
        } else {
            Error error = Error.generateError(NullPointerException.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                    "getDataFromIntent", Error.ERROR_TYPE_NORMAL_ERROR, "Arguments Null");

            displayErrorUI(error);
        }
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(this, R.id.activity_category_listing_root_layout, error);
    }

    private void initViews() {
        headingPage = findViewById(R.id.headingPage);
        mPopularContentRecyclerView = findViewById(R.id.activity_category_listing_popular_content_recyclerview);
        mContentsRecyclerView = findViewById(R.id.activity_category_listing_content_recyclerview);
        mNestedScrollView = findViewById(R.id.activity_category_listing_nestedscrollview);
    }

    private void setContentItemClickListener() {

        mContentRecyclerAdapter.setOnClickListener(new OnDraftedContentItemClickListener() {
            @Override
            public void onEditClick(View v, int position) {

            }

            @Override
            public void onDeleteClick(View v, int position) {

            }

            @Override
            public void onItemClick(View v, int position) {
                // Redirecting to Product Page on Click
                Intent productIntent = new Intent(CategoryListingActivity.this, ProductActivity.class);
                productIntent.putExtra(Constants.Network.CONTENT_ID, mContents.get(position).getContentId());
                productIntent.putExtra(Constants.Network.CONTENT_TYPE, mContents.get(position).getContentType());
                productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, mContents.get(position).getStatusType());
                startActivity(productIntent);
            }

            @Override
            public void onLikeCOunt(View v, int position) {

            }

        });

        mPopularContentRecyclerAdapter.setOnClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                // Redirection to the Product Page
                Intent productIntent = new Intent(CategoryListingActivity.this, ProductActivity.class);
                productIntent.putExtra(Constants.Network.CONTENT_ID, mPopularContents.get(position).getContentId());
                productIntent.putExtra(Constants.Network.CONTENT_TYPE, mPopularContents.get(position).getContentType());
                productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, mPopularContents.get(position).getStatusType());
                startActivity(productIntent);
            }
        });
    }

    private void initContentList() {
        mContents = new ArrayList<>();
        addProgressBarInContentList();
    }

    private void initPopularContentList() {
        mPopularContents = new ArrayList<>();
        addProgressBarInPopularContentList();
    }

    private void setContentRecyclerViewScrollListener() {


        mNestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = mNestedScrollView.getChildAt(mNestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (mNestedScrollView.getHeight() + mNestedScrollView.getScrollY()));

                if (doPagination && diff == 0) {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (mContentsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mContentsLoading = false;
                            addProgressBarInContentList();
                            mContentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);
                            pageNumber++;
                            fetchContentData();
                        }
                    }
                }
            }
        });
    }

    private void fetchPopularContentData() {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.CAT_ID, mCategoryId);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_POPULAR_CONTENT_CATEGORY);
            // Request Building for POST
            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT_LISTING_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
// Response Success
                                    //to remove progress bar
                                    mPopularContents.remove(mPopularContents.size() - 1);
                                    mPopularContentRecyclerAdapter.notifyItemRemoved(mPopularContents.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Content content = new Content();
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                        content.setStatusType(jsonObject1.getString(Constants.Network.CONTENT_STATUS_TYPE));
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));

                                        if (jsonObject1.has(Constants.AUTHOR)) {
                                            content.setSourceName(jsonObject1.getString(Constants.Network.SOURCE_NAME));
                                            content.setAuthorName(jsonObject1.getString(Constants.AUTHOR));
                                        }

                                        content.setAvgContentRating(jsonObject1.getString(Constants.Network.RATING));
                                        content.setContentType(jsonObject1.getString(Constants.Network.CONTENT_TYPE));
                                        content.setThumnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        content.setAvgContentRating(jsonObject1.getString(Constants.Network.RATING));

                                        //Adding Data to List
                                        mPopularContents.add(content);
                                    }

                                    mPopularContentRecyclerAdapter.notifyItemRangeInserted(mPopularContents.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {

                                    // On Authorization Faild Logout User to Login Screen
                                    SideBarPanel.logout(CategoryListingActivity.this);

                                } else {
                                    //to remove progress bar
                                    mPopularContents.remove(mPopularContents.size() - 1);
                                    mPopularContentRecyclerAdapter.notifyItemRemoved(mPopularContents.size() - 1);

                                    showEmptyViewInPopularContentList();
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                                        "fetchPopularContentData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                                    "fetchPopularContentData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                    "fetchPopularContentData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }


    private void fetchContentData() {
        mContentsLoading = false;
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.CAT_ID, mCategoryId);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_CONTENT_FOR_CATEGORY_LISTING);
            jsonObject.put(Constants.Network.SELECTED_FILTERS, selectedFilterObject);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT_LISTING_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mContents.remove(mContents.size() - 1);

                                    mContentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Content content = new Content();
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                        content.setContentType(jsonObject1.getString(Constants.Network.CONTENT_TYPE));
                                        if (jsonObject1.has(Constants.AUTHOR)) {
                                            content.setSourceName(jsonObject1.getString(Constants.Network.SOURCE_NAME));
                                            content.setAuthorName(jsonObject1.getString(Constants.AUTHOR));
                                        }
                                        content.setThumnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        content.setAvgContentRating(jsonObject1.getString(Constants.Network.RATING));
                                        content.setStatusType(jsonObject1.getString(Constants.Network.CONTENT_STATUS_TYPE));
                                        content.setAvgContentRating(jsonObject1.getString(Constants.Network.RATING));
                                        // Adding ddata to List
                                        mContents.add(content);
                                    }

                                    mContentRecyclerAdapter.notifyItemRangeInserted(mContents.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    // On Authorization Faild Logout User to Login Screen
                                    SideBarPanel.logout(CategoryListingActivity.this);

                                } else {

                                    //stop call to pagination in any case
                                    doPagination = false;

                                    //to remove progress bar
                                    mContents.remove(mContents.size() - 1);
                                    mContentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    if (pageNumber == 1) {
                                        //show msg no contents
                                        showEmptyViewInContentList();
                                    } else {
                                        //no more contents
                                    }
                                }
                                mContentsLoading = true;

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                                        "fetchContentData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                                    "fetchContentData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), CategoryListingActivity.class.getSimpleName(),
                    "fetchContentData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void showEmptyViewInContentList() {

        // Adding Empty View when No Data found in the List
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyContent.setTitle(getString(R.string.content_list_empty));
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));

        mContents.add(emptyContent);

        mContentsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mContentRecyclerAdapter.notifyItemInserted(0);
    }

    private void showEmptyViewInPopularContentList() {
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyContent.setTitle(getString(R.string.content_list_empty));
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));

        mPopularContents.add(emptyContent);

        mPopularContentRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mPopularContentRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInContentList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mContents.add(progressBarContent);
    }

    private void addProgressBarInPopularContentList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mPopularContents.add(progressBarContent);
    }

    private void initContentRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(this);
        mContentsRecyclerView.setLayoutManager(linearLayoutManager);

        mContentRecyclerAdapter = new ContentFragmentRecyclerAdapter(this, mContents);
        mContentsRecyclerView.setAdapter(mContentRecyclerAdapter);

        ViewCompat.setNestedScrollingEnabled(mContentsRecyclerView, false);
    }

    private void initPopularContentRecyclerView() {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mPopularContentRecyclerView.setLayoutManager(mLinearLayoutManager);

        mPopularContentRecyclerAdapter = new PopularContentAdapter(this, mPopularContents);
        mPopularContentRecyclerView.setAdapter(mPopularContentRecyclerAdapter);
    }

    @Override
    public void onRefresh() {

        fetchContentData();
    }

    @Override
    public void OnFilterApplied(Bundle arguments) {

        withingCompanyArray.remove(0);
        ArrayList<String> filterMyCompany = arguments.getStringArrayList(CategoryListingActivityFilterFragment.FILTER_MY_COMPANY);
        for (int i = 0; i < filterMyCompany.size(); i++)
            withingCompanyArray.put(filterMyCompany.get(i));

        for (int i = 0; i < subCatArray.length(); i++)
            subCatArray.remove(i);
        ArrayList<String> filterCategories = arguments.getStringArrayList(CategoryListingActivityFilterFragment.FILTER_CATEGORIES);

        for (int i = 0; i < filterCategories.size(); i++) {
            subCatArray.put(filterCategories.get(i));
        }

        for (int i = 0; i < contentTypeArray.length(); i++) {
            contentTypeArray.remove(i);
        }

        ArrayList<String> filterContentTypes = arguments.getStringArrayList(CategoryListingActivityFilterFragment.FILTER_CONTENT_TYPES);
        for (int i = 0; i < filterContentTypes.size(); i++) {
            contentTypeArray.put(filterContentTypes.get(i));
        }

        int oldSize = mContents.size();
        mContents.clear();
        mContentRecyclerAdapter.notifyItemRangeRemoved(0, oldSize);
        pageNumber = 1;
        doPagination = true;
        mContentsLoading = true;

        addProgressBarInContentList();
        fetchContentData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}