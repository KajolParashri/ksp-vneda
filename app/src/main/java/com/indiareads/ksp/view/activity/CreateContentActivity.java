package com.indiareads.ksp.view.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.CategoriesAdapter;
import com.indiareads.ksp.adapters.CategoriesImageAdapter;
import com.indiareads.ksp.adapters.CreateStepPagerAdapter;
import com.indiareads.ksp.listeners.OnCategoryClickListener;
import com.indiareads.ksp.listeners.OnCategoryImageClickListener;
import com.indiareads.ksp.listeners.OnFileCopiedListener;
import com.indiareads.ksp.model.Category;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.Params;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.service.PublishMediaService;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.FileExtensionUtil;
import com.indiareads.ksp.utils.FilePicker;
import com.indiareads.ksp.utils.ImageHandler;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.NonSwipeableViewPager;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;
import static com.indiareads.ksp.utils.CommonMethods.getStringSizeLengthFile;
import static com.indiareads.ksp.utils.Constants.AWS.BUCKET_NAME;
import static com.indiareads.ksp.utils.Constants.Article.ACTION;
import static com.indiareads.ksp.utils.Constants.Article.CATEGORY;
import static com.indiareads.ksp.utils.Constants.Article.CREATE_ARTICLE;
import static com.indiareads.ksp.utils.Constants.Article.DRAFT_STATUS;
import static com.indiareads.ksp.utils.Constants.Article.EXISTING_ARRAY;
import static com.indiareads.ksp.utils.Constants.Article.FEAT_IMAGE;
import static com.indiareads.ksp.utils.Constants.Article.NEW_ARRAY;
import static com.indiareads.ksp.utils.Constants.Article.TITLE;
import static com.indiareads.ksp.utils.Constants.Article.USER_ID;
import static com.indiareads.ksp.utils.Constants.COURSE.CREATE_COURSE;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_CHILD_ID;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_CHILD_NAME;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_ID;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_NAME;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_SUCCESS;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_TYPE_CHILD;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_TYPE_PARENT;
import static com.indiareads.ksp.utils.Constants.Category.GET_CHILD_CATEGORY;
import static com.indiareads.ksp.utils.Constants.Category.GET_PARENT_CATEGORY;
import static com.indiareads.ksp.utils.Constants.Content.ARTICLE;
import static com.indiareads.ksp.utils.Constants.Content.COURSE;
import static com.indiareads.ksp.utils.Constants.Content.EBOOK;
import static com.indiareads.ksp.utils.Constants.Content.INFOGRAPHIC;
import static com.indiareads.ksp.utils.Constants.Content.PODCAST;
import static com.indiareads.ksp.utils.Constants.Content.VIDEO;
import static com.indiareads.ksp.utils.Constants.Infographic.DESCRIPTION;
import static com.indiareads.ksp.utils.Constants.Network.DATA;
import static com.indiareads.ksp.utils.Constants.Network.RESPONSE_CODE;
import static com.indiareads.ksp.utils.Constants.Network.RESPONSE_MESSAGE;
import static com.indiareads.ksp.utils.Constants.Network.RESPONSE_OK;
import static com.indiareads.ksp.utils.FilePicker.REQUEST_CODE_CHOOSE_AUDIO;
import static com.indiareads.ksp.utils.FilePicker.REQUEST_CODE_CHOOSE_IMAGE;
import static com.indiareads.ksp.utils.FilePicker.REQUEST_CODE_CHOOSE_PDF;
import static com.indiareads.ksp.utils.FilePicker.REQUEST_CODE_CHOOSE_VIDEO;
import static com.indiareads.ksp.utils.FilePicker.chooseFile;
import static com.indiareads.ksp.utils.SharedPreferencesHelper.getCurrentUser;
import static com.indiareads.ksp.utils.Urls.baseAWSUrl;

public class CreateContentActivity extends AppCompatActivity implements OnCategoryClickListener, OnCategoryImageClickListener {

    static final String TAG = CreateContentActivity.class.getSimpleName();
    String dataType;

    static final int READCODE = 111;
    ImageView featureImage;
    TextView resetImage;
    static final int IMAGE_TYPE_FEAT = 0;
    static final int IMAGE_TYPE_INFOGRAPHIC = 104;
    CreateStepPagerAdapter viewPagerAdapter;
    int IMAGE_TYPE;
    List<Category> mParentCategoryList = new ArrayList<>();
    List<Category> mChildCategoryList = new ArrayList<>();
    Dialog mDialog;
    CategoriesAdapter mParentAdapter;
    CategoriesAdapter mChildAdapter;
    public static String selectedCategory = "";
    public String selectedCategoryCheck = "";
    Uri mUploadFeatImageUri;
    RelativeLayout ss1;
    Uri mUploadVideoUri;
    Uri mUploadAudioUri;
    Uri mUploadImageUri;
    Uri mUploadBookUri;
    CardView card;
    public static int draftValue = 0;
    boolean isCheck = false;
    static final String PUBLIC = "1";
    static final String PRIVATE = "2";

    EditText mLink;
    CardView mDescriptionCardView;
    EditText mDescription;

    EditText mTranscript;
    EditText mISBNno;

    Toolbar mToolbar;
    EditText mTitle;
    FrameLayout activityCreateRootLayout;

    String UPLOAD_FEAT_NAME = "";
    String UPLOAD_VIDEO_NAME = "";
    String UPLOAD_PODCAST_NAME = "";
    String UPLOAD_INFOGRAPHIC_NAME = "";
    String UPLOAD_BOOK_NAME = "";
    String UrlFromEdittext = "";
    String selectedParent = "", selectedChild = "";
    String stepCheck = "1";
    String content_type_Str = "";
    ImageView deleteFile;
    StateProgressBar stateProgressBar;
    ProgressBar progressBar;
    NonSwipeableViewPager viewPager;
    ImageView post_button, post_button_prev;
    TextView mOrText;
    TextView selectedCategoryTv, text, selected_cat_text;
    //  RelativeLayout draftLayout;
    Button uploadFileBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_content);
        setViewPager();
        getIntentData();
        initViews();
        setupToolbar();
        fetchParentCategories();
        initCategoryDialog();

    }

    private void setupToolbar() {

        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        mToolbar.setTitle("Create " +
                dataType);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

    }

    private void initViews() {

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        stateProgressBar = findViewById(R.id.stateProgressBar);
        post_button = findViewById(R.id.post_button);
        post_button_prev = findViewById(R.id.post_button_prev);

        post_button_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                post_button.setImageResource(R.drawable.next_btn);
                stepCheck = "1";

                if (stepCheck.equals("1")) {
                    post_button_prev.setVisibility(View.GONE);

                } else {
                    post_button_prev.setVisibility(View.VISIBLE);

                }

                StateProgressBar.StateNumber sNum = StateProgressBar.StateNumber.ONE;
                if (viewPager.getCurrentItem() == 1) {
                    viewPager.setCurrentItem(0);
                }
                stateProgressBar.setCurrentStateNumber(sNum);
            }
        });


        post_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!selectedCategoryCheck.equals("")) {

                    StateProgressBar.StateNumber sNum = StateProgressBar.StateNumber.TWO;
                    if (stepCheck.equals("2")) {
                        draftValue = 0;
                        submitBasicInfo(dataType, draftValue);
                    }
                    stepCheck = "2";
                    if (stepCheck.equals("1")) {
                        post_button_prev.setVisibility(View.GONE);
                    } else {
                        post_button_prev.setVisibility(View.VISIBLE);
                    }

                    if (viewPager.getCurrentItem() == 0) {
                        post_button.setImageResource(R.drawable.check_btn);
                        sNum = StateProgressBar.StateNumber.TWO;
                        viewPager.setCurrentItem(1);
                    }

                    stateProgressBar.setCurrentStateNumber(sNum);
                    setupToolbar();
                } else {
                    displayToast(CreateContentActivity.this, "Please Select Category");
                }

            }
        });

        if (stepCheck.equals("1")) {
            post_button_prev.setVisibility(View.GONE);
        }

        mToolbar = findViewById(R.id.activity_create_toolbar);
        mTitle = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_title);
        resetImage = viewPagerAdapter.getView(1).findViewById(R.id.removeBtn);
        featureImage = viewPagerAdapter.getView(1).findViewById(R.id.mainImage);
        RelativeLayout clickToUpload = viewPagerAdapter.getView(1).findViewById(R.id.clickToUpload);
        ss1 = viewPagerAdapter.getView(1).findViewById(R.id.ss1);
        card = viewPagerAdapter.getView(1).findViewById(R.id.card);

        selected_cat_text = viewPagerAdapter.getView(1).findViewById(R.id.selected_cat_text);
        text = viewPagerAdapter.getView(1).findViewById(R.id.text);
        card.setVisibility(View.GONE);
        resetImage.setVisibility(View.GONE);
        featureImage.setVisibility(View.GONE);
        if (dataType.equals(INFOGRAPHIC)) {
            ss1.setVisibility(View.GONE);
        }
        clickToUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onUploadImageClick(IMAGE_TYPE_FEAT);
            }
        });

        resetImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                card.setVisibility(View.GONE);
                featureImage.setVisibility(View.GONE);
                resetImage.setVisibility(View.GONE);
                mUploadFeatImageUri = null;

            }
        });

        resetImage.setVisibility(View.GONE);
        activityCreateRootLayout = viewPagerAdapter.getView(1).

                findViewById(R.id.activity_create_root_layout);
        //  draftLayout.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //  public void onClick(View v) {
        //submitBasicInfo();
        //        draftValue = 1;
        //        submitDraftInfo(dataType, draftValue);
        //     }
        //});

    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/KSP/data/"
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "INFOGRAPHIC_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private File storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Logger.debug(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);

            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

        } catch (FileNotFoundException e) {
            Logger.debug(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Logger.debug(TAG, "Error accessing file: " + e.getMessage());
        }
        return pictureFile;
    }

    private void setViewPager() {
        viewPager = findViewById(R.id.viewPagerCreate);
        viewPagerAdapter = new CreateStepPagerAdapter(this);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    //          draftLayout.setVisibility(View.VISIBLE);
                } else if (position == 0) {
                    //        draftLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //Pickers
    private void bringImagePicker(Uri uri) {
        if (dataType.equals(INFOGRAPHIC)) {
            CropImage.activity(uri)
                    .setGuidelines(CropImageView.Guidelines.ON_TOUCH)
                    .setAllowFlipping(true)
                    .setAllowCounterRotation(true)
                    .start(this);
        } else {
            CropImage.activity(uri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAllowFlipping(true)
                    .setAspectRatio(4, 6)
                    .setAllowCounterRotation(true)
                    .setShowCropOverlay(true)
                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                    .start(this);
        }
    }

    private void bringVideoChooser() {
        chooseFile(CreateContentActivity.this, REQUEST_CODE_CHOOSE_VIDEO);
    }

    private void bringAudioPicker() {
        chooseFile(CreateContentActivity.this, REQUEST_CODE_CHOOSE_AUDIO);
    }

    private void bringPDFPicker() {
        chooseFile(CreateContentActivity.this, REQUEST_CODE_CHOOSE_PDF);
    }

    private void onUploadImageClick(int type) {

        IMAGE_TYPE = type;

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, READCODE);


        } else {
            FilePicker.chooseFile(this, REQUEST_CODE_CHOOSE_IMAGE);
            //   bringImagePicker();

        }
    }

    private void onUploadVideoClick() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            displayToast(this, getString(R.string.grant_permissions));

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, READCODE);

        } else {
            bringVideoChooser();
        }

    }

    private void onUploadAudioClick() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            displayToast(this, getString(R.string.grant_permissions));

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, READCODE);
        } else {
            bringAudioPicker();
        }
    }

    private void onUploadPDFClick() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            displayToast(this, getString(R.string.grant_permissions));

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, READCODE);

        } else {
            bringPDFPicker();
        }
    }


    private void getIntentData() {
        dataType = getIntent().getStringExtra("content");
        content_type_Str = getIntent().getStringExtra("content_type");
        switch (dataType) {
            case ARTICLE:
                break;
            case INFOGRAPHIC:
                displayExtraUIForInfographic();
                break;
            case VIDEO:
                displayExtraUIForVideo();
                break;
            case PODCAST:
                displayExtraUIForPodcast();
                break;
            case EBOOK:
                displayExtraUIForEBook();
                break;
            case COURSE:
                displayExtraUIForCourse();
        }
    }

    private void initCategoryDialog() {
        mDialog = new Dialog(this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.select_category_dialog);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void initParentCategoryImageRecyclerView() {
        RecyclerView mDisplayParentCategoryRecyclerView = findViewById(R.id.activity_create_category_image_recycler_view);
        mDisplayParentCategoryRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        CategoriesImageAdapter mCategoriesImageAdapter = new CategoriesImageAdapter(mParentCategoryList, this);
        mDisplayParentCategoryRecyclerView.setAdapter(mCategoriesImageAdapter);
    }


    public void initDialogItems(String categoryId, String CategoryName, String svgImage) {

        selectedCategoryTv = mDialog.findViewById(R.id.selectedCategory);
        selectedCategoryTv.setText(CategoryName);
        RecyclerView mChildCategoriesRecyclerView = mDialog.findViewById(R.id.child_category_rv);
        mChildCategoriesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mChildAdapter = new CategoriesAdapter(mChildCategoryList, this, categoryId, svgImage);
        mChildCategoriesRecyclerView.setAdapter(mChildAdapter);
        mDialog.show();
    }

    private void fetchParentCategories() {

        try {
            progressBar.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, GET_PARENT_CATEGORY);

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        if (response.getString(RESPONSE_MESSAGE).equals(CATEGORY_SUCCESS) &&
                                response.getInt(RESPONSE_CODE) == 200) {
                            progressBar.setVisibility(View.GONE);

                            JSONArray jsonArray = response.getJSONArray(DATA);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                int j = i + 1;
                                String url = Urls.baseImageUrl + "/icons/category/" + j + ".svg";
                                Category category = new Category();
                                category.setCategoryId(jsonArray.getJSONObject(i).getString(CATEGORY_ID));
                                category.setCategoryImage(url);
                                category.setCategoryName(jsonArray.getJSONObject(i).getString(CATEGORY_NAME));
                                category.setCategoryType(CATEGORY_TYPE_PARENT);
                                mParentCategoryList.add(category);
                            }

                            initParentCategoryImageRecyclerView();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(CreateContentActivity.this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressBar.setVisibility(View.GONE);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Logger.info(TAG, error.getMessage() + " is the error");
                    progressBar.setVisibility(View.GONE);
                }
            });

            VolleySingleton.getInstance(CreateContentActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            progressBar.setVisibility(View.GONE);

            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void fetchChildCategories(String parentCatId) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, GET_CHILD_CATEGORY);
            jsonObject.put(Constants.Category.PARENR_CAT_ID, parentCatId);

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getString(RESPONSE_MESSAGE).equals(CATEGORY_SUCCESS) &&
                                response.getInt(RESPONSE_CODE) == 200) {

                            mChildAdapter.clearList();

                            JSONArray jsonArray = response.getJSONArray(DATA);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                Category category = new Category();
                                category.setCategoryId(jsonArray.getJSONObject(i).getString(CATEGORY_CHILD_ID));
                                String child_cat_name = jsonArray.getJSONObject(i).getString(CATEGORY_CHILD_NAME);
                                if (child_cat_name.contains("Others")) {
                                    child_cat_name = "Others";
                                }
                                category.setCategoryName(child_cat_name);
                                category.setCategoryType(CATEGORY_TYPE_CHILD);
                                mChildCategoryList.add(category);
                            }
                            mChildAdapter.notifyDataSetChanged();
                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(CreateContentActivity.this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Handle error (finish activity)
                    Logger.info(TAG, error.getMessage() + " is the error");
                }
            });

            VolleySingleton.getInstance(CreateContentActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void submitBasicInfo(String dataType, int draftStr) {
        switch (dataType) {
            case ARTICLE:
                areArticleDetailsCorrect(draftStr);
                break;
            case INFOGRAPHIC:
                areInfographicDetailsCorrect(draftStr);
                break;
            case VIDEO:
                areVideoDetailsCorect(draftStr);
                break;
            case PODCAST:
                arePodcastDetailsCorect(draftStr);
                break;
            case EBOOK:
                areBookDetailsCorrect(draftStr);
                break;
            case COURSE:
                areCourseDetailsCorect(draftStr);
            default:
                break;

        }
    }

    private void submitDraftInfo(String dataType, int draftStr) {
        switch (dataType) {
            case ARTICLE:
                areArticleDetailsCorrectDraft(draftStr);
                break;
            case INFOGRAPHIC:
                areInfoDetailsCorrectDraft(draftStr);
                break;
            case VIDEO:
                areVideoDetailsCorrectDraft(draftStr);
                break;
            case PODCAST:
                arePodcastDetailsCorrectDraft(draftStr);
                break;
            case EBOOK:
                areBookDetailsCorrectDraft(draftStr);
                break;
            case COURSE:
                areCourseDetailsCorrectDraft(draftStr);
            default:
                break;

        }
    }

    //Setting up dialog (private/public) for those items who need direct upload
    private void setupDialog(final int draftStr) {
        final BottomSheetDialog mPublishDialog = new BottomSheetDialog(this);
        mPublishDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mPublishDialog.setContentView(R.layout.editor_publish_confirmation_dialog);

        Button mPublishPublic = mPublishDialog.findViewById(R.id.btn_publish);
        CheckBox statusCheck = mPublishDialog.findViewById(R.id.statusCheck);
        TextView mPublishDialogTextView = mPublishDialog.findViewById(R.id.publish_text_view);

        mPublishDialogTextView.setText("Publish " + dataType + " ?");

        mPublishPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isCheck) {
                    switch (dataType) {
                        case INFOGRAPHIC:
                            startPublishInfographicService(PUBLIC, mUploadFeatImageUri, draftStr);
                            break;
                        case VIDEO:
                            startPublishVideoService(PUBLIC, mUploadFeatImageUri, draftStr);
                            break;
                        case PODCAST:
                            startPublishPodcastService(PUBLIC, mUploadFeatImageUri, draftStr);
                            break;
                        case EBOOK:
                            startPublishBookService(PUBLIC, mUploadFeatImageUri, draftStr);
                            break;
                    }
                } else {

                    switch (dataType) {
                        case INFOGRAPHIC:
                            startPublishInfographicService(PRIVATE, mUploadFeatImageUri, draftStr);
                            break;
                        case VIDEO:
                            startPublishVideoService(PRIVATE, mUploadFeatImageUri, draftStr);
                            break;
                        case PODCAST:
                            startPublishPodcastService(PRIVATE, mUploadFeatImageUri, draftStr);
                            break;
                        case EBOOK:
                            startPublishBookService(PRIVATE, mUploadFeatImageUri, draftStr);
                            break;
                    }
                }
                mPublishDialog.dismiss();

            }
        });

        statusCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (compoundButton.isChecked()) {
                    isCheck = true;

                } else if (!compoundButton.isChecked()) {
                    isCheck = false;
                }

            }

        });
        User user = User.getCurrentUser(this);

        String featureId = user.getCompanyFeaturesFeatureId();
        String featureValue = user.getCompanyFeaturesValue();
        if (featureId.equalsIgnoreCase("1") && featureValue.equalsIgnoreCase("1")) {
            statusCheck.setVisibility(View.VISIBLE);
        } else {
            statusCheck.setVisibility(View.GONE);
        }

        mPublishDialog.show();
    }

    public ImageView getFeatureImage() {
        return featureImage;
    }


    public void saveDraft(int draftStr) {
        switch (dataType) {
            case INFOGRAPHIC:
                startPublishInfographicService(PRIVATE, mUploadFeatImageUri, draftStr);
                break;
            case VIDEO:
                startPublishVideoService(PRIVATE, mUploadFeatImageUri, draftStr);
                break;
            case PODCAST:
                startPublishPodcastService(PRIVATE, mUploadFeatImageUri, draftStr);
                break;
            case EBOOK:
                startPublishBookService(PRIVATE, mUploadFeatImageUri, draftStr);
                break;
        }
    }

    //Checks for confirming whether data is added in required fields or not
    private void areArticleDetailsCorrect(int strDraft) {
        String title = mTitle.getText().toString();
        title = title.trim();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            //    title = title.replace("'", "''");
            uploadArticle(strDraft);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void areInfographicDetailsCorrect(int draftStr) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            if (mUploadImageUri == null && mLink.getText().toString().equals("")) {
                displayToast(this, "Please enter required fields");
                return;
            }


            setupDialog(draftStr);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void areVideoDetailsCorect(int draftStr) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            if (mUploadVideoUri == null && mLink.getText().toString().equals("")) {
                displayToast(this, "Please enter required fields");
                return;
            }
            setupDialog(draftStr);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void arePodcastDetailsCorect(int draftStr) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            if (mUploadAudioUri == null && mLink.getText().toString().equals("")) {
                displayToast(this, "Please enter required fields");
                return;
            }
            setupDialog(draftStr);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void areBookDetailsCorrect(int draftStr) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            if (mUploadBookUri == null && mLink.getText().toString().equals("")) {
                displayToast(this, "Please enter required fields");
                return;
            }
            setupDialog(draftStr);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void areCourseDetailsCorect(int draftStr) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            uploadCourse(draftStr);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }


    private void areArticleDetailsCorrectDraft(int draftStr) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            uploadArticle(draftStr);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void areBookDetailsCorrectDraft(int strDraft) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            saveDraft(strDraft);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void areInfoDetailsCorrectDraft(int strDraft) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            saveDraft(strDraft);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void areVideoDetailsCorrectDraft(int strDraft) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            saveDraft(strDraft);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void arePodcastDetailsCorrectDraft(int strDraft) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            saveDraft(strDraft);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }

    private void areCourseDetailsCorrectDraft(int strDraft) {
        String title = mTitle.getText().toString();
        if (!title.isEmpty() && !selectedCategory.isEmpty()) {
            uploadCourse(strDraft);
        } else {
            displayToast(this, "Please enter required fields");
        }
    }


    private void uploadArticle(final int draftStr) {

        if (mUploadFeatImageUri != null) {
            uploadFile(mUploadFeatImageUri);
        }
        CommonMethods.showProgressBar(activityCreateRootLayout, R.id.activity_create_root_layout);
        try {
            JSONArray existingArray = new JSONArray();
            existingArray.put("35762");

            JSONArray newArray = new JSONArray();
            newArray.put("dfs");
            newArray.put("gdhg");
            newArray.put("poio");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, CREATE_ARTICLE);
            jsonObject.put(CATEGORY, selectedCategory);
            if (mUploadFeatImageUri != null)
                jsonObject.put(FEAT_IMAGE, baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + UPLOAD_FEAT_NAME);
            else jsonObject.put(FEAT_IMAGE, "");
            String title = mTitle.getText().toString();
            title = String.valueOf(Html.fromHtml(title));
            jsonObject.put(TITLE, title);
            jsonObject.put(USER_ID, getCurrentUser(this).getUserId());
            jsonObject.put(DRAFT_STATUS, draftStr);
            jsonObject.put(EXISTING_ARRAY, existingArray);
            jsonObject.put(NEW_ARRAY, newArray);

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {
                            if (draftStr == 0) {

                                String title = mTitle.getText().toString();
                                //    title = title.replace("'", "''");
                                Intent editorIntent = new Intent(CreateContentActivity.this, EditorActivity.class);
                                editorIntent.putExtra(DATA, response.getString(DATA));
                                editorIntent.putExtra("type", ARTICLE);
                                editorIntent.putExtra("content_type", content_type_Str);
                                editorIntent.putExtra("typeIntent", "insert");

                                editorIntent.putExtra(TITLE, title);
                                startActivity(editorIntent);
                                finish();

                            } else if (draftStr == 1) {
                                finish();
                                Toast.makeText(CreateContentActivity.this, R.string.article_draft_success, Toast.LENGTH_SHORT).show();
                            }

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(CreateContentActivity.this);
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                                    "createArticle", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                            displayErrorUI(error);
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                                "createArticle", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        displayErrorUI(error);
                    }
                    CommonMethods.hideProgressBar(activityCreateRootLayout, R.id.activity_create_root_layout);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                            "createArticle", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                    displayErrorUI(error);
                }
            });
            VolleySingleton.getInstance(CreateContentActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                    "createArticle", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    switch (IMAGE_TYPE) {
                        case IMAGE_TYPE_FEAT:

                            mUploadFeatImageUri = result.getUri();

                            File file = new File(mUploadFeatImageUri.getPath());
                            long sizeOriginal = file.length();
                            String sizeOri = getStringSizeLengthFile(sizeOriginal);
                            Logger.debug("sizeCom", sizeOri);

                            File compressFile = CommonMethods.fileCompress(file, this);
                            long size = compressFile.length();
                            String sizeStr = getStringSizeLengthFile(size);
                            Logger.debug("sizeCom", sizeStr);

                            Logger.info(TAG, mUploadFeatImageUri.toString());
                            mUploadFeatImageUri = Uri.fromFile(compressFile);

                            String fiName = CommonMethods.getFileName(mUploadFeatImageUri, CreateContentActivity.this);
                            fiName = fiName.replace("cropped", "");
                            card.setVisibility(View.VISIBLE);
                            featureImage.setVisibility(View.VISIBLE);
                            Picasso.get().load(mUploadFeatImageUri).into(featureImage);
                            resetImage.setVisibility(View.VISIBLE);

                            break;

                        case IMAGE_TYPE_INFOGRAPHIC:

                            mUploadImageUri = result.getUri();
                            ArrayList<String> list1 = FileExtensionUtil.checkForInfo();
                            Logger.debug("activityResult", list1.toString());
                            String fileExtension1 = CommonMethods.getExtensionFile(this, mUploadImageUri);

                            if (list1.contains(fileExtension1)) {

                                UPLOAD_INFOGRAPHIC_NAME = CommonMethods.createUploadFileName(mUploadImageUri, this);

                                Bitmap srcBitmp = ImageHandler.uriToBitmap(mUploadImageUri, CreateContentActivity.this);
                                Bitmap dstBtmp = generateCentreCropBit(srcBitmp);
                                File path = storeImage(dstBtmp);
                                Uri newFile = Uri.fromFile(path);
                                mUploadFeatImageUri = newFile;

                                String fileName = CommonMethods.getFileName(mUploadImageUri, CreateContentActivity.this);

                                if (uploadFileBtn != null && uploadFileBtn.getVisibility() == View.VISIBLE) {
                                    uploadFileBtn.setText(fileName);
                                    deleteFile.setVisibility(View.VISIBLE);
                                    mLink.setVisibility(View.GONE);
                                }

                                Logger.info(TAG, mUploadImageUri + " is the info uri");
                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid File Type", Toast.LENGTH_SHORT).show();
                            }
                            break;


                    }
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    Logger.info(TAG, error.getMessage());
                }
                break;
            case REQUEST_CODE_CHOOSE_VIDEO:
                if (resultCode == RESULT_OK) {
                    if (data != null) {

                        Uri uri = data.getData();
                        ArrayList<String> list1 = FileExtensionUtil.checkForVideo();
                        Logger.debug("activityResult", list1.toString());
                        String fileExtension1 = CommonMethods.getExtensionFile(this, uri);

                        if (list1.contains(fileExtension1)) {

                            String fileName = CommonMethods.getFileName(uri, CreateContentActivity.this);

                            if (uploadFileBtn != null && uploadFileBtn.getVisibility() == View.VISIBLE) {
                                uploadFileBtn.setText(fileName);
                                deleteFile.setVisibility(View.VISIBLE);
                                mLink.setVisibility(View.GONE);
                                mOrText.setVisibility(View.GONE);
                            }

                            copyFileToLocalStorage(uri, REQUEST_CODE_CHOOSE_VIDEO);
                        }
                    }
                } else {
                    Logger.info(TAG, "Error fetching video");
                }
                break;
            case REQUEST_CODE_CHOOSE_AUDIO:
                if (resultCode == RESULT_OK) {
                    if (data != null) {

                        Uri uri = data.getData();
                        ArrayList<String> list1 = FileExtensionUtil.checkForPodcast();
                        Logger.debug("activityResult", list1.toString());
                        String fileExtension1 = CommonMethods.getExtensionFile(this, uri);

                        if (list1.contains(fileExtension1)) {

                            String fileName = CommonMethods.getFileName(uri, CreateContentActivity.this);


                            if (uploadFileBtn != null && uploadFileBtn.getVisibility() == View.VISIBLE) {
                                uploadFileBtn.setText(fileName);
                                deleteFile.setVisibility(View.VISIBLE);
                                mLink.setVisibility(View.GONE);
                                mOrText.setVisibility(View.GONE);
                            }

                            copyFileToLocalStorage(uri, REQUEST_CODE_CHOOSE_AUDIO);
                        }
                    }
                } else {
                    Logger.info(TAG, "Error fetching audio");
                }
                break;
            case REQUEST_CODE_CHOOSE_IMAGE:
                if (resultCode == RESULT_OK) {
                    if (data != null) {

                        Uri uri = data.getData();
                        ArrayList<String> list1 = FileExtensionUtil.checkForImage();
                        Logger.debug("activityResult", list1.toString());
                        String fileExtension1 = CommonMethods.getExtensionFile(this, uri);

                        if (list1.contains(fileExtension1)) {

                            copyFileToLocalStorage(uri);
                        }

                    } else {
                        CommonMethods.displayToast(this, getString(R.string.something_went_wrong));
                    }
                }
                break;
            case REQUEST_CODE_CHOOSE_PDF:
                if (resultCode == RESULT_OK) {
                    if (data != null) {

                        Uri uri = data.getData();

                        ArrayList<String> list1 = FileExtensionUtil.checkForBooks();
                        Logger.debug("activityResult", list1.toString());
                        String fileExtension1 = CommonMethods.getExtensionFile(this, uri);

                        if (list1.contains(fileExtension1)) {

                            String fileName = CommonMethods.getFileName(uri, CreateContentActivity.this);

                            if (uploadFileBtn != null && uploadFileBtn.getVisibility() == View.VISIBLE) {
                                uploadFileBtn.setText(fileName);
                                deleteFile.setVisibility(View.VISIBLE);
                                mLink.setVisibility(View.GONE);
                                mOrText.setVisibility(View.GONE);
                            }

                            copyFileToLocalStorage(uri, REQUEST_CODE_CHOOSE_PDF);
                        }

                    }
                } else {
                    Logger.info(TAG, "Error fetching book");
                }
        }

    }

    private void copyFileToLocalStorage(final Uri uri) {

        FilePicker.copyFileFromUri(this, uri, new OnFileCopiedListener() {
            @Override
            public void onFileCopied(File file) {
                Logger.info(TAG, file.getAbsolutePath());
                bringImagePicker(uri);
            }

            @Override
            public void onFileCopyFailed(Error error) {
                Logger.info(TAG, "Error");

            }
        });

    }

    public Bitmap generateCentreCropBit(Bitmap srcBmp) {
        Bitmap dstBmp = null;
        float RATIO1 = (float) 5 / 4;
        float RATIO2 = (float) 7 / 4;

        float width = (float) srcBmp.getWidth();
        float height = (float) srcBmp.getHeight();

        float ratio = height / width;

        if (ratio <= RATIO2 && ratio >= RATIO1) {
            dstBmp = srcBmp;

        } else {
            int dimension = Math.min(srcBmp.getWidth(), srcBmp.getHeight());
            dstBmp = ThumbnailUtils.extractThumbnail(srcBmp, dimension, dimension);
        }

        return dstBmp;
    }

    private void uploadCourse(final int draftStr) {

        if (mUploadFeatImageUri != null) uploadFile(mUploadFeatImageUri);

        CommonMethods.showProgressBar(activityCreateRootLayout, R.id.activity_create_root_layout);
        try {
            JSONArray existingArray = new JSONArray();
            existingArray.put("35762");

            JSONArray newArray = new JSONArray();
            newArray.put("dfs");
            newArray.put("gdhg");
            newArray.put("poio");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ACTION, CREATE_COURSE);
            jsonObject.put(CATEGORY, selectedCategory);
            if (mUploadFeatImageUri != null)
                jsonObject.put(FEAT_IMAGE, baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + UPLOAD_FEAT_NAME);
            else jsonObject.put(FEAT_IMAGE, "");
            String title = mTitle.getText().toString();
            jsonObject.put(TITLE, title);
            jsonObject.put(USER_ID, getCurrentUser(this).getUserId());
            jsonObject.put(EXISTING_ARRAY, existingArray);
            jsonObject.put(DRAFT_STATUS, draftStr);
            jsonObject.put(NEW_ARRAY, newArray);
            jsonObject.put(DESCRIPTION, mDescription.getText().toString());
            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Logger.info(TAG, response.toString());

                        if (response.getString(RESPONSE_CODE).equals(RESPONSE_OK)) {


                            if (draftStr == 0) {
                                String title = mTitle.getText().toString();
                                //  title = title.replace("'", "''");
                                String courseName = title;
                                String data = response.getString("data");

                                startSetupCourseActivity(data, courseName);

                            } else if (draftStr == 1) {
                                finish();
                                Toast.makeText(CreateContentActivity.this, R.string.article_draft_success, Toast.LENGTH_SHORT).show();
                            }

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(CreateContentActivity.this);
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                                    "createCourse", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);
                            displayErrorUI(error);
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                                "createCourse", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        displayErrorUI(error);
                    }
                    CommonMethods.hideProgressBar(activityCreateRootLayout, R.id.activity_create_root_layout);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    Error error = Error.generateError(VolleyError.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                            "createCourse", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                    displayErrorUI(error);

                }
            });

            VolleySingleton.getInstance(CreateContentActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                    "createCourse", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);

        }
    }

    private void uploadFile(final Uri fileUri) {
        File file = new File(fileUri.getPath());
        String fileExt = MimeTypeMap.getFileExtensionFromUrl(file.toString());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");
        String formattedDate = df.format(c.getTime());
        UPLOAD_FEAT_NAME = getCurrentUser(this)
                .getUserId() + "_" + formattedDate + "." + fileExt;

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "ap-south-1:345799b2-3506-4245-a702-3a7e9d4c0071", // Identity pool ID
                Regions.AP_SOUTH_1 // Region
        );
        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider, Region.getRegion(Regions.AP_SOUTH_1));
        TransferNetworkLossHandler.getInstance(this);

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .defaultBucket(BUCKET_NAME)
                        .context(getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

        TransferObserver uploadObserver =
                transferUtility.upload(Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + UPLOAD_FEAT_NAME, file, CannedAccessControlList.PublicRead);

        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    Logger.info(TAG, "Uploading completed !!" + "--" + baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + UPLOAD_FEAT_NAME);
                    try {
                        File file = new File(fileUri.getPath());
                        if (file.exists())
                            file.delete();
                    } catch (Exception e) {
                        Logger.error(TAG, e.getMessage());
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;
                Logger.info(TAG, String.valueOf(percentDone) + "% uploading complete");
            }

            @Override
            public void onError(int id, Exception ex) {
                displayToast(CreateContentActivity.this, "ERROR: " + ex.getMessage() + ". Please try again");
                Logger.info(TAG, ex.getMessage() + " is the error while uploading");
                try {
                    File file = new File(fileUri.getPath());
                    if (file.exists())
                        file.delete();
                } catch (Exception e) {
                    Logger.error(TAG, e.getMessage());
                }
            }
        });

    }


    //Start publish Services
    private void startPublishVideoService(String statusType, Uri featImageUri, int draftStr) {

        Logger.info("Publish", "Startred");

        String featImageName = "";
        String featImageUrl = "";

        if (featImageUri != null) {
            featImageName = CommonMethods.createUploadFileName(featImageUri, this);
            featImageUrl = baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + featImageName;
        } else featImageUri = Uri.parse("");

        String mLinkData = mLink.getText().toString();

        String videoName = "";
        if (mLinkData.isEmpty()) videoName = UPLOAD_VIDEO_NAME;

        if (!mLinkData.isEmpty()) {
            checkWhichVideoUrl(mLinkData);
        } else {
            UrlFromEdittext = baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + videoName;
        }

        Params params = new Params();
        params.setMediaType(VIDEO);
        params.setContentType(content_type_Str);
        params.setMediaName(videoName);
        params.setDraftStatus(draftStr);
        params.setSelectedCategory(selectedCategory);
        params.setFeatImageName(featImageName);
        params.setFeatUri(featImageUri.toString());


        if (mUploadVideoUri != null) {
            params.setMediaUri(mUploadVideoUri.toString());

        } else {
            params.setMediaUri("");

        }

        if (!UrlFromEdittext.equals("")) {
            params.setMediaURL(UrlFromEdittext);
        } else {
            params.setMediaURL("");
        }

        String title = mTitle.getText().toString();
        //   title = title.replace("'", "''");
        params.setMediaTitle(title);
        params.setPrivacyStatus(statusType);
        params.setUserId(getCurrentUser(CreateContentActivity.this).getUserId());
        params.setMediaDescription(mDescription.getText().toString());
        params.setMediaFeatImage(featImageUrl);
        Intent intent = new Intent(CreateContentActivity.this, PublishMediaService.class);
        intent.putExtra("data", params);

        startService(intent);
        if (draftValue == 0) {
            Snackbar.make(activityCreateRootLayout, "Content Uploading", Snackbar.LENGTH_SHORT).show();
            delayFinish();
        } else {
            Snackbar.make(activityCreateRootLayout, "Content Drafting", Snackbar.LENGTH_SHORT).show();
            delayFinish();
        }

    }

    private void startPublishPodcastService(String statusType, Uri featImageUri, int draftStr) {

        String featImageName = "";
        String featImageUrl = "";

        if (featImageUri != null) {
            featImageName = CommonMethods.createUploadFileName(featImageUri, this);
            featImageUrl = baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + featImageName;
        } else featImageUri = Uri.parse("");

        String mLinkData = mLink.getText().toString();

        String podCastName = "";
        if (mLinkData.isEmpty()) podCastName = UPLOAD_PODCAST_NAME;

        if (!mLinkData.isEmpty()) {
            UrlFromEdittext = mLinkData;
        } else {
            UrlFromEdittext = baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + podCastName;
        }

        Params params = new Params();
        params.setMediaType(PODCAST);
        params.setMediaName(podCastName);
        params.setDraftStatus(draftStr);
        params.setSelectedCategory(selectedCategory);
        params.setContentType(content_type_Str);
        params.setFeatImageName(featImageName);
        params.setFeatUri(featImageUri.toString());
        if (mUploadAudioUri != null) {
            params.setMediaUri(mUploadAudioUri.toString());
            //  params.setMediaURL(UrlFromEdittext);
        } else {
            params.setMediaUri("");
            //    params.setMediaURL("");
        }

        if (!UrlFromEdittext.equals("")) {
            params.setMediaURL(UrlFromEdittext);
        } else {
            params.setMediaURL("");
        }

        params.setMediaTranscript(mTranscript.getText().toString());

        String title = mTitle.getText().toString();
        // title = title.replace("'", "''");

        params.setMediaTitle(title);

        params.setPrivacyStatus(statusType);
        params.setUserId(getCurrentUser(CreateContentActivity.this).getUserId());
        params.setMediaDescription(mDescription.getText().toString());
        params.setMediaFeatImage(featImageUrl);


        Intent intent = new Intent(CreateContentActivity.this, PublishMediaService.class);
        intent.putExtra("data", params);
        startService(intent);
        if (draftValue == 0) {
            Snackbar.make(activityCreateRootLayout, "Content Uploading", Snackbar.LENGTH_LONG).show();
            delayFinish();
        } else {
            Snackbar.make(activityCreateRootLayout, "Content Drafting", Snackbar.LENGTH_LONG).show();
            delayFinish();
        }
    }

    private void startPublishInfographicService(String statusType, Uri featImageUri, int draftStr) {
        String featImageName = "";
        String featImageUrl = "";
        if (featImageUri != null) {
            featImageName = CommonMethods.createUploadFileName(featImageUri, this);
            featImageUrl = baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + featImageName;
        } else featImageUri = Uri.parse("");

        String mLinkData = mLink.getText().toString();
        String imageName = "";
        if (mLinkData.isEmpty()) imageName = UPLOAD_INFOGRAPHIC_NAME;
        if (!mLinkData.isEmpty()) {
            UrlFromEdittext = mLinkData;
        } else {
            UrlFromEdittext = baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + imageName;
        }

        Params params = new Params();
        params.setMediaType(INFOGRAPHIC);
        params.setMediaName(imageName);
        params.setDraftStatus(draftStr);
        params.setSelectedCategory(selectedCategory);
        params.setFeatImageName(featImageName);
        params.setFeatUri(featImageUri.toString());

        if (mUploadImageUri != null && !mUploadImageUri.toString().equals("")) {
            params.setMediaUri(mUploadImageUri.toString());
        } else {
            params.setMediaUri("");
        }

        if (!UrlFromEdittext.equals("")) {
            params.setMediaURL(UrlFromEdittext);
        } else {
            params.setMediaURL("");
        }

        String title = mTitle.getText().toString();

        //      title = title.replace("'", "''");

        params.setMediaTitle(title);
        params.setPrivacyStatus(statusType);
        params.setUserId(getCurrentUser(CreateContentActivity.this).getUserId());
        params.setMediaDescription(mDescription.getText().toString());
        params.setMediaFeatImage(featImageUrl);

        Logger.info(TAG, "PARAMS:" + params.toString());
        Intent intent = new Intent(CreateContentActivity.this, PublishMediaService.class);
        intent.putExtra("data", params);
        startService(intent);

        if (draftValue == 0) {
            Snackbar.make(activityCreateRootLayout, "Content Uploading", Snackbar.LENGTH_LONG).show();
            delayFinish();
        } else {
            Snackbar.make(activityCreateRootLayout, "Content Drafting", Snackbar.LENGTH_LONG).show();
            delayFinish();
        }
    }

    public void delayFinish() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 2000);
    }

    private void startPublishBookService(String statusType, Uri featImageUri, int draftStr) {
        String featImageName = "";
        String featImageUrl = "";
        if (featImageUri != null) {
            featImageName = CommonMethods.createUploadFileName(featImageUri, this);
            featImageUrl = baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + featImageName;
        } else featImageUri = Uri.parse("");

        String mLinkData = mLink.getText().toString();

        String bookName = "";
        if (mLinkData.isEmpty()) bookName = UPLOAD_BOOK_NAME;

        if (!mLinkData.isEmpty()) {
            UrlFromEdittext = mLinkData;
        } else {
            UrlFromEdittext = baseAWSUrl + "/" + Constants.AWS.CREATE_CONTENT_KEY + User.getCurrentUser(CreateContentActivity.this).getUserId() + "/" + bookName;
        }

        Params params = new Params();
        params.setMediaType(EBOOK);
        params.setMediaName(bookName);
        params.setDraftStatus(draftStr);
        params.setContentType(content_type_Str);

        params.setSelectedCategory(selectedCategory);
        params.setFeatImageName(featImageName);
        params.setFeatUri(featImageUri.toString());

        if (mUploadBookUri != null) {
            params.setMediaUri(mUploadBookUri.toString());
            //      params.setMediaURL(UrlFromEdittext);
        } else {
            params.setMediaUri("");
            //    params.setMediaURL("");
        }

        if (!UrlFromEdittext.equals("")) {
            params.setMediaURL(UrlFromEdittext);
        } else {
            params.setMediaURL("");
        }


        String title = mTitle.getText().toString();
        //    title = title.replace("'", "''");

        params.setMediaTitle(title);
        params.setPrivacyStatus(statusType);

        params.setUserId(getCurrentUser(CreateContentActivity.this).getUserId());
        params.setMediaDescription(mDescription.getText().toString());
        params.setMediaFeatImage(featImageUrl);

        Intent intent = new Intent(CreateContentActivity.this, PublishMediaService.class);
        intent.putExtra("data", params);

        startService(intent);

        if (draftValue == 0) {
            Snackbar.make(activityCreateRootLayout, "Content Uploading", Snackbar.LENGTH_SHORT).show();
            delayFinish();
        } else {
            Snackbar.make(activityCreateRootLayout, "Content Drafting", Snackbar.LENGTH_SHORT).show();
            delayFinish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_next:

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCategoryClickListener(Category category) {
        int lastKnown = mChildCategoryList.size();
        mChildCategoryList.clear();
        mChildAdapter.notifyItemRangeRemoved(0, lastKnown);
        fetchChildCategories(category.getCategoryId());
        mChildAdapter.notifyDataSetChanged();
        mParentAdapter.setSelectedId(category.getCategoryId());
    }

    @Override
    public void onCategoryChildClickListener(Category category) {

        selectedCategory = category.getCategoryId();
        selectedCategoryCheck = category.getCategoryId();

        mDialog.dismiss();
        selectedChild = category.getCategoryName();
        selected_cat_text.setText("Category : " + selectedParent + " > " + selectedChild);
        StateProgressBar.StateNumber sNum = StateProgressBar.StateNumber.TWO;
        if (stepCheck.equals("2")) {
            draftValue = 0;
            submitBasicInfo(dataType, draftValue);
        }
        stepCheck = "2";

        if (stepCheck.equals("1")) {
            post_button_prev.setVisibility(View.GONE);
        } else {
            post_button_prev.setVisibility(View.VISIBLE);
        }

        if (viewPager.getCurrentItem() == 0) {
            post_button.setImageResource(R.drawable.check_btn);
            sNum = StateProgressBar.StateNumber.TWO;
            viewPager.setCurrentItem(1);
        }

        stateProgressBar.setCurrentStateNumber(sNum);
        setupToolbar();
    }

    @Override
    public void onCategoryImageClickListener(Category category) {

        selectedParent = category.getCategoryName();
        String mimae = category.getCategoryImage();
        initDialogItems(category.getCategoryId(), category.getCategoryName(), mimae);
        fetchChildCategories(category.getCategoryId());
    }

    private void checkWhichVideoUrl(String url) {
        if (url.contains("youtube") || url.contains("youtu.be")) {

            if (url.contains("embed")) {
                UrlFromEdittext = url;
            } else {
                String item = "https://www.youtube.com/embed/";

                String ss = url;
                if (ss.contains("v=")) {
                    ss = ss.substring(ss.indexOf("v=") + 2);
                    item += ss;
                } else {
                    ss = ss.substring(ss.indexOf("e/") + 2);
                    item += ss;
                }
                UrlFromEdittext = item;
            }

        } else {
            UrlFromEdittext = url;
        }
    }

    private void startSetupCourseActivity(String data, String courseName) {

        String title = mTitle.getText().toString();
        //   title = title.replace("'", "''");

        Intent intent = new Intent(CreateContentActivity.this, SetupCourseActivity.class);
        intent.putExtra("course_name", courseName);
        intent.putExtra("content_type", content_type_Str);
        intent.putExtra("title", title);
        intent.putExtra("data", data);
        intent.putExtra("typeIntent", "insert");
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void copyFileToLocalStorage(final Uri uri, final int REQUEST_CODE) {

        final Dialog dialog = new Dialog(CreateContentActivity.this);
        dialog.setContentView(R.layout.progress_dialog_layout);
        dialog.setCancelable(false);
        if (!isFinishing()) {
            dialog.show();
        }

        FilePicker.copyFileFromUri(this, uri, new OnFileCopiedListener() {
            @Override
            public void onFileCopied(File file) {
                switch (REQUEST_CODE) {

                    case REQUEST_CODE_CHOOSE_VIDEO:
                        mUploadVideoUri = Uri.parse(file.getAbsolutePath());
                        UPLOAD_VIDEO_NAME = CommonMethods.createUploadFileName(mUploadVideoUri, CreateContentActivity.this);
                        break;

                    case REQUEST_CODE_CHOOSE_AUDIO:
                        mUploadAudioUri = Uri.parse(file.getAbsolutePath());
                        UPLOAD_PODCAST_NAME = CommonMethods.createUploadFileName(mUploadAudioUri, CreateContentActivity.this);
                        break;

                    case REQUEST_CODE_CHOOSE_PDF:
                        mUploadBookUri = Uri.parse(file.getAbsolutePath());
                        UPLOAD_BOOK_NAME = CommonMethods.createUploadFileName(mUploadBookUri, CreateContentActivity.this);

                        if (UPLOAD_BOOK_NAME.contains(".pdf")) {
                            UPLOAD_BOOK_NAME = UPLOAD_BOOK_NAME;
                        } else {
                            UPLOAD_BOOK_NAME = UPLOAD_BOOK_NAME + "pdf";
                        }

                        break;
                }

                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFileCopyFailed(Error error) {
//                CommonMethods.displayToast(CreateContentActivity.this, getString(R.string.something_went_wrong));
                dialog.dismiss();
            }
        });
    }

    //Displaying appropriate UI
    private void displayErrorUI(Error error) {
        CommonMethods.hideProgressBar(activityCreateRootLayout, R.id.activity_create_root_layout);
        CommonMethods.showErrorView(CreateContentActivity.this, R.id.activity_create_root_layout, error);
    }

    private void displayExtraUIForInfographic() {
        deleteFile = viewPagerAdapter.getView(1).findViewById(R.id.deleteFile);
        deleteFile.setVisibility(View.GONE);

        uploadFileBtn = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_data_upload_button);
        mOrText = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_upload_or_text);
        mLink = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_upload_link);
        mDescription = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_content_description);
        mDescriptionCardView = viewPagerAdapter.getView(1).findViewById(R.id.desc_card_view);

        uploadFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onUploadImageClick(IMAGE_TYPE_INFOGRAPHIC);
            }
        });

        mDescriptionCardView.setVisibility(View.VISIBLE);
        uploadFileBtn.setText(R.string.upload_infographic);
        uploadFileBtn.setVisibility(View.VISIBLE);
        mOrText.setVisibility(View.VISIBLE);
        mLink.setVisibility(View.VISIBLE);


        mLink.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (text.length() > 0) {

                    uploadFileBtn.setVisibility(View.GONE);
                    deleteFile.setVisibility(View.GONE);
                    mLink.setVisibility(View.VISIBLE);
                    mOrText.setVisibility(View.GONE);

                } else {

                    uploadFileBtn.setVisibility(View.VISIBLE);
                    deleteFile.setVisibility(View.VISIBLE);
                    uploadFileBtn.setText(R.string.upload_infographic);
                    mLink.setVisibility(View.VISIBLE);
                    mOrText.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        deleteFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFile.setVisibility(View.GONE);
                uploadFileBtn.setText(R.string.upload_infographic);
                mLink.setVisibility(View.VISIBLE);
                mOrText.setVisibility(View.VISIBLE);
            }
        });
    }

    private void displayExtraUIForVideo() {
        deleteFile = viewPagerAdapter.getView(1).findViewById(R.id.deleteFile);
        deleteFile.setVisibility(View.GONE);
        uploadFileBtn = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_data_upload_button);
        mOrText = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_upload_or_text);
        mLink = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_upload_link);
        mDescription = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_content_description);
        mDescriptionCardView = viewPagerAdapter.getView(1).findViewById(R.id.desc_card_view);

        uploadFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onUploadVideoClick();
            }
        });

        mDescriptionCardView.setVisibility(View.VISIBLE);
        uploadFileBtn.setText(R.string.upload_video);
        uploadFileBtn.setVisibility(View.VISIBLE);
        mOrText.setVisibility(View.VISIBLE);
        mLink.setVisibility(View.VISIBLE);


        mLink.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (text.length() > 0) {

                    uploadFileBtn.setVisibility(View.GONE);
                    deleteFile.setVisibility(View.GONE);
                    mLink.setVisibility(View.VISIBLE);
                    mOrText.setVisibility(View.GONE);

                } else {

                    uploadFileBtn.setVisibility(View.VISIBLE);
                    deleteFile.setVisibility(View.VISIBLE);
                    uploadFileBtn.setText(R.string.upload_video);
                    mLink.setVisibility(View.VISIBLE);
                    mOrText.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        deleteFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFile.setVisibility(View.GONE);
                uploadFileBtn.setText(R.string.upload_video);
                mLink.setVisibility(View.VISIBLE);
                mOrText.setVisibility(View.VISIBLE);
            }
        });
    }

    private void displayExtraUIForPodcast() {
        deleteFile = viewPagerAdapter.getView(1).findViewById(R.id.deleteFile);
        deleteFile.setVisibility(View.GONE);
        uploadFileBtn = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_data_upload_button);
        mOrText = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_upload_or_text);
        mLink = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_upload_link);
        mDescription = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_content_description);
        mDescriptionCardView = viewPagerAdapter.getView(1).findViewById(R.id.desc_card_view);

        mTranscript = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_content_transcript);
        CardView mTranscriptCardView = viewPagerAdapter.getView(1).findViewById(R.id.trans_card_view);
        mTranscriptCardView.setVisibility(View.VISIBLE);

        uploadFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onUploadAudioClick();
            }
        });

        mDescriptionCardView.setVisibility(View.VISIBLE);
        uploadFileBtn.setText(R.string.upload_podcast);
        uploadFileBtn.setVisibility(View.VISIBLE);
        mOrText.setVisibility(View.VISIBLE);
        mLink.setVisibility(View.VISIBLE);


        mLink.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (text.length() > 0) {

                    uploadFileBtn.setVisibility(View.GONE);
                    deleteFile.setVisibility(View.GONE);
                    mLink.setVisibility(View.VISIBLE);
                    mOrText.setVisibility(View.GONE);

                } else {

                    uploadFileBtn.setVisibility(View.VISIBLE);
                    deleteFile.setVisibility(View.VISIBLE);
                    uploadFileBtn.setText(R.string.upload_podcast);
                    mLink.setVisibility(View.VISIBLE);
                    mOrText.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        deleteFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFile.setVisibility(View.GONE);
                uploadFileBtn.setText(R.string.upload_podcast);
                mLink.setVisibility(View.VISIBLE);
                mOrText.setVisibility(View.VISIBLE);
            }
        });
    }

    public void displayExtraUIForEBook() {
        deleteFile = viewPagerAdapter.getView(1).findViewById(R.id.deleteFile);
        deleteFile.setVisibility(View.GONE);
        uploadFileBtn = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_data_upload_button);
        mOrText = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_upload_or_text);
        TextView mExtraDetailsHeading = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_extra_details_heading);
        mLink = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_upload_link);
        mDescription = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_content_description);
        mDescriptionCardView = viewPagerAdapter.getView(1).findViewById(R.id.desc_card_view);

        mISBNno = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_content_transcript);
        mISBNno.setInputType(InputType.TYPE_CLASS_NUMBER);
        CardView mTranscriptCardView = viewPagerAdapter.getView(1).findViewById(R.id.trans_card_view);
        mTranscriptCardView.setVisibility(View.VISIBLE);

        uploadFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onUploadPDFClick();
            }
        });


        mExtraDetailsHeading.setText(R.string.isbn_no);
        mDescriptionCardView.setVisibility(View.VISIBLE);
        uploadFileBtn.setText(R.string.upload_ebook);
        uploadFileBtn.setVisibility(View.VISIBLE);
        mOrText.setVisibility(View.VISIBLE);
        mLink.setVisibility(View.VISIBLE);


        mLink.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (text.length() > 0) {

                    uploadFileBtn.setVisibility(View.GONE);
                    deleteFile.setVisibility(View.GONE);
                    mLink.setVisibility(View.VISIBLE);
                    mOrText.setVisibility(View.GONE);

                } else {

                    uploadFileBtn.setVisibility(View.VISIBLE);
                    deleteFile.setVisibility(View.VISIBLE);
                    uploadFileBtn.setText(R.string.upload_ebook);
                    mLink.setVisibility(View.VISIBLE);
                    mOrText.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        deleteFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFile.setVisibility(View.GONE);
                uploadFileBtn.setText(R.string.upload_ebook);
                mLink.setVisibility(View.VISIBLE);
                mOrText.setVisibility(View.VISIBLE);
            }
        });
    }

    public void displayExtraUIForCourse() {
        deleteFile = viewPagerAdapter.getView(1).findViewById(R.id.deleteFile);
        deleteFile.setVisibility(View.GONE);

        mDescription = viewPagerAdapter.getView(1).findViewById(R.id.activity_create_content_description);
        mDescriptionCardView = viewPagerAdapter.getView(1).findViewById(R.id.desc_card_view);
        mDescriptionCardView.setVisibility(View.VISIBLE);

    }
}
