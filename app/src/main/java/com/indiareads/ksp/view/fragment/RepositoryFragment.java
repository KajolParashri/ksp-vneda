package com.indiareads.ksp.view.fragment;


import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.DirectoryItemRecyclerAdapter;
import com.indiareads.ksp.adapters.PathRecyclerAdapter;
import com.indiareads.ksp.listeners.OnDirectoryItemClickListener;
import com.indiareads.ksp.listeners.OnFileCopiedListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.model.Directory;
import com.indiareads.ksp.model.DirectoryItem;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.UploadParams;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.service.UploadService;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.FileExtensionUtil;
import com.indiareads.ksp.utils.FilePicker;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class RepositoryFragment extends Fragment implements OnRefreshListener {

    public static final String REPO_TYPE = "Repo Type";
    public static final String REPO_TYPE_PERSONAL = "1onItemClicked";
    public static final String REPO_TYPE_COMPANY = "2";
    private static final String TAG = RepositoryFragment.class.getSimpleName();
    private static final String RESPONSE_STATUS_HAS_REPOSITORY = "2";
    private static final String DIRECTORY_HOME_ID = "0";
    private static final int REQUEST_CODE_PERMISSION_STORAGE = 100;

    private RecyclerView pathRecyclerView;
    private RecyclerView dirRecyclerView;
    private PathRecyclerAdapter mPathRecyclerAdapter;
    private DirectoryItemRecyclerAdapter mDirectoryItemRecyclerAdapter;
    private LinearLayoutManager mPathLinearLayoutManager;

    private Button uploadFilesButton;
    private Button createFolderButton;
    private View fragmentRootView;
    private String repoType;
    private String repoId;
    private String currentDirId;

    private List<DirectoryItem> mDirectoryItems;
    private List<Directory> mDirectories;
    LinearLayout viewMainNoRepo;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            UploadParams uploadParams = intent.getParcelableExtra(UploadService.UPLOAD_PARAMS);
            String status = intent.getStringExtra(UploadService.STATUS);
            try {
                if (uploadParams.getJsonObject().getString(Constants.Network.PARENT_FOLDER).equals(currentDirId)) {
                    if (status.equals(UploadService.STATUS_SUCCESS)) {
                        enableTouch();
                        CommonMethods.displayToast(getContext(), getContext().getString(R.string.upload_successful));
                        fetchDirectoryDetails();
                        try {
                            File file = new File(Constants.File.ROOT_DIR_PATH + "/" + uploadParams.getFileName());
                            if (file.exists())
                                file.delete();
                        } catch (Exception e) {
                            Logger.error(TAG, e.getMessage());
                        }
                    } else if (status.equals(UploadService.STATUS_FAILED)) {
                        enableTouch();
                        CommonMethods.displayToast(getContext(), getContext().getString(R.string.upload_failed));
                        fetchDirectoryDetails();
                        try {
                            File file = new File(Constants.File.ROOT_DIR_PATH + "/" + uploadParams.getFileName());
                            if (file.exists())
                                file.delete();
                        } catch (Exception e) {
                            Logger.error(TAG, e.getMessage());
                        }
                    } else {
                        DirectoryItem directoryItem = null;
                        for (int i = 0; i < mDirectoryItems.size(); i++) {
                            if (mDirectoryItems.get(i).getId().equals(uploadParams.getEntityId())) {
                                directoryItem = mDirectoryItems.get(i);
                                break;
                            }
                        }

                        if (directoryItem == null) {
                            directoryItem = createDirectoryItem(new File(Constants.File.ROOT_DIR_PATH, uploadParams.getFileName()));
                            directoryItem.setId(uploadParams.getEntityId());
                        }
                    }
                }
            } catch (JSONException e) {
                Error.generateError(JSONException.class.getSimpleName(), TAG,
                        "onReceive", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            }
        }
    };

    public RepositoryFragment() {
        // Required empty public constructor
    }

    public void disableTouch() {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void enableTouch() {
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    public void onPause() {
        // Unregister since the activity is paused.
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(
                mMessageReceiver);
        super.onPause();
    }

    @Override
    public void onResume() {
        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                mMessageReceiver, new IntentFilter(Constants.Network.ACTION_UPLOADED_FILE_DATABASE_ENTRY));
        super.onResume();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentRootView = inflater.inflate(R.layout.fragment_repository, container, false);

        getDataFromArguments();
        initViews();
        String isAdmin = User.getCurrentUser(getActivity()).getIsAdmin();

        if (repoType.equals(REPO_TYPE_COMPANY) && isAdmin.equals("1")) {
            checkForRepositoryAdmin();
        } else if (repoType.equals(REPO_TYPE_COMPANY)) {
            checkForRepositoryAdmin();
        } else {
            checkForRepository();
        }
        return fragmentRootView;
    }

    private void initViews() {
        pathRecyclerView = fragmentRootView.findViewById(R.id.fragment_repository_path_recyclerview);
        dirRecyclerView = fragmentRootView.findViewById(R.id.fragment_repository_directory_recyclerview);
        uploadFilesButton = fragmentRootView.findViewById(R.id.fragment_repository_upload_button);
        createFolderButton = fragmentRootView.findViewById(R.id.fragment_repository_create_folder_button);
    }

    private void checkForRepository() {
        CommonMethods.showProgressBar(fragmentRootView, R.id.fragment_repository_root_layout);
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_REPO_CHECK);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getContext()).getUserId());
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(getContext()).getCompanyId());

            jsonObject.put(Constants.Network.TYPE, repoType);
            Logger.info("resultRepo", jsonObject.toString());
            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info("resultRepo", response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    if (response.getString(Constants.Network.RESPONSE_STATUS).equals(RESPONSE_STATUS_HAS_REPOSITORY)) {
                                        JSONObject data = response.getJSONArray(Constants.Network.DATA).getJSONObject(0);
                                        repoId = data.getString(Constants.Network.ID);
                                        Logger.error("repoId", repoId);
                                        setUpRepositoryLayout();
                                    } else
                                        setUpNoRepositoryLayout();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                            "checkForRepository", Error.ERROR_TYPE_SERVER_ERROR, "400");

                                    displayErrorUI(error);
                                }

                                CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_repository_root_layout);
                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                                        "checkForRepository", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                    "checkForRepository", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "checkForRepository", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void checkForRepositoryAdmin() {
        CommonMethods.showProgressBar(fragmentRootView, R.id.fragment_repository_root_layout);
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_REPO_CHECK);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getContext()).getCompanyId());
            jsonObject.put(Constants.Network.TYPE, repoType);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(getContext()).getCompanyId());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    if (response.getString(Constants.Network.RESPONSE_STATUS).equals(RESPONSE_STATUS_HAS_REPOSITORY)) {
                                        JSONObject data = response.getJSONArray(Constants.Network.DATA).getJSONObject(0);
                                        repoId = data.getString(Constants.Network.ID);
                                        Logger.error("repoId", repoId);
                                        setUpRepositoryLayout();
                                    } else
                                        setUpNoRepositoryLayout();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                            "checkForRepository", Error.ERROR_TYPE_SERVER_ERROR, "400");

                                    displayErrorUI(error);
                                }

                                CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_repository_root_layout);
                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                                        "checkForRepository", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                    "checkForRepository", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "checkForRepository", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void setUpNoRepositoryLayout() {
        viewMainNoRepo = fragmentRootView.findViewById(R.id.repository_fragment_no_repository_layout);
        viewMainNoRepo.setVisibility(View.VISIBLE);
        String isAdmin = User.getCurrentUser(getActivity()).getIsAdmin();

        if (repoType.equals(REPO_TYPE_PERSONAL) || isAdmin.equals("1")) {
            ((TextView) fragmentRootView.findViewById(R.id.fragment_repository_start_text)).setText(R.string.start_uploading_your_files_to_your_private_repository);
            Button fragment_repository_start_button = fragmentRootView.findViewById(R.id.fragment_repository_start_button);
            fragment_repository_start_button.setVisibility(View.VISIBLE);
            setStartRepositoryClickListener();

        } else {
            ((TextView) fragmentRootView.findViewById(R.id.fragment_repository_start_text)).setText(R.string.contact_hr_to_start_with_your_repository);
            Button fragment_repository_start_button = fragmentRootView.findViewById(R.id.fragment_repository_start_button);
            fragment_repository_start_button.setVisibility(View.GONE);
        }
    }

    private void setStartRepositoryClickListener() {
        fragmentRootView.findViewById(R.id.fragment_repository_start_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String isAdmin = User.getCurrentUser(getActivity()).getIsAdmin();
                if (isAdmin.equals("1")) {
                    sendStartRepositoryRequestAdmin();
                } else {
                    sendStartRepositoryRequest();
                }
            }
        });
    }

    private void sendStartRepositoryRequest() {
//        CommonMethods.showProgressBar(fragmentRootView, R.id.fragment_repository_root_layout);
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_REPO_CREATE);
            //if (repoType.equals(REPO_TYPE_PERSONAL)) {
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getContext()).getUserId());
            // } else {
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(getContext()).getCompanyId());
            //  }
            jsonObject.put(Constants.Network.TYPE, repoType);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    repoId = response.getString(Constants.Network.DATA);

                                    viewMainNoRepo.setVisibility(View.GONE);
                                    setUpRepositoryLayout();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                            "sendStartRepositoryRequest", Error.ERROR_TYPE_SERVER_ERROR, "400");

                                    displayErrorUI(error);
                                }


                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                                        "sendStartRepositoryRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                    "sendStartRepositoryRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                            //  Toast.makeText(getActivity(), "VolleyError" + error.toString(), Toast.LENGTH_SHORT).show();

                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "checkForRepository", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            // Toast.makeText(getActivity(), "JSONLast" + error.toString(), Toast.LENGTH_SHORT).show();
            displayErrorUI(error);
        }
    }

    private void sendStartRepositoryRequestAdmin() {
//        CommonMethods.showProgressBar(fragmentRootView, R.id.fragment_repository_root_layout);
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_REPO_CREATE);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getContext()).getCompanyId());
            jsonObject.put(Constants.Network.TYPE, repoType);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(getActivity(), "" + response, Toast.LENGTH_SHORT).show();
                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    repoId = response.getString(Constants.Network.DATA);

                                    viewMainNoRepo.setVisibility(View.GONE);
                                    setUpRepositoryLayout();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                            "sendStartRepositoryRequest", Error.ERROR_TYPE_SERVER_ERROR, "400");

                                    displayErrorUI(error);
                                }

//                                CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_repository_root_layout);
                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                                        "sendStartRepositoryRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                Toast.makeText(getActivity(), "JSONException" + error.toString(), Toast.LENGTH_SHORT).show();
                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                    "sendStartRepositoryRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                            Toast.makeText(getActivity(), "VolleyError" + error.toString(), Toast.LENGTH_SHORT).show();

                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "checkForRepository", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            Toast.makeText(getActivity(), "JSONLast" + error.toString(), Toast.LENGTH_SHORT).show();
            displayErrorUI(error);
        }
    }

    private void setUpRepositoryLayout() {
        fragmentRootView.findViewById(R.id.repository_fragment_repository_layout).setVisibility(View.VISIBLE);

        initDirectoryList();
        initDirectoryItemList();

        initPathRecyclerView();
        initDirectoryItemsRecyclerView();

        setBackButtonClickListener();

        String isAdmin = User.getCurrentUser(getActivity()).getIsAdmin();
        if (isAdmin.equals("1") || repoType.equals(REPO_TYPE_PERSONAL)) {
            setCreateFolderClickListener();
            setUploadFilesClickListener();
        } else {
            createFolderButton.setVisibility(View.GONE);
            uploadFilesButton.setVisibility(View.GONE);
        }

        setPathRecyclerViewClickListener();
        setDirectoryItemsRecyclerViewClickListener();

        currentDirId = DIRECTORY_HOME_ID;
        fetchDirectoryDetails();
    }

    private void setUploadFilesClickListener() {
        uploadFilesButton.setVisibility(View.VISIBLE);
        uploadFilesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE
                            , Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_STORAGE);
                } else {
                    FilePicker.chooseFile(getActivity(), FilePicker.REQUEST_CODE_CHOOSE_FILE);
                }
            }
        });
    }

    private void setCreateFolderClickListener() {
        createFolderButton.setVisibility(View.VISIBLE);

        createFolderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCreateFolderRequest();
            }
        });
    }

    private void sendCreateFolderRequest() {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_REPO_CREATE_FOLDER);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getContext()).getUserId());
            jsonObject.put(Constants.Network.REPO_ID, repoId);
            jsonObject.put(Constants.Network.PARENT_FOLDER, currentDirId);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    fetchDirectoryDetails();
                                    CommonMethods.displayToast(getContext(), getContext().getString(R.string.folder_created_successfully));
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                            "sendCreateFolderRequest", Error.ERROR_TYPE_SERVER_ERROR, "400");
                                    CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                                }
                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                                        "sendCreateFolderRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                    "sendCreateFolderRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "sendCreateFolderRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
        }
    }

    private void setBackButtonClickListener() {
        fragmentRootView.findViewById(R.id.fragment_repository_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDirectories.size() > 1)
                    setAsCurrentDirectory(mDirectories.size() - 2);
            }
        });

    }

    private void setAsCurrentDirectory(int position) {

        currentDirId = mDirectories.get(position).getId();

        int oldSize = mDirectories.size();
        for (int i = position + 1; i < mDirectories.size(); )
            mDirectories.remove(i);
        mPathRecyclerAdapter.notifyItemRangeRemoved(position + 1, oldSize - 1);

        fetchDirectoryDetails();

    }

    private void setPathRecyclerViewClickListener() {
        mPathRecyclerAdapter.setOnClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                setAsCurrentDirectory(position);
            }
        });
    }

    private void setDirectoryItemsRecyclerViewClickListener() {

        mDirectoryItemRecyclerAdapter.setOnClickListener(new OnDirectoryItemClickListener() {
            @Override
            public void onOptionsClicked(View view, final int position) {
                PopupMenu popup = new PopupMenu(getActivity(), view);
                popup.inflate(R.menu.directory_item_menu);

                String isAdmin = User.getCurrentUser(getActivity()).getIsAdmin();
                if (repoType.equals(REPO_TYPE_COMPANY) && !isAdmin.equals("1")) {
                    popup.getMenu().getItem(0).setVisible(false);
                    popup.getMenu().getItem(1).setVisible(false);
                }

                if (mDirectoryItems.get(position).getObjectType().equals(DirectoryItem.TYPE_FOLDER))
                    popup.getMenu().getItem(2).setVisible(false);

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.directory_item_rename:

                                showRenameDirectoryItemDialog(mDirectoryItems.get(position));
                                break;
                            case R.id.directory_item_delete:

                                sendDeleteDirectoryItemRequest(mDirectoryItems.get(position));
                                break;
                            case R.id.directory_item_download:

                                downloadItem(mDirectoryItems.get(position));
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }

            @Override
            public void onItemClicked(View view, int position) {
                DirectoryItem directoryItem = mDirectoryItems.get(position);
                if (directoryItem.getObjectType().equals(DirectoryItem.TYPE_FILE_UPLOADING)) {
                    CommonMethods.displayToast(getContext(), getContext().getString(R.string.upload_in_progress));
                } else {
                    if (directoryItem.getObjectType().equals(DirectoryItem.TYPE_FOLDER)) {
                        currentDirId = directoryItem.getId();

                        fetchDirectoryDetails();

                        Directory directory = new Directory();
                        directory.setId(directoryItem.getId());
                        directory.setName(directoryItem.getName());
                        directory.setParentFid(directoryItem.getFid());

                        mDirectories.add(directory);

                        mPathRecyclerAdapter.notifyItemInserted(mDirectories.size() - 1);

                        mPathLinearLayoutManager.scrollToPosition(mDirectories.size() - 1);

                    } else {
                        try {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Urls.AWS_REPOSITORY_PATH + directoryItem.getPath()));
                            startActivity(browserIntent);
                        } catch (Exception e) {
                            Logger.info(TAG, "No supported application found");
                            downloadItem(directoryItem);
                        }
                    }
                }
            }
        });
    }

    private void downloadItem(DirectoryItem directoryItem) {
        Logger.info(TAG, Urls.AWS_REPOSITORY_PATH + directoryItem.getPath());
        DownloadManager.Request r = new DownloadManager.Request(Uri.parse(Urls.AWS_REPOSITORY_PATH + directoryItem.getPath()));
        r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, directoryItem.getName());
        r.allowScanningByMediaScanner();
        r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        DownloadManager dm = (DownloadManager) getContext().getSystemService(DOWNLOAD_SERVICE);
        dm.enqueue(r);
    }

    private void sendDeleteDirectoryItemRequest(DirectoryItem directoryItem) {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.progress_dialog_layout);
        dialog.setCancelable(true);
        dialog.show();

        try {

            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_REPO_DELETE_FOLDER);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getContext()).getUserId());
            jsonObject.put(Constants.Network.REPO_ID, repoId);
            jsonObject.put(Constants.Network.DATA_ID, directoryItem.getId());
            jsonObject.put(Constants.Network.OBJECT_TYPE, directoryItem.getObjectType());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    fetchDirectoryDetails();
                                    CommonMethods.displayToast(getContext(), getContext().getString(R.string.item_deleted_successfully));
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                            "sendDeleteDirectoryItemRequest", Error.ERROR_TYPE_SERVER_ERROR, "400");

                                    CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                                }
                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                                        "sendDeleteDirectoryItemRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                            }
                            dialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                    "sendDeleteDirectoryItemRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                            dialog.dismiss();
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "sendDeleteDirectoryItemRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
            dialog.dismiss();
        }
    }

    private void showRenameDirectoryItemDialog(final DirectoryItem directoryItem) {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.directory_item_rename_dialog_layout);
        dialog.findViewById(R.id.directory_item_rename_dialog_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newName = ((EditText) dialog.findViewById(R.id.directory_item_rename_dialog_edittext)).getText().toString();
                String OldName = directoryItem.getName();
                if (!newName.trim().isEmpty()) {
                    if (!newName.equalsIgnoreCase(OldName)) {
                        dialog.setContentView(R.layout.progress_dialog_layout);
                        sendRenameDirectoryItemRequest(directoryItem, newName.trim(), dialog);
                    } else {
                        Toast.makeText(getActivity(), "Folder Name Already Exist", Toast.LENGTH_SHORT).show();
                    }

                } else
                    CommonMethods.displayToast(getContext(), getContext().getString(R.string.please_enter_valid_name));
            }
        });
        dialog.findViewById(R.id.directory_item_rename_dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void sendRenameDirectoryItemRequest(DirectoryItem directoryItem, String newName, final Dialog dialog) {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_REPO_RENAME_FOLDER);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getContext()).getUserId());
            jsonObject.put(Constants.Network.REPO_ID, repoId);
            jsonObject.put(Constants.Network.DATA_ID, directoryItem.getId());
            jsonObject.put(Constants.Network.NEW_NAME_OBJ, newName);
            jsonObject.put(Constants.Network.OBJECT_TYPE, directoryItem.getObjectType());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    fetchDirectoryDetails();
                                    CommonMethods.displayToast(getContext(), getContext().getString(R.string.item_renamed_successfully));
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                            "sendRenameDirectoryItemRequest", Error.ERROR_TYPE_SERVER_ERROR, "400");
                                    CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                                }
                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                                        "sendRenameDirectoryItemRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                            }
                            dialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                    "sendRenameDirectoryItemRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                            dialog.dismiss();
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "sendRenameDirectoryItemRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
            dialog.dismiss();
        }
    }

    private void initDirectoryItemsRecyclerView() {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dirRecyclerView.setLayoutManager(mLinearLayoutManager);

        mDirectoryItemRecyclerAdapter = new DirectoryItemRecyclerAdapter(getContext(), mDirectoryItems, repoType);
        dirRecyclerView.setAdapter(mDirectoryItemRecyclerAdapter);
    }

    private void initPathRecyclerView() {
        mPathLinearLayoutManager = new LinearLayoutManager(getContext());
        mPathLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        pathRecyclerView.setLayoutManager(mPathLinearLayoutManager);

        mPathRecyclerAdapter = new PathRecyclerAdapter(getContext(), mDirectories);
        pathRecyclerView.setAdapter(mPathRecyclerAdapter);
    }

    private void initDirectoryItemList() {
        mDirectoryItems = new ArrayList<>();
    }

    private void addProgressBarInDirectoryItemList() {
        DirectoryItem progressDirectoryItem = new DirectoryItem();
        progressDirectoryItem.setObjectType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mDirectoryItems.add(progressDirectoryItem);
    }

    private void initDirectoryList() {
        mDirectories = new ArrayList<>();

        Directory homeDirectory = new Directory();
        homeDirectory.setId(DIRECTORY_HOME_ID);
        homeDirectory.setName(getContext().getString(R.string.home));
        homeDirectory.setParentFid(DIRECTORY_HOME_ID);

        mDirectories.add(homeDirectory);
    }

    private void fetchDirectoryDetails() {

        clearListAndInsertProgressBar();

        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_REPO_DETAILS);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getContext()).getUserId());
            jsonObject.put(Constants.Network.REPO_ID, repoId);
            jsonObject.put(Constants.Network.CURRENT_FOLDER, currentDirId);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    mDirectoryItems.remove(0);
                                    mDirectoryItemRecyclerAdapter.notifyItemRemoved(0);

                                    JSONArray directoryItemsArray = response.getJSONArray(Constants.Network.DATA).getJSONArray(0);

                                    for (int i = 0; i < directoryItemsArray.length(); i++) {

                                        JSONObject directoryItemObject = directoryItemsArray.getJSONObject(i);

                                        DirectoryItem directoryItem = new DirectoryItem();
                                        directoryItem.setObjectType(directoryItemObject.getString(Constants.Network.OBJECT_TYPE));
                                        directoryItem.setId(directoryItemObject.getString(Constants.Network.ID));
                                        directoryItem.setFid(directoryItemObject.getString(Constants.Network.FID));
                                        directoryItem.setName(directoryItemObject.getString(Constants.Network.NAME));
                                        directoryItem.setDescription(directoryItemObject.getString(Constants.Network.DESCRIPTION));
                                        directoryItem.setRepoId(directoryItemObject.getString(Constants.Network.REPO_ID));
                                        directoryItem.setPath(directoryItemObject.getString(Constants.Network.PATH));
                                        directoryItem.setFileType(directoryItemObject.getString(Constants.Network.FILE_TYPE));
                                        directoryItem.setUserId(directoryItemObject.getString(Constants.Network.USER_ID));
                                        directoryItem.setCreatedDate(directoryItemObject.getString(Constants.Network.CREATED_DATE));

                                        mDirectoryItems.add(directoryItem);
                                    }

                                    mDirectoryItemRecyclerAdapter.notifyItemRangeInserted(0, directoryItemsArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    //no files/folders
                                    mDirectoryItems.remove(0);
                                    mDirectoryItemRecyclerAdapter.notifyItemRemoved(0);

                                    showEmptyViewInDirectoryItemList();
                                }
                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                                        "fetchDirectoryDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG,
                                    "fetchDirectoryDetails", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "fetchDirectoryDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void showEmptyViewInDirectoryItemList() {
        DirectoryItem emptyDirectoryItem = new DirectoryItem();
        emptyDirectoryItem.setIcon(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyDirectoryItem.setName(getString(R.string.no_attachments));
        emptyDirectoryItem.setObjectType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));

        mDirectoryItems.add(emptyDirectoryItem);

        dirRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mDirectoryItemRecyclerAdapter.notifyItemInserted(0);
    }


    private void clearListAndInsertProgressBar() {
        int oldSize = mDirectoryItems.size();
        mDirectoryItems.clear();
        mDirectoryItemRecyclerAdapter.notifyItemRangeRemoved(0, oldSize);
        addProgressBarInDirectoryItemList();
        mDirectoryItemRecyclerAdapter.notifyItemInserted(0);
    }

    private void displayErrorUI(Error error) {
//        CommonMethods.hideProgressBar(fragmentRootView, R.id.fragment_repository_root_layout);
        CommonMethods.showErrorView(this, R.id.fragment_repository_root_layout, error);
    }

    private void getDataFromArguments() {
        repoType = getArguments().getString(RepositoryFragment.REPO_TYPE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*super.onActivityResult(requestCode, resultCode, data);*/
        if (requestCode == FilePicker.REQUEST_CODE_CHOOSE_FILE) {
            if (resultCode == RESULT_OK) {

                if (data != null) {
                    Uri uri = data.getData();

                    ArrayList<String> list = FileExtensionUtil.checkForRepo();
                    Logger.debug("activityResult", list.toString());
                    String fileExtension = CommonMethods.getExtensionFile(getActivity(), uri);

                    if (list.contains(fileExtension)) {
                        copyFileToLocalStorage(uri);
                    } else {
                        Toast.makeText(getActivity(), "Invalid File Type", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
                }
            }
        }
    }


    private void copyFileToLocalStorage(final Uri uri) {

        FilePicker.copyFileFromUri(getContext(), uri, new OnFileCopiedListener() {
            @Override
            public void onFileCopied(File file) {

                // check if no files were there previously, then remove the error item
                if (mDirectoryItems.get(0).getObjectType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {
                    mDirectoryItems.remove(0);
                    mDirectoryItemRecyclerAdapter.notifyItemRemoved(0);
                }
                //.setVisibility(View.VISIBLE);
                disableTouch();
                DirectoryItem directoryItem = createDirectoryItem(file);
                startUpload(directoryItem);
            }

            @Override
            public void onFileCopyFailed(Error error) {
//                CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
            }
        });
    }

    private void startUpload(DirectoryItem directoryItem) {

        JSONObject jsonObject = getRepositoryUploadRequest(directoryItem);

        UploadParams uploadParams = new UploadParams(Constants.Network.ACTION_UPLOADED_FILE_DATABASE_ENTRY
                , directoryItem.getId()
                , directoryItem.getName()
                , directoryItem.getPath()
                , Constants.AWS.REPO_KEY
                , jsonObject);

        Intent uploadIntent = new Intent(getContext(), UploadService.class);
        uploadIntent.putExtra(UploadService.UPLOAD_PARAMS, uploadParams);
        getContext().startService(uploadIntent);
    }

    private JSONObject getRepositoryUploadRequest(DirectoryItem directoryItem) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_UPLOADED_FILE_DATABASE_ENTRY);
            jsonObject.put(Constants.Network.USER_ID, directoryItem.getUserId());
            jsonObject.put(Constants.Network.REPO_ID, directoryItem.getRepoId());
            jsonObject.put(Constants.Network.PARENT_FOLDER, directoryItem.getFid());
            jsonObject.put(Constants.Network.FILE_NAME, directoryItem.getName());
            jsonObject.put(Constants.Network.FILE_TYPE, directoryItem.getFileType());
            jsonObject.put(Constants.Network.PATH, directoryItem.getPath());
            Logger.error("repoId", jsonObject.toString());
        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), TAG,
                    "getRepositoryUploadRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.displayToast(getContext(), getContext().getString(R.string.something_went_wrong));
        }
        return jsonObject;
    }

    private DirectoryItem createDirectoryItem(File file) {
        DirectoryItem directoryItem = new DirectoryItem();
        directoryItem.setObjectType(DirectoryItem.TYPE_FILE_UPLOADING);
        directoryItem.setId(CommonMethods.getCurrentDate("yyMMddHHmmss")); //temporary id to keep track of download
        directoryItem.setFid(currentDirId);
        directoryItem.setName(file.getName());
        directoryItem.setDescription("");
        directoryItem.setRepoId(repoId);
        directoryItem.setPath(repoId + "/");
        directoryItem.setFileType(CommonMethods.getFileTypeFromName(file.getName()));
        directoryItem.setUserId(User.getCurrentUser(getContext()).getUserId());
        directoryItem.setCreatedDate(CommonMethods.getCurrentDate("yy:MM:dd HH:mm:ss"));

        mDirectoryItems.add(directoryItem);

        mDirectoryItemRecyclerAdapter.notifyItemInserted(mDirectoryItems.size() - 1);

        return directoryItem;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                FilePicker.chooseFile(this, FilePicker.REQUEST_CODE_CHOOSE_FILE);
            else
                CommonMethods.displayToast(getActivity(), getString(R.string.permissions_denied));
        }
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}
