package com.indiareads.ksp.view.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.CourseChapterPagerAdapter;
import com.indiareads.ksp.model.Chapter;

import java.util.List;

public class ViewCourseActivity extends AppCompatActivity {

    public static final String CURRENT_CHAPTER_POSITION = "Current Chapter Position";
    public static final String CHAPTERS = "Chapters";
    ViewPager viewPager;
    CourseChapterPagerAdapter mCourseChapterPagerAdapter;
    List<Chapter> mChapters;
    int currentPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_course);

        getDataFromIntent();
        setCourseTitle();
        setUpViewPager();
    }

    private void setCourseTitle() {
        String title = mChapters.get(0).getCourseName();
        title = String.valueOf(Html.fromHtml(title));
        ((TextView) findViewById(R.id.activity_course_title)).setText(title);
    }

    private void getDataFromIntent() {
        mChapters = getIntent().getParcelableArrayListExtra(CHAPTERS);
        currentPos = getIntent().getIntExtra(CURRENT_CHAPTER_POSITION, 0);
    }

    private void setUpViewPager() {
        viewPager = findViewById(R.id.activity_course_view_pager);
        mCourseChapterPagerAdapter = new CourseChapterPagerAdapter(this, mChapters, viewPager);
        viewPager.setAdapter(mCourseChapterPagerAdapter);
        viewPager.setCurrentItem(currentPos);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCourseChapterPagerAdapter.stopWebView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCourseChapterPagerAdapter.startWebView(viewPager.getCurrentItem());
    }
}
