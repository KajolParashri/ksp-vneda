package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.MessageActivityRecyclerAdapter;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Conversation;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MessageActivity extends AppCompatActivity {

    private static String TAG = MessageActivity.class.getSimpleName();
    private LinearLayoutManager mLinearLayoutManager;
    private MessageActivityRecyclerAdapter mMessageActivityRecyclerAdapter;
    private RecyclerView mConversationRecyclerView;
    private List<Conversation> mConversations;

    private int pageNumber = 1;
    private boolean doPagination = true;
    private boolean mConversationsLoading = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        setUpToolbar();
        initConversationsList();
        initConversationRecyclerView();
        setRecyclerViewScrollListener();
        setRecyclerViewItemClickListener();
        fetchData();
        setListeners();
    }

    private void setListeners() {
        findViewById(R.id.new_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNewChatActivity(Conversation.CHAT_TYPE_SINGLE);
            }
        });

        findViewById(R.id.create_team).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNewChatActivity(Conversation.CHAT_TYPE_GROUP);
            }
        });
    }

    //startNewChatActivity(Conversation.CHAT_TYPE_GROUP);
    private void startNewChatActivity(String chatType) {
        Intent intent = new Intent(this, NewChatActivity.class);
        intent.putExtra(Conversation.CHAT_TYPE, chatType);
     //   intent.putExtra("intent", "message");
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mConversationsLoading) {
            int oldSize = mConversations.size();
            mConversations.clear();
            mMessageActivityRecyclerAdapter.notifyItemRangeRemoved(0, oldSize);
            addProgressBarInConversationList();
            mMessageActivityRecyclerAdapter.notifyItemInserted(0);
            pageNumber = 1;
            doPagination = true;
            fetchData();
        }
    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(this);
    }

    private void setUpToolbar() {

        Toolbar toolbar = findViewById(R.id.activity_message_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void initConversationsList() {
        mConversations = new ArrayList<>();
        addProgressBarInConversationList();
    }

    private void addProgressBarInConversationList() {

        Conversation progressBarConversation = new Conversation();
        progressBarConversation.setChatType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mConversations.add(progressBarConversation);

    }

    private void initConversationRecyclerView() {

        mConversationRecyclerView = findViewById(R.id.activity_message_recyclerview);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mConversationRecyclerView.setLayoutManager(mLinearLayoutManager);

        mMessageActivityRecyclerAdapter = new MessageActivityRecyclerAdapter(this, mConversations);
        mConversationRecyclerView.setAdapter(mMessageActivityRecyclerAdapter);
    }

    private void setRecyclerViewItemClickListener() {

        mMessageActivityRecyclerAdapter.setOnRecyclerItemClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent chatIntent = new Intent(MessageActivity.this, ChatActivity.class);
                chatIntent.putExtra(Constants.Network.CONVO_ID, mConversations.get(position).getChannelId());
                chatIntent.putExtra(Constants.Network.USER_FULL_NAME, mConversations.get(position).getChatName());
                chatIntent.putExtra("intent", "message");
                startActivity(chatIntent);
            }
        });

    }

    private void setRecyclerViewScrollListener() {
        //Fetching next page's data on reaching bottom

        mConversationRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mLinearLayoutManager.getChildCount();
                    totalItemCount = mLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                    if (mConversationsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mConversationsLoading = false;
                            addProgressBarInConversationList();
                            mMessageActivityRecyclerAdapter.notifyItemInserted(mConversations.size() - 1);
                            pageNumber++;
                            fetchData();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void fetchData() {
        mConversationsLoading = false;
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE, pageNumber);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_CONVOS);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mConversations.remove(mConversations.size() - 1);

                                    mMessageActivityRecyclerAdapter.notifyItemRemoved(mConversations.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Conversation conversation = new Conversation();
                                        conversation.setId(jsonObject1.getString(Constants.Network.ID));
                                        conversation.setChannelId(jsonObject1.getString(Constants.Network.CHANNEL_ID));
                                        conversation.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                        conversation.setJoinDate(jsonObject1.getString(Constants.Network.JOIN_DATE));
                                        conversation.setStatus(jsonObject1.getString(Constants.Network.STATUS));
                                        conversation.setMessageId(jsonObject1.getString(Constants.Network.MESSAGE_ID));
                                        conversation.setMessage(jsonObject1.getString(Constants.Network.MESSAGE));
                                        conversation.setChatType(jsonObject1.getString(Constants.Network.CHAT_TYPE));
                                        conversation.setChatImage(jsonObject1.getString(Constants.Network.CHAT_IMAGE));
                                        conversation.setChatName(jsonObject1.getString(Constants.Network.CHAT_NAME));
                                        conversation.setReadCount(jsonObject1.getString(Constants.Network.READ_COUNT));

                                        mConversations.add(conversation);
                                    }

                                    mMessageActivityRecyclerAdapter.notifyItemRangeInserted(mConversations.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(MessageActivity.this);
                                } else {

                                    //stop call to pagination in any case
                                    doPagination = false;

                                    //to remove progress bar
                                    mConversations.remove(mConversations.size() - 1);
                                    mMessageActivityRecyclerAdapter.notifyItemRemoved(mConversations.size() - 1);

                                    if (pageNumber == 1) {
                                        //show msg no contents
                                        showEmptyViewInConversationList();
                                    } else {
                                        //no more contents
                                    }
                                }

                                mConversationsLoading = true;

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void showEmptyViewInConversationList() {
        Conversation emptyConversation = new Conversation();
        emptyConversation.setChatImage(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyConversation.setChatName(getString(R.string.message_list_empty));
        emptyConversation.setChatType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));

        mConversations.add(emptyConversation);

        mConversationRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMessageActivityRecyclerAdapter.notifyItemInserted(0);
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(this, R.id.activity_message_root_layout, error);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
