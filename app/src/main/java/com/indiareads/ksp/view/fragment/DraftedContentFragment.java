package com.indiareads.ksp.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.DraftedContentFragmentRecyclerAdapter;
import com.indiareads.ksp.listeners.OnDraftedContentItemClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.EditorActivity;
import com.indiareads.ksp.view.activity.SetupCourseActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.indiareads.ksp.utils.Constants.COURSE.TITLE;
import static com.indiareads.ksp.utils.Constants.Network.DATA;

/**
 * A simple {@link Fragment} subclass.
 */
public class DraftedContentFragment extends Fragment {

    private static final String TAG = DraftedContentFragment.class.getSimpleName();

    private View mFragmentRootView; //Root view
    LinearLayoutManager linearLayoutManager;
    //private GridLayoutManager mAutoFitGridLayoutManager;
    private DraftedContentFragmentRecyclerAdapter mContentFragmentRecyclerAdapter;
    private RecyclerView mContentsRecyclerView;

    private List<Content> mContents; //Contents list

    private int pageNumber = 1;

    public DraftedContentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentRootView = inflater.inflate(R.layout.fragment_drafted_content, container, false);

        initViews();
        initDraftedContentList();
        initRecyclerView();
        setContentFragmentClickListener();
        fetchData();

        return mFragmentRootView;
    }

    private void setContentFragmentClickListener() {

        mContentFragmentRecyclerAdapter.setOnClickListener(new OnDraftedContentItemClickListener() {
            @Override
            public void onEditClick(View v, int position) {
                if (mContents.get(position).getContentType().equalsIgnoreCase("1")) {
                    Intent intent = new Intent(getActivity(), EditorActivity.class);
                    intent.putExtra("type", Constants.Content.ARTICLE);
                    intent.putExtra(DATA, mContents.get(position).getContentId());
                    intent.putExtra(TITLE, mContents.get(position).getTitle());
                    intent.putExtra("content_type", "1");
                    startActivity(intent);

                } else if (mContents.get(position).getContentType().equalsIgnoreCase("3")) {
                    Intent intent = new Intent(getActivity(), SetupCourseActivity.class);
                    intent.putExtra("type", Constants.Content.COURSE);
                    intent.putExtra(DATA, mContents.get(position).getContentId());
                    intent.putExtra(TITLE, mContents.get(position).getTitle());
                    intent.putExtra("content_type", "3");
                    startActivity(intent);

                }
            }

            @Override
            public void onDeleteClick(View v, int position) {
                if (position < mContents.size()) {
                    deleteData(mContents.get(position).getContentId(), position);
                }
            }

            @Override
            public void onItemClick(View v, int position) {

            }

            @Override
            public void onLikeCOunt(View v, int position) {

            }

        });
    }

    private void initDraftedContentList() {
        mContents = new ArrayList<>();
        addProgressBarInList();
    }

    private void initViews() {
        mContentsRecyclerView = mFragmentRootView.findViewById(R.id.drafted_content_fragment_recycler_view);
    }

    private void fetchData() {
        try {

            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getActivity()).getUserId());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_DRAFTED_CONTENTS);

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.HOMEPAGE_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    if (mContents.size() > 0) {
                                        mContents.remove(mContents.size() - 1);
                                        mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                                    }

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        User user = new User();
                                        user.setUserId(jsonObject1.getString(Constants.Network.USER_ID));

                                        Content content = new Content();
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                        content.setCreatedDate(jsonObject1.getString(Constants.Network.CREATED_DATE));
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                        content.setThumbnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));
                                        content.setContentType(jsonObject1.getString(Constants.Network.CONTENT_TYPE));
                                        content.setStatusType(jsonObject1.getString(Constants.Network.STATUS));
                                        content.setUser(user);

                                        mContents.add(content);
                                    }

                                    mContentFragmentRecyclerAdapter.notifyItemRangeInserted(mContents.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    //stop call to pagination in any case
                                    //to remove progress bar
                                    if (mContents.size() > 0) {
                                        mContents.remove(mContents.size() - 1);
                                        mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                                    }

                                    if (pageNumber == 1) {
                                        //show msg no contents
                                        showEmptyView(R.mipmap.ic_fevicon, getString(R.string.content_list_empty));
                                    } else {
                                        //no more contents
                                    }
                                }

                            } catch (JSONException e) {
                                if (mContents.size() > 0) {
                                    mContents.remove(mContents.size() - 1);
                                    mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                                }
                                Logger.error(TAG, "In OnResponse" + e.getMessage());
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            //to remove progress bar
                            if (mContents.size() > 0) {
                                mContents.remove(mContents.size() - 1);
                                mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                            }

                            if (error instanceof NetworkError)
                                showEmptyView(R.drawable.ic_signal_wifi_off, getString(R.string.connection_error_message));
                            else
                                showEmptyView(R.drawable.ic_error_outline, getString(R.string.something_went_wrong));

                            Logger.error(TAG, error.getMessage() + " is the volley error ");
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {

            if (mContents.size() > 0) {
                mContents.remove(mContents.size() - 1);
                mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
            }
            Logger.error(TAG, "In fetchData" + e.getMessage());
            e.printStackTrace();
        }
    }


    private void deleteData(String content_id, final int pos) {
        try {

            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.CONTENT_ID, content_id);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.DELETE_CONTENT);

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    if (mContents.size() > 0) {
                                        mContents.remove(pos);
                                        mContentFragmentRecyclerAdapter.notifyItemRemoved(pos);

                                    }
                                    Toast.makeText(getActivity(), "Draft Successfully Deleted", Toast.LENGTH_SHORT).show();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {

                                }


                            } catch (JSONException e) {

                                Logger.error(TAG, "In OnResponse" + e.getMessage());
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            //to remove progress bar

                            if (error instanceof NetworkError)
                                showEmptyView(R.drawable.ic_signal_wifi_off, getString(R.string.connection_error_message));
                            else
                                showEmptyView(R.drawable.ic_error_outline, getString(R.string.something_went_wrong));

                            Logger.error(TAG, error.getMessage() + " is the volley error ");
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Logger.error(TAG, "In fetchData" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showEmptyView(int imageId, String msg) {
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(imageId));
        emptyContent.setTitle(msg);
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mContents.add(emptyContent);

        mContentsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mContentFragmentRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mContents.add(progressBarContent);
    }

    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mContentsRecyclerView.setLayoutManager(linearLayoutManager);

        mContentFragmentRecyclerAdapter = new DraftedContentFragmentRecyclerAdapter(getActivity(), mContents);
        mContentsRecyclerView.setAdapter(mContentFragmentRecyclerAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}
