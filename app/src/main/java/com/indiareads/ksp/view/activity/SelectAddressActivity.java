package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.AddressListAdapter;
import com.indiareads.ksp.listeners.OnAddressClickListener;
import com.indiareads.ksp.model.Address;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.LibraryBook;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SharedPreferencesHelper;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;
import static com.indiareads.ksp.utils.CommonMethods.getStateList;
import static com.indiareads.ksp.utils.Constants.PLACE_ORDER_SELECT_ADDRESS;
import static com.indiareads.ksp.utils.Constants.RETURN_ORDER_SELECT_ADDRESS;


public class SelectAddressActivity extends AppCompatActivity implements OnAddressClickListener {

    private static final String TAG = SelectAddressActivity.class.getSimpleName();
    private String mIsbnNo;
    private String mContentId;
    private String TYPE;

    private boolean UIRV = true;

    private Toolbar mToolbar;
    private ProgressBar mProgress;

    private LinearLayout mAddNewAddressLayout;
    private CheckBox mHomeAddress, mWorkAddress;
    private EditText mFullName, mAddressLine1, mAddressLine2, city, pincode, phone;
    private Button mDeliverHere;
    private Spinner mStateSpinner;


    private RecyclerView mAddressRecyclerView;
    private AddressListAdapter mAdapter;
    private Button mAddNewAddressBtn;

    private List<Address> mAddressList;
    String intent = "";
    String orderId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_book);
        getIntentData();
        initViews();
        setOnClickListeners();
        initRecyclerView();
        getExistingAddress();
    }

    private void getIntentData() {
        if (getIntent().hasExtra("intent")) {
            intent = getIntent().getStringExtra("intent");
        }
        if (getIntent().hasExtra("orderId")) {
            orderId = getIntent().getStringExtra("orderId");
        }
        mIsbnNo = getIntent().getStringExtra(LibraryBook.ISBN);
        mContentId = getIntent().getStringExtra(LibraryBook.CONTENT_ID);
        TYPE = getIntent().getStringExtra(LibraryBook.TYPE);
    }

    private void initViews() {
        mToolbar = findViewById(R.id.activity_order_toolbar);
        mToolbar.setTitle("Select delivery address");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        mProgress = findViewById(R.id.activity_order_book_progress);
        mAddressRecyclerView = findViewById(R.id.addressRecyclerView);
        mAddNewAddressLayout = findViewById(R.id.add_new_address_layout);
        mFullName = findViewById(R.id.address_full_name);
        mAddressLine1 = findViewById(R.id.address_line1);
        mAddressLine2 = findViewById(R.id.address_line2);
        city = findViewById(R.id.address_city);
        pincode = findViewById(R.id.address_pincode);
        phone = findViewById(R.id.address_phone);
        mHomeAddress = findViewById(R.id.check_home_address);
        mWorkAddress = findViewById(R.id.check_work_address);
        mAddNewAddressBtn = findViewById(R.id.btn_add_new_address);
        mDeliverHere = findViewById(R.id.btn_deliver_here);

        mStateSpinner = findViewById(R.id.address_state);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getStateList());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mStateSpinner.setAdapter(dataAdapter);
        mWorkAddress.setChecked(true);

    }


    private void showEmptyView(int imageId, String msg) {
        Address emptyContent = new Address();
        emptyContent.setCity(String.valueOf(imageId));
        emptyContent.setFullname(msg);
        emptyContent.setType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mAddressList.add(emptyContent);

        mAddressRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter.notifyItemInserted(0);
    }

    private void setOnClickListeners() {

        mAddNewAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIRV = false;
                displayNewAddressUI();
            }
        });


        User user = User.getCurrentUser(this);

        String featureId = user.getCompanyFeaturesFeatureId();
        String featureValue = user.getCompanyFeaturesValue();
        Logger.error("CHECKFeature", featureId + " , " + featureValue);
        if (featureId.equalsIgnoreCase("4") && featureValue.equalsIgnoreCase("1")) {
            mAddNewAddressBtn.setVisibility(View.GONE);
        } else {
            mAddNewAddressBtn.setVisibility(View.VISIBLE);

        }


        mDeliverHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String numBer = phone.getText().toString();
                if (mFullName.getText().toString().isEmpty() || mAddressLine1.getText().toString().isEmpty()
                        || city.getText().toString().isEmpty() || pincode.getText().toString().isEmpty() || phone.getText().toString().isEmpty()) {
                    displayToast(SelectAddressActivity.this, "Please enter the required fields");
                } else if (mStateSpinner.getSelectedItem().toString().equals("Select State")) {
                    displayToast(SelectAddressActivity.this, "Please select State");
                } else if (isValidMobile(numBer)) {
                    checkPincode();
                }
            }
        });

        mHomeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mWorkAddress.isChecked())
                    mWorkAddress.setChecked(false);
                mHomeAddress.setChecked(true);
            }
        });
        mWorkAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mHomeAddress.isChecked())
                    mHomeAddress.setChecked(false);
                mWorkAddress.setChecked(true);
            }
        });
    }

    private boolean isValidMobile(String phoneNumBer) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phoneNumBer)) {
            if (phoneNumBer.length() < 6 || phoneNumBer.length() > 13) {
                // if(phoneNumBer.length() != 10) {
                check = false;
                phone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    private void initRecyclerView() {
        mAddressList = new ArrayList<>();
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mAddressRecyclerView.setLayoutManager(mLinearLayoutManager);
    }

    private void getExistingAddress() {
        mAddressList = new ArrayList<>();
        CommonMethods.showProgressBar(this, R.id.activity_order_book_root_layout);
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, "getdeliveryaddress");
            jsonObject.put("user_id", SharedPreferencesHelper.getCurrentUser(this).getUserId());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.GET_EXISTING_ADDRESS,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONArray jsonArray;
                                    jsonArray = response.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1;
                                        jsonObject1 = jsonArray.getJSONObject(i);
                                        Address address = new Address();
                                        address.setAddressBookId(jsonObject1.getString("address_book_id"));
                                        address.setFullname(jsonObject1.getString("fullname"));
                                        address.setAddressLine1(jsonObject1.getString("address_line1"));
                                        address.setAddressLine2(jsonObject1.getString("address_line2"));
                                        address.setCity(jsonObject1.getString("city"));
                                        address.setState(jsonObject1.getString("state"));
                                        address.setPincode(jsonObject1.getString("pincode"));
                                        address.setPhone(jsonObject1.getString("phone"));
                                        //address.setType(jsonObject1.getString("address_type"));
                                        mAddressList.add(address);
                                    }

                                    mAdapter = new AddressListAdapter(mAddressList, SelectAddressActivity.this);
                                    mAddressRecyclerView.setAdapter(mAdapter);
                                } else {
                                    Error error = Error.generateError(HttpResponse.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                                            "getExistingAddress", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));
                                    displayErrorUI(error);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                                        "getExistingAddress", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                displayErrorUI(error);
                            }
                            CommonMethods.hideProgressBar(SelectAddressActivity.this, R.id.activity_order_book_root_layout);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                                    "getExistingAddress", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(SelectAddressActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                    "getExistingAddress", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(SelectAddressActivity.this, R.id.activity_order_book_root_layout);
            displayErrorUI(error);
        }
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(SelectAddressActivity.this, R.id.activity_order_book_root_layout, error);
    }

    private void displayNewAddressUI() {
        clearTextViews();
        getSupportActionBar().setTitle("Add new address");
        mAddNewAddressLayout.setVisibility(View.VISIBLE);
        mAddressRecyclerView.setVisibility(View.GONE);
        mAddNewAddressBtn.setVisibility(View.GONE);
    }

    private void hideNewAddressUI() {
        getSupportActionBar().setTitle("Select delivery address");
        mAddNewAddressLayout.setVisibility(View.GONE);
        mAddressRecyclerView.setVisibility(View.VISIBLE);
        mAddNewAddressBtn.setVisibility(View.VISIBLE);
    }

    private void checkPincode() {
        mAddNewAddressLayout.setClickable(false);
        mProgress.setVisibility(View.VISIBLE);
        mDeliverHere.setVisibility(View.GONE);
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, "checkpincode");
            jsonObject.put("pincode", pincode.getText().toString());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.ORDER_PLACE,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                switch (response.getString(Constants.Network.RESPONSE_CODE)) {
                                    case Constants.Network.RESPONSE_OK:
                                        //Adding this address
                                        addNewAddress();
                                        break;
                                    case Constants.Network.RESPONSE_ERROR:
                                        mAddNewAddressLayout.setClickable(true);
                                        mProgress.setVisibility(View.GONE);
                                        mDeliverHere.setVisibility(View.VISIBLE);
                                        displayToast(SelectAddressActivity.this, "Service is not available at this pincode");
                                        break;
                                    case Constants.Network.RESPONSE_UNAUTHORISED:
                                        SideBarPanel.logout(SelectAddressActivity.this);
                                        break;
                                }

                            } catch (JSONException e) {
                                mAddNewAddressLayout.setClickable(true);
                                Error error = Error.generateError(JSONException.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                                        "checkPincode", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                mProgress.setVisibility(View.GONE);
                                mDeliverHere.setVisibility(View.VISIBLE);
                                displayToast(SelectAddressActivity.this, "Error contacting servers");
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            mAddNewAddressLayout.setClickable(true);
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                                    "checkPincode", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                            mProgress.setVisibility(View.GONE);
                            mDeliverHere.setVisibility(View.VISIBLE);
                            displayToast(SelectAddressActivity.this, "Error contacting servers");
                        }
                    });

            VolleySingleton.getInstance(SelectAddressActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            mAddNewAddressLayout.setClickable(true);
            Error error = Error.generateError(JSONException.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                    "checkPincode", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            mProgress.setVisibility(View.GONE);
            mDeliverHere.setVisibility(View.VISIBLE);
            displayToast(SelectAddressActivity.this, "Error contacting servers");
        }
    }

    private void addNewAddress() {
        JSONObject addressJson = new JSONObject();
        try {
            addressJson.put("action", "addnewaddress");
            addressJson.put("addline1", mAddressLine1.getText().toString());
            addressJson.put("addline2", mAddressLine2.getText().toString());

            if (mHomeAddress.isChecked()) {
                addressJson.put("address_type", "2");
            } else {
                addressJson.put("address_type", "1");
            }

            addressJson.put("fullname", mFullName.getText().toString());
            addressJson.put("city_modal", city.getText().toString());
            addressJson.put("phone_modal", phone.getText().toString());
            addressJson.put("pincode_modal", pincode.getText().toString());
            addressJson.put("state_modal", mStateSpinner.getSelectedItem().toString());
            addressJson.put("user_id", SharedPreferencesHelper.getCurrentUser(SelectAddressActivity.this).getUserId());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest addressRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.GET_EXISTING_ADDRESS,
                addressJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                        mAddNewAddressLayout.setClickable(true);
                        mProgress.setVisibility(View.GONE);
                        mDeliverHere.setVisibility(View.VISIBLE);
                        hideNewAddressUI();

                        getExistingAddress();

                    } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                        SideBarPanel.logout(SelectAddressActivity.this);
                    } else {
                        mAddNewAddressLayout.setClickable(true);
                        mProgress.setVisibility(View.GONE);
                        mDeliverHere.setVisibility(View.VISIBLE);
                        displayToast(SelectAddressActivity.this, "Error contacting servers");
                    }
                } catch (JSONException e) {
                    Error error = Error.generateError(JSONException.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                            "checkPincode", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    mAddNewAddressLayout.setClickable(true);
                    mProgress.setVisibility(View.GONE);
                    mDeliverHere.setVisibility(View.VISIBLE);
                    displayToast(SelectAddressActivity.this, "Error contacting servers");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Error error1 = Error.generateError(VolleyError.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                        "checkPincode", Error.getErrorTypeFromVolleyError(error), error.getMessage());
                mAddNewAddressLayout.setClickable(true);
                mProgress.setVisibility(View.GONE);
                mDeliverHere.setVisibility(View.VISIBLE);
                displayToast(SelectAddressActivity.this, "Error contacting servers");
            }
        });
        VolleySingleton.getInstance(SelectAddressActivity.this).addToRequestQueue(addressRequest);
    }

    private void clearTextViews() {
        mFullName.setText("");
        mAddressLine1.setText("");
        mAddressLine2.setText("");
        city.setText("");
        pincode.setText("");
        phone.setText("");
    }

    @Override
    public void onBackPressed() {
        if (UIRV)
            finish();
        else {
            UIRV = true;
            hideNewAddressUI();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (UIRV)
                    finish();
                else {
                    UIRV = true;
                    hideNewAddressUI();
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnAddressClick(int position) {
        if (intent.equalsIgnoreCase("")) {
            Address address = mAddressList.get(position);
            mProgress.setVisibility(View.VISIBLE);
            mAddressRecyclerView.setClickable(false);

            if (TYPE.equals(PLACE_ORDER_SELECT_ADDRESS)) {
                placeOrder(address);
            } else if (TYPE.equals(RETURN_ORDER_SELECT_ADDRESS)) {
                returnOrder(address.getAddressBookId());
            }
        } else {

        }
    }

    @Override
    public void OnAddressRemoveClick(int position) {
        Address address = mAddressList.get(position);
        mProgress.setVisibility(View.VISIBLE);

        mAddressRecyclerView.setClickable(false);

        removeAddress(address, position);

    }

    private void placeOrder(final Address address) {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("action", "orderplace");
            jsonObject.put("address_book_id", address.addressBookId);
            jsonObject.put("isbn", mIsbnNo);
            jsonObject.put("content_id", mContentId);
            jsonObject.put("user_id", SharedPreferencesHelper.getCurrentUser(this).getUserId());

            Logger.info("SelectId", jsonObject.toString() + " is json");

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.ORDER_PLACE,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Logger.info("SelectId", response.toString() + " is response");
                    JSONObject jsonObject1 = response;
                    try {

                        if (jsonObject1.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            displayToast(SelectAddressActivity.this, "Order Success !");
                            Intent intent = new Intent(SelectAddressActivity.this, PlaceOrderActivity.class);
                            intent.putExtra("content_id", mContentId + "");
                            intent.putExtra("data", jsonObject1.getString("data"));
                            intent.putExtra("address_book_id", address.addressBookId);
                            intent.putExtra("messageLay", "newOrder");
                            startActivity(intent);
                            finish();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(SelectAddressActivity.this);
                        } else if (jsonObject1.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_ERROR)) {
                            displayToast(SelectAddressActivity.this, "Please return your prevoius books to place new order");
                            mProgress.setVisibility(View.GONE);
                            mAddressRecyclerView.setClickable(true);

                            Intent userProfileIntent = new Intent(SelectAddressActivity.this, UserDetailsActivity.class);
                            userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(SelectAddressActivity.this).getUserId());
                            userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(SelectAddressActivity.this).getCompanyId());
                            userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 4);
                            startActivity(userProfileIntent);
                        }
                    } catch (
                            JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.info("SelectId", error.toString() + " is json");
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(SelectAddressActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (
                JSONException e) {
            e.printStackTrace();
        }

    }

    private void removeAddress(final Address address, final int mOrderPosition) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("action", "deleteaddress");
            jsonObject.put("address_book_id", address.addressBookId);

            Logger.info("SelectId", jsonObject.toString() + " is json");

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.USER,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Logger.info("SelectId", response.toString() + " is response");
                    JSONObject jsonObject1 = response;
                    try {
                        if (jsonObject1.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                            displayToast(SelectAddressActivity.this, "Address Deleted !");

                            mProgress.setVisibility(View.GONE);
                            mAddressRecyclerView.setClickable(true);
                            if (mAddressList.size() > 0) {
                                mAddressList.remove(mOrderPosition);
                            }
                            mAdapter.hideProgress();
                            mAdapter.notifyItemRemoved(mOrderPosition);

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(SelectAddressActivity.this);
                        } else if (jsonObject1.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_ERROR)) {
                            displayToast(SelectAddressActivity.this, "Clear out previous orders !!");
                            mProgress.setVisibility(View.GONE);
                            mAddressRecyclerView.setClickable(true);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgress.setVisibility(View.GONE);
                        mAddressRecyclerView.setClickable(true);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.info("SelectId", error.toString() + " is json");
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(SelectAddressActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
            mProgress.setVisibility(View.GONE);
            mAddressRecyclerView.setClickable(true);
        }
    }


    private void returnOrder(final String addressBookId) {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("action", "pickuprequest");
            jsonObject.put("user_id", User.getCurrentUser(this).getUserId());
            jsonObject.put("address_book_id", addressBookId);
            jsonObject.put("order_id", orderId);
            Logger.info("AddressReturn", jsonObject.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.ORDER_PLACE,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.info("AddressReturn", response.toString());
                        JSONObject jsonObject1 = response;
                        if (response.getString(Constants.Network.RESPONSE_CODE)
                                .equals(Constants.Network.RESPONSE_OK)) {
                            displayToast(SelectAddressActivity.this, "Pickup Request Received");

                            Intent intent = new Intent(SelectAddressActivity.this, PlaceOrderActivity.class);
                            intent.putExtra("content_id", mContentId + "");
                            intent.putExtra("data", orderId);
                            intent.putExtra("address_book_id", addressBookId);
                            intent.putExtra("messageLay", "return");
                            startActivity(intent);
                            finish();
                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(SelectAddressActivity.this);
                        } else {
                            mProgress.setVisibility(View.GONE);
                            Logger.info("AddressReturn", response.toString());
                            //displayToast(SelectAddressActivity.this, "Unexpected error occurred, Please try again later.");
                            // adapter.notifyItemChanged(position);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        mProgress.setVisibility(View.GONE);
                        displayToast(SelectAddressActivity.this, "Unexpected error occurred, Please try again later.");
                        //adapter.notifyItemChanged(position);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.info("AddressReturn", error.toString());
                    displayToast(SelectAddressActivity.this, "Unexpected error occurred, Please try again later.");
                    //adapter.notifyItemChanged(position);
                }
            });

            jsonObjectRequest.setTag(TAG);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(SelectAddressActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
            displayToast(SelectAddressActivity.this, "Unexpected error occurred, Please try again later.");
        }

    }

}

