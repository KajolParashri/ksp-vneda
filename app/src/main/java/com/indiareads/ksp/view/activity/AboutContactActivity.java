package com.indiareads.ksp.view.activity;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.indiareads.ksp.R;
import com.indiareads.ksp.utils.Logger;

public class AboutContactActivity extends AppCompatActivity {
    private static final String TAG = AboutContactActivity.class.getSimpleName();
    WebView webView;
    ProgressBar progress_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_contact);

        progress_bar = findViewById(R.id.progress_bar);
        String URL = getIntent().getStringExtra("URL");
        webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Logger.info(TAG, "shouldInterceptRequest:" + request.getUrl().toString());
                }
                return super.shouldInterceptRequest(view, request);
            }

            @Override
            public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
                // Do something with the event here
                Logger.info(TAG, "shouldOverrideKeyEvent:" + event.toString());
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Logger.info(TAG, "shouldOverrideUrlLoading:" + url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progress_bar.setVisibility(View.VISIBLE);
            }

            public void onPageFinished(WebView view, String url) {
                progress_bar.setVisibility(View.GONE);
            }
        });
        webView.loadUrl(URL);
    }
}
