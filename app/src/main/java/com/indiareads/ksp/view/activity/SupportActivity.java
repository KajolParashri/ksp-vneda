package com.indiareads.ksp.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.MyCompanyFragment;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class SupportActivity extends AppCompatActivity {
    ProgressBar progress_bar;
    ImageView backButton;
    TextView submitRequestBtn;
    EditText userName, userContact, userEmail, userMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        init();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        submitRequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!userEmail.getText().toString().equals("")) {
                    if (!userMessage.getText().toString().equals("")) {
                        String name = userName.getText().toString();
                        String email = userEmail.getText().toString();
                        String contact = userContact.getText().toString();
                        String msg = userMessage.getText().toString();
                        progress_bar.setVisibility(View.VISIBLE);
                        submitSupportRequeset(name, email, contact, msg);
                    } else {
                        Toast.makeText(SupportActivity.this, "Please Enter Message", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SupportActivity.this, "Please Enter Email Address", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void init() {
        backButton = findViewById(R.id.backButton);
        progress_bar = findViewById(R.id.progressBar);
        progress_bar.setVisibility(View.GONE);
        userName = findViewById(R.id.userName);
        userContact = findViewById(R.id.userContact);
        userEmail = findViewById(R.id.userEmail);
        userMessage = findViewById(R.id.userMessage);
        submitRequestBtn = findViewById(R.id.submitRequest);

    }

    private void submitSupportRequeset(String mUserName, String mEmail, String contact, String msg) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.SUPPORTACTION);
            jsonObject.put(Constants.Network.NAME1, mUserName);
            jsonObject.put(Constants.Network.EMAIL, mEmail);
            jsonObject.put(Constants.Network.CONTACT1, contact);
            jsonObject.put(Constants.Network.MESSAGE1, msg);

            Logger.info("Feedback", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST, Urls.FEEDBACK_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progress_bar.setVisibility(View.GONE);
                    Logger.info("Feedback", response.toString());

                    try {
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            finish();
                            Toast.makeText(SupportActivity.this, "Thanks , We have received your request. We will get back to you shortly", Toast.LENGTH_SHORT).show();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(SupportActivity.this);
                        } else {
                            progress_bar.setVisibility(View.GONE);
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                                    "getCompanyDetails", Error.ERROR_TYPE_SERVER_ERROR, Constants.Network.RESPONSE_MESSAGE);

                            displayErrorUI(error);
                        }
                    } catch (JSONException e) {
                        progress_bar.setVisibility(View.GONE);
                        Error error = Error.generateError(JSONException.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                                "getCompanyDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                        displayErrorUI(error);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progress_bar.setVisibility(View.GONE);
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                            "getCompanyDetails", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                    displayErrorUI(error);
                }
            });
            jsonObjectRequest.setTag("Feedback");
            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            progress_bar.setVisibility(View.GONE);
            Error error = Error.generateError(JSONException.class.getSimpleName(), MyCompanyFragment.class.getSimpleName(),
                    "getCompanyDetails", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayErrorUI(error);
        }
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(SupportActivity.this, R.id.frame_layout, error);
    }
}
