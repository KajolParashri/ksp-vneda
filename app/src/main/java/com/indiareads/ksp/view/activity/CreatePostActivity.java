package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.MyProgresbar;
import com.indiareads.ksp.utils.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;

public class CreatePostActivity extends AppCompatActivity {
    ImageView backButton;
    String type;
    TextView postOrAskButton;
    MyProgresbar myProgresbar;
    EditText postEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        Intent content = getIntent();
        if (content.hasExtra("type")) {
            type = content.getStringExtra("type");
        }
        initView();
    }


    public void showProgressBar() {
        if (myProgresbar == null)
            myProgresbar = MyProgresbar.show(this, R.style.Theme_MyprogressDialog_yellow);
    }

    public void hideProgressBar() {
        if (myProgresbar != null) {
            myProgresbar.dismiss();
            myProgresbar = null;
        }
    }

    public void initView() {
        backButton = findViewById(R.id.backButton);

        postEdit = findViewById(R.id.postEdit);

        postOrAskButton = findViewById(R.id.postOrAskButton);

        postOrAskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String postContent = postEdit.getText().toString();

                if (postContent.trim().equals("")) {

                    Toast.makeText(CreatePostActivity.this, "Please Enter Content", Toast.LENGTH_SHORT).show();

                } else {

                    postContent = postContent.replace("'", "''");
                    postContent = String.valueOf(Html.fromHtml(postContent));

                    showProgressBar();

                    createPost(postContent);
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void createPost(String enteredData) {
        try {
            JSONObject jsonObject = new JSONObject();
            //Creating credentials jsonObject
            jsonObject.put(Constants.Create.POST_TEXT, enteredData);
            jsonObject.put(Constants.Create.ACTION, Constants.Create.SUBMIT_POST);
            jsonObject.put(Constants.Create.USER_ID, User.getCurrentUser(CreatePostActivity.this).getUserId());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.postApi,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                hideProgressBar();
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
//                                    JSONObject data = response.getJSONArray(Constants.Network.DATA).getJSONObject(0);
                                    String mes = response.getString("response_message");
                                    if (mes.equals("success")) {

                                        Toast.makeText(CreatePostActivity.this, "Created Successfully", Toast.LENGTH_SHORT).show();
                                        finish();

                                    } else {
                                        Toast.makeText(CreatePostActivity.this, "" + mes, Toast.LENGTH_SHORT).show();
                                    }

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(CreatePostActivity.this);
                                } else {


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideProgressBar();
                                displayToast(CreatePostActivity.this, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                                //                          CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Error occurred
                            hideProgressBar();
                            displayToast(CreatePostActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);

                            //                    CommonMethods.hideProgressBar(LoginActivity.this, R.id.activity_home_root);
                        }
                    });

            VolleySingleton.getInstance(CreatePostActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            hideProgressBar();
            displayToast(CreatePostActivity.this, Constants.Network.SERVER_ERROR_MESSAGE);
        }
    }
}
