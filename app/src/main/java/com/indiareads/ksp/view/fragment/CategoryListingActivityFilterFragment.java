package com.indiareads.ksp.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.FilterFragmentChildRecyclerAdapter;
import com.indiareads.ksp.adapters.FilterFragmentContentRecyclerAdapter;
import com.indiareads.ksp.listeners.OnFilterAppliedListener;
import com.indiareads.ksp.listeners.OnFilterCategoryClickListener;
import com.indiareads.ksp.model.ChildCategory;
import com.indiareads.ksp.model.ContentType;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryListingActivityFilterFragment extends Fragment {

    public static final String FILTER_MY_COMPANY = "Filter My Company";
    public static final String FILTER_MY_COMPANY_VALUE = "0";
    public static final String FILTER_CATEGORIES = "Filter Categories";
    public static final String FILTER_CONTENT_TYPES = "Filter Content Types";
    private static final String TAG = CategoryListingActivityFilterFragment.class.getSimpleName();

    private View mFragmentRootView;

    ImageView closeButton;
    TextView clearButton;
    TextView cancelButton;
    TextView applyButton;

    private RecyclerView subCategoryRecyclerView;
    private RecyclerView contentTypeRecyclerView;

    private FilterFragmentChildRecyclerAdapter subCategoryRecyclerAdapter;
    private FilterFragmentContentRecyclerAdapter contentTypeRecyclerAdapter;

    private List<ChildCategory> childCategories;
    private List<ContentType> contentTypes;

    private ArrayList<String> filterMyCompany;
    private ArrayList<String> filterCategories;
    private ArrayList<String> filterContentTypes;

    private OnFilterAppliedListener onFilterAppliedListener;

    public CategoryListingActivityFilterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentRootView = inflater.inflate(R.layout.fragment_category_listing_activity_filter, container, false);

        initViews();
        setClickListeners();
        getFilters();
        getSubCategories();
        getContentTypes();

        return mFragmentRootView;
    }

    private void getFilters() {

        filterMyCompany = getArguments().getStringArrayList(FILTER_MY_COMPANY);
        filterCategories = getArguments().getStringArrayList(FILTER_CATEGORIES);
        filterContentTypes = getArguments().getStringArrayList(FILTER_CONTENT_TYPES);

        setMyCompanyFilter();
    }

    private void setMyCompanyFilter() {
        AppCompatCheckBox checkBox = mFragmentRootView.findViewById(R.id.fragment_filter_mycompany_checkbox);

        if (filterMyCompany.size() > 0)
            checkBox.setChecked(true);
    }

    private void setContentTypeRecyclerViewClickListener() {
        contentTypeRecyclerAdapter.setOnClickListener(new OnFilterCategoryClickListener() {
            @Override
            public void onParentCategoryClicked(View view, int position) {
                // do nothing
            }

            @Override
            public void onChildCategoryClicked(View view, int parentCategoryposition, int childCategoryposition) {
                AppCompatCheckBox checkBox = view.findViewById(R.id.fragment_filter_child_category_checkbox);

                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);

                    filterContentTypes.remove(contentTypes.get(childCategoryposition).getContentTypeId());
                } else {
                    checkBox.setChecked(true);

                    filterContentTypes.add(contentTypes.get(childCategoryposition).getContentTypeId());
                }
            }
        });
    }

    private void setSubCategoryRecyclerViewClickListener() {

        subCategoryRecyclerAdapter.setOnClickListener(new OnFilterCategoryClickListener() {
            @Override
            public void onParentCategoryClicked(View view, int position) {

                // do nothing
            }

            @Override
            public void onChildCategoryClicked(View view, int parentCategoryposition, int childCategoryposition) {
                AppCompatCheckBox checkBox = view.findViewById(R.id.fragment_filter_child_category_checkbox);

                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);

                    filterCategories.remove(childCategories.get(childCategoryposition).getCategoryId());
                } else {
                    checkBox.setChecked(true);

                    filterCategories.add(childCategories.get(childCategoryposition).getCategoryId());
                }
            }
        });
    }

    private void getSubCategories() {
        try {
            CommonMethods.showProgressBar(mFragmentRootView, R.id.activity_category_listing_fragment_filter_root_layout);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_CHILD);
            jsonObject.put(Constants.Network.PARENT_CAT_ID, getActivity().getIntent().getStringExtra(Constants.Category.CATEGORY_ID));

            Logger.debug(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.debug(TAG, response.toString());

                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            childCategories = new ArrayList<>();

                            JSONArray childArray = response.getJSONArray(Constants.Network.DATA);

                            for (int j = 0; j < childArray.length(); j++) {
                                JSONObject childObject = childArray.getJSONObject(j);

                                ChildCategory childCategory = new ChildCategory();
                                childCategory.setCategoryId(childObject.getString(Constants.Network.ID));
                                childCategory.setCategoryName(childObject.getString(Constants.Network.NAME));

                                childCategories.add(childCategory);
                            }

                            setCategoriesLayout();

                            CommonMethods.hideProgressBar(mFragmentRootView, R.id.activity_category_listing_fragment_filter_root_layout);

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG, "getSubCategories", Error.ERROR_TYPE_SERVER_ERROR, "400");
                            showErrorUI(error);
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), TAG, "getSubCategories", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        showErrorUI(error);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError e) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG, "getSubCategories", Error.getErrorTypeFromVolleyError(e), e.getMessage());
                    showErrorUI(error);
                }
            });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG, "getSubCategories", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            showErrorUI(error);
        }
    }

    private void getContentTypes() {
        contentTypes = ContentType.getContentTypeList();
        setContentLayout();
    }

    private void setContentLayout() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        contentTypeRecyclerView.setLayoutManager(linearLayoutManager);
        contentTypeRecyclerAdapter = new FilterFragmentContentRecyclerAdapter(getActivity(), contentTypes, 1, filterContentTypes);
        contentTypeRecyclerView.setAdapter(contentTypeRecyclerAdapter);
        setContentTypeRecyclerViewClickListener();

    }

    private void setCategoriesLayout() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        subCategoryRecyclerView.setLayoutManager(linearLayoutManager);
        subCategoryRecyclerAdapter = new FilterFragmentChildRecyclerAdapter(getActivity(), childCategories, 0, filterCategories);
        subCategoryRecyclerView.setAdapter(subCategoryRecyclerAdapter);
        setSubCategoryRecyclerViewClickListener();
    }

    private void showErrorUI(Error error) {
        CommonMethods.hideProgressBar(mFragmentRootView, R.id.activity_category_listing_fragment_filter_root_layout);
        CommonMethods.showErrorView(CategoryListingActivityFilterFragment.this, R.id.activity_category_listing_fragment_filter_root_layout, error);
    }

    private void setClickListeners() {
        setMyCompanyClickListener();
        setCloseClickListener();
        setCancelClickListener();
        setClearClickListener();
        setApplyClickListener();
    }

    private void setCloseClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishFragment();
            }
        });
    }

    private void setCancelClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishFragment();
            }
        });
    }

    private void setClearClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterMyCompany.clear();
                filterCategories.clear();
                filterContentTypes.clear();
            }
        });
    }

    private void setApplyClickListener() {

        mFragmentRootView.findViewById(R.id.fragment_filter_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle arguments = new Bundle();
                arguments.putStringArrayList(FILTER_MY_COMPANY, filterMyCompany);
                arguments.putStringArrayList(FILTER_CATEGORIES, filterCategories);
                arguments.putStringArrayList(FILTER_CONTENT_TYPES, filterContentTypes);

                onFilterAppliedListener.OnFilterApplied(arguments);

                finishFragment();
            }
        });
    }

    private void setMyCompanyClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_mycompany).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatCheckBox checkBox = mFragmentRootView.findViewById(R.id.fragment_filter_mycompany_checkbox);

                if (checkBox.isChecked()) {

                    checkBox.setChecked(false);
                    filterMyCompany.remove(FILTER_MY_COMPANY_VALUE);

                } else {

                    checkBox.setChecked(true);
                    filterMyCompany.add(FILTER_MY_COMPANY_VALUE);

                }
            }
        });
    }

    public void setOnFilterAppliedListener(OnFilterAppliedListener onFilterAppliedListener) {
        this.onFilterAppliedListener = onFilterAppliedListener;
    }

    private void initViews() {

        subCategoryRecyclerView = mFragmentRootView.findViewById(R.id.fragment_filter_subcategories_recyclerview);
        contentTypeRecyclerView = mFragmentRootView.findViewById(R.id.fragment_filter_content_type_recyclerview);

        closeButton = mFragmentRootView.findViewById(R.id.fragment_filter_close);
        clearButton = mFragmentRootView.findViewById(R.id.fragment_filter_clear);
        cancelButton = mFragmentRootView.findViewById(R.id.fragment_filter_cancel);
        applyButton = mFragmentRootView.findViewById(R.id.fragment_filter_apply);
    }

    private void finishFragment() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}
