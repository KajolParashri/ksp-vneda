package com.indiareads.ksp.view.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.ReadingShelfPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReadingShelfFragment extends Fragment {

    View mFragmentRootView;
    ViewPager viewPager;
    TabLayout tabLayout;

    public ReadingShelfFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentRootView = inflater.inflate(R.layout.fragment_reading_shelf, container, false);

        setUpViewPager();
        return mFragmentRootView;
    }

    private void setUpViewPager() {

        viewPager = mFragmentRootView.findViewById(R.id.fragment_reading_shelf_viewpager);

        viewPager.setOffscreenPageLimit(6);
        viewPager.setAdapter(new ReadingShelfPagerAdapter(getChildFragmentManager(), getActivity()));
        tabLayout = mFragmentRootView.findViewById(R.id.fragment_reading_shelf_tablayout);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_layout, null);
            TextView tv = view.findViewById(R.id.text1);
            tv.setText(tabLayout.getTabAt(i).getText());
            tabLayout.getTabAt(i).setCustomView(tv);
        }
    }

}
