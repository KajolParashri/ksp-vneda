package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.NewChatActivityRecyclerAdapter;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Conversation;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewChatActivity extends AppCompatActivity {

    private static String TAG = NewChatActivity.class.getSimpleName();
    private NewChatActivityRecyclerAdapter mRecyclerAdapter;
    private RecyclerView mRecyclerView;
    private ArrayList<User> mUsers;
    EditText searchBar;
    ArrayList<User> searchedItem;
    private String chatType;
    private HashMap<String, Boolean> selectedUsers = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_chat);

        getDataFromIntent();
        setUpToolbar();
        initViews();
        initConversationsList();
        initConversationRecyclerView();
        setRecyclerViewItemClickListener(mUsers);
        fetchData();
        setListeners();
        setCursorColor(searchBar, getResources().getColor(R.color.white));
    }

    private void getDataFromIntent() {
        chatType = getIntent().getStringExtra(Conversation.CHAT_TYPE);
    }

    private void initViews() {

        searchBar = findViewById(R.id.searchBar);
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String str = s.toString();
                str = str.trim();

                if (str.length() > 0) {
                    fetchSearcheduser(str);
                } else {
                    setAdapter(mUsers);
                    setRecyclerViewItemClickListener(mUsers);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        if (chatType.equals(Conversation.CHAT_TYPE_SINGLE))
            findViewById(R.id.create_layout).setVisibility(View.GONE);
        else
            findViewById(R.id.create_layout).setVisibility(View.VISIBLE);
    }

    public static void setCursorColor(EditText editText, @ColorInt int color) {
        try {
            // Get the cursor resource id
            Field field = TextView.class.getDeclaredField("mCursorDrawableRes");
            field.setAccessible(true);
            int drawableResId = field.getInt(editText);

            // Get the drawable and set a color filter
            Drawable drawable = ContextCompat.getDrawable(editText.getContext(), drawableResId);
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
            Drawable[] drawables = {drawable, drawable};

            if (Build.VERSION.SDK_INT == 15) {
                // Get the editor
                Class<?> drawableFieldClass = TextView.class;
                // Set the drawables
                field = drawableFieldClass.getDeclaredField("mCursorDrawable");
                field.setAccessible(true);
                field.set(editText, drawables);

            } else {
                // Get the editor
                field = TextView.class.getDeclaredField("mEditor");
                field.setAccessible(true);
                Object editor = field.get(editText);
                // Set the drawables
                field = editor.getClass().getDeclaredField("mCursorDrawable");
                field.setAccessible(true);
                field.set(editor, drawables);
            }
        } catch (Exception e) {
            Logger.error(TAG, e.getMessage());
        }
    }

    private void setListeners() {
        findViewById(R.id.create_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String groupName = ((EditText) findViewById(R.id.groupName)).getText().toString();
                if (groupName.trim().isEmpty())
                    Toast.makeText(NewChatActivity.this, R.string.enter_valid_group_name, Toast.LENGTH_SHORT).show();
                else {
                    StringBuilder users = new StringBuilder("");
                    int selected = 0;
                    for (Map.Entry<String, Boolean> entry : selectedUsers.entrySet()) {
                        if (entry.getValue()) {
                            users.append(entry.getKey()).append(",");
                            selected++;
                        }
                    }
                    if (selected < 2)
                        Toast.makeText(NewChatActivity.this, R.string.select_atleast_two_members, Toast.LENGTH_SHORT).show();
                    else {
                        sendNewChatRequest(users.deleteCharAt(users.length() - 1).toString(), groupName);
                    }
                }
            }
        });
    }

    private void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.activity_new_chat_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (chatType.equals(Conversation.CHAT_TYPE_SINGLE)) {
            toolbar.setTitle("Select Person");
        } else {
            toolbar.setTitle("Create Team");
        }
    }

    private void initConversationsList() {
        mUsers = new ArrayList<>();
        addProgressBarInConversationList();
    }

    private void addProgressBarInConversationList() {
        User progressBarConversation = new User();
        progressBarConversation.setUserType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mUsers.add(progressBarConversation);
    }

    private void initConversationRecyclerView() {
        mRecyclerView = findViewById(R.id.activity_new_chat_recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRecyclerAdapter = new NewChatActivityRecyclerAdapter(this, mUsers, chatType, selectedUsers);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    private void setRecyclerViewItemClickListener(final ArrayList<User> list) {
        mRecyclerAdapter.setOnRecyclerItemClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                if (chatType.equals(Conversation.CHAT_TYPE_SINGLE)) {
                    Logger.debug("userID", list.get(position).getUserId());
                    Logger.debug("userName", list.get(position).getFullName());
                    chatType = list.get(position).getUserType();
                    sendNewChatRequest(list.get(position).getUserId(), list.get(position).getFullName());
                } else {
                    CheckBox checkbox = v.findViewById(R.id.checkbox);
                    selectedUsers.put(list.get(position).getUserId(), !checkbox.isChecked());
                    checkbox.setChecked(selectedUsers.get(list.get(position).getUserId()));
                }
            }
        });
    }

    private void sendNewChatRequest(String userId, final String chatName) {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_START_CHAT);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CHAT_TYPE, chatType);
            jsonObject.put(Constants.Network.CHAT_NAME, chatName);
            jsonObject.put(Constants.Network.USERS, userId);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    Intent chatIntent = new Intent(NewChatActivity.this, ChatActivity.class);
                                    chatIntent.putExtra(Constants.Network.CONVO_ID, response.getString(Constants.Network.DATA));
                                    chatIntent.putExtra(Constants.Network.USER_FULL_NAME, chatName);
                                    chatIntent.putExtra(Constants.Network.CHAT_TYPE, chatType);
                                    startActivity(chatIntent);
                                    finish();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(NewChatActivity.this);
                                } else {
                                    Toast.makeText(NewChatActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                Toast.makeText(NewChatActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {


                            Error error = Error.generateError(VolleyError.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                            Toast.makeText(NewChatActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            Toast.makeText(NewChatActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchData() {
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_USERS_FOR_CHAT);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.KEY, "");
            jsonObject.put(Constants.Network.CHAT_TYPE_I, "2");

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar
                                    mUsers.remove(mUsers.size() - 1);

                                    mRecyclerAdapter.notifyItemRemoved(mUsers.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        User conversation = new User();
                                        conversation.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                        conversation.setFullName(jsonObject1.getString(Constants.Network.FULL_NAME));
                                        conversation.setDesignation(jsonObject1.getString(Constants.Network.DESIGNATION));
                                        conversation.setProfilePic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                        conversation.setUserType(User.USER_TYPE_NORMAL);

                                        mUsers.add(conversation);
                                        selectedUsers.put(conversation.getUserId(), false);
                                    }

                                    mRecyclerAdapter.notifyItemRangeInserted(mUsers.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(NewChatActivity.this);
                                } else {
                                    //to remove progress bar
                                    mUsers.remove(mUsers.size() - 1);
                                    mRecyclerAdapter.notifyItemRemoved(mUsers.size() - 1);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }


    private void fetchSearcheduser(String query) {

        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_USERS_FOR_CHAT);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());
            jsonObject.put(Constants.Network.KEY, query);
            jsonObject.put(Constants.Network.CHAT_TYPE_I, "1");

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CHAT_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            searchedItem = new ArrayList<>();

                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        User conversation = new User();
                                        conversation.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                        conversation.setFullName(jsonObject1.getString(Constants.Network.FULL_NAME));
                                        String designation = jsonObject1.getString(Constants.Network.DESIGNATION);

                                        if (designation.equals("0") || designation.equals("null")) {
                                            designation = "";
                                        }

                                        conversation.setDesignation(designation);
                                        conversation.setProfilePic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                        conversation.setUserType(jsonObject1.getString(Constants.Network.CHAT_TYPE));
                                        String chatTypes = jsonObject1.getString(Constants.Network.CHAT_TYPE);

                                        if (chatTypes.equals("1")) {
                                            searchedItem.add(conversation);
                                        }
                                    }

                                    setAdapter(searchedItem);
                                    setRecyclerViewItemClickListener(searchedItem);

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(NewChatActivity.this);
                                } else {
                                    //to remove progress bar
                                    setAdapter(searchedItem);
                                    setRecyclerViewItemClickListener(searchedItem);
                                }

                            } catch (JSONException e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    50000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), MessageActivity.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    public void setAdapter(ArrayList<User> arrayList) {
        mRecyclerAdapter = new NewChatActivityRecyclerAdapter(this, arrayList, chatType, selectedUsers);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    private void displayErrorUI(Error error) {
        CommonMethods.showErrorView(this, R.id.activity_new_chat_root_layout, error);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}