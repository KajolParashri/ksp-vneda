package com.indiareads.ksp.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.ReadingShelfContentFragmentRecyclerAdapter;
import com.indiareads.ksp.listeners.OnOrderClickListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReadingShelfContentFragment extends Fragment {

    private static final String TAG = ReadingShelfContentFragment.class.getSimpleName();

    private View mFragmentRootView; //Root view
    private LinearLayoutManager linearLayoutManager;
    private ReadingShelfContentFragmentRecyclerAdapter mContentFragmentRecyclerAdapter;
    private RecyclerView mContentsRecyclerView;

    private List<Content> mContents; //Contents list

    private int pageNumber = 1;
    private boolean doPagination = true;
    private boolean mContentsLoading = true;

    private String mContentType;

    public ReadingShelfContentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDataFromIntent();
    }

    private void getDataFromIntent() {
        if (getArguments() != null)
            mContentType = getArguments().getString(Constants.Content.CONTENT_TYPE);
        else
            showErrMsg();
    }

    private void showErrMsg() {
        Logger.error(TAG, "ERROR");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentRootView = inflater.inflate(R.layout.fragment_reading_shelf_content, container, false);
        initViews();
        initContentList();
        initRecyclerView();
        setContentFragmentClickListener();
        setRecyclerViewScrollListener();
        fetchData();
        return mFragmentRootView;
    }

    private void setContentFragmentClickListener() {

        mContentFragmentRecyclerAdapter.setOnClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent productIntent = new Intent(getContext(), ProductActivity.class);
                productIntent.putExtra(Constants.Network.CONTENT_ID, mContents.get(position).getContentId());
                productIntent.putExtra(Constants.Network.CONTENT_TYPE, mContentType);
                productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, mContents.get(position).getStatusType());
                startActivity(productIntent);
            }
        });
    }

    private void initContentList() {
        mContents = new ArrayList<>();
        addProgressBarInList();
    }

    private void initViews() {
        mContentsRecyclerView = mFragmentRootView.findViewById(R.id.reading_shelf_content_fragment_recycler_view);
    }

    private void setRecyclerViewScrollListener() {
        //Fetching next page's data on reaching bottom
        mContentsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (mContentsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mContentsLoading = false;
                            addProgressBarInList();
                            mContentFragmentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);
                            pageNumber++;
                            fetchData();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void fetchData() {
        mContentsLoading = false;
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(getActivity()).getUserId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, mContentType);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_LOAD_SHELF_CONTENTS);

            Logger.error(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.error(TAG, response.toString());
                            if (mContents.size() > 0) {
                                mContents.remove(mContents.size() - 1);
                                mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
                            }

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        Content content = new Content();
                                        content.setContentType(mContentType);
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));

                                        if (jsonObject1.has(Constants.Network.SOURCE_NAME)) {
                                            content.setSourceName(jsonObject1.getString(Constants.Network.SOURCE_NAME));
                                            content.setAuthorName(jsonObject1.getString(Constants.AUTHOR));
                                        }
                                        content.setThumbnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        if (jsonObject1.has(Constants.Network.RATING)) {
                                            content.setAvgContentRating(jsonObject1.getString(Constants.Network.RATING));
                                        }
                                        content.setStatusType(jsonObject1.getString(Constants.Network.CONTENT_STATUS_TYPE));
                                        mContents.add(content);
                                    }

                                    mContentFragmentRecyclerAdapter.notifyItemRangeInserted(mContents.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {
                                    doPagination = false;
                                    if (pageNumber == 1) {
                                        showEmptyView(R.mipmap.ic_fevicon, getString(R.string.content_list_empty));
                                    } else {
                                        //no more contents
                                    }
                                }
                                mContentsLoading = true;
                            } catch (JSONException e) {
                                Logger.error(TAG, "In OnResponse" + e.getMessage());
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Logger.error(TAG, error.getMessage() + " is the volley error ");
                            doPagination = false;
                            if (error instanceof NetworkError)
                                showEmptyView(R.drawable.ic_signal_wifi_off, getString(R.string.connection_error_message));
                            else
                                showEmptyView(R.drawable.ic_error_outline, getString(R.string.something_went_wrong));
                            mContentsLoading = true;
                        }
                    });
            jsonObjectRequest.setTag(TAG);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
            if (mContents.size() > 0) {
                mContents.remove(mContents.size() - 1);
                mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);
            }
            Logger.error(TAG, "In fetchData" + e.getMessage());
            e.printStackTrace();

        }
    }

    private void showEmptyView(int imageId, String msg) {
        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(imageId));
        emptyContent.setTitle(msg);
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mContents.add(emptyContent);

        mContentsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mContentFragmentRecyclerAdapter.notifyItemInserted(0);
    }

    private void addProgressBarInList() {
        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mContents.add(progressBarContent);
    }

    private void initRecyclerView() {
        mContentFragmentRecyclerAdapter = new ReadingShelfContentFragmentRecyclerAdapter(getActivity(), mContents, new OnOrderClickListener() {
            @Override
            public void OnReturnClick(int position) {
            }

            @Override
            public void OnCancelClick(int position) {
            }
        });

        linearLayoutManager = new LinearLayoutManager(getActivity());
        mContentsRecyclerView.setLayoutManager(linearLayoutManager);
        mContentsRecyclerView.setAdapter(mContentFragmentRecyclerAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}
