package com.indiareads.ksp.view.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.ContentFragmentAdapter;
import com.indiareads.ksp.listeners.OnDraftedContentItemClickListener;
import com.indiareads.ksp.listeners.OnRefreshListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.EditorActivity;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.view.activity.SetupCourseActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.indiareads.ksp.utils.Constants.COURSE.TITLE;
import static com.indiareads.ksp.utils.Constants.Network.DATA;

public class DemoContentFragment extends Fragment implements OnRefreshListener {

    private static final String TAG = DemoContentFragment.class.getSimpleName();
    private View mFragmentRootView;
    private LinearLayoutManager linearLayoutManager;
    private ContentFragmentAdapter mContentFragmentRecyclerAdapter;
    private RecyclerView mContentsRecyclerView;
    private List<Content> mContents; //Contents list
    private int pageNumber = 1;
    private boolean doPagination = true;
    private boolean mContentsLoading = true;
    private String userId;
    private String skipContentType;
    private String requiredContentType;
    Dialog mAlertDialog;
    private String contentTypeStr = "";

    public DemoContentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getDataFromArguments();
    }

    private void getDataFromArguments() {

        if (getArguments() != null) {
            contentTypeStr = getArguments().getString("Type");
            userId = getArguments().getString(Constants.Network.USER_ID, "");
            skipContentType = getArguments().getString(Constants.Network.SKIP_CONTENT_TYPE, String.valueOf(Constants.Content.CONTENT_TYPE_POST) + "," + String.valueOf(Constants.Content.CONTENT_TYPE_ANNOUNCEMENT));
            requiredContentType = getArguments().getString(Constants.Network.REQUIRED_CONTENT_TYPE, "");
        } else {
            Error error = Error.generateError(NullPointerException.class.getSimpleName(), DemoContentFragment.class.getSimpleName(),
                    "getDataFromArguments", Error.ERROR_TYPE_NORMAL_ERROR, "Arguments Null");

            displayErrorUI(error);
        }

    }

    private void displayErrorUI(Error error) {

        CommonMethods.showErrorView(this, R.id.fragment_content_root_layout, error);

    }

    public void showProgressBar() {
        mAlertDialog = new Dialog(getActivity());
        mAlertDialog.setContentView(R.layout.progress_dialog_layout);
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();
    }


    public void hideProgressBar() {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentRootView = inflater.inflate(R.layout.fragment_content, container, false);

        initViews();
        initContentList();
        initRecyclerView();
        setContentFragmentClickListener();
        setRecyclerViewScrollListener();

        fetchData();

        return mFragmentRootView;
    }

    private void setContentFragmentClickListener() {

        mContentFragmentRecyclerAdapter.setOnClickListener(new OnDraftedContentItemClickListener() {
            @Override
            public void onEditClick(View v, int position) {
                if (mContents.get(position).getContentType().equalsIgnoreCase("1")) {
                    Intent intent = new Intent(getActivity(), EditorActivity.class);
                    intent.putExtra("type", Constants.Content.ARTICLE);
                    intent.putExtra(DATA, mContents.get(position).getContentId());
                    intent.putExtra(TITLE, mContents.get(position).getTitle());
                    intent.putExtra("content_type", "1");
                    startActivity(intent);

                } else if (mContents.get(position).getContentType().equalsIgnoreCase("3")) {
                    Intent intent = new Intent(getActivity(), SetupCourseActivity.class);
                    intent.putExtra("type", Constants.Content.COURSE);
                    intent.putExtra(DATA, mContents.get(position).getContentId());
                    intent.putExtra(TITLE, mContents.get(position).getTitle());
                    intent.putExtra("content_type", "3");
                    startActivity(intent);

                }
            }

            @Override
            public void onDeleteClick(View v, int position) {

                deleteData(mContents.get(position).getContentId(), position);
            }

            @Override
            public void onItemClick(View v, int position) {

                Intent productIntent = new Intent(getContext(), ProductActivity.class);
                productIntent.putExtra(Constants.Network.CONTENT_ID, mContents.get(position).getContentId());
                productIntent.putExtra(Constants.Network.CONTENT_TYPE, mContents.get(position).getContentType());
                productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);
                startActivity(productIntent);
            }

            @Override
            public void onLikeCOunt(View v, int position) {

            }

        });
    }

    private void deleteData(String content_id, final int pos) {
        try {
            showProgressBar();
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.CONTENT_ID, content_id);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.DELETE_CONTENT);

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideProgressBar();
                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    if (mContents.size() > pos) {
                                        mContents.remove(pos);
                                        mContentFragmentRecyclerAdapter.notifyItemRemoved(pos);
                                    }
                                    Toast.makeText(getActivity(), "Content Successfully Deleted", Toast.LENGTH_SHORT).show();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {

                                }

                            } catch (Exception e) {

                                Logger.error(TAG, "In OnResponse" + e.getMessage());
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideProgressBar();
                            Logger.error(TAG, error.getMessage() + " is the volley error ");

                        }
                    });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Logger.error(TAG, "In fetchData" + e.getMessage());
            e.printStackTrace();
            hideProgressBar();
        }
    }

    private void initContentList() {
        mContents = new ArrayList<>();
        addProgressBarInList();
    }

    private void initViews() {
        mContentsRecyclerView = mFragmentRootView.findViewById(R.id.content_fragment_recycler_view);
    }

    private void setRecyclerViewScrollListener() {
        //Fetching next page's data on reaching bottom
        mContentsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (mContentsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mContentsLoading = false;
                            addProgressBarInList();
                            mContentFragmentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);
                            pageNumber++;
                            fetchData();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void fetchData() {
        mContentsLoading = false;
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNumber);
            jsonObject.put(Constants.Network.USER_ID, userId);
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(getActivity()).getCompanyId());
            jsonObject.put(Constants.Network.SKIP_CONTENT_TYPE, skipContentType);
            jsonObject.put(Constants.Network.REQUIRED_CONTENT_TYPE, requiredContentType);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_DISPLAY_CONTENTS);
            jsonObject.put(Constants.Network.SESSION_USER_ID, User.getCurrentUser(getContext()).getUserId());
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);

            Logger.info(TAG, jsonObject.toString());
            Logger.error("POSTINTENT_CONTENTFRAG", jsonObject.toString());
            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.HOMEPAGE_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.error("POSTINTENT_CONTENTFRAG", response.toString());
                            Logger.info(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //to remove progress bar

                                    mContents.remove(mContents.size() - 1);

                                    mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //to locally filter skipContentType
                                    CommonMethods.filterContentsJsonArray(jsonArray, skipContentType);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        User user = new User();

                                        user.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                        user.setFullName(jsonObject1.getString(Constants.Network.USER_FULL_NAME));
                                        user.setProfilePic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                        user.setDesignation(jsonObject1.getString(Constants.Network.DESIGNATION));

                                        Content content = new Content();
                                        content.setContentId(jsonObject1.getString(Constants.Network.CONTENT_ID));

                                        if (jsonObject1.has(Constants.Network.SOURCE_NAME)) {
                                            content.setSourceName(jsonObject1.getString(Constants.Network.SOURCE_NAME));
                                            content.setAuthorName(jsonObject1.getString(Constants.AUTHOR));
                                        }

                                        content.setCreatedDate(jsonObject1.getString(Constants.Network.CREATED_DATE));
                                        content.setTitle(jsonObject1.getString(Constants.Network.TITLE));
                                        content.setThumnail(jsonObject1.getString(Constants.Network.THUMNAIL));
                                        // Toast.makeText(getActivity(), "" + jsonObject1.getString(Constants.Network.THUMNAIL), Toast.LENGTH_SHORT).show();
                                        content.setDescription(jsonObject1.getString(Constants.Network.DESCRIPTION));

                                        content.setContentType(jsonObject1.getString(Constants.Network.CONTENT_TYPE));
                                        content.setLikeStatus(jsonObject1.getString(Constants.Network.LIKED_STATUS));
                                        content.setLikeCount(jsonObject1.getString(Constants.Network.LIKE_COUNT));
                                        content.setCommentCount(jsonObject1.getString(Constants.Network.COMMENT_COUNT));

                                        content.setAvgContentRating(jsonObject1.getString(Constants.Network.AVG_CONTENT_RATING));
                                        content.setUser(user);

                                        mContents.add(content);
                                    }

                                    mContentFragmentRecyclerAdapter.notifyItemInserted(mContents.size() - 1);

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(getActivity());
                                } else {

                                    //stop call to pagination in any case
                                    doPagination = false;

                                    //to remove progress bar
                                    mContents.remove(mContents.size() - 1);
                                    mContentFragmentRecyclerAdapter.notifyItemRemoved(mContents.size() - 1);

                                    if (pageNumber == 1) {
                                        //show msg no contents
                                        showEmptyView();
                                    } else {
                                        //no more contents
                                    }
                                }

                                mContentsLoading = true;

                            } catch (Exception e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), DemoContentFragment.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

                                displayErrorUI(error);
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Logger.error("POSTINTENT_CONTENTFRAG", volleyError.toString());
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), DemoContentFragment.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());

                            displayErrorUI(error);
                        }
                    });

            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), DemoContentFragment.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            displayErrorUI(error);
        }
    }

    private void showEmptyView() {

        Content emptyContent = new Content();
        emptyContent.setThumbnail(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyContent.setTitle(getString(R.string.content_list_empty));
        emptyContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mContents.add(emptyContent);

        mContentsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mContentFragmentRecyclerAdapter.notifyItemInserted(0);

    }

    private void addProgressBarInList() {

        Content progressBarContent = new Content();
        progressBarContent.setContentType(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        mContents.add(progressBarContent);

    }

    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mContentsRecyclerView.setLayoutManager(linearLayoutManager);
        mContentFragmentRecyclerAdapter = new ContentFragmentAdapter(getActivity(), mContents, contentTypeStr);
        mContentsRecyclerView.setAdapter(mContentFragmentRecyclerAdapter);
    }


    @Override
    public void onRefresh() {
        fetchData();
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }
}
