package com.indiareads.ksp.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.indiareads.ksp.R;
import com.indiareads.ksp.view.fragment.MyCompanyFragment;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;

public class CompanyActivity extends AppCompatActivity {

    private static final String TAG = CompanyActivity.class.getSimpleName();
    int pageNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        getDataFromIntent();
        inflateFragment();
    }

    private void getDataFromIntent() {
        pageNo = getIntent().getIntExtra(MyCompanyFragment.PAGE_NO, 0);
        if (getIntent().getAction().equals("ACTION_VIEW_COMPANY"))
            pageNo = Integer.parseInt(getIntent().getStringExtra(UserDetailsFragment.PAGE_NO));
    }

    private void inflateFragment() {
        Bundle args = new Bundle();
        args.putInt(MyCompanyFragment.PAGE_NO, pageNo);

        MyCompanyFragment myCompanyFragment = new MyCompanyFragment();
        myCompanyFragment.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(R.id.activity_company_root_layout, myCompanyFragment).commit();
    }
}
