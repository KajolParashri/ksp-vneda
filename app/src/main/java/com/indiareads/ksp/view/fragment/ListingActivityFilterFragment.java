package com.indiareads.ksp.view.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.FilterFragmentRecyclerAdapter;
import com.indiareads.ksp.listeners.OnFilterAppliedListener;
import com.indiareads.ksp.listeners.OnFilterCategoryClickListener;
import com.indiareads.ksp.model.Category;
import com.indiareads.ksp.model.ChildCategory;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListingActivityFilterFragment extends Fragment {

    public static final String FILTER_MY_COMPANY = "Filter My Company";
    public static final String FILTER_MY_COMPANY_VALUE = "0";
    public static final String FILTER_CATEGORIES = "Filter Categories";
    private static final String TAG = ListingActivityFilterFragment.class.getSimpleName();

    private View mFragmentRootView;

    private ImageView closeButton;
    private TextView clearButton;
    private TextView cancelButton;
    private TextView applyButton;
    private RecyclerView recyclerView;

    private LinearLayoutManager linearLayoutManager;

    private FilterFragmentRecyclerAdapter filterFragmentRecyclerAdapter;

    private List<Category> parentCategories;

    private ArrayList<String> filterMyCompany;
    private ArrayList<String> filterCategories;

    private OnFilterAppliedListener onFilterAppliedListener;

    public ListingActivityFilterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mFragmentRootView = inflater.inflate(R.layout.fragment_listing_activity_filter, container, false);

        initViews();
        setClickListeners();
        getFilters();
        getCategories();

        return mFragmentRootView;
    }

    private void getFilters() {

        filterMyCompany = getArguments().getStringArrayList(FILTER_MY_COMPANY);
        filterCategories = getArguments().getStringArrayList(FILTER_CATEGORIES);

        setMyCompanyFilter();
    }

    private void setMyCompanyFilter() {
        AppCompatCheckBox checkBox = mFragmentRootView.findViewById(R.id.fragment_filter_mycompany_checkbox);

        if (filterMyCompany.size() > 0)
            checkBox.setChecked(true);
    }

    private void setRecyclerViewClickListener() {

        filterFragmentRecyclerAdapter.setOnClickListener(new OnFilterCategoryClickListener() {
            @Override
            public void onParentCategoryClicked(View view, int position) {
                CommonMethods.toggleVisibility(view.findViewById(R.id.fragment_filter_child_recyclerview));
            }

            @Override
            public void onChildCategoryClicked(View view, int parentCategoryposition, int childCategoryposition) {
                AppCompatCheckBox checkBox = view.findViewById(R.id.fragment_filter_child_category_checkbox);

                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);

                    filterCategories.remove(parentCategories.get(parentCategoryposition).getChildCategories().get(childCategoryposition).getCategoryId());
                } else {
                    checkBox.setChecked(true);

                    filterCategories.add(parentCategories.get(parentCategoryposition).getChildCategories().get(childCategoryposition).getCategoryId());
                }
            }
        });
    }

    private void getCategories() {
        try {
            CommonMethods.showProgressBar(mFragmentRootView, R.id.activity_listing_fragment_filter_root_layout);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_PARENT_AND_CHILD_CATEGORY);

            Logger.debug(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.CONTENT_LISTING_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.debug(TAG, response.toString());

                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                            parentCategories = new ArrayList<>();

                            JSONArray parentArray = response.getJSONArray(Constants.Network.DATA);

                            for (int i = 0; i < parentArray.length(); i++) {
                                JSONObject parentObject = parentArray.getJSONObject(i);

                                Category category = new Category();
                                category.setCategoryId(parentObject.getString(Constants.Network.CAT_ID));
                                category.setCategoryName(parentObject.getString(Constants.Network.CAT_NAME));

                                List<ChildCategory> childCategories = new ArrayList<>();

                                JSONArray childArray = parentObject.getJSONArray(Constants.Network.CHILD);

                                for (int j = 0; j < childArray.length(); j++) {
                                    JSONObject childObject = childArray.getJSONObject(j);

                                    ChildCategory childCategory = new ChildCategory();
                                    childCategory.setCategoryId(childObject.getString(Constants.Network.ID));
                                    childCategory.setCategoryName(childObject.getString(Constants.Network.NAME));

                                    childCategories.add(childCategory);
                                }

                                category.setChildCategories(childCategories);

                                parentCategories.add(category);
                            }

                            setCategoriesLayout();

                            CommonMethods.hideProgressBar(mFragmentRootView, R.id.activity_listing_fragment_filter_root_layout);

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG, "getCategories", Error.ERROR_TYPE_SERVER_ERROR, "400");
                            showErrorUI(error);
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), TAG, "getCategories", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        showErrorUI(error);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError e) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), TAG, "getCategories", Error.getErrorTypeFromVolleyError(e), e.getMessage());
                    showErrorUI(error);
                }
            });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), TAG, "getCategories", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            showErrorUI(error);
        }

    }

    private void setCategoriesLayout() {

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        filterFragmentRecyclerAdapter = new FilterFragmentRecyclerAdapter(getActivity(), parentCategories, filterCategories);
        recyclerView.setAdapter(filterFragmentRecyclerAdapter);
        setRecyclerViewClickListener();
    }

    private void showErrorUI(Error error) {
        CommonMethods.hideProgressBar(mFragmentRootView, R.id.activity_listing_fragment_filter_root_layout);
        CommonMethods.showErrorView(ListingActivityFilterFragment.this, R.id.activity_listing_fragment_filter_root_layout, error);
    }

    private void setClickListeners() {
        setMyCompanyClickListener();
        setCloseClickListener();
        setCancelClickListener();
        setClearClickListener();
        setApplyClickListener();
    }

    private void setCloseClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishFragment();
            }
        });
    }

    private void setCancelClickListener() {

        mFragmentRootView.findViewById(R.id.fragment_filter_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishFragment();
            }
        });
    }

    private void setClearClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterMyCompany.clear();
                filterCategories.clear();
            }
        });
    }

    private void setApplyClickListener() {

        mFragmentRootView.findViewById(R.id.fragment_filter_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle arguments = new Bundle();
                arguments.putStringArrayList(FILTER_MY_COMPANY, filterMyCompany);
                arguments.putStringArrayList(FILTER_CATEGORIES, filterCategories);

                onFilterAppliedListener.OnFilterApplied(arguments);

                finishFragment();
            }
        });
    }

    private void setMyCompanyClickListener() {
        mFragmentRootView.findViewById(R.id.fragment_filter_mycompany).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatCheckBox checkBox = mFragmentRootView.findViewById(R.id.fragment_filter_mycompany_checkbox);

                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);

                    filterMyCompany.remove(FILTER_MY_COMPANY_VALUE);
                } else {
                    checkBox.setChecked(true);

                    filterMyCompany.add(FILTER_MY_COMPANY_VALUE);
                }
            }
        });
    }

    public void setOnFilterAppliedListener(OnFilterAppliedListener onFilterAppliedListener) {
        this.onFilterAppliedListener = onFilterAppliedListener;
    }

    private void initViews() {

        recyclerView = mFragmentRootView.findViewById(R.id.fragment_filter_recyclerview);
        closeButton = mFragmentRootView.findViewById(R.id.fragment_filter_close);
        clearButton = mFragmentRootView.findViewById(R.id.fragment_filter_clear);
        cancelButton = mFragmentRootView.findViewById(R.id.fragment_filter_cancel);
        applyButton = mFragmentRootView.findViewById(R.id.fragment_filter_apply);
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }

    private void finishFragment() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
