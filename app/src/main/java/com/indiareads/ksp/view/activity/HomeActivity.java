package com.indiareads.ksp.view.activity;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.MenuExpandAdapter;
import com.indiareads.ksp.explore.fragment.NewExploreFragment;
import com.indiareads.ksp.model.CompanyFeature;
import com.indiareads.ksp.model.MenuModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.BottomNavigationViewHelper;
import com.indiareads.ksp.utils.CircleTransform;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SharedPreferencesHelper;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.fragment.CreateFragment;
import com.indiareads.ksp.view.fragment.HomeFragment;
import com.indiareads.ksp.view.fragment.MyCompanyFragment;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.indiareads.ksp.utils.SideBarPanel.accountMenuName;
import static com.indiareads.ksp.utils.SideBarPanel.chatMenuName;
import static com.indiareads.ksp.utils.SideBarPanel.companyMenuName;
import static com.indiareads.ksp.utils.SideBarPanel.feedbackMenuName;
import static com.indiareads.ksp.utils.SideBarPanel.othersMenuName;
import static com.indiareads.ksp.utils.SideBarPanel.redeemPoints;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = HomeActivity.class.getSimpleName();
    public Toolbar toolbar;
    public int lastExpandedPosition = -1;
    public DrawerLayout drawer;
    public BottomNavigationView mHomeBottomNav;
    public ImageView profileImageView;
    public EditText searchEdittext;
    private ArrayList<String> listGroupMenuName;
    private HashMap<String, ArrayList<MenuModel>> listChildMenuName;

    private String notificationCount;
    private String messageCount;

    public static PubNub pubnub;
    TextView user_name_tv, designation_tv, rank_tv, earned_tv, content_created_tv, content_consume_tv;
    SelectedBundle selectedBundle;
    TextView home_notification_count;
    ImageView user_imageView;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_home);

        mContext = this;
        if (User.getCurrentUser(this) == null)
            sendToLogin();
        else {

            setUpToolbar();
            fetchViews();
            setToolbarIconClickListener();
            setUpSearchBar();
            setUpNavigationDrawer();
            setupBottomNav();
            selectCreateDefault();
            getDataFromIntent();
            handlePushNotificationService();

            if (!SharedPreferencesHelper.getTutorialStatus(this)) {
                SharedPreferencesHelper.saveTutorialStatus(HomeActivity.this, true);
                showTutorial();
            }
        }
    }

    private void handlePushNotificationService() {
        try {
            Intent intent = new Intent();

            String manufacturer = android.os.Build.MANUFACTURER;

            switch (manufacturer) {

                case "xiaomi":
                    intent.setComponent(new ComponentName("com.miui.securitycenter",
                            "com.miui.permcenter.autostart.AutoStartManagementActivity"));
                    break;
                case "oppo":
                    intent.setComponent(new ComponentName("com.coloros.safecenter",
                            "com.coloros.safecenter.permission.startup.StartupAppListActivity"));

                    break;
                case "vivo":
                    intent.setComponent(new ComponentName("com.vivo.permissionmanager",
                            "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
                    break;
            }

            List<ResolveInfo> arrayList = getPackageManager().queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY);

            if (arrayList.size() > 0) {
                startActivity(intent);
            }
        } catch (Exception e) {
            Logger.error(TAG, e.getMessage());
        }
    }


    public void showTutorial() {

        // We have a sequence of targets, so lets build it!
        final TapTargetSequence sequence = new TapTargetSequence(this)
                .targets(
                        // Likewise, this tap target will target the search button
                        TapTarget.forView(findViewById(R.id.home_message_icon), "Messages",
                                "Peer to peer, group-chat and file sharing")
                                .targetRadius(20)
                                .dimColor(android.R.color.white)
                                .outerCircleColor(R.color.colorAccent)
                                .targetCircleColor(android.R.color.white)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(3),
                        TapTarget.forView(findViewById(R.id.home_notification_icon), "Notifications",
                                "Updates on your account and company's account")
                                .dimColor(android.R.color.white)
                                .outerCircleColor(R.color.colorAccent)
                                .targetCircleColor(android.R.color.white)
                                .targetRadius(20)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(2),

                        TapTarget.forView(findViewById(R.id.bottom_books), "Corporate Library",
                                "Search , Select and Place your book order")
                                .dimColor(android.R.color.white)
                                .outerCircleColor(R.color.colorAccent)
                                .targetCircleColor(android.R.color.white)
                                .targetRadius(25)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(4),
                        TapTarget.forView(findViewById(R.id.bottom_create), "Create",
                                "Create different type of content and share with others")
                                .dimColor(android.R.color.white)
                                .targetRadius(25)
                                .outerCircleColor(R.color.colorAccent)
                                .targetCircleColor(android.R.color.white)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(5),
                        TapTarget.forView(findViewById(R.id.bottom_mycompany), "Company",
                                "Get all company learning and information")
                                .dimColor(android.R.color.white)
                                .outerCircleColor(R.color.colorAccent)
                                .targetCircleColor(android.R.color.white)
                                .targetRadius(30)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(7),
                        TapTarget.forView(findViewById(R.id.bottom_explore), "Explore",
                                "Explore different type of knowledge resources")
                                .dimColor(android.R.color.white)
                                .targetRadius(25)
                                .outerCircleColor(R.color.colorAccent)
                                .targetCircleColor(android.R.color.white)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(6)
                )
                .listener(new TapTargetSequence.Listener() {
                    // This listener will tell us when interesting(tm) events happen in regards
                    // to the sequence
                    @Override
                    public void onSequenceFinish() {
                        SharedPreferencesHelper.saveTutorialStatus(HomeActivity.this, true);
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        Logger.debug("TapTargetView", "Clicked on " + lastTarget.id());
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {
                        SharedPreferencesHelper.saveTutorialStatus(HomeActivity.this, true);
                    }
                });

        TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.activity_home_toolbar_profile),
                "Your Account", "Your profile, content and orders")
                        .cancelable(true)
                        .targetRadius(20)
                        .drawShadow(true)
                        .dimColor(android.R.color.white)
                        .outerCircleColor(R.color.colorAccent)
                        .targetCircleColor(android.R.color.white)
                        .transparentTarget(true)
                        .titleTextDimen(R.dimen.fab_margin)
                        .tintTarget(false),

                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        sequence.start();
                    }

                    @Override
                    public void onOuterCircleClick(TapTargetView view) {
                        super.onOuterCircleClick(view);
                        sequence.start();
                    }

                    @Override
                    public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                        SharedPreferencesHelper.saveTutorialStatus(HomeActivity.this, true);
                    }
                });
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent.hasExtra("fragment")) {
            Logger.info(TAG, "INTENT");
            switch (intent.getStringExtra("fragment")) {
                case "create":
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, new CreateFragment()).commit();
                    setUpFragmentLayout(R.id.bottom_create);
                    break;
                case "explore":
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction =
                            fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.activity_home_fragment_layout, NewExploreFragment.newInstance("SET1", "SET1"));
                    fragmentTransaction.commit();
                    setUpFragmentLayout(R.id.bottom_explore);
                    break;
                case "mycompany":
                    MyCompanyFragment myCompanyFragment = new MyCompanyFragment();
                    Bundle args = new Bundle();
                    args.putInt(MyCompanyFragment.PAGE_NO, Integer.parseInt(intent.getStringExtra("page_no")));
                    myCompanyFragment.setArguments(args);

                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, myCompanyFragment).commit();
                    setUpFragmentLayout(R.id.bottom_mycompany);
                    break;
                default:
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, new HomeFragment(), "Home").commit();
            }
        }
    }

    public static PubNub getPubnubInstance() {
        if (pubnub == null)
            initialisePubnub();
        return pubnub;
    }

    private static void initialisePubnub() {
        PNConfiguration config = new PNConfiguration();
        config.setPublishKey(Constants.Pubnub.PUBNUB_PUBLISH_KEY);
        config.setSubscribeKey(Constants.Pubnub.PUBNUB_SUBSCRIBE_KEY);
//        config.setUuid(User.getCurrentUser(this).getUserId());
        config.setSecure(false);
        pubnub = new PubNub(config);
    }

    public void selectCreateDefault() {
        Intent intent = getIntent();
        if (intent.hasExtra("intentaction")) {
            openCreateFrag();
        }
    }

    public interface SelectedBundle {
        void onBundleSelect(String bundle);

    }

    public void setOnBundleSelected(SelectedBundle selectedBundle) {
        this.selectedBundle = selectedBundle;
    }

    public void CLickEvent(final DrawerLayout drawer) {

        RelativeLayout header = drawer.findViewById(R.id.main);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        LinearLayout created_overlay = drawer.findViewById(R.id.created_overlay);

        LinearLayout consumed_overlay = drawer.findViewById(R.id.consumed_overlay);

        user_name_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
                accountBundle(7);
            }
        });

        created_overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountBundle(1);
            }
        });

        consumed_overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
                accountBundle(2);
            }
        });

        findViewById(R.id.callCustomerCare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:01204294235"));
                startActivity(intent);
            }
        });
    }

    public void initDrawer(final DrawerLayout drawer) {
        final ExpandableListView expListView = findViewById(R.id.expandList);
        expListView.setChildDivider(getResources().getDrawable(R.color.white));
        expListView.setDividerHeight(2);
        prepareListData();

        MenuExpandAdapter listAdapter = new MenuExpandAdapter(this, listGroupMenuName, listChildMenuName);
        // setting list adapter
        expListView.setAdapter(listAdapter);

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPos, int childPos, long l) {
                MenuModel menuModel = listChildMenuName.get(listGroupMenuName.get(groupPos)).get(childPos);
                String name = menuModel.getMenuName();
                String type = menuModel.getType();
                int pos = menuModel.getMenuNumberIndex();
                if (name.equalsIgnoreCase("About Us")) {

                    Intent intent = new Intent(HomeActivity.this, AboutContactActivity.class);
                    intent.putExtra("URL", "https://www.vneda.com/aboutus");
                    startActivity(intent);
                } else if (name.equalsIgnoreCase("Contact Us")) {
                    Intent intent = new Intent(HomeActivity.this, AboutContactActivity.class);
                    intent.putExtra("URL", "https://www.vneda.com/contactus");
                    startActivity(intent);
                } else if (name.equalsIgnoreCase("Privacy Policies")) {

                    Intent intent = new Intent(HomeActivity.this, AboutContactActivity.class);
                    intent.putExtra("URL", "https://www.vneda.com/privacy");
                    startActivity(intent);
                } else if (name.equalsIgnoreCase("Terms and Conditions")) {

                    Intent intent = new Intent(HomeActivity.this, AboutContactActivity.class);
                    intent.putExtra("URL", "https://www.vneda.com/terms");
                    startActivity(intent);
                } else if (name.equalsIgnoreCase("Logout")) {
                    logoutApp();
                } else if (name.equalsIgnoreCase("Redeem Points")) {
                    Intent intent = new Intent(HomeActivity.this, ReedemPointsActivity.class);
                    startActivity(intent);
                } else if (name.equalsIgnoreCase("My Coupons")) {
                    Intent intent = new Intent(HomeActivity.this, MyCoupons.class);
                    intent.putExtra("intent", "home");
                    startActivity(intent);
                } else if (name.equalsIgnoreCase("My Address Book")) {
                    Intent intent = new Intent(HomeActivity.this, SelectAddressActivity.class);
                    intent.putExtra("intent", "home");
                    startActivity(intent);
                } else if (name.equalsIgnoreCase("Feedback")) {
                    Intent intent = new Intent(HomeActivity.this, FeedbackActivity.class);
                    intent.putExtra("intent", "home");
                    startActivity(intent);
                } else {

                    returnIntent(name, type, drawer, pos);
                }
                return false;
            }
        });
        expListView.expandGroup(0);
    }

    public void prepareListData() {
        listGroupMenuName = new ArrayList<String>();
        listChildMenuName = new HashMap<String, ArrayList<MenuModel>>();

        // Adding child data
        listGroupMenuName.add(SideBarPanel.ACCOUNT);
        listGroupMenuName.add(SideBarPanel.REDEEM);
        listGroupMenuName.add(SideBarPanel.COMPANY);
        listGroupMenuName.add(SideBarPanel.CHAT);
        listGroupMenuName.add(SideBarPanel.OTHERS);
        listGroupMenuName.add(SideBarPanel.FEEDBACK);

        // Adding child data
        listChildMenuName.put(listGroupMenuName.get(0), accountMenuName());
        listChildMenuName.put(listGroupMenuName.get(1), redeemPoints());
        listChildMenuName.put(listGroupMenuName.get(2), companyMenuName());
        listChildMenuName.put(listGroupMenuName.get(3), chatMenuName());
        listChildMenuName.put(listGroupMenuName.get(4), othersMenuName());
        listChildMenuName.put(listGroupMenuName.get(5), feedbackMenuName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (searchEdittext != null)
            searchEdittext.setText("");

        if (SharedPreferencesHelper.getToken(this).trim().equals("")) {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            try {
                                if (!task.isSuccessful()) {
                                    Logger.error(TAG, "New FCM Token not generated");
                                    return;
                                }
                                String token = task.getResult().getToken();
                                SharedPreferencesHelper.saveToken(getApplicationContext(), token);
                                Logger.error(TAG, "New FCM Token:" + token);
                                getAccountDetailsRequest();
                            } catch (Exception e) {
                                Logger.error(TAG, e.getMessage());
                            }
                        }
                    });
        } else
            getAccountDetailsRequest();
    }

    private void setUpSearchBar() {

        searchEdittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Intent searchActivityIntent = new Intent(HomeActivity.this, SearchActivity.class);
                searchActivityIntent.putExtra(SearchActivity.SEARCH_QUERY, v.getText().toString());
                HomeActivity.this.startActivity(searchActivityIntent);
                CommonMethods.hideSoftInput(HomeActivity.this);
                return true;
            }
        });
    }

    private void setToolbarIconClickListener() {
        setMessageIconClickListener();
        setProfileIconClickListener();
        setNotificationIconClickListener();
    }

    private void setMessageIconClickListener() {
        findViewById(R.id.home_message_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, MessageActivity.class));
            }
        });
    }

    private void setNotificationIconClickListener() {

        findViewById(R.id.home_notification_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

    }

    private void setProfileIconClickListener() {
        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAccountFragment();
            }
        });
    }

    public void openAccountFragment() {

        Intent userProfileIntent = new Intent(HomeActivity.this, UserDetailsActivity.class);
        userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(HomeActivity.this).getUserId());
        userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(HomeActivity.this).getCompanyId());
        userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 0);
        startActivity(userProfileIntent);
    }

    private void fetchViews() {
        profileImageView = toolbar.findViewById(R.id.activity_home_toolbar_profile);
        searchEdittext = toolbar.findViewById(R.id.activity_home_search_bar);
        home_notification_count = toolbar.findViewById(R.id.home_notification_count);

        drawer = findViewById(R.id.home_drawer_layout);
        user_imageView = drawer.findViewById(R.id.imageView);
        user_name_tv = drawer.findViewById(R.id.user_name);
        designation_tv = drawer.findViewById(R.id.designation);
        rank_tv = drawer.findViewById(R.id.rank_tv);
        earned_tv = drawer.findViewById(R.id.earned_tv);
        content_created_tv = drawer.findViewById(R.id.content_created_tv);
        content_consume_tv = drawer.findViewById(R.id.content_consume_tv);

    }

    private void loadToolbarIconDetails() {
        Picasso.get().load(Urls.AWS_PROFILE_IMAGE_PATH + User.getCurrentUser(this).getProfilePic())
                .error(R.drawable.ic_account_circle)
                .placeholder(R.drawable.ic_account_circle)
                .transform(new CircleTransform())
                .into(profileImageView);
    }

    public void setUpFragmentLayout(int itemId) {
        if (itemId == R.id.bottom_explore) {
            //      mAboutDataListener.onDataReceived();
        }
        mHomeBottomNav.setSelectedItemId(itemId);
    }

    public void setUpNavigationDrawer() {

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                Logger.info(TAG, "onDrawerOpened");
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        initDrawer(drawer);
        CLickEvent(drawer);
    }

    public void setUpToolbar() {
        toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
    }

    private void sendToLogin() {
        Intent loginIntent = new Intent(HomeActivity.this, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
        finish();
    }

    public void openCreateFrag() {
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, new CreateFragment()).commit();
        mHomeBottomNav.setSelectedItemId(R.id.bottom_create);
    }

    //Setting up bottom nav
    private void setupBottomNav() {

        mHomeBottomNav = findViewById(R.id.activity_home_bottom_nav);
        BottomNavigationViewHelper.disableShiftMode(mHomeBottomNav);

        mHomeBottomNav.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                //do nothing
            }
        });

        mHomeBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.bottom_books:
                        FragmentManager fragmentManager1 = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction1 =
                                fragmentManager1.beginTransaction();
                        fragmentTransaction1.replace(R.id.activity_home_fragment_layout, NewExploreFragment.newInstance("SET2", "SET2"));
                        fragmentTransaction1.commit();
                        return true;
                    case R.id.bottom_create:
                        getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, new CreateFragment()).commit();
                        return true;
                    case R.id.bottom_explore:

                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction =
                                fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.activity_home_fragment_layout, NewExploreFragment.newInstance("SET1", "SET1"));
                        fragmentTransaction.commit();
                        return true;
                    case R.id.bottom_mycompany:

                        MyCompanyFragment myCompanyFragment = new MyCompanyFragment();
                        Bundle args = new Bundle();
                        args.putInt(MyCompanyFragment.PAGE_NO, 0);
                        myCompanyFragment.setArguments(args);

                        getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, myCompanyFragment).commit();
                        return true;
                    default:
                        getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, new HomeFragment(), "Home").commit();
                        return true;
                }
            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, new HomeFragment()).commit();
    }

    public void accountBundle(int pos) {

        Intent userProfileIntent = new Intent(HomeActivity.this, UserDetailsActivity.class);
        userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(HomeActivity.this).getUserId());
        userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(HomeActivity.this).getCompanyId());
        userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, pos);
        startActivity(userProfileIntent);
    }

    public void companyBundle(int pageeNmner) {

        MyCompanyFragment myCompanyFragment = new MyCompanyFragment();
        Bundle args = new Bundle();
        args.putInt(MyCompanyFragment.PAGE_NO, pageeNmner);
        myCompanyFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_home_fragment_layout, myCompanyFragment)
                .commit();
        mHomeBottomNav.setSelectedItemId(R.id.bottom_mycompany);
    }


    public void returnIntent(String name, String type, DrawerLayout drawer, int pos) {

        drawer.closeDrawer(Gravity.START, true);
        //accountBundle
        if (name.equals(getString(R.string.myaccount)) || name.equals(getString(R.string.my_posts)) || name.equals(getString(R.string.my_contents))
                || name.equals(getString(R.string.my_bookshelf)) || name.equals(getString(R.string.my_reading_shelf)) || name.equals(getString(R.string.my_drafts)) || name.equals(getString(R.string.my_orders)) || name.equals(getString(R.string.my_repo))
                || name.equals(getString(R.string.my_address_book)) || name.equals(getString(R.string.edit_profile))) {

            if (type.equalsIgnoreCase(getString(R.string.account))) {
                accountBundle(pos);
            }
        }
        //companyBundle
        if (name.equals(getString(R.string.announcement)) || name.equals(getString(R.string.posts))
                || name.equals(getString(R.string.contents)) || name.equals(getString(R.string.central_repo))) {

            if (type.equals(getString(R.string.company))) {
                companyBundle(pos);
            }

        } else if (name.equals(getString(R.string.suggestion_board))) {
            Intent intent = new Intent(HomeActivity.this, SuggestionBoardActi.class);
            startActivity(intent);
        } else if (name.equals(getString(R.string.request_book))) {
            Intent intent = new Intent(HomeActivity.this, RequestBookActivity.class);
            intent.putExtra("content_type", "Books");
            startActivity(intent);
        } else if (name.equals(getString(R.string.leaderBoard))) {
            Intent intent = new Intent(HomeActivity.this, LeaderBoardActivity.class);
            startActivity(intent);
        } else if (name.equals(getString(R.string.my_inbox))) {
            Intent intent = new Intent(HomeActivity.this, MessageActivity.class);
            startActivity(intent);
        } else if (name.equals(getString(R.string.group_chat))) {
            Intent intent = new Intent(this, MessageActivity.class);
            startActivity(intent);
        }
    }

    @SuppressLint("HardwareIds")
    public void getAccountDetailsRequest() {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_MAIN_PAGE_DETAILS_FOR_STARTUP);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.TOKEN, SharedPreferencesHelper.getToken(this));
            try {
                jsonObject.put(Constants.Network.DEVICE_ID, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
                jsonObject.put(Constants.Network.APP_VERSION, String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode));
            } catch (Exception e) {
                Logger.error(TAG, e.getMessage());
            }

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.HOMEPAGE_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Logger.info(TAG, response.toString());

                                if (response.getString(Constants.Network.CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONObject data = response.getJSONObject(Constants.Network.DATA);

                                    User user = User.getCurrentUser(HomeActivity.this);
                                    user.setProfilePic(data.getString(Constants.Network.PROFILE_PIC));
                                    user.setDesignation(data.getString(Constants.Network.DESIGNATION));
                                    user.setCreatedContents(data.getString(Constants.Network.CONTENT_CREATED_COUNT));
                                    user.setConsumedContents(data.getString(Constants.Network.CONTENT_CONSUMED_COUNT));
                                    if (data.has(Constants.Network.USER_TOTAL_POINTS)) {
                                        String points = data.getString(Constants.Network.USER_TOTAL_POINTS);
                                        user.setPoints(points);
                                    }
                                    user.setRank(data.getString(Constants.Network.USER_RANK));
                                    user.setFullName(data.getString(Constants.Network.USER_FULL_NAME));
                                    User.setAsCurrentUser(HomeActivity.this, user);

                                    JSONArray featuresArray = data.getJSONArray(Constants.Network.FEATURES);

                                    for (int i = 0; i < featuresArray.length(); i++) {
                                        JSONObject jsonObject1 = featuresArray.getJSONObject(i);
                                        CompanyFeature.reset(HomeActivity.this);
                                        CompanyFeature.saveFeature(HomeActivity.this, new CompanyFeature(jsonObject1.getString(Constants.Network.FEATURE_ID), jsonObject1.getString(Constants.Network.VALUE)));
                                    }

                                    notificationCount = data.getString(Constants.Network.COUNT_NOTIFICATION);
                                    messageCount = data.getString(Constants.Network.COUNT_MESSAGE);

                                    loadToolbarIconDetails();
                                    setNotificationAndMessageCount();
                                    setNavigationHeader(user);

                                } else if (response.getString(Constants.Network.CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(HomeActivity.this);
                                } else {
                                    Toast.makeText(HomeActivity.this, R.string.check_your_network_connection, Toast.LENGTH_SHORT).show();
                                    Logger.error(TAG, "400");
                                }
                            } catch (JSONException e) {
                                //                     Toast.makeText(mContext, "JsonExces_"+e.getMessage(), Toast.LENGTH_SHORT).show();
                                Logger.error(TAG, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  Logger.error(TAG, error.get());
                            //                   Toast.makeText(mContext, "volleyError_"+error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

            VolleySingleton.getInstance(HomeActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            //    Toast.makeText(mContext, "volleyException_"+e.getMessage(), Toast.LENGTH_SHORT).show();
            Logger.error(TAG, e.getMessage());
        }
    }

    private void setNavigationHeader(User user) {
        user_name_tv.setText(user.getFullName());
        designation_tv.setText(user.getDesignation());
        rank_tv.setText("Rank - " + (user.getRank()));
        earned_tv.setText("Points Earned - " + (user.getPoints()));
        content_created_tv.setText(user.getCreatedContents());
        content_consume_tv.setText(user.getConsumedContents());
        Picasso.get()
                .load(Urls.AWS_PROFILE_IMAGE_PATH + user.getProfilePic())
                .error(R.drawable.profile)
                .placeholder(R.drawable.profile)
                .into(user_imageView);
    }

    private void setNotificationAndMessageCount() {
        if (notificationCount == null || notificationCount.equals("0") || notificationCount.equals("null")) {
            home_notification_count.setVisibility(View.GONE);
        } else {
            home_notification_count.setVisibility(View.VISIBLE);
            home_notification_count.setText(notificationCount);
        }

        if (messageCount == null || messageCount.equals("0") || messageCount.equals("null"))
            findViewById(R.id.home_message_count).setVisibility(View.GONE);
        else {
            findViewById(R.id.home_message_count).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.home_message_count)).setText(messageCount);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag("Home");
        if (currentFragment != null && currentFragment.isVisible() && currentFragment instanceof HomeFragment) {
            //DO STUFF
            HomeFragment homeFragment = (HomeFragment) currentFragment;
            boolean check = homeFragment.scrollView();
            if (check) {
                super.onBackPressed();
            } else {
                if (mHomeBottomNav.getSelectedItemId() == R.id.bottom_home) {
                    super.onBackPressed();
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, new HomeFragment(), "Home").commit();
                    mHomeBottomNav.setSelectedItemId(R.id.bottom_home);
                }
            }

        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_fragment_layout, new HomeFragment(), "Home").commit();
            mHomeBottomNav.setSelectedItemId(R.id.bottom_home);
        }
    }

    public void logoutApp() {
        sendLogoutRequest(mContext);
    }

    @SuppressLint("HardwareIds")
    public void sendLogoutRequest(final Context context) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_LOGOUT_APP);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(context).getUserId());

            try {
                jsonObject.put(Constants.Network.DEVICE_ID, Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
                jsonObject.put(Constants.Network.APP_VERSION, String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode));
            } catch (Exception e) {
                Logger.error("LOGOUT_API", e.getMessage());
            }

            Logger.info("LOGOUT_API", jsonObject.toString());
            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(context, Request.Method.POST, Urls.EXTRA_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Logger.info("LOGOUT_API", response.toString());

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    Logger.info("LOGOUT_API", "Logged Out successfully");

                                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    User.removeCurrentUser(mContext);
                                    finish();
                                    Toast.makeText(mContext, R.string.logged_out_successfully, Toast.LENGTH_SHORT).show();


                                } else {
                                    Logger.error("LOGOUT_API", "400");
                                }
                            } catch (JSONException e) {
                                Logger.error("LOGOUT_API", e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  Logger.error(TAG, error.get());
                        }
                    });

            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error("LOGOUT_API", e.getMessage());
        }
    }
}
