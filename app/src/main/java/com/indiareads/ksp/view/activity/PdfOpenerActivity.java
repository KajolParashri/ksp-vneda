package com.indiareads.ksp.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.onEBookLoad;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.PDFLoader;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class PdfOpenerActivity extends AppCompatActivity implements OnPageChangeListener, OnLoadCompleteListener {
    PDFView pdfView;
    private static final String TAG = ViewEbookActivity.class.getSimpleName();
    String dayNightSwitch = "true";
    private String mUrl;
    private String mTitle;
    private Handler mHandler = new Handler();
    private Toolbar toolbar;
    ProgressBar progressBar;
    String url = "";
    int page = 0;

    TextView percentage;
    Button continueRead;
    String pageNumber;
    String contentId;
    String contentStatusType;
    String contentType;
    private boolean paused = false;
    private long onScreenTimeInMilliseconds = 0;
    private Handler customHandler = new Handler();
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            onScreenTimeInMilliseconds = onScreenTimeInMilliseconds + 1000;
            if (!paused) {
                customHandler.postDelayed(this, 1000);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_opener);

        getDataFromIntent();
        setupToolbar();
        initViews();

        if (checkIfAlreadyhavePermission()) {
            setWebView(mUrl);
        } else
            requestForSpecificPermission();

    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkIfAlreadyhavePermission() {
        int permission_read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permission_write = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return ((permission_read) == PackageManager.PERMISSION_GRANTED && permission_write == PackageManager.PERMISSION_GRANTED);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.activity_view_ebook_toolbar);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dayNightSwitch.equals("true")) {
                    pdfView.setNightMode(true);
                    pdfView.useBestQuality(true);
                    pdfView.loadPages();
                    dayNightSwitch = "false";
                } else if (dayNightSwitch.equals("false")) {
                    pdfView.setNightMode(false);
                    pdfView.loadPages();
                    dayNightSwitch = "true";
                }
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mTitle);
    }

    private void setWebView(String pdfUrl) {

        new PDFLoader(this).initFromNetwork(pdfUrl, new onEBookLoad() {

            @Override
            public void resultData(Bitmap data) {
                Logger.info("PDFLoad", "result data");
            }

            @Override
            public void progressData(int progress) {
                continueRead.setEnabled(false);
                Logger.info("PDFLoad", "progress" + progress);
                percentage.setText("" + progress + "%");
            }

            @Override
            public void failed(Throwable t) {
                Logger.info("PDFLoad", "failed:" + t.getMessage());
            }


            @Override
            public void complete() {
                Logger.info("PDFLoad", "complete");
            }

            @Override
            public void onDownloadComplete(File file) {
                continueRead.setEnabled(true);
                page = Integer.valueOf(pageNumber);
                percentage.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                pdfView.fromFile(file)
                        // .defaultPage(page)
                        .onPageChange(PdfOpenerActivity.this)
                        .enableAnnotationRendering(true)
                        .onLoad(PdfOpenerActivity.this)

                        .scrollHandle(new DefaultScrollHandle(PdfOpenerActivity.this))
                        .load();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                pageNumber = String.valueOf(pdfView.getCurrentPage());
                sendCountPage();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setWebView(mUrl);
            } else {
                Toast.makeText(this, R.string.please_allow_permission_to_view_ebook, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onPageChanged(int page, int pageCount) {

    }

    @Override
    public void loadComplete(int nbPages) {

    }

    @SuppressLint("JavascriptInterface")
    private void initViews() {
        continueRead = findViewById(R.id.continueRead);

        pdfView = findViewById(R.id.pdfView);
        pdfView.setBackgroundColor(0);
        progressBar = findViewById(R.id.progressBar);
        percentage = findViewById(R.id.percentage);
        continueRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueRead.setVisibility(View.GONE);
                pdfView.jumpTo(Integer.parseInt(pageNumber), true);
            }
        });

        if (pageNumber.equals("0")) {
            continueRead.setVisibility(View.GONE);
        }

        Toolbar mToolbar = findViewById(R.id.activity_view_ebook_toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getIntent().getStringExtra(Content.TITLE));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void sendPostConsumedRequest() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_POST_CONSUMED);
            jsonObject.put(Constants.Network.CONTENT_ID, contentId);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, contentStatusType);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, contentType);
            jsonObject.put(Constants.Network.TOTAL_TIME, (int) (onScreenTimeInMilliseconds / 1000));

            Logger.info("HITCONSUME", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.info("HITCONSUME", response.toString());
                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(PdfOpenerActivity.this);
                                }

                            } catch (Exception e) {
                                Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "sendPostConsumedRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(PdfOpenerActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        paused = false;
        customHandler.postDelayed(updateTimerThread, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        paused = true;
        customHandler.removeCallbacks(updateTimerThread);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        pageNumber = String.valueOf(pdfView.getCurrentPage());
        sendCountPage();
    }

    private void sendCountPage() {
        try {

            findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.SAVE_BOOK_PAGE);
            jsonObject.put(Constants.Network.CONTENT_ID, contentId);
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, contentStatusType);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.PAGE, pageNumber);

            Logger.info("HITCONSUME", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            findViewById(R.id.progressBar).setVisibility(View.GONE);
                            Logger.info("HITCONSUME", response.toString());
                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    finish();
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(PdfOpenerActivity.this);
                                }

                            } catch (Exception e) {
                                findViewById(R.id.progressBar).setVisibility(View.GONE);
                                Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                        "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                            } finally {
                                finish();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            findViewById(R.id.progressBar).setVisibility(View.GONE);
                            Error.generateError(VolleyError.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                                    "sendPostConsumedRequest", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                            finish();
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(PdfOpenerActivity.this).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            Error.generateError(JSONException.class.getSimpleName(), ProductActivity.class.getSimpleName(),
                    "sendPostConsumedRequest", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }


    private void getDataFromIntent() {
        Intent intent = getIntent();
        mUrl = getIntent().getStringExtra(Content.DATA);
        Logger.error("URLFIND", mUrl);
        mTitle = getIntent().getStringExtra(Content.TITLE);

        pageNumber = intent.getStringExtra(Constants.Network.PAGE_NO);//, last_page);
        contentId = intent.getStringExtra(Constants.Network.CONTENT_ID);//, mContentId);
        contentStatusType = intent.getStringExtra(Constants.Network.CONTENT_STATUS_TYPE);//, mContentStatusType);
        contentType = intent.getStringExtra(Constants.Network.CONTENT_TYPE);//, mContentType);
    }

    @Override
    protected void onDestroy() {
        sendPostConsumedRequest();
        super.onDestroy();
    }

}