package com.indiareads.ksp.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.MyOrdersAdapter;
import com.indiareads.ksp.listeners.OnOrderClickListener;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.Order;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SharedPreferencesHelper;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.SelectAddressActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.displayToast;
import static com.indiareads.ksp.utils.Constants.Order.CANCELLED;
import static com.indiareads.ksp.utils.Constants.Order.DELIVERED;
import static com.indiareads.ksp.utils.Constants.Order.ORDER_PLACED;
import static com.indiareads.ksp.utils.Constants.Order.ORDER_PROCESSING;
import static com.indiareads.ksp.utils.Constants.RETURN_BOOK_ADDRESS_CODE;
import static com.indiareads.ksp.utils.Constants.RETURN_ORDER_SELECT_ADDRESS;

public class MyOrdersFragment extends Fragment {

    private static final String TAG = MyOrdersFragment.class.getSimpleName();
    private View mMainView;
    private RecyclerView mOrdersRv;
    private MyOrdersAdapter adapter;
    private List<Order> mOrdersList = new ArrayList<>();

    public MyOrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Logger.info(TAG, "OnCreateView");
        mMainView = inflater.inflate(R.layout.fragment_my_orders, container, false);
        initViews();
        initRecyclerView();
        getOrderList();
        return mMainView;
    }

    private void initViews() {
        mOrdersRv = mMainView.findViewById(R.id.my_orders_rv);
    }

    private void initRecyclerView() {
        mOrdersList = new ArrayList<>();
        mOrdersRv.setLayoutManager(new LinearLayoutManager(getContext()));
        OnOrderClickListener mListener = new OnOrderClickListener() {
            @Override
            public void OnReturnClick(int position) {

            }

            @Override
            public void OnCancelClick(int position) {
                Order mOrder = mOrdersList.get(position);
                Logger.error("OrderDetail", mOrder.toString());


                if (mOrder.getOrder_status() != null && mOrder.getOrder_status().equals(ORDER_PROCESSING) || mOrder.getOrder_status().equals(ORDER_PLACED)) {

                    cancelOrder(position);

                } else if (mOrder.getOrder_status() != null && mOrder.getOrder_status().equals(DELIVERED)) {

                    Intent orderIntent = new Intent(getActivity(), SelectAddressActivity.class);
                    orderIntent.putExtra("content_id", mOrder.getContent_id());
                    orderIntent.putExtra("isbn_no", "");
                    orderIntent.putExtra("type", RETURN_ORDER_SELECT_ADDRESS);
                    orderIntent.putExtra("position", position); //Do not change
                    orderIntent.putExtra("orderId", mOrder.getOrder_id());
                    startActivityForResult(orderIntent, RETURN_BOOK_ADDRESS_CODE);
                }
            }
        };
        adapter = new MyOrdersAdapter(getActivity(), mOrdersList, mListener);
        mOrdersRv.setAdapter(adapter);
    }

    private String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private void cancelOrder(final int position) {
        try {
            final Order order = mOrdersList.get(position);
            JSONObject jsonObject = new JSONObject();
            // Logger.info("CancelOrder", jsonObject.toString() + " is response");
            jsonObject.put("action", "cancelorder");
            jsonObject.put("user_id", SharedPreferencesHelper.getCurrentUser(getContext()).getUserId());
            jsonObject.put("content_id", order.getContent_id());
            jsonObject.put("library_id", order.getLibrary_id());
            jsonObject.put("order_id", order.getOrder_id());

            Logger.info("CancelOrder", jsonObject.toString() + " is response");
            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.ORDER_PLACE, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {


                        if (response.getString(Constants.Network.RESPONSE_CODE)
                                .equals(Constants.Network.RESPONSE_OK)) {

                            Logger.info("CancelOrder", response.toString() + " is response");
                            displayToast(getContext(), "Order Cancelled !");
                            order.setOrder_status(CANCELLED);

                            order.setOrder_date(getCurrentDateTime());
                            mOrdersList.set(position, order);
                            adapter.notifyDataSetChanged();
                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), MyOrdersFragment.class.getSimpleName(),
                                    "cancelOrder", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));
                            displayCancelReturnError(position);
                        }
                    } catch (JSONException e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), MyOrdersFragment.class.getSimpleName(),
                                "cancelOrder", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        displayCancelReturnError(position);
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.info("CancelOrder", "ServcerError" + " is response");
                    Error error1 = Error.generateError(VolleyError.class.getSimpleName(), MyOrdersFragment.class.getSimpleName(),
                            "cancelOrder", Error.getErrorTypeFromVolleyError(error), error.getMessage());
                    displayCancelReturnError(position);
                }
            });
            jsonObjectRequest.setTag(TAG);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Error error1 = Error.generateError(VolleyError.class.getSimpleName(), MyOrdersFragment.class.getSimpleName(),
                    "cancelOrder", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            displayCancelReturnError(position);
            e.printStackTrace();
        }
    }

    private void getOrderList() {
        try {

            CommonMethods.showProgressBar(mMainView, R.id.order_frag_frame_lay);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("action", "load_myorders");
            jsonObject.put("user_id", SharedPreferencesHelper.getCurrentUser(getContext()).getUserId());

            Logger.info("OrdersList", jsonObject.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.USER, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.info("OrdersList", response.toString());
                        if (response.getString(Constants.Network.RESPONSE_CODE)
                                .equals(Constants.Network.RESPONSE_OK)) {
                            JSONArray jsonArray = response.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Order order = new Order();
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                order.setContent_id(jsonObject1.getString("content_id") + "");
                                order.setContent_type(jsonObject1.getString("content_type") + "");
                                order.setTitle(jsonObject1.getString("title") + "");
                                order.setDescription(jsonObject1.getString("description") + "");
                                order.setThumnail(jsonObject1.getString("thumnail") + "");
                                order.setSource(jsonObject1.getString("source") + "");
                                order.setCat_id(jsonObject1.getString("cat_id") + "");
                                order.setStatus(jsonObject1.getString("status") + "");
                                order.setOrder_id(jsonObject1.getString("order_id") + "");
                                order.setUser_id(jsonObject1.getString("user_id") + "");
                                order.setLibrary_id(jsonObject1.getString("library_id") + "");
                                order.setOrder_status(jsonObject1.getString("order_status") + "");
                                order.setInventory_id(jsonObject1.getString("inventory_id") + "");
                                order.setTracking_id(jsonObject1.getString("tracking_id") + "");
                                order.setTracking_link(jsonObject1.getString("tracking_link") + "");
                                order.setCourier_vendor(jsonObject1.getString("courier_vendor") + "");
                                order.setOrder_date(jsonObject1.getString("order_date") + "");
                                order.setOrder_comments(jsonObject1.getString("order_comments") + "");
                                order.setDelivery_address_id(jsonObject1.getString("delivery_address_id") + "");
                                order.setPickup_address_id(jsonObject1.getString("pickup_address_id") + "");
                                order.setAddress_book_id(jsonObject1.getString("address_book_id") + "");
                                order.setAddress_type(jsonObject1.getString("address_type") + "");
                                order.setFullname(jsonObject1.getString("fullname") + "");
                                order.setAddress_line1(jsonObject1.getString("address_line1") + "");
                                order.setAddress_line2(jsonObject1.getString("address_line2") + "");
                                order.setCity(jsonObject1.getString("city") + "");
                                order.setState(jsonObject1.getString("state") + "");
                                order.setPincode(jsonObject1.getString("pincode") + "");
                                order.setPhone(jsonObject1.getString("phone") + "");
                                mOrdersList.add(order);
                            }
                            adapter.notifyDataSetChanged();
                            CommonMethods.hideProgressBar(mMainView, R.id.order_frag_frame_lay);
                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            Error error = Error.generateError(HttpResponse.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                                    "getOrderList", Error.ERROR_TYPE_SERVER_ERROR, response.getString(Constants.Network.RESPONSE_MESSAGE));
                            CommonMethods.hideProgressBar(mMainView, R.id.order_frag_frame_lay);
                            showEmptyView();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Error error = Error.generateError(JSONException.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                                "getOrderList", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                        CommonMethods.hideProgressBar(mMainView, R.id.order_frag_frame_lay);
                        displayErrorUI(error);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Error error1 = Error.generateError(VolleyError.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                            "getThankYouMessage", Error.getErrorTypeFromVolleyError(error), error.getMessage());
                    displayToast(getContext(), "Error contacting servers");
                    CommonMethods.hideProgressBar(mMainView, R.id.order_frag_frame_lay);
                    displayErrorUI(error1);
                }
            });
            jsonObjectRequest.setTag(TAG);
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Error error1 = Error.generateError(VolleyError.class.getSimpleName(), SelectAddressActivity.class.getSimpleName(),
                    "getThankYouMessage", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

            CommonMethods.hideProgressBar(mMainView, R.id.order_frag_frame_lay);
            displayErrorUI(error1);
        }
    }

    private void showEmptyView() {
        Order emptyContent = new Order();
        emptyContent.setThumnail(String.valueOf(Constants.Content.EMPTY_CONTENT_LIST_ICON));
        emptyContent.setTitle(getString(R.string.content_list_empty));
        emptyContent.setContent_type(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR));
        mOrdersList.add(emptyContent);

        mOrdersRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter.notifyItemInserted(0);
    }

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getActivity()).cancelRequests(TAG);
    }


    private void displayErrorUI(Error error) {
        CommonMethods.showErrorViewNoContent(getActivity(), R.id.order_frag_frame_lay, error);
    }

    private void displayCancelReturnError(int position) {
        displayToast(getContext(), "An unexpected error occurred, please try again.");
        adapter.notifyItemChanged(position);
    }

}
