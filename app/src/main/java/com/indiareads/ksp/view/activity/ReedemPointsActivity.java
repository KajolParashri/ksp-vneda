package com.indiareads.ksp.view.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.BreakageAdapter;
import com.indiareads.ksp.adapters.CouponRecycler;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.CouponModel;
import com.indiareads.ksp.model.HistoryModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CircleTransform;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SharedPreferencesHelper;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReedemPointsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<CouponModel> arrayList;
    NestedScrollView nestedScoller;
    ImageView backBtn;
    ImageView user_profile_imageview;
    TextView pointsTotal, breakrageText, user_name, user_designation, activity_user_details_rank;
    ArrayList<HistoryModel> breakageList;
    TextView availablePoints;
    int remaining_Points, totalPoints;

    //REWARDS_API
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reedem_points);

        init();
        setUserDetails();
        getAccountDetailsRequest();
        getCouponList();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void showBreakage() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.breakage_layout, null);
        dialogBuilder.setView(dialogView);
        TextView totalPoint = dialogView.findViewById(R.id.totalPoints);
        totalPoint.setText("Total Points : " + totalPoints);
        final AlertDialog alertDialog = dialogBuilder.create();
        ImageView close = dialogView.findViewById(R.id.close);

        TextView remainingPoints = dialogView.findViewById(R.id.remainingPoints);

        remainingPoints.setText("Total Remaining : " + remaining_Points);
        RecyclerView couponHistory = dialogView.findViewById(R.id.couponHistory);

        couponHistory.setLayoutManager(new LinearLayoutManager(this));

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        BreakageAdapter breakageAdapter = new BreakageAdapter(this, breakageList);
        couponHistory.setAdapter(breakageAdapter);

        alertDialog.show();
    }


    public void getAccountDetailsRequest() {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_GET_MAIN_PAGE_DETAILS_FOR_STARTUP);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.TOKEN, SharedPreferencesHelper.getToken(this));
            try {
                jsonObject.put(Constants.Network.DEVICE_ID, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
                jsonObject.put(Constants.Network.APP_VERSION, String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode));
            } catch (Exception e) {
                Logger.error("TAG", e.getMessage());
            }

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.HOMEPAGE_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Logger.info("TAG", response.toString());

                                if (response.getString(Constants.Network.CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    JSONObject data = response.getJSONObject(Constants.Network.DATA);

                                    activity_user_details_rank.setText(data.getString(Constants.Network.USER_RANK));
                                    nestedScoller.fullScroll(NestedScrollView.FOCUS_UP);
                                    nestedScoller.smoothScrollTo(0, 0);

                                } else if (response.getString(Constants.Network.CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ReedemPointsActivity.this);
                                } else {
                                    Logger.error("TAG", "400");
                                }
                            } catch (JSONException e) {
                                Logger.error("TAG", e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Logger.error("TAG", error.getMessage());
                        }
                    });

            VolleySingleton.getInstance(ReedemPointsActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error("TAG", e.getMessage());
        }
    }

    public void getCouponList() {
        CommonMethods.showProgressBar(this, R.id.activity_redeem_points_root_layout);
        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.DISPLAY_PARTNERS);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(this).getUserId());
            jsonObject.put(Constants.Network.COMPANY_ID, User.getCurrentUser(this).getCompanyId());

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(this, Request.Method.POST, Urls.REWARDS_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {


                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    arrayList = new ArrayList<>();
                                    breakageList = new ArrayList<>();
                                    JSONObject data = response.getJSONObject(Constants.Network.DATA);

                                    JSONArray partners = data.getJSONArray("partners");
                                    Logger.info("TAG", partners.toString());
                                    JSONObject points = data.getJSONObject("points");
                                    JSONArray breakage = points.getJSONArray("breakage");
                                    remaining_Points = points.getInt("available_points");

                                    totalPoints = points.getInt("total_points");
                                    pointsTotal.setText("" + totalPoints);

                                    availablePoints.setText("" + remaining_Points);

                                    for (int i = 0; i < breakage.length(); i++) {

                                        JSONObject jsonObject1 = breakage.getJSONObject(i);
                                        HistoryModel historyModel = new HistoryModel();
                                        historyModel.setBurnedCoins(jsonObject1.getString("no_of_points"));
                                        historyModel.setDealId(jsonObject1.getString("created_date"));
                                        historyModel.setHistoryTitle(jsonObject1.getString("deal_title"));
                                        breakageList.add(historyModel);
                                    }


                                    for (int i = 0; i < partners.length(); i++) {
                                        JSONObject coupon = partners.getJSONObject(i);
                                        String partner_id = coupon.getString("partner_id");
                                        String image_url = coupon.getString("image_url");
                                        CouponModel couponModel = new CouponModel();
                                        couponModel.setCouponId(partner_id);
                                        couponModel.setCouponImage(image_url);
                                        arrayList.add(couponModel);
                                    }
                                    setAdapter();
                                    CommonMethods.hideProgressBar(ReedemPointsActivity.this, R.id.activity_redeem_points_root_layout);
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout(ReedemPointsActivity.this);
                                } else {
                                    Logger.error("TAG", "400");
                                }
                            } catch (JSONException e) {

                                Logger.error("TAG", e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Logger.error("TAG", "responseError");
                        }
                    });

            VolleySingleton.getInstance(ReedemPointsActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error("TAG", e.getMessage());
        }
    }

    public void setUserDetails() {
        Picasso.get().load(Urls.AWS_PROFILE_IMAGE_PATH + User.getCurrentUser(this)
                .getProfilePic()).error(R.mipmap.ic_profile).placeholder(R.mipmap.ic_profile)
                .transform(new CircleTransform()).into(user_profile_imageview);

        user_name.setText(User.getCurrentUser(this).getFullName());
        user_designation.setText(User.getCurrentUser(this).getDesignation());
        activity_user_details_rank.setText(User.getCurrentUser(this).getRank());
        pointsTotal.setText(User.getCurrentUser(this).getPoints());
    }


    public void init() {
        availablePoints = findViewById(R.id.availablePoints);
        backBtn = findViewById(R.id.backBtn);
        breakrageText = findViewById(R.id.breakrageText);

        user_profile_imageview = findViewById(R.id.user_profile_imageview);
        pointsTotal = findViewById(R.id.pointsTotal);
        user_name = findViewById(R.id.user_name);

        user_designation = findViewById(R.id.user_designation);
        activity_user_details_rank = findViewById(R.id.activity_user_details_rank);
        nestedScoller = findViewById(R.id.nestedScoller);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        breakrageText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBreakage();
            }
        });
    }

    public void setAdapter() {
        CouponRecycler couponRecycler = new CouponRecycler(this, arrayList, new OnRecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                String pointsTotal = availablePoints.getText().toString();
                Intent intent = new Intent(ReedemPointsActivity.this, RedeemDetailsActivity.class);
                intent.putExtra("partner_id", arrayList.get(position).getCouponId());
                intent.putExtra("pointsTotal", pointsTotal);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(couponRecycler);
    }
}
