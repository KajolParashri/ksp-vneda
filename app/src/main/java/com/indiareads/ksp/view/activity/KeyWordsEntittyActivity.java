package com.indiareads.ksp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.KeywordsAdapter;
import com.indiareads.ksp.adapters.SelectedKeywordsAdapter;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.KeywordsModel;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.utils.AutoFitLayoutManger;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.ItemOffsetDecoration;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class KeyWordsEntittyActivity extends AppCompatActivity {
    private static final String TAG = KeyWordsEntittyActivity.class.getSimpleName();
    SelectedKeywordsAdapter selectedKeywordsAdapter;
    String suggestionQuery;
    EditText key_words;
    LinearLayoutManager linearLayoutManager;
    KeywordsAdapter keywordsAdapter;
    RecyclerView recyclerViewSuggestion, selectedKeyword;

    ArrayList<KeywordsModel> selected_list;
    ArrayList<KeywordsModel> Clickselected_list;
    ArrayList<KeywordsModel> newSlectedList;
    int pageNumber = 1;
    String publicQuery = "";
    private boolean doPagination = true;
    private boolean mContentsLoading = true;
    ArrayList<KeywordsModel> suggestionList;
    String content_id = "";
    String content_type = "";

    ProgressBar progressBar;
    Button submit_btn;
    TextView manualtext;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_words_entitty);

        Intent inten = getIntent();

        if (inten.hasExtra("content_type")) {
            content_id = inten.getStringExtra("content_id");
            content_type = inten.getStringExtra("content_type");
        }

        suggestionList = new ArrayList<>();
        selected_list = new ArrayList<>();
        Clickselected_list = new ArrayList<>();

        manualtext = findViewById(R.id.manualtext);
        key_words = findViewById(R.id.key_words);
        manualtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String manualKeyWord = manualtext.getText().toString();
                if (!manualKeyWord.trim().equals(""))
                    keywordAdd(manualKeyWord);
                selectedKeywordsAdapter.notifyDataSetChanged();
                manualtext.setText("");
                key_words.setText("");
            }

        });
        submit_btn = findViewById(R.id.submit_btn);
        progressBar = findViewById(R.id.progressBar);
        recyclerViewSuggestion = findViewById(R.id.recyclerView);
        selectedKeyword = findViewById(R.id.selectedKeyword);

        manualtext.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        int spaceInPixels = 3;

        selectedKeyword.setLayoutManager(new GridLayoutManager(KeyWordsEntittyActivity.this, 3));
        selectedKeyword.addItemDecoration(new ItemOffsetDecoration(spaceInPixels, true));

        linearLayoutManager = new LinearLayoutManager(KeyWordsEntittyActivity.this);
        recyclerViewSuggestion.setLayoutManager(linearLayoutManager);

        recyclerViewSuggestion.setVisibility(View.GONE);

        setAdapterNew();


        key_words.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    manualtext.setVisibility(View.VISIBLE);
                    manualtext.setText(charSequence.toString());
                    suggestionQuery = charSequence.toString();
                    recyclerViewSuggestion.setVisibility(View.VISIBLE);
                    publicQuery = charSequence.toString();
                    getKeyWords(charSequence.toString());

                } else {
                    manualtext.setVisibility(View.GONE);
                    recyclerViewSuggestion.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        newSlectedList = new ArrayList<>();
        key_words.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    String name = key_words.getText().toString();
                    if (!name.equals("")) {
                        KeywordsModel keywordsModel = new KeywordsModel();
                        keywordsModel.set_name(name);
                        selected_list.add(keywordsModel);
                        newSlectedList.add(keywordsModel);
                        selectedKeywordsAdapter.notifyItemChanged(selected_list.size() - 1);
                        key_words.setText("");
                        handled = true;
                    }
                }
                return handled;
            }
        });
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    pushKeyWords();
                } catch (Exception e) {

                }
            }
        });

    }

    public void suggestAdapterSet() {
        keywordsAdapter = new KeywordsAdapter(KeyWordsEntittyActivity.this, suggestionList, new OnItemClick() {
            @Override
            public void onArticleClick(String tileName) {

            }

            @Override
            public void onInfoClick(String tileName) {

            }

            @Override
            public void onCategoryClick(int pos) {
                try {
                    setClickItem(pos);
                    recyclerViewSuggestion.setVisibility(View.GONE);
                    key_words.setText("");
                } catch (Exception e) {
                    Logger.error(TAG, e.getMessage());
                }
            }
        });

        recyclerViewSuggestion.setAdapter(keywordsAdapter);
    }

    public void setClickItem(int pos) {

        String item = suggestionList.get(pos).get_name();
        KeywordsModel keywordsModel = suggestionList.get(pos);
        selected_list.add(keywordsModel);
        Clickselected_list.add(keywordsModel);
        selectedKeywordsAdapter.notifyItemChanged(pos);
        suggestionList = new ArrayList<>();
    }

    public void getKeyWords(String query) {
        suggestionList = new ArrayList<>();
        try {

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.GET,
                    Urls.getKeywordsApi + "q=" + query + "&page=" + pageNumber, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        JSONArray jsonArray = response.getJSONArray(Constants.Network.RESULTS);
                        //   if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject data = jsonArray.getJSONObject(i);
                            KeywordsModel keywordsModel = new KeywordsModel();
                            String _id = data.getString(Constants.Network.ID);
                            String _name = data.getString(Constants.Network.TEXT);
                            keywordsModel.set_id(_id);
                            keywordsModel.set_name(_name);
                            keywordsModel.setContentType(Constants.Network.DATA);
                            suggestionList.add(keywordsModel);

                        }

                        suggestAdapterSet();
                        setContentRecyclerViewScrollListener();

                    } catch (Exception e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                                "createArticle", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                            "createArticle", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });
            VolleySingleton.getInstance(KeyWordsEntittyActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                    "createArticle", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }

    }

    public void getKeyWordsPagination(String query) {
        try {

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.GET,
                    Urls.getKeywordsApi + "q=" + query + "&page=" + pageNumber, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        JSONArray jsonArray = response.getJSONArray(Constants.Network.RESULTS);
                        //   if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject data = jsonArray.getJSONObject(i);
                            KeywordsModel keywordsModel = new KeywordsModel();
                            String _id = data.getString(Constants.Network.ID);
                            String _name = data.getString(Constants.Network.TEXT);
                            keywordsModel.set_id(_id);
                            keywordsModel.set_name(_name);
                            keywordsModel.setContentType(Constants.Network.DATA);
                            suggestionList.add(keywordsModel);

                        }

                        keywordsAdapter.notifyItemRangeInserted(suggestionList.size() - 5, 5);

                    } catch (Exception e) {
                        Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                                "createArticle", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                            "createArticle", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });
            VolleySingleton.getInstance(KeyWordsEntittyActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                    "createArticle", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }

    }

    public JSONArray getExistingData() {
        JSONArray jsonArray = new JSONArray();
        for (KeywordsModel keywordsModel : Clickselected_list) {
            jsonArray.put(keywordsModel._id);
        }

        return jsonArray;
    }

    public JSONArray getSelectedData() {
        JSONArray jsonArray = new JSONArray();
        for (KeywordsModel keywordsModel : newSlectedList) {
            jsonArray.put(keywordsModel._name);
        }

        return jsonArray;
    }

    public void pushKeyWords() {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ADD_TAGS_CREATE);
            jsonObject.put(Constants.Network.EXISTING_ARRAY, getExistingData());
            jsonObject.put(Constants.Network.CONTENT_ID, content_id);
            jsonObject.put(Constants.Network.NEW_ARRAY, getSelectedData());


            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(this, Request.Method.POST,
                    Urls.CREATE_API, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        String responseStr = response.getString("response_message");

                        if (responseStr.equalsIgnoreCase("Success")) {

                            Intent productIntent = new Intent(KeyWordsEntittyActivity.this, ProductActivity.class);
                            productIntent.putExtra(Constants.Network.CONTENT_ID, content_id);
                            productIntent.putExtra(Constants.Network.CONTENT_TYPE, content_type);
                            productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);
                            startActivity(productIntent);
                            finish();

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(KeyWordsEntittyActivity.this);
                        }

                    } catch (Exception e) {
                        progressBar.setVisibility(View.GONE);
                        Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                                "createArticle", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressBar.setVisibility(View.GONE);
                    Error error = Error.generateError(VolleyError.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                            "createArticle", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                }
            });
            VolleySingleton.getInstance(KeyWordsEntittyActivity.this).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            Error error = Error.generateError(JSONException.class.getSimpleName(), CreateContentActivity.class.getSimpleName(),
                    "createArticle", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }

    }

    private void setContentRecyclerViewScrollListener() {
        try {
            recyclerViewSuggestion.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (doPagination && dy > 0) //check for scroll down
                    {
                        int visibleItemCount, totalItemCount, pastVisiblesItems;
                        visibleItemCount = linearLayoutManager.getChildCount();
                        totalItemCount = linearLayoutManager.getItemCount();
                        pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                        if (mContentsLoading) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                                mContentsLoading = false;
                                keywordsAdapter.notifyItemInserted(suggestionList.size() - 1);
                                pageNumber++;
                                getKeyWordsPagination(publicQuery);

                            }
                        }
                    }
                }

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }
            });

        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAdapterNew() {
        selectedKeywordsAdapter = new SelectedKeywordsAdapter(KeyWordsEntittyActivity.this, selected_list, new OnItemClick() {
            @Override
            public void onArticleClick(String tileName) {

            }

            @Override
            public void onInfoClick(String tileName) {

            }

            @Override
            public void onCategoryClick(int pos) {

                int remaingSize = selected_list.size() - 1;

                if (pos <= remaingSize) {
                    selected_list.remove(pos);
                    selectedKeywordsAdapter.notifyItemRemoved(pos);
                }

            }
        });

        selectedKeyword.setAdapter(selectedKeywordsAdapter);
    }

    public void keywordAdd(String addKey) {
        KeywordsModel keywordsModel = new KeywordsModel();
        keywordsModel.set_name(addKey);
        selected_list.add(keywordsModel);
    }
}
