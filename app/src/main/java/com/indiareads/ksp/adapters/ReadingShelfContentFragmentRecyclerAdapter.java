package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnOrderClickListener;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.viewholder.ContentViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;

import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;

public class ReadingShelfContentFragmentRecyclerAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<Content> mContents;
    private OnRecyclerItemClickListener mOnClickListener;
    OnOrderClickListener onOrderClickListener;

    public ReadingShelfContentFragmentRecyclerAdapter(Context mContext, List<Content> mContents, OnOrderClickListener onOrderClickListener) {
        this.mContext = mContext;
        this.mContents = mContents;
        this.onOrderClickListener = onOrderClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_progress_loader, parent, false);
                return new ProgressBarViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.linearlisting_categoryt, parent, false);
                return new ContentViewHolder(view, mOnClickListener);
        }

    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        final Content content = mContents.get(position);

        if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {

            ProgressBarViewHolder progressBarViewHolder = (ProgressBarViewHolder) holder;
            progressBarViewHolder.getShimmer().startShimmerAnimation();

        } else if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {

            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(content.getThumbnail()));
            emptyListViewHolder.getTextView().setText(content.getTitle());

        } else {
            ContentViewHolder contentViewHolder = (ContentViewHolder) holder;
            String dummy = content.getThumbnail();
            if (dummy != null && !dummy.equals("") && !dummy.equals("0")) {
                CommonMethods.loadContentImageWithGlide(mContext, content.getThumbnail(), contentViewHolder.getContentImage());
                contentViewHolder.getContentImage().setVisibility(View.VISIBLE);
                contentViewHolder.getDummyLay().setVisibility(View.GONE);
            } else {
                contentViewHolder.getContentImage().setVisibility(View.GONE);
                contentViewHolder.getDummyLay().setVisibility(View.VISIBLE);
                int color = generateRandomColor();
                contentViewHolder.getDummyLay().setBackgroundColor(color);
                String title = content.getTitle();
                title = title.replace("'", "''");
                title = String.valueOf(Html.fromHtml(title));
                contentViewHolder.getTitleDummy().setText(title);
                if (content.getAuthorName().equalsIgnoreCase("0")) {
                    contentViewHolder.getAuthor_Name().setVisibility(View.GONE);
                    contentViewHolder.getSourceDummy().setText(content.getSourceName());
                } else {
                    contentViewHolder.getAuthor_Name().setVisibility(View.VISIBLE);
                    contentViewHolder.getAuthor_Name().setText(content.getAuthorName());
                }
            }

            CommonMethods.loadDrawableWithGlide(mContext, CommonMethods.getIconImage(content.getContentType()), contentViewHolder.getImg_Content());
            String title = content.getTitle();
            title = title.replace("'", "''");
            title = String.valueOf(Html.fromHtml(title));
            contentViewHolder.getTextContent().setText(title);

            if (content.getBookShelfType() != null && content.getOrderStatus() != null &&
                    content.getBookShelfType().equals("3") && content.getOrderStatus().equals("4") && content.getReturnBtnText().equals("Return")) {
                contentViewHolder.getReturnButton().setVisibility(View.VISIBLE);
                contentViewHolder.getReturnButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onOrderClickListener.OnReturnClick(position);
                    }
                });
            } else {
                contentViewHolder.getReturnButton().setVisibility(View.GONE);
            }

            if (content.getBookShelfType() != null && content.getBookShelfType().equals("3")) {
                contentViewHolder.getTextStatus().setVisibility(View.VISIBLE);
                int status = Integer.valueOf(content.getOrderStatus());
                String statusStr = CommonMethods.getOrderStatus(status);
                contentViewHolder.getTextStatus().setText(statusStr);
            } else {
                contentViewHolder.getTextStatus().setVisibility(View.GONE);
            }

            if (content.getAuthorName() != null && !content.getAuthorName().equals("null")) {
                if (content.getAuthorName().equals("0")) {
                    contentViewHolder.getAuthor_Name().setVisibility(View.GONE);
                } else {
                    contentViewHolder.getAuthor_Name().setVisibility(View.VISIBLE);
                    contentViewHolder.getAuthor_Name().setText("Author: " + content.getAuthorName());
                }
            }

            if (content.getSourceName() != null && !content.getSourceName().equals("null")) {
                contentViewHolder.getSource_Name().setText("Source: " + content.getSourceName());
            }

            if (content.getAvgContentRating() != null) {
                float rating = Float.parseFloat(content.getAvgContentRating());
                if (rating > 3) {
                    contentViewHolder.getRatingBar().setRating(Float.parseFloat(content.getAvgContentRating()));
                }
            }

            String type = CommonMethods.getContentNameFromType(content.getContentType());
            contentViewHolder.getTextType().setText(type);
        }
    }

    public void setOnClickListener(OnRecyclerItemClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mContents.get(position).getContentType());
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}
