package com.indiareads.ksp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnOrderClickListener;
import com.indiareads.ksp.model.Order;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.viewholder.AttachmentViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.OrderViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;


import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;
import static com.indiareads.ksp.utils.CommonMethods.loadContentImageWithGlide;
import static com.indiareads.ksp.utils.CommonMethods.loadNormalImageWithGlide;
import static com.indiareads.ksp.utils.Constants.Order.CANCELLED;
import static com.indiareads.ksp.utils.Constants.Order.DELIVERED;
import static com.indiareads.ksp.utils.Constants.Order.ORDER_DISPATCHED;
import static com.indiareads.ksp.utils.Constants.Order.ORDER_PLACED;
import static com.indiareads.ksp.utils.Constants.Order.ORDER_PROCESSING;
import static com.indiareads.ksp.utils.Constants.Order.PICKUP_PROCESSING;
import static com.indiareads.ksp.utils.Constants.Order.PICKUP_REQUESTED;
import static com.indiareads.ksp.utils.Constants.Order.RETURNED;
import static com.indiareads.ksp.utils.Constants.Order.SENT_TO_COURIER;
import static com.indiareads.ksp.utils.Urls.BOOK_COVER_IMAGE_BASE;
import static com.indiareads.ksp.utils.Urls.baseAWSUrl;

public class MyOrdersAdapter extends Adapter {

    private List<Order> mOrdersList;
    private OnOrderClickListener mListener;
    Context mContext;

    public MyOrdersAdapter(Context mContext, List<Order> list, OnOrderClickListener listener) {
        mListener = listener;
        mOrdersList = list;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_progress_loader, parent, false);
                return new ProgressBarViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_single_item, parent, false);
                return new OrderViewHolder(view, mListener, parent.getContext());
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Order mOrder = mOrdersList.get(position);
        String mCTitle = mOrder.getTitle();

        int typeContent = Integer.parseInt(mOrder.getContent_type());

        if (typeContent == Constants.Content.CONTENT_TYPE_ERROR) {

            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(R.mipmap.ic_fevicon);
            emptyListViewHolder.getTextView().setText(mOrder.getTitle());

        } else {

            //String mCTitle = mOrder.getTitle();
            OrderViewHolder orderViewHolder = (OrderViewHolder) holder;
            orderViewHolder.setIsRecyclable(false);

            if (position != RecyclerView.NO_POSITION)

                orderViewHolder.getOrderId().setText("Order ID : " + mOrder.getOrder_id());
            orderViewHolder.getOrderTitle().setText("Title : " + mCTitle);
            orderViewHolder.getOrderDate().setText("Date : " + mOrder.getOrder_date());
            orderViewHolder.getOrderDate().setVisibility(View.GONE);
            String thmbNail = mOrder.getThumnail();
            if (thmbNail != null && !thmbNail.equals("") && !thmbNail.equals("0")) {
                loadContentImageWithGlide(mContext,
                        BOOK_COVER_IMAGE_BASE + thmbNail, orderViewHolder.getOrderImage());
            } else {

                orderViewHolder.getOrderImage().setVisibility(View.GONE);
                orderViewHolder.getDummyLay().setVisibility(View.VISIBLE);

                int color = generateRandomColor();

                orderViewHolder.getDummyLay().setBackgroundColor(color);

                // String title = orderViewHolder.getOrderTitle().getText().toString();
                mCTitle = mCTitle.replace("Title : ", "");

                if (mCTitle.length() > 30) {
                    String Nyatitle = mCTitle.substring(0, 30);
                    orderViewHolder.getTitleDummy().setText(Nyatitle + "..");
                } else {
                    orderViewHolder.getTitleDummy().setText(mCTitle);
                }


            }


            switch (mOrder.getOrder_status()) {
                case CANCELLED:
                    orderViewHolder.getProgressBar().setVisibility(View.GONE);
                    orderViewHolder.getmCancelReturnButton().setVisibility(View.GONE);
                    orderViewHolder.getOrderStatus().setText("Status : Cancelled");
                    orderViewHolder.getOrderStatus().setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    break;
                case ORDER_PLACED:
                    orderViewHolder.getProgressBar().setVisibility(View.GONE);
                    orderViewHolder.getmCancelReturnButton().setText("Cancel");
                    orderViewHolder.getmCancelReturnButton().setVisibility(View.VISIBLE);
                    orderViewHolder.getOrderStatus().setText("Status : Order Placed");
                    orderViewHolder.getOrderStatus().setTextColor(mContext.getResources().getColor(R.color.completed));
                    break;
                case ORDER_PROCESSING:
                    orderViewHolder.getProgressBar().setVisibility(View.GONE);
                    orderViewHolder.getmCancelReturnButton().setText("Cancel");
                    orderViewHolder.getmCancelReturnButton().setVisibility(View.VISIBLE);
                    orderViewHolder.getOrderStatus().setText("Status : Order Processing");
                    orderViewHolder.getOrderStatus().setTextColor(mContext.getResources().getColor(R.color.completed));
                    break;
                case ORDER_DISPATCHED:
                    orderViewHolder.getProgressBar().setVisibility(View.GONE);
                    orderViewHolder.getmCancelReturnButton().setVisibility(View.GONE);
                    orderViewHolder.getOrderStatus().setText("Status : Order Dispatched");
                    orderViewHolder.getOrderStatus().setTextColor(mContext.getResources().getColor(R.color.completed));
                    break;
                case DELIVERED:
                    orderViewHolder.getProgressBar().setVisibility(View.GONE);
                    orderViewHolder.getmCancelReturnButton().setText("RETURN");
                    orderViewHolder.getmCancelReturnButton().setVisibility(View.VISIBLE);
                    orderViewHolder.getOrderStatus().setText("Status : Delivered");
                    orderViewHolder.getOrderStatus().setTextColor(mContext.getResources().getColor(R.color.completed));
                    break;
                case PICKUP_REQUESTED:
                    orderViewHolder.getProgressBar().setVisibility(View.GONE);
                    orderViewHolder.getmCancelReturnButton().setVisibility(View.GONE);
                    orderViewHolder.getOrderStatus().setText("Status : Pickup Requested");
                    orderViewHolder.getOrderStatus().setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    break;
                case PICKUP_PROCESSING:
                    orderViewHolder.getProgressBar().setVisibility(View.GONE);
                    orderViewHolder.getmCancelReturnButton().setVisibility(View.GONE);
                    orderViewHolder.getOrderStatus().setText("Status : Pickup Processing");
                    orderViewHolder.getOrderStatus().setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    break;
                case RETURNED:
                    orderViewHolder.getProgressBar().setVisibility(View.GONE);
                    orderViewHolder.getmCancelReturnButton().setVisibility(View.GONE);
                    orderViewHolder.getOrderStatus().setText("Status : Returned");
                    orderViewHolder.getOrderStatus().setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    break;
                case SENT_TO_COURIER:
                    orderViewHolder.getProgressBar().setVisibility(View.GONE);
                    orderViewHolder.getmCancelReturnButton().setVisibility(View.GONE);
                    orderViewHolder.getOrderStatus().setText("Status : Sent To Courier");
                    orderViewHolder.getOrderStatus().setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    break;
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProductActivity.class);
                    intent.putExtra(Constants.Network.CONTENT_ID, mOrder.getContent_id());
                    intent.putExtra(Constants.Network.CONTENT_TYPE, mOrder.getContent_type());
                    intent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, "2");
                    mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        int type = Integer.parseInt(mOrdersList.get(position).getContent_type());
        return type;
    }

    @Override
    public long getItemId(int position) {
        Object obj = mOrdersList.get(position).getOrder_id();
        return obj.hashCode();
    }

    @Override
    public int getItemCount() {
        return mOrdersList.size();
    }
}
