package com.indiareads.ksp.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Chapter;
import com.indiareads.ksp.utils.HTMLUtils;

import java.util.List;

import static com.indiareads.ksp.view.activity.ProductActivity.FONT_FAMILY;
import static com.indiareads.ksp.view.activity.ProductActivity.REGULAR_FONT;

public class CourseChapterPagerAdapter extends PagerAdapter implements View.OnClickListener {

    private Context mContext;
    private List<Chapter> mChapters;
    private ViewPager mViewPager;
    private WebView chapterData;

    public CourseChapterPagerAdapter(Context mContext, List<Chapter> mChapters, ViewPager viewPager) {
        this.mContext = mContext;
        this.mChapters = mChapters;
        this.mViewPager = viewPager;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {
        ViewGroup layout = (ViewGroup) LayoutInflater.from(mContext).inflate(R.layout.course_chapter_layout, collection, false);

        TextView chapterTitle = layout.findViewById(R.id.course_chapter_name);
        chapterData = layout.findViewById(R.id.course_chapter_data);
        ImageView navigateBack = layout.findViewById(R.id.course_navigate_back);
        ImageView navigateNext = layout.findViewById(R.id.course_navigate_next);

        Chapter chapter = mChapters.get(position);
        chapterTitle.setText(mContext.getString(R.string.chapter) + " " + (position + 1) + ": " + chapter.getChapterName());

        startWebView(position);

        if (position == 0)
            navigateBack.setVisibility(View.GONE);
        else if (position == mChapters.size() - 1)
            navigateNext.setVisibility(View.GONE);

        navigateBack.setOnClickListener(this);
        navigateNext.setOnClickListener(this);

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mChapters.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.course_navigate_back)
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
        else
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
    }

    public void stopWebView() {
        chapterData.loadUrl("");
    }

    public static String getStyledFont(String html) {
        boolean addBodyStart = !html.toLowerCase().contains("<body>");
        boolean addBodyEnd = !html.toLowerCase().contains("</body");
        return "<style type=\"text/css\">@font-face {font-family: CustomFont;" +
                "src: url(\"file:///android_asset/fonts/Montserrat-Regular.ttf\")}" +
                "body {font-family: CustomFont;font-size: small;text-align: justify;}</style>" +
                (addBodyStart ? "<body>" : "") + html + (addBodyEnd ? "</body>" : "");
    }


    public void startWebView(int position) {
        if (chapterData == null)
            return;
        chapterData.clearCache(true);
        chapterData.clearHistory();
        chapterData.getSettings().setMediaPlaybackRequiresUserGesture(false);
        chapterData.getSettings().setJavaScriptEnabled(true);
        chapterData.getSettings().setDomStorageEnabled(true);


        chapterData.getSettings().setAppCacheEnabled(true);
        chapterData.getSettings().setAppCachePath(mContext.getFilesDir().getAbsolutePath() + "/cache");
        chapterData.getSettings().setDatabaseEnabled(true);
        chapterData.getSettings().setDatabasePath(mContext.getFilesDir().getAbsolutePath() + "/databases");

        //String getStyledFnt = getStyledFont(mChapters.get(position).getChapterData());

        String getFormattedData = getHtmlData(mChapters.get(position).getChapterData());
        String contentMain = HTMLUtils.getLocalFontInHTML(getFormattedData, FONT_FAMILY, REGULAR_FONT);

        chapterData.loadDataWithBaseURL("file:///android_asset/",
                contentMain,
                "text/html; charset=UTF-8", null, "about:blank");

    }

    private String getHtmlData(String bodyHTML) {
        String head = "<head><style>img{max-width: 100%; width:auto; height: auto;} iframe {max-width:100%; border: 0; width: 100%; height:auto; padding:0px; margin:0px;}</style></head>";
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }
}
