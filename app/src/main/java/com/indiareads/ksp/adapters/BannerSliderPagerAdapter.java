package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;

import java.util.List;

public class BannerSliderPagerAdapter extends PagerAdapter {
    private Context context;
    List<String> bannerSliderUrls;

    public BannerSliderPagerAdapter(Context context, List<String> bannerSliderUrls) {
        this.context = context;
        this.bannerSliderUrls = bannerSliderUrls;
    }

    @Override
    public int getCount() {
        return bannerSliderUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.banner_slider_item, null);

        //to-do

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
