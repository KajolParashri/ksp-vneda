package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.BannerModel;
import com.indiareads.ksp.model.CouponModel;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.viewholder.CouponSliderHolder;
import com.indiareads.ksp.viewholder.SliderHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CouponRecycler extends RecyclerView.Adapter {

    private ArrayList<CouponModel> mList;
    private OnRecyclerItemClickListener mListener;
    Context mContext;

    public CouponRecycler(Context mcontext, ArrayList<CouponModel> list, OnRecyclerItemClickListener listener) {
        mList = list;
        mListener = listener;
        mContext = mcontext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coupon_item, parent, false);
        return new CouponSliderHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        CouponSliderHolder sliderHolder = (CouponSliderHolder) holder;
        CouponModel couponModel = mList.get(position);

        if (!couponModel.getCouponImage().equals("")) {
            Glide.with(mContext)
                    .load(couponModel.getCouponImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    .into(sliderHolder.getImageView());
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        Object obj = mList.get(position).getCouponId();
        return obj.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}

