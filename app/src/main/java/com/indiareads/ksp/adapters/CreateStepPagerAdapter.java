package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;

import java.util.ArrayList;

/**
 * Created by HP on 09-07-2017.
 */

public class CreateStepPagerAdapter extends PagerAdapter {

    Context context;
    ViewGroup collection;
    View v1, v2;
    ArrayList<View> views;

    public CreateStepPagerAdapter(Context context) {
        this.context = context;
        v1 = LayoutInflater.from(context).inflate(R.layout.select_category_xml, collection, false);
        v2 = LayoutInflater.from(context).inflate(R.layout.create_rest_content, collection, false);

        views = new ArrayList<>();
        views.add(v1);
        views.add(v2);

    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        this.collection = collection;
        collection.addView(views.get(position));
        return views.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }

    public View getView(int i) {
        return views.get(i);
    }

    @Override
    public int getCount() {
        return views.size(); // number of maximum views in View Pager.
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1; // return true if both are equal.
    }
}

