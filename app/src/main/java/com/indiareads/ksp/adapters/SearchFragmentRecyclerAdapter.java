package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;
import com.indiareads.ksp.viewholder.SearchFragmentViewHolder;

import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;

public class SearchFragmentRecyclerAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<Content> mContents;
    private OnRecyclerItemClickListener mOnClickListener;

    public SearchFragmentRecyclerAdapter(Context mContext, List<Content> mContents) {
        this.mContext = mContext;
        this.mContents = mContents;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_progress_loader, parent, false);
                return new ProgressBarViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_search_item, parent, false);
                return new SearchFragmentViewHolder(view, mOnClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Content content = mContents.get(position);
        if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            ProgressBarViewHolder progressBarViewHolder = (ProgressBarViewHolder) holder;
            progressBarViewHolder.getShimmer().startShimmerAnimation();
        } else if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {
            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(content.getThumbnail()));
            emptyListViewHolder.getTextView().setText(content.getTitle());
        } else {
            SearchFragmentViewHolder contentViewHolder = (SearchFragmentViewHolder) holder;
            String imagePath = content.getThumbnail();
            if (imagePath != null && !imagePath.equals("") && !imagePath.equals("0")) {
                if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_BOOK_LIBRARY))) {
                    imagePath = Urls.BOOK_COVER_IMAGE_BASE + content.getThumbnail();
                }
                CommonMethods.loadContentImageWithGlide(mContext, imagePath, contentViewHolder.getContentImage());
                contentViewHolder.getContentImage().setVisibility(View.VISIBLE);
                contentViewHolder.getDummyLay().setVisibility(View.GONE);
            } else {
                contentViewHolder.getContentImage().setVisibility(View.GONE);
                contentViewHolder.getDummyLay().setVisibility(View.VISIBLE);
                contentViewHolder.getDummyLay().setBackgroundColor(generateRandomColor());
                if (content.getAuthorName() != null && content.getAuthorName().equals("0") && content.getAuthorName().equals("")) {
                    contentViewHolder.getSourceDummy().setText(content.getAuthorName());
                } else {
                    contentViewHolder.getSourceDummy().setText(content.getSourceName());
                }
            }
            String titles = content.getTitle();
            titles = titles.replace("&#039;", "'");
            contentViewHolder.getContentTitle().setText(titles);
            if (content.getAuthorName().equalsIgnoreCase("0")) {
                contentViewHolder.getAuthorName().setVisibility(View.GONE);
                contentViewHolder.getSourceDummy().setText(content.getSourceName());
            } else {
                contentViewHolder.getAuthorName().setVisibility(View.VISIBLE);
                contentViewHolder.getAuthorName().setText(content.getAuthorName());
            }
            contentViewHolder.getRatingBar().setRating(Float.parseFloat(content.getRating()));
            contentViewHolder.getContentSource().setText(mContext.getString(R.string.source).concat(": ").concat(content.getSourceName()));
            contentViewHolder.getContentTypeTitle().setText(CommonMethods.getContentTypeNameFromContentType(content.getContentType()));
            CommonMethods.loadDrawableWithGlide(mContext, CommonMethods.getIconImage(content.getContentType()), contentViewHolder.getContentTypeImage());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mContents.get(position).getContentType());
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }

    public void setOnClickListener(OnRecyclerItemClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }
}