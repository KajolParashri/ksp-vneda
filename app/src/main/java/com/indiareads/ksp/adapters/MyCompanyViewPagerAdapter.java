package com.indiareads.ksp.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indiareads.ksp.R;
import com.indiareads.ksp.view.fragment.CompanyAboutAnnouncementFragment;
import com.indiareads.ksp.view.fragment.ContentFragment;
import com.indiareads.ksp.view.fragment.PostFragment;
import com.indiareads.ksp.view.fragment.RepositoryFragment;

import java.util.ArrayList;
import java.util.List;

public class MyCompanyViewPagerAdapter extends FragmentStatePagerAdapter {

    public static final String ARG_ABOUT_ANNOUNCEMENT_FRAGMENT = "About Announcement Fragment";
    public static final String ARG_POST_FRAGMENT = "Home Fragment Arguments";
    public static final String ARG_CONTENT_FRAGMENT = "Content Fragment Arguments";
    public static final String CENTRAL_REPOSITORY_FRAGMENT = "Central Repository Fragment";

    private List<String> mTitles;
    private Bundle mArguments;

    public MyCompanyViewPagerAdapter(Context context, FragmentManager fm, Bundle mArguments) {
        super(fm);

        this.mArguments = mArguments;

        mTitles = new ArrayList<>();
        mTitles.add(context.getString(R.string.announcement_title));
        mTitles.add(context.getString(R.string.posts));
        mTitles.add(context.getString(R.string.contents));
        mTitles.add(context.getString(R.string.central_repository));
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                PostFragment homeFragment = new PostFragment();
                homeFragment.setArguments(mArguments.getBundle(ARG_POST_FRAGMENT));
                return homeFragment;
            case 2:
                ContentFragment contentFragment = new ContentFragment();
                contentFragment.setArguments(mArguments.getBundle(ARG_CONTENT_FRAGMENT));
                return contentFragment;
            case 3:
                RepositoryFragment repositoryFragment = new RepositoryFragment();
                repositoryFragment.setArguments(mArguments.getBundle(CENTRAL_REPOSITORY_FRAGMENT));
                return repositoryFragment;
            default:
                CompanyAboutAnnouncementFragment companyAboutAnnouncementFragment = new CompanyAboutAnnouncementFragment();
                companyAboutAnnouncementFragment.setArguments(mArguments.getBundle(ARG_ABOUT_ANNOUNCEMENT_FRAGMENT));
                return companyAboutAnnouncementFragment;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    @Override
    public int getCount() {
        return mTitles.size();
    }

}
