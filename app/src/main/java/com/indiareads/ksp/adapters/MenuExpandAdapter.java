package com.indiareads.ksp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.model.MenuModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MenuExpandAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<MenuModel>> _listDataChild;

    public MenuExpandAdapter(Context context, ArrayList<String> listDataHeader,
                             HashMap<String, ArrayList<MenuModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public MenuModel getChild(int groupPosition, int childPosititon) {

        MenuModel menuModel = (MenuModel) _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosititon);
        return menuModel;

    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        MenuModel menuModel = getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_child, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.subMenuName);
        ImageView iconChild = (ImageView) convertView
                .findViewById(R.id.subMenuIcon);

        txtListChild.setText(menuModel.getMenuName());
        iconChild.setImageResource(menuModel.getMenuIcon());

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<MenuModel> list = _listDataChild.get(_listDataHeader.get(groupPosition));
        return list.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.groupName);

        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
