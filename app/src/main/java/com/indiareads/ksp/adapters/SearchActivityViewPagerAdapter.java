package com.indiareads.ksp.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indiareads.ksp.R;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.view.fragment.SearchFragment;

import java.util.ArrayList;
import java.util.List;

public class SearchActivityViewPagerAdapter extends FragmentStatePagerAdapter {

    public static final String SEARCH_QUERY = "Search Query";
    public static final String CURRENT_POSITION = "Current Position";

    private List<String> tabTitles;
    private Context context;
    private String query;
    private List<Bundle> filterArguments;
    private SearchFragment currentFragment;

    public SearchActivityViewPagerAdapter(FragmentManager fm, Context context, String query, List<Bundle> filterArguments) {
        super(fm);
        this.context = context;
        this.query = query;
        this.filterArguments = filterArguments;

        tabTitles = new ArrayList<>();
        tabTitles.add(context.getString(R.string.all));
        tabTitles.add(context.getString(R.string.articles));
        tabTitles.add(context.getString(R.string.ebooks));
        tabTitles.add(context.getString(R.string.videos));
        tabTitles.add(context.getString(R.string.courses));
        tabTitles.add(context.getString(R.string.podcasts));
        tabTitles.add(context.getString(R.string.infographics));
        tabTitles.add(context.getString(R.string.library_books));
    }

    @Override
    public Fragment getItem(int position) {
        SearchFragment searchFragment = new SearchFragment();
        Bundle arguments = new Bundle();

        arguments.putString(SEARCH_QUERY, query);
        arguments.putBundle(SearchFragment.FILTER_ARGUMENTS, filterArguments.get(position));

        switch (position) {
            case 1:
                arguments.putInt(Constants.Content.CONTENT_TYPE, Constants.Content.CONTENT_TYPE_ARTICLE);
                break;
            case 2:
                arguments.putInt(Constants.Content.CONTENT_TYPE, Constants.Content.CONTENT_TYPE_EBOOK);
                break;
            case 3:
                arguments.putInt(Constants.Content.CONTENT_TYPE, Constants.Content.CONTENT_TYPE_VIDEO);
                break;
            case 4:
                arguments.putInt(Constants.Content.CONTENT_TYPE, Constants.Content.CONTENT_TYPE_COURSE);
                break;
            case 5:
                arguments.putInt(Constants.Content.CONTENT_TYPE, Constants.Content.CONTENT_TYPE_PODCAST);
                break;
            case 6:
                arguments.putInt(Constants.Content.CONTENT_TYPE, Constants.Content.CONTENT_TYPE_INFOGRAPHIC);
                break;
            case 7:
                arguments.putInt(Constants.Content.CONTENT_TYPE, Constants.Content.CONTENT_TYPE_BOOK_LIBRARY);
                break;
            default:
                arguments.putInt(Constants.Content.CONTENT_TYPE, Constants.Content.CONTENT_TYPE_ALL);
        }
        searchFragment.setArguments(arguments);
        currentFragment = searchFragment;
        return searchFragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }

    @Override
    public int getCount() {
        return tabTitles.size();
    }

    public SearchFragment getCurrentFragment() {
        return currentFragment;
    }
}