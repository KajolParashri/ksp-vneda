package com.indiareads.ksp.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.BookMainModel;
import com.indiareads.ksp.model.ExploreModel;
import com.indiareads.ksp.viewholder.BookAdapterHolder;

import java.util.ArrayList;
import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapterHolder> {

    public List<BookMainModel> exploreList;
    Activity mContext;
    OnItemClick mListener;


    public BookAdapter(Activity context, List<BookMainModel> mExploreList, OnItemClick listener) {
        exploreList = mExploreList;
        mListener = listener;
        mContext = context;
    }

    @NonNull
    @Override
    public BookAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = parent;

        LayoutInflater inflater = LayoutInflater.from(mContext);//(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(R.layout.book_item_main, parent, false);

        return new BookAdapterHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull BookAdapterHolder holder, int position) {

        LinearLayoutManager layoutManager2
                = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.list_item.setLayoutManager(layoutManager2);

        final BookMainModel dataModel = exploreList.get(position);

        //Picasso.get().load(dataModel.getImage_Url()).into(holder.imgTile);
        holder.heading.setText(dataModel.getHeadingText());
        ArrayList<ExploreModel> arrayList = dataModel.getArrayList();
        BookMainAdapter bookMainAdapter = new BookMainAdapter(mContext, arrayList, new OnItemClick() {
            @Override
            public void onArticleClick(String tileName) {

            }

            @Override
            public void onInfoClick(String tileName) {

            }

            @Override
            public void onCategoryClick(int tileName) {

            }
        });

        holder.list_item.setAdapter(bookMainAdapter);


    }

    @Override
    public int getItemCount() {
        return exploreList.size();
    }

    @Override
    public long getItemId(int position) {
        Object listItem = exploreList.get(position);
        return listItem.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}