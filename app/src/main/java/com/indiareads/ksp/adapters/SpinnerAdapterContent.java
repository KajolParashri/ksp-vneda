package com.indiareads.ksp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.model.ContentSpinnerModel;

import java.util.ArrayList;

public class SpinnerAdapterContent extends BaseAdapter {
    // ArrayList<String> name, company, email, id, status;
    ArrayList<ContentSpinnerModel> clientArrayList;
    Context c;

    public SpinnerAdapterContent(Context c, ArrayList<ContentSpinnerModel> list) {
        clientArrayList = list;
        this.c = c;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return clientArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return clientArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View row = null;
        LayoutInflater inflater = (LayoutInflater) c
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            row = inflater.inflate(R.layout.spinner_item_content, parent,
                    false);
        } else {
            row = convertView;
        }
        ContentSpinnerModel detail = clientArrayList.get(position);

        TextView item_name = row.findViewById(R.id.item_name);
        ImageView imgIcon = row.findViewById(R.id.imgIcon);

        item_name.setText(detail.getItemName());
        imgIcon.setImageResource(detail.getImage());
        return row;
    }

}
