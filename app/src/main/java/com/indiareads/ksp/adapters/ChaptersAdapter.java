package com.indiareads.ksp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnChapterButtonsClickListener;
import com.indiareads.ksp.model.Chapter;
import com.indiareads.ksp.viewholder.CreateChapterViewHolder;

import java.util.List;


public class ChaptersAdapter extends RecyclerView.Adapter<CreateChapterViewHolder> {

    private List<Chapter> mChapters;
    private OnChapterButtonsClickListener mListener;

    public ChaptersAdapter(List<Chapter> mChapters, OnChapterButtonsClickListener listener) {
        this.mChapters = mChapters;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public CreateChapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_create_single_chapter, parent, false);
        return new CreateChapterViewHolder(view, mListener, mChapters);
    }

    @Override
    public void onBindViewHolder(@NonNull CreateChapterViewHolder holder, int position) {
        holder.bindData(mChapters.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        Object obj = mChapters.get(position);
        return obj.hashCode();
    }

    @Override
    public int getItemCount() {
        return mChapters.size();
    }
}
