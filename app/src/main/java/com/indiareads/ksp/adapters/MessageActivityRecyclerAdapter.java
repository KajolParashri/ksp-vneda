package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Conversation;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.viewholder.ConversationViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;

import java.util.List;

public class MessageActivityRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = MessageActivityRecyclerAdapter.class.getSimpleName();
    private Context mContext;
    private List<Conversation> mConversations;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public MessageActivityRecyclerAdapter(Context mContext, List<Conversation> mConversations) {
        this.mContext = mContext;
        this.mConversations = mConversations;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mConversations.get(position).getChatType());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == Constants.Content.CONTENT_TYPE_PROGRESS_LOADER) {
            view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_progressbar_item, parent, false);
            return new ConversationViewHolder(view);
        } else if (viewType == Constants.Content.CONTENT_TYPE_ERROR) {
            view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_empty_item, parent, false);
            return new EmptyListViewHolder(view);
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.conversation_item, parent, false);
            return new ConversationViewHolder(view, onRecyclerItemClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == Constants.Content.CONTENT_TYPE_PROGRESS_LOADER) {
        } else if (holder.getItemViewType() == Constants.Content.CONTENT_TYPE_ERROR) {
            EmptyListViewHolder viewHolder = (EmptyListViewHolder) holder;
            viewHolder.getImageView().setImageResource(Integer.parseInt(mConversations.get(position).getChatImage()));
            viewHolder.getTextView().setText(mConversations.get(position).getChatName());
        } else {
            ConversationViewHolder conversationViewHolder = (ConversationViewHolder) holder;
            Conversation conversation = mConversations.get(position);
            if (conversation.getChatType().equals(Conversation.CHAT_TYPE_SINGLE)) {
                CommonMethods.loadProfileImageWithGlide(mContext, conversation.getChatImage(), conversationViewHolder.getImageView());
            } else {
                CommonMethods.loadGroupImageWithGlide(mContext, conversation.getChatImage(), conversationViewHolder.getImageView());
            }
            conversationViewHolder.getNameTextView().setText(conversation.getChatName());
            conversationViewHolder.getMessageTextView().setText(conversation.getMessage());

            if (conversation.getReadCount().equals("0")) {
                conversationViewHolder.getReadCountTextView().setVisibility(View.GONE);
                conversationViewHolder.getRootView().setBackgroundColor(mContext.getResources().getColor(R.color.white));
            } else {
                conversationViewHolder.getReadCountTextView().setVisibility(View.VISIBLE);
                conversationViewHolder.getReadCountTextView().setText(conversation.getReadCount());
                conversationViewHolder.getRootView().setBackgroundColor(mContext.getResources().getColor(R.color.progress_background));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mConversations.size();
    }

    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }
}
