package com.indiareads.ksp.adapters;

import android.app.Activity;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.ExploreModel;
import com.indiareads.ksp.viewholder.ExploreMainHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ExploreMainAdapter extends RecyclerView.Adapter {

    public List<ExploreModel> exploreList;
    Activity mContext;
    OnItemClick mListener;


    public ExploreMainAdapter(Activity context, List<ExploreModel> mExploreList, OnItemClick listener) {
        exploreList = mExploreList;
        mListener = listener;
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = parent;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        row = inflater.inflate(R.layout.explore_item, parent, false);

        return new ExploreMainHolder(row, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        ExploreMainHolder exploreMainHolder = (ExploreMainHolder) holder;
        final ExploreModel dataModel = exploreList.get(position);
        exploreMainHolder.getTextTile().setText(dataModel.getTitle());

        exploreMainHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {

      /*          if (dataModel.isClickable()) {
                    if (dataModel.getItem_type().equals("Article")) {
                        mListener.onArticleClick(dataModel.getItem_Name());

                    } else if (dataModel.getItem_type().equals("InfoGraph")) {
                        mListener.onInfoClick(dataModel.getItem_Name());
                    } else if (dataModel.getItem_type().equals("Category")) {
                        mListener.onCategoryClick(position);
                    }
                } else {
                    VibrationEffect.createOneShot(10000, 10);
                }*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return exploreList.size();
    }

    @Override
    public long getItemId(int position) {
        Object listItem = exploreList.get(position);
        return listItem.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}