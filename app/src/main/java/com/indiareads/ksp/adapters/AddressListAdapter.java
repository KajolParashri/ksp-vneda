package com.indiareads.ksp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnAddressClickListener;
import com.indiareads.ksp.model.Address;
import com.indiareads.ksp.viewholder.AddressViewHolder;

import java.util.List;

public class AddressListAdapter extends RecyclerView.Adapter {

    private List<Address> mAddressList;
    private OnAddressClickListener mListener;
    AddressViewHolder addressViewHolder;

    public AddressListAdapter(List<Address> list, OnAddressClickListener listener) {
        mAddressList = list;
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_single_item, parent, false);
        return new AddressViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        addressViewHolder = (AddressViewHolder) holder;
        if (position != RecyclerView.NO_POSITION)
            addressViewHolder.setData(mAddressList.get(position));

        addressViewHolder.getDelte_lay().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressViewHolder.getProgressBar().setVisibility(View.VISIBLE);
                mListener.OnAddressRemoveClick(holder.getAdapterPosition());
            }
        });
    }

    public void hideProgress() {
        addressViewHolder.getProgressBar().setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return mAddressList.size();
    }
}
