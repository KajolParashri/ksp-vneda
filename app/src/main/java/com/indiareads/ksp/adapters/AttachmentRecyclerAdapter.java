package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Attachment;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.viewholder.AttachmentViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;
import com.indiareads.ksp.viewholder.ProgressViewHolderLoader;

import java.util.List;

public class AttachmentRecyclerAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<Attachment> mAttachments;
    private OnRecyclerItemClickListener mOnClickListener;

    public AttachmentRecyclerAdapter(Context mContext, List<Attachment> mAttachments) {
        this.mContext = mContext;
        this.mAttachments = mAttachments;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachmentloader, parent, false);
                return new ProgressViewHolderLoader(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachment_item, parent, false);
                return new AttachmentViewHolder(view, mOnClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Attachment attachment = mAttachments.get(position);
        if (attachment.getFileType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            // do nothing
        } else if (attachment.getFileType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {
            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(attachment.getAttachmentId()));
            emptyListViewHolder.getTextView().setText(attachment.getAttachmentName());
        } else {

            AttachmentViewHolder attachmentViewHolder = (AttachmentViewHolder) holder;

            attachmentViewHolder.getAttachmentName().setText(attachment.getAttachmentName());
            String nameFile = attachment.getAttachmentName();
            try {
                String extension = nameFile.substring(nameFile.lastIndexOf("."));
                attachmentViewHolder.getExtension().setText(extension);
            } catch (Exception e) {
                attachmentViewHolder.getExtension().setText("FILE");
            }

        }
    }

    public void setOnClickListener(OnRecyclerItemClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mAttachments.get(position).getFileType());
    }

    @Override
    public int getItemCount() {
        return mAttachments.size();
    }
}