package com.indiareads.ksp.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.BookMainModel;
import com.indiareads.ksp.model.CouponModelList;
import com.indiareads.ksp.model.ExploreModel;
import com.indiareads.ksp.viewholder.BookAdapterHolder;
import com.indiareads.ksp.viewholder.CouponViewHolder;

import java.util.ArrayList;
import java.util.List;

public class CouponAdapter extends RecyclerView.Adapter<CouponViewHolder> {

    public List<CouponModelList> couponModelLists;
    Activity mContext;
    OnItemClick onItemClick;

    public CouponAdapter(Activity context, List<CouponModelList> couponModels, OnItemClick onitemclick) {
        couponModelLists = couponModels;
        mContext = context;
        onItemClick = onitemclick;
    }

    @NonNull
    @Override
    public CouponViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = parent;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        row = inflater.inflate(R.layout.coupon_item_xml, parent, false);
        return new CouponViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull final CouponViewHolder holder, int position) {


        final CouponModelList dataModel = couponModelLists.get(position);

        holder.getTextCoupon().setText(dataModel.getCouponCode());
        holder.getTitleCoupon().setText(dataModel.getCouponName());
        holder.getValidity().setText(dataModel.getCouponValidity());
        holder.getTermsAndConditon().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onCategoryClick(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return couponModelLists.size();
    }

    @Override
    public long getItemId(int position) {
        Object listItem = couponModelLists.get(position);
        return listItem.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}