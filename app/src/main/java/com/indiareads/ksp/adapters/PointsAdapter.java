package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.KeywordsModel;
import com.indiareads.ksp.model.PointsModel;
import com.indiareads.ksp.viewholder.PointsHolder;
import com.indiareads.ksp.viewholder.Sel_KeywordViewHolder;

import java.util.List;

public class PointsAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<PointsModel> mContents;


    public PointsAdapter(Context mContext, List<PointsModel> mContents) {
        this.mContext = mContext;
        this.mContents = mContents;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.points_xml, parent, false);
        return new PointsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        PointsModel content = mContents.get(position);

        final PointsHolder pointsHolder = (PointsHolder) holder;
        pointsHolder.getMainText().setText(content.getSUM() + " Points for " + content.getCount() + " " + content.getContent_name());
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        Object object = mContents.get(position).getSUM();
        return object.hashCode();
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}