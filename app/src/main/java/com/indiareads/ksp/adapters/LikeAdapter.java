package com.indiareads.ksp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.view.activity.HomeActivity;
import com.indiareads.ksp.view.activity.UserDetailsActivity;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;
import com.indiareads.ksp.viewholder.CommentViewHolder;

import java.util.List;

/**
 * Created by Kashish on 05-08-2018.
 */

public class LikeAdapter extends RecyclerView.Adapter {

    private static final String TAG = HomeFragmentRecyclerAdapter.class.getSimpleName();
    private Context mContext;
    private List<User> mComments; //Users list

    public LikeAdapter(List<User> commentList) {
        this.mComments = commentList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_single_comment, parent, false);
        return new CommentViewHolder(view);

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        User comment = mComments.get(position);

        CommentViewHolder commentViewHolder = (CommentViewHolder) holder;
        CommonMethods.loadProfileImageWithGlide(mContext, comment.getProfilePic(), commentViewHolder.getCommentUserImage());
        commentViewHolder.setCommentContent(comment.getDesignation());
        commentViewHolder.setCommentDate(comment.getCreatedContents());
        commentViewHolder.setCommentUserName(comment.getFullName());
        commentViewHolder.getCommentUserName().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent userProfileIntent = new Intent(mContext, UserDetailsActivity.class);
                userProfileIntent.putExtra(Constants.Network.USER_ID, mComments.get(position).getUserId());
                userProfileIntent.putExtra(Constants.Network.COMPANY_ID,mComments.get(position).getCompanyId());
                userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 0);
                mContext.startActivity(userProfileIntent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return mComments.size();
    }

    @Override
    public int getItemViewType(int position) {
        int type = Integer.parseInt(mComments.get(position).getUserId());
        return type;
    }

}
