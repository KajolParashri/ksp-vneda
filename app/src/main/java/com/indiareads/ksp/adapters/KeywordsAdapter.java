package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.KeywordsModel;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.viewholder.CategoryContentViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.KeywordViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;

import java.util.List;

public class KeywordsAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<KeywordsModel> mContents;
    public OnItemClick itemClick;

    public KeywordsAdapter(Context mContext, List<KeywordsModel> mContents, OnItemClick onItemClick) {
        this.mContext = mContext;
        this.mContents = mContents;
        this.itemClick = onItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.keyword_item, parent, false);
        return new KeywordViewHolder(view, itemClick);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        KeywordsModel content = mContents.get(position);

        KeywordViewHolder keywordViewHolder = (KeywordViewHolder) holder;

        keywordViewHolder.getHeading().setText(content.get_name());

        keywordViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClick.onCategoryClick(position);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        Object object = mContents.get(position).get_id();
        return object.hashCode();
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}