package com.indiareads.ksp.adapters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indiareads.ksp.view.fragment.BookShelfFragment;
import com.indiareads.ksp.view.fragment.DemoContentFragment;
import com.indiareads.ksp.view.fragment.DemoPostFragment;
import com.indiareads.ksp.view.fragment.DraftedContentFragment;
import com.indiareads.ksp.view.fragment.EditProfileFragment;
import com.indiareads.ksp.view.fragment.MyOrdersFragment;
import com.indiareads.ksp.view.fragment.ReadingShelfFragment;
import com.indiareads.ksp.view.fragment.RepositoryFragment;

import java.util.List;

/**
 * Created by Kashish on 26-07-2018.
 */

public class UserDetailsViewPagerAdapter extends FragmentStatePagerAdapter {

    public static final String MY_REPOSITORY_FRAGMENT = "My Repository Fragment";
    public static String ARG_POST_FRAGMENT = "Home Fragment Arguments";
    public static String ARG_CONTENT_FRAGMENT = "Content Fragment Arguments";
    public static String ARG_READING_SHELF_FRAGMENT = "Reading Shelf Fragment Arguments";
    public static String ARG_BOOK_SHELF_FRAGMENT = "Booking Shelf Fragment Arguments";
    public static String ARG_DRAFTED_CONTENT_FRAGMENT = "Drafted Content Fragment Arguments";

    private Bundle mArguments;

    private List<String> mTitles;

    private Fragment mParentFragment;

    public UserDetailsViewPagerAdapter(FragmentManager fm, Fragment parentFragment, List<String> tabTitles, Bundle arguments) {
        super(fm);
        mArguments = arguments;
        mTitles = tabTitles;
        mParentFragment = parentFragment;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                DemoContentFragment contentFragment = new DemoContentFragment();
                contentFragment.setArguments(mArguments.getBundle(ARG_CONTENT_FRAGMENT));
                return contentFragment;
            case 2:
                ReadingShelfFragment readingShelfFragment = new ReadingShelfFragment();
                readingShelfFragment.setArguments(mArguments.getBundle(ARG_READING_SHELF_FRAGMENT));
                return readingShelfFragment;
            case 3:
                BookShelfFragment bookShelfFragment = new BookShelfFragment();
                bookShelfFragment.setArguments(mArguments.getBundle(ARG_BOOK_SHELF_FRAGMENT));
                return bookShelfFragment;
            case 4:
                return new MyOrdersFragment();
            case 5:
                DraftedContentFragment draftedContentFragment = new DraftedContentFragment();
                draftedContentFragment.setArguments(mArguments.getBundle(ARG_DRAFTED_CONTENT_FRAGMENT));
                return draftedContentFragment;
            case 6:
                RepositoryFragment repositoryFragment = new RepositoryFragment();
                repositoryFragment.setArguments(mArguments.getBundle(MY_REPOSITORY_FRAGMENT));
                return repositoryFragment;
            case 7:
                EditProfileFragment editProfileFragment = new EditProfileFragment();
                editProfileFragment.setOnUpdateUserDetailsListener(mParentFragment);
                return editProfileFragment;
            default:
                DemoPostFragment homeFragment = new DemoPostFragment();
                homeFragment.setArguments(mArguments.getBundle(ARG_POST_FRAGMENT));
                return homeFragment;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    @Override
    public int getCount() {
        return mTitles.size();
    }
}