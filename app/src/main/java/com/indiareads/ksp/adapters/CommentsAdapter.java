package com.indiareads.ksp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Comment;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.UserDetailsActivity;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;
import com.indiareads.ksp.viewholder.CommentViewHolder;

import java.util.List;

/**
 * Created by Kashish on 05-08-2018.
 */

public class CommentsAdapter extends RecyclerView.Adapter {

    private static final String TAG = HomeFragmentRecyclerAdapter.class.getSimpleName();
    private Context mContext;
    private List<Comment> mComments; //Users list

    public CommentsAdapter(List<Comment> commentList) {
        this.mComments = commentList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_single_comment, parent, false);
        return new CommentViewHolder(view);

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        Comment comment = mComments.get(position);

        if (holder.getItemViewType() == Constants.Comment.COMMENT_TYPE_COMMENT) {
            CommentViewHolder commentViewHolder = (CommentViewHolder) holder;
            CommonMethods.loadProfileImage(Urls.AWS_PROFILE_IMAGE_PATH + comment.getProfilePic(), commentViewHolder.getCommentUserImage());
            commentViewHolder.setCommentContent(comment.getComment());
            commentViewHolder.setCommentDate(CommonMethods.getElapsedTime(comment.getCreatedAt()));
            commentViewHolder.setCommentUserName(comment.getFullUserName());

            commentViewHolder.getCommentUserName().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent userProfileIntent = new Intent(mContext, UserDetailsActivity.class);
                    userProfileIntent.putExtra(Constants.Network.USER_ID, mComments.get(position).getUserId());
                    userProfileIntent.putExtra(Constants.Network.COMPANY_ID,mComments.get(position).getCompanyId());
                    userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 0);
                    mContext.startActivity(userProfileIntent);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return mComments.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mComments.get(position).getCommentType();
    }

}
