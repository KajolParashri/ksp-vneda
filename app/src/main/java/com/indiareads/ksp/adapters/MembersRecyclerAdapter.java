package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.MemberViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;
import com.indiareads.ksp.viewholder.ProgressViewHolderLoader;

import java.util.List;

public class MembersRecyclerAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<User> mUsers;
    private OnRecyclerItemClickListener mOnClickListener;

    public MembersRecyclerAdapter(Context mContext, List<User> mUsers) {
        this.mContext = mContext;
        this.mUsers = mUsers;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachmentloader, parent, false);
                return new ProgressViewHolderLoader(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false);
                return new MemberViewHolder(view, mOnClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        User user = mUsers.get(position);
        if (user.getUserType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            // do nothing
        } else if (user.getUserType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {
            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(user.getProfilePic()));
            emptyListViewHolder.getTextView().setText(user.getFullName());
        } else {
            MemberViewHolder memberViewHolder = (MemberViewHolder) holder;
            memberViewHolder.getUserName().setText(user.getFullName());
            memberViewHolder.getUserDesignation().setText(user.getDesignation());
            CommonMethods.loadProfileImageWithGlide(mContext, user.getProfilePic(), memberViewHolder.getProfilePic());
        }
    }

    public void setOnClickListener(OnRecyclerItemClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mUsers.get(position).getUserType());
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }
}