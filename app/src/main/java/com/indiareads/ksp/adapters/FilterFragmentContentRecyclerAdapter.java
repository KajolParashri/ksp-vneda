package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnFilterCategoryClickListener;
import com.indiareads.ksp.model.ContentType;
import com.indiareads.ksp.viewholder.ChildCategoryViewHolder;

import java.util.ArrayList;
import java.util.List;

public class FilterFragmentContentRecyclerAdapter extends RecyclerView.Adapter<ChildCategoryViewHolder> {

    private Context context;
    private List<ContentType> contentTypeList;
    private int parentCategoryPosition;
    private ArrayList<String> filterCategoryList;
    private OnFilterCategoryClickListener onFilterCategoryClickListener;

    public FilterFragmentContentRecyclerAdapter(Context context, List<ContentType> contentTypeList, int parentCategoryPosition, ArrayList<String> filterCategoryList) {
        this.context = context;
        this.contentTypeList = contentTypeList;
        this.parentCategoryPosition = parentCategoryPosition;
        this.filterCategoryList = filterCategoryList;
    }

    @NonNull
    @Override
    public ChildCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_category_item, parent, false);
        return new ChildCategoryViewHolder(view, onFilterCategoryClickListener, parentCategoryPosition);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildCategoryViewHolder holder, int position) {

        ContentType contentType = contentTypeList.get(position);

        holder.getChildCategoryTextView().setText(contentType.getContentTypeName());

        if(filterCategoryList.contains(contentType.getContentTypeId()))
            holder.getChildCategoryCheckBox().setChecked(true);
    }

    @Override
    public int getItemCount() {
        return contentTypeList.size();
    }

    public void setOnClickListener(OnFilterCategoryClickListener onFilterCategoryClickListener) {
        this.onFilterCategoryClickListener = onFilterCategoryClickListener;
    }

}
