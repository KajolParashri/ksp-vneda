package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.CouponModel;
import com.indiareads.ksp.model.HistoryModel;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.viewholder.BreakageHistoryHolder;
import com.indiareads.ksp.viewholder.CouponSliderHolder;

import java.util.ArrayList;

public class BreakageAdapter extends RecyclerView.Adapter {

    private ArrayList<HistoryModel> mList;

    Context mContext;

    public BreakageAdapter(Context mcontext, ArrayList<HistoryModel> list) {
        mList = list;
        mContext = mcontext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item, parent, false);
        return new BreakageHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BreakageHistoryHolder sliderHolder = (BreakageHistoryHolder) holder;
        sliderHolder.getBurnedPoints().setText(mList.get(position).getBurnedCoins());
        sliderHolder.getTitle().setText(mList.get(position).getHistoryTitle());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        Object obj = mList.get(position).getDealId();
        return obj.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}

