package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnContentLikeCommentListener;
import com.indiareads.ksp.listeners.OnHomeFragmentClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.SuggestionModel;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.viewholder.AnnouncementViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;
import com.indiareads.ksp.viewholder.SuggestionViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SuggestionRecyclerAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<SuggestionModel> mContents;
    OnHomeFragmentClickListener onContentLikeCommentListener;

    public SuggestionRecyclerAdapter(Context mContext, List<SuggestionModel> mContents, OnHomeFragmentClickListener onContentLikeCommentListener) {
        this.mContext = mContext;
        this.mContents = mContents;
        this.onContentLikeCommentListener = onContentLikeCommentListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_progress_loader, parent, false);
                return new ProgressBarViewHolder(view);
            case 3:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_suggestion_item, parent, false);
                return new SuggestionViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_suggestion_item, parent, false);
                return new SuggestionViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        SuggestionModel content = mContents.get(position);
        if (content.getContent_type() == ((Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            // do nothing
            ProgressBarViewHolder progressBarViewHolder = (ProgressBarViewHolder) holder;
            progressBarViewHolder.getShimmer().startShimmerAnimation();

        } else if (content.getContent_type() == ((Constants.Content.CONTENT_TYPE_ERROR))) {

            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getTextView().setText(content.getTitle());

        } else {

            SuggestionViewHolder sHolder = (SuggestionViewHolder) holder;
            sHolder.getComment_count().setText(content.getComment_count());
            sHolder.getLike_count().setText(content.getLike_count());
            sHolder.getHome_post_item_person_name().setText(content.getUser_full_name());
            sHolder.getHome_post_item_person_designation().setText(content.getDesignation());
            sHolder.getHome_post_item_createdate().setText(content.getCreate_date());
            sHolder.getHome_post_item_description().setText(content.getSuggestion());
            String imageUrl = Urls.baseNotificationUserUrl + content.getProfile_pic();
            CommonMethods.loadProfileImage(imageUrl, sHolder.getProfile_image());

            if (content.getLikeStatus().equals(Constants.Content.CONTENT_LIKED)) {
                sHolder.getLike_image().setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.up_arrow_red));
                sHolder.getLike_text_sugg().setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                sHolder.getLike_text_sugg().setText("Upvoted");
                if (sHolder.getLike_count() != null) {
                    sHolder.getLike_count().setText(content.getLike_count() + "");
                }

            } else {
                sHolder.getLike_image().setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.up_arrow));
                sHolder.getLike_text_sugg().setTextColor(ContextCompat.getColor(mContext, R.color.grey));
                sHolder.getLike_text_sugg().setText("Upvote");
                if (sHolder.getLike_count() != null) {
                    sHolder.getLike_count().setText(content.getLike_count() + "");
                }
            }

            sHolder.getComment_layout().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onContentLikeCommentListener.onCommentClicked(v, position);
                }
            });

            sHolder.getLike_layout().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onContentLikeCommentListener.onLikeClicked(v, position);
                }
            });
        }
    }

    @Override
    public long getItemId(int position) {
        Object obj = mContents.get(position);
        return obj.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return mContents.get(position).getContent_type();
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}
