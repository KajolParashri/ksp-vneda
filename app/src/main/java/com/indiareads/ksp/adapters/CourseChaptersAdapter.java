package com.indiareads.ksp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Chapter;
import com.indiareads.ksp.view.activity.ViewCourseActivity;
import com.indiareads.ksp.viewholder.ChapterViewHolder;

import java.util.ArrayList;
import java.util.List;


public class CourseChaptersAdapter extends RecyclerView.Adapter<ChapterViewHolder> {

    private Context mContext;
    private List<Chapter> mChapters;

    public CourseChaptersAdapter(Context mContext, List<Chapter> mChapters) {
        this.mContext = mContext;
        this.mChapters = mChapters;
    }

    @NonNull
    @Override
    public ChapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.chapter_item, parent, false);
        return new ChapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChapterViewHolder holder, int position) {

        //  holder.getChapterName().setText(mContext.getString(R.string.chapter));
        int chptNm = position + 1;
        holder.getChapterNumber().setText(chptNm + ".");
        holder.getChapterName().setText(mChapters.get(position).getChapterName());

        holder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCourseActivity(holder.getAdapterPosition());
            }
        });
    }

    private void startCourseActivity(int adapterPosition) {
        Intent courseIntent = new Intent(mContext, ViewCourseActivity.class);
        courseIntent.putExtra(ViewCourseActivity.CURRENT_CHAPTER_POSITION, adapterPosition);
        courseIntent.putParcelableArrayListExtra(ViewCourseActivity.CHAPTERS, (ArrayList) mChapters);
        mContext.startActivity(courseIntent);
    }

    @Override
    public int getItemCount() {
        return mChapters.size();
    }
}
