package com.indiareads.ksp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.indiareads.ksp.R;
import com.indiareads.ksp.glide.SvgDecoder;
import com.indiareads.ksp.glide.SvgDrawableTranscoder;
import com.indiareads.ksp.glide.SvgSoftwareLayerSetter;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.CategoryModel;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.view.activity.CategoryListingActivity;
import com.indiareads.ksp.viewholder.CategoryViewHolder;

import java.io.InputStream;
import java.util.List;

import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_NAME;

public class CategoryAdapter extends RecyclerView.Adapter {

    public List<CategoryModel> exploreList;
    Context mContext;
    OnItemClick mListener;

    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;


    public CategoryAdapter(Context context, List<CategoryModel> mExploreList) {
        exploreList = mExploreList;
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = parent;

        LayoutInflater inflater = LayoutInflater.from(mContext);//(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(R.layout.category_item, parent, false);

        return new CategoryViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
        final CategoryModel dataModel = exploreList.get(position);

        setupSVGfiles(mContext, categoryViewHolder.getImgTile(), dataModel.getImage_Url());

        //holder.imgTile.setImageResource(dataModel.getImage_Url());
        categoryViewHolder.getTextTile().setText(dataModel.getItem_Name());

        categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CategoryListingActivity.class);
                intent.putExtra(Constants.Category.CATEGORY_ID, exploreList.get(position).getItem_Id());
                intent.putExtra(CATEGORY_NAME, exploreList.get(position).getItem_Name());
                mContext.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return exploreList.size();
    }

    @Override
    public long getItemId(int position) {
        Object listItem = exploreList.get(position);
        return listItem.hashCode();
    }

    public void setupSVGfiles(Context mContext, ImageView image, String url) {

        requestBuilder = Glide.with(mContext)
                .using(Glide.buildStreamModelLoader(Uri.class, mContext), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.drawable.bookiconsmall)
                .error(R.drawable.bookiconsmall)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<Uri>());

        requestBuilder.diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .load(Uri.parse(url))
                .into(image);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}