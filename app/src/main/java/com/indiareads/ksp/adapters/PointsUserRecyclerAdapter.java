package com.indiareads.ksp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.model.PointsUserModel;
import com.indiareads.ksp.model.SuggestionModel;
import com.indiareads.ksp.utils.CircleTransform;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.UserDetailsActivity;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;
import com.indiareads.ksp.viewholder.SuggestionViewHolder;
import com.indiareads.ksp.viewholder.UserPointsViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PointsUserRecyclerAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<PointsUserModel> mContents;

    public PointsUserRecyclerAdapter(Context mContext, List<PointsUserModel> mContents) {
        this.mContext = mContext;
        this.mContents = mContents;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_progress_loader, parent, false);
                return new ProgressBarViewHolder(view);
            case 3:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.points_user, parent, false);
                return new UserPointsViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.points_user, parent, false);
                return new UserPointsViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        PointsUserModel content = mContents.get(position);
        if (content.getContentType() == ((Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            // do nothing
            ProgressBarViewHolder progressBarViewHolder = (ProgressBarViewHolder) holder;
            progressBarViewHolder.getShimmer().startShimmerAnimation();

        } else if (content.getContentType() == ((Constants.Content.CONTENT_TYPE_ERROR))) {

            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getTextView().setText(content.getUserImage());

        } else {

            UserPointsViewHolder sHolder = (UserPointsViewHolder) holder;

            sHolder.getPoints().setText(content.getUserPoints());
            sHolder.getUserName().setText(content.getUserName());
            String imageUrl = Urls.baseNotificationUserUrl + content.getUserImage();
            CommonMethods.loadProfileImage(imageUrl, sHolder.getProfile_image());

            sHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent userProfileIntent = new Intent(mContext, UserDetailsActivity.class);
                    userProfileIntent.putExtra(Constants.Network.USER_ID, mContents.get(position).getUserId());
                    userProfileIntent.putExtra(Constants.Network.COMPANY_ID, mContents.get(position).getCompanyId());
                    mContext.startActivity(userProfileIntent);
                }
            });
        }
    }

    @Override
    public long getItemId(int position) {
        Object obj = mContents.get(position);
        return obj.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return mContents.get(position).getContentType();
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}
