package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Conversation;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.viewholder.NewChatViewHolder;

import java.util.HashMap;
import java.util.List;

public class NewChatActivityRecyclerAdapter extends RecyclerView.Adapter<NewChatViewHolder> {

    private static final String TAG = NewChatActivityRecyclerAdapter.class.getSimpleName();
    private Context mContext;
    private List<User> mConversations;
    private HashMap<String, Boolean> selectedUsers;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    private String chatType;

    public NewChatActivityRecyclerAdapter(Context mContext, List<User> mConversations, String chatType, HashMap<String, Boolean> selectedUsers) {
        this.mContext = mContext;
        this.mConversations = mConversations;
        this.chatType = chatType;
        this.selectedUsers = selectedUsers;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mConversations.get(position).getUserType());
    }

    @NonNull
    @Override
    public NewChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == Constants.Content.CONTENT_TYPE_PROGRESS_LOADER)
            view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_progressbar_item, parent, false);
        else if (viewType == Constants.Content.CONTENT_TYPE_ERROR)
            view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_empty_item, parent, false);
        else
            view = LayoutInflater.from(mContext).inflate(R.layout.new_chat_item, parent, false);

        return new NewChatViewHolder(view, onRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NewChatViewHolder holder, int position) {
        if (holder.getItemViewType() != Constants.Content.CONTENT_TYPE_PROGRESS_LOADER) {

            User conversation = mConversations.get(position);

            if (conversation.getUserType().equals(User.USER_TYPE_NORMAL)) {
                CommonMethods.loadProfileImageWithGlide(mContext, conversation.getProfilePic(), holder.getProfileImageView());
                holder.getNameTextView().setText(conversation.getFullName());
                holder.getDesignationTextView().setText(conversation.getDesignation());

                if (chatType.equals(Conversation.CHAT_TYPE_SINGLE))
                    holder.getCheckBox().setVisibility(View.GONE);
                else {
                    holder.getCheckBox().setVisibility(View.VISIBLE);
                    holder.getCheckBox().setChecked(selectedUsers.get(conversation.getUserId()));
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mConversations.size();
    }

    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }

}
