package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnDirectoryItemClickListener;
import com.indiareads.ksp.model.DirectoryItem;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.FileExtensionUtil;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.viewholder.DirectoryItemViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;

import java.util.List;

import static com.indiareads.ksp.view.fragment.RepositoryFragment.REPO_TYPE_COMPANY;
import static com.indiareads.ksp.view.fragment.RepositoryFragment.REPO_TYPE_PERSONAL;

public class DirectoryItemRecyclerAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<DirectoryItem> mDirectoryItems;
    private OnDirectoryItemClickListener mOnClickListener;
    String repoType;

    public DirectoryItemRecyclerAdapter(Context mContext, List<DirectoryItem> mDirectoryItems, String repoType) {
        this.mContext = mContext;
        this.mDirectoryItems = mDirectoryItems;
        this.repoType = repoType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_progress_loader, parent, false);
                return new ProgressBarViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.directory_item_layout, parent, false);
                return new DirectoryItemViewHolder(view, mOnClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        DirectoryItem directoryItem = mDirectoryItems.get(position);

        if (directoryItem.getObjectType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            // do nothing
        } else if (directoryItem.getObjectType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {
            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(directoryItem.getIcon()));
            emptyListViewHolder.getTextView().setText(directoryItem.getName());
        } else {
            DirectoryItemViewHolder directoryItemViewHolder = (DirectoryItemViewHolder) holder;

            if (directoryItem.getObjectType().equals(DirectoryItem.TYPE_FOLDER)) {
                CommonMethods.loadDrawableWithGlide(mContext, R.drawable.file_repo, directoryItemViewHolder.getFileIcon());

                if (repoType.equalsIgnoreCase(REPO_TYPE_COMPANY) &&
                        User.getCurrentUser(mContext).getIsAdmin().equalsIgnoreCase("1")) {
                    directoryItemViewHolder.getOptionsIcon().setVisibility(View.VISIBLE);
                } else if (repoType.equalsIgnoreCase(REPO_TYPE_PERSONAL)) {
                    directoryItemViewHolder.getOptionsIcon().setVisibility(View.VISIBLE);
                } else {
                    directoryItemViewHolder.getOptionsIcon().setVisibility(View.GONE);
                }

            } else if (directoryItem.getObjectType().equals(DirectoryItem.TYPE_FILE)) {

                int extensions = directoryItem.getName().lastIndexOf(".");
                String extension = directoryItem.getName().substring(extensions + 1, directoryItem.getName().length());
                //String extension = MimeTypeMap.getFileExtensionFromUrl(directoryItem.getName());
                Logger.error("extension", extension);
                if (FileExtensionUtil.checkForImage().contains(extension)) {
                    CommonMethods.loadDrawableWithGlide(mContext, R.drawable.image_logo, directoryItemViewHolder.getFileIcon());
                } else if (FileExtensionUtil.checkForVideo().contains(extension)) {
                    CommonMethods.loadDrawableWithGlide(mContext, R.drawable.video_logo, directoryItemViewHolder.getFileIcon());
                } else if (FileExtensionUtil.checkForAudio().contains(extension)) {
                    CommonMethods.loadDrawableWithGlide(mContext, R.drawable.audio_logo, directoryItemViewHolder.getFileIcon());
                } else if (FileExtensionUtil.checkForDoc().contains(extension)) {
                    CommonMethods.loadDrawableWithGlide(mContext, R.drawable.doc_logo, directoryItemViewHolder.getFileIcon());
                } else if (FileExtensionUtil.checkForSheet().contains(extension)) {
                    CommonMethods.loadDrawableWithGlide(mContext, R.drawable.sheet_logo, directoryItemViewHolder.getFileIcon());
                } else if (FileExtensionUtil.checkForPDF().contains(extension)) {
                    CommonMethods.loadDrawableWithGlide(mContext, R.drawable.pdf_logo, directoryItemViewHolder.getFileIcon());
                } else {
                    CommonMethods.loadDrawableWithGlide(mContext, R.drawable.file_logo, directoryItemViewHolder.getFileIcon());
                }
            } else {
                directoryItemViewHolder.getOptionsIcon().setVisibility(View.GONE);
            }
            directoryItemViewHolder.getItemName().setText(directoryItem.getName());
        }
    }

    public void setOnClickListener(OnDirectoryItemClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mDirectoryItems.get(position).getObjectType());
    }

    @Override
    public int getItemCount() {
        return mDirectoryItems.size();
    }
}