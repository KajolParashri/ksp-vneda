package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnDraftedContentItemClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.viewholder.ContentViewHolderDraft;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;

import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;

public class DraftedContentFragmentRecyclerAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<Content> mContents;
    private OnDraftedContentItemClickListener mOnClickListener;

    public DraftedContentFragmentRecyclerAdapter(Context mContext, List<Content> mContents) {
        this.mContext = mContext;
        this.mContents = mContents;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_progress_loader, parent, false);
                return new ProgressBarViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lineardraft, parent, false);
                return new ContentViewHolderDraft(view, mOnClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        Content content = mContents.get(position);
        if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            // do nothing
        } else if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {
            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(content.getThumbnail()));
            emptyListViewHolder.getTextView().setText(content.getTitle());
        } else {
            final ContentViewHolderDraft contentViewHolder = (ContentViewHolderDraft) holder;

            // CommonMethods.loadContentImageWithGlide(mContext, content.getThumbnail(), contentViewHolder.getContentImage());


            String url = content.getThumbnail();

            if (url != null && !url.equals("") && !url.equals("0")) {
                CommonMethods.loadContentImageWithGlide(mContext, content.getThumbnail(), contentViewHolder.getContentImage());
            } else {
                contentViewHolder.getContentImage().setVisibility(View.GONE);
                contentViewHolder.getDummyLay().setVisibility(View.VISIBLE);

                int color = generateRandomColor();

                contentViewHolder.getDummyLay().setBackgroundColor(color);
                String title = content.getTitle();

                if (title.length() > 30) {

                    title = title.substring(0, 30);
                    contentViewHolder.getTitleDummy().setText(title + "..");

                } else {
                    contentViewHolder.getTitleDummy().setText(title);
                }

                contentViewHolder.getTitleDummy().setText(content.getTitle());
            }


            CommonMethods.loadDrawableWithGlide(mContext, CommonMethods.getIconImage(content.getContentType()), contentViewHolder.getImg_Content());
            contentViewHolder.getTextContent().setText(content.getTitle());

            String type = CommonMethods.getContentNameFromType(content.getContentType());
            contentViewHolder.getTextType().setText(type);

            contentViewHolder.getEdtlay().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnClickListener.onEditClick(view, contentViewHolder.getAdapterPosition());
                }
            });

            contentViewHolder.getDelLay().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnClickListener.onDeleteClick(view, contentViewHolder.getAdapterPosition());
                }
            });

        }
    }

    public void setOnClickListener(OnDraftedContentItemClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mContents.get(position).getContentType());
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}
