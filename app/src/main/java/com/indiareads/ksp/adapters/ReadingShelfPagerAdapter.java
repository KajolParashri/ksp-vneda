package com.indiareads.ksp.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.indiareads.ksp.R;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.view.fragment.ReadingShelfContentFragment;

import java.util.ArrayList;
import java.util.List;

public class ReadingShelfPagerAdapter extends FragmentStatePagerAdapter {

    private static final String TAG = ReadingShelfPagerAdapter.class.getSimpleName();

    private List<String> mTitles;
    private Context mContext;

    public ReadingShelfPagerAdapter(FragmentManager fm,Context context) {
        super(fm);

        mContext = context;

        mTitles = new ArrayList<>();
        mTitles.add(mContext.getString(R.string.articles));
        mTitles.add(mContext.getString(R.string.videos));
        mTitles.add(mContext.getString(R.string.courses));
        mTitles.add(mContext.getString(R.string.infographics));
        mTitles.add(mContext.getString(R.string.ebooks));
        mTitles.add(mContext.getString(R.string.podcasts));
    }

    @Override
    public Fragment getItem(int position) {
        ReadingShelfContentFragment readingShelfContentFragment = new ReadingShelfContentFragment();
        Bundle args = new Bundle();

        if(position == 0)
            args.putString(Constants.Content.CONTENT_TYPE,String.valueOf(Constants.Content.CONTENT_TYPE_ARTICLE));
        else if(position == 1)
            args.putString(Constants.Content.CONTENT_TYPE,String.valueOf(Constants.Content.CONTENT_TYPE_VIDEO));
        else if(position == 2)
            args.putString(Constants.Content.CONTENT_TYPE,String.valueOf(Constants.Content.CONTENT_TYPE_COURSE));
        else if(position == 3)
            args.putString(Constants.Content.CONTENT_TYPE,String.valueOf(Constants.Content.CONTENT_TYPE_INFOGRAPHIC));
        else if(position == 4)
            args.putString(Constants.Content.CONTENT_TYPE,String.valueOf(Constants.Content.CONTENT_TYPE_EBOOK));
        else
            args.putString(Constants.Content.CONTENT_TYPE,String.valueOf(Constants.Content.CONTENT_TYPE_PODCAST));

        readingShelfContentFragment.setArguments(args);

        return readingShelfContentFragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    @Override
    public int getCount() {
        return mTitles.size();
    }
}
