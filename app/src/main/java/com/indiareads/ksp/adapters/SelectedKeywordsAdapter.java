package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.KeywordsModel;
import com.indiareads.ksp.viewholder.KeywordViewHolder;
import com.indiareads.ksp.viewholder.Sel_KeywordViewHolder;

import java.util.List;

public class SelectedKeywordsAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<KeywordsModel> mContents;
    public OnItemClick itemClick;

    public SelectedKeywordsAdapter(Context mContext, List<KeywordsModel> mContents, OnItemClick onItemClick) {
        this.mContext = mContext;
        this.mContents = mContents;
        this.itemClick = onItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.selected_keyword, parent, false);
        return new Sel_KeywordViewHolder(view, itemClick);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        final KeywordsModel content = mContents.get(position);


        final Sel_KeywordViewHolder keywordViewHolder = (Sel_KeywordViewHolder) holder;

        keywordViewHolder.getHeading().setText(content.get_name());

        keywordViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClick.onCategoryClick(keywordViewHolder.getAdapterPosition());
            }
        });
    }

    public int returnPos(int pos) {
        int position = pos;
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        Object object = mContents.get(position).get_id();
        return object.hashCode();
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}