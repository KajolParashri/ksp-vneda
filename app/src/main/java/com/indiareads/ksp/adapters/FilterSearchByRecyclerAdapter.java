package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.ChildCategory;
import com.indiareads.ksp.viewholder.CheckBoxViewHolder;

import java.util.ArrayList;
import java.util.List;

public class FilterSearchByRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ChildCategory> searchByList;
    private ArrayList<String> filterSearchByList;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public FilterSearchByRecyclerAdapter(Context context, List<ChildCategory> searchByList, ArrayList<String> filterSearchByList) {
        this.context = context;
        this.searchByList = searchByList;
        this.filterSearchByList = filterSearchByList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.filter_checkbox_item, parent, false);
        return new CheckBoxViewHolder(view, onRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ChildCategory childCategory = searchByList.get(position);

        CheckBoxViewHolder checkBoxViewHolder = (CheckBoxViewHolder) holder;
        checkBoxViewHolder.getTextView().setText(childCategory.getCategoryName());

        if (filterSearchByList.contains(childCategory.getCategoryId()))
            checkBoxViewHolder.getCheckBox().setChecked(true);
        else
            checkBoxViewHolder.getCheckBox().setChecked(false);
    }

    @Override
    public int getItemCount() {
        return searchByList.size();
    }

    public void setOnClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener)
    {
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }
}
