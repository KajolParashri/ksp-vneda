package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnHomeFragmentClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.LikeCommentHelper;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.viewholder.CircularProgressHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.HomeFragmentAnnouncementViewHolder;
import com.indiareads.ksp.viewholder.HomeFragmentMediaViewHolder;
import com.indiareads.ksp.viewholder.HomeFragmentPostViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Kashish on 28-07-2018.
 */

public class HomeFragmentRecyclerAdapter extends RecyclerView.Adapter {

    private static final String TAG = HomeFragmentRecyclerAdapter.class.getSimpleName();
    private Context mContext;
    private List<User> mUsers; //Users list
    private List<Content> mContents; //Contents list
    private OnHomeFragmentClickListener mHomeFragmentClickListener;
    private boolean showEditOptions;

    public HomeFragmentRecyclerAdapter(List<User> users, List<Content> contents, boolean showEditOptions) {
        this.mUsers = users;
        this.mContents = contents;
        this.showEditOptions = showEditOptions;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_progress_dialog, parent, false);
                return new CircularProgressHolder(view);
            case Constants.Content.CONTENT_TYPE_POST:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_home_post_item, parent, false);
                return new HomeFragmentPostViewHolder(view, mHomeFragmentClickListener);
            case Constants.Content.CONTENT_TYPE_ANNOUNCEMENT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_home_announcement_item, parent, false);
                return new HomeFragmentAnnouncementViewHolder(view, mHomeFragmentClickListener);
            case Constants.Content.CONTENT_TYPE_DATALOAD:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_progress_dialog, parent, false);
                return new CircularProgressHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_home_media_item, parent, false);
                return new HomeFragmentMediaViewHolder(view, mHomeFragmentClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        User user = mUsers.get(position);
        Content content = mContents.get(position);

        if (holder.getItemViewType() == Constants.Content.CONTENT_TYPE_ANNOUNCEMENT) {

            HomeFragmentAnnouncementViewHolder homeFragmentAnnouncementViewHolder = (HomeFragmentAnnouncementViewHolder) holder;

            CommonMethods.loadProfileImageWithGlide(mContext, user.getProfilePic(), homeFragmentAnnouncementViewHolder.getHomeAnnouncementProfileImage());

            homeFragmentAnnouncementViewHolder.getHomeAnnouncementPersonName().setText(user.getFullName());
            homeFragmentAnnouncementViewHolder.getHomeAnnouncementPersonDesignation().setText(user.getDesignation());
            try {
                String time = CommonMethods.getElapsedTime(content.getCreatedDate());
                if (time.equalsIgnoreCase("1 months ago")) {
                    time = "1 month ago";
                }
                homeFragmentAnnouncementViewHolder.getHomeAnnouncementItemCreateDate().setText(time);
                ;
            } catch (Exception e) {

            }
            String title = content.getTitle();
            title = title.replace("'", "''");

            title = String.valueOf(Html.fromHtml(title));


            homeFragmentAnnouncementViewHolder.getHomeAnnouncementItemTitle().setText(title);

            homeFragmentAnnouncementViewHolder.getHomeAnnouncementItemDescription().setText(content.getDescription());

            final TextView description = homeFragmentAnnouncementViewHolder.getHomeAnnouncementItemDescription();
            ViewTreeObserver vto = description.getViewTreeObserver();

            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {

                    ViewTreeObserver obs = description.getViewTreeObserver();
                    obs.removeOnPreDrawListener(this);
                    if (description.getLineCount() > 2) {
                        Logger.debug("", "Line[" + description.getLineCount() + "]" + description.getText());
                        int lineEndIndex = description.getLayout().getLineEnd(1);
                        String text = description.getText().subSequence(0, lineEndIndex - 4) + "...";
                        description.setText(text);
                        Logger.debug("", "NewText:" + text);
                    }

                    return true;
                }

            });

        } else if (holder.getItemViewType() == Constants.Content.CONTENT_TYPE_POST) {

            HomeFragmentPostViewHolder homeFragmentPostViewHolder = (HomeFragmentPostViewHolder) holder;

            CommonMethods.loadProfileImageWithGlide(mContext, user.getProfilePic(), homeFragmentPostViewHolder.getHomePostProfileImage());
            if (showEditOptions)
                homeFragmentPostViewHolder.getDeletePost().setVisibility(View.VISIBLE);
            else
                homeFragmentPostViewHolder.getDeletePost().setVisibility(View.GONE);

            homeFragmentPostViewHolder.getHomePostPersonName().setText(user.getFullName());
            homeFragmentPostViewHolder.getHomePostPersonDesignation().setText(user.getDesignation());
            try {
                String time = CommonMethods.getElapsedTime(content.getCreatedDate());
                if (time.equalsIgnoreCase("1 months ago")) {
                    time = "1 month ago";
                }
                homeFragmentPostViewHolder.getHomePostItemCreateDate().setText(time);
            } catch (Exception e) {

            }


            String desc = Html.fromHtml(content.getDescription()).toString();
            homeFragmentPostViewHolder.getHomePostItemDescription().setText(desc);
            /*//final TextView description = homeFragmentPostViewHolder.getHomePostItemDescription();
             *//* ViewTreeObserver vto = description.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {

                    ViewTreeObserver obs = description.getViewTreeObserver();
                    obs.removeOnPreDrawListener(this);
                    if (description.getLineCount() > 2) {
                        Logger.debug("", "Line[" + description.getLineCount() + "]" + description.getText());
                        int lineEndIndex = description.getLayout().getLineEnd(1);
                        String text = description.getText().subSequence(0, lineEndIndex - 4) + "...";
                        description.setText(text);
                        Logger.debug("", "NewText:" + text);
                    }

                    return true;
                }

            });*//*
            //description.setText(text);*/

            if (!content.getLikeCount().equalsIgnoreCase("0")) {
                homeFragmentPostViewHolder.getHomePostLikeCount().setVisibility(View.VISIBLE);
                homeFragmentPostViewHolder.getHomePostLikeText().setText("Like  |");
                homeFragmentPostViewHolder.getHomePostLikeCount().setText(content.getLikeCount());
            } else {
                homeFragmentPostViewHolder.getHomePostLikeText().setText("Like ");
                homeFragmentPostViewHolder.getHomePostLikeCount().setVisibility(View.GONE);
            }

            homeFragmentPostViewHolder.getHomePostCommentCount().setText(content.getCommentCount());

            LikeCommentHelper.handleLikeButtonConfig(content, mContext, homeFragmentPostViewHolder.getHomePostLikeImage(),
                    homeFragmentPostViewHolder.getHomePostLikeText(), homeFragmentPostViewHolder.getHomePostLikeCount());


        } else if (holder.getItemViewType() == Constants.Content.CONTENT_TYPE_PROGRESS_LOADER) {

//            CircularProgressHolder homeFragmentPostViewHolder = (CircularProgressHolder) holder;

        } else if (content.getContentType().

                equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {

            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(content.getThumbnail()));
            emptyListViewHolder.getTextView().setText(content.getTitle());

        } else if (content.getContentType().

                equals(String.valueOf(Constants.Content.CONTENT_TYPE_DATALOAD))) {
            CircularProgressHolder circularProgressHolder = (CircularProgressHolder) holder;

        } else {


            HomeFragmentMediaViewHolder homeFragmentMediaViewHolder = (HomeFragmentMediaViewHolder) holder;

            CommonMethods.loadProfileImageWithGlide(mContext, user.getProfilePic(), homeFragmentMediaViewHolder.getHomeMediaProfileImage());

            CommonMethods.loadContentImageWithGlide(mContext, content.getThumbnail(), homeFragmentMediaViewHolder.getHomeMediaItemMediaImage());

            String contentName = CommonMethods.getContentNameFromType(content.getContentType());
            int contentImage = CommonMethods.getContentImageFromType(content.getContentType());

            Picasso.get().load(contentImage).resize(110, 110)
                    .onlyScaleDown().into(homeFragmentMediaViewHolder.getContent_item_icon());

            homeFragmentMediaViewHolder.getHomeMediaContentIconImage().setText(contentName);
            homeFragmentMediaViewHolder.getHomeMediaPersonName().setText(user.getFullName());
            homeFragmentMediaViewHolder.getHomeMediaPersonDesignation().setText(user.getDesignation());

            if (!content.getLikeCount().equalsIgnoreCase("0")) {
                homeFragmentMediaViewHolder.getHomeMediaLikeCount().setVisibility(View.VISIBLE);
                homeFragmentMediaViewHolder.getHomeMediaLikeText().setText("Like  |");
                homeFragmentMediaViewHolder.getHomeMediaLikeCount().setText(content.getLikeCount());
            } else {

                homeFragmentMediaViewHolder.getHomeMediaLikeText().setText("Like ");
                homeFragmentMediaViewHolder.getHomeMediaLikeCount().setVisibility(View.GONE);
            }

            try {

                String time = CommonMethods.getElapsedTime(content.getCreatedDate());
                if (time.equalsIgnoreCase("1 months ago")) {
                    time = "1 month ago";
                }
                homeFragmentMediaViewHolder.getHomeMediaItemCreateDate().setText(time);

            } catch (Exception e) {

            }

            String title = content.getTitle();
            title = title.replace("'", "''");

            title = String.valueOf(Html.fromHtml(title));


            homeFragmentMediaViewHolder.getHomeMediaItemMediaTitle().setText(title);
            final TextView titles = homeFragmentMediaViewHolder.getHomeMediaItemMediaTitle();
            ViewTreeObserver vto2 = titles.getViewTreeObserver();
            vto2.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    ViewTreeObserver obs = titles.getViewTreeObserver();
                    obs.removeOnPreDrawListener(this);
                    if (titles.getLineCount() > 2) {
                        Logger.debug("", "Line[" + titles.getLineCount() + "]" + titles.getText());
                        int lineEndIndex = titles.getLayout().getLineEnd(1);
                        String text = titles.getText().subSequence(0, lineEndIndex - 4) + "...";
                        titles.setText(text);
                        Logger.debug("", "NewText:" + text);
                    }
                    return true;
                }

            });

            homeFragmentMediaViewHolder.getHomeMediaItemMediaDescription().setText(content.getDescription());


            final TextView description = homeFragmentMediaViewHolder.getHomeMediaItemMediaDescription();
            ViewTreeObserver vto = description.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    ViewTreeObserver obs = description.getViewTreeObserver();
                    obs.removeOnPreDrawListener(this);
                    if (description.getLineCount() > 2) {
                        Logger.debug("", "Line[" + description.getLineCount() + "]" + description.getText());
                        int lineEndIndex = description.getLayout().getLineEnd(1);
                        String text = description.getText().subSequence(0, lineEndIndex - 4) + "...";
                        description.setText(text);
                        Logger.debug("", "NewText:" + text);
                    }
                    return true;
                }

            });

            homeFragmentMediaViewHolder.getHomeMediaCommentCount().setText(content.getCommentCount());
            homeFragmentMediaViewHolder.getHomeMediaLikeCount().setText(content.getLikeCount());

            LikeCommentHelper.handleLikeButtonConfig(content, mContext, homeFragmentMediaViewHolder.getHomeMediaLikeImage(),
                    homeFragmentMediaViewHolder.getHomeMediaLikeText(), homeFragmentMediaViewHolder.getHomeMediaLikeCount());
        }

    }

    public void setOnClickListener(OnHomeFragmentClickListener homeFragmentClickListener) {
        mHomeFragmentClickListener = homeFragmentClickListener;
    }


    @Override
    public int getItemCount() {
        return mContents.size();
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mContents.get(position).getContentType());
    }

}
