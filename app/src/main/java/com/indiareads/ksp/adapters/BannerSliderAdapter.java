package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.BannerModel;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.viewholder.SliderHolder;

import java.util.ArrayList;

public class BannerSliderAdapter extends RecyclerView.Adapter {

    private ArrayList<BannerModel> mList;
    private OnRecyclerItemClickListener mListener;
    Context mContext;

    public BannerSliderAdapter(Context mcontext, ArrayList<BannerModel> list, OnRecyclerItemClickListener listener) {
        mList = list;
        mListener = listener;
        mContext = mcontext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_item, parent, false);
        return new SliderHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        SliderHolder sliderHolder = (SliderHolder) holder;
        CommonMethods.loadBannerWithGlide(mContext, mList.get(position).getImgUrl(), sliderHolder.getImageView());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        Object obj = mList.get(position).getImgUrl();
        return obj.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}

