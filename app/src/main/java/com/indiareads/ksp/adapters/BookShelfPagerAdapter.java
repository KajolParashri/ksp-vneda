package com.indiareads.ksp.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indiareads.ksp.R;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.view.fragment.BookShelfContentFragment;
import com.indiareads.ksp.view.fragment.BookShelfFragment;
import com.indiareads.ksp.view.fragment.ReadingShelfContentFragment;

import java.util.ArrayList;
import java.util.List;

import static com.indiareads.ksp.utils.Constants.Bookshelf.CURRENTLY_READING;

public class BookShelfPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> mTitles;
    private Context mContext;


    public BookShelfPagerAdapter(FragmentManager fm, Context context) {
        super(fm);

        mContext = context;
        mTitles = new ArrayList<>();
        mTitles.add(mContext.getString(R.string.book_currentlyreading));
        mTitles.add(mContext.getString(R.string.book_wishlist));
        mTitles.add(mContext.getString(R.string.book_history));
        mTitles.add(mContext.getString(R.string.book_available));
    }

    @Override
    public Fragment getItem(int position) {
        BookShelfContentFragment bookShelfFragment = new BookShelfContentFragment();
        Bundle args = new Bundle();

        if (position == 0)
            args.putString(Constants.Network.SHELF_TYPE, "3");
        else if (position == 1)
            args.putString(Constants.Network.SHELF_TYPE, "2");
        else if (position == 2)
            args.putString(Constants.Network.SHELF_TYPE, "4");
        else if (position == 3)
            args.putString(Constants.Network.SHELF_TYPE, "1");

        bookShelfFragment.setArguments(args);

        return bookShelfFragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    @Override
    public int getCount() {
        return mTitles.size();
    }
}
