package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnDraftedContentItemClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.viewholder.CategoryContentViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;

import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;

public class ContentFragmentAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<Content> mContents;
    private OnDraftedContentItemClickListener mOnClickListener;
    String contentType = "";

    public ContentFragmentAdapter(Context mContext, List<Content> mContents, String contentTypeStr) {
        this.mContext = mContext;
        this.mContents = mContents;
        this.contentType = contentTypeStr;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_progress_loader, parent, false);
                return new ProgressBarViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.linearlisting_categoryt, parent, false);
                return new CategoryContentViewHolder(view, mOnClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        final Content content = mContents.get(position);
        if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {

            ProgressBarViewHolder progressBarViewHolder = (ProgressBarViewHolder) holder;
            progressBarViewHolder.getShimmer().startShimmerAnimation();

        } else if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {

            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(content.getThumbnail()));
            emptyListViewHolder.getTextView().setText(content.getTitle());

        } else {

            final CategoryContentViewHolder contentViewHolder = (CategoryContentViewHolder) holder;

            if (contentType.equals("Profile")) {
                contentViewHolder.getEd_de_layout().setVisibility(View.VISIBLE);

                contentViewHolder.getDelLay().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnClickListener.onDeleteClick(v, contentViewHolder.getAdapterPosition());
                    }
                });

                contentViewHolder.getEdtlay().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnClickListener.onEditClick(v, contentViewHolder.getAdapterPosition());
                    }
                });

                if (mContents.get(position).getContentType().equals("1") ||
                        mContents.get(position).getContentType().equals("3")) {
                    contentViewHolder.getEdtlay().setVisibility(View.VISIBLE);
                } else {
                    contentViewHolder.getEdtlay().setVisibility(View.GONE);
                }
            }
            contentViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnClickListener.onItemClick(v, contentViewHolder.getAdapterPosition());
                }
            });


            String url = content.getThumnail();

            if (url != null && !url.equals("") && !url.equals("0")) {
                contentViewHolder.getContentImage().setVisibility(View.VISIBLE);
                contentViewHolder.getDummyLay().setVisibility(View.GONE);
                CommonMethods.loadContentImageWithGlide(mContext, content.getThumnail(), contentViewHolder.getContentImage());
            } else {

                contentViewHolder.getContentImage().setVisibility(View.GONE);
                contentViewHolder.getDummyLay().setVisibility(View.VISIBLE);

                int color = generateRandomColor();

                contentViewHolder.getDummyLay().setBackgroundColor(color);
                String title = content.getTitle();
                title = title.replace("'", "''");

                title = String.valueOf(Html.fromHtml(title));


                if (title.length() > 30) {

                    title = title.substring(0, 30);
                    contentViewHolder.getTitleDummy().setText(title + "..");

                } else {
                    contentViewHolder.getTitleDummy().setText(title);
                }

                if (content == null || content.getAuthorName().equals("0") || content.getAuthorName().equals("")) {
                    contentViewHolder.getSourceDummy().setText(content.getSourceName());
                } else {
                    contentViewHolder.getSourceDummy().setText(content.getSourceName());
                }
            }

            String title = content.getTitle();
            title = title.replace("'", "''");

            title = String.valueOf(Html.fromHtml(title));

            final TextView titleView = contentViewHolder.getTextContent();
            titleView.setText(title);
            ViewTreeObserver vto = titleView.getViewTreeObserver();

            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {

                    ViewTreeObserver obs = titleView.getViewTreeObserver();
                    obs.removeOnPreDrawListener(this);
                    if (titleView.getLineCount() > 2) {
                        Logger.debug("", "Line[" + titleView.getLineCount() + "]" + titleView.getText());
                        int lineEndIndex = titleView.getLayout().getLineEnd(1);
                        String text = titleView.getText().subSequence(0, lineEndIndex - 4) + "...";
                        titleView.setText(text);
                        Logger.debug("", "NewText:" + text);
                    }
                    return true;
                }

            });

            String stype = content.getContentType();
            int resId = CommonMethods.getContentImageFromType(stype);
            contentViewHolder.getImg_Content().setImageResource(resId);
            String type = CommonMethods.getContentNameFromType(stype);
            Integer rating = Integer.valueOf(content.getAvgContentRating());
            if (rating == null || rating == 0) {

            } else {
                contentViewHolder.getRatingBar().setRating(Integer.valueOf(content.getAvgContentRating()));
            }

            //  contentViewHolder.getRatingBar().setRating(Integer.valueOf(content.getAvgContentRating()));
            contentViewHolder.getTextType().setText(type);
            contentViewHolder.getTxtSource().setText("Source: " + content.getSourceName());

            if (content.getAuthorName() != null && content.getAuthorName().equals("0")) {
                contentViewHolder.getTxtAuthor().setVisibility(View.GONE);
            } else {
                contentViewHolder.getTxtAuthor().setText("Author: " + content.getAuthorName());
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mContents.get(position).getContentType());
    }

    public void setOnClickListener(OnDraftedContentItemClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}