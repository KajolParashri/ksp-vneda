package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Directory;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.viewholder.ContentViewHolder;
import com.indiareads.ksp.viewholder.DirectoryViewHolder;

import java.util.List;

public class PathRecyclerAdapter extends RecyclerView.Adapter<DirectoryViewHolder> {

    private Context mContext;
    private List<Directory> mDiretories;
    private OnRecyclerItemClickListener mOnClickListener;

    public PathRecyclerAdapter(Context mContext, List<Directory> mDiretories) {
        this.mContext = mContext;
        this.mDiretories = mDiretories;
    }

    @NonNull
    @Override
    public DirectoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.directory_item, parent, false);
        return new DirectoryViewHolder(view, mOnClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DirectoryViewHolder holder, int position) {

        Directory directory = mDiretories.get(position);
        holder.getDirectoryName().setText(directory.getName());
    }

    public void setOnClickListener(OnRecyclerItemClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemCount() {
        return mDiretories.size();
    }
}