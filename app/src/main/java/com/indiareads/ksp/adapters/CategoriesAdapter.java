package com.indiareads.ksp.adapters;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.indiareads.ksp.R;
import com.indiareads.ksp.glide.SvgDecoder;
import com.indiareads.ksp.glide.SvgDrawableTranscoder;
import com.indiareads.ksp.glide.SvgSoftwareLayerSetter;
import com.indiareads.ksp.listeners.OnCategoryClickListener;
import com.indiareads.ksp.model.Category;
import com.indiareads.ksp.viewholder.CategoryChildViewHolder;
import com.indiareads.ksp.viewholder.CategoryParentViewHolder;

import java.io.InputStream;
import java.util.List;

import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_TYPE_CHILD;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_TYPE_PARENT;

/**
 * Created by Kashish on 23-08-2018.
 */

public class CategoriesAdapter extends RecyclerView.Adapter {

    private static final String TAG = CategoriesAdapter.class.getSimpleName();
    private Context mContext;
    private List<Category> mCategories; //Users list
    private OnCategoryClickListener mParentListener;
    private OnCategoryClickListener mChildListener;
    private String selectedId = "", svgImage = "";
    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;

    public CategoriesAdapter(List<Category> categories, OnCategoryClickListener listener, String categoryId, String svgImage) {
        this.mCategories = categories;
        this.mParentListener = listener;
        this.mChildListener = listener;
        this.svgImage = svgImage;

        if (!mCategories.isEmpty()) {
            selectedId = categoryId;
        }

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        mContext = parent.getContext();
        switch (viewType) {
            case CATEGORY_TYPE_PARENT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_category_single_item, parent, false);
                return new CategoryParentViewHolder(view, mCategories, mParentListener);
            case CATEGORY_TYPE_CHILD:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_category_single_item, parent, false);
                return new CategoryChildViewHolder(view, mCategories, mChildListener);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == CATEGORY_TYPE_PARENT) {
            Category category = mCategories.get(position);
            CategoryParentViewHolder categoryParentViewHolder = (CategoryParentViewHolder) holder;
            categoryParentViewHolder.setCategoryName(category.getCategoryName());

            if (selectedId.equals(category.getCategoryId())) {
                categoryParentViewHolder.getTextView().setBackgroundColor(ContextCompat.getColor(mContext, R.color.progress_background));
            } else {
                categoryParentViewHolder.getTextView().setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            }
        } else {
            Category category = mCategories.get(position);
            CategoryChildViewHolder categoryChildViewHolder = (CategoryChildViewHolder) holder;
            categoryChildViewHolder.setCategoryName(category.getCategoryName());

        }
    }

    public void setSelectedId(String id) {
        selectedId = id;
        notifyDataSetChanged();
    }

    public void clearList() {
        mCategories.clear();
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mCategories.get(position).getCategoryType();
    }
}
