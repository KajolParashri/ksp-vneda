package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolderGrid;

import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;

public class PopularContentAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<Content> mContents;
    View view;
    private OnRecyclerItemClickListener mOnClickListener;

    class PopularViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textTile, typeContent;
        public ImageView imgTile, contentTypeICon;
        RelativeLayout dummyLayout;
        TextView dummyTitle;

        public PopularViewHolder(View itemView, OnRecyclerItemClickListener listener) {
            super(itemView);
            view = itemView;

            dummyLayout = view.findViewById(R.id.dummyLayout);
            dummyTitle = view.findViewById(R.id.dummyTitle);
            contentTypeICon = view.findViewById(R.id.contentTypeICon);
            typeContent = view.findViewById(R.id.typeContent);
            imgTile = view.findViewById(R.id.imgTile);
            textTile = view.findViewById(R.id.textTile);
            mOnClickListener = listener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnClickListener.onClick(v, getAdapterPosition());
        }

    }


    public PopularContentAdapter(Context mContext, List<Content> mContents) {
        this.mContext = mContext;
        this.mContents = mContents;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_shimmer, parent, false);
                return new ProgressBarViewHolderGrid(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_grid, parent, false);
                return new PopularViewHolder(view, mOnClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Content content = mContents.get(position);
        if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            ProgressBarViewHolderGrid progressBarViewHolder = (ProgressBarViewHolderGrid) holder;
            progressBarViewHolder.getShimmer().startShimmerAnimation();
        } else if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {

            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(content.getThumbnail()));
            emptyListViewHolder.getTextView().setText(content.getTitle());

        } else {

            PopularViewHolder contentViewHolder = (PopularViewHolder) holder;
            String thumbNail = content.getThumnail();
//            CommonMethods.loadContentImageWithGlide(mContext, thumbNail, contentViewHolder.imgTile);


            if (thumbNail != null && !thumbNail.equals("") && !thumbNail.equals("0")) {
                Logger.error("URL", thumbNail);
                contentViewHolder.imgTile.setVisibility(View.VISIBLE);
                contentViewHolder.dummyLayout.setVisibility(View.GONE);
                contentViewHolder.dummyTitle.setVisibility(View.GONE);
                if (content.getContentType().equals("9")) {
                    thumbNail = Urls.BOOK_COVER_IMAGE_BASE + content.getThumnail();
                } else {
                    thumbNail = content.getThumnail();
                }
                CommonMethods.loadContentImageWithGlide(mContext, thumbNail, contentViewHolder.imgTile);
            } else {
                contentViewHolder.imgTile.setVisibility(View.GONE);
                contentViewHolder.dummyLayout.setVisibility(View.VISIBLE);
                contentViewHolder.dummyTitle.setVisibility(View.VISIBLE);
                int color = generateRandomColor();

                contentViewHolder.dummyLayout.setBackgroundColor(color);

                String title = content.getTitle();
                title = Html.fromHtml(title).toString();
                title = title.replace("'", "''");
                if (title.length() > 25) {
                    title = title.substring(0, 25);
                    contentViewHolder.dummyTitle.setText(title + "..");
                } else {
                    contentViewHolder.dummyTitle.setText(title);
                }

            }

            String title = content.getTitle();

            if (title.length() > 20) {

                title = title.substring(0, 20);
                title += "...";
                contentViewHolder.textTile.setText(title);

            } else {
                contentViewHolder.textTile.setText(title);
            }

            String stype = content.getContentType();
            int resId = CommonMethods.getContentImageFromType(stype);
            contentViewHolder.contentTypeICon.setImageResource(resId);
            String type = CommonMethods.getContentNameFromType(stype);

            contentViewHolder.typeContent.setText(type);
        }
    }

    public void setOnClickListener(OnRecyclerItemClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mContents.get(position).getContentType());
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}