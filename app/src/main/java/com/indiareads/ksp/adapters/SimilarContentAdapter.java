package com.indiareads.ksp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.viewholder.SimilarContentViewHolder;

import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;

public class SimilarContentAdapter extends RecyclerView.Adapter<SimilarContentViewHolder> {

    Context mContext;
    List<Content> mContents;

    public SimilarContentAdapter(Context mContext, List<Content> mContents) {
        this.mContext = mContext;
        this.mContents = mContents;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mContents.get(position).getContentType());
    }

    @NonNull
    @Override
    public SimilarContentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == Constants.Content.CONTENT_TYPE_PROGRESS_LOADER)
            view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_progress_loader, parent, false);
        else
            view = LayoutInflater.from(mContext).inflate(R.layout.linearlisting_categoryt, parent, false);

        return new SimilarContentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimilarContentViewHolder contentViewHolder, int position) {

        if (contentViewHolder.getItemViewType() != Constants.Content.CONTENT_TYPE_PROGRESS_LOADER) {

            Content content = mContents.get(position);

            String dummy = content.getThumnail();

            if (dummy != null && !dummy.equals("") && !dummy.equals("0")) {
                contentViewHolder.getContentImage().setVisibility(View.VISIBLE);
                contentViewHolder.getDummyLay().setVisibility(View.GONE);
                CommonMethods.loadContentImageWithGlide(mContext, dummy, contentViewHolder.getContentImage());

            } else {

                contentViewHolder.getContentImage().setVisibility(View.GONE);
                contentViewHolder.getDummyLay().setVisibility(View.VISIBLE);

                int color = generateRandomColor();

                contentViewHolder.getDummyLay().setBackgroundColor(color);

                String title = content.getTitle();
                title = String.valueOf(Html.fromHtml(title));
                if (title.length() > 30) {

                    title = title.substring(0, 30);
                    contentViewHolder.getTitleDummy().setText(title + "..");

                } else {
                    contentViewHolder.getTitleDummy().setText(title);
                }

                if (content.getAuthorName() != null && content.getAuthorName().equals("0") && content.getAuthorName().equals("")) {
                    contentViewHolder.getSourceDummy().setText(content.getAuthorName());
                } else {
                    contentViewHolder.getSourceDummy().setText(content.getSourceName());
                }

            }


            CommonMethods.loadDrawableWithGlide(mContext, CommonMethods.getIconImage(content.getContentType()), contentViewHolder.getContentIconImage());

            contentViewHolder.getTypeText().setText(CommonMethods.getContentNameFromType(content.getContentType()));
            String text, desc;
            //  holder.getRatingBar().setVisibility(View.GONE);
            text = content.getTitle();

            text = String.valueOf(Html.fromHtml(text));
            desc = content.getDescription();
            if (text.length() >= 25) {
                text = text.substring(0, 25);
                text = text + "..";
                contentViewHolder.getContentTitle().setText(text);
            } else {
                contentViewHolder.getContentTitle().setText(text);
            }

            if (desc.length() >= 40) {
                desc = desc.substring(0, 40);
                desc = desc + "..";
                contentViewHolder.getContentDescription().setText(desc);
            } else {
                contentViewHolder.getContentDescription().setText(desc);
            }

            contentViewHolder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startProductActivity(mContents.get(contentViewHolder.getAdapterPosition()));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }

    private void startProductActivity(Content content) {
        Intent intent = new Intent(mContext, ProductActivity.class);
        intent.putExtra(Constants.Network.CONTENT_ID, content.getContentId());
        intent.putExtra(Constants.Network.CONTENT_TYPE, content.getContentType());
        intent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, content.getStatusType());
        mContext.startActivity(intent);
    }
}
