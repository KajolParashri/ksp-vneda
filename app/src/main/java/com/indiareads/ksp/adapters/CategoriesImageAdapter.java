package com.indiareads.ksp.adapters;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.indiareads.ksp.R;
import com.indiareads.ksp.glide.SvgDecoder;
import com.indiareads.ksp.glide.SvgDrawableTranscoder;
import com.indiareads.ksp.glide.SvgSoftwareLayerSetter;
import com.indiareads.ksp.listeners.OnCategoryImageClickListener;
import com.indiareads.ksp.model.Category;
import com.indiareads.ksp.viewholder.ParentCategoryImagesViewHolder;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Kashish on 10-09-2018.
 */

public class CategoriesImageAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private OnCategoryImageClickListener mListener;
    private List<Category> mCategories;
    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;

    public CategoriesImageAdapter(List<Category> categories, OnCategoryImageClickListener listener) {
        this.mCategories = categories;
        this.mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        mContext = parent.getContext();
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_category_display_single_item, parent, false);
        return new ParentCategoryImagesViewHolder(view, mContext, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ParentCategoryImagesViewHolder parentCategoryImagesViewHolder = (ParentCategoryImagesViewHolder) holder;
        parentCategoryImagesViewHolder.setCategoryImageData(mCategories.get(position));
    }
    @Override
    public int getItemCount() {
        return mCategories.size();
    }
}
