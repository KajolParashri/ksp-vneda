package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.viewholder.AnnouncementViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;
import com.indiareads.ksp.viewholder.ProgressBarViewHolder;

import java.util.List;

public class AnnouncementRecyclerAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<Content> mContents;

    public AnnouncementRecyclerAdapter(Context mContext, List<Content> mContents) {
        this.mContext = mContext;
        this.mContents = mContents;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.Content.CONTENT_TYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
                return new EmptyListViewHolder(view);
            case Constants.Content.CONTENT_TYPE_PROGRESS_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_progress_loader, parent, false);
                return new ProgressBarViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.announcement_item, parent, false);
                return new AnnouncementViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Content content = mContents.get(position);
        if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            // do nothing
        } else if (content.getContentType().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {
            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(content.getThumbnail()));
            emptyListViewHolder.getTextView().setText(content.getTitle());
        } else {
            AnnouncementViewHolder announcementViewHolder = (AnnouncementViewHolder) holder;
            announcementViewHolder.getHomeAnnouncementItemDescription().setText(content.getDescription());
            announcementViewHolder.getHomeAnnouncementItemCreateDate().setText(CommonMethods.getElapsedTime(content.getCreatedDate()));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mContents.get(position).getContentType());
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}
