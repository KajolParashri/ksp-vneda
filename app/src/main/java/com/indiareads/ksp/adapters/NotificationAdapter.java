package com.indiareads.ksp.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.NotificationModel;
import com.indiareads.ksp.utils.CircleTransform;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Urls;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    public List<NotificationModel> mNotiList;
    public OnRecyclerItemClickListener mListener;
    Activity mContext;

    class NotificationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView notificationText, dateTime;
        public ImageView profilePic;
        public OnRecyclerItemClickListener listener;
        RelativeLayout mainLayout;
        ShimmerFrameLayout shimmerFrameLayout;

        public NotificationViewHolder(View itemView, OnRecyclerItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            shimmerFrameLayout = itemView.findViewById(R.id.shimmer_view_container);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            notificationText = itemView.findViewById(R.id.notificationText);
            dateTime = itemView.findViewById(R.id.datetime);
            profilePic = itemView.findViewById(R.id.userImage);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(view, getAdapterPosition());
        }
    }

    public NotificationAdapter(Activity context, List<NotificationModel> mNotifiList, OnRecyclerItemClickListener listener) {
        mNotiList = mNotifiList;
        mListener = listener;
        mContext = context;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = parent;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        row = inflater.inflate(R.layout.notificationitem, parent, false);

        return new NotificationViewHolder(row, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {

        NotificationModel notificationModel = mNotiList.get(position);

        if (notificationModel.getItemType().equals("Progress")) {

            holder.shimmerFrameLayout.setVisibility(View.VISIBLE);
            holder.shimmerFrameLayout.startShimmerAnimation();
            holder.mainLayout.setVisibility(View.GONE);

        } else {

            holder.shimmerFrameLayout.stopShimmerAnimation();
            holder.shimmerFrameLayout.setVisibility(View.GONE);
            holder.mainLayout.setVisibility(View.VISIBLE);

            if (!notificationModel.getActivityStatus().equals("0")) {
                holder.mainLayout.setBackgroundResource(R.color.tilecolor);
            } else {
                holder.mainLayout.setBackgroundResource(R.color.white);
            }

            if (position != RecyclerView.NO_POSITION) {

                String mainMsg;
                if (notificationModel.getActivityValue().equals("commented")) {
                    mainMsg = notificationModel.getActivityType() + " on your" + notificationModel.getActivityValue();
                } else {
                    mainMsg = notificationModel.getActivityType() + " your " + notificationModel.getActivityValue();
                }

                holder.notificationText.setText(notificationModel.getUserName() + " " + mainMsg);

                holder.dateTime.setText(CommonMethods.getElapsedTime(notificationModel.getCreatedDate()));

                if (!notificationModel.getProfileURL().equals("null")) {
                    String url = Urls.baseNotificationUserUrl + notificationModel.getProfileURL();
                    Picasso.get().load(url).transform(new CircleTransform()).into(holder.profilePic);
                } else {
                    holder.profilePic.setImageDrawable(CommonMethods.setCircularImage(R.drawable.profile, mContext));
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mNotiList.size();
    }

    @Override
    public long getItemId(int position) {
        Object listItem = mNotiList.get(position);
        return listItem.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}