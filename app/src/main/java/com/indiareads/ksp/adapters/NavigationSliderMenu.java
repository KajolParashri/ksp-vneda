package com.indiareads.ksp.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.ExploreModel;
import com.indiareads.ksp.model.NavigationModel;
import com.indiareads.ksp.model.NotificationModel;

import java.util.List;

public class NavigationSliderMenu extends RecyclerView.Adapter<NavigationSliderMenu.NotificationViewHolder> {

    public List<NavigationModel> menuList;
    Activity mContext;


    class NotificationViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout small, large;
        public TextView item_text, heading;


        public NotificationViewHolder(View itemView) {
            super(itemView);

            heading = itemView.findViewById(R.id.heading);
            small = itemView.findViewById(R.id.small);
            large = itemView.findViewById(R.id.large);
            item_text = itemView.findViewById(R.id.item_text);
        }
    }

    public NavigationSliderMenu(Activity context, List<NavigationModel> MenuList) {
        menuList = MenuList;
        mContext = context;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = parent;

        LayoutInflater inflater = LayoutInflater.from(mContext);//(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(R.layout.navigation_item, parent, false);

        return new NotificationViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {

        final NavigationModel dataModel = menuList.get(position);

        if (dataModel.getMenuType().equals("Heading")) {
            holder.small.setVisibility(View.GONE);
            holder.large.setVisibility(View.VISIBLE);
            holder.heading.setText(dataModel.getMenuName());
        } else if (dataModel.getMenuType().equals("Item")) {

            holder.small.setVisibility(View.VISIBLE);
            holder.large.setVisibility(View.GONE);
            holder.item_text.setText(dataModel.getMenuName());

        }

    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    @Override
    public long getItemId(int position) {
        Object listItem = menuList.get(position);
        return listItem.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}