package com.indiareads.ksp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.indiareads.ksp.R;
import com.indiareads.ksp.explore.fragment.DynamicExploreActivity;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.model.BookMainModel;
import com.indiareads.ksp.model.ExploreModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.view.activity.CategoryListingActivity;
import com.indiareads.ksp.view.activity.HomeActivity;
import com.indiareads.ksp.view.activity.LeaderBoardActivity;
import com.indiareads.ksp.view.activity.ListingActivity;
import com.indiareads.ksp.view.activity.MessageActivity;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.view.activity.SearchActivity;
import com.indiareads.ksp.view.activity.UserDetailsActivity;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;
import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_NAME;
import static com.indiareads.ksp.utils.Constants.Network.CATEGORY_ID;

public class BookMainAdapter extends RecyclerView.Adapter<BookMainAdapter.BookViewHolder> {

    public List<ExploreModel> exploreList;
    Context mContext;
    OnItemClick mListener;

    class BookViewHolder extends RecyclerView.ViewHolder {
        public TextView textTile, typeContent;
        public ImageView imgTile;
        public RelativeLayout viewAllButton;
        public RelativeLayout mainLayout;
        public RelativeLayout dummyLay;
        public TextView dummyText;

        public BookViewHolder(View itemView, OnItemClick listener) {

            super(itemView);

            dummyLay = itemView.findViewById(R.id.dummyLay);
            dummyText = itemView.findViewById(R.id.dummyText);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            viewAllButton = itemView.findViewById(R.id.viewAllButton);
            typeContent = itemView.findViewById(R.id.typeContent);
            imgTile = itemView.findViewById(R.id.imgTile);
            textTile = itemView.findViewById(R.id.textTile);
        }
    }

    public BookMainAdapter(Context context, List<ExploreModel> mExploreList, OnItemClick listener) {
        exploreList = mExploreList;
        mListener = listener;
        mContext = context;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = parent;

        LayoutInflater inflater = LayoutInflater.from(mContext);//(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(R.layout.explore_item, parent, false);

        return new BookViewHolder(row, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {

        final ExploreModel dataModel = exploreList.get(position);

        if (dataModel.getIntent_type().equals("ViewAll")) {
            holder.viewAllButton.setVisibility(View.VISIBLE);
            holder.mainLayout.setVisibility(View.GONE);
        } else {
            holder.viewAllButton.setVisibility(View.GONE);

            holder.mainLayout.setVisibility(View.VISIBLE);

            if (dataModel.getThumnail() != null && !dataModel.getThumnail().equals("")) {
                Picasso.get().load(dataModel.getThumnail()).into(holder.imgTile);
            } else {

                holder.imgTile.setVisibility(View.GONE);
                holder.dummyLay.setVisibility(View.VISIBLE);
                holder.dummyText.setVisibility(View.VISIBLE);
                int color = generateRandomColor();

                holder.dummyLay.setBackgroundColor(color);
                String title = dataModel.getTitle();

                if (title.length() > 25) {
                    title = title.substring(0, 25);
                    holder.dummyText.setText(title + "..");
                } else {
                    holder.dummyText.setText(title);
                }
            }

            String title = dataModel.getTitle();
            if (title.length() < 20) {
                holder.textTile.setText(dataModel.getTitle());
            } else {
                title = title.substring(0, 20);
                holder.textTile.setText(title + "..");
            }
            String contentType = CommonMethods.getContentNameFromType(dataModel.getContent_type());
            holder.typeContent.setText(contentType);


        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dataModel.getIntent_type().equals("ViewAll")) {
                    Intent intent = new Intent(mContext, DynamicExploreActivity.class);
                    intent.putExtra("SETVALUE", dataModel.getIntent_parameter_1());
                    mContext.startActivity(intent);

                } else {
                    String type = dataModel.getIntent_type();
                    String param1 = dataModel.getIntent_parameter_1();
                    String param2 = dataModel.getIntent_parameter_2();
                    String param3 = dataModel.getIntent_parameter_3();
                    setupClick(type, param1, param2, param3);
                }

            }
        });
    }

    public void setupClick(String type, String param1, String param2, String param3) {

        if (type.equals("1")) {
            ((HomeActivity) mContext).setUpFragmentLayout(R.id.bottom_explore);
        } else if (type.equals("2")) {
            Intent intent = new Intent(mContext, CategoryListingActivity.class);
            intent.putExtra(CATEGORY_ID, param1);
            intent.putExtra(CATEGORY_NAME, param2);
            mContext.startActivity(intent);
        } else if (type.equals("3")) {
            Intent intent = new Intent(mContext, ListingActivity.class);
            intent.putExtra(Constants.Content.CONTENT_TYPE, param1);
            mContext.startActivity(intent);
        } else if (type.equals("4")) {
            ((HomeActivity) mContext).setUpFragmentLayout(R.id.bottom_mycompany);
        } else if (type.equals("5")) {
            Intent userProfileIntent = new Intent(mContext, UserDetailsActivity.class);
            userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 0);
            userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(mContext).getUserId());
            userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(mContext).getCompanyId());
            mContext.startActivity(userProfileIntent);
        } else if (type.equals("6")) {
            mContext.startActivity(new Intent(mContext, MessageActivity.class));
        } else if (type.equals("7")) {
            Intent userProfileIntent = new Intent(mContext, UserDetailsActivity.class);
            userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 4);
            userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(mContext).getUserId());
            userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(mContext).getCompanyId());
            mContext.startActivity(userProfileIntent);
        } else if (type.equals("8")) {
            ((HomeActivity) mContext).setUpFragmentLayout(R.id.bottom_create);
        } else if (type.equals("9")) {
            Intent searchActivityIntent = new Intent(mContext, SearchActivity.class);
            searchActivityIntent.putExtra(SearchActivity.SEARCH_QUERY, param2);
            mContext.startActivity(searchActivityIntent);
        } else if (type.equals("10")) {
            Intent userProfileIntent = new Intent(mContext, UserDetailsActivity.class);
            userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 6);
            userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(mContext).getUserId());
            userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(mContext).getCompanyId());
            mContext.startActivity(userProfileIntent);
        } else if (type.equals("11")) {
            ((HomeActivity) mContext).setUpFragmentLayout(R.id.bottom_mycompany);
        } else if (type.equals("12")) {
            //leadership board
            Intent userProfileIntent = new Intent(mContext, LeaderBoardActivity.class);
            mContext.startActivity(userProfileIntent);

        } else if (type.equals("13")) {
            Intent userProfileIntent = new Intent(mContext, ProductActivity.class);
            userProfileIntent.putExtra(Constants.Network.CONTENT_ID, param1);
            userProfileIntent.putExtra(Constants.Network.CONTENT_TYPE, param2);
            userProfileIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, param3);
            mContext.startActivity(userProfileIntent);
        }

    }

    @Override
    public int getItemCount() {
        return exploreList.size();
    }

    @Override
    public long getItemId(int position) {
        Object listItem = exploreList.get(position);
        return listItem.hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}