package com.indiareads.ksp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnFilterCategoryClickListener;
import com.indiareads.ksp.model.Category;
import com.indiareads.ksp.viewholder.ParentCategoryViewHolder;

import java.util.ArrayList;
import java.util.List;

public class FilterFragmentRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Category> categoryList;
    private ArrayList<String> filterCategoryList;
    private OnFilterCategoryClickListener onFilterCategoryClickListener;

    public FilterFragmentRecyclerAdapter(Context context, List<Category> categoryList,ArrayList<String> filterCategoryList) {
        this.context = context;
        this.categoryList = categoryList;
        this.filterCategoryList = filterCategoryList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.parent_category_item, parent, false);
        return new ParentCategoryViewHolder(view, onFilterCategoryClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Category category = categoryList.get(position);

        ParentCategoryViewHolder parentCategoryViewHolder = (ParentCategoryViewHolder) holder;
        parentCategoryViewHolder.getCategoryNameTextView().setText(category.getCategoryName());

        parentCategoryViewHolder.getChildCategoryRecyclerView().setLayoutManager(new LinearLayoutManager(context));
        FilterFragmentChildRecyclerAdapter filterFragmentChildRecyclerAdapter = new FilterFragmentChildRecyclerAdapter(context, category.getChildCategories(),position,filterCategoryList);
        parentCategoryViewHolder.getChildCategoryRecyclerView().setAdapter(filterFragmentChildRecyclerAdapter);
        filterFragmentChildRecyclerAdapter.setOnClickListener(onFilterCategoryClickListener);
    }

    public void setOnClickListener(OnFilterCategoryClickListener onFilterCategoryClickListener) {
        this.onFilterCategoryClickListener = onFilterCategoryClickListener;
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}
