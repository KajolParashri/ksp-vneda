package com.indiareads.ksp.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.Chat;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.viewholder.ChatViewHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;

import java.io.File;
import java.util.ArrayList;

public class ChatActivityRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = MessageActivityRecyclerAdapter.class.getSimpleName();
    private Context mContext;
    private ArrayList<Chat> mChats;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public ChatActivityRecyclerAdapter(Context mContext, ArrayList<Chat> mChats) {
        this.mContext = mContext;
        this.mChats = mChats;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mChats.get(position).getMessageType());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == Constants.Content.CONTENT_TYPE_PROGRESS_LOADER) {
            view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_progressbar_item, parent, false);
            return new ChatViewHolder(view, onRecyclerItemClickListener);
        } else if (viewType == Constants.Content.CONTENT_TYPE_ERROR) {
            view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_empty_item, parent, false);
            return new EmptyListViewHolder(view);
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.chat_item, parent, false);
            return new ChatViewHolder(view, onRecyclerItemClickListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewholder, int position) {


        if (position != RecyclerView.NO_POSITION) {
            // Do your binding here

            Chat chat = mChats.get(position);

            if (viewholder.getItemViewType() == Constants.Content.CONTENT_TYPE_ERROR) {
                EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) viewholder;
                emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(chat.getUserImage()));
                emptyListViewHolder.getTextView().setText(chat.getMessage());
            } else if (viewholder.getItemViewType() == Constants.Content.CONTENT_TYPE_PROGRESS_LOADER) {

            } else {
                ChatViewHolder holder = (ChatViewHolder) viewholder;
                holder.getNameTextView().setText(chat.getUserFullname());

                holder.getImageLayout().setVisibility(View.GONE);
                holder.getMessageTextView().setVisibility(View.GONE);
                holder.getAttachmentTextView().setVisibility(View.GONE);
                holder.getImageViewLeft().setVisibility(View.GONE);
                holder.getImageViewRight().setVisibility(View.GONE);
                holder.getProgressBar().setVisibility(View.GONE);

                if (chat.getAttachmentId().equals(Constants.Message.MESSAGE_TYPE_TEXT_VALUE)) {
                    holder.getMessageTextView().setVisibility(View.VISIBLE);
                    holder.getMessageTextView().setText(chat.getMessage());
                } else {
                    if (CommonMethods.getFileTypeFromName(chat.getAttachmentName()).equals(Constants.File.FILE_TYPE_IMAGE)) {
                        holder.getImageLayout().setVisibility(View.VISIBLE);
                        if (chat.getSyncStatus().equals(Constants.Message.SYNC_STATUS_DONE)) {
                            loadImage(holder.getItemView(), Urls.AWS_MESSAGE_IMAGE_PATH + chat.getAttachmentId(), holder.getMessageImage());
                        } else {
                            loadImage(holder.getItemView(), new File(Constants.File.ROOT_DIR_PATH + "/" + chat.getAttachmentName()), holder.getMessageImage());
                        }
                    } else {
                        holder.getAttachmentTextView().setVisibility(View.VISIBLE);
                        holder.getAttachmentTextView().setText(chat.getAttachmentName());
                    }
                }

                if (chat.getSenderId().equals(User.getCurrentUser(mContext).getUserId())) {
                    holder.getChatRootLayout().setGravity(Gravity.END);
                    CommonMethods.loadProfileImageWithGlide(mContext, chat.getUserImage(), holder.getImageViewRight());
                    holder.getImageViewRight().setVisibility(View.VISIBLE);

                    if (chat.getSyncStatus().equals(Constants.Message.SYNC_STATUS_FAILED)) {
                        CommonMethods.loadDrawableWithGlide(mContext, R.drawable.ic_error_outline_red, holder.getImageViewLeft());
                        ImageViewCompat.setImageTintList(holder.getImageViewLeft(), ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.colorPrimary)));
                        holder.getImageViewLeft().setVisibility(View.VISIBLE);
                        holder.getProgressBar().setVisibility(View.GONE);
                    } else if (chat.getSyncStatus().equals(Constants.Message.SYNC_STATUS_IN_PROGRESS) && chat.getMessageType().equals(Constants.Message.MESSAGE_TYPE_FILE)) {
                        holder.getImageViewLeft().setVisibility(View.GONE);
                        holder.getProgressBar().setVisibility(View.VISIBLE);
                    } else {
                        holder.getProgressBar().setVisibility(View.GONE);
                        holder.getImageViewLeft().setVisibility(View.GONE);
                    }

                } else {
                    holder.getChatRootLayout().setGravity(Gravity.START);
                    CommonMethods.loadProfileImageWithGlide(mContext, chat.getUserImage(), holder.getImageViewLeft());
                    holder.getImageViewLeft().setVisibility(View.VISIBLE);

                    if (chat.getSyncStatus().equals(Constants.Message.SYNC_STATUS_FAILED)) {
                        CommonMethods.loadDrawableWithGlide(mContext, R.drawable.ic_error_outline_red, holder.getImageViewRight());
                        ImageViewCompat.setImageTintList(holder.getImageViewRight(), ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.colorPrimary)));
                        holder.getImageViewRight().setVisibility(View.VISIBLE);
                    } else if (chat.getSyncStatus().equals(Constants.Message.SYNC_STATUS_IN_PROGRESS)) {
                        //chat item should not be visible, so code should not reach here
                    } else
                        holder.getImageViewRight().setVisibility(View.GONE);
                }
            }

        }
    }

    private void loadImage(final View view, String url, ImageView imageView) {
        Logger.info("IMAGE-URL", url);
        Glide.with(mContext)
                .load(url)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Logger.error(TAG, "IMG_EXC:" + e.getMessage());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        view.findViewById(R.id.chat_image_gradient).setVisibility(View.INVISIBLE);
                        view.findViewById(R.id.chat_image_progressbar).setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .error(R.mipmap.ic_fevicon)
                .placeholder(R.mipmap.ic_fevicon)
                .into(imageView);
    }

    private void loadImage(final View view, File file, ImageView imageView) {
        Glide.with(mContext)
                .load(file)
                .listener(new RequestListener<File, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, File model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, File model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        view.findViewById(R.id.chat_image_gradient).setVisibility(View.INVISIBLE);
                        view.findViewById(R.id.chat_image_progressbar).setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .error(R.mipmap.ic_fevicon)
                .placeholder(R.mipmap.ic_fevicon)
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return mChats.size();
    }

    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }
}