package com.indiareads.ksp.explore.holder;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.indiareads.ksp.R;

public class BannerViewHolder extends RecyclerView.ViewHolder {
    public RecyclerView recyclerView;

    public BannerViewHolder(View itemView, Context context) {
        super(itemView);
        recyclerView = itemView.findViewById(R.id.exploreBanner);
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }
}
