package com.indiareads.ksp.explore.holder;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indiareads.ksp.R;

public class Multi_Book_Holder extends RecyclerView.ViewHolder {
    public RecyclerView recyclerView;
    public TextView heading;

    public Multi_Book_Holder(View itemView, Context context) {
        super(itemView);
        recyclerView = itemView.findViewById(R.id.parent_book_item);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        heading = itemView.findViewById(R.id.heading);
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }
}
