package com.indiareads.ksp.explore.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.explore.holder.GridContentHolder;
import com.indiareads.ksp.explore.mode.GridModel;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.viewholder.CircularProgressHolder;
import com.indiareads.ksp.viewholder.EmptyListViewHolder;

import java.util.List;

import static com.indiareads.ksp.utils.CommonMethods.generateRandomColor;

public class GridMainAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<GridModel> mContents;
    //private OnRecyclerItemClickListener mOnClickListener;


    public GridMainAdapter(Context mContext, List<GridModel> mContents) {
        this.mContext = mContext;
        this.mContents = mContents;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        if (viewType == Constants.Content.CONTENT_TYPE_ERROR) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_empty_item, parent, false);
            return new EmptyListViewHolder(view);
        } else if (viewType == Constants.Content.CONTENT_TYPE_PROGRESS_LOADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_loader_progress, parent, false);
            return new CircularProgressHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.linearlisting_categoryt, parent, false);
            return new GridContentHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        GridModel content = mContents.get(position);
        if (content.getContent_type().equals(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER))) {
            CircularProgressHolder progressBarViewHolder = (CircularProgressHolder) holder;
        } else if (content.getContent_type().equals(String.valueOf(Constants.Content.CONTENT_TYPE_ERROR))) {

            EmptyListViewHolder emptyListViewHolder = (EmptyListViewHolder) holder;
            emptyListViewHolder.getImageView().setImageResource(Integer.parseInt(content.getThumbnail()));
            emptyListViewHolder.getTextView().setText(content.getTitle());

        } else {

            GridContentHolder contentViewHolder = (GridContentHolder) holder;
            String imagePath = "";

            if (content.getContent_type().equals("9")) {
                imagePath = Urls.BOOK_COVER_IMAGE_BASE + content.getThumbnail();
            } else {
                imagePath = content.getThumbnail();
            }

            String dummy = content.getThumbnail();

            if (dummy != null && !dummy.equals("") && !dummy.equals("0")) {
                contentViewHolder.getContentImage().setVisibility(View.VISIBLE);
                contentViewHolder.getDummyLay().setVisibility(View.GONE);
                CommonMethods.loadContentImageWithGlide(mContext, imagePath, contentViewHolder.getContentImage());
            } else {

                contentViewHolder.getContentImage().setVisibility(View.GONE);
                contentViewHolder.getDummyLay().setVisibility(View.VISIBLE);

                int color = generateRandomColor();
                contentViewHolder.getDummyLay().setBackgroundColor(color);

                String title = content.getTitle();

                if (title.length() > 30) {

                    title = title.substring(0, 30);
                    contentViewHolder.getTitleDummy().setText(title + "..");

                } else {
                    contentViewHolder.getTitleDummy().setText(title);
                }

                if (content == null || content.getAuthor().equals("0") || content.getAuthor().equals("")) {
                    contentViewHolder.getSourceDummy().setText(content.getSource_name());
                } else {
                    contentViewHolder.getSourceDummy().setText(content.getAuthor());
                }
            }

            String title = content.getTitle();
            final TextView titleView = contentViewHolder.getTextContent();
            titleView.setText(title);
            ViewTreeObserver vto = titleView.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {

                    ViewTreeObserver obs = titleView.getViewTreeObserver();
                    obs.removeOnPreDrawListener(this);
                    if (titleView.getLineCount() > 2) {
                        Logger.debug("", "Line[" + titleView.getLineCount() + "]" + titleView.getText());
                        int lineEndIndex = titleView.getLayout().getLineEnd(1);
                        String text = titleView.getText().subSequence(0, lineEndIndex - 4) + "...";
                        titleView.setText(text);
                        Logger.debug("", "NewText:" + text);
                    }
                    return true;

                }

            });

            String stype = content.getContent_type();
            int resId = CommonMethods.getContentImageFromType(stype);
            contentViewHolder.getImg_Content().setImageResource(resId);
            String type = CommonMethods.getContentNameFromType(stype);

            Integer rating = content.getRating();
            if (rating == null || rating == 0) {

            } else {
                contentViewHolder.getRatingBar().setRating(content.getRating());
            }

            contentViewHolder.getTextType().setText(type);
            contentViewHolder.getTxtSource().setText("Source: " + content.getSource_name());

            if (content.getAuthor() != null && content.getAuthor().equals("0")) {
                contentViewHolder.getTxtAuthor().setVisibility(View.GONE);
            } else {
                contentViewHolder.getTxtAuthor().setText("Author: " + content.getAuthor());
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent productIntent = new Intent(mContext, ProductActivity.class);
                    productIntent.putExtra(Constants.Network.CONTENT_ID, mContents.get(position).getContent_id_s());
                    productIntent.putExtra(Constants.Network.CONTENT_TYPE, mContents.get(position).getContent_type());
                    productIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, mContents.get(position).getContent_status_type());
                    mContext.startActivity(productIntent);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mContents.get(position).getContent_type());
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }
}