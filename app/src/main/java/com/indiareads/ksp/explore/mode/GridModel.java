package com.indiareads.ksp.explore.mode;

import com.indiareads.ksp.model.BaseParentModel;

public class GridModel extends BaseParentModel {

    public String title;
    public String author;
    public String description;
    public String thumbnail;
    public String content_id_s;
    public String content_type;
    public String category;
    public String source_name;
    public int rating;
    public String content_status_type;

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getContent_status_type() {
        return content_status_type;
    }

    public void setContent_status_type(String content_status_type) {
        this.content_status_type = content_status_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getContent_id_s() {
        return content_id_s;
    }

    public void setContent_id_s(String content_id_s) {
        this.content_id_s = content_id_s;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }


}

