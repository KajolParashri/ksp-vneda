package com.indiareads.ksp.explore.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.indiareads.ksp.R;

public class DualImageHolder extends RecyclerView.ViewHolder {
    public ImageView firstImage;
    public ImageView secondImage;

    public DualImageHolder(View itemView) {
        super(itemView);
        firstImage = itemView.findViewById(R.id.firstImage);
        secondImage = itemView.findViewById(R.id.secondImage);

    }

    public ImageView getFirstImage() {
        return firstImage;
    }

    public void setFirstImage(ImageView firstImage) {
        this.firstImage = firstImage;
    }

    public ImageView getSecondImage() {
        return secondImage;
    }

    public void setSecondImage(ImageView secondImage) {
        this.secondImage = secondImage;
    }
}
