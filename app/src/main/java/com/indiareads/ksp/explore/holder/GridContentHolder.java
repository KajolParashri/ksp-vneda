package com.indiareads.ksp.explore.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;

public class GridContentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private View view;
    private ImageView contentImage, img_Content;
    private TextView textContent, textType, txtAuthor, txtSource;
    private RatingBar ratingBar;
    private OnRecyclerItemClickListener mOnClickListener;
    RelativeLayout dummyLay;
    TextView titleDummy, sourceDummy;

    public GridContentHolder(View itemView) {
        super(itemView);

        view = itemView;

        titleDummy = itemView.findViewById(R.id.titleDummy);
        sourceDummy = itemView.findViewById(R.id.sourceDummy);

        dummyLay = itemView.findViewById(R.id.dummyLay);
        contentImage = itemView.findViewById(R.id.mainImage);
        textContent = itemView.findViewById(R.id.textContent);
        textType = itemView.findViewById(R.id.textType);
        img_Content = itemView.findViewById(R.id.img_Content);
        txtAuthor = itemView.findViewById(R.id.textAuthor);
        txtSource = itemView.findViewById(R.id.textSource);
        ratingBar = itemView.findViewById(R.id.ratingBar);

        view.setOnClickListener(this);
    }

    public TextView getTitleDummy() {
        return titleDummy;
    }

    public TextView getSourceDummy() {
        return sourceDummy;
    }

    public RelativeLayout getDummyLay() {
        return dummyLay;
    }

    public RatingBar getRatingBar() {
        return ratingBar;
    }

    @Override
    public void onClick(View v) {
        mOnClickListener.onClick(v, getAdapterPosition());
    }

    public ImageView getContentImage() {
        return contentImage;
    }

    public ImageView getImg_Content() {
        return img_Content;
    }

    public TextView getTextContent() {
        return textContent;
    }

    public TextView getTextType() {
        return textType;
    }


    public TextView getTxtAuthor() {
        return txtAuthor;
    }

    public TextView getTxtSource() {
        return txtSource;
    }
}