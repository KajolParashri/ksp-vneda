package com.indiareads.ksp.explore.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.BannerSliderAdapter;
import com.indiareads.ksp.adapters.CategoryAdapter;
import com.indiareads.ksp.adapters.ParentSliderAdapter;
import com.indiareads.ksp.explore.holder.BannerViewHolder;
import com.indiareads.ksp.explore.holder.DualImageHolder;
import com.indiareads.ksp.explore.holder.ExploreViewHolder;
import com.indiareads.ksp.explore.holder.Multi_Book_Holder;
import com.indiareads.ksp.explore.holder.PosterImageModelHolder;
import com.indiareads.ksp.explore.mode.DualPosterModel;
import com.indiareads.ksp.explore.mode.GridModel;
import com.indiareads.ksp.explore.mode.PosterModel;
import com.indiareads.ksp.listeners.OnItemClick;
import com.indiareads.ksp.listeners.OnRecyclerItemClickListener;
import com.indiareads.ksp.model.BannerModel;
import com.indiareads.ksp.model.BookMainModel;
import com.indiareads.ksp.model.CategoryModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.view.activity.CategoryListingActivity;
import com.indiareads.ksp.view.activity.HomeActivity;
import com.indiareads.ksp.view.activity.LeaderBoardActivity;
import com.indiareads.ksp.view.activity.ListingActivity;
import com.indiareads.ksp.view.activity.MessageActivity;
import com.indiareads.ksp.view.activity.ProductActivity;
import com.indiareads.ksp.view.activity.SearchActivity;
import com.indiareads.ksp.view.activity.UserDetailsActivity;
import com.indiareads.ksp.view.fragment.UserDetailsFragment;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static com.indiareads.ksp.utils.Constants.Category.CATEGORY_NAME;
import static com.indiareads.ksp.utils.Constants.Network.CATEGORY_ID;

@RequiresApi(api = Build.VERSION_CODES.M)
public class ExploreAdapter extends RecyclerView.Adapter {

    public int page_number = 1;

    private Context mContext;
    LinkedHashMap<String, ArrayList> data;
    ArrayList<String> itemType;
    LinearLayoutManager linearLayoutManager;
    ArrayList<GridModel> listMain;
    String satVal;

    public ExploreAdapter(Context mContext, ArrayList<String> itemType, LinkedHashMap<String, ArrayList> data, String satVl) {
        this.mContext = mContext;
        this.itemType = itemType;
        this.data = data;
        this.satVal = satVl;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case Constants.ExploreType.banner_slider:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_banner, parent, false);
                return new BannerViewHolder(view, mContext);
            case Constants.ExploreType.poster_image:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poster_image, parent, false);
                return new PosterImageModelHolder(view);
            case Constants.ExploreType.dual_image:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dual_image_poster, parent, false);
                return new DualImageHolder(view);
            case Constants.ExploreType.slider:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.multibook_layout, parent, false);
                return new Multi_Book_Holder(view, mContext);
            case Constants.ExploreType.explore:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_layout_item, parent, false);
                return new ExploreViewHolder(view, mContext);
            case Constants.ExploreType.categories:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_main_layout, parent, false);
                return new Multi_Book_Holder(view, mContext);
            case Constants.ExploreType.grid:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_main_layout, parent, false);
                return new Multi_Book_Holder(view, mContext);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_banner, parent, false);
                return new BannerViewHolder(view, mContext);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        String type = itemType.get(position);
        int typeCount = CommonMethods.returnDynamicType(type);
        if (typeCount == (Constants.ExploreType.banner_slider)) {
            ArrayList list = data.get(type);

            if (list != null && list.get(0) instanceof BannerModel) {
                final ArrayList<BannerModel> mainData = data.get(type);

                BannerSliderAdapter bannerSliderAdapter = new BannerSliderAdapter(mContext, mainData, new OnRecyclerItemClickListener() {
                    @Override
                    public void onClick(View v, int position) {
                        BannerModel bannerModel = mainData.get(position);
                        String type = bannerModel.getIntentType();
                        String param1 = bannerModel.getIntentParameter1();
                        String param2 = bannerModel.getIntent_parameter_2();
                        String param3 = bannerModel.getIntent_parameter_3();
                        setupClick(type, param1, param2, param3);
                        //Product Page	content_id	content_type	content_status_type

                    }
                });
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.getRecyclerView().setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                bannerViewHolder.getRecyclerView().setAdapter(bannerSliderAdapter);
            }

        } else if (typeCount == Constants.ExploreType.poster_image) {
            ArrayList list1 = data.get(type);

            if (list1 != null && list1.get(0) instanceof PosterModel) {
                ArrayList<PosterModel> mainData = data.get(type);
                final PosterModel posterModel = mainData.get(0);
                PosterImageModelHolder posterImageModelHolder = (PosterImageModelHolder) holder;
                Logger.error("ListData", mainData.get(0).getImage_url());
                CommonMethods.loadBannerWithGlide(mContext, mainData.get(0).getImage_url(), posterImageModelHolder.getImage_poster());

                posterImageModelHolder.getImage_poster().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String type = posterModel.getIntent_type();
                        String param1 = posterModel.getIntent_parameter_1();
                        String param2 = posterModel.getIntent_parameter_2();
                        String param3 = posterModel.getIntent_parameter_3();
                        setupClick(type, param1, param2, param3);
                    }
                });

            }
        } else if (typeCount == Constants.ExploreType.dual_image) {
            ArrayList list1 = data.get(type);

            if (list1 != null && list1.get(0) instanceof DualPosterModel) {
                final ArrayList<DualPosterModel> mainData = data.get(type);
                DualImageHolder emptyListViewHolder = (DualImageHolder) holder;

                CommonMethods.loadBannerWithGlide(mContext, mainData.get(0).getFimage_url(), emptyListViewHolder.getFirstImage());
                emptyListViewHolder.getFirstImage().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DualPosterModel dualPosterModel = mainData.get(0);
                        String type = dualPosterModel.getIntent_type();
                        String param1 = dualPosterModel.getIntent_parameter_1();
                        String param2 = dualPosterModel.getIntent_parameter_2();
                        String param3 = dualPosterModel.getIntent_parameter_3();
                        setupClick(type, param1, param2, param3);
                    }
                });
                emptyListViewHolder.getSecondImage().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DualPosterModel dualPosterModel = mainData.get(1);
                        String type = dualPosterModel.getIntent_type();
                        String param1 = dualPosterModel.getIntent_parameter_1();
                        String param2 = dualPosterModel.getIntent_parameter_2();
                        String param3 = dualPosterModel.getIntent_parameter_3();
                        setupClick(type, param1, param2, param3);
                    }
                });

                CommonMethods.loadBannerWithGlide(mContext, mainData.get(1).getSimage_url(), emptyListViewHolder.getSecondImage());

            }
        } else if (typeCount == Constants.ExploreType.slider) {
            ArrayList list1 = data.get(type);

            if (list1 != null && list1.get(0) instanceof BookMainModel) {

                ArrayList<BookMainModel> mainData = data.get(type);
                //for (int i = 0; i < mainData.size(); i++) {

                Multi_Book_Holder bookAdapterHolder = (Multi_Book_Holder) holder;
                //   bookAdapterHolder.getHeading().setText(mainData.get(i).getHeadingText());

                ParentSliderAdapter bookMainAdapter = new ParentSliderAdapter(mContext, mainData, new OnItemClick() {
                    @Override
                    public void onArticleClick(String tileName) {

                    }

                    @Override
                    public void onInfoClick(String tileName) {

                    }

                    @Override
                    public void onCategoryClick(int tileName) {

                    }
                });

                bookAdapterHolder.getRecyclerView().setLayoutManager(new LinearLayoutManager(mContext));
                bookAdapterHolder.getRecyclerView().setAdapter(bookMainAdapter);
                // }
            }

        } else if (typeCount == Constants.ExploreType.explore) {

            ExploreViewHolder exploreViewHolder = (ExploreViewHolder) holder;
            exploreViewHolder.getCard_article().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startLandingActivity(Constants.Content.CONTENT_TYPE_ARTICLE);
                }
            });
            exploreViewHolder.getVideo().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startLandingActivity(Constants.Content.CONTENT_TYPE_VIDEO);
                }
            });
            exploreViewHolder.getCourse().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startLandingActivity(Constants.Content.CONTENT_TYPE_COURSE);
                }
            });
            exploreViewHolder.getPodcast().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startLandingActivity(Constants.Content.CONTENT_TYPE_PODCAST);
                }
            });
            exploreViewHolder.getInfographic().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startLandingActivity(Constants.Content.CONTENT_TYPE_INFOGRAPHIC);
                }
            });
            exploreViewHolder.getBooks().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startLandingActivity(Constants.Content.CONTENT_TYPE_EBOOK);
                }
            });

        } else if (typeCount == Constants.ExploreType.categories) {
            ArrayList mainData = data.get(type);
            if (mainData.get(0) instanceof CategoryModel) {

                Multi_Book_Holder multi_book_holder = (Multi_Book_Holder) holder;

                GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 4);
                multi_book_holder.getRecyclerView().setLayoutManager(gridLayoutManager);
                CategoryAdapter exploreMainAdapter = new CategoryAdapter(mContext, mainData);
                multi_book_holder.getRecyclerView().setAdapter(exploreMainAdapter);

            }
        } else if (typeCount == Constants.ExploreType.grid) {
            //   ArrayList mainData = data.get(type);
            listMain = data.get(type);
            if (listMain.get(0) instanceof GridModel) {

                Multi_Book_Holder multi_book_holder = (Multi_Book_Holder) holder;
                multi_book_holder.heading.setVisibility(View.GONE);

                linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);

                multi_book_holder.getRecyclerView().setLayoutManager(linearLayoutManager);

                RecyclerView recyclerViewMain = multi_book_holder.getRecyclerView();

                GridMainAdapter exploreMainAdapter = new GridMainAdapter(mContext, listMain);
                multi_book_holder.getRecyclerView().setAdapter(exploreMainAdapter);


            }
        }
    }

    private void startLandingActivity(int contentType) {
        Intent landingIntent = new Intent(mContext, ListingActivity.class);
        landingIntent.putExtra(Constants.Content.CONTENT_TYPE, String.valueOf(contentType));
        mContext.startActivity(landingIntent);
    }

    public void setupClick(String type, String param1, String param2, String param3) {

        if (type.equals("1")) {
            ((HomeActivity) mContext).setUpFragmentLayout(R.id.bottom_explore);
        } else if (type.equals("2")) {
            Intent intent = new Intent(mContext, CategoryListingActivity.class);
            intent.putExtra(CATEGORY_ID, param1);
            intent.putExtra(CATEGORY_NAME, param2);
            mContext.startActivity(intent);
        } else if (type.equals("3")) {
            Intent intent = new Intent(mContext, ListingActivity.class);
            intent.putExtra(Constants.Content.CONTENT_TYPE, param1);
            mContext.startActivity(intent);
        } else if (type.equals("4")) {
            ((HomeActivity) mContext).setUpFragmentLayout(R.id.bottom_mycompany);
        } else if (type.equals("5")) {
            Intent userProfileIntent = new Intent(mContext, UserDetailsActivity.class);
            userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 0);
            userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(mContext).getUserId());
            userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(mContext).getCompanyId());
            mContext.startActivity(userProfileIntent);
        } else if (type.equals("6")) {
            mContext.startActivity(new Intent(mContext, MessageActivity.class));
        } else if (type.equals("7")) {
            Intent userProfileIntent = new Intent(mContext, UserDetailsActivity.class);
            userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 4);
            userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(mContext).getUserId());
            userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(mContext).getCompanyId());
            mContext.startActivity(userProfileIntent);
        } else if (type.equals("8")) {
            ((HomeActivity) mContext).setUpFragmentLayout(R.id.bottom_create);
        } else if (type.equals("9")) {
            Intent searchActivityIntent = new Intent(mContext, SearchActivity.class);
            searchActivityIntent.putExtra(SearchActivity.SEARCH_QUERY, param2);
            mContext.startActivity(searchActivityIntent);
        } else if (type.equals("10")) {
            Intent userProfileIntent = new Intent(mContext, UserDetailsActivity.class);
            userProfileIntent.putExtra(UserDetailsFragment.PAGE_NO, 6);
            userProfileIntent.putExtra(Constants.Network.USER_ID, User.getCurrentUser(mContext).getUserId());
            userProfileIntent.putExtra(Constants.Network.COMPANY_ID, User.getCurrentUser(mContext).getCompanyId());
            mContext.startActivity(userProfileIntent);
        } else if (type.equals("11")) {
            ((HomeActivity) mContext).setUpFragmentLayout(R.id.bottom_mycompany);
        } else if (type.equals("12")) {
            //leadership board
            Intent userProfileIntent = new Intent(mContext, LeaderBoardActivity.class);
            mContext.startActivity(userProfileIntent);

        } else if (type.equals("13")) {
            Intent userProfileIntent = new Intent(mContext, ProductActivity.class);
            userProfileIntent.putExtra(Constants.Network.CONTENT_ID, param1);
            userProfileIntent.putExtra(Constants.Network.CONTENT_TYPE, param2);
            userProfileIntent.putExtra(Constants.Network.CONTENT_STATUS_TYPE, param3);
            mContext.startActivity(userProfileIntent);
        }

    }

    @Override
    public int getItemViewType(int position) {
        String key = itemType.get(position);
        int type = CommonMethods.returnDynamicType(key);
        return type;
    }

    @Override
    public int getItemCount() {
        return itemType.size();
    }
}
