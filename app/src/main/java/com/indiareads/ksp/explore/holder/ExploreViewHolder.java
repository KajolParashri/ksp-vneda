package com.indiareads.ksp.explore.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.indiareads.ksp.R;

public class ExploreViewHolder extends RecyclerView.ViewHolder {
    public LinearLayout card_article;
    public LinearLayout video;
    public LinearLayout course;
    public LinearLayout podcast;
    public LinearLayout infographic;
    public LinearLayout books;

    public ExploreViewHolder(View itemView, Context context) {
        super(itemView);
        card_article = itemView.findViewById(R.id.card_article);
        video = itemView.findViewById(R.id.card_video);
        course = itemView.findViewById(R.id.card_course);
        podcast = itemView.findViewById(R.id.card_podcast);
        infographic = itemView.findViewById(R.id.card_infographic);
        books = itemView.findViewById(R.id.card_ebook);
    }

    public LinearLayout getCard_article() {
        return card_article;
    }

    public LinearLayout getVideo() {
        return video;
    }

    public LinearLayout getCourse() {
        return course;
    }

    public LinearLayout getPodcast() {
        return podcast;
    }

    public LinearLayout getInfographic() {
        return infographic;
    }

    public LinearLayout getBooks() {
        return books;
    }
}
