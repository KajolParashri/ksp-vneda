package com.indiareads.ksp.explore.fragment;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indiareads.ksp.R;


public class DynamicExploreActivity extends AppCompatActivity {

    String SETVALUE = "";
    TextView main_Title;
    ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_explore);

        main_Title = findViewById(R.id.main_Title);
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent ii = getIntent();
        if (ii.hasExtra("SETVALUE")) {
            SETVALUE = ii.getStringExtra("SETVALUE");
        }
        openFrag();
    }

    public void setTitle(String title) {
        main_Title.setText(title);
    }

    public void openFrag() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.root_layout, NewExploreFragment.newInstance(SETVALUE, SETVALUE));
        fragmentTransaction.commit();
    }
}
