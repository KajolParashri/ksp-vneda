package com.indiareads.ksp.explore.fragment;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.explore.adapter.ExploreAdapter;
import com.indiareads.ksp.explore.adapter.GridMainAdapter;
import com.indiareads.ksp.explore.mode.DualPosterModel;
import com.indiareads.ksp.explore.mode.GridModel;
import com.indiareads.ksp.explore.mode.PosterModel;
import com.indiareads.ksp.model.BannerModel;
import com.indiareads.ksp.model.BookMainModel;
import com.indiareads.ksp.model.CategoryModel;
import com.indiareads.ksp.model.ExploreModel;
import com.indiareads.ksp.utils.CommonMethods;
import com.indiareads.ksp.utils.Constants;
import com.indiareads.ksp.utils.Logger;
import com.indiareads.ksp.utils.SideBarPanel;
import com.indiareads.ksp.utils.Urls;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewExploreFragment extends Fragment {

    RecyclerView recyclerView;
    ArrayList<String> typeList;
    String loadGrid = "false";
    LinkedHashMap<String, ArrayList> mainHash;
    ArrayList<GridModel> gridData;

    LinkedHashMap<String, String> keyHash;
    ProgressBar progressBar;
    public String setVl;

    NestedScrollView nestedScroll;

    public int page_number = 1;
    private boolean doPagination = true;

    private boolean mContentsLoading = true;
    GridMainAdapter mainAdapter;

    public static String ARG_PARAM1;
    public static String ARG_PARAM2;
    RecyclerView grid_recyclerView;

    LinearLayoutManager linearLayoutManager;

    public NewExploreFragment() {
        // Required empty public constructor
    }

    public static NewExploreFragment newInstance(String param1, String param2) {

        NewExploreFragment fragment = new NewExploreFragment();

        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            setVl = getArguments().getString(ARG_PARAM1);
            setVl = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mFragmentRootView = inflater.inflate(R.layout.new_explore_frag, container, false);
        recyclerView = mFragmentRootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.getLayoutManager().setMeasurementCacheEnabled(false);
        nestedScroll = mFragmentRootView.findViewById(R.id.nestedScroll);
        progressBar = mFragmentRootView.findViewById(R.id.progressBar);

        linearLayoutManager = new LinearLayoutManager(getActivity());

        grid_recyclerView = mFragmentRootView.findViewById(R.id.grid_recyclerView);
        grid_recyclerView.setLayoutManager(linearLayoutManager);
        grid_recyclerView.getLayoutManager().setMeasurementCacheEnabled(false);

        gridData = new ArrayList<>();
        ///  grid_recyclerView.setNestedScrollingEnabled(false);
        setScrollListener(nestedScroll);
        grid_recyclerView.setVisibility(View.GONE);

        getDynamicData(setVl, page_number);
        return mFragmentRootView;
    }

    private void getDynamicData(String SetValue, int pageNumber) {
        try {

            keyHash = new LinkedHashMap<>();
            mainHash = new LinkedHashMap<>();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, SetValue);
            jsonObject.put(Constants.Network.PAGE, pageNumber);
            Logger.error("Dynamic", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.DYNAMIC_CONTENT_API,
                    jsonObject, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Logger.error("Dynamic", response.toString());

                        if (progressBar.getVisibility() == View.VISIBLE) {
                            progressBar.setVisibility(View.GONE);
                        }
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                            JSONObject data = response.getJSONObject(Constants.Network.DATA);
                            JSONArray jsonArrayMain = data.getJSONArray("config");
                            Logger.error("Dynamic", jsonArrayMain.toString());
                            for (int i = 0; i < jsonArrayMain.length(); i++) {
                                JSONObject dataObject = jsonArrayMain.getJSONObject(i);
                                typeList = new ArrayList<>();

                                String actionbar_title = dataObject.getString("actionbar_title");
                                if (getActivity() instanceof DynamicExploreActivity) {
                                    ((DynamicExploreActivity) getActivity()).setTitle(actionbar_title);
                                }
                                keyHash.put("actionbar_title", actionbar_title);
                                if (actionbar_title.equals("1")) {
                                    typeList.add("actionbar_title");

                                }
                                String banner_slider = dataObject.getString("banner_slider");
                                keyHash.put("banner_slider", banner_slider);
                                if (banner_slider.equals("1")) {
                                    typeList.add("banner_slider");
                                }
                                String dual_image = dataObject.getString("dual_image");
                                keyHash.put("dual_image", dual_image);
                                if (dual_image.equals("1")) {
                                    typeList.add("dual_image");
                                }
                                String poster_image = dataObject.getString("poster_image");
                                keyHash.put("poster_image", poster_image);
                                if (poster_image.equals("1")) {
                                    typeList.add("poster_image");
                                }
                                String slider = dataObject.getString("slider");
                                keyHash.put("slider", slider);
                                if (slider.equals("1")) {
                                    typeList.add("slider");
                                }
                                String grid = dataObject.getString("grid");
                                keyHash.put("grid", grid);
                                if (grid.equals("1")) {
                                    typeList.add("grid");
                                }
                                String categories = dataObject.getString("categories");
                                keyHash.put("categories", categories);
                                if (categories.equals("1")) {
                                    typeList.add("categories");
                                }
                                String explore = dataObject.getString("explore");
                                keyHash.put("explore", explore);
                                if (explore.equals("1")) {
                                    typeList.add("explore");
                                }
                            }
                            Logger.error("ListData", typeList.toString());

                            try {

                                JSONObject details = data.getJSONObject("details");
                                Logger.error("Dynamic", details.toString());
                                for (int j = 0; j < typeList.size(); j++) {

                                    String key = typeList.get(j);
                                    String value = keyHash.get(key);
                                    if (value.equals("1")) {
                                        int valueType = CommonMethods.returnDynamicType(key);
                                        if (valueType == Constants.ExploreType.banner_slider && value.equals("1")) {
                                            JSONArray banner = details.getJSONArray(typeList.get(j));
                                            Logger.error("Dynamic", banner.toString());
                                            ArrayList<BannerModel> list = new ArrayList<>();
                                            for (int i = 0; i < banner.length(); i++) {
                                                JSONObject dataObject = banner.getJSONObject(i);
                                                BannerModel bannerModel = new BannerModel();
                                                bannerModel.setImgUrl(Urls.AWS_SERVER_URL + dataObject.getString("url"));
                                                bannerModel.setIntentStatus(dataObject.getString("intent_status"));
                                                bannerModel.setIntentType(dataObject.getString("intent_type"));
                                                bannerModel.setIntentParameter1(dataObject.getString("intent_parameter_1"));
                                                bannerModel.setIntentParameter2(dataObject.getString("intent_parameter_2"));
                                                bannerModel.setIntentParameter3(dataObject.getString("intent_parameter_3"));
                                                list.add(bannerModel);
                                            }
                                            mainHash.put(typeList.get(j), list);
                                        } else if (valueType == Constants.ExploreType.poster_image && value.equals("1")) {
                                            try {
                                                JSONObject dataObject = details.getJSONObject(typeList.get(j));
                                                Logger.error("Dynamic", dataObject.toString());
                                                ArrayList<PosterModel> list = new ArrayList<>();

                                                PosterModel posterModel = new PosterModel();
                                                String url = dataObject.getString("url");

                                                posterModel.setImage_url(Urls.AWS_SERVER_URL + url);
                                                posterModel.setIntent_status(dataObject.getString("intent_status"));
                                                posterModel.setIntent_type(dataObject.getString("intent_type"));
                                                posterModel.setIntent_parameter_1(dataObject.getString("intent_parameter_1"));
                                                posterModel.setIntent_parameter_2(dataObject.getString("intent_parameter_2"));
                                                posterModel.setIntent_parameter_3(dataObject.getString("intent_parameter_3"));
                                                list.add(posterModel);

                                                mainHash.put(typeList.get(j), list);

                                            } catch (Exception e) {
                                                Logger.error("Dynamic", "ErrorPoster");
                                            }

                                        } else if (valueType == Constants.ExploreType.dual_image && value.equals("1")) {
                                            try {
                                                JSONArray dataObject = details.getJSONArray(typeList.get(j));
                                                ArrayList<DualPosterModel> list = new ArrayList<>();
                                                Logger.error("Dynamic", dataObject.toString());
                                                for (int i = 0; i < dataObject.length(); i++) {
                                                    DualPosterModel posterModel = new DualPosterModel();
                                                    JSONObject dataObj = dataObject.getJSONObject(i);
                                                    String url = dataObj.getString("url");
                                                    //   Toast.makeText(getActivity(), "" + url, Toast.LENGTH_SHORT).show();

                                                    if (i == 0) {
                                                        posterModel.setFimage_url(Urls.AWS_SERVER_URL + url);
                                                    } else if (i == 1) {
                                                        posterModel.setSimage_url(Urls.AWS_SERVER_URL + url);
                                                    }
                                                    posterModel.setIntent_status(dataObj.getString("intent_status"));
                                                    posterModel.setIntent_type(dataObj.getString("intent_type"));
                                                    posterModel.setIntent_parameter_1(dataObj.getString("intent_parameter_1"));
                                                    posterModel.setIntent_parameter_2(dataObj.getString("intent_parameter_2"));
                                                    posterModel.setIntent_parameter_3(dataObj.getString("intent_parameter_3"));
                                                    list.add(posterModel);
                                                }
                                                mainHash.put(typeList.get(j), list);

                                            } catch (Exception e) {
                                                Logger.error("Dynamic", "DualImageError");
                                            }
                                        } else if (valueType == Constants.ExploreType.slider && value.equals("1")) {
                                            try {

                                                JSONArray slider = details.getJSONArray(typeList.get(j));
                                                int count = 0;
                                                ArrayList<BookMainModel> list = new ArrayList<>();
                                                Logger.error("Dynamic", slider.toString());
                                                for (int i = 0; i < slider.length(); i++) {

                                                    JSONObject dataObj = slider.getJSONObject(i);

                                                    JSONObject detail = dataObj.getJSONObject("detail");
                                                    JSONArray dataSlider = dataObj.getJSONArray("data");

                                                    String title = detail.getString("title");
                                                    String set = "";
                                                    if (detail.has("set")) {
                                                        set = detail.getString("set");
                                                    }
                                                    ArrayList<ExploreModel> arrayList = new ArrayList<>();
                                                    for (int kk = 0; kk < dataSlider.length(); kk++) {
                                                        JSONObject mainDataObj = dataSlider.getJSONObject(kk);
                                                        ExploreModel exploreModel = new ExploreModel();
                                                        exploreModel.setTitle(mainDataObj.getString("title"));

                                                        exploreModel.setContent_type(mainDataObj.getString("content_type"));
                                                        String content_type = mainDataObj.getString("content_type");

                                                        if (content_type.equals("9")) {
                                                            exploreModel.setThumnail(Urls.BOOK_COVER_IMAGE_BASE + mainDataObj.getString("thumnail"));
                                                        } else {
                                                            exploreModel.setThumnail(mainDataObj.getString("thumnail"));
                                                        }

                                                        exploreModel.setIntent_type(mainDataObj.getString("intent_type"));
                                                        exploreModel.setIntent_parameter_1(mainDataObj.getString("intent_parameter_1"));
                                                        exploreModel.setIntent_parameter_2(mainDataObj.getString("intent_parameter_2"));
                                                        exploreModel.setIntent_parameter_3(mainDataObj.getString("intent_parameter_3"));
                                                        arrayList.add(exploreModel);
                                                    }

                                                    BookMainModel bookMainModel = new BookMainModel();
                                                    bookMainModel.setSetValue(set);
                                                    bookMainModel.setHeadingText(title);
                                                    bookMainModel.setArrayList(arrayList);

                                                    list.add(bookMainModel);

                                                }

                                                if (count == 0) {
                                                    mainHash.put(typeList.get(j), list);
                                                    count++;
                                                } else {
                                                    String name = typeList.get(j);
                                                    typeList.add(j, name);
                                                    mainHash.put(name + count, list);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } else if (valueType == Constants.ExploreType.explore && value.equals("1")) {
                                            ArrayList<String> blankString = new ArrayList<>();
                                            mainHash.put(typeList.get(j), blankString);

                                            Logger.error("Dynamic", "Explore");

                                        } else if (valueType == Constants.ExploreType.categories && value.equals("1")) {


                                            ArrayList<CategoryModel> category = new ArrayList<>();
                                            JSONArray categories = details.getJSONArray(typeList.get(j));
                                            Logger.error("Dynamic", categories.toString());
                                            for (int i = 0; i < categories.length(); i++) {
                                                int jjj = i + 1;
                                                JSONObject dataObj = categories.getJSONObject(i);

                                                String url = Urls.baseImageUrl + "/icons/category/" + jjj + ".svg";
                                                CategoryModel exploreModel = new CategoryModel();
                                                exploreModel.setImage_Url(url);
                                                exploreModel.setItem_Name(dataObj.getString("cat_name"));
                                                exploreModel.setItem_Id(dataObj.getString("cat_id"));
                                                exploreModel.setItem_type("Category");

                                                category.add(exploreModel);
                                            }
                                            mainHash.put(typeList.get(j), category);
                                        } else if (valueType == Constants.ExploreType.grid && value.equals("1")) {
                                            //    Toast.makeText(getActivity(), "gridcallled", Toast.LENGTH_SHORT).show();
                                            grid_recyclerView.setVisibility(View.VISIBLE);
                                            loadGrid = "true";
                                            gridData = new ArrayList<>();
                                            JSONArray grid = details.getJSONArray(typeList.get(j));

                                            typeList.remove(j);

                                            for (int i = 0; i < grid.length(); i++) {
                                                JSONObject dataObj = grid.getJSONObject(i);
                                                JSONArray dataObjJSONArray = dataObj.getJSONArray("data");
                                                Logger.error("Dynamic", dataObjJSONArray.toString());
                                                for (int jj = 0; jj < dataObjJSONArray.length(); jj++) {
                                                    JSONObject dataObj1 = dataObjJSONArray.getJSONObject(jj);

                                                    GridModel gridModel = new GridModel();
                                                    if (dataObj1.has("author")) {
                                                        gridModel.setAuthor(dataObj1.getString("author"));
                                                    }
                                                    if (dataObj1.has("category"))
                                                        gridModel.setCategory(dataObj1.getString("category"));
                                                    if (dataObj1.has("content_id_s"))
                                                        gridModel.setContent_id_s(dataObj1.getString("content_id_s"));
                                                    if (dataObj1.has("title"))
                                                        gridModel.setTitle(dataObj1.getString("title"));
                                                    if (dataObj1.has("thumbnail"))
                                                        gridModel.setThumbnail(dataObj1.getString("thumbnail"));
                                                    if (dataObj1.has("content_type"))
                                                        gridModel.setContent_type(dataObj1.getString("content_type"));
                                                    if (dataObj1.has("source_name"))
                                                        gridModel.setSource_name(dataObj1.getString("source_name"));
                                                    if (dataObj1.has("rating"))
                                                        gridModel.setRating(dataObj1.getInt("rating"));
                                                    if (dataObj1.has("content_status_type"))
                                                        gridModel.setContent_status_type(dataObj1.getString("content_status_type"));
                                                    gridData.add(gridModel);

                                                }
                                            }
                                            mainAdapter = new GridMainAdapter(getActivity(), gridData);
                                            grid_recyclerView.setAdapter(mainAdapter);
                                        }
                                    }
                                }

                                ExploreAdapter exploreAdapter = new ExploreAdapter(getActivity(), typeList, mainHash, setVl);
                                recyclerView.setAdapter(exploreAdapter);

                            } catch (Exception e) {
                                Logger.error("Dynamic", "ExceptionTry");

                                if (progressBar.getVisibility() == View.VISIBLE) {
                                    progressBar.setVisibility(View.GONE);
                                }
                                e.printStackTrace();
                            }


                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout(getActivity());
                        } else {
                            Logger.error("Dynamic", "Error");
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Logger.error("Dynamic", "ExceptionParent");

                        if (progressBar.getVisibility() == View.VISIBLE) {
                            progressBar.setVisibility(View.GONE);
                        }
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.error("Dynamic", "Exception");
                    if (progressBar.getVisibility() == View.VISIBLE) {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Logger.error("Dynamic", "Exception");
            if (progressBar.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    private void getDynamicData(String SetValue, int pageNumber, final ArrayList mainData) {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, SetValue);
            jsonObject.put(Constants.Network.PAGE, pageNumber);
            Logger.error("response", jsonObject.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(getActivity(), Request.Method.POST, Urls.DYNAMIC_CONTENT_API,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        gridData.remove(gridData.size() - 1);
                        mainAdapter.notifyItemRemoved(gridData.size() - 1);

                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                            Logger.error("response", response.toString());
                            doPagination = true;

                            JSONObject data = response.getJSONObject(Constants.Network.DATA);
                            JSONArray jsonArrayMain = data.getJSONArray("config");
                            for (int i = 0; i < jsonArrayMain.length(); i++) {
                                JSONObject dataObject = jsonArrayMain.getJSONObject(i);
                                String grid = dataObject.getString("grid");

                                if (grid.equals("1")) {

                                    JSONObject details = data.getJSONObject("details");
                                    Logger.error("response", details.toString());
                                    int valueType = 5;
                                    if (valueType == Constants.ExploreType.grid) {
                                        JSONArray gridArray = details.getJSONArray("grid");
                                        int sizeCurrent = gridData.size();
                                        for (int ii = 0; ii < gridArray.length(); ii++) {
                                            JSONObject dataObj = gridArray.getJSONObject(ii);
                                            JSONArray dataObjJSONArray = dataObj.getJSONArray("data");
                                            for (int jj = 0; jj < dataObjJSONArray.length(); jj++) {
                                                JSONObject dataObj1 = dataObjJSONArray.getJSONObject(jj);
                                                GridModel gridModel = new GridModel();

                                                if (dataObj1.has("author")) {
                                                    gridModel.setAuthor(dataObj1.getString("author"));
                                                }
                                                if (dataObj1.has("category"))
                                                    gridModel.setCategory(dataObj1.getString("category"));
                                                if (dataObj1.has("content_id_s"))
                                                    gridModel.setContent_id_s(dataObj1.getString("content_id_s"));
                                                if (dataObj1.has("title"))
                                                    gridModel.setTitle(dataObj1.getString("title"));
                                                if (dataObj1.has("thumbnail"))
                                                    gridModel.setThumbnail(dataObj1.getString("thumbnail"));
                                                if (dataObj1.has("content_type"))
                                                    gridModel.setContent_type(dataObj1.getString("content_type"));
                                                if (dataObj1.has("source_name"))
                                                    gridModel.setSource_name(dataObj1.getString("source_name"));
                                                if (dataObj1.has("rating"))
                                                    gridModel.setRating(dataObj1.getInt("rating"));
                                                if (dataObj1.has("content_status_type"))
                                                    gridModel.setContent_status_type(dataObj1.getString("content_status_type"));

                                                gridData.add(gridModel);
                                            }
                                            mainAdapter.notifyItemRangeInserted(sizeCurrent, gridData.size() - 1);
                                        }
                                    }
                                }
                            }
                        } else {
                            //stop call to pagination in any case
                            doPagination = false;
                        }

                        mContentsLoading = true;

                    } catch (Exception e) {
                        Logger.error("response", e.getMessage());
                        Toast.makeText(getActivity(), "ExcetptionE", Toast.LENGTH_SHORT).show();
                        gridData.remove(gridData.size() - 1);
                        mainAdapter.notifyItemRemoved(gridData.size() - 1);
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Toast.makeText(getActivity(), "ExcetptionError", Toast.LENGTH_SHORT).show();
                    gridData.remove(gridData.size() - 1);
                    mainAdapter.notifyItemRemoved(gridData.size() - 1);
                }
            });

            VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "ExcetptionLastCatch", Toast.LENGTH_SHORT).show();
            gridData.remove(gridData.size() - 1);
            mainAdapter.notifyItemRemoved(gridData.size() - 1);
            e.printStackTrace();
        }
    }

    public void setScrollListener(NestedScrollView nestedScrollView) {

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                if (v.getChildAt(v.getChildCount() - 1) != null) {

                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //code to fetch more data for endless scrolling
                        if (doPagination && loadGrid.equals("true")) //check for scroll down

                        {
                            if (mContentsLoading) {

                                addProgressBarInList();
                                mContentsLoading = false;
                                page_number++;
                                getDynamicData(setVl, page_number, gridData);

                            }
                        }

                    }
                }
            }
        });

    }

    private void addProgressBarInList() {
        GridModel progressBarContent = new GridModel();
        progressBarContent.setContent_type(String.valueOf(Constants.Content.CONTENT_TYPE_PROGRESS_LOADER));
        gridData.add(progressBarContent);
        mainAdapter.notifyItemInserted(gridData.size() - 1);
    }
}
