package com.indiareads.ksp.explore.mode;

import com.indiareads.ksp.model.BaseParentModel;

public class PosterModel extends BaseParentModel {

    public String image_url;
    public String clickable = "true";
    public String intent_status;
    public String intent_type;
    public String intent_parameter_1;
    public String intent_parameter_2;
    public String intent_parameter_3;

    public String getIntent_parameter_3() {
        return intent_parameter_3;
    }

    public void setIntent_parameter_3(String intent_parameter_3) {
        this.intent_parameter_3 = intent_parameter_3;
    }

    public String getIntent_status() {
        return intent_status;
    }

    public void setIntent_status(String intent_status) {
        this.intent_status = intent_status;
    }

    public String getIntent_type() {
        return intent_type;
    }

    public void setIntent_type(String intent_type) {
        this.intent_type = intent_type;
    }

    public String getIntent_parameter_1() {
        return intent_parameter_1;
    }

    public void setIntent_parameter_1(String intent_parameter_1) {
        this.intent_parameter_1 = intent_parameter_1;
    }

    public String getIntent_parameter_2() {
        return intent_parameter_2;
    }

    public void setIntent_parameter_2(String intent_parameter_2) {
        this.intent_parameter_2 = intent_parameter_2;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getClickable() {
        return clickable;
    }

    public void setClickable(String clickable) {
        this.clickable = clickable;
    }
}

