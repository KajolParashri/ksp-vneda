package com.indiareads.ksp.explore.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.indiareads.ksp.R;
import com.indiareads.ksp.listeners.OnAddressClickListener;

public class PosterImageModelHolder extends RecyclerView.ViewHolder {
    public ImageView image_poster;

    public PosterImageModelHolder(View itemView) {
        super(itemView);
        image_poster = itemView.findViewById(R.id.image_poster);
    }

    public ImageView getImage_poster() {
        return image_poster;
    }
}
