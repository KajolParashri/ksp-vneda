package com.indiareads.ksp.utils;

public class Urls {
    public static final String baseAWSUrl = "https://assets.vneda.com";
    public static final String AWS_PROFILE_IMAGE_PATH = baseAWSUrl + "/ksp/user_profile/";
    public static final String AWS_CURATED_PROFILE_IMAGE_PATH = baseAWSUrl + "/ksp/curated_source/";
    public static final String AWS_CURATED_CONTENT_PATH = baseAWSUrl + "/ksp/curated/";
    public static final String AWS_MESSAGE_IMAGE_PATH = baseAWSUrl + "/ksp/repo/media/";
    public static final String AWS_REPOSITORY_PATH = baseAWSUrl + "/ksp/repo/";
    public static final String BOOK_COVER_IMAGE_BASE = baseAWSUrl + "/book_i/";
    private static final String baseUrl = "https://www.vneda.com/ksp/resources/services/";
    public static final String REWARDS_API = baseUrl + "rewards_api.php";
    public static final String FEEDBACK_API = baseUrl + "vneda-form.php";
    public static final String postApi = baseUrl + "create_api.php";
    public static final String getKeywordsApi = baseUrl + "tag_api.php?";
    public static final String bookShelfUrl = baseUrl + "user.php";
    public static final String AUTHENTICATION = baseUrl + "authentication.php";
    public static final String HOMEPAGE_API = baseUrl + "homepage_api.php";
    public static final String CHAT_API = baseUrl + "chat_api.php";
    public static final String CONTENT = baseUrl + "content.php";
    public static final String CONTENT_CURATED_API = baseUrl + "content_curated_api.php";
    public static final String DYNAMIC_CONTENT_API = baseUrl + "dynamic_content_api.php";
    public static final String CREATE_API = baseUrl + "create_api.php";
    public static final String USER = baseUrl + "user.php";
    public static final String MYCOMPANY_API = baseUrl + "mycompany_api.php";
    public static final String LEADERBOARD_API = baseUrl + "leaderboard_api.php";
    public static final String AWS_SERVER_URL = "https://assets.vneda.com/";
    public static final String CONTENT_LISTING_API = baseUrl + "content_listing_api.php";
    public static final String SEARCH_API = baseUrl + "search_api.php";
    public static final String GET_EXISTING_ADDRESS = baseUrl + "user.php";
    public static final String ORDER_PLACE = baseUrl + "order_place_api.php";
    public static final String MEDIUM_PRODUCT_API = baseUrl + "medium_product_api.php";
    public static final String YOUTUBE_PRODUCT_API = baseUrl + "youtube_product_api.php";
    public static final String PODCAST_LISTENNOTES_PRODUCT_API = baseUrl + "podcast_listen_notes_product_api.php";
    public static final String EXTRA_API = baseUrl + "extra-api.php";
    public static final String BaseImgURL = "https://assets.vneda.com/ksp/company_logo/";
    public static final String baseImageUrl = "https://assets.vneda.com/ksp";
    public static final String baseNotificationUserUrl = "https://assets.vneda.com/ksp/user_profile/";
}