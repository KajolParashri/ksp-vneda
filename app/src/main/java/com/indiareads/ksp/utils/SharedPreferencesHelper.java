package com.indiareads.ksp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.indiareads.ksp.model.CompanyFeature;
import com.indiareads.ksp.model.User;

public class SharedPreferencesHelper {

    private static final String USER_DETAILS = "USER_DETAILS";
    private static final String COMPANY_DETAILS = "COMPANY_DETAILS";
    private static final String FCM_DETAILS = "FCM_DETAILS";
    private static final String TOTURIAL_STATUS = "TOTURIAL_STATUS";
    private static final String IS_TUTORIAL_FINISHED = "is_tutorial_finished";
    private static final String FCM_TOKEN = "fcm_token";

    private static SharedPreferences.Editor openSharedPreferencesInWriteMode(Context context, String name) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(name, Context.MODE_PRIVATE);
        return sharedPreferences.edit();
    }

    private static SharedPreferences openSharedPreferencesInReadMode(Context context, String name) {
        return context.getApplicationContext().getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public static void saveUser(Context context, User user) {
        SharedPreferences.Editor editor = openSharedPreferencesInWriteMode(context, SharedPreferencesHelper.USER_DETAILS);
        editor.putString(User.USER_ID, CommonMethods.checkNotNull(user.getUserId()));
        editor.putString(User.USER_EMAIL, CommonMethods.checkNotNull(user.getUserEmail()));
        editor.putString(User.LOAD_TUTORIAL, CommonMethods.checkNotNull(user.getLoadTutorials()));
        editor.putString(User.COMPANY_ID, CommonMethods.checkNotNull(user.getCompanyId()));
        editor.putString(User.COMPANY_NAME, CommonMethods.checkNotNull(user.getCompanyName()));
        editor.putString(User.PROFILE_PIC, CommonMethods.checkNotNull(user.getProfilePic()));
        editor.putString(User.DESIGNATION, CommonMethods.checkNotNull(user.getDesignation()));
        editor.putString(User.FIRST_NAME, CommonMethods.checkNotNull(user.getFirstName()));
        editor.putString(User.LAST_NAME, CommonMethods.checkNotNull(user.getLastName()));
        editor.putString(User.IS_ADMIN, CommonMethods.checkNotNull(user.getIsAdmin()));
        editor.putString(User.COMPANY_FEATURE_ID, CommonMethods.checkNotNull(user.getCompanyFeaturesId()));
        editor.putString(User.COMPANY_FEATURE_COMPANY_ID, CommonMethods.checkNotNull(user.getCompanyFeaturesCompanyId()));
        editor.putString(User.COMPANY_FEATURE_FEATURE_ID, CommonMethods.checkNotNull(user.getCompanyFeaturesFeatureId()));
        editor.putString(User.COMPANY_FEATURE_VALUE, CommonMethods.checkNotNull(user.getCompanyFeaturesValue()));
        editor.putString(User.COMPANY_FEATURE_STATUS, CommonMethods.checkNotNull(user.getCompanyFeaturesStatus()));
        editor.putString(User.FULLNAME, CommonMethods.checkNotNull(user.getFullName()));
        editor.putString(User.CONTENT_CREATED_COUNT, CommonMethods.checkNotNull(user.getCreatedContents()));
        editor.putString(User.CONTENT_CONSUMED_COUNT, CommonMethods.checkNotNull(user.getConsumedContents()));
        editor.putString(User.TOTAL_POINTS, CommonMethods.checkNotNull(user.getPoints()));
        editor.putString(User.BEARER_TOKEN, CommonMethods.checkNotNull(user.getBearerToken()));
        editor.apply();
    }

    public static User getCurrentUser(Context context) {

        User user = null;
        SharedPreferences sharedPreferences = openSharedPreferencesInReadMode(context, USER_DETAILS);
        if (sharedPreferences.getString(User.USER_ID, null) != null) {
            user = new User();
            user.setUserId(sharedPreferences.getString(User.USER_ID, ""));
            user.setUserEmail(sharedPreferences.getString(User.USER_EMAIL, ""));
            user.setCompanyId(sharedPreferences.getString(User.COMPANY_ID, ""));
            user.setProfilePic(sharedPreferences.getString(User.PROFILE_PIC, ""));
            user.setLoadTutorials(sharedPreferences.getString(User.LOAD_TUTORIAL, ""));
            user.setDesignation(sharedPreferences.getString(User.DESIGNATION, ""));
            user.setFirstName(sharedPreferences.getString(User.FIRST_NAME, ""));
            user.setLastName(sharedPreferences.getString(User.LAST_NAME, ""));
            user.setIsAdmin(sharedPreferences.getString(User.IS_ADMIN, ""));
            user.setCompanyFeaturesId(sharedPreferences.getString(User.COMPANY_FEATURE_ID, ""));
            user.setCompanyFeaturesCompanyId(sharedPreferences.getString(User.COMPANY_FEATURE_COMPANY_ID, ""));
            user.setCompanyFeaturesFeatureId(sharedPreferences.getString(User.COMPANY_FEATURE_FEATURE_ID, ""));
            user.setCompanyFeaturesValue(sharedPreferences.getString(User.COMPANY_FEATURE_VALUE, ""));
            user.setCompanyFeaturesStatus(sharedPreferences.getString(User.COMPANY_FEATURE_STATUS, ""));
            user.setFullName(sharedPreferences.getString(User.FULLNAME, ""));
            user.setBearerToken(sharedPreferences.getString(User.BEARER_TOKEN, ""));
        }
        return user;
    }

    public static void removeUser(Context context) {
        SharedPreferences sharedPreferences = openSharedPreferencesInReadMode(context, USER_DETAILS);
        sharedPreferences.edit().clear().apply();
    }

    public static void saveFeature(Context context, CompanyFeature companyFeature) {
        SharedPreferences.Editor editor = openSharedPreferencesInWriteMode(context, COMPANY_DETAILS);
        editor.putString(companyFeature.getFeatureId(), companyFeature.getValue());
        editor.apply();
    }

    public static void saveBookMark(Context context, String PageName, String pageNumber) {
        SharedPreferences.Editor editor = openSharedPreferencesInWriteMode(context, COMPANY_DETAILS);
        editor.putString(PageName, pageNumber);
        editor.apply();
    }

    public static void resetCompanyFeatures(Context context) {
        SharedPreferences.Editor editor = openSharedPreferencesInWriteMode(context, COMPANY_DETAILS);
        editor.clear().apply();
    }

    public static void saveToken(Context context, String token) {
        SharedPreferences.Editor editor = openSharedPreferencesInWriteMode(context, FCM_DETAILS);
        editor.putString(FCM_TOKEN, token);
        editor.commit();
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPreferences = openSharedPreferencesInReadMode(context, FCM_DETAILS);
        return sharedPreferences.getString(FCM_TOKEN, "");
    }

    public static void saveTutorialStatus(Context context, boolean isFinished) {
        SharedPreferences.Editor editor = openSharedPreferencesInWriteMode(context, TOTURIAL_STATUS);
        editor.putBoolean(IS_TUTORIAL_FINISHED, isFinished);
        editor.commit();
    }

    public static Boolean getTutorialStatus(Context context) {
        SharedPreferences sharedPreferences = openSharedPreferencesInReadMode(context, TOTURIAL_STATUS);
        return sharedPreferences.getBoolean(IS_TUTORIAL_FINISHED, false);
    }

    public static String getBearerToken(Context context) {
        SharedPreferences sharedPreferences = openSharedPreferencesInReadMode(context, USER_DETAILS);
        return sharedPreferences.getString(User.BEARER_TOKEN, "");
    }
}
