package com.indiareads.ksp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.MenuModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.view.activity.HomeActivity;
import com.indiareads.ksp.view.activity.LoginActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SideBarPanel {

    public static String ACCOUNT = "Account";
    public static String REDEEM = "Rewards and Recognition";
    public static String COMPANY = "Company";
    public static String CHAT = "Chat";
    public static String OTHERS = "Others";
    public static String FEEDBACK = "Feedback";


    public static ArrayList<MenuModel> redeemPoints() {
        ArrayList<MenuModel> menu = new ArrayList<>();

        menu.add(new MenuModel("My Coupons", R.drawable.voucher, "Account", 0));
        menu.add(new MenuModel("LeaderBoard", R.drawable.leaderboard_icon, "Account", 0));
        menu.add(new MenuModel("Redeem Points", R.drawable.redeem_point_icon, "Account", 1));

        return menu;
    }


    public static ArrayList<MenuModel> feedbackMenuName() {
        ArrayList<MenuModel> menu = new ArrayList<>();

        menu.add(new MenuModel("Feedback", R.drawable.feedback_icon, "Account", 1));

        return menu;
    }

    public static ArrayList<MenuModel> accountMenuName() {
        ArrayList<MenuModel> menu = new ArrayList<>();

        menu.add(new MenuModel("My Account", R.drawable.account_icon, "Account", 0));
        menu.add(new MenuModel("My Posts", R.drawable.post_icon, "Account", 0));
        menu.add(new MenuModel("My Contents", R.drawable.article_icon, "Account", 1));
        menu.add(new MenuModel("My Book Shelf", R.drawable.bookshelf_icon, "Account", 3));
        menu.add(new MenuModel("My Reading Shelf", R.drawable.readingshelf_icon, "Account", 2));

        menu.add(new MenuModel("My Drafts", R.drawable.drafts_icon, "Account", 5));
        menu.add(new MenuModel("My Orders", R.drawable.orders_icon, "Account", 4));
        menu.add(new MenuModel("My Repository", R.drawable.central_repo_icon, "Account", 6));
        menu.add(new MenuModel("My Address Book", R.drawable.addressbook_icon, "Account", 0));
        menu.add(new MenuModel("Edit Profile", R.drawable.edit_profile, "Account", 7));

        return menu;
    }

    public static ArrayList<MenuModel> companyMenuName() {

        ArrayList<MenuModel> menu = new ArrayList<>();
        menu.add(new MenuModel("Announcements", R.drawable.announcemnt_icon, "Company", 0));
        menu.add(new MenuModel("Posts", R.drawable.post_icon, "Company", 1));
        menu.add(new MenuModel("Contents", R.drawable.article_icon, "Company", 2));
        menu.add(new MenuModel("Central Repository", R.drawable.central_repo_icon, "Company", 3));
        menu.add(new MenuModel("LeaderBoard", R.drawable.leaderboard_icon, "Company", 4));
        menu.add(new MenuModel("Suggestion Board", R.drawable.suggestion_icon, "Company", 5));
        return menu;
    }

    public static ArrayList<MenuModel> chatMenuName() {
        ArrayList<MenuModel> menu = new ArrayList<>();

        menu.add(new MenuModel("Inbox", R.drawable.inbox_icon, "Chat", 1));
        menu.add(new MenuModel("Group Chat", R.drawable.group_chat_icon, "Chat", 2));

        return menu;
    }

    public static ArrayList<MenuModel> othersMenuName() {
        ArrayList<MenuModel> menu = new ArrayList<>();

        menu.add(new MenuModel("Request Book", R.drawable.home_bottom_book, "Others", 0));
        menu.add(new MenuModel("About Us", R.drawable.about_team, "Others", 1));
        menu.add(new MenuModel("Contact Us", R.drawable.contactus_icon, "Others", 2));
        menu.add(new MenuModel("Privacy Policies", R.drawable.terms_conditions, "Others", 3));
        menu.add(new MenuModel("Terms and Conditions", R.drawable.terms_conditions, "Others", 4));
        menu.add(new MenuModel("Logout", R.drawable.log_out, "Others", 5));

        return menu;
    }


    public static void logout(Activity context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
        sendLogoutRequest(context);
        User.removeCurrentUser(context);
        context.finish();
        Toast.makeText(context, R.string.logged_out_successfully, Toast.LENGTH_SHORT).show();
    }

    public static void logoutApp(Activity context, Context contextMain) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
        sendLogoutRequest(contextMain);
        User.removeCurrentUser(context);
        context.finish();
        Toast.makeText(context, R.string.logged_out_successfully, Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("HardwareIds")
    public static void sendLogoutRequest(final Context context) {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_LOGOUT_APP);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(context).getUserId());

            try {
                jsonObject.put(Constants.Network.DEVICE_ID, Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
                jsonObject.put(Constants.Network.APP_VERSION, String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode));
            } catch (Exception e) {
                Logger.error("LOGOUT_API", e.getMessage());
            }

            Logger.info("LOGOUT_API", jsonObject.toString());
            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(context, Request.Method.POST, Urls.EXTRA_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Logger.info("LOGOUT_API", response.toString());

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    Logger.info("LOGOUT_API", "Logged out successfully");
                                } else {
                                    Logger.error("LOGOUT_API", "400");
                                }
                            } catch (JSONException e) {
                                Logger.error("LOGOUT_API", e.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  Logger.error(TAG, error.get());
                        }
                    });

            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            Logger.error("LOGOUT_API", e.getMessage());
        }
    }
}
