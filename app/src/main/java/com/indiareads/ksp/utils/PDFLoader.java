package com.indiareads.ksp.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.indiareads.ksp.listeners.onEBookLoad;


import java.io.File;

public class PDFLoader {
    static onEBookLoad onResultListener;
    static EBookDownloader renderingPDFNetwork;
    private Context context;

    public PDFLoader(Context context) {
        this.context = context;
    }

    public static Throwable failedData(Throwable t) {
        onResultListener.failed(t);
        return t;
    }

    public static int progressData(int progress) {
        onResultListener.progressData(progress);
        return progress;
    }

    public static void onDownloadComplete(File file) {
        onResultListener.onDownloadComplete(file);
    }

    public void initFromNetwork(String endpoint, onEBookLoad resultListener) {
        onResultListener = resultListener;
        renderingPDFNetwork = new EBookDownloader(context, endpoint);
    }
}
