package com.indiareads.ksp.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indiareads.ksp.R;
import com.indiareads.ksp.adapters.CommentsAdapter;
import com.indiareads.ksp.listeners.OnContentLikeCommentListener;
import com.indiareads.ksp.model.Comment;
import com.indiareads.ksp.model.Content;
import com.indiareads.ksp.model.Error;
import com.indiareads.ksp.model.SuggestionModel;
import com.indiareads.ksp.model.User;
import com.indiareads.ksp.view.activity.HomeActivity;
import com.indiareads.ksp.volley.JsonObjectAuthRequest;
import com.indiareads.ksp.volley.VolleySingleton;
import com.indiareads.ksp.view.fragment.ContentFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LikeCommentHelper {

    public static final String TAG = "LikeCommentHelper";

    private static int mCommentPageNumber;
    private static boolean mCommentsLoading;
    private static boolean doCommentsPagination;
    private static List<Comment> mComments;
    private static LinearLayoutManager mCommentsLinearLayoutManager;

    public static void likeContentItem(final Context context, final Content content, final OnContentLikeCommentListener onContentLikeCommentListener) {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_LIKE_CONTENT);
            jsonObject.put(Constants.Network.CONTENT_ID, content.getContentId());
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(context).getUserId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, content.getContentType());

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(context, Request.Method.POST, Urls.CONTENT,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Logger.info(TAG, response.toString());

                    try {
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            JSONObject nestedJsonObject = response.getJSONObject(Constants.Network.DATA);
                            String likeStatus = nestedJsonObject.getString(Constants.Network.STATUS);
                            String likeCount = nestedJsonObject.getString(Constants.Network.LIKE_COUNT);

                            content.setLikeStatus(likeStatus);
                            content.setLikeCount(likeCount);
                            onContentLikeCommentListener.onContentLiked(content, likeStatus, likeCount);

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout((Activity) context);
                        } else {
                            CommonMethods.displayToast(context, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                            revertLikeChanges(content);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonMethods.displayToast(context, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                    //Request failed, revert back button changes
                    revertLikeChanges(content);
                    onContentLikeCommentListener.onContentLiked(content, content.getLikeStatus(), content.getLikeCount());
                }
            });
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void upvoteSuggestoin(String suggestion_id, final Context context) {

        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.SUGGESTION_ID, suggestion_id);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(context).getUserId());
            jsonObject.put(Constants.Network.ACTION, Constants.Network.LIKE_SUGGESTION);

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(context, Request.Method.POST, Urls.MYCOMPANY_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout((Activity) context);
                                }

                            } catch (Exception e) {

                                Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            volleyError.printStackTrace();

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                        }
                    });

            jsonObjectRequest.setTag("TAG");
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {


            Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    private static void revertLikeChanges(Content content) {
        switchLikeInfo(content);
    }

    public static void openComments(final int position, final Context context, View parentView, final Content content, final OnContentLikeCommentListener onContentLikeCommentListener) {

        View inflatedView = LayoutInflater.from(context).inflate(R.layout.like_comment_popup_layout, null, false);

        RecyclerView commentsRecyclerView = inflatedView.findViewById(R.id.comments_recycler_view);
        final EditText commentsEditText = inflatedView.findViewById(R.id.comment_edit_text);
        Button mPostCommentButton = inflatedView.findViewById(R.id.post_comment_button);

        final PopupWindow popWindow = new PopupWindow(inflatedView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
        popWindow.setAnimationStyle(R.style.PopupAnimation);
        popWindow.showAtLocation(parentView, Gravity.BOTTOM, 0, 100);

        mCommentPageNumber = 1;
        mCommentsLoading = true;
        doCommentsPagination = true;
        mComments = new ArrayList<>();
        final CommentsAdapter commentsAdapter = new CommentsAdapter(mComments);
        initCommentRecyclerView(context, commentsRecyclerView, commentsAdapter);
        mCommentsLinearLayoutManager.setReverseLayout(true);
//        setCommentsRecyclerViewScrollListener(context, commentsRecyclerView, commentsAdapter, content);
        fetchComments(context, commentsAdapter, content);

        popWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        });

        mPostCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!commentsEditText.getText().toString().trim().isEmpty()) {
                    commentItem(position, context, content, commentsEditText.getText().toString(), onContentLikeCommentListener, commentsAdapter);
                    commentsEditText.setText("");
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(commentsEditText.getWindowToken(), 0);
                }else {
                    Toast.makeText(context, "Comment Can't be Empty! ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public static void openSuggestionComments(final int position, final Context context, View parentView, final SuggestionModel content, final OnContentLikeCommentListener onContentLikeCommentListener) {

        View inflatedView = LayoutInflater.from(context).inflate(R.layout.like_comment_popup_layout, null, false);

        RecyclerView commentsRecyclerView = inflatedView.findViewById(R.id.comments_recycler_view);
        final EditText commentsEditText = inflatedView.findViewById(R.id.comment_edit_text);
        Button mPostCommentButton = inflatedView.findViewById(R.id.post_comment_button);

        final PopupWindow popWindow = new PopupWindow(inflatedView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
        popWindow.setAnimationStyle(R.style.PopupAnimation);
        popWindow.showAtLocation(parentView, Gravity.BOTTOM, 0, 100);

        mCommentPageNumber = 1;
        mCommentsLoading = true;
        doCommentsPagination = true;

        initCommentList();
        final CommentsAdapter commentsAdapter = new CommentsAdapter(mComments);
        initCommentRecyclerView(context, commentsRecyclerView, commentsAdapter);
        setSugg_CommentsRecyclerViewScrollListener(context, commentsRecyclerView, commentsAdapter, content);
        loadComments(context, mCommentPageNumber, content.getContent_id(), commentsAdapter);

        popWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        });

        mPostCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(commentsEditText.getText().toString())) {
                    commentSuggestion(position, context, content, commentsEditText.getText().toString(), onContentLikeCommentListener, commentsAdapter);
                    commentsEditText.setText("");
                }
            }
        });
    }

    private static void initCommentRecyclerView(Context context, RecyclerView recyclerView, CommentsAdapter commentsAdapter) {
        mCommentsLinearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mCommentsLinearLayoutManager);
        recyclerView.setAdapter(commentsAdapter);
    }

    private static void initCommentList() {
        mComments = new ArrayList<>();
    }

    private static void setSugg_CommentsRecyclerViewScrollListener(final Context context, RecyclerView commentsRecyclerView, final CommentsAdapter commentsAdapter, final SuggestionModel content) {
        //Fetching next page's data on reaching bottom

        commentsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (doCommentsPagination && dy > 0) //check for scroll down
                {
                    int visibleItemCount, totalItemCount, pastVisiblesItems;
                    visibleItemCount = mCommentsLinearLayoutManager.getChildCount();
                    totalItemCount = mCommentsLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mCommentsLinearLayoutManager.findFirstVisibleItemPosition();

                    if (mCommentsLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount - 2) {
                            mCommentsLoading = false;
                            //    addProgressBarInCommentList();
                            commentsAdapter.notifyItemInserted(mComments.size() - 1);
                            mCommentPageNumber++;
                            loadComments(context, mCommentPageNumber, content.getContent_id(), commentsAdapter);
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    public static void loadComments(final Context context, int pageNmbr, String suggestion_id, final CommentsAdapter adapter) {

        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.SUGGESTION_ID, suggestion_id);
            jsonObject.put(Constants.Network.PAGE_NUMBER, pageNmbr);
            jsonObject.put(Constants.Network.ACTION, Constants.Network.LOAD_SUGGESTION_COMMENTS);

            Logger.info("TAG", jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(context, Request.Method.POST, Urls.MYCOMPANY_API,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                                    // mComments.remove(mComments.size() - 1);
                                    //  adapter.notifyItemRemoved(mComments.size() - 1);

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = jsonArray.length() - 1; i >= 0; i--) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Comment comment = new Comment();
                                        comment.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                        comment.setFullUserName(jsonObject1.getString(Constants.Network.FULL_USER_NAME));
                                        comment.setProfilePic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                        comment.setDesignation(jsonObject1.getString(Constants.Network.DESIGNATION));
                                        comment.setCreatedAt(jsonObject1.getString(Constants.Comment.CREATED_AT));
                                        comment.setComment(jsonObject1.getString(Constants.Network.COMMENT));
                                        comment.setCompanyId(jsonObject1.getString(Constants.Network.COMPANY_ID));
                                        comment.setCommentType(Constants.Comment.COMMENT_TYPE_COMMENT);

                                        mComments.add(comment);
                                    }

                                    adapter.notifyItemRangeInserted(mComments.size() - jsonArray.length(), jsonArray.length());

                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout((Activity) context);
                                } else {

                                    //stop call to pagination in any case
                                    doCommentsPagination = false;

                                    if (mCommentPageNumber == 1) {
                                        //no comments
                                    } else {
                                        //no more comments
                                    }

                                    //mComments.remove(mComments.size() - 1);
                                    //adapter.notifyItemRemoved(mComments.size() - 1);
                                }

                                mCommentsLoading = true;


                            } catch (Exception e) {
                                Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                        "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            volleyError.printStackTrace();

                            Error error = Error.generateError(VolleyError.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                                    "fetchData", Error.getErrorTypeFromVolleyError(volleyError), volleyError.getMessage());
                        }
                    });

            jsonObjectRequest.setTag("TAG");
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {


            Error error = Error.generateError(JSONException.class.getSimpleName(), ContentFragment.class.getSimpleName(),
                    "fetchData", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());

        }
    }

    private static void fetchComments(final Context context, final CommentsAdapter commentsAdapter, final Content content) {

        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.Network.PAGE_NUMBER, mCommentPageNumber);
            jsonObject.put(Constants.Network.ACTION, Constants.Comment.LOAD_COMMENTS);
            jsonObject.put(Constants.Network.CONTENT_ID, content.getContentId());
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);

            Logger.error(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest =
                    new JsonObjectAuthRequest(context, Request.Method.POST, Urls.CONTENT,
                            jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Logger.error(TAG, response.toString());

                            try {
                                if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                                    //Getting jsonArray from response object
                                    JSONArray jsonArray = response.getJSONArray(Constants.Network.DATA);

                                    //Adding jsonArray data into a list of jsonObjects
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        Comment comment = new Comment();
                                        comment.setUserId(jsonObject1.getString(Constants.Network.USER_ID));
                                        comment.setFullUserName(jsonObject1.getString(Constants.Network.FULL_USER_NAME));
                                        comment.setProfilePic(jsonObject1.getString(Constants.Network.PROFILE_PIC));
                                        comment.setDesignation(jsonObject1.getString(Constants.Network.DESIGNATION));
                                        comment.setCreatedAt(jsonObject1.getString(Constants.Comment.CREATED_AT));
                                        comment.setComment(jsonObject1.getString(Constants.Network.COMMENT));
                                        comment.setCompanyId(jsonObject1.getString(Constants.Network.COMPANY_ID));
                                        comment.setCommentType(Constants.Comment.COMMENT_TYPE_COMMENT);

                                        mComments.add(comment);
                                    }
                                    mCommentPageNumber++;
                                    fetchComments(context, commentsAdapter, content);
                                    mCommentsLinearLayoutManager.scrollToPosition(mComments.size() - 1);
                                } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                                    SideBarPanel.logout((Activity) context);
                                } else {

                                    commentsAdapter.notifyDataSetChanged();
                                    //stop call to pagination in any case
                                    doCommentsPagination = false;

                                    if (mCommentPageNumber == 1) {
                                        //no comments
                                    } else {
                                        //no more comments
                                    }

//                                    mComments.remove(mComments.size() - 1);
//                                    commentsAdapter.notifyItemRemoved(mComments.size() - 1);
                                }

                                mCommentsLoading = true;

                            } catch (JSONException e) {
                                Logger.error(TAG, "In OnResponse" + e.getMessage());
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonMethods.displayToast(context, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                            Logger.info(TAG, error.getMessage() + " is the volley error ");
                        }
                    });

            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            Logger.error(TAG, "In fetchData" + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void commentSuggestion(final int position, final Context context, final SuggestionModel content, final String comment, final OnContentLikeCommentListener onContentLikeCommentListener, final CommentsAdapter commentsAdapter) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.Network.ACTION, Constants.Network.POST_COMMENT_SUGGESTION);
            jsonObject.put(Constants.Network.SUGGESTION_ID, content.getContent_id());
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(context).getUserId());
            jsonObject.put(Constants.Network.COMMENT, comment);

            Logger.info(TAG, jsonObject.toString() + " is the comment jsonObject");

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(context, Request.Method.POST, Urls.MYCOMPANY_API,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {
                            Logger.error(TAG, response.toString());

                            JSONObject nestedJsonObject = response.getJSONObject(Constants.Network.DATA);
                            String commmentBy = nestedJsonObject.getString(Constants.Network.FULL_USER_NAME);
                            String commentCount = nestedJsonObject.getString(Constants.Network.COMMENT_COUNT);

                            content.setComment_count(commentCount);

                            Comment commentObject = new Comment();
                            commentObject.setComment(comment);
                            commentObject.setUserId(User.getCurrentUser(context).getUserId());
                            commentObject.setProfilePic(User.getCurrentUser(context).getProfilePic());
                            commentObject.setCommentType(Constants.Comment.COMMENT_TYPE_COMMENT);
                            commentObject.setCreatedAt("moments ago");
                            commentObject.setFullUserName(commmentBy);
                            mComments.add(commentObject);

                            commentsAdapter.notifyItemInserted(mComments.size() - 1);
                            mCommentsLinearLayoutManager.scrollToPosition(mComments.size());
                            onContentLikeCommentListener.onContentCommented(content, commentCount, position);

                        } else if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_UNAUTHORISED)) {
                            SideBarPanel.logout((Activity) context);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonMethods.displayToast(context, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                    //Request failed, revert back button changes
                }
            });

            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private static void commentItem(final int position, final Context context, final Content content, final String comment, final OnContentLikeCommentListener onContentLikeCommentListener, final CommentsAdapter commentsAdapter) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.Network.ACTION, Constants.Network.ACTION_POST_COMMENT);
            jsonObject.put(Constants.Network.CONTENT_ID, content.getContentId());
            jsonObject.put(Constants.Network.CONTENT_STATUS_TYPE, Constants.Content.CONTENT_STATUS_TYPE_DEFAULT);
            jsonObject.put(Constants.Network.USER_ID, User.getCurrentUser(context).getUserId());
            jsonObject.put(Constants.Network.CONTENT_TYPE, content.getContentType());
            jsonObject.put(Constants.Network.COMMENT, comment);

            Logger.info(TAG, jsonObject.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectAuthRequest(context, Request.Method.POST, Urls.CONTENT,
                    jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        Logger.error(TAG, response.toString());

                        if (response.getString(Constants.Network.RESPONSE_CODE).equals(Constants.Network.RESPONSE_OK)) {

                            JSONObject nestedJsonObject = response.getJSONObject(Constants.Network.DATA);
                            String commmentBy = nestedJsonObject.getString(Constants.Network.FULL_USER_NAME);
                            String commentCount = nestedJsonObject.getString(Constants.Network.COMMENT_COUNT);

                            //Commented, update comments count
                            content.setCommentCount(commentCount);

                            Comment commentObject = new Comment();
                            commentObject.setComment(comment);
                            commentObject.setUserId(User.getCurrentUser(context).getUserId());
                            commentObject.setProfilePic(User.getCurrentUser(context).getProfilePic());
                            commentObject.setCommentType(Constants.Comment.COMMENT_TYPE_COMMENT);
                            commentObject.setCreatedAt("moments ago");
                            commentObject.setFullUserName(commmentBy);
                            mComments.add(commentObject);

                            commentsAdapter.notifyItemInserted(mComments.size() - 1);
                            mCommentsLinearLayoutManager.scrollToPosition(mComments.size());

                            onContentLikeCommentListener.onContentCommented(content, commentCount, position);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonMethods.displayToast(context, Constants.Network.SOMETHING_WENT_WRONG_MESSAGE);
                    //Request failed, revert back button changes
                }
            });

            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void handleLikeButtonConfig(Content content, Context context, ImageView likeImageView,
                                              TextView likeTextView, TextView likeCountView) {
        if (content.getLikeStatus().equals(Constants.Content.CONTENT_LIKED)) {
            likeImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.like_web_red));
            likeTextView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else {
            likeImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.like_web));
            likeTextView.setTextColor(ContextCompat.getColor(context, R.color.grey));
        }
        likeCountView.setText(content.getLikeCount());
        if (content.getLikeCount() != null && (!content.getLikeCount().equals("0"))) {
            likeCountView.setVisibility(View.VISIBLE);
            likeTextView.setText("Like  |");
        } else {
            likeCountView.setVisibility(View.GONE);
            likeTextView.setText("Like");
        }
    }


    public static void handle_Sugg_LikeButtonConfig(SuggestionModel content, Context context
            , ImageView likeImageView, TextView likeTextView, TextView likeCountView) {

        if (content.getLikeStatus().equals(Constants.Content.CONTENT_LIKED)) {
            likeImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.up_arrow_red));
            likeTextView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            likeTextView.setText("Upvoted");
            if (likeCountView != null) {
                likeCountView.setText(content.getLike_count() + "");
            }

        } else {
            likeImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.up_arrow));
            likeTextView.setTextColor(ContextCompat.getColor(context, R.color.grey));
            likeTextView.setText("Upvote");
            if (likeCountView != null) {
                likeCountView.setText(content.getLike_count() + "");
            }
        }
    }

    public static void handleSugges_LikeButtonConfig(SuggestionModel content, Context context, View suggestLay) {
        ImageView likeImageView = suggestLay.findViewById(R.id.like_image_sugg);
        TextView likeTextView = suggestLay.findViewById(R.id.like_text_sugg);
        TextView like_count_sugg = suggestLay.findViewById(R.id.like_count_sugg);

        handle_Sugg_LikeButtonConfig(content, context, likeImageView, likeTextView, like_count_sugg);
    }

    public static void handleLikeButtonConfig(Content content, Context context, View likeLayout) {
        ImageView likeImageView = likeLayout.findViewById(R.id.like_image);
        TextView likeTextView = likeLayout.findViewById(R.id.like_text);
        TextView likeCountView = likeLayout.findViewById(R.id.like_count);
        handleLikeButtonConfig(content, context, likeImageView, likeTextView, likeCountView);
    }


    //liked_status
    public static void switchLikeInfo(Content content) {
        if (content.getLikeStatus().equals(Constants.Content.CONTENT_LIKED)) {
            content.setLikeStatus(Constants.Content.CONTENT_UNLIKED);
            int newLikeCount = Integer.parseInt(content.getLikeCount()) - 1;
            content.setLikeCount(String.valueOf(newLikeCount));
        } else {
            content.setLikeStatus(Constants.Content.CONTENT_LIKED);
            int newLikeCount = Integer.parseInt(content.getLikeCount()) + 1;
            content.setLikeCount(String.valueOf(newLikeCount));
        }
    }

    public static void switchSuggs_LikeInfo(SuggestionModel content) {
        if (content.getLikeStatus().equals(Constants.Content.CONTENT_LIKED)) {
            content.setLikeStatus(Constants.Content.CONTENT_UNLIKED);
            int newLikeCount = Integer.parseInt(content.getLike_count()) - 1;
            content.setLike_count(String.valueOf(newLikeCount));
        } else {
            content.setLikeStatus(Constants.Content.CONTENT_LIKED);
            int newLikeCount = Integer.parseInt(content.getLike_count()) + 1;
            content.setLike_count(String.valueOf(newLikeCount));
        }
    }
}
