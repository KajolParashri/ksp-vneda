package com.indiareads.ksp.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.OpenableColumns;
import android.support.v4.app.Fragment;

import com.indiareads.ksp.listeners.OnFileCopiedListener;
import com.indiareads.ksp.model.Error;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class FilePicker {

    public final static int REQUEST_CODE_CHOOSE_FILE = 100;
    public final static int REQUEST_CODE_CHOOSE_INFO = 104;
    public final static int REQUEST_CODE_CHOOSE_VIDEO = 101;
    public final static int REQUEST_CODE_CHOOSE_AUDIO = 102;
    public final static int REQUEST_CODE_CHOOSE_PDF = 103;
    public final static int REQUEST_CODE_CHOOSE_IMAGE = 105;

    private static String TAG = FilePicker.class.getSimpleName();

    public static void chooseFile(Activity context, int REQUEST_CODE) {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        switch (REQUEST_CODE) {
            case REQUEST_CODE_CHOOSE_FILE:
                intent.setType("*/*");
                break;
            case REQUEST_CODE_CHOOSE_INFO:
                intent.setType("*/*");
                break;
            case REQUEST_CODE_CHOOSE_AUDIO:
                intent.setType("audio/*");
                break;
            case REQUEST_CODE_CHOOSE_VIDEO:
                intent.setType("video/*");
                break;
            case REQUEST_CODE_CHOOSE_PDF:
                intent.setType("application/pdf");
                break;
            case REQUEST_CODE_CHOOSE_IMAGE:
                intent.setType("image/*");
                break;
        }

        context.startActivityForResult(intent, REQUEST_CODE);
    }


    public static void chooseFile(Fragment fragment, int REQUEST_CODE) {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);

        switch (REQUEST_CODE) {
            case REQUEST_CODE_CHOOSE_FILE:
                intent.setType("*/*");
                break;
            case REQUEST_CODE_CHOOSE_INFO:
                intent.setType("image/*|application/pdf");
                break;
            case REQUEST_CODE_CHOOSE_AUDIO:
                intent.setType("audio/*");
                break;
            case REQUEST_CODE_CHOOSE_VIDEO:
                intent.setType("video/*");
                break;
            case REQUEST_CODE_CHOOSE_PDF:
                intent.setType("application/pdf");
                break;
        }

        fragment.startActivityForResult(intent, REQUEST_CODE_CHOOSE_FILE);
    }

    public static String getFileName(ContentResolver contentResolver, Uri uri) {
        Cursor cursor = contentResolver.query(uri, null, null, null, null, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                if (!cursor.isNull(cursor.getColumnIndex(OpenableColumns.SIZE))) {
                    String fileName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    if (contentResolver.getType(uri).contains("audio")) {
                        String ext = "." + CommonMethods.getExtensionFile(contentResolver, uri);
                        if (!fileName.endsWith(ext)) {
                            fileName = fileName + ext;
                        }
                    }
                    return fileName;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (NullPointerException e) {
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static void copyFileFromUri(Context context, Uri uri, OnFileCopiedListener onFileCopiedListener) {
        try {
            ContentResolver contentResolver = context.getContentResolver();
            new FileCopyTask(contentResolver, uri, onFileCopiedListener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        } catch (Exception e) {
            Error error = Error.generateError(FileNotFoundException.class.getSimpleName(), TAG
                    , "copyFileFromUri", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
            onFileCopiedListener.onFileCopyFailed(error);
        }
    }

    private static class FileCopyTask extends AsyncTask<String, String, String> {
        private ContentResolver contentResolver;
        private Uri uri;
        private OnFileCopiedListener onFileCopiedListener;
        private File targetFile;

        public FileCopyTask(ContentResolver contentResolver, Uri uri, OnFileCopiedListener onFileCopiedListener) {
            this.contentResolver = contentResolver;
            this.uri = uri;
            this.onFileCopiedListener = onFileCopiedListener;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                File rootDir = new File(Constants.File.ROOT_DIR_PATH);
                if (!rootDir.exists()) {
                    if (!rootDir.mkdir()) {
                        return null;
                    }
                }

                String fileName = getFileName(contentResolver, uri);
                if (fileName == null) {
                    return null;
                }
                Logger.info(TAG, fileName);

                targetFile = new File(Constants.File.ROOT_DIR_PATH + "/" + fileName);

                if (targetFile.exists()) {
                    return "Success";
                }

                InputStream inputStream = contentResolver.openInputStream(uri);

                final byte[] buffer = new byte[inputStream.available()];
                inputStream.read(buffer);

                OutputStream outStream = new FileOutputStream(targetFile);
                outStream.write(buffer);

                return "Success";
            } catch (Exception e) {
                Logger.error(TAG, e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s == null) {
                Error error = Error.generateError(FileNotFoundException.class.getSimpleName(), TAG
                        , "FileCopyTask", Error.ERROR_TYPE_EXCEPTION_ERROR, "Cannot create File");
                onFileCopiedListener.onFileCopyFailed(error);
            } else
                onFileCopiedListener.onFileCopied(targetFile);
        }
    }
}