package com.indiareads.ksp.utils;

public class HTMLUtils {


    public static String getLocalFontInHTML(String html, String fontFamily, String fontFileName) {

        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<style>" +
                "@font-face {" +
                "font-family: " + fontFamily + ";" +
                "src: url('" + fontFileName + "');" +
                "}" +
                "* {font-family: '" + fontFamily + "' !important;}" +
                "* {font-size: 1.05rem !important;}" +
                "* {line-height: 30px !important;}" +
                "</style>" +
                "</head>\n" +
                "<body>\n" +
                html +
                "\n" +
                "</body>\n" +
                "</html>";
    }

    public static String getLocalFontInBook(String html, String fontFamily, String fontFileName) {

        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<style>" +
                "@font-face {" +
                "font-family: " + fontFamily + ";" +
                "src: url('" + fontFileName + "');" +
                "}" +
                "* {font-family: '" + fontFamily + "' !important;}" +
                "* {font-size: 1 rem !important;}" +
                "* {line-height: 20px !important;}" +
                "</style>" +
                "</head>\n" +
                "<body>\n" +
                html +
                "\n" +
                "</body>\n" +
                "</html>";
    }

}
