package com.indiareads.ksp.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.widget.CircularProgressDrawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.indiareads.ksp.R;
import com.indiareads.ksp.model.Error;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import id.zelory.compressor.Compressor;

import static com.indiareads.ksp.utils.SharedPreferencesHelper.getCurrentUser;


/**
 * Created by Kashish on 24-07-2018.
 */

public class CommonMethods {
    static final Random mRandom = new Random(System.currentTimeMillis());


    public static final String TAG = CommonMethods.class.getSimpleName();
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final long WEEK_MILLIS = 7 * DAY_MILLIS;
    private static final long MONTH_MILLIS = 24 * DAY_MILLIS;
    private static final long YEAR_MILLIS = 12 * MONTH_MILLIS;

    private static List<String> imageExtensions = new ArrayList<>(Arrays.asList("ai", "bmp", "gif", "ico", "jpeg", "jpg", "png", "ps", "psd", "svg", "tif"));
    private static List<String> audioExtensions = new ArrayList<>(Arrays.asList("aif", "cda", "mid", "midi", "mp3", "mpa", "ogg", "wav", "wma", "wpl"));
    private static List<String> videoExtensions = new ArrayList<>(Arrays.asList("3g2", "3gp", "avi", "flv", "h264", "m4v", "mkv", "mov", "mp4", "mpg", "mpeg", "rm", "swf", "wmv"));
    private static List<String> pdfExtensions = new ArrayList<>(Arrays.asList("pdf"));
    private static List<String> zipExtensions = new ArrayList<>(Arrays.asList("zip", "z", "rar", "tar.gz", "rpm", "pkg", "deb"));
    private static List<String> textExtensions = new ArrayList<>(Arrays.asList("txt"));
    private static List<String> codeExtensions = new ArrayList<>(Arrays.asList("asp", "aspx", "cer", "cfm", "cgi", "pl", "css", "ht", "html", "js", "jsp", "php", "py", "rss", "xhtml", "c", "class", "cpp", "cs", "h", "java", "sh", "swift", "vb"));
    private static List<String> xlsExtensions = new ArrayList<>(Arrays.asList("xls", "xlsx", "xlr", "ods"));
    private static List<String> docExtensions = new ArrayList<>(Arrays.asList("doc", "docx", "odt", "rtf", "tex", "wks", "wps", "wpd"));
    private static List<String> pptExtensions = new ArrayList<>(Arrays.asList("ppt", "pptx"));

    public static void displayToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public static int generateRandomColor() {
        // This is the base color which will be mixed with the generated one
        final int baseColor = Color.WHITE;

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) / 2;
        final int green = (baseGreen + mRandom.nextInt(256)) / 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) / 2;

        return Color.rgb(red, green, blue);
    }

    public static int returnDynamicType(String key) {

        int value = 0;

        if (key.contains("banner_slider")) {
            value = 1;
        } else if (key.contains("dual_image")) {
            value = 2;
        } else if (key.contains("poster_image")) {
            value = 3;
        } else if (key.contains("slider")) {
            value = 4;
        } else if (key.contains("grid")) {
            value = 5;
        } else if (key.contains("categories")) {
            value = 6;
        } else if (key.contains("explore")) {
            value = 7;
        }

        return value;
    }

    public static String getStringSizeLengthFile(long size) {

        DecimalFormat df = new DecimalFormat("0.00");

        float sizeKb = 1024.0f;
        float sizeMb = sizeKb * sizeKb;
        float sizeGb = sizeMb * sizeKb;
        float sizeTerra = sizeGb * sizeKb;


        if (size < sizeMb)
            return df.format(size / sizeKb) + " Kb";
        else if (size < sizeGb)
            return df.format(size / sizeMb) + " Mb";
        else if (size < sizeTerra)
            return df.format(size / sizeGb) + " Gb";

        return "";
    }

    public static File fileCompress(File actualImage, Context activity) {
        File compressedImage = null;

        long size = actualImage.length();
        String sizeStr = getStringSizeLengthFile(size);
        Logger.debug("size", sizeStr);

        try {
            compressedImage = new Compressor(activity)
                    .setMaxWidth(512)
                    .setMaxHeight(512)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .setQuality(75)
                    .compressToFile(actualImage);

        } catch (Exception e) {

        }
        return compressedImage;
    }

    public static void loadNormalImageWithGlide(Context context, String imageUrl, ImageView imageView) {

        Glide.with(context)
                .load(imageUrl)
                .into(imageView);

    }

    public static void loadBannerWithGlide(Context context, String imageUrl, ImageView imageView) {

        Glide.with(context)
                .load(imageUrl)
                .into(imageView);

    }

    public static void loadNormalImageWithGlide(Context context, File imageFile, ImageView imageView) {
        Picasso.get().load(imageFile).error(R.mipmap.ic_fevicon)
                .placeholder(R.mipmap.ic_fevicon)
                .into(imageView);
    }

    public static String getExtensionFile(Context context, Uri uri) {
        String extension;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
        }
        return extension;
    }

    public static String getExtensionFile(ContentResolver contentResolver, Uri uri) {
        String extension;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(contentResolver.getType(uri));
        } else {
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
        }
        return extension;
    }

    public static String createUploadFileName(Uri uri, Context context) {
        File file = new File(uri.getPath());
        String name = "";

        String fileExt = MimeTypeMap.getFileExtensionFromUrl(file.toString());
        Logger.info("fileExtension", fileExt);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmss");
        String formattedDate = df.format(c.getTime());
        name = getCurrentUser(context)
                .getUserId() + "_" + formattedDate + "." + fileExt;
        Logger.info("fileExtension", name);
        return name;
    }

    public static String getOrderStatus(int orderStatus) {
        String orderStatusStr = "";
        switch (orderStatus) {
            case -1:
                orderStatusStr = "Deleted";
                break;
            case 1:
                orderStatusStr = "Order Placed";
                break;
            case 2:
                orderStatusStr = "Order Processing";
                break;
            case 3:
                orderStatusStr = "Dispatched";
                break;
            case 4:
                orderStatusStr = "Delivered";
                break;
            case 5:
                orderStatusStr = "Pickup Requested";
                break;
            case 6:
                orderStatusStr = "Pickup Processing";
                break;
            case 7:
                orderStatusStr = "Picked Up";
                break;
            case 8:
                orderStatusStr = "Return";
                break;
        }
        return orderStatusStr;
    }

    public static String getFileName(Uri uri, Context contexts) {
        String result;

        //if uri is content
        if (uri.getScheme() != null && uri.getScheme().equals("content")) {
            Cursor cursor = contexts.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    //local filesystem
                    int index = cursor.getColumnIndex("_data");
                    if (index == -1)
                        //google drive
                        index = cursor.getColumnIndex("_display_name");
                    result = cursor.getString(index);
                    if (result != null)
                        uri = Uri.parse(result);
                    else
                        return null;
                }
            } finally {
                cursor.close();
            }
        }

        result = uri.getPath();

        //get filename + ext of path

        if (result != null) {
            int cut = result.lastIndexOf('/');
            if (cut != -1)
                result = result.substring(cut + 1);
        }
        return result;
    }

    public static RoundedBitmapDrawable setCircularImage(int id, Context mContext) {
        Resources res = mContext.getResources();
        Bitmap src = BitmapFactory.decodeResource(res, id);
        RoundedBitmapDrawable roundedBitmapDrawable =
                RoundedBitmapDrawableFactory.create(res, src);
        roundedBitmapDrawable.setCornerRadius(Math.max(src.getWidth(), src.getHeight()) / 2.0f);
        return roundedBitmapDrawable;
    }

    public static void loadContentImageWithGlide(Context context, String imageUrl, ImageView imageView) {

        Glide.with(context)
                .load(imageUrl)
                .placeholder(R.mipmap.ic_fevicon)
                .error(R.mipmap.ic_fevicon)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(imageView);
    }

    public static String getContentNameFromType(String pos) {
        switch (pos) {
            case "1":
                return "Article";
            case "2":
                return "Video";
            case "3":
                return "Course";
            case "4":
                return "Podcast";
            case "5":
                return "InfoGraphic";
            case "6":
                return "Post";
            case "7":
                return "E Books";
            case "8":
                return "Announcement";
            case "9":
                return "Books";
        }
        return new String("");

    }

    public static int getContentTypeFromName(String pos) {

        switch (pos) {
            case "Article":
                return 1;
            case "Video":
                return 2;
            case "Course":
                return 3;
            case "Podcast":
                return 4;
            case "Infographic":
                return 5;
            case "Post":
                return 6;
            case "E Books":
                return 7;
            case "Announcement":
                return 8;
            case "Books":
                return 9;
            case "EBook":
                return 7;

        }
        return 1;

    }


    public static int getContentImageFromType(String pos) {

        switch (pos) {
            case "1":
                return R.drawable.article_explore;
            case "2":
                return R.drawable.video_explore;
            case "3":
                return R.drawable.course_explore;
            case "4":
                return R.drawable.podcast_expo;
            case "5":
                return R.drawable.info_explore;
            case "9":
                return R.drawable.book_explore;
            default:
                return R.drawable.article_explore;

        }
        //return new Integer(R.drawable.article_explore);
    }

    public static void loadDrawableWithGlide(Context context, int drawableId, ImageView imageView) {
        Glide.with(context)
                .load("")
                .placeholder(drawableId)
                .thumbnail(0.2f)
                .into(imageView);
    }

    public static void loadDrawableWithGlide(Context context, Drawable drawableId, ImageView imageView) {
        Glide.with(context)
                .load(drawableId)
                .thumbnail(0.2f)
                .into(imageView);
    }

    public static void loadProfileImageWithGlide(final Context context, String imageUrl, ImageView imageView) {

        Picasso.get().load(Urls.AWS_PROFILE_IMAGE_PATH + imageUrl).error(R.mipmap.ic_profile)
                .placeholder(R.mipmap.ic_profile)
                .transform(new CircleTransform())
                .into(imageView);
    }


    public static void loadProfileImage(String imageUrl, ImageView imageView) {

        Picasso.get().load(imageUrl).error(R.mipmap.ic_profile)
                .placeholder(R.mipmap.ic_profile)
                .transform(new CircleTransform())
                .into(imageView);
    }

    public static void loadSourceImageWithGlide(Context context, String imageUrl, ImageView imageView) {

        Glide.with(context)
                .load(imageUrl)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);
    }

    public static void loadGroupImageWithGlide(Context context, String imageUrl, ImageView imageView) {

        Picasso.get().load(Urls.AWS_PROFILE_IMAGE_PATH + imageUrl).error(R.mipmap.ic_group)
                .placeholder(R.mipmap.ic_group)
                .error(R.mipmap.ic_group)
                .transform(new CircleTransform()).into(imageView);

    }

    public static void loadCompanyLogo(String imageUrl, ImageView imageView) {

        Picasso.get().load(Urls.AWS_PROFILE_IMAGE_PATH + imageUrl).error(R.drawable.profile)
                .placeholder(R.drawable.profile)
                .into(imageView);

    }

    public static CircularProgressDrawable getProgressDrawable(Context context) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(7f);
        circularProgressDrawable.setCenterRadius(20f);

        circularProgressDrawable.start();
        return circularProgressDrawable;
    }

    public static void showProgressBar(Activity context, int rootViewId) {
        View myView = LayoutInflater.from(context).inflate(R.layout.fragment_progress, null);
        ((FrameLayout) context.findViewById(rootViewId)).addView(myView);
    }

    public static void hideProgressBar(Activity context, int rootViewId) {
        ((FrameLayout) context.findViewById(rootViewId)).removeView(context.findViewById(R.id.fragment_progress_root_layout));
    }

    public static void showProgressBar(View fragmentRootView, int rootViewId) {
        View myView = LayoutInflater.from(fragmentRootView.getContext()).inflate(R.layout.fragment_progress, null);
        ((FrameLayout) fragmentRootView.findViewById(rootViewId)).addView(myView);
    }

    public static void hideProgressBar(View fragmentRootView, int rootViewId) {
        if ((fragmentRootView.findViewById(rootViewId).findViewById(R.id.fragment_progress_root_layout) != null)) {
            fragmentRootView.findViewById(rootViewId).findViewById(R.id.fragment_progress_root_layout).setVisibility(View.GONE);
            ((FrameLayout) fragmentRootView.findViewById(rootViewId)).removeView(fragmentRootView.findViewById(R.id.fragment_progress_root_layout));
        }
    }

    public static void showErrorView(final Fragment fragment, int rootViewId, Error error) {

        final View errorView = LayoutInflater.from(fragment.getContext()).inflate(R.layout.error_layout, null);
        ((FrameLayout) fragment.getView().findViewById(rootViewId)).addView(errorView);

        if (error.getType().equals(Error.ERROR_TYPE_NO_CONNECTION_ERROR)) {
            ((TextView) errorView.findViewById(R.id.error_layout_text)).setText(R.string.connection_error_message);
            ((ImageView) errorView.findViewById(R.id.error_layout_image)).setImageResource(R.drawable.ic_signal_wifi_off);
        } else {
            ((TextView) errorView.findViewById(R.id.error_layout_text)).setText(R.string.something_went_wrong);
            ((ImageView) errorView.findViewById(R.id.error_layout_image)).setImageResource(R.drawable.ic_error_outline);
        }

        errorView.setVisibility(View.VISIBLE);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorView.setVisibility(View.GONE);
            }
        });
    }

    public static void showErrorView(final Activity activity, int rootViewId, Error error) {
        final View errorView = LayoutInflater.from(activity).inflate(R.layout.error_layout, null);
        ((FrameLayout) activity.findViewById(rootViewId)).addView(errorView);
        if (error.getType().equals(Error.ERROR_TYPE_NO_CONNECTION_ERROR)) {
            ((TextView) errorView.findViewById(R.id.error_layout_text)).setText(R.string.connection_error_message);
            ((ImageView) errorView.findViewById(R.id.error_layout_image)).setImageResource(R.drawable.ic_signal_wifi_off);
        } else {
            ((TextView) errorView.findViewById(R.id.error_layout_text)).setText(R.string.something_went_wrong);
            ((ImageView) errorView.findViewById(R.id.error_layout_image)).setImageResource(R.drawable.ic_error_outline);
        }

        errorView.setVisibility(View.VISIBLE);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorView.setVisibility(View.GONE);
            }
        });
    }

    public static void showErrorViewNoContent(final Activity activity, int rootViewId, Error error) {
        final View errorView = LayoutInflater.from(activity).inflate(R.layout.error_layout, null);
        ((FrameLayout) activity.findViewById(rootViewId)).addView(errorView);
        if (error.getType().equals(Error.ERROR_TYPE_NO_CONNECTION_ERROR)) {
            ((TextView) errorView.findViewById(R.id.error_layout_text)).setText(R.string.connection_error_message);
            ((ImageView) errorView.findViewById(R.id.error_layout_image)).setImageResource(R.drawable.ic_signal_wifi_off);
        } else {
            ((TextView) errorView.findViewById(R.id.error_layout_text)).setText(R.string.no_orders);
            ((ImageView) errorView.findViewById(R.id.error_layout_image)).setVisibility(View.GONE);
        }

        errorView.setVisibility(View.VISIBLE);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorView.setVisibility(View.GONE);
            }
        });
    }

    public static int getIconImage(String contentType) {

        int type = Integer.parseInt(contentType);

        if (type == Constants.Content.CONTENT_TYPE_INFOGRAPHIC)
            return R.drawable.info_explore;
        else if (type == Constants.Content.CONTENT_TYPE_ARTICLE)
            return R.drawable.article_explore;
        else if (type == Constants.Content.CONTENT_TYPE_VIDEO)
            return R.drawable.video_explore;
        else if (type == Constants.Content.CONTENT_TYPE_PODCAST)
            return R.drawable.podcast_expo;
        else if (type == Constants.Content.CONTENT_TYPE_COURSE)
            return R.drawable.course_explore;
        else
            return R.drawable.book_explore;
    }

    public static String getElapsedTime(String createdDate) {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            long time = sdf.parse(createdDate).getTime();

            if (time < 1000000000000L) {
                time *= 1000;
            }

            long now = System.currentTimeMillis();
            if (time > now || time <= 0) {
                return null;
            }

            final long diff = now - time;
            if (diff < MINUTE_MILLIS) {
                return "just now";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago";
            } else if (diff < 50 * MINUTE_MILLIS) {
                return diff / MINUTE_MILLIS + " minutes ago";
            } else if (diff < 90 * MINUTE_MILLIS) {
                return "an hour ago";
            } else if (diff < 24 * HOUR_MILLIS) {
                return diff / HOUR_MILLIS + " hours ago";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday";
            } else if (diff < WEEK_MILLIS) {
                return diff / DAY_MILLIS + " days ago";
            } else if (diff < MONTH_MILLIS) {

                if (diff / WEEK_MILLIS == 1) {
                    return diff / WEEK_MILLIS + " week ago";
                } else {
                    return diff / WEEK_MILLIS + " weeks ago";
                }

            } else if (diff < YEAR_MILLIS) {
                if (diff / MONTH_MILLIS == 1) {
                    return diff / MONTH_MILLIS + " month ago";
                } else {
                    return diff / MONTH_MILLIS + " months ago";
                }
            } else {
                if (diff / YEAR_MILLIS == 1) {
                    return diff / YEAR_MILLIS + " years ago";
                } else {
                    return diff / YEAR_MILLIS + " years ago";
                }
            }
        } catch (ParseException e) {
            if (createdDate.equalsIgnoreCase("yesterday"))
                return createdDate;

            if (createdDate.equalsIgnoreCase("0 minutes ago"))
                return "moments ago";

            createdDate = createdDate.split(",")[0];

            if (createdDate.contains("ago") || createdDate.contains("yesterday"))
                return createdDate;
            else
                return createdDate + " ago";
        }
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context) {
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int getNoOfColumnsForGridView(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }

    public static void filterContentsJsonArray(JSONArray jsonArray, String skipContentType) {

        try {
            String[] contentTypes = skipContentType.split(",");
            int i, j;

            if (!skipContentType.isEmpty()) {
                for (i = 0; i < jsonArray.length(); i++) {
                    for (j = 0; j < contentTypes.length; j++) {

                        if (contentTypes[j].equals(jsonArray.getJSONObject(i).getString(Constants.Network.CONTENT_TYPE))) {
                            jsonArray.remove(i);
                            i--;
                            break;
                        }

                    }
                }
            }
        } catch (JSONException e) {
            Error.generateError(JSONException.class.getSimpleName(), CommonMethods.class.getSimpleName(),
                    "filterContentsJsonArray", Error.ERROR_TYPE_EXCEPTION_ERROR, e.getMessage());
        }
    }

    public static String checkNotNull(String value) {

        if (value == null || value.trim().isEmpty())
            return "";
        return value;

    }

    public static int getContentTypeNameFromContentType(String mContentType) {

        if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_ARTICLE)))
            return R.string.article;
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_INFOGRAPHIC)))
            return R.string.infographic;
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_PODCAST)))
            return R.string.podcast;
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_COURSE)))
            return R.string.course;
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_VIDEO)))
            return R.string.video;
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_BOOK_LIBRARY)))
            return R.string.library_book;
        else
            return R.string.ebook;

    }

    public static int getContentTypeDescriptionFromContentType(String mContentType) {

        if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_ARTICLE)))
            return R.string.article_description;
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_INFOGRAPHIC)))
            return R.string.infographic_description;
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_PODCAST)))
            return R.string.podcast_description;
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_COURSE)))
            return R.string.course_description;
        else if (mContentType.equals(String.valueOf(Constants.Content.CONTENT_TYPE_VIDEO)))
            return R.string.video_description;
        else
            return R.string.ebook_description;
    }

    public static void toggleVisibility(View view) {

        if (view.getVisibility() == View.VISIBLE)
            view.setVisibility(View.GONE);
        else
            view.setVisibility(View.VISIBLE);
    }

    public static String getFileTypeFromName(String name) {

        String extention = "";
        if (name != null) {
            extention = name.substring(name.lastIndexOf(".") + 1).toLowerCase();
        }

        if (imageExtensions.contains(extention))
            return Constants.File.FILE_TYPE_IMAGE;
        else if (audioExtensions.contains(extention))
            return Constants.File.FILE_TYPE_AUDIO;
        else if (videoExtensions.contains(extention))
            return Constants.File.FILE_TYPE_VIDEO;
        else if (pdfExtensions.contains(extention))
            return Constants.File.FILE_TYPE_PDF;
        else if (zipExtensions.contains(extention))
            return Constants.File.FILE_TYPE_ARCHIVE;
        else if (textExtensions.contains(extention))
            return Constants.File.FILE_TYPE_TEXT;
        else if (codeExtensions.contains(extention))
            return Constants.File.FILE_TYPE_CODE;
        else if (xlsExtensions.contains(extention))
            return Constants.File.FILE_TYPE_EXCEL;
        else if (docExtensions.contains(extention))
            return Constants.File.FILE_TYPE_DOC;
        else if (pptExtensions.contains(extention))
            return Constants.File.FILE_TYPE_POWERPOINT;
        else
            return Constants.File.FILE_TYPE_OTHER;

    }

    public static void hideSoftInput(Activity activity) {

        View view = activity.getCurrentFocus();
        if (view != null) {
            view.clearFocus();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static int getFileIconFromType(String fileType) {
        return R.drawable.file_repo;
    }

    public static String getCurrentDate(String pattern) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(c.getTime());
    }

    public static List<String> getStateList() {

        List<String> list = new ArrayList<>();
        list.add("Select State");
        list.add("Andhra Pradesh");
        list.add("Arunachal Pradesh");
        list.add("Assam");
        list.add("Bihar");

        list.add("Chhattisgarh");
        list.add("Dadra and Nagar Haveli");
        list.add("Daman and Diu");
        list.add("Delhi");

        list.add("Goa");
        list.add("Gujarat");
        list.add("Haryana");
        list.add("Himachal Pradesh");

        list.add("Jammu and Kashmir");
        list.add("Jharkhand");
        list.add("Karnataka");
        list.add("Kerala");

        list.add("Madhya Pradesh");
        list.add("Maharashtra");
        list.add("Manipur");
        list.add("Meghalaya");

        list.add("Mizoram");
        list.add("Nagaland");
        list.add("Odisha");
        list.add("Puducherry");

        list.add("Punjab");
        list.add("Rajasthan");
        list.add("Sikkim");
        list.add("Tamil Nadu");

        list.add("Telengana");
        list.add("Tripura");
        list.add("Uttar Pradesh");
        list.add("Uttarakhand");

        list.add("West Bengal");

        return list;
    }

}
