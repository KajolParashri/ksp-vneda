package com.indiareads.ksp.utils;

import android.util.Log;

import com.indiareads.ksp.model.Error;

public class Logger {

    public static final String TAG = "KSP Logger";

    private static final boolean DEBUGGING_BUILD = false;

    public static void debug(String tag, String message) {
        if (DEBUGGING_BUILD)
            Log.d(tag, message);
    }

    public static void info(String tag, String message) {
        if (DEBUGGING_BUILD)
            Log.i(tag, message);
    }

    public static void error(String tag, String message) {
        if (DEBUGGING_BUILD)
            Log.e(tag, message);
    }

    public static void error(String tag, String message, Throwable e) {
        if (DEBUGGING_BUILD)
            Log.e(tag, message, e);
    }

    public static void error(Error error) {
        if (DEBUGGING_BUILD)
            Log.e(error.getContainerClassName(), error.getContainerFunctionName() + " " + error.getErrorClassName() + " " + error.getMessage());
    }

}
