package com.indiareads.ksp.utils;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.indiareads.ksp.view.activity.ViewEbookActivity;

import static com.indiareads.ksp.view.activity.ViewEbookActivity.CurrentCount;
import static com.indiareads.ksp.view.activity.ViewEbookActivity.hideprogress;
import static com.indiareads.ksp.view.activity.ViewEbookActivity.setCount;

public final class CustomWebView extends WebView {
    private float x1;
    private int pageCount;
    private int currentPage;
    private int currentX;
    Context mContext;
    float deltaX;
    private int delta;

    public CustomWebView(Context context) {
        super(context);
        this.mContext = context;
    }

    private final int getPrevPagePosition() {
        this.currentPage += -1;
        return (int) Math.ceil((double) (this.currentPage * this.getMeasuredWidth()));
    }

    private final int getNextPagePosition() {
        ++this.currentPage;
        return (int) Math.ceil((double) (this.currentPage * this.getMeasuredWidth()));
    }

    private final int getPagePosition(int pos) {
        return (int) Math.ceil((double) (pos * this.getMeasuredWidth()));
    }

    private final void setDelta() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Context mContext = this.getContext();
        if (mContext == null) {

        } else {
            WindowManager windowManager = ((Activity) mContext).getWindowManager();
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
            this.delta = (int) ((double) displayMetrics.widthPixels * 0.05D);
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case 0:
                this.x1 = event.getX();
                return super.onTouchEvent(event);
            case 1:
                float x2 = event.getX();
                deltaX = x2 - this.x1;
                if (Math.abs(deltaX) > (float) this.delta) {
                    boolean checkBool;

                    if (x2 > this.x1) {
                        this.turnPageLeft(deltaX);
                        checkBool = true;
                        setCount(currentPage + "/" + pageCount);
                        CurrentCount(currentPage);
                    } else {
                        this.turnPageRight(deltaX);
                        checkBool = true;
                        setCount(currentPage + "/" + pageCount);
                        CurrentCount(currentPage);
                    }

                    return checkBool;
                }
                break;
            case 2:
                super.onTouchEvent(event);
                break;
            default:
                super.onTouchEvent(event);
        }

        return super.onTouchEvent(event);
    }

    public final void turnPageLeft(float deltaX) {
        if (this.currentPage > 0) {
            int scrollX = this.getPrevPagePosition();
            this.loadAnimation(scrollX, deltaX);
            this.currentX = scrollX;
            this.scrollTo(scrollX, 0);
        }

    }

    public final void turnToContinue(int currentPage) {
        if (this.currentPage > 0) {
            int scrollX = getPagePosition(currentPage);
            this.loadAnimation(scrollX, this.deltaX);
            this.currentX = scrollX;
            this.scrollTo(scrollX, 0);
        }

    }

    public final void turnPageRight(float deltaX) {
        if (this.currentPage < this.pageCount - 1) {
            int scrollX = this.getNextPagePosition();
            this.loadAnimation(scrollX + 10, deltaX);
            this.currentX = scrollX + 10;
            this.scrollTo(scrollX + 10, 0);
        }

    }

    private final void loadAnimation(int scrollX, float deltaX) {
        ObjectAnimator anim = ObjectAnimator.ofInt(this, "scrollX", new int[]{this.currentX - (int) deltaX, scrollX});
        anim.setDuration(200L);
        anim.setInterpolator((TimeInterpolator) (new LinearInterpolator()));
        anim.start();
    }

    private final void injectJavascript() {
        String js = "function initialize(){\n    var d = document.getElementsByTagName('body')[0];\n    var ourH = window.innerHeight - 40;\n    var ourW = window.innerWidth - (2*15);\n    var fullH = d.offsetHeight;\n    var pageCount = Math.floor(fullH/ourH)+1;\n    var currentPage = 0;\n    var newW = pageCount*window.innerWidth - (2*15);\n    d.style.height = ourH+'px';\n    d.style.width = newW+'px';\n    d.style.margin = 0;\n    d.style.webkitColumnGap = '20px';\n    d.style.webkitColumnCount = pageCount;\n    document.head.innerHTML = document.head.innerHTML + '<meta name=\"viewport\" content=\"height=device-height, user-scalable=yes\" />';    return pageCount;\n}";
        this.loadUrl("javascript:" + js);
        this.loadUrl("javascript:alert(initialize())");
    }

    private final void injectCSS() {
        this.loadUrl("javascript:(function() {var parent = document.getElementsByTagName('head').item(0);var style = document.createElement('style');style.type = 'text/css';style.innerHTML = 'body { padding: 20px 20px !important; }';parent.appendChild(style)})()");
    }

    public final void setPageCount(int pageCount) {
        this.pageCount = pageCount;
        setCount(0 + "/" + pageCount);
        CurrentCount(currentPage);
    }

    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.x1 = -1.0F;
        this.delta = 30;
        this.setDelta();

        this.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
                // Do something with the event here
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (Uri.parse(url).getHost().equals("www.google.com")) {
                    // This is my web site, so do not override; let my WebView load the page
                    return false;
                }
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                CustomWebView.this.injectJavascript();
                hideprogress();
            }
        });
        this.

                setWebChromeClient((WebChromeClient) (new

                                                              WebChromeClient() {
                                                                  public boolean onJsAlert(WebView view, String url, String message, JsResult result) {

                                                                      int pageCount = Integer.parseInt(message);
                                                                      CustomWebView.this.setPageCount(pageCount);

                                                                      CustomWebView.this.injectCSS();
                                                                      result.confirm();

                                                                      return true;
                                                                  }
                                                              }));
    }
}
