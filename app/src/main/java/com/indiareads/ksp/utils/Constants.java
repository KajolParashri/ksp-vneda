package com.indiareads.ksp.utils;

import android.os.Environment;

import com.indiareads.ksp.BuildConfig;
import com.indiareads.ksp.R;

/**
 * Created by Kashish on 24-07-2018.
 */

public class Constants {

    public static final String PLACE_ORDER_SELECT_ADDRESS = "place_order_select_address";
    public static final String RETURN_ORDER_SELECT_ADDRESS = "return_order_select_address";
    public static final int RETURN_BOOK_ADDRESS_CODE = 16;
    public static final String RETURN_SUCCESS = "return_success";
    public static final String RETURN_ERROR = "return_error";

    public static final String AUTHOR = "author";
    public static final String LOGIN_TYPE = "Login Type";
    public static final String SAVED_USER_DETAILS = "User Details";
    public static final String USER_ID = "User Id";
    public static final String USER_NAME = "User Name";
    public static final String USER_EMAIL = "User Email";
    public static final String USER_GENDER = "User Gender";
    public static final String C_KEY = "C Key";
    public static final String ROLE_ID = "Role Id";
    public static final String PIC_URL = "Profile Picture Url";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String FULLNAME = "fullname";
    public static final String LOGGED_IN = "logged in";
    public static final String FIRST_TIME = "first time";
    public static final String LOCATION = "location";
    public static final String LOCATION1 = "location1";
    public static final String DB_LOCATION = "db_location";
    public static final String TUTORIAL_FLAG = "tutorial_flag";
    public static final String TUTORIAL_FLAG_USER = "tutorial_flag_user";

    public static class CreateContent {
        public static final String CREATE_CONTENT = "Create Content";
        public static final String CREATE_POST = "Create Post";
        public static final String ASK_QUESTION = "Ask Question";

    }

    //rank
    public static class Bookshelf {
        public static final String CURRENTLY_READING = "Currently Reading";
        public static final String AVAILABLE = "Available";
        public static final String HISTORY = "History";
        public static final String WISHLIST = "Wish list";
    }


    public static class LeaderBoard {
        public static final String COUNT_CREATE = "total_content";
        public static final String COUNT_CONSUME = "total_content_consumed";
        public static final String POINTS_CREATE = "total_create_points";
        public static final String POINTS_CONSUME = "total_consume_points";
        public static final String CREATE_POINTS_INDIVIDUAL = "individual_create_points";
        //consumed_content_by_content_type
        public static final String CONSUME_COUNT = "consumed_content_by_content_type";
        //individual_consume_points
        public static final String CONSUME_POINTS = "individual_consume_points";
        public static final String USER_ID = "user_id";
        public static final String COMPANY_ID = "company_id";

        public static final String TOTAL_POINTS = "total_indiareads_points";
        public static final String RANK = "user_rank";
        public static final String DATA = "data";
        public static final String DATA1 = "data1";
        public static final String SUM = "SUM";
        public static final String COUNT = "total_count";
        public static final String CONTENT_NAME = "content_name";
        public static final String CONTENT_TYPE = "content_type";
        public static final String SCORE = "score";
        public static final String FIRST_NAME = "first_name";
        public static final String PROFILE_PIC = "profile_pic";


        //score
        //first_name
        //profile_pic
    }

    public static class AWS {
        public static final String BUCKET_NAME = "assets.vneda.com";
        public static final String REPO_KEY = "ksp/repo/";
        public static final String MEDIA_KEY = "ksp/repo/media/";
        public static final String CREATE_CONTENT_KEY = "ksp/create/";
    }

    public static class Network {
        //existing_array
        //update_profile_picture_user
        public static final String UPDATE_PROFILE_PIC = "update_profile_picture_user";
        public static final String EXISTING_ARRAY = "existing_array";
        public static final String NEW_ARRAY = "new_array";

        public static final String RESULTS = "results";
        public static final String GET_BOOKSHELF_CONTENT = "get_bookshelf_content";
        public static final String SHELF_TYPE = "shelf_type";
        public static final String RESPONSE_CODE = "response_code";
        public static final String RESPONSE_MESSAGE = "response_message";
        public static final String RESPONSE_OK = "200";
        public static final String RESPONSE_ERROR = "400";
        public static final String INCORRECT_PASSWORD = "Error ! Incorrect Password";
        public static final String INCORRECT_PASSWORD_MESSAGE = "Incorrect Password";
        public static final String EMAIL_NOT_REGISTERED = "Email not found or not registered !";
        public static final String EMAIL_NOT_REGISTERED_MESSAGE = "Email not found or not registered !";
        public static final String INACTIVE_USER = "Inactive User";
        public static final String INACTIVE_USER_MESSAGE = "Inactive user";
        public static final String SUSPENDED_USER = "Suspended User";
        public static final String SUSPENDED_USER_MESSGAE = "Suspended User";
        public static final String SOMETHING_WENT_WRONG = "Something went wrong ! Please try again later !";
        public static final String SOMETHING_WENT_WRONG_MESSAGE = "Something went wrong ! Please try again later !";
        public static final String SERVER_ERROR_MESSAGE = "Server error";
        public static final String CONTENT_FETCHED_SUCCESSFULLY = "Content fetched Successfully !";

        public static final String ACTION = "action";
        public static final String ACTION_LOGIN = "login";
        public static final Object ACTION_DISPLAY_CONTENTS = "displaycontents";
        //get_top_ten

        public static final Object GET_POINTS = "get_points_new";
        public static final Object GET_TOP_TEN = "get_top_ten";
        public static final Object ACTION_PUBLISH_SUGGEST = "publish_suggestion";
        public static final Object ACTION_DISPLAY_SUGGEST = "display_suggestions";
        public static final String DATA = "data";

        public static final String POST_COMMENT_SUGGESTION = "postcomment_suggestion";
        public static final String PAGE_NUMBER = "page_number";
        public static final String SUGGESTION = "suggestion";
        public static final String LIKE_SUGGESTION = "like_suggestion";
        public static final String LOAD_SUGGESTION_COMMENTS = "loadcomments_suggestion";//
        public static final String LOAD_CONTENT_LIKES = "loadlikes";
        public static final String SUGGESTION_ID = "suggestion_id";
        public static final String USER_ID = "user_id";

        public static final String NAME1 = "name1";
        public static final String CONTACT1 = "contact1";
        public static final String MESSAGE1 = "message1";

        public static final String COMPANY_ID = "company_id";
        public static final String SKIP_CONTENT_TYPE = "skip_content_type";
        public static final String REQUIRED_CONTENT_TYPE = "required_content_type";
        public static final String SESSION_USER_ID = "session_user_id";
        public static final String CONTENT_STATUS_TYPE = "content_status_type";
        public static final String PASSWORD = "pass";
        public static final String EMAIL = "email";
        public static final String USER_FULLNAME = "user_fullname";
        public static final String PROFILE_PIC = "profile_pic";
        public static final String DESIGNATION = "designation";
        //order_status
        public static final String ORDER_STATUS = "order_status";
        public static final String CONTENT_ID = "content_id";//address_book_id
        public static final String CREATED_DATE = "created_date";
        public static final String TITLE = "title";
        public static final String THUMNAIL = "thumnail";
        public static final String THUMBNAIL = "thumbnail";
        public static final String DESCRIPTION = "description";

        public static final String E_BOOK_DATA = "ebook_data";
        public static final String SUMMARY_DATA = "summary_data";

        public static final String CONTENT_TYPE = "content_type";
        public static final String LIKED_STATUS = "liked_status";
        public static final String LIKE_COUNT = "like_count";
        public static final String COMMENT_COUNT = "comment_count";
        public static final String AVG_CONTENT_RATING = "avg_content_rating";
        public static final String ACTION_GET_CONVOS = "get_convos";
        public static final String CHANNEL_ID = "channel_id";
        public static final String JOIN_DATE = "join_date";
        public static final String MESSAGE_ID = "message_id";
        public static final String MESSAGE = "message";
        public static final String CHAT_TYPE = "chat_type";
        public static final String CHAT_IMAGE = "chat_image";
        public static final String CHAT_NAME = "chat_name";
        public static final String CONVO_ID = "convo_id";
        public static final String SENDER_ID = "sender_id";
        public static final String ATTACHMENT_ID = "attachment_id";
        public static final String ATTACHMENT_NAME = "attachment_name";
        public static final String MESSAGE_TYPE = "message_type";
        public static final String READ_STATUS = "read_status";
        public static final String DELETE_STATUS_R = "delete_status_r";
        public static final String DELETE_CONTENT = "delete_content";//deleteaddress
        public static final String DELETE_ADDRes = "deleteaddress";
        public static final String DELETE_STATUS_S = "delete_status_s";
        public static final String USER_IMAGE = "user_image";
        public static final String SENDER_OR_RECEIVER = "sender_or_receiver";
        public static final String ACTION_GET_CHAT = "get_chat";
        public static final String ACTION_SEND_MESSAGE_CHAT = "send_message_chat";
        public static final String STATUS = "status";
        public static final String FULL_USER_NAME = "full_user_name";
        public static final String USER_FULL_NAME = "user_full_name";
        public static final String RATING = "rating";
        public static final String CHILD_CAT_NAME = "child_cat_name";
        public static final String CHILD_CAT_ID = "child_cat_id";
        public static final String PARENT_CAT_NAME = "parent_cat_name";
        public static final String PARENT_CAT_ID = "parent_cat_id";
        public static final String CONTENT_COMPANY_ID = "content_company_id";
        public static final String ACTION_GET_CONTENT = "getcontent";
        //getdeliverytime
        public static final String GET_DELI_TIME = "getdeliverytime";
        public static final String CONTENT = "content";
        public static final String CHAPTERS = "chapters";
        public static final String CHAPTER_ID = "chapter_id";
        public static final String CHAPTER_NAME = "chapter_name";
        public static final String CHANGE_PASS = "change_password";//dup_password//new_password
        public static final String CHAPTER_DATA = "chapter_data";

        public static final String NEW_PASS = "new_password";
        public static final String DUPLICATE_PASS = "dup_password";


        public static final String CHAPTER_STATUS = "chapter_status";
        public static final String ACTION_GET_SIMILAR_CONTENT = "getsimilararticles";
        public static final String CATEGORY_ID = "category_id";
        public static final String CATEGORY = "category";
        public static final String ACTION_POST_CONSUMED = "postconsumed";
        public static final String SAVE_BOOK_PAGE = "save_ebook_page";
        public static final String ACTIO_GET_USER_DETAILS = "getuserdetails";
        public static final String USER_RANK = "user_rank";
        public static final String USER_TOTAL_POINTS = "user_total_points";
        public static final String TOTAL_POINTS = "total_points";
        public static final String CONTENT_CREATED_COUNT = "content_created_count";
        public static final String CONTENT_CONSUMED_COUNT = "content_consumed_count";
        public static final String ADDRESS_BOOK_ID = "address_book_id";
        public static final String ADDRESS_TYPE = "address_type";
        public static final String ADDRESS_LINE_1 = "address_line1";
        public static final String ADDRESS_LINE_2 = "address_line2";
        public static final String CITY = "city";
        public static final String STATE = "state";
        public static final String PINCODE = "pincode";
        public static final String PHONE = "phone";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String ACTION_DRAFTED_CONTENTS = "drafted_content";
        public static final String ACTION_LOAD_SHELF_CONTENTS = "load_shelf_contents";
        public static final String ACTION_GET_CURRENT_DETAILS = "get_current_details";
        public static final String BIRTH_DATE = "birthdate";
        public static final String LANDLINE = "landline";
        public static final String MOBILE = "mobile";
        public static final String GENDER = "gender";
        public static final String LOCATION = "location";
        public static final String MALE = "Male";
        public static final String ACTION_UPDATE_USER_DETAILS_EDIT_PROFILE = "update_user_details_edit_profile";
        public static final String ACTIO_GET_COMPANY_DETAILS = "getcompanydetails";
        public static final String INSERT_FEEDBACK = "insert_feedback";
        public static final String SUPPORTACTION = "insert_talk_to_us_data";
        public static final String COMPANY_LOGO = "company_logo";
        public static final String COMPANY_NAME = "company_name";
        public static final String COMPANY_INFO = "company_info";
        public static final String TOTAL_CONTENT_CREATED = "total_content_created";
        public static final String TOTAL_USERS = "total_users";
        public static final String TOTAL_CONTENT_CONSUMED = "total_content_consumed";
        public static final String ACTION_FETCH_ANNOUNCEMENTS = "fetchannouncements";
        public static final String ANNOUNCEMENT_ID = "announcement_id";
        public static final String ANNOUNCEMENT = "announcement";
        public static final String SELECTED_FILTERS = "selectedFilters";
        public static final String ACTION_GET_CONTENT_FOR_LISTING = "get_content_for_listing";
        public static final String WITHIN_COMPANY = "within_company";
        public static final String SUB_CAT = "sub_cat";
        public static final String ACTION_GET_POPULAR_CONTENT = "get_popular_content";
        public static final String USER_EMAIL = "user_email";
        public static final String IS_ADMIN = "is_admin";
        public static final String COMPANY_FEATURES = "company_features";
        public static final String COMPANY_FEATURES_ID = "id";
        public static final String COMPANY_FEATURES_COMPANY_ID = "company_id";
        public static final String COMPANY_FEATURES_FEATURE_ID = "company_id";
        public static final String COMPANY_FEATURES_VALUE = "value";
        public static final String COMPANY_FEATURES_STATUS = "status";
        public static final String ACTION_GET_PARENT_AND_CHILD_CATEGORY = "get_parent_and_child_category";
        public static final String CAT_ID = "cat_id";
        public static final String CHILD = "child";
        public static final String CAT_NAME = "cat_name";
        public static final String ID = "id";
        //add_tags_create
        //text
        public static final String TEXT = "text";
        public static final String ADD_TAGS_CREATE = "add_tags_create";


        public static final String NAME = "name";//forgot_password
        public static final String FORGOT_PASS = "forgot_password";
        public static final String ACTION_GET_CHILD = "get_category_child";
        public static final String ACTION_GET_CONTENT_FOR_CATEGORY_LISTING = "get_content_for_category_listing";
        public static final String ACTION_GET_POPULAR_CONTENT_CATEGORY = "get_popular_content_category";
        public static final String SEARCH_MAIN = "search_main";
        public static final String PAGE = "page";
        public static final String YOUTUBE_PAGE_TOKEN = "youtube_page_token";
        public static final String KEY = "key";
        public static final String SEARCH_BY = "search_by";
        public static final String CONTENT_ID_S = "content_id_s";
        public static final String USER_ID_SOURCE = "user_id_source";
        public static final String USER_SOURCE_TYPE = "user_source_type";
        public static final String SOURCE_NAME = "source_name";
        public static final String PAGE_TOKEN = "pageToken";
        public static final String ACTION_GET_FILES_CHAT = "get_files_chat";
        public static final String FILE_TYPE = "file_type";
        public static final String ACTION_GET_MEMBERS = "get_members";
        public static final String FULL_NAME = "full_name";
        public static final String ACTION_GET_USER_CHAT_DETAILS = "get_user_chat_details";
        public static final String ACTION_UPDATE_ATTACHMENT = "update_attachment";
        public static final String ACTION_REPO_CHECK = "repo_check";
        public static final String TYPE = "type";
        public static final String RESPONSE_STATUS = "response_status";
        public static final String ACTION_REPO_CREATE = "repo_create";
        public static final String ACTION_REPO_DETAILS = "repo_detail";
        public static final String REPO_ID = "repo_id";
        public static final String CURRENT_FOLDER = "current_folder";
        public static final String OBJECT_TYPE = "object_type";
        public static final String FID = "fid";
        public static final String PATH = "path";
        public static final String ACTION_REPO_CREATE_FOLDER = "repo_create_folder";
        public static final String PARENT_FOLDER = "parent_folder";
        public static final String ACTION_REPO_RENAME_FOLDER = "repo_rename_folder";
        public static final String DATA_ID = "data_id";
        public static final String NEW_NAME_OBJ = "new_name_obj";
        public static final String ACTION_REPO_DELETE_FOLDER = "repo_delete_folder";
        public static final String ACTION_UPLOADED_FILE_DATABASE_ENTRY = "uploaded_file_database_entry";
        public static final String FILE_NAME = "file_name";
        public static final String ACTION_GET_CURATED_CONTENT = "getcuratedcontent";
        public static final String SOURCE_ID = "source_id";
        public static final String SOURCE_PROFILE_PIC = "source_profile_pic";
        public static final String ISBN = "isbn";
        public static final String CONTRIBUTOR_NAME = "contributor_name1";
        public static final String WEIGHT = "weight";
        public static final String PRODUCT_FORM = "product_form";
        public static final String PAGE_NO = "page_no";
        public static final String PUBLISHER_NAME = "publisher_name";
        public static final String PUBLICATION_PLACE = "publication_place";
        public static final String TEXT_LANGUAGE = "text_language";
        public static final String PRICE = "price";
        public static final String SEARCH = "search";
        public static final String SIMILAR = "similar";
        public static final String LINK = "link";
        public static final String URL = "url";
        public static final String IMAGE = "image";
        //display_partners
        public static final String DISPLAY_PARTNERS = "display_partners";
        public static final String VIDEOID = "videoid";
        public static final String IMAGESRC = "imagesrc";
        //update_profile_picture_user
        public static final String ACTION_UPDATE_PROFILE_PIC = "update_profile_picture_user";
        public static final String ACTION_GET_PODCAST_DETAIL = "get_podcast_detail";
        public static final String ACTION_GET_PODCAST_SIMILAR = "get_podcast_similar";
        public static final String ACTION_GET_MAIN_PAGE_DETAILS_FOR_STARTUP = "get_main_page_details_for_startup";
        public static final String COUNT_NOTIFICATION = "count_notification";
        public static final String COUNT_MESSAGE = "count_message";
        public static final String CODE = "code";
        public static final String FEATURES = "features";
        public static final String FEATURE_ID = "feature_id";
        public static final String VALUE = "value";
        public static final String ACTION_GET_HOME_BANNER_DATA = "get_home_banners_data";
        public static final String INTENT_STATUS = "intent_status";
        public static final String INTENT_TYPE = "intent_type";
        public static final String INTENT_PARAMETER_1 = "intent_parameter_1";
        public static final String INTENT_PARAMETER_2 = "intent_parameter_2";
        public static final String MAIN_MESSAGE = "main_message";
        public static final String FULLNAME = "fullname";
        public static final String ORIGIN = "origin";
        public static final String TOKEN = "token";
        public static final String ACTION_GET_USERS_FOR_CHAT = "get_users_for_chat";
        public static final String CHAT_TYPE_I = "chat_type_i";
        public static final String ACTION_START_CHAT = "start_chat";
        public static final String USERS = "users";
        public static final String DEVICE_ID = "device_id";
        public static final String APP_VERSION = "app_version";
        public static final String READ_COUNT = "read_count";
        public static final String ACTION_LOGOUT_APP = "logout_app";
        public static final String RESPONSE_UNAUTHORISED = "401";
        public static final String TOTAL_TIME = "total_time";
        static final String COMMENT = "comment";
        //submitrating
        public static final String SUBMITRATING = "submitrating";
        public static final String ACTION_POST_COMMENT = "postcomment";
        public static final String ACTION_LIKE_CONTENT = "likecontent";
        public static final int COMMENT_TYPE_COMMENT = 1254;
    }

    public static class ExploreType {
        public static final int banner_slider = 1;
        public static final int dual_image = 2;
        public static final int poster_image = 3;
        public static final int slider = 4;
        public static final int grid = 5;
        public static final int categories = 6;
        public static final int explore = 7;
    }

    public static class Content {
        public static final String CONTENT_TYPE = "content type";
        public static final int CONTENT_TYPE_ERROR = -1;
        public static final int CONTENT_TYPE_PROGRESS_LOADER = 0;
        public static final int CONTENT_TYPE_ALL = 0;
        public static final int CONTENT_TYPE_ARTICLE = 1;
        public static final int CONTENT_TYPE_VIDEO = 2;
        public static final int CONTENT_TYPE_COURSE = 3;
        public static final int CONTENT_TYPE_PODCAST = 4;
        public static final int CONTENT_TYPE_INFOGRAPHIC = 5;
        public static final int CONTENT_TYPE_POST = 6;
        public static final int CONTENT_TYPE_EBOOK = 7;
        public static final int CONTENT_TYPE_ANNOUNCEMENT = 8;

        public static final int CONTENT_TYPE_BOOK_LIBRARY = 9;
        public static final int CONTENT_TYPE_DATALOAD = 88;
        public static final String CONTENT_LIKED = "1";
        public static final String CONTENT_UNLIKED = "0";
        public static final String CONTENT_STATUS_TYPE_DEFAULT = "1";

        public static final String ARTICLE = "Article";
        public static final String INFOGRAPHIC = "Infographic";
        public static final String VIDEO = "Video";
        public static final String COURSE = "Course";
        public static final String PODCAST = "Podcast";
        public static final String EBOOK = "EBook";
        public static final String BOOK_LIBRARY = "Book Library";

        public static final String ARTICLES = "article";
        public static final String COURSES = "course";


        public static final int EMPTY_CONTENT_LIST_ICON = R.mipmap.ic_fevicon;
        public static final int CONTENT_TYPE_SUGGESTION = 101;
    }

    public static class Create {
        public static final String POST_TEXT = "post_text";
        public static final String USER_ID = "user_id";
        public static final String ACTION = "action";
        //submit_post
        public static final String SUBMIT_POST = "submit_post";
    }

    public static class Category {
        public static final String GET_PARENT_CATEGORY = "get_category_parent";
        public static final String GET_CHILD_CATEGORY = "get_category_child";
        public static final String CATEGORY_ID = "cat_id";
        public static final String CATEGORY_NAME = "cat_name";

        public static final String CATEGORY_CHILD_ID = "id";
        public static final String CATEGORY_CHILD_NAME = "name";

        public static final String CATEGORY_SUCCESS = "success";
        public static final String PARENR_CAT_ID = "parent_cat_id";

        public static final int CATEGORY_TYPE_PARENT = 2121;
        public static final int CATEGORY_TYPE_CHILD = 2122;

        public static final String CATEGORY_SEARCH_BY_TITLE = "111";
        public static final String CATEGORY_SEARCH_BY_AUTHOR = "222";
        public static final String CATEGORY_SEARCH_BY_ISBN = "333";

    }

    public static class Tag {
        public static final String TAG_ID = "id";
        public static final String TAG_TEXT = "text";
    }

    public static class Comment {
        public static final int COMMENT_TYPE_PROGRESS = 0;
        public static final int COMMENT_TYPE_COMMENT = 1;
        public static final String LOAD_COMMENTS = "loadcomments";
        public static final String CREATED_AT = "created_at";
    }

    public static class Article {
        public static final String ARTICLE = "article";
        public static final String TITLE = "title";
        public static final String ACTION = "action";
        public static final String CREATE_ARTICLE = "create_article";
        public static final String CATEGORY = "category";
        public static final String USER_ID = "user_id";
        public static final String EXISTING_ARRAY = "existing_array";
        public static final String NEW_ARRAY = "new_array";
        public static final String FEAT_IMAGE = "feat_img";
        public static final String DRAFT_STATUS = "draft_status";
        public static final String CONTENT_ID = "content_id";
        public static final String SAVE_ARTICLE_DATA = "save_article_data";
        public static final String PRIVACY_STATUS = "privacy_status";

    }

    public static class Infographic {
        public static final String TITLE = "title";
        public static final String ACTION = "action";
        public static final String UPLOAD_INFOGRAPHIC = "upload_infographic";
        public static final String DESCRIPTION = "description";
        public static final String IMAGE_URL = "image_url";
        public static final String CATEGORY = "category";
        public static final String USER_ID = "user_id";
        public static final String EXISTING_ARRAY = "existing_array";
        public static final String NEW_ARRAY = "new_array";
        public static final String CONTENT_ID = "content_id";
        public static final String PRIVACY_STATUS = "privacy_status";
    }

    public static class Video {
        public static final String TITLE = "title";
        public static final String ACTION = "action";
        public static final String UPLOAD_VIDEO = "upload_video";
        public static final String DESCRIPTION = "description";
        public static final String VIDEO_URL = "video_url";
        public static final String CATEGORY = "category";
        public static final String USER_ID = "user_id";
        public static final String EXISTING_ARRAY = "existing_array";
        public static final String NEW_ARRAY = "new_array";
        public static final String CONTENT_ID = "content_id";
        public static final String PRIVACY_STATUS = "privacy_status";
    }

    public static class Podcast {
        public static final String TITLE = "title";
        public static final String ACTION = "action";
        public static final String UPLOAD_PODCAST = "upload_podcast";
        public static final String DESCRIPTION = "description";
        public static final String AUDIO_URL = "audio_url";
        public static final String TRANSCRIPT = "transcript";
        public static final String CATEGORY = "category";
        public static final String USER_ID = "user_id";
        public static final String EXISTING_ARRAY = "existing_array";
        public static final String NEW_ARRAY = "new_array";
        public static final String CONTENT_ID = "content_id";
        public static final String PRIVACY_STATUS = "privacy_status";
    }

    public static class REQUEST_BOOK {

        public static final String TITLE = "book_name";
        public static final String AUTHOR = "author";
        public static final String ISBN = "isbn";
        public static final String LANGUAGE = "language";
        public static final String EMAIL = "email";
        public static final String USRE_ID = "user_id";
        public static final String CONTENT_TYPE = "content_type";
        public static final String ACTION = "action";
    }

    public static class EBOOK {
        public static final String TITLE = "title";
        public static final String ACTION = "action";
        public static final String UPLOAD_EBOOK = "upload_ebook";
        public static final String DESCRIPTION = "description";
        public static final String EBOOK_URL = "ebook_url";
        public static final String ISBN = "isbn_ebook";
        public static final String CATEGORY = "category";
        public static final String USER_ID = "user_id";
        public static final String EXISTING_ARRAY = "existing_array";
        public static final String NEW_ARRAY = "new_array";
        public static final String CONTENT_ID = "content_id";
        public static final String PRIVACY_STATUS = "privacy_status";
    }

    public static class COURSE {
        public static final String TITLE = "title";
        public static final String ACTION = "action";
        public static final String CREATE_COURSE = "create_course";
        public static final String DESCRIPTION = "description";
        public static final String CATEGORY = "category";
        public static final String USER_ID = "user_id";
        public static final String EXISTING_ARRAY = "existing_array";
        public static final String NEW_ARRAY = "new_array";
        public static final String CONTENT_ID = "content_id";
        public static final String PRIVACY_STATUS = "privacy_status";
    }

    public static class RequestCode {
        public static final int REQUEST_LIKE_COMMENT_COUNT = 101;
    }

    public static class Message {

        public static final String SYNC_STATUS_DONE = "Done";
        public static final String SYNC_STATUS_IN_PROGRESS = "In Progress";
        public static final String SYNC_STATUS_FAILED = "Failed";
        public static final String TEMP_ATTACHMENT = "temp_attachment";
        public static final String MESSAGE_TYPE_NORMAL = "1";
        public static final String MESSAGE_TYPE_FILE = "2";
        public static final String MESSAGE_TYPE_TEXT_VALUE = "0";
        public static final String MESSAGE_STATUS_TYPE_NORMAL = "1";
    }

    public static class File {
        public static final String FILE_TYPE_IMAGE = "1";
        public static final String FILE_TYPE_AUDIO = "2";
        public static final String FILE_TYPE_VIDEO = "3";
        public static final String FILE_TYPE_PDF = "4";
        public static final String FILE_TYPE_ARCHIVE = "5";
        public static final String FILE_TYPE_TEXT = "6";
        public static final String FILE_TYPE_CODE = "7";
        public static final String FILE_TYPE_EXCEL = "8";
        public static final String FILE_TYPE_DOC = "9";
        public static final String FILE_TYPE_POWERPOINT = "10";
        public static final String FILE_TYPE_OTHER = "11";
        public static final String ROOT_DIR_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Indiareads";
    }

    public static class Order {
        public static final String ORDER_BOOK = "orderplace";
        public static final String ORDER_ID = "order_id";
        public static final String CANCELLED = "-1";
        public static final String ORDER_PLACED = "1";
        public static final String ORDER_PROCESSING = "2";
        public static final String ORDER_DISPATCHED = "3";
        public static final String DELIVERED = "4";
        public static final String PICKUP_REQUESTED = "5";
        public static final String PICKUP_PROCESSING = "6";
        public static final String SENT_TO_COURIER = "7";
        public static final String RETURNED = "8";
    }

    public static class Pubnub {
        public static final String PUBNUB_PUBLISH_KEY = BuildConfig.PUBNUB_PUBLISH_KEY;
        public static final String PUBNUB_SUBSCRIBE_KEY = BuildConfig.PUBNUB_SUBSCRIBE_KEY;
        public static final String PUBLISH_TYPE_CHAT = "chat";
        public static final String PUBLISH_ORIGIN_ANDROID = "android";
    }

}
