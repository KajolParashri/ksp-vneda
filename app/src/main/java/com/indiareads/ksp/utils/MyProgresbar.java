package com.indiareads.ksp.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager;

import com.indiareads.ksp.R;

public class MyProgresbar extends Dialog {

    public MyProgresbar(Context context) {
        super(context);
    }

    public MyProgresbar(Context context, int theme) {
        super(context, theme);
    }

    public static MyProgresbar show(Context context, int theme) {
        MyProgresbar dialog = new MyProgresbar(context, theme);
        dialog.setTitle("");
        dialog.setContentView(R.layout.custom_progress_dialog);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.2f;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.show();
        return dialog;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

}

