package com.indiareads.ksp.utils;

import java.util.ArrayList;

public class FileExtensionUtil {

    public static String getExtension(int typeNumber) {
        String str = "";
        switch (typeNumber) {
            case 1:
                str = "ai,bmp,gif,ico,jpeg,jpg,png,ps,psd,svg,tif";
                break;
            case 2:
                str = "aif,cda,mid,midi,mp3,mpa,ogg,wav,wma,wpl";
                break;
            case 3:
                str = "3g2,3gp,avi,flv,h264,m4v,mkv,mov,mp4,mpg,mpeg,rm,swf,wmv";
                break;
            case 4:
                str = "pdf";
                break;
            case 5:
                str = "zip,z,rar,tar.gz,rpm,pkg,deb,apk";
                break;
            case 6:
                str = "txt";
                break;
            case 7:
                str = "asp,aspx,cer,cfm,cgi,pl,css,ht,html,js,jsp,php,py,rss,xhtml,c,class,cpp,cs,h,java,sh,swift,vb";
                break;
            case 8:
                str = "xls,xlsx,xlr,ods";
                break;
            case 9:
                str = "doc,docx,odt,rtf,tex,wks,wps,wpd";
                break;
            case 10:
                str = "ppt,pptx";
                break;
        }
        return str;
    }

    public static ArrayList<String> checkForImage() {
        ArrayList<String> list = new ArrayList();

        ArrayList<String> listMain = getFormat(1);
        for (String str : listMain) {
            list.add(str);
        }

        return list;
    }

    public static ArrayList<String> checkForRepo() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {

            ArrayList<String> listMain = getFormat(i);
            for (String str : listMain) {
                list.add(str);
            }
        }

        return list;
    }


    public static ArrayList<String> checkForArticle() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 5) {

            } else {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }

        return list;
    }

    public static ArrayList<String> checkForCourse() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 5) {

            } else {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }

        return list;
    }


    public static ArrayList<String> checkForPodcast() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 2) {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }
        return list;
    }


    public static ArrayList<String> checkForVideo() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 3) {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }
        return list;
    }

    public static ArrayList<String> checkForBooks() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 4) {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }
        return list;
    }

    public static ArrayList<String> checkForInfo() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 1 || i == 4) {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }
        return list;
    }


    public static ArrayList<String> checkForAudio() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 2) {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }
        return list;
    }

    //audio.video,image,doc,sheet,file
    public static ArrayList<String> checkForDoc() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 6 || i == 9 || i == 10) {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }
        return list;
    }

    public static ArrayList<String> checkForPDF() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 4) {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }
        return list;
    }

    public static ArrayList<String> checkForSheet() {
        ArrayList<String> list = new ArrayList();
        for (int i = 1; i < 11; i++) {
            if (i == 8) {
                ArrayList<String> listMain = getFormat(i);
                for (String str : listMain) {
                    list.add(str);
                }
            }
        }
        return list;
    }

    public static ArrayList<String> getFormat(int number) {
        String format1 = getExtension(number);
        String[] format1arr = format1.split(",");
        ArrayList<String> list = new ArrayList();
        for (int i = 0; i < format1arr.length; i++) {
            String item = format1arr[i];
            list.add(item);
        }
        return list;
    }
}
